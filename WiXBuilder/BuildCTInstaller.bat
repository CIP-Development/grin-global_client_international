
IF [%1]==[] GOTO DEFAULTPATH
  SET ProjectBinariesPath=%1
  SET ProjectBinariesPath=%ProjectBinariesPath:"=%
GOTO ENDPATH
:DEFAULTPATH
  SET ProjectBinariesPath=.\Debug
:ENDPATH

IF [%2]==[] GOTO DEFAULTVERSIONNUMBER
  SET ProjectBinariesVersion=%2
  SET ProjectBinariesVersion=%ProjectBinariesVersion:"=%
GOTO ENDVERSIONNUMBER
:DEFAULTVERSIONNUMBER
FOR /F "tokens=2 delims==" %%I IN ('wmic datafile where "name='%ProjectBinariesPath:\=\\%GRINGlobal-CuratorTool.exe'" get version /value') DO SET ProjectBinariesVersion=%%I
SET ProjectBinariesVersion=%ProjectBinariesVersion%
:ENDVERSIONNUMBER

SET ProjectProgramDataPath=%ProjectBinariesPath%\DefaultProgramDataFiles
SET ProjectFormsPath=%ProjectBinariesPath%\Forms
SET ProjectImagesPath=%ProjectBinariesPath%\Images
SET ProjectReportsPath=%ProjectBinariesPath%\Reports
SET ProjectWizardsPath=%ProjectBinariesPath%\Wizards

"%WIX%bin\heat.exe" dir "%ProjectFormsPath%" -var env.ProjectFormsPath -cg FormsGroup -dr INSTALLDIR -gg -sfrag -template fragment -out ProjectForms.wxs
"%WIX%bin\candle.exe" -arch x64 ProjectForms.wxs

"%WIX%bin\heat.exe" dir "%ProjectImagesPath%" -var env.ProjectImagesPath -cg ImagesGroup -dr INSTALLDIR -gg -sfrag -template fragment -out ProjectImages.wxs
"%WIX%bin\candle.exe" -arch x64 ProjectImages.wxs

"%WIX%bin\heat.exe" dir "%ProjectReportsPath%" -var env.ProjectReportsPath -cg ReportsGroup -dr INSTALLDIR -gg -sfrag -template fragment -out ProjectReports.wxs
"%WIX%bin\candle.exe" -arch x64 ProjectReports.wxs

"%WIX%bin\heat.exe" dir "%ProjectWizardsPath%" -var env.ProjectWizardsPath -cg WizardsGroup -dr INSTALLDIR -gg -sfrag -template fragment -out ProjectWizards.wxs
"%WIX%bin\candle.exe" -arch x64 ProjectWizards.wxs

"%WIX%bin\candle.exe" -arch x64 CuratorToolProduct.wxs
"%WIX%bin\light.exe" CuratorToolProduct.wixobj ProjectForms.wixobj ProjectImages.wixobj ProjectReports.wixobj ProjectWizards.wixobj -out GrinGlobal_Client_Installer.msi

"%WIX%bin\candle.exe" -arch x64 -ext WiXBalExtension -ext WixUtilExtension -ext WiXNetFxExtension CuratorToolBundle.wxs
"%WIX%bin\light.exe" CuratorToolBundle.wixobj -ext WiXBalExtension -ext WixUtilExtension -ext WiXNetFxExtension -out GrinGlobal_Client_Installer.exe