﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GRINGlobal.Client.Common;

namespace InventoryAttachmentWizard
{
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
        //string PreferredDataview { get; }
        //bool EditMode { get; set; }
    }

    public partial class InventoryAttachmentWizard : Form, IGRINGlobalDataWizard
    {
        char _lastDGVCharPressed;
        string _originalPKeys;
        string _originalFilePaths;
        string _previousInventoryIDs;
        string _currentInventoryIDs;
        string _previousFolderPathOrFileNames = "";
        string _currentFolderPathOrFileNames = "";
        SharedUtils _sharedUtils;
        // Filename Parsing Options...
        string _includeFileFilter = "*.jpg; *.png; *.gif; *.xls; *.xlsx; *.doc; *.docx; *.ppt; *.pptx; *.pdf; *.txt; *.rtf; *.zip";
        string _excludedFileFilter = "*.tif; *_t.jpg; *.exe; *.dll";
        string _parseMethod = "SmartParse";
        bool _useCaseSensitiveFileNames = false;
        char[] _delimiters = new char[2] { '_', ' ' };
        int[] _fixedFieldTokenLengths = new int[12];
        Dictionary<string, int> _tokenPosition = new Dictionary<string, int>(6);
        // SmartParse default parameters...
        string _allowedFormTypeCode = " ** BA BD BL CA CL CM CT DN EA ER FI FR GS HE HS IO IV LA LV MF MI MS PA PD PF PL PO PR RH RN RT SC SD SG SP ST TC TU VI ";
        string _allowedDescriptionCode = " Achenes Bud Branch Bulb Cladode Collection_Site Ear Flower Field Fruit Greenhouse Head Kernels Leaves Miscellaneous Panicle Plant Pod Root Seeds Seedling Shoot Stem Silique Stalk Spike Spine Stipule Tendril Umbel Vegetative ";
        string _allowedInventoryPrefix = " Ames PI ";
        string _supportedImageExtensions = "";
        // Attachment info...
        Dictionary<string, int> _attachmentFilesDictionary = new Dictionary<string, int>();
        Dictionary<string, int> _dragAndDropAttachmentFilesDictionary = new Dictionary<string, int>();
        ImageList _treeviewImageList;
        DataSet _dsInventoryAttach = null;
        DataTable _inventoryAttach = null;
        BindingSource _inventoryAttachBindingSource = null;
        ImageList _imageList = null;

        public string FormName
        {
            get
            {
                return "Inventory Attachment Wizard";
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();
                //if (_orderRequest != null && _changedRecords != null && _changedRecords.Tables.Contains(_orderRequest.TableName))
                //{
                //    dt = _changedRecords.Tables[_orderRequest.TableName].Copy();
                //}
                return dt;
            }
        }

        public string PKeyName
        {
            get
            {
                return "inventory_attach_id";
            }
        }

        public InventoryAttachmentWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();

            _inventoryAttachBindingSource = new BindingSource();
            _inventoryAttachBindingSource.ListChanged += new ListChangedEventHandler(_inventoryAttachBindingSource_ListChanged);
            _inventoryAttachBindingSource.CurrentChanged += new EventHandler(_inventoryAttachindingSource_CurrentChanged);

            // Copy the parameters into private class variables...
            _originalPKeys = pKeys;  // ex: ":accessionid=1453504,1445409; :inventoryid=; :orderrequestid=; :cooperatorid=; :geographyid=; :taxonomygenusid=; :cropid="
            _previousInventoryIDs = "";
            _previousFolderPathOrFileNames = "";
            _sharedUtils = sharedUtils;

            _currentInventoryIDs = LoadInventoryParameterList(pKeys);
            _currentFolderPathOrFileNames = LoadFilePathsParameterList(pKeys);
        }

        private string LoadInventoryParameterList(string pKeys)
        {
            string inventoryIDList = ":inventoryid=";
            foreach (string pkeyToken in pKeys.Split(';'))
            {
                string[] keyValue = pkeyToken.Split('=');
                if (keyValue[0].Trim().ToUpper() == ":INVENTORYID")
                {
                    inventoryIDList += keyValue[1];
                }
                else
                {
                    string seQueryString = "";
                    switch (keyValue[0].Trim().ToUpper())
                    {
                        case ":ACCESSIONID":
                            seQueryString = "@accession.accession_id IN (" + keyValue[1] + ")";
                            break;
                        case ":ORDERREQUESTID":
                            seQueryString = "@order_request.order_request_id IN (" + keyValue[1] + ")";
                            break;
                        default:
                            break;
                    }
                    // Use the Search Engine to find the inventory IDs...
                    if (_sharedUtils != null)
                    {
                        DataSet ds = _sharedUtils.SearchWebService(seQueryString, true, true, null, "inventory", 0, 0);
                        // Build the parameter string to pass to the get_order_request, order_wizard_get_order_request_item, and get_order_request_action dataview...
                        if (ds.Tables.Contains("SearchResult"))
                        {
                            // Build a list of order_request_ids to use for gathering the order_request_items...
                            foreach (DataRow dr in ds.Tables["SearchResult"].Rows)
                            {
                                inventoryIDList += dr["ID"].ToString() + ",";
                            }
                            inventoryIDList = inventoryIDList.TrimEnd(',');
                        }
                    }
                }
            }

            return inventoryIDList;
        }

        private string LoadFilePathsParameterList(string pKeys)
        {
            string filepathList = "";
            foreach (string pkeyToken in pKeys.Split(';'))
            {
                string[] keyValue = pkeyToken.Split('=');
                if (keyValue[0].Trim().ToUpper() == ":FILEPATHS")
                {
                    filepathList += keyValue[1];
                }
            }
            return filepathList;
        }

        private string LoadFormTypeCodes()
        {
            string returnFormTypeCodes = "";

            DataTable formTypeCodeTable = _sharedUtils.GetLocalData("SELECT * FROM code_value_lookup WHERE group_name = @groupname", "@groupname=germplasm_form");
            if (formTypeCodeTable != null &&
                formTypeCodeTable.Rows.Count > 0)
            {
                foreach (DataRow dr in formTypeCodeTable.Rows)
                {
                    returnFormTypeCodes += dr["display_member"].ToString() + " ";
                }
            }

            return " " + returnFormTypeCodes + " ";
        }

        private string LoadDescriptionCodes()
        {
            string returnDescriptionCodes = "";

            DataTable descriptionCodeTable = _sharedUtils.GetLocalData("SELECT * FROM code_value_lookup WHERE group_name = @groupname", "@groupname=attach_description_code");
            if (descriptionCodeTable != null &&
                descriptionCodeTable.Rows.Count > 0)
            {
                foreach (DataRow dr in descriptionCodeTable.Rows)
                {
                    returnDescriptionCodes += dr["display_member"].ToString() + " ";
                }
            }

            return " " + returnDescriptionCodes + " ";
        }

        private string LoadInventoryPrefix()
        {
            string returnInventoryPrefix = "";

            DataSet ds = _sharedUtils.GetWebServiceData("inventory_attach_wizard_get_distinct_prefix", "", 0, 0);
            if (ds != null &&
                ds.Tables.Contains("inventory_attach_wizard_get_distinct_prefix") &&
                ds.Tables["inventory_attach_wizard_get_distinct_prefix"].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables["inventory_attach_wizard_get_distinct_prefix"].Rows)
                {
                    returnInventoryPrefix += dr["prefix"].ToString() + " ";
                }
            }

            return " " + returnInventoryPrefix + " ";
        }

        private void InventoryAttachmentWizard_Load(object sender, EventArgs e)
        {
            // Load the form_type_codes from the local LUT database...
            string localDBInventoryFormTypeCodes = LoadFormTypeCodes();
            if (!string.IsNullOrEmpty(localDBInventoryFormTypeCodes.Trim())) _allowedFormTypeCode = localDBInventoryFormTypeCodes;

            // Load the inventory prefixes from the remote database...
            string remoteDBInventoryPrefix = LoadInventoryPrefix();
            if (!string.IsNullOrEmpty(remoteDBInventoryPrefix.Trim())) _allowedInventoryPrefix = remoteDBInventoryPrefix;

            // Load the attach_description_codes from the remote database...
            string remoteDBDescriptionCodes = LoadDescriptionCodes();
            if (!string.IsNullOrEmpty(remoteDBDescriptionCodes.Trim())) _allowedDescriptionCode = remoteDBDescriptionCodes;

            // Load .NET Image object supported file extensions...
            string supportedImageExtensions = "";
            foreach (System.Drawing.Imaging.ImageCodecInfo imageCodec in System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders())
            {
                supportedImageExtensions += imageCodec.FilenameExtension.ToLower() + ";";
            }
            if (!string.IsNullOrEmpty(supportedImageExtensions.Trim())) _supportedImageExtensions += supportedImageExtensions;

            // Load the inventory attachment filenames into a dictionary for resolving to inventory PKEYs...
            //_attachmentDictionary = LoadAttachmentDictionary(_previousFolderPathOrFileNames);
            _attachmentFilesDictionary = LoadAttachmentDictionary(_currentFolderPathOrFileNames);
// Get the inventoryID from the parameters passed into this class (if one was passed)...
int inventoryID = -1;
if (_currentInventoryIDs.ToUpper().Contains(":INVENTORYID="))
{
    string pkeys = _currentInventoryIDs.Split('=')[1];
    if(!pkeys.Contains(','))
    {
        if(!int.TryParse(pkeys, out inventoryID))
        {
            inventoryID = -1;
        }
    }
}
// Remember the association between filepaths and the inventoryID passed into this class...
foreach (string key in _attachmentFilesDictionary.Keys)
{
    if (!_dragAndDropAttachmentFilesDictionary.ContainsKey(key))
    {
        _dragAndDropAttachmentFilesDictionary.Add(key, inventoryID);
    }
}

            RefreshData();
//for (int i = 0; i < _attachmentDictionary.Count; i++)
//{
//    string fullPath = _attachmentDictionary.ElementAt(i).Key;
//    int inventoryID = 4445409;
////    if (_attachmentDictionary.ElementAt(i).Value < 0) _attachmentDictionary[_attachmentDictionary.ElementAt(i).Key] = 4445409;
//    _attachmentDictionary[fullPath] = inventoryID;
//    DataRow[] drs = _inventoryAttach.Select("virtual_path='" + fullPath.Trim() + "' AND inventory_id = -1");
//    if (drs.Length > 0)
//    {
//        foreach (DataRow dr in drs)
//        {
//            dr["inventory_id"] = inventoryID;
//        }
//    }
//}

            // Bind and format the controls on the panel...
            bindControls(ux_panelFormView.Controls, _inventoryAttachBindingSource);
            formatControls(ux_panelFormView.Controls, _inventoryAttachBindingSource);

            // Wireup the event handlers for Foreign Key columns...
            ux_datagridviewAttachments.CellFormatting += new DataGridViewCellFormattingEventHandler(ux_datagridview_CellFormatting);
            ux_datagridviewAttachments.DataError += new DataGridViewDataErrorEventHandler(ux_datagridview_DataError);
            ux_datagridviewAttachments.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(ux_datagridview_EditingControlShowing);
            ux_datagridviewAttachments.PreviewKeyDown += new PreviewKeyDownEventHandler(ux_datagridview_PreviewKeyDown);
            ux_datagridviewAttachments.KeyDown += new KeyEventHandler(ux_datagridview_KeyDown);

            // Get language translations for the components and controls in this applicataion...
            if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);
        }

        private void InventoryAttachmentWizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Save local user setttings...
            Properties.Settings.Default.Save();

            // Check to see if there are unsaved attachment changes and if so
            // ask the user if they would like to save them before exiting the wizard...
            if (_inventoryAttach.GetChanges() != null)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} unsaved row change(s), are you sure you want to cancel?", "Cancel Edits", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "InventoryAttachmentWizard_FormClosingMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = _inventoryAttach.GetChanges().Rows.Count.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                if (DialogResult.No == ggMessageBox.ShowDialog())
                {
                    e.Cancel = true;
                }
            }
        }

/* Old LoadAttachmentDictionary()
        private Dictionary<string, int> LoadAttachmentDictionary(string fullFolderPathOrFileNameCollection)
        {
            Dictionary<string, int> attachmentDictionary = new Dictionary<string, int>();

            if (System.IO.Directory.Exists(fullFolderPathOrFileNameCollection))
            {
                // This is a folder so consume everything inside of it - starting with  
                // adding all files in the root folder to the dictionary...
                System.IO.DirectoryInfo folderInfo = new System.IO.DirectoryInfo(fullFolderPathOrFileNameCollection);
                AddAttachmentCollection(attachmentDictionary, folderInfo);
                // Now look for subfolders and gather up a collection of them also to recursively iterate through...
                System.IO.DirectoryInfo[] subFolders = folderInfo.GetDirectories("*.*", System.IO.SearchOption.AllDirectories);
                if (subFolders != null &&
                    subFolders.Length > 0)
                {
                    // Process each subfolder in the collection...
                    foreach (System.IO.DirectoryInfo di in subFolders)
                    {
                        // Add all files in the subfolder to the dictionary...
                        AddAttachmentCollection(attachmentDictionary, di);
                    }
                }
            }
            else
            {
                // This is a collection of filenames (and or folder names)...
                string[] fileOrFolderTokens = fullFolderPathOrFileNameCollection.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                // Iterate through the string tokens looking for file or folder names...
                foreach (string fileOrFolderToken in fileOrFolderTokens)
                {
                    if (System.IO.File.Exists(fileOrFolderToken))
                    {
                        // This is an individual file...
                        System.IO.FileInfo fi = new System.IO.FileInfo(fileOrFolderToken);
                        if (_allowedAttachmentExt.Contains(fi.Extension))
                        {
                            int inventory_id = FindInventoryIDFromFileNameOrPath(fi.FullName);
                            //int inventory_id = FindInventoryID(fi.Name);
                            if (inventory_id > 0)
                            {
                                attachmentDictionary.Add(fi.FullName, inventory_id);
                            }
                            else
                            {
                                // The inventory_id could not be found from the filename or path so don't add it...
                            }
                        }
                    }
                    else if (System.IO.Directory.Exists(fileOrFolderToken))
                    {
                        // This is a folder which must be handled recursively because it can contain
                        // multiple files and subfolders...
                        System.IO.DirectoryInfo folderInfo = new System.IO.DirectoryInfo(fileOrFolderToken);
                        AddAttachmentCollection(attachmentDictionary, folderInfo);
                        // Now look for subfolders and gather up a collection of them also to recursively iterate through...
                        System.IO.DirectoryInfo[] subFolders = folderInfo.GetDirectories("*.*", System.IO.SearchOption.AllDirectories);
                        if (subFolders != null &&
                            subFolders.Length > 0)
                        {
                            // Process each subfolder in the collection...
                            foreach (System.IO.DirectoryInfo di in subFolders)
                            {
                                // Add all files in the subfolder to the dictionary...
                                AddAttachmentCollection(attachmentDictionary, di);
                            }
                        }
                    }
                }
            }

            return attachmentDictionary;
        }
*/

        private Dictionary<string, int> LoadAttachmentDictionary(string fullFolderPathOrFileNameCollection)
        {
            Dictionary<string, int> attachmentDictionary = new Dictionary<string, int>();

            // This is a collection of filenames and/or folder names...
            string[] fileOrFolderTokens = fullFolderPathOrFileNameCollection.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
            // Iterate through the string tokens looking for file or folder names...
            foreach (string fileOrFolderToken in fileOrFolderTokens)
            {
                if (System.IO.File.Exists(fileOrFolderToken))
                {
                    // This is an individual file...
                    System.IO.FileInfo fi = new System.IO.FileInfo(fileOrFolderToken);
                    if (IsFileIncluded(fi) &&
                        !IsFileExcluded(fi))
                    {
                        int inventory_id = FindInventoryIDFromFileNameOrPath(fi.FullName);
                        // If the full filepath did not resolve to an inventoryID check the DragAndDrop dictionary
                        // for a matching entry...
                        if (inventory_id < 0 &&
                            _dragAndDropAttachmentFilesDictionary.ContainsKey(fi.FullName))
                        {
                            inventory_id = _dragAndDropAttachmentFilesDictionary[fi.FullName];
                        }
                        // Add the entry to the dictionary if it is not already there...
                        if (!attachmentDictionary.ContainsKey(fi.FullName))
                        {
                            attachmentDictionary.Add(fi.FullName, inventory_id);
                        }
                        else
                        {
                            // The inventory_id could not be found from the filename or path so don't add it...
                        }
                    }
                }
                else if (System.IO.Directory.Exists(fileOrFolderToken))
                {
                    // This is a folder which must be handled recursively because it can contain
                    // multiple files and subfolders...
                    System.IO.DirectoryInfo folderInfo = new System.IO.DirectoryInfo(fileOrFolderToken);
                    AddAttachmentCollection(attachmentDictionary, folderInfo);
                    // Now look for subfolders and gather up a collection of them also to recursively iterate through...
                    System.IO.DirectoryInfo[] subFolders = folderInfo.GetDirectories("*.*", System.IO.SearchOption.AllDirectories);
                    if (subFolders != null &&
                        subFolders.Length > 0)
                    {
                        // Process each subfolder in the collection...
                        foreach (System.IO.DirectoryInfo di in subFolders)
                        {
                            // Add all files in the subfolder to the dictionary...
                            AddAttachmentCollection(attachmentDictionary, di);
                        }
                    }
                }
            }

            
            //if (System.IO.Directory.Exists(fullFolderPathOrFileNameCollection))
            //{
            //    // This is a folder so consume everything inside of it - starting with  
            //    // adding all files in the root folder to the dictionary...
            //    System.IO.DirectoryInfo folderInfo = new System.IO.DirectoryInfo(fullFolderPathOrFileNameCollection);
            //    AddAttachmentCollection(attachmentDictionary, folderInfo);
            //    // Now look for subfolders and gather up a collection of them also to recursively iterate through...
            //    System.IO.DirectoryInfo[] subFolders = folderInfo.GetDirectories("*.*", System.IO.SearchOption.AllDirectories);
            //    if (subFolders != null &&
            //        subFolders.Length > 0)
            //    {
            //        // Process each subfolder in the collection...
            //        foreach (System.IO.DirectoryInfo di in subFolders)
            //        {
            //            // Add all files in the subfolder to the dictionary...
            //            AddAttachmentCollection(attachmentDictionary, di);
            //        }
            //    }
            //}
            //else
            //{
            //}

            return attachmentDictionary;
        }

        private void AddAttachmentCollection(Dictionary<string, int> attachmentDictionary, System.IO.DirectoryInfo folderInfo)
        {
            System.IO.FileInfo[] attachmentFiles = folderInfo.GetFiles("*.*", System.IO.SearchOption.TopDirectoryOnly);
//UpdateProgressBar(true, 0, attachmentFiles.Length, 0, "Inspecting files in folder '" + folderInfo.Name + "'");
            int i = 0;
            foreach (System.IO.FileInfo fi in attachmentFiles)
            {
                i++;
                if (IsFileIncluded(fi) &&
                    !IsFileExcluded(fi))
                {
int inventory_id = FindInventoryIDFromFileNameOrPath(fi.FullName);
//int inventory_id = FindInventoryID(fi.Name);
//if (inventory_id > 0 &&
//   !attachmentDictionary.ContainsKey(fi.FullName))
                    if (!attachmentDictionary.ContainsKey(fi.FullName))
                    {
                        attachmentDictionary.Add(fi.FullName, inventory_id);
                    }
                    else
                    {
                        //inventory_id = FindInventoryIDFromFullPath(fi.DirectoryName);
                    }
                    UpdateProgressBar(true, 0, attachmentFiles.Length, i, "Inspecting files in folder '" + folderInfo.Name + "'");
                }
            }
            UpdateProgressBar(false, 0, attachmentFiles.Length, i, "Done inspecting files in folder '" + folderInfo.Name + "'");
        }
// Versions of methods for resolving a filenamepath to the inventory_id...
/*
        private int FindInventoryID(string fileName)
        {
            string inventoryID = "";
            int returnID = -1;
            string[] fileNameTokens = fileName.Split(' ', '_');
            string inventoryName = "";

            // Build the inventory name string...
            foreach (string token in fileNameTokens)
            {
                // Keep adding tokens to the inventoryName until you find the form_type_code (which is a required field)...
                inventoryName += token;
                if (_formTypeCode.Contains(token)) break;
                inventoryName += " ";
            }
            // Attempt to find the inventory name in the inventory LUT...
            inventoryID = _sharedUtils.GetLookupValueMember(null, "inventory_lookup", inventoryName.Trim(), "", "Error!");
            if (!int.TryParse(inventoryID, out returnID))
            {
                // The inventory name doesn't exist in the inventory LUT so search the accession/inventory name table to see if a match can be found...
                DataSet ds = _sharedUtils.SearchWebService("@accession_inv_name.plant_name = '" + inventoryName.Trim() + "'", true, true, null, "inventory", 0, 0);
                if (ds != null &&
                    ds.Tables.Contains("SearchResult") &&
                    ds.Tables["SearchResult"].Rows.Count > 0)
                {
                    // A matching name was found in the accession/inventory names table so use the returned inventoryID...
                    inventoryID = ds.Tables["SearchResult"].Rows[0]["ID"].ToString();
                    if (!int.TryParse(inventoryID, out returnID))
                    {
                        returnID = -1;
                    }
                }
                else
                {
                    // No match was found in the accesion/inventory name table so give up and return -1...
                    returnID = -1;
                }
            }
            return returnID;
        }
*/
/*
        private int FindInventoryIDFromFileNameOrPath(string fileName)
        {
            //string inventoryID = "";
            int returnID = -1;
            //string[] fileNameTokens = fileName.Split(' ', '_');
            //string inventoryName = "";
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            string[] fileNameTokens = fi.Name.Split(new char[] {' ', '_'}, StringSplitOptions.RemoveEmptyEntries);

            // Working with the filename first...
            // Check for a known prefix in the file name...
            for (int i = 0; i < fileNameTokens.Length; i++)
            {
                if (_allowedInventoryPrefix.Contains(" " + fileNameTokens[i].Trim() + " "))
                {
                    // We found the prefix in the file name so now add the number to it (should be next token) and see if any inventory numbers in the LUT match...
                    string inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[Math.Min(i + 1, fileNameTokens.Length - 1)] + "%";
                    DataTable dt = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                    if (dt.Rows.Count > 0)
                    {
                        // Found some inventory LUT records matching the prefix + number so now add the suffix to attempt to narrow the search...
                        inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[Math.Min(i + 1, fileNameTokens.Length - 1)] + " " + fileNameTokens[Math.Min(i + 2, fileNameTokens.Length - 1)] + "%";
                        dt = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                        if (dt.Rows.Count > 0)
                        {
                            // Found some inventory LUT records matching the prefix + number + suffix so now add the form_type_code to attempt to narrow the search...
                            inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[Math.Min(i + 1, fileNameTokens.Length - 1)] + " " + fileNameTokens[Math.Min(i + 2, fileNameTokens.Length - 1)] + " " + fileNameTokens[Math.Min(i + 3, fileNameTokens.Length - 1)] + "%";
                            dt = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                            if (dt.Rows.Count == 1)
                            {
                                // Bingo - this is an inventory number matching prefix + number + suffix + form_type_code - so return the inventory_id...
                                if (!int.TryParse(dt.Rows[0]["value_member"].ToString(), out returnID))
                                {
                                    returnID = -1;
                                }
                                break;
                            }
                            else
                            {
                                // No matches for prefix + number + suffix + form_type_code - so this could be an accession number with a suffix 
                                // and because the filename cannot include the '**' the next token in the filename is possibly unrelated - so let's confirm that...
                                inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[Math.Min(i + 1, fileNameTokens.Length - 1)] + " " + fileNameTokens[Math.Min(i + 2, fileNameTokens.Length - 1)] + " **%";
                                dt = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                                if (dt.Rows.Count == 1)
                                {
                                    // Bingo - this is an accession number with a suffix - so return the '**' inventory_id...
                                    if (!int.TryParse(dt.Rows[0]["value_member"].ToString(), out returnID))
                                    {
                                        returnID = -1;
                                    }
                                    break;
                                }
                                else
                                {
                                    // Nothing in the inventory LUT matches the prefix + number + suffix + '**' - so search the accession/inventory name table to see if a match can be found...
                                    DataSet ds = _sharedUtils.SearchWebService("@accession_inv_name.plant_name = '" + inventoryNumberSearch.Trim() + "'", true, true, null, "inventory", 0, 0);
                                    if (ds != null &&
                                        ds.Tables.Contains("SearchResult") &&
                                        ds.Tables["SearchResult"].Rows.Count > 0)
                                    {
                                        // A matching name was found in the accession/inventory names table so use the returned inventoryID...
                                        if (!int.TryParse(ds.Tables["SearchResult"].Rows[0]["ID"].ToString(), out returnID))
                                        {
                                            returnID = -1;
                                        }
                                    }
                                    else
                                    {
                                        // No match was found in the accesion/inventory name table so give up and return -1...
                                        returnID = -1;
                                    }                              
                                }
                            }
                        }
                        else
                        {
                            // Couldn't find any inventory LUT records that matched the prefix + number + suffix which could mean
                            // that this is an accession number (because the filename cannot include the '**') - so let's confirm that...
                            inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[Math.Min(i + 1, fileNameTokens.Length - 1)] + " **%";
                            dt = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                            if (dt.Rows.Count == 1)
                            {
                                // Bingo - this is the accession number so return the '**' inventory_id...
                                if (!int.TryParse(dt.Rows[0]["value_member"].ToString(), out returnID))
                                {
                                    returnID = -1;
                                }
                                break;
                            }
                            else
                            {
                                // This isn't an accession number - so return -1...
                                returnID = -1;
                                break;
                            }
                        }
                    }
                    else
                    {
                        // Nothing in the inventory LUT matches the prefix + number - so search the accession/inventory name table to see if a match can be found...
                        DataSet ds = _sharedUtils.SearchWebService("@accession_inv_name.plant_name = '" + inventoryNumberSearch.Trim() + "'", true, true, null, "inventory", 0, 0);
                        if (ds != null &&
                            ds.Tables.Contains("SearchResult") &&
                            ds.Tables["SearchResult"].Rows.Count > 0)
                        {
                            // A matching name was found in the accession/inventory names table so use the returned inventoryID...
                            if (!int.TryParse(ds.Tables["SearchResult"].Rows[0]["ID"].ToString(), out returnID))
                            {
                                returnID = -1;
                            }
                        }
                        else
                        {
                            // No match was found in the accesion/inventory name table so give up and return -1...
                            returnID = -1;
                        }

                    }
                }
            }

            // If an inventory/accession number was not found in the filename itself - try to find a number in the path...
            if (returnID == -1 &&
                System.IO.File.Exists(fi.FullName))
            {
                // We need to change the file path delimeter ('\' OR '/') to the '_' so it can be processed like a filenamelook like a filename so replace the '/' and '\' with '_/
                string pathName = fi.DirectoryName.Replace('\\', '_').Replace('/', '_');
                returnID = FindInventoryIDFromFileNameOrPath(pathName);
            }
            //foreach (string token in fileNameTokens)
            //{
            //}
            //// Build the inventory name string...
            //foreach (string token in fileNameTokens)
            //{
            //    // Keep adding tokens to the inventoryName until you find the form_type_code (which is a required field)...
            //    inventoryName += token;
            //    if(_formTypeCode.Contains(token)) break;
            //    inventoryName += " ";
            //}
            //// Attempt to find the inventory name in the inventory LUT...
            //inventoryID = _sharedUtils.GetLookupValueMember(null, "inventory_lookup", inventoryName.Trim(), "", "Error!");
            //if (!int.TryParse(inventoryID, out returnID))
            //{
            //    // The inventory name doesn't exist in the inventory LUT so search the accession/inventory name table to see if a match can be found...
            //    DataSet ds = _sharedUtils.SearchWebService("@accession_inv_name.plant_name = '" + inventoryName.Trim() + "'", true, true, null, "inventory", 0, 0);
            //    if (ds != null &&
            //        ds.Tables.Contains("SearchResult") &&
            //        ds.Tables["SearchResult"].Rows.Count > 0)
            //    {
            //        // A matching name was found in the accession/inventory names table so use the returned inventoryID...
            //        inventoryID = ds.Tables["SearchResult"].Rows[0]["ID"].ToString();
            //        if (!int.TryParse(inventoryID, out returnID))
            //        {
            //            returnID = -1;
            //        }
            //    }
            //    else
            //    {
            //        // No match was found in the accesion/inventory name table so give up and return -1...
            //        returnID = -1;
            //    }
            //}
            return returnID;
        }
*/
        private int FindInventoryIDFromFileNameOrPath(string fileName)
        {
            int returnID = -1;
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            string[] fileNameTokens = fi.Name.TrimEnd(fi.Extension.ToCharArray()).Split(new char[] { ' ', '_' }, StringSplitOptions.RemoveEmptyEntries);
//string[] fileNamePathTokens = fi.DirectoryName.Split(new char[] { '\\', ' ', '_' }, StringSplitOptions.RemoveEmptyEntries);

            // Look for a known prefix + number combination (requires 2 adjacent tokens somewhere in the filename)...
            for (int i = 0; ((i < fileNameTokens.Length - 1) && (returnID < 0)); i++)
            {
                if (IsPrefix(" " + fileNameTokens[i].Trim() + " "))
                {
                    // We found a token in the filename that matches an allowed inventory prefix - let the fun begin...
                    // so in theory the next token *must be* an integer...
                    if (IsInteger(" " + fileNameTokens[i + 1].Trim() + " "))
                    {
                        // Found an integer - so add that to the prefix and search the LUT for a match...
                        string inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[i + 1] + "%";
                        DataTable dtPart12 = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                        if (dtPart12.Rows.Count == 0)
                        {
                            // Nothing in the inventory LUT matches this filename token so move on to the next token and keep looking...
                            continue;
                        }
                        else if (dtPart12.Rows.Count == 1)
                        {
                            // The only match in the inventory LUT is prefix + number (most likely because it is an accessin number) so return that...
                            returnID = (int)dtPart12.Rows[0]["value_member"];
                            break;
                        }
                        else if (dtPart12.Rows.Count > 1)
                        {
                            // More than one matching inventory lotcode so see if we can add one
                            // or more tokens to find a single match...
                            if (i < fileNameTokens.Length - 2)
                            {
                                inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[i + 1] + " " + fileNameTokens[i + 2] + "%";
                                DataTable dtPart123 = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                                if (dtPart123.Rows.Count == 0)
                                {
                                    // The follow-on token does not match any inventory lotcodes so this must be the accession number 
                                    // to be sure we will go find the '**' inventory_id and make sure we get one match...
                                    inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[i + 1] + " **%";
                                    dtPart123 = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                                    if (dtPart123.Rows.Count == 1)
                                    {
                                        // The only match in the inventory LUT is prefix + number + '**' (aka the 'system inventory' for the accession) so return that...
                                        returnID = (int)dtPart123.Rows[0]["value_member"];
                                        break;
                                    }
                                    else
                                    {
                                        // Nothing in the inventory LUT matches this filename token so move on to the next token and keep looking...
                                        continue;
                                    }
                                }
                                else if (dtPart123.Rows.Count == 1)
                                {
                                    // The only match in the inventory LUT is prefix + number + (either suffix or form_type_code) so return that...
                                    returnID = (int)dtPart123.Rows[0]["value_member"];
                                    break;
                                }
                                else if (dtPart123.Rows.Count > 1)
                                {
                                    if (i < fileNameTokens.Length - 3)
                                    {
                                        // Still more than one matching inventory lotcode so see if we can add the form_type_code 
                                        // in order to narrow it down to a single match...
                                        inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[i + 1] + " " + fileNameTokens[i + 2] + " " + fileNameTokens[i + 3];
                                        DataTable dtPart123Form = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                                        if (dtPart123Form.Rows.Count == 1)
                                        {
                                            // The only match in the inventory LUT is prefix + number + '**' (aka the 'system inventory' for the accession) so return that...
                                            returnID = (int)dtPart123Form.Rows[0]["value_member"];
                                            break;
                                        }
                                        else
                                        {
                                            // Nothing in the inventory LUT matches this filename token so move on to the next token and keep looking...
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        // We are out of tokens to try so this must be the accession number 
                                        // to be sure we will go find the '**' inventory_id and make sure we get one match...
                                        inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[i + 1] + " " + fileNameTokens[i + 2] + " **";
                                        DataTable dtPart123SystemForm = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                                        if (dtPart123SystemForm.Rows.Count == 1)
                                        {
                                            // The only match in the inventory LUT is prefix + number + '**' (aka the 'system inventory' for the accession) so return that...
                                            returnID = (int)dtPart123SystemForm.Rows[0]["value_member"];
                                            break;
                                        }
                                        else
                                        {
                                            // Nothing in the inventory LUT matches this filename token so move on to the next token and keep looking...
                                            continue;
                                        }
                                    }
                                }
                            }
                            else if (i < fileNameTokens.Length - 1)
                            {
                                // The follow-on token does not match any inventory lotcodes so this must be the accession number 
                                // to be sure we will go find the '**' inventory_id and make sure we get one match...
                                inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[i + 1] + " **%";
                                DataTable dtPart123 = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                                if (dtPart123.Rows.Count == 1)
                                {
                                    // The only match in the inventory LUT is prefix + number + '**' (aka the 'system inventory' for the accession) so return that...
                                    returnID = (int)dtPart123.Rows[0]["value_member"];
                                    break;
                                }
                                else
                                {
                                    // Nothing in the inventory LUT matches this filename token so move on to the next token and keep looking...
                                    continue;
                                }
                            }
                            else
                            {
                            }
                        }
                    }
                    else
                    {
                    }
                }

//if (IsInteger(""))
//{
//    // We found the prefix in the file name so now add the number to it (should be next token) and see if any inventory numbers in the LUT match...
//    string inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[Math.Min(i + 1, fileNameTokens.Length - 1)] + "%";
//    DataTable dt = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
//    if (dt.Rows.Count > 0)
//    {
//        // Found some inventory LUT records matching the prefix + number so now add the suffix to attempt to narrow the search...
//        inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[Math.Min(i + 1, fileNameTokens.Length - 1)] + " " + fileNameTokens[Math.Min(i + 2, fileNameTokens.Length - 1)] + "%";
//        dt = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
//        if (dt.Rows.Count > 0)
//        {
//            // Found some inventory LUT records matching the prefix + number + suffix so now add the form_type_code to attempt to narrow the search...
//            inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[Math.Min(i + 1, fileNameTokens.Length - 1)] + " " + fileNameTokens[Math.Min(i + 2, fileNameTokens.Length - 1)] + " " + fileNameTokens[Math.Min(i + 3, fileNameTokens.Length - 1)] + "%";
//            dt = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
//            if (dt.Rows.Count == 1)
//            {
//                // Bingo - this is an inventory number matching prefix + number + suffix + form_type_code - so return the inventory_id...
//                if (!int.TryParse(dt.Rows[0]["value_member"].ToString(), out returnID))
//                {
//                    returnID = -1;
//                }
//                break;
//            }
//            else
//            {
//                // No matches for prefix + number + suffix + form_type_code - so this could be an accession number with a suffix 
//                // and because the filename cannot include the '**' the next token in the filename is possibly unrelated - so let's confirm that...
//                inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[Math.Min(i + 1, fileNameTokens.Length - 1)] + " " + fileNameTokens[Math.Min(i + 2, fileNameTokens.Length - 1)] + " **%";
//                dt = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
//                if (dt.Rows.Count == 1)
//                {
//                    // Bingo - this is an accession number with a suffix - so return the '**' inventory_id...
//                    if (!int.TryParse(dt.Rows[0]["value_member"].ToString(), out returnID))
//                    {
//                        returnID = -1;
//                    }
//                    break;
//                }
//                else
//                {
//                    // Nothing in the inventory LUT matches the prefix + number + suffix + '**' - so search the accession/inventory name table to see if a match can be found...
//                    DataSet ds = _sharedUtils.SearchWebService("@accession_inv_name.plant_name = '" + inventoryNumberSearch.Trim() + "'", true, true, null, "inventory", 0, 0);
//                    if (ds != null &&
//                        ds.Tables.Contains("SearchResult") &&
//                        ds.Tables["SearchResult"].Rows.Count > 0)
//                    {
//                        // A matching name was found in the accession/inventory names table so use the returned inventoryID...
//                        if (!int.TryParse(ds.Tables["SearchResult"].Rows[0]["ID"].ToString(), out returnID))
//                        {
//                            returnID = -1;
//                        }
//                    }
//                    else
//                    {
//                        // No match was found in the accesion/inventory name table so give up and return -1...
//                        returnID = -1;
//                    }
//                }
//            }
//        }
//        else
//        {
//            // Couldn't find any inventory LUT records that matched the prefix + number + suffix which could mean
//            // that this is an accession number (because the filename cannot include the '**') - so let's confirm that...
//            inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[Math.Min(i + 1, fileNameTokens.Length - 1)] + " **%";
//            dt = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
//            if (dt.Rows.Count == 1)
//            {
//                // Bingo - this is the accession number so return the '**' inventory_id...
//                if (!int.TryParse(dt.Rows[0]["value_member"].ToString(), out returnID))
//                {
//                    returnID = -1;
//                }
//                break;
//            }
//            else
//            {
//                // This isn't an accession number - so return -1...
//                returnID = -1;
//                break;
//            }
//        }
//    }
//    else
//    {
//        // Nothing in the inventory LUT matches the prefix + number - so search the accession/inventory name table to see if a match can be found...
//        DataSet ds = _sharedUtils.SearchWebService("@accession_inv_name.plant_name = '" + inventoryNumberSearch.Trim() + "'", true, true, null, "inventory", 0, 0);
//        if (ds != null &&
//            ds.Tables.Contains("SearchResult") &&
//            ds.Tables["SearchResult"].Rows.Count > 0)
//        {
//            // A matching name was found in the accession/inventory names table so use the returned inventoryID...
//            if (!int.TryParse(ds.Tables["SearchResult"].Rows[0]["ID"].ToString(), out returnID))
//            {
//                returnID = -1;
//            }
//        }
//        else
//        {
//            // No match was found in the accesion/inventory name table so give up and return -1...
//            returnID = -1;
//        }

//    }
//}
            }

            // If an inventory/accession number was not found in the filename itself - try to find a number in the path...
//if (returnID == -1 &&
//    System.IO.File.Exists(fi.FullName))
            if (returnID == -1 &&
                !string.IsNullOrEmpty(fi.DirectoryName))
            {
                // We need to change the file path delimeter ('\' OR '/') to the '_' so it can be processed like a filenamelook like a filename so replace the '/' and '\' with '_/
                string pathName = fi.DirectoryName.Replace('\\', '_').Replace('/', '_');
//                returnID = FindInventoryIDFromFileNameOrPath(pathName);
                returnID = FindInventoryIDFromFileNameOrPath(fi.DirectoryName);
            }

            return returnID;
        }

        private DateTime FindDateFromFileNameOrPath(string fileName)
        {
            bool foundDate = false;
            DateTime returnDate = DateTime.Now.Date;
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            string[] fileNameTokens = fi.Name.TrimEnd(fi.Extension.ToCharArray()).Split(new char[] { ' ', '_' }, StringSplitOptions.RemoveEmptyEntries);
            // First try to find the date in the filename itself...
            foreach (string token in fileNameTokens)
            {
                if (DateTime.TryParse(token, out returnDate))
                {
                    foundDate = true;
                    break;
                }
            }

            // If the filename doesn't contain a valid date look in the path of the file for a date...
            if (!foundDate &&
                !string.IsNullOrEmpty(fi.DirectoryName))
            {
                returnDate = FindDateFromFileNameOrPath(fi.DirectoryName);
            }

            return returnDate;
        }

        private string FindDescriptionCodeFromFileNameOrPath(string fileName)
        {
            bool foundContentType = false;
            string returnContentType = "";
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            string[] fileNameTokens = fi.Name.TrimEnd(fi.Extension.ToCharArray()).Split(new char[] { ' ', '_' }, StringSplitOptions.RemoveEmptyEntries);
            // First try to find the description code in the filename itself...
            foreach (string token in fileNameTokens)
            {
                if (IsDescriptionCode(token))
                {
                    returnContentType = token;
                    foundContentType = true;
                    break;
                }
            }

            // If the filename doesn't contain a valid description code look in the path of the file for a date...
            if (!foundContentType &&
                !string.IsNullOrEmpty(fi.DirectoryName))
            {
                returnContentType = FindDescriptionCodeFromFileNameOrPath(fi.DirectoryName);
            }

            return returnContentType;
        }


        private string FindContentTypeFromFileNameOrPath(string fileName)
        {
            string returnContentType = System.Web.MimeMapping.GetMimeMapping(fileName);
            return returnContentType;
        }

        private string FindCategoryCodeFromFileName(string fileName)
        {
            string returnCategoryCode = "";
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            string mimeType = System.Web.MimeMapping.GetMimeMapping(fileName);
            //if (mimeType.ToUpper().Contains("IMAGE")) if (_supportedImageExtensions.Contains(fi.Extension))
            // First try to detect if this is an image from the .NET mime mapping...
            if (mimeType.ToUpper().Contains("IMAGE"))
            {
                returnCategoryCode = "IMAGE";
            }
            // Next try to detect if this is an image from the file extension...
            else if (_supportedImageExtensions.Contains(fi.Extension))
            {
                returnCategoryCode = "IMAGE";
            }
            else
            // Couldn't detect that the file was an image so fallback to a default of documewnt type...
            {
                returnCategoryCode = "DOCUMENT";
            }

            return returnCategoryCode;
        }



        private bool IsPrefix(string stringToken)
        {
            string token = stringToken;
            string allowedPrefix = _allowedInventoryPrefix;
            if (!_useCaseSensitiveFileNames)
            {
                token = stringToken.ToLower();
                allowedPrefix = _allowedInventoryPrefix.ToLower();
            }

            return allowedPrefix.Contains(" " + token.Trim() + " ");
        }

        private bool IsInteger(string stringToken)
        {
            int intResult = 0;

            return int.TryParse(stringToken.Trim(), out intResult);
        }

        private bool IsFormTypeCode(string stringToken)
        {
            string token = stringToken;
            string allowedFormTypeCode = _allowedFormTypeCode;
            if (!_useCaseSensitiveFileNames)
            {
                token = stringToken.ToLower();
                allowedFormTypeCode = _allowedFormTypeCode.ToLower();
            }

            return allowedFormTypeCode.Contains(" " + token.Trim() + " ");
        }

        private bool IsDescriptionCode(string stringToken)
        {
            string token = stringToken;
            string allowedDescriptionCode = _allowedDescriptionCode;
            if (!_useCaseSensitiveFileNames)
            {
                token = stringToken.ToLower();
                allowedDescriptionCode = _allowedDescriptionCode.ToLower();
            }

            return allowedDescriptionCode.Contains(" " + token.Trim() + " ");
        }

        private bool IsFileIncluded(System.IO.FileInfo fi)
        {
            bool included = false;
            string[] filters = _includeFileFilter.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string filter in filters)
            {
                string pattern = "^" + filter.Trim().Replace(".", "\\.").Replace("*", ".*") + "$";
                System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                if (r.IsMatch(fi.Name))
                {
                    included = true;
                    break;
                }
            }

            return included;
        }

        private bool IsFileExcluded(System.IO.FileInfo fi)
        {
            bool excluded = false;
            string[] filters = _excludedFileFilter.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string filter in filters)
            {
                string pattern = "^" + filter.Trim().Replace(".", "\\.").Replace("*", ".*") + "$";
                System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                if (r.IsMatch(fi.Name))
                {
                    excluded = true;
                    break;
                }
            }

            return excluded;
        }

/* Original RefreshData()

        private void RefreshData()
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            string paramList = ":inventoryid=";

            // Don't reload the remote data unless something has changed in the list of attachments being worked on...
            if (_previousInventoryIDs != _currentInventoryIDs ||
                _previousFolderPathOrFileNames != _currentFolderPathOrFileNames)
            {
                if (_currentInventoryIDs.StartsWith(":inventoryid="))
                {
                    // Current attachment filter is a list of inventory IDs so retrieve all attachments on the 
                    // server associated with this list of inventory_ids...
                    paramList = _currentInventoryIDs;
                }

                if (_previousFolderPathOrFileNames != _currentFolderPathOrFileNames ||
                    paramList == _currentInventoryIDs)
                {
                    // This is a list of files so process each fullpath file name...
                    // Create a ditionary by inspecting each file in the file collection or folder 
                    // and using the file name to retrieve the inventory_id PKEY...
                    _attachmentDictionary = LoadAttachmentDictionary(_currentFolderPathOrFileNames);

                    // Get the remote servers filepath for each inventory_id attachment to be saved....
                    // First build the inventory_id parameter list...
                    foreach (int inventory_id in _attachmentDictionary.Values)
                    {
                        if (inventory_id > 0)
                        {
                            // Update the parameter list of inventory IDs that will be used to retrieve exisiting attachment records...
                            paramList += inventory_id.ToString() + ",";
                        }
                    }
                    paramList = paramList.TrimEnd(',');
                }

                // Get any pre-existing attachment records for the inventory_ids in the dictionary list...
                _dsInventoryAttach = _sharedUtils.GetWebServiceData("get_accession_inv_attach", paramList, 0, 0);

                // If data was retrieved build a new interface...
                if (_dsInventoryAttach != null &&
                    _dsInventoryAttach.Tables.Contains("get_accession_inv_attach"))
                {
                    _inventoryAttach = _dsInventoryAttach.Tables["get_accession_inv_attach"];
                    _inventoryAttachBindingSource.DataSource = _inventoryAttach;

                    // Create the edit mode datagridview...
                    _sharedUtils.BuildEditDataGridView(ux_datagridviewAttachments, _inventoryAttach);

//// Wireup the event handlers for Foreign Key columns...
//ux_datagridviewAttachments.CellFormatting += new DataGridViewCellFormattingEventHandler(ux_datagridview_CellFormatting);
//ux_datagridviewAttachments.DataError += new DataGridViewDataErrorEventHandler(ux_datagridview_DataError);
//ux_datagridviewAttachments.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(ux_datagridview_EditingControlShowing);
//ux_datagridviewAttachments.PreviewKeyDown += new PreviewKeyDownEventHandler(ux_datagridview_PreviewKeyDown);
                }

                // Call the dataview to calculate the filepath of the attachments in the dictionary list...
                DataSet dsFilePath = _sharedUtils.GetWebServiceData("inventory_attach_wizard_get_filepath", paramList, 0, 0);

                // Process each of the entries in the dictionary list that has a valid inventory_id...
                if (_inventoryAttach != null &&
                    dsFilePath != null &&
                    dsFilePath.Tables.Contains("inventory_attach_wizard_get_filepath") &&
                    dsFilePath.Tables["inventory_attach_wizard_get_filepath"].Rows.Count > 0)
                {
                    DataTable inventoryAttachFilePath = dsFilePath.Tables["inventory_attach_wizard_get_filepath"];
                    if (_inventoryAttach != null)
                    {
                        int i = 0;
                        // Add the new records to the table...
                        foreach (string localFilePath in _attachmentDictionary.Keys)
                        {
                            i++;
                            UpdateProgressBar(true, 0, _attachmentDictionary.Count, i, "Processing database records...");
                            // Get the inventory_id...
                            int inventoryID = _attachmentDictionary[localFilePath];
                            // Get the filepath for this inventory_id from the remote dataview...
                            System.IO.FileInfo fi = new System.IO.FileInfo(localFilePath);
                            string fileName = fi.Name;
                            DataRow remoteFilePath = inventoryAttachFilePath.Rows.Find(inventoryID);
                            DataRow[] drs = _inventoryAttach.Select("inventory_id = " + inventoryID.ToString() + " AND virtual_path LIKE '%" + fileName + "%'");
                            if (drs.Length > 0)
                            {
                                // There is an existing accession_inv_attach record associated with this
                                // inventory_id linked to a file using this filename - so do not create a new
                                // inventoryAttach record but instead let the user update the existing record...
                                string fullFileName = remoteFilePath["attach_filepath"].ToString() + fileName;
                                drs[0]["virtual_path"] = fullFileName;
                                drs[0]["thumbnail_virtual_path"] = fullFileName.Insert(fullFileName.LastIndexOf('.'), "_thumbnail");
                            }
                            else
//else if (inventoryID > 0 &&
//    remoteFilePath != null)
                            {
                                string fullFileName = fi.FullName;
                                if (remoteFilePath != null) fullFileName = remoteFilePath["attach_filepath"].ToString() + fileName;
                                // Add a new inventoryAttach record for this valid inventory_id...
                                DataRow newInventoryAttachment = _inventoryAttach.NewRow();
                                newInventoryAttachment["inventory_id"] = inventoryID;
                                newInventoryAttachment["virtual_path"] = fullFileName;
                                newInventoryAttachment["thumbnail_virtual_path"] = fullFileName.Insert(fullFileName.LastIndexOf('.'), "_thumbnail");
                                //newInventoryAttachment["sort_order"] = 0;
                                //newInventoryAttachment["title"] = 0;
                                //newInventoryAttachment["description"] = 0;
                                //newInventoryAttachment["content_type"] = "IMAGE";
                                newInventoryAttachment["category_code"] = "IMAGE";
                                //newInventoryAttachment["copyright_information"] = "";
                                newInventoryAttachment["attach_cooperator_id"] = _sharedUtils.UserCooperatorID;
                                newInventoryAttachment["is_web_visible"] = "Y";
                                newInventoryAttachment["attach_date"] = DateTime.Now.Date;
                                newInventoryAttachment["attach_date_code"] = "MM/dd/yyyy";
                                //newInventoryAttachment["note"] = "";
                                //newInventoryAttachment["image_file_physical_path"] = physicalPath;
                                _inventoryAttach.Rows.Add(newInventoryAttachment);
                            }
                        }
                    }
                }
                UpdateProgressBar(false, 0, _attachmentDictionary.Count, 0, "Done processing database records...");

                // Create an imagelist to be used by the imagelistview control...
                _imageList = LoadImageList(_attachmentDictionary, _inventoryAttach);
                UpdateProgressBar(false, 0, _attachmentDictionary.Count, 0, "Done processing Attachment Files...");

                // Create a treeview of the files being attached to inventory...
                BuildTreeView();

                // Remember the path of the currently loaded attachments...
                _previousFolderPathOrFileNames = _currentFolderPathOrFileNames;
                // Remember the list of inventory IDs being used...
                _previousInventoryIDs = _currentInventoryIDs;
            }

            // Bind the inventoryAttach datatable to the binding source...        
            _inventoryAttachBindingSource.DataSource = _inventoryAttach;

            // Update the listview....
            BuildListView();

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }
*/

        private void RefreshData()
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            string validInventoryIDs = ":inventoryid=";

            // Don't reload the remote data unless something has changed in the list of attachments being worked on...
            if (_previousInventoryIDs != _currentInventoryIDs ||
                _previousFolderPathOrFileNames != _currentFolderPathOrFileNames)
            {
                if (_currentInventoryIDs.StartsWith(":inventoryid="))
                {
                    // Current attachment filter is a list of inventory IDs so retrieve all attachments on the 
                    // server associated with this list of inventory_ids...
                    validInventoryIDs = _currentInventoryIDs + ",";
                }

                if (_previousFolderPathOrFileNames != _currentFolderPathOrFileNames ||
                    validInventoryIDs. Contains(_currentInventoryIDs))
                {
                    // This is a list of files so process each fullpath file name...
                    // Create a ditionary by inspecting each file in the file collection or folder 
                    // and using the file name to retrieve the inventory_id PKEY...
                    _attachmentFilesDictionary = LoadAttachmentDictionary(_currentFolderPathOrFileNames);

                    // Get the remote servers filepath for each inventory_id attachment to be saved....
                    // First build the inventory_id parameter list...
                    foreach (int inventory_id in _attachmentFilesDictionary.Values)
                    {
                        if (inventory_id > 0 &&
                            !validInventoryIDs.Contains(inventory_id.ToString()))
                        {
                            // Update the parameter list of inventory IDs that will be used to retrieve exisiting attachment records...
                            validInventoryIDs += inventory_id.ToString() + ",";
                        }
                    }
                }

                validInventoryIDs = validInventoryIDs.TrimEnd(',');

                // Get any pre-existing attachment records for the inventory_ids in the dictionary list...
                _dsInventoryAttach = _sharedUtils.GetWebServiceData("get_accession_inv_attach", validInventoryIDs, 0, 0);

                // If data was retrieved build a new interface...
                if (_dsInventoryAttach != null &&
                    _dsInventoryAttach.Tables.Contains("get_accession_inv_attach"))
                {
                    _inventoryAttach = _dsInventoryAttach.Tables["get_accession_inv_attach"];
                    _inventoryAttachBindingSource.DataSource = _inventoryAttach;

                    // Create the edit mode datagridview...
                    _sharedUtils.BuildEditDataGridView(ux_datagridviewAttachments, _inventoryAttach);
                }

                // Call the dataview to calculate the filepath of the attachments in the dictionary list...
                DataSet dsFilePath = _sharedUtils.GetWebServiceData("inventory_attach_wizard_get_filepath", validInventoryIDs, 0, 0);

                // Process each of the entries in the dictionary list that has a valid inventory_id...
                if (_inventoryAttach != null &&
                    dsFilePath != null &&
                    dsFilePath.Tables.Contains("inventory_attach_wizard_get_filepath") &&
                    dsFilePath.Tables["inventory_attach_wizard_get_filepath"].Rows.Count >= 0)
                {
                    DataTable inventoryAttachFilePath = dsFilePath.Tables["inventory_attach_wizard_get_filepath"];
                    if (_inventoryAttach != null)
                    {
                        int i = 0;
                        // Add the new records to the table...
                        foreach (string localFilePath in _attachmentFilesDictionary.Keys)
                        {
                            i++;
                            UpdateProgressBar(true, 0, _attachmentFilesDictionary.Count, i, "Processing database records...");
                            // Get the inventory_id...
                            int inventoryID = _attachmentFilesDictionary[localFilePath];
                            // Get the filepath for this inventory_id from the remote dataview...
                            System.IO.FileInfo fi = new System.IO.FileInfo(localFilePath);
                            string fileName = fi.Name;
                            DateTime imageDate = FindDateFromFileNameOrPath(fi.FullName);
                            string attachmentDescriptionCode = FindDescriptionCodeFromFileNameOrPath(fi.FullName);
                            string attachmentContentType = FindContentTypeFromFileNameOrPath(fi.FullName);
                            string attachmentCategoryCode = FindCategoryCodeFromFileName(fi.FullName);
                            DataRow remoteFilePath = inventoryAttachFilePath.Rows.Find(inventoryID);
                            DataRow[] drs = _inventoryAttach.Select("inventory_id = " + inventoryID.ToString() + " AND virtual_path LIKE '%" + fileName + "%'");
                            if (drs.Length > 0)
                            {
                                // There is an existing accession_inv_attach record associated with this
                                // inventory_id linked to a file using this filename - so do not create a new
                                // inventoryAttach record but instead let the user update the existing record...
                                string fullFileName = remoteFilePath["attach_filepath"].ToString() + fileName;
                                drs[0]["virtual_path"] = fullFileName;
                                drs[0]["thumbnail_virtual_path"] = fullFileName.Insert(fullFileName.LastIndexOf('.'), "_thumbnail");
                            }
                            else
                            {
                                string fullFileName = fi.FullName;
                                if (remoteFilePath != null) fullFileName = remoteFilePath["attach_filepath"].ToString() + fileName;
                                // Add a new inventoryAttach record for this valid inventory_id...
                                DataRow newInventoryAttachment = _inventoryAttach.NewRow();
                                newInventoryAttachment["inventory_id"] = inventoryID;
                                newInventoryAttachment["virtual_path"] = fullFileName;
                                newInventoryAttachment["thumbnail_virtual_path"] = fullFileName.Insert(fullFileName.LastIndexOf('.'), "_thumbnail");
                                //newInventoryAttachment["sort_order"] = 0;
                                //newInventoryAttachment["title"] = "";
                                //newInventoryAttachment["description"] = "";
                                newInventoryAttachment["description_code"] = attachmentDescriptionCode;
                                newInventoryAttachment["content_type"] = attachmentContentType;
                                newInventoryAttachment["category_code"] = attachmentCategoryCode;
                                //newInventoryAttachment["copyright_information"] = "";
                                newInventoryAttachment["attach_cooperator_id"] = _sharedUtils.UserCooperatorID;
                                newInventoryAttachment["is_web_visible"] = "Y";
                                newInventoryAttachment["attach_date"] = imageDate;
                                newInventoryAttachment["attach_date_code"] = "MM/dd/yyyy";
                                //newInventoryAttachment["note"] = "";
                                _inventoryAttach.Rows.Add(newInventoryAttachment);
                            }
                        }
                    }
                }
                UpdateProgressBar(false, 0, _attachmentFilesDictionary.Count, 0, "Done processing database records...");

                // Create an imagelist to be used by the imagelistview control...
                string currentRowFilter = _inventoryAttach.DefaultView.RowFilter;
                if (!ux_checkboxViewExistingAttachments.Checked)
                {
                    if (string.IsNullOrEmpty(currentRowFilter))
                    {
                        _inventoryAttach.DefaultView.RowFilter = "accession_inv_attach_id < 0";
                    }
                    else if (!currentRowFilter.Contains("accession_inv_attach_id < 0"))
                    {
                        _inventoryAttach.DefaultView.RowFilter = currentRowFilter + " AND accession_inv_attach_id < 0";
                    }
                }
                _imageList = LoadImageList(_attachmentFilesDictionary, _inventoryAttach.DefaultView.ToTable());
                UpdateProgressBar(false, 0, _attachmentFilesDictionary.Count, 0, "Done processing Attachment Files...");

                // Create a treeview of the files being attached to inventory...
                BuildTreeView();

                // Remember the path of the currently loaded attachments...
                _previousFolderPathOrFileNames = _currentFolderPathOrFileNames;
                // Remember the list of inventory IDs being used...
                _previousInventoryIDs = _currentInventoryIDs;
            }

            // Bind the inventoryAttach datatable to the binding source...        
            _inventoryAttachBindingSource.DataSource = _inventoryAttach;

            //// Create a treeview of the files being attached to inventory...
            //BuildTreeView();

            // Update the listview....
            BuildListView();

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void BuildListView()
        {
            if (_imageList != null &&
                _inventoryAttach != null)
            {
                ux_listviewAttachments.LargeImageList = _imageList;
                ux_listviewAttachments.View = View.Tile;
                ux_listviewAttachments.TileSize = new Size(ux_listviewAttachments.Size.Width, _imageList.ImageSize.Height + 4);

                ux_listviewAttachments.Items.Clear();
                ux_listviewAttachments.Groups.Clear();
                ux_listviewAttachments.Columns.Clear();
                ux_listviewAttachments.Columns.Add("Inventory Number");
                ux_listviewAttachments.Columns.Add("Attachment Location");

                foreach (DataRowView drv in _inventoryAttach.DefaultView)
                {
                    string inventoryNumber = _sharedUtils.GetLookupDisplayMember("inventory_lookup", drv["inventory_id"].ToString(), "", drv["inventory_id"].ToString());
                    if (ux_listviewAttachments.Groups[inventoryNumber] == null)
                    {
                        ux_listviewAttachments.Groups.Add(inventoryNumber, inventoryNumber);
                    }
                    ListViewItem newLVI = new ListViewItem(inventoryNumber, drv["accession_inv_attach_id"].ToString(), ux_listviewAttachments.Groups[inventoryNumber]);
                    newLVI.SubItems.Add(drv["virtual_path"].ToString());
                    ux_listviewAttachments.Items.Add(newLVI);
                }
            }
        }

        private ImageList LoadImageList(Dictionary<string, int> attachmentDictionary, DataTable inventoryAttach)
        {
            ImageList returnImageList = new ImageList();
            returnImageList.ImageSize = new Size(128, 128);
            returnImageList.ColorDepth = ColorDepth.Depth32Bit;

            // Load the images into an imagelist cache to be used by the imagelistview control...
            int i = 0;
            //foreach (string localFilePath in attachmentDictionary.Keys)
            foreach (DataRow dr in inventoryAttach.Rows)
            {
                string remoteServerFilePath = dr["virtual_path"].ToString();
                // Default image path should be the remote server's thumbnail image...
                string attachmentPath = dr["thumbnail_virtual_path"].ToString();
                // Thumbnail field is nullable so we need to make sure it has a value...
                if (string.IsNullOrEmpty(attachmentPath)) attachmentPath = remoteServerFilePath;
                // If the virtual_path is not an absolute URI the attachment record is new (and thus 
                // no remote image exists or a newer local copy of the image file is being used to
                // update the existing record - the only way to know is to check the attachmentDictionary for a matching
                // entry and if one is found use it instead of attempting to retrieve a remote image...
                System.IO.FileInfo fi;
                string escapedURI = Uri.EscapeUriString(attachmentPath);
                Uri tempURI;
                if (Uri.IsWellFormedUriString(escapedURI, UriKind.Absolute))
                {
                    // Create a fileinfo object using the remote server's virtual_path...
                    tempURI = new Uri(attachmentPath);
                    fi = new System.IO.FileInfo(tempURI.AbsolutePath);
                }
                else
                {
                    if(Uri.TryCreate(escapedURI, UriKind.Absolute, out tempURI))
                    {
                        tempURI = new Uri(attachmentPath);
                        fi = new System.IO.FileInfo(tempURI.AbsolutePath);
                    }
                    else
                    {
                        fi = new System.IO.FileInfo(attachmentPath);
                    }
                }
                // Get the filename from the server virtual_path...
                string fileName = fi.Name;
                // Get the inventory_id...
                string inventoryID = dr["inventory_id"].ToString();
                // Now iterate through the dictionary looking for a key that matches both 
                // the filename and the parent foldername (aka the inventory_id) which 
                // indicates a local copy of the image is available - so we should use that instead...
                foreach (string key in attachmentDictionary.Keys)
                {
                    if (key.Contains(fileName) &&
                        attachmentDictionary[key].ToString().Trim() == inventoryID.Trim())
                    {
                        attachmentPath = key;
                        break;
                    }
                }
                // Load either the remote thumbnail image of the attachment,
                // a local copy of the image,
                // or an icon image (based on the file extension)...
                if (attachmentPath.Length > 0)
                {
                    if (dr["category_code"].ToString().ToUpper() != "LINK" &&
                        Uri.IsWellFormedUriString(attachmentPath, UriKind.Absolute) &&
                        !string.IsNullOrEmpty(fi.Extension) &&
                        _supportedImageExtensions.Contains(fi.Extension.ToLower()))
                    {
                        byte[] attachBytes = _sharedUtils.GetAttachment(attachmentPath);
                        if (attachBytes != null && attachBytes.Length > 0)
                        {
                            System.IO.MemoryStream ms = new System.IO.MemoryStream(attachBytes);
                            Image fullImage = Image.FromStream(ms, false, true);
                            Image thumbnailImage = fullImage.GetThumbnailImage(Math.Min(128, fullImage.Width), Math.Min(128, fullImage.Height), null, IntPtr.Zero);
                            //Image thumbnailImage = fullImage.GetThumbnailImage(fullImage.Width / 10, fullImage.Height / 10, null, IntPtr.Zero);
                            //Image thumbnailImage = fullImage.GetThumbnailImage(128, 128, null, IntPtr.Zero);
                            fullImage.Dispose();
                            returnImageList.Images.Add(dr["accession_inv_attach_id"].ToString(), thumbnailImage);
                        }
                        else
                        {
                            // Thumbnail was not retrieved - use a file-type specific icon instead...
                            string[] matchingFileExtImages = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + @"\Images\AttachmentWizard\", "file_extension_" + fi.Extension.TrimStart('.') + ".*", System.IO.SearchOption.TopDirectoryOnly);
                            if (matchingFileExtImages.Length > 0)
                            {
                                Image fullImage = Image.FromFile(matchingFileExtImages[0]);
                                Image thumbnailImage = fullImage.GetThumbnailImage(fullImage.Width, fullImage.Height, null, IntPtr.Zero);
                                fullImage.Dispose();
                                returnImageList.Images.Add(dr["accession_inv_attach_id"].ToString(), thumbnailImage);
                            }
                        }
                    }
                    else if (System.IO.File.Exists(attachmentPath) &&
                        !string.IsNullOrEmpty(fi.Extension) &&
                        _supportedImageExtensions.Contains(fi.Extension.ToLower()))
                    {
                        Image fullImage = Image.FromFile(attachmentPath);
                        Image thumbnailImage = fullImage.GetThumbnailImage(128, 128, null, IntPtr.Zero);
                        fullImage.Dispose();
                        returnImageList.Images.Add(dr["accession_inv_attach_id"].ToString(), thumbnailImage);
                    }
                    else if (!string.IsNullOrEmpty(fi.Extension) &&
                        _supportedImageExtensions.Contains(fi.Extension.ToLower()))
                    {
                        byte[] attachBytes = _sharedUtils.GetAttachment(attachmentPath.TrimStart('/'));
                        if (attachBytes != null && attachBytes.Length > 0)
                        {
                            // Thumbnail image retrieved - add it to the image list...
                            System.IO.MemoryStream ms = new System.IO.MemoryStream(attachBytes);
                            Image fullImage = Image.FromStream(ms, false, true);
                            Image thumbnailImage = fullImage.GetThumbnailImage(Math.Min(128, fullImage.Width), Math.Min(128, fullImage.Height), null, IntPtr.Zero);
                            fullImage.Dispose();
                            returnImageList.Images.Add(dr["accession_inv_attach_id"].ToString(), thumbnailImage);
                        }
                        else
                        {
                            // Thumbnail was not retrieved - use a file-type specific icon instead...
                            string[] matchingFileExtImages = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + @"\Images\AttachmentWizard\", "file_extension_" + fi.Extension.TrimStart('.') + ".*", System.IO.SearchOption.TopDirectoryOnly);
                            if (matchingFileExtImages.Length > 0)
                            {
                                Image fullImage = Image.FromFile(matchingFileExtImages[0]);
                                Image thumbnailImage = fullImage.GetThumbnailImage(fullImage.Width, fullImage.Height, null, IntPtr.Zero);
                                fullImage.Dispose();
                                returnImageList.Images.Add(dr["accession_inv_attach_id"].ToString(), thumbnailImage);
                            }
                        }

                    }
                    else
                    {
                        // This attachment is not a supported image so use an icon image instead...
                        string[] matchingFileExtImages = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + @"\Images\AttachmentWizard\", "file_extension_" + fi.Extension.TrimStart('.') + ".*", System.IO.SearchOption.TopDirectoryOnly);
                        if (matchingFileExtImages.Length > 0)
                        {
                            Image fullImage = Image.FromFile(matchingFileExtImages[0]);
                            Image thumbnailImage = fullImage.GetThumbnailImage(fullImage.Width, fullImage.Height, null, IntPtr.Zero);
                            fullImage.Dispose();
                            returnImageList.Images.Add(dr["accession_inv_attach_id"].ToString(), thumbnailImage);
                        }
                        else if (dr["category_code"].ToString().ToUpper() == "LINK" &&
                            dr["content_type"].ToString().ToUpper().Contains("HTML"))
                        {
                            matchingFileExtImages = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + @"\Images\AttachmentWizard\", "file_extension_html" + ".*", System.IO.SearchOption.TopDirectoryOnly);
                            if (matchingFileExtImages.Length > 0)
                            {
                                Image fullImage = Image.FromFile(matchingFileExtImages[0]);
                                Image thumbnailImage = fullImage.GetThumbnailImage(fullImage.Width, fullImage.Height, null, IntPtr.Zero);
                                fullImage.Dispose();
                                returnImageList.Images.Add(dr["accession_inv_attach_id"].ToString(), thumbnailImage);
                            }
                        }
                        else
                        {
                            matchingFileExtImages = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + @"\Images\AttachmentWizard\", "file_extension_unknown" + ".*", System.IO.SearchOption.TopDirectoryOnly);
                            if (matchingFileExtImages.Length > 0)
                            {
                                Image fullImage = Image.FromFile(matchingFileExtImages[0]);
                                Image thumbnailImage = fullImage.GetThumbnailImage(fullImage.Width, fullImage.Height, null, IntPtr.Zero);
                                fullImage.Dispose();
                                returnImageList.Images.Add(dr["accession_inv_attach_id"].ToString(), thumbnailImage);
                            }
                        }
                    }
                }
                i++;
                UpdateProgressBar(true, 0, inventoryAttach.Rows.Count, i, "Processing Attachment Files...");
            }

            return returnImageList;
        }

        private void BuildTreeView()
        {
            //DataTable dt;
            //if (ux_datagridviewAttachments.DataSource.GetType() == typeof(BindingSource))
            //{
            //    dt = ((DataTable)((BindingSource)ux_datagridviewAttachments.DataSource).DataSource);
            //}
            //else
            //{
            //    dt = ((DataTable)ux_datagridviewAttachments.DataSource);
            //}

            //DataGridView dgv = new DataGridView();

            //Image i = Image.FromFile(@"C:\Temp\InvAttTesting\Images_1999-09-21\Ames_24067_97ncao01_SD\Ames_24067_97ncao01_SD_FR_XX_1999-09-21_01.jpg");
            //Image t = i.GetThumbnailImage(100, 100, null, System.IntPtr.Zero);
            //Image z = Image.FromFile(@"C:\Temp\InvAttTesting\Images_1999-09-21\Ames_24067_97ncao01_SD\Ames_24067_97ncao01_SD_FR_XX_1999-09-21_01.jpg").GetThumbnailImage(100, 100, null, System.IntPtr.Zero);
            ux_treeviewInventoryList.Nodes.Clear();
            ux_treeviewInventoryList.ShowNodeToolTips = true;
            ux_treeviewInventoryList.ImageList = GetTreeviewImageList();
            ux_treeviewInventoryList.ImageKey = "inactive_unknown";
            ux_treeviewInventoryList.SelectedImageKey = "active_unknown";

            TreeNode rootNode = new TreeNode();
            rootNode.Name = _currentFolderPathOrFileNames;
            rootNode.Text = "Accession/Inventory Attachments"; // _currentFolderPathOrFileNames;
            rootNode.Tag = "ROOTFOLDER";
            rootNode.ImageKey = "inactive_folder";
            rootNode.SelectedImageKey = "active_folder";
            //rootNode.ToolTipText = "Hello World";

/*
            sortableValue = _sharedUtils.GetLookupDisplayMember(lookupTable, dr[dcName].ToString(), "", dr[dcName].ToString());
            char[] Delimiter = new char[] { ' ', '\t', '\n', '\r' };
            string[] stringTokens = sortableValue.Split(Delimiter, StringSplitOptions.RemoveEmptyEntries);
            Decimal paddingNumber = 0.0M;
            string alphanumericSortableValue = "";
            foreach (string token in stringTokens)
            {
                if (Decimal.TryParse(token, out paddingNumber))
                {
                    alphanumericSortableValue += paddingNumber.ToString("00000000000000000000.0000000000") + " ";
                }
                else
                {
                    alphanumericSortableValue += token.PadRight(40, ' ') + " ";
                }
            }
            sortableValue = alphanumericSortableValue;
*/

            // Create an empty folder for each inventory_id being processed currently...
            if (_currentInventoryIDs.ToUpper().Contains(":INVENTORYID="))
            {
                string pkeys = _currentInventoryIDs.Split('=')[1];
                string[] inventoryIDs = pkeys.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string inventoryID in inventoryIDs)
                {
                    TreeNode newAttachNode = new TreeNode();
                    string inventoryNum = _sharedUtils.GetLookupDisplayMember("inventory_lookup", inventoryID, "", inventoryID);
                    if (!rootNode.Nodes.ContainsKey(inventoryID))
                    {
                        TreeNode newInventoryNumNode = new TreeNode();
                        newInventoryNumNode.Name = inventoryID;
                        newInventoryNumNode.Text = inventoryNum;
                        newInventoryNumNode.Tag = "FOLDER";
                        newInventoryNumNode.ImageKey = "inactive_folder";
                        newInventoryNumNode.SelectedImageKey = "active_folder";
                        //newInventoryNumNode.ToolTipText = "Hello World";
                        rootNode.Nodes.Add(newInventoryNumNode);
                    }
                }
            }

            foreach (DataRowView drv in _inventoryAttach.DefaultView)
            {
                TreeNode newAttachNode = new TreeNode();
                TreeNode currentInventoryNumNode = new TreeNode();
                string inventoryNum = _sharedUtils.GetLookupDisplayMember("inventory_lookup", drv["inventory_id"].ToString(), "", drv["inventory_id"].ToString());
                if (!rootNode.Nodes.ContainsKey(drv["inventory_id"].ToString()))
                {
                    TreeNode newInventoryNumNode = new TreeNode();
                    newInventoryNumNode.Name = drv["inventory_id"].ToString();
                    newInventoryNumNode.Text = inventoryNum;
                    newInventoryNumNode.Tag = "FOLDER";
                    newInventoryNumNode.ImageKey = "inactive_folder";
                    newInventoryNumNode.SelectedImageKey = "active_folder";
                    //newInventoryNumNode.ToolTipText = "Hello World";
                    rootNode.Nodes.Add(newInventoryNumNode);
                    currentInventoryNumNode = newInventoryNumNode;
                }
                else
                {
                    currentInventoryNumNode = rootNode.Nodes[drv["inventory_id"].ToString()];
                }
                string[] fileNameTokens = drv["virtual_path"].ToString().Split('\\', '/', ':');
                newAttachNode.Name = drv["accession_inv_attach_id"].ToString();
                newAttachNode.Text = fileNameTokens[fileNameTokens.Length - 1];
                newAttachNode.Tag = "ATTACHMENT";
                //newAttachNode.ImageKey = "inactive_inventory_id";
                //newAttachNode.SelectedImageKey = "active_inventory_id";
                newAttachNode.ImageKey = "inactive_accession_inv_attach_id";
                newAttachNode.SelectedImageKey = "active_accession_inv_attach_id";
                newAttachNode.ToolTipText = drv["virtual_path"].ToString();
                currentInventoryNumNode.Nodes.Add(newAttachNode);
            }

            if (rootNode.Nodes.Count > 0)
            {
                ux_splitcontainerAttachmentDataDisplay.Enabled = true;
            }
            else
            {
                ux_splitcontainerAttachmentDataDisplay.Enabled = false;
            }

            ux_treeviewInventoryList.Nodes.Add(rootNode);
            ux_treeviewInventoryList.ExpandAll();
            ux_treeviewInventoryList.Sort();
        }

        private ImageList GetTreeviewImageList()
        {
            if (_treeviewImageList == null)
            {
                ImageList treeviewImageList = new ImageList();

                // Load the images for the tree view(s)...
                treeviewImageList.ColorDepth = ColorDepth.Depth32Bit;

                // Load the icons from the image sub-directrory (this directory is found under the directory the Curator Tool was launched from)...
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.IO.Directory.GetCurrentDirectory());
                System.IO.FileInfo[] iconFiles = di.GetFiles("Images\\TreeView\\*.ico", System.IO.SearchOption.TopDirectoryOnly);
                if (iconFiles != null && iconFiles.Length > 0)
                {
                    for (int i = 0; i < iconFiles.Length; i++)
                    {
                        treeviewImageList.Images.Add(iconFiles[i].Name.TrimEnd('.', 'i', 'c', 'o'), Icon.ExtractAssociatedIcon(iconFiles[i].FullName));
                    }
                }

                // Cache the list so you don't have to load it again...
                _treeviewImageList = treeviewImageList;
            }

            return _treeviewImageList;
        }

        private void ux_buttonBrowse_Click(object sender, EventArgs e)
        {
            //OpenFileDialog ofd = ux_openfiledialog;
            //ofd.ShowDialog();
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = Properties.Settings.Default.previousFilePath;
            if (DialogResult.OK == fbd.ShowDialog())
            {
                _currentFolderPathOrFileNames = fbd.SelectedPath;
                _currentInventoryIDs = "";
                Properties.Settings.Default.previousFilePath = fbd.SelectedPath;

                // Change cursor to the wait cursor...
                //Cursor origCursor = Cursor.Current;
                //Cursor.Current = Cursors.WaitCursor;

                RefreshData();

                // Restore cursor to default cursor...
                //Cursor.Current = origCursor;
            }
        }

        private void ux_checkboxViewExistingAttachments_CheckedChanged(object sender, EventArgs e)
        {
            _previousInventoryIDs = "";
            _previousFolderPathOrFileNames = "";
            RefreshData();
        }

        private void ux_buttonSave_Click(object sender, EventArgs e)
        {
            int errorCount = SaveInventoryAttachmentData();

            if (errorCount == 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Attachment Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "InventoryAttachmentWizard_ux_buttonSave1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();

                // Get a fresh copy of the data and rebuild the interface (in case some attachments were deleted)...
                //_previousInventoryIDs = "";
                //_previousFolderPathOrFileNames = "";
                RefreshData();

            }
            else
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed in the Grid View tab.\n\n  Error Count: {0}", "Attachment Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "InventoryAttachmentWizard_ux_buttonSave2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = errorCount.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }
        }

        private void ux_buttonSaveAndExit_Click(object sender, EventArgs e)
        {
            int errorCount = SaveInventoryAttachmentData();

            if (errorCount == 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Attachment Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "InventoryAttachmentWizard_ux_buttonSave1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();

                // Close the wizard now...
                this.Close();
            }
            else
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed.\n\nWould you like to review them now?\n\nClick Yes to review the errors now.\n(Click No to abandon the errors and exit the Order Wizard).\n\n  Error Count: {0}", "Attachment Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "InventoryAttachmentWizard_ux_buttonSave2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = errorCount.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();

                // There were errors in saving the data so let the user decide if the wizard should be closed...
                if (DialogResult.No == ggMessageBox.ShowDialog())
                {
                    this.Close();
                }
                else
                {
                    // Do nothing (stay in the wizard)...
                }
            }
        }

        private int SaveInventoryAttachmentData()
        {
            DataSet inventoryAttachChanges = new DataSet();
            DataSet inventoryAttachResults = new DataSet();
            int errorCount = 0;
            string remotePath = "";

            // Make sure that any edits pending have been committed to the datatable
            // prior to getting changes...
            _inventoryAttachBindingSource.EndEdit();

            if (_inventoryAttach.GetChanges() != null)
            {
                // Make sure all edits to the _inventoryAttach table have been commited...
                _inventoryAttachBindingSource.EndEdit();

                // Show the user that something is being done...
                UpdateProgressBar(true, 0, 4, 1, "Processing database records...");
                inventoryAttachChanges.Tables.Add(_inventoryAttach.GetChanges());
                ScrubData(inventoryAttachChanges);
                // Save the changes to the remote server...
                UpdateProgressBar(true, 0, 4, 2, "Processing database records...");
                inventoryAttachResults = _sharedUtils.SaveWebServiceData(inventoryAttachChanges);
                UpdateProgressBar(true, 0, 4, 3, "Processing database records...");
                if (inventoryAttachResults.Tables.Contains(_inventoryAttach.TableName))
                {
                    errorCount += SyncSavedResults(_inventoryAttach, inventoryAttachResults.Tables[_inventoryAttach.TableName]);
                }
                UpdateProgressBar(false, 0, 4, 0, "Done processing database records...");

                Dictionary<string, string> imageLoadDictionary = new Dictionary<string, string>();
                foreach (DataRow dr in inventoryAttachResults.Tables[_inventoryAttach.TableName].Rows)
                //foreach (DataRow dr in inventoryAttachChanges.Tables[_inventoryAttach.TableName].Rows)
                    {
                    if (dr["SavedAction"].ToString().ToUpper() != "DELETE" &&
                        dr["SavedStatus"].ToString().ToUpper() == "SUCCESS" &&
                        !string.IsNullOrEmpty(dr["virtual_path"].ToString()))
                    {
                        // Get local file name and full path...
                        string localFilePath = "";
                        string remoteFilePath = dr["virtual_path"].ToString();
                        string[] tokens = dr["virtual_path"].ToString().Split('/', '\\', ':');
                        string remoteFileName = tokens[tokens.Length - 1];
                        foreach (string key in _attachmentFilesDictionary.Keys)
                        {
                            if (key.Contains(remoteFileName) &&
                                _attachmentFilesDictionary[key] == (int)dr["inventory_id"])
                            {
                                // Found the dictionary entry that matches the filename and the inventory_id...
                                localFilePath = key;
                                break;
                            }
                        }

                        // If the local filename and path was found in the dictionary 
                        // and the record was either inserted or the virtual path was changed then
                        // the image needs to be loaded to the server - so add it to the dictionary list...
                        if (!string.IsNullOrEmpty(localFilePath))
                        {
                            imageLoadDictionary.Add(localFilePath, dr["virtual_path"].ToString());
                        }
                    }
                }

                int i = 0;
                UpdateProgressBar(true, 0, imageLoadDictionary.Count, i, "Uploading image files...");
                foreach (string localFilePath in imageLoadDictionary.Keys)
                {
                    i++;
                    UpdateProgressBar(true, 0, imageLoadDictionary.Count, i, "Uploading image files...");
                    byte[] imageBytes = System.IO.File.ReadAllBytes(localFilePath);
                    // Attempt to upload the image to the remote server...
                    if (imageBytes != null)
                    {
                        remotePath = _sharedUtils.SaveAttachment(imageLoadDictionary[localFilePath], imageBytes, true, true);
                    }
                }
                UpdateProgressBar(false, 0, imageLoadDictionary.Count, 0, "Done uploading image files...");
            }
            return errorCount;
        }

        private void UpdateProgressBar(bool visible, int min, int max, int progressVal, string description)
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            ux_progressbar.Visible = visible;
            ux_labelProgressValue.Visible = visible;
            ux_labelProgressDescription.Visible = visible;
            ux_progressbar.Minimum = min;
            ux_progressbar.Maximum = max;
            ux_progressbar.Value = progressVal;
            ux_labelProgressValue.Text = (progressVal * 100 / (Math.Max(max - min, 1))).ToString() + " %";
            ux_labelProgressValue.Update();
            ux_labelProgressDescription.Text = description;
            ux_labelProgressDescription.Update();
            
            Application.DoEvents();
            
            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void ScrubData(DataSet ds)
        {
            // Make sure all non-nullable fields do not contain a null value - if they do, replace it with the default value...
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr.RowState != DataRowState.Deleted)
                    {
                        foreach (DataColumn dc in dt.Columns)
                        {
                            if (dc.ExtendedProperties.Contains("is_nullable") &&
                                dc.ExtendedProperties["is_nullable"].ToString().Trim().ToUpper() == "N" &&
                                dr[dc] == DBNull.Value)
                            {
                                if (dc.ExtendedProperties.Contains("default_value") &&
                                    !string.IsNullOrEmpty(dc.ExtendedProperties["default_value"].ToString()) &&
                                    dc.ExtendedProperties["default_value"].ToString().Trim().ToUpper() != "{DBNULL.VALUE}")
                                {
                                    dr[dc] = dc.ExtendedProperties["default_value"].ToString();
                                }
                            }
                        }
                    }
                }
            }
        }

        private int SyncSavedResults(DataTable originalTable, DataTable savedResults)
        {
            int errorCount = 0;

            // The middle tier will ignore 'changed records' that have no 'real' changes by not putting them in the saved results datatable
            // so we will 'pretend' that the MT returned 'success' for saving an unchanged record...
            foreach (DataRow dr in originalTable.GetChanges().Rows)
            {
                if(dr.RowState == DataRowState.Modified  &&
                    !savedResults.Rows.Contains(dr[originalTable.PrimaryKey[0].ColumnName]))
                {
                    DataRow newDR = savedResults.NewRow();
                    newDR.ItemArray = dr.ItemArray;
                    newDR["OriginalPrimaryKeyID"] = dr[originalTable.PrimaryKey[0].ColumnName];
                    newDR["NewPrimaryKeyID"] = dr[originalTable.PrimaryKey[0].ColumnName];
                    newDR["SavedAction"] = "Update";
                    newDR["SavedStatus"] = "Success";
                    newDR["ExceptionMessage"] = "";
                    savedResults.Rows.Add(newDR);
                }
            }
            savedResults.AcceptChanges();

            if (savedResults != null && savedResults.PrimaryKey.Length == 1)
            {
                string pKeyCol = savedResults.PrimaryKey[0].ColumnName.Trim().ToUpper();
                savedResults.Columns[pKeyCol].ReadOnly = false;
                foreach (DataRow dr in savedResults.Rows)
                {
                    DataRow originalRow = originalTable.Rows.Find(dr["OriginalPrimaryKeyID"]);

                    switch (dr["SavedAction"].ToString())
                    {
                        case "Insert":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the originalTable row's status for this new row to committed (and update the pkey with the int returned from the server DB)...
                                if (originalRow != null)
                                {
                                    bool origColumnReadOnlyValue = originalRow.Table.Columns[pKeyCol].ReadOnly;
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = false;
                                    originalRow[pKeyCol] = dr["NewPrimaryKeyID"];
                                    originalRow.AcceptChanges();
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = origColumnReadOnlyValue;
                                    originalRow.ClearErrors();
                                }
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Update":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                originalRow.AcceptChanges();
                                originalRow.ClearErrors();
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Delete":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                string deleteAttachmentVirtualPath = "";
                                string deleteAttachmentThumbnailVirtualPath = "";
                                // Set the row's status for this deleted row to committed...
                                if (originalRow != null)
                                {
                                    if (originalRow.Table.Columns.Contains("virtual_path")) deleteAttachmentVirtualPath = originalRow["virtual_path"].ToString().Trim();
                                    if (originalRow.Table.Columns.Contains("thumbnail_virtual_path")) deleteAttachmentThumbnailVirtualPath = originalRow["thumbnail_virtual_path"].ToString().Trim();
                                    originalRow.AcceptChanges();
                                    originalRow.ClearErrors();
                                }
                                else
                                {
                                    DataRow deletedRow = null;
                                    foreach (DataRow deleteddr in originalTable.Rows)
                                    {
                                        if (deleteddr.RowState == DataRowState.Deleted && deleteddr[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                        {
                                            if (deleteddr.Table.Columns.Contains("virtual_path")) deleteAttachmentVirtualPath = deleteddr["virtual_path", DataRowVersion.Original].ToString().Trim();
                                            if (deleteddr.Table.Columns.Contains("thumbnail_virtual_path")) deleteAttachmentThumbnailVirtualPath = deleteddr["thumbnail_virtual_path", DataRowVersion.Original].ToString().Trim();
                                            deletedRow = deleteddr;
                                        }
                                    }
                                    deletedRow.AcceptChanges();
                                    deletedRow.ClearErrors();
                                }
                                // Now that the record has been deleted the attachment should be deleted too...
                                if (!string.IsNullOrEmpty(deleteAttachmentVirtualPath)) _sharedUtils.DeleteAttachment(deleteAttachmentVirtualPath);
                                if (!string.IsNullOrEmpty(deleteAttachmentThumbnailVirtualPath)) _sharedUtils.DeleteAttachment(deleteAttachmentThumbnailVirtualPath);
                            }
                            else
                            {
                                errorCount++;
                                // Find the deleted row (NOTE: datatable.rows.find() method does not work on deleted rows)...
                                foreach (DataRow deletedRow in originalTable.Rows)
                                {
                                    if (deletedRow[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                    {
                                        deletedRow.RejectChanges();
                                        deletedRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            return errorCount;
        }
       
        #region Dynamic Controls logic...
        private void bindControls(Control.ControlCollection controlCollection, BindingSource bindingSource)
        {
            foreach (Control ctrl in controlCollection)
            {
                //if (ctrl != ux_bindingnavigatorForm)  // Leave the bindingnavigator alone
                if (!(ctrl is BindingNavigator))  // Leave the bindingnavigators alone
                {
                    // If the ctrl has children - bind them too...
                    if (ctrl.Controls.Count > 0)
                    {
                        bindControls(ctrl.Controls, bindingSource);
                    }
                    // Bind the control (by type)...
                    if (ctrl is ComboBox) bindComboBox((ComboBox)ctrl, bindingSource);
                    if (ctrl is TextBox) bindTextBox((TextBox)ctrl, bindingSource);
                    if (ctrl is CheckBox) bindCheckBox((CheckBox)ctrl, bindingSource);
                    if (ctrl is DateTimePicker) bindDateTimePicker((DateTimePicker)ctrl, bindingSource);
                    if (ctrl is Label) bindLabel((Label)ctrl, bindingSource);
                }
            }
        }

        private void formatControls(Control.ControlCollection controlCollection, BindingSource bindingSource)
        {
            foreach (Control ctrl in controlCollection)
            {
                if (ctrl != ux_panelFormView)  // Leave the panel control alone
                {
                    // If the ctrl has children - set their edit mode too...
                    if (ctrl.Controls.Count > 0)
                    {
                        formatControls(ctrl.Controls, bindingSource);
                    }
                    // Set the edit mode for the control...
                    if (ctrl != null &&
                        ctrl.Tag != null &&
                        ctrl.Tag is string &&
                        bindingSource != null &&
                        bindingSource.DataSource is DataTable &&
                        ((DataTable)bindingSource.DataSource).Columns.Contains(ctrl.Tag.ToString().Trim().ToLower()))
                    {
                        // If the field is a ReadOnly field do not allow the tab key to be used to navigate to it...
                        ctrl.TabStop = !((DataTable)bindingSource.DataSource).Columns[ctrl.Tag.ToString().Trim().ToLower()].ReadOnly;
                        // Set the control's edit properties based on what type of control it is, what edit mode is current, and if the field is readonly...
                        if (ctrl is TextBox)
                        {
                            // TextBoxes have a ReadOnly property in addition to an Enabled property so we handle this one separate...
                            ((TextBox)ctrl).ReadOnly = ((DataTable)bindingSource.DataSource).Columns[ctrl.Tag.ToString().Trim().ToLower()].ReadOnly;
                        }
                        else if (ctrl is Label)
                        {
                            // Do nothing to the Label
                        }
                        else
                        {
                            // All other control types (ComboBox, CheckBox, DateTimePicker) except Labels...
                            ctrl.Enabled = !((DataTable)bindingSource.DataSource).Columns[ctrl.Tag.ToString().Trim().ToLower()].ReadOnly;
                        }
                    }
                }
            }
        }

        private void bindComboBox(ComboBox comboBox, BindingSource bindingSource)
        {
            comboBox.DataBindings.Clear();
            comboBox.Enabled = false;
            if (comboBox != null &&
                comboBox.Tag != null &&
                comboBox.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(comboBox.Tag.ToString().Trim().ToLower()))
            {
                if (_sharedUtils != null)
                {
                    DataColumn dc = ((DataTable)bindingSource.DataSource).Columns[comboBox.Tag.ToString().Trim().ToLower()];
                    _sharedUtils.BindComboboxToCodeValue(comboBox, dc);
                    if (comboBox.DataSource.GetType() == typeof(DataTable))
                    {
                        // Calculate the maximum width needed for displaying the dropdown items and set the combobox property...
                        int maxWidth = comboBox.DropDownWidth;
                        foreach (DataRow dr in ((DataTable)comboBox.DataSource).Rows)
                        {
                            if (TextRenderer.MeasureText(dr["display_member"].ToString().Trim(), comboBox.Font).Width > maxWidth)
                            {
                                maxWidth = TextRenderer.MeasureText(dr["display_member"].ToString().Trim(), comboBox.Font).Width;
                            }
                        }
                        comboBox.DropDownWidth = maxWidth;
                    }

                    // Bind the SelectedValue property to the binding source...
                    comboBox.DataBindings.Add("SelectedValue", bindingSource, comboBox.Tag.ToString().Trim().ToLower(), true, DataSourceUpdateMode.OnPropertyChanged);

                    // Wire up to an event handler if this column is a date_code (format) field...
                    if (dc.ColumnName.Trim().ToLower().EndsWith("_code") &&
                        dc.Table.Columns.Contains(dc.ColumnName.Trim().ToLower().Substring(0, dc.ColumnName.Trim().ToLower().LastIndexOf("_code"))))
                    {
                        comboBox.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
                    }
                }
                else
                {
                    // Bind the Text property to the binding source...
                    comboBox.DataBindings.Add("Text", bindingSource, comboBox.Tag.ToString().Trim().ToLower(), true, DataSourceUpdateMode.OnPropertyChanged);
                }
            }
        }

        private void bindTextBox(TextBox textBox, BindingSource bindingSource)
        {
            textBox.DataBindings.Clear();
            textBox.ReadOnly = true;
            if (textBox != null &&
                textBox.Tag != null &&
                textBox.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(textBox.Tag.ToString().Trim().ToLower()))
            {
                DataTable dt = (DataTable)bindingSource.DataSource;
                DataColumn dc = dt.Columns[textBox.Tag.ToString().Trim().ToLower()];
                if (_sharedUtils.LookupTablesIsValidFKField(dc))
                {
                    // Create a new binding that handles display_member/value_member conversions...
                    Binding textBinding = new Binding("Text", bindingSource, textBox.Tag.ToString().Trim().ToLower());
                    textBinding.Format += new ConvertEventHandler(textLUBinding_Format);
                    textBinding.Parse += new ConvertEventHandler(textLUBinding_Parse);
                    textBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                    // Bind it to the textbox...
                    textBox.DataBindings.Add(textBinding);
                }
                else if (dc.DataType == typeof(DateTime))
                {
                    // Create a new binding that handles display_member/value_member conversions...
                    Binding textBinding = new Binding("Text", bindingSource, textBox.Tag.ToString().Trim().ToLower());
                    textBinding.Format += new ConvertEventHandler(textDateTimeBinding_Format);
                    textBinding.Parse += new ConvertEventHandler(textDateTimeBinding_Parse);
                    textBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                    // Bind it to the textbox...
                    textBox.DataBindings.Add(textBinding);
                }
                else
                {
                    // Bind to a plain-old text field in the database (no LU required)...
                    textBox.DataBindings.Add("Text", bindingSource, textBox.Tag.ToString().Trim().ToLower());
                }

                // Add an event handler for processing the first key press (to display the lookup picker dialog)...
                textBox.KeyDown += new KeyEventHandler(textBox_KeyDown);
                textBox.KeyPress += new KeyPressEventHandler(textBox_KeyPress);
            }
        }

        private void bindCheckBox(CheckBox checkBox, BindingSource bindingSource)
        {
            checkBox.DataBindings.Clear();
            checkBox.Enabled = false;
            if (checkBox != null &&
                checkBox.Tag != null &&
                checkBox.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(checkBox.Tag.ToString().Trim().ToLower()))
            {
                DataTable dt = (DataTable)bindingSource.DataSource;
                DataColumn dc = dt.Columns[checkBox.Tag.ToString().Trim().ToLower()];
                checkBox.Text = dc.Caption;
                Binding boolBinding = new Binding("Checked", bindingSource, checkBox.Tag.ToString().Trim().ToLower());
                boolBinding.Format += new ConvertEventHandler(boolBinding_Format);
                boolBinding.Parse += new ConvertEventHandler(boolBinding_Parse);
                boolBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                checkBox.DataBindings.Add(boolBinding);
            }
        }

        private void bindDateTimePicker(DateTimePicker dateTimePicker, BindingSource bindingSource)
        {
            dateTimePicker.DataBindings.Clear();
            dateTimePicker.Enabled = false;
            if (dateTimePicker != null &&
                dateTimePicker.Tag != null &&
                dateTimePicker.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(dateTimePicker.Tag.ToString().Trim().ToLower()))
            {
                // Now bind the control to the column in the bindingSource...
                dateTimePicker.DataBindings.Add("Text", bindingSource, dateTimePicker.Tag.ToString().Trim().ToLower(), true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        private void bindLabel(Label label, BindingSource bindingSource)
        {
            if (label != null &&
                label.Tag != null &&
                label.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(label.Tag.ToString().Trim().ToLower()))
            {
                //label.DataBindings.Add("Text", bindingSource, label.Tag.ToString().Trim().ToLower());
                label.Text = ((DataTable)bindingSource.DataSource).Columns[label.Tag.ToString().Trim().ToLower()].Caption;
            }
        }

        void boolBinding_Format(object sender, ConvertEventArgs e)
        {
            switch (e.Value.ToString().ToUpper())
            {
                case "Y":
                    e.Value = true;
                    break;
                case "N":
                    e.Value = false;
                    break;
                default:
                    e.Value = false;
                    break;
            }
        }

        void boolBinding_Parse(object sender, ConvertEventArgs e)
        {
            if (e.Value != null)
            {
                switch ((bool)e.Value)
                {
                    case true:
                        e.Value = "Y";
                        break;
                    case false:
                        e.Value = "N";
                        break;
                    default:
                        e.Value = "N";
                        break;
                }
            }
            else
            {
                e.Value = "N";
            }
        }

        void textDateTimeBinding_Format(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (dc.DataType == typeof(DateTime) &&
                !string.IsNullOrEmpty(e.Value.ToString()) &&
                dt.Columns.Contains(dc.ColumnName.Trim().ToLower() + "_code"))
            {
                DataRowView drv = (DataRowView)((BindingSource)b.DataSource).Current;
                string dateFormat = "MM/dd/yyyy";
                dateFormat = drv[dc.ColumnName + "_code"].ToString().Trim();
                e.Value = ((DateTime)e.Value).ToString(dateFormat);
            }
            else if (dc.ColumnName.ToLower().Trim() == "ordered_date" ||
                dc.ColumnName.ToLower().Trim() == "completed_date")
            {
                DateTime formattedDate;
                if (DateTime.TryParse(e.Value.ToString(), out formattedDate))
                {
                    e.Value = formattedDate.ToString("d");
                }
            }
        }

        void textDateTimeBinding_Parse(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (dc.DataType == typeof(DateTime) &&
                !string.IsNullOrEmpty(e.Value.ToString()) &&
                dt.Columns.Contains(dc.ColumnName.Trim().ToLower() + "_code"))
            {
                DataRowView drv = (DataRowView)((BindingSource)b.DataSource).Current;
                string dateFormat = drv[dc.ColumnName + "_code"].ToString().Trim();
                DateTime parsedDateTime;
                if (DateTime.TryParseExact(e.Value.ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out parsedDateTime))
                {
                    e.Value = parsedDateTime;
                }
            }
        }

        void textLUBinding_Format(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (!string.IsNullOrEmpty(e.Value.ToString()))
            {
                //e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_resultset_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());
                e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());
            }
        }

        void textLUBinding_Parse(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (!string.IsNullOrEmpty(e.Value.ToString()))
            {
                //e.Value = _sharedUtils.GetLookupValueMember(dc.ExtendedProperties["foreign_key_resultset_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());  ((DataRowView)((BindingSource)b.DataSource).Current).Row
                e.Value = _sharedUtils.GetLookupValueMember(((DataRowView)((BindingSource)b.DataSource).Current).Row, dc.ExtendedProperties["foreign_key_dataview_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());
            }
        }

        void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            string dateColumnName = cb.Tag.ToString().Replace("_code", "");
            foreach (Binding b in cb.DataBindings)
            {
                foreach (Control ctrl in cb.Parent.Controls)
                {
                    if (ctrl.Tag.ToString() == dateColumnName &&
                        ctrl.GetType() == typeof(TextBox))
                    {
                        DataRowView drv = (DataRowView)((BindingSource)b.DataSource).Current;
                        if (drv == null) continue;
                        string dateFormat = "MM/dd/yyyy";
                        dateFormat = drv[cb.Tag.ToString()].ToString().Trim();
                        DateTime dt;
                        if (DateTime.TryParseExact(drv[dateColumnName].ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out dt))
                        {
                            ctrl.Text = ((DateTime)drv[dateColumnName]).ToString(dateFormat);
                        }
                    }
                }
            }
        }

        void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;


            if (!tb.ReadOnly)
            {
                foreach (Binding b in tb.DataBindings)
                {
                    if (b.BindingManagerBase != null &&
                        b.BindingManagerBase.Current != null &&
                        b.BindingManagerBase.Current is DataRowView &&
                        b.BindingMemberInfo.BindingField != null)
                    {
                        if (_sharedUtils.LookupTablesIsValidFKField(((DataRowView)b.BindingManagerBase.Current).Row.Table.Columns[b.BindingMemberInfo.BindingField]) &&
                            e.KeyChar != Convert.ToChar(Keys.Escape)) // Ignore the Escape key and process anything else...
                        {
                            string filterText = tb.Text;
                            if (System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), "[a-zA-Z0-9]"))
                            {
                                filterText = e.KeyChar.ToString();
                            }
                            LookupTablePicker ltp = new LookupTablePicker(_sharedUtils, tb.Tag.ToString(), ((DataRowView)b.BindingManagerBase.Current).Row, filterText);
                            ltp.StartPosition = FormStartPosition.CenterParent;
                            if (DialogResult.OK == ltp.ShowDialog())
                            {
                                tb.Text = ltp.NewValue.Trim();
                            }
                            e.Handled = true;
                        }
                    }
                }
            }
        }

        void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) // Process the Delete key (since it is not passed on to the KeyPress event handler)...
            {
                TextBox tb = (TextBox)sender;

                if (!tb.ReadOnly)
                {
                    foreach (Binding b in tb.DataBindings)
                    {
                        if (b.BindingManagerBase != null &&
                            b.BindingManagerBase.Current != null &&
                            b.BindingManagerBase.Current is DataRowView &&
                            b.BindingMemberInfo.BindingField != null)
                        {
                            // Just in case the user selected only a part of the full text to delete - strip out the selected text and process normally...
                            string remainingText = tb.Text.Remove(tb.SelectionStart, tb.SelectionLength);
                            if (string.IsNullOrEmpty(remainingText))
                            {
                                // When a textbox is bound to a table - some datatypes will not revert to a DBNull via the bound control - so
                                // take control of the update and force the field back to a null (non-nullable fields should show up to the GUI with colored background)...
                                ((DataRowView)b.BindingManagerBase.Current).Row[b.BindingMemberInfo.BindingField] = DBNull.Value;
                                b.ReadValue();
                                e.Handled = true;
                            }
                            else
                            {
                                if (_sharedUtils.LookupTablesIsValidFKField(((DataRowView)b.BindingManagerBase.Current).Row.Table.Columns[b.BindingMemberInfo.BindingField]))
                                {
                                    LookupTablePicker ltp = new LookupTablePicker(_sharedUtils, tb.Tag.ToString(), ((DataRowView)b.BindingManagerBase.Current).Row, remainingText);
                                    ltp.StartPosition = FormStartPosition.CenterParent;
                                    if (DialogResult.OK == ltp.ShowDialog())
                                    {
                                        tb.Text = ltp.NewValue.Trim();
                                        b.WriteValue();
                                        e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        void _inventoryAttachBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (_inventoryAttachBindingSource.List.Count > 0)
            {
            }
            else
            {
            }

            formatControls(ux_panelFormView.Controls, _inventoryAttachBindingSource);
        }

        void _inventoryAttachindingSource_CurrentChanged(object sender, EventArgs e)
        {
            if (_inventoryAttachBindingSource.List.Count > 0)
            {
                // Update the row error message for this row...
                if (string.IsNullOrEmpty(((DataRowView)_inventoryAttachBindingSource.Current).Row.RowError))
                {
                }
                else
                {
                }
            }
        }

        #region DGV control logic...
        private void ux_datagridview_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            // Get the defaultview of the datatable used by the dgv...
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }

            // Begin processing cell formatting based on the datatype of the field...
            if (dv != null &&
                e.ColumnIndex > -1 &&
                e.RowIndex > -1 &&
                e.RowIndex < dv.Count)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                // Format FKey fields...
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    e.RowIndex < dv.Count &&
                    dv[e.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (dv[e.RowIndex][e.ColumnIndex] != DBNull.Value)
                    {
                        e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim(), dv[e.RowIndex][e.ColumnIndex].ToString().Trim(), "", dv[e.RowIndex][e.ColumnIndex].ToString().Trim());
                    }
                    dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    e.FormattingApplied = true;
                }
                // Format date/time fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        e.Value = ((DateTime)dv[e.RowIndex][e.ColumnIndex]).ToString(dateFormat);
                        e.FormattingApplied = true;
                    }
                }
                // Format integer fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    (dc.DataType == typeof(int) ||
                    dc.DataType == typeof(Int16) ||
                    dc.DataType == typeof(Int32) ||
                    dc.DataType == typeof(Int64)))
                {
                    int junk;
                    if (!int.TryParse(e.Value.ToString(), out junk))
                    {
                        dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    }
                }

                if (dc.ReadOnly)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }

                if (dc.ExtendedProperties.Contains("is_nullable") &&
                    dc.ExtendedProperties["is_nullable"].ToString() == "N" &&
                    string.IsNullOrEmpty(dv[e.RowIndex][e.ColumnIndex].ToString()))
                {
                    e.CellStyle.BackColor = Color.Plum;
                }
            }
        }

        private void ux_datagridview_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }
            if (dv != null && e.ColumnIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        DateTime formattedDate;
                        if (DateTime.TryParseExact(e.Value.ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out formattedDate))
                        {
                            e.Value = formattedDate;
                            e.ParsingApplied = true;
                        }
                    }
                }
            }
        }

        private void ux_datagridview_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            string errorMessage = e.Exception.Message;
            int columnWithError = -1;

            // Find the cell the error belongs to (don't use e.ColumnIndex because it points to the current cell *NOT* the offending cell)...
            foreach (DataGridViewColumn col in dgv.Columns)
            {
                if (errorMessage.Contains(col.Name))
                {
                    dgv[col.Name, e.RowIndex].ErrorText = errorMessage;
                    columnWithError = col.Index;
                }
            }
        }

        private void ux_datagridview_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            // Get the defaultview of the datatable used by the dgv...
            DataTable dt = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dt = ((DataTable)((BindingSource)dgv.DataSource).DataSource);
            }
            else
            {
                dt = ((DataTable)dgv.DataSource);
            }
            string columnName = dgv.CurrentCell.OwningColumn.Name;
            DataColumn dc = dt.Columns[columnName];
            DataRow dr;

            if (_sharedUtils.LookupTablesIsValidFKField(dc))
            {
                //string luTableName = dc.ExtendedProperties["foreign_key_resultset_name"].ToString().Trim();
                string luTableName = dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim();
                dr = ((DataRowView)dgv.CurrentRow.DataBoundItem).Row;
                string suggestedFilter = dgv.CurrentCell.EditedFormattedValue.ToString();
                if (_lastDGVCharPressed > 0) suggestedFilter = _lastDGVCharPressed.ToString();
                GRINGlobal.Client.Common.LookupTablePicker ltp = new GRINGlobal.Client.Common.LookupTablePicker(_sharedUtils, columnName, dr, suggestedFilter);
                //_lastDGVCharPressed = (char)0;
                ltp.StartPosition = FormStartPosition.CenterParent;
                if (DialogResult.OK == ltp.ShowDialog())
                {
                    if (dr != null)
                    {
                        if (ltp.NewKey != null && dr[dgv.CurrentCell.ColumnIndex].ToString().Trim() != ltp.NewKey.Trim())
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = ltp.NewKey.Trim();
                            dgv.CurrentCell.Value = ltp.NewValue.Trim();
                        }
                        else if (ltp.NewKey == null)
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = DBNull.Value;
                            dgv.CurrentCell.Value = "";
                        }
                        dr.SetColumnError(dgv.CurrentCell.ColumnIndex, null);
                    }
                }
                dgv.EndEdit();
            }
        }

        private void ux_datagridview_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            // Change cursor to the wait cursor...
            //Cursor origCursor = Cursor.Current;
            //Cursor.Current = Cursors.WaitCursor;

            //ux_webbrowserAttachment.Navigate("http://");
            //if (ux_datagridviewOrderRequestAttach.Columns.Contains("local_path"))
            //{
            //    string attachLocalPath = ux_datagridviewOrderRequestAttach.Rows[e.RowIndex].Cells["local_path"].FormattedValue.ToString();
            //    string attachVirtualPath = ux_datagridviewOrderRequestAttach.Rows[e.RowIndex].Cells["virtual_path"].FormattedValue.ToString();
            //    //if (!string.IsNullOrEmpty(attachLocalPath) && !System.IO.File.Exists(attachLocalPath) && !string.IsNullOrEmpty(attachVirtualPath))
            //    if (System.IO.File.Exists(attachLocalPath))
            //    {
            //        //ux_pictureboxThumbnail.ImageLocation = attachLocalPath;
            //        ux_webbrowserAttachment.Navigate(attachLocalPath);
            //        try
            //        {
            //            if (!Image.FromFile(attachLocalPath).PhysicalDimension.IsEmpty)
            //            {
            //                SHDocVw.WebBrowser wb = (SHDocVw.WebBrowser)ux_webbrowserAttachment.ActiveXInstance;
            //                Image tempImage = Image.FromFile(attachLocalPath);
            //                int zoomWidth = 100 * ux_webbrowserAttachment.Width / tempImage.Width;
            //                int zoomHeight = 100 * ux_webbrowserAttachment.Height / tempImage.Height;
            //                int zoom = Math.Min(zoomWidth, zoomHeight);
            //                if (zoom < 1) zoom = 100;
            //                wb.ExecWB(SHDocVw.OLECMDID.OLECMDID_OPTICAL_ZOOM, SHDocVw.OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER, zoom, null);
            //            }
            //        }
            //        catch
            //        {
            //        }
            //    }
            //    else
            //    {
            //        if (!string.IsNullOrEmpty(attachLocalPath) &&
            //            !string.IsNullOrEmpty(attachVirtualPath) &&
            //            ux_tabcontrolMain.SelectedTab == AttachmentsPage)
            //        {
            //            // Get the attachment data...
            //            byte[] attachBytes = _sharedUtils.GetImage(attachVirtualPath);
            //            if (attachBytes != null && attachBytes.Length > 0)
            //            {
            //                // Make sure the directory exists before writing the attachment data to disk...
            //                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(attachLocalPath));
            //                // Create a local copy of the attachment...
            //                System.IO.File.WriteAllBytes(attachLocalPath, attachBytes);
            //                //ux_pictureboxThumbnail.ImageLocation = attachLocalPath;
            //                ux_webbrowserAttachment.Navigate(attachLocalPath);
            //                try
            //                {
            //                    if (!Image.FromFile(attachLocalPath).PhysicalDimension.IsEmpty)
            //                    {
            //                        SHDocVw.WebBrowser wb = (SHDocVw.WebBrowser)ux_webbrowserAttachment.ActiveXInstance;
            //                        Image tempImage = Image.FromFile(attachLocalPath);
            //                        int zoomWidth = 100 * ux_webbrowserAttachment.Width / tempImage.Width;
            //                        int zoomHeight = 100 * ux_webbrowserAttachment.Height / tempImage.Height;
            //                        int zoom = Math.Min(zoomWidth, zoomHeight);
            //                        if (zoom < 1) zoom = 100;
            //                        wb.ExecWB(SHDocVw.OLECMDID.OLECMDID_OPTICAL_ZOOM, SHDocVw.OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER, zoom, null);
            //                    }
            //                }
            //                catch
            //                {
            //                }
            //            }
            //        }
            //        else
            //        {
            //            //ux_pictureboxThumbnail.ImageLocation = "";
            //            ux_webbrowserAttachment.Navigate("http://");
            //        }
            //    }
            //}

            // Restore cursor to default cursor...
            //Cursor.Current = origCursor;
        }

        private void ux_datagridview_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            // Reset the last char pressed global variable...
            _lastDGVCharPressed = (char)0;

            // Remember the keypress so that it can be passed into the Lookup Picker dialog...
            if (!e.Alt && !e.Control)
            {
                KeysConverter kc = new KeysConverter();
                string lastChar = kc.ConvertToString(e.KeyValue).Replace("NumPad", "");
                //string lastChar = Convert.ToChar(e.KeyCode).ToString();
                if (lastChar.Length == 1)
                {
                    if (e.Shift)
                    {
                        _lastDGVCharPressed = lastChar.ToUpper()[0];
                    }
                    else
                    {
                        _lastDGVCharPressed = lastChar.ToLower()[0];
                    }
                }
            }
        }

        private void ux_datagridview_KeyDown(object sender, KeyEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (_sharedUtils.ProcessDGVEditShortcutKeys(dgv, e, _sharedUtils.UserCooperatorID))
            {
                foreach (DataGridViewRow dgvr in dgv.Rows)
                {
                    //ApplyOrderItemBusinessRules(dgvr);
                }
                //ApplyOrderBusinessRules();
                RefreshDGVFormatting(dgv);
            }
        }

        private void RefreshDGVFormatting(DataGridView dgv)
        {
            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                RefreshDGVRowFormatting(dgvr);
            }

            // Show SortGlyphs for the column headers (this takes two steps)...
            // First reset them all to No Sort...
            foreach (DataGridViewColumn dgvc in dgv.Columns)
            {
                dgvc.HeaderCell.SortGlyphDirection = SortOrder.None;
            }
            // Now inspect the sort string from the datatable in use to set the SortGlyphs...
            string strOrder = "";
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                strOrder = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView.Sort;
            }
            else
            {
                strOrder = ((DataTable)dgv.DataSource).DefaultView.Sort;
            }
            char[] chararrDelimiters = { ',' };
            string[] strarrSortCols = strOrder.Split(chararrDelimiters);
            foreach (string strSortCol in strarrSortCols)
            {
                if (strSortCol.Contains("ASC"))
                {
                    if (dgv.Columns.Contains(strSortCol.Replace(" ASC", "").Replace("_sortable", "").Trim())) dgv.Columns[strSortCol.Replace(" ASC", "").Replace("_sortable", "").Trim()].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                }
                if (strSortCol.Contains("DESC"))
                {
                    if (dgv.Columns.Contains(strSortCol.Replace(" DESC", "").Replace("_sortable", "").Trim())) dgv.Columns[strSortCol.Replace(" DESC", "").Replace("_sortable", "").Trim()].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                }
            }
        }

        private void RefreshDGVRowFormatting(DataGridViewRow dgvr)
        {
            foreach (DataGridViewCell dgvc in dgvr.Cells)
            {
                // Reset the background and foreground color...
                dgvc.Style.BackColor = Color.Empty;
                dgvc.Style.ForeColor = Color.Empty;
                dgvc.Style.SelectionBackColor = Color.Empty;
                dgvc.Style.SelectionForeColor = Color.Empty;
            }

            // Reset the row's default background color...
            dgvr.DefaultCellStyle.BackColor = Color.Empty;

            // If the row has changes make each changed cell yellow...
            DataRow dr = ((DataRowView)dgvr.DataBoundItem).Row;
            if (dr.RowState == DataRowState.Modified)
            {
                foreach (DataGridViewCell dgvc in dgvr.Cells)
                {
                    string dcName = dgvc.OwningColumn.Name;
                    // If the cell has been changed make it yellow...
                    if (dr[dcName, DataRowVersion.Original].ToString().Trim() != dr[dcName, DataRowVersion.Current].ToString().Trim())
                    {
                        dgvc.Style.BackColor = Color.Yellow;
                        dr.SetColumnError(dcName, null);
                    }
                }
            }
        }


        #endregion

        private void ux_treeviewInventoryList_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeView tv = (TreeView)sender;
            TreeNode tn = tv.SelectedNode;
            if (tn.Tag.ToString().ToUpper() == "ROOTFOLDER")
            {
                _inventoryAttach.DefaultView.RowFilter = "";
            }
            else if (tn.Tag.ToString().ToUpper() == "FOLDER")
            {
                _inventoryAttach.DefaultView.RowFilter = "inventory_id = " + tn.Name;
            }
            else if (tn.Tag.ToString().ToUpper() == "ATTACHMENT")
            {
                _inventoryAttach.DefaultView.RowFilter = "accession_inv_attach_id = " + tn.Name;
            }

            if (!ux_checkboxViewExistingAttachments.Checked)
            {
                if (string.IsNullOrEmpty(_inventoryAttach.DefaultView.RowFilter))
                {
                    _inventoryAttach.DefaultView.RowFilter = "accession_inv_attach_id < 0";
                }
                else if (!_inventoryAttach.DefaultView.RowFilter.Contains("accession_inv_attach_id < 0"))
                {
                    _inventoryAttach.DefaultView.RowFilter = _inventoryAttach.DefaultView.RowFilter + " AND accession_inv_attach_id < 0";
                }
            }

            RefreshData();
        }

        private void ux_listviewAttachments_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;
            //_inventoryAttachBindingSource.Position = lv.FocusedItem.Index;
            if (lv.SelectedIndices != null &&
                lv.SelectedIndices.Count > 0)
            {
                _inventoryAttachBindingSource.Position = lv.SelectedIndices[0];
            }
        }

        private void ux_treeviewInventoryList_DragEnter(object sender, DragEventArgs e)
        {
            // Okay we are in the middle of a Drag and Drop operation and the mouse is in 
            // the treeview control so lets handle this event...

            // Convert the mouse coordinates from screen to client...
            Point ptClientCoord = ((TreeView)sender).PointToClient(new Point(e.X, e.Y));
            // Get the node closest to the mouse cursor (to make sure it is a folder)...
            TreeNode tnClosestToMouse = ((TreeView)sender).GetNodeAt(ptClientCoord);

            // Is this a collection of dataset rows being dragged to a node...
            if (e.Data.GetDataPresent("System.Data.DataSet"))
            {
                e.Effect = DragDropEffects.Copy;
            }
            // Is this a treeview node being dragged to a new location...
            else if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode"))
            {
                TreeNode tn = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");
            }
            // Is this an attachment being dragged in to the Attachment Wizard treeview...
            else if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            // Is this a query criteria from the Search Tool...
            else if (e.Data.GetDataPresent(DataFormats.Text))
            {
                string dndText = (string)e.Data.GetData(DataFormats.Text);
                if (dndText.StartsWith("Search Tool Query :: "))
                {
                    e.Effect = DragDropEffects.Copy;
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void ux_treeviewInventoryList_DragDrop(object sender, DragEventArgs e)
        {
            // The drag-drop event is coming to a close - process this event to handle the dropping of
            // data into the treeview...

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Convert the mouse coordinates from screen to client...
            Point ptClientCoord = ((TreeView)sender).PointToClient(new Point(e.X, e.Y));
            string[] formats = e.Data.GetFormats();

            // Is this a collection of dataset rows being dragged to a node...
            if (e.Data.GetDataPresent("System.Data.DataSet") && e.Effect != DragDropEffects.None)
            {
                DataSet dndData = (DataSet)e.Data.GetData("System.Data.DataSet");

                if (dndData != null &&
                    dndData.Tables.Count > 0)
                {
                    DataTable dt = dndData.Tables[0];
                    string pKey = "";
                    string inventoryIDs = ":inventoryid=";
                    if (dt.PrimaryKey.Length > 0) pKey = dt.PrimaryKey[0].ColumnName.ToLower();
                    if (pKey == "inventory_id")
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            inventoryIDs += dr["inventory_id"].ToString() + ",";
                        }
                    }
                    else if (pKey == "accession_id")
                    {
                        string seQuery = "@accession.accession_id IN (";
                        foreach (DataRow dr in dt.Rows)
                        {
                            seQuery += dr["accession_id"].ToString() + ",";
                        }
                        seQuery = seQuery.Trim(',') + ")";
                        DataSet ds = _sharedUtils.SearchWebService(seQuery, true, true, null, "inventory", 0, 0);
                        // Build the parameter string to pass to the get_order_request, order_wizard_get_order_request_item, and get_order_request_action dataview...
                        if (ds.Tables.Contains("SearchResult"))
                        {
                            // Build a list of order_request_ids to use for gathering the order_request_items...
                            foreach (DataRow dr in ds.Tables["SearchResult"].Rows)
                            {
                                inventoryIDs += dr["ID"].ToString() + ",";
                            }
                            inventoryIDs = inventoryIDs.TrimEnd(',');
                        }
                    }
//_currentFolderPathOrFileNames = inventoryIDs.TrimEnd(',');
if(_currentInventoryIDs.StartsWith(":inventoryid="))
{
    _currentInventoryIDs += inventoryIDs.Replace(":inventoryid=", ",").TrimEnd(',');
}
else
{
    _currentInventoryIDs = inventoryIDs.TrimEnd(',');
}
                    _previousInventoryIDs = "";
                    _previousFolderPathOrFileNames = "";
                    RefreshData();
                }
            }
            else if (e.Data.GetDataPresent("FileDrop") && e.Effect != DragDropEffects.None)
            {
                string[] fullPaths = (string[])e.Data.GetData(DataFormats.FileDrop);
                TreeNode destinationNode = ((TreeView)sender).GetNodeAt(((TreeView)sender).PointToClient(new Point(e.X, e.Y)));

                if (destinationNode != null)
                {
                    // Get the inventoryID for the node...
                    int inventoryID = -1;
                    if (destinationNode.Tag.ToString().ToUpper().StartsWith("FOLDER"))
                    {
                        inventoryID = int.Parse(destinationNode.Name);
                    }
                    else if (destinationNode.Tag.ToString().ToUpper().StartsWith("ATTACHMENT") &&
                        destinationNode.Parent != null &&
                        destinationNode.Parent.Tag.ToString().ToUpper().StartsWith("FOLDER"))
                    {
                        inventoryID = int.Parse(destinationNode.Parent.Name);
                    }

                    // Process each filepath in the collection...
                    foreach (string fullPath in fullPaths)
                    {
                        // Remember the association between file and inventoryID specified by user...
                        if (!_dragAndDropAttachmentFilesDictionary.ContainsKey(fullPath))
                        {
                            _dragAndDropAttachmentFilesDictionary.Add(fullPath, inventoryID);
                        }
                        // Concatenate each fullpath into a single string used when processing RefreshData()...
                        if (System.IO.File.Exists(fullPath) ||
                            System.IO.Directory.Exists(fullPath))
                        {
                            if (!_currentFolderPathOrFileNames.Contains(fullPath)) _currentFolderPathOrFileNames += "; " + fullPath;
                        }
                    }

                    // Reload image, dictionary, and attachment data based on updated current IDs and updated current Paths global variables...
                    _previousInventoryIDs = "";
                    _previousFolderPathOrFileNames = "";
                    RefreshData();
                }
                // Check to see if any of the new files could not be resolved to an inventory ID...
                //bool refreshInterfaceNeeded = false;
                //foreach (string fullPath in fullPaths)
                //{
                //    if (_attachmentDictionary.ContainsKey(fullPath) &&
                //        _attachmentDictionary[fullPath] < 0)
                //    {
                //        if (destinationNode != null)
                //        {
                //            if (destinationNode.Tag.ToString().ToUpper().StartsWith("FOLDER"))
                //            {
                //                int inventoryID = int.Parse(destinationNode.Name);
                //                _attachmentDictionary[fullPath] = inventoryID;
                //                DataRow[] drs = _inventoryAttach.Select("virtual_path='" + fullPath.Trim() + "' AND inventory_id = -1");
                //                if (drs.Length > 0)
                //                {
                //                    drs[0]["inventory_id"] = inventoryID;
                //                }
                //                refreshInterfaceNeeded = true;
                //            }
                //            else if (destinationNode.Tag.ToString().ToUpper().StartsWith("ATTACHMENT") &&
                //                destinationNode.Parent != null &&
                //                destinationNode.Parent.Tag.ToString().ToUpper().StartsWith("FOLDER"))
                //            {
                //                int inventoryID = int.Parse(destinationNode.Parent.Name);
                //                _attachmentDictionary[fullPath] = inventoryID;
                //                DataRow[] drs = _inventoryAttach.Select("virtual_path='" + fullPath.Trim() + "' AND inventory_id = -1");
                //                if (drs.Length > 0)
                //                {
                //                    drs[0]["inventory_id"] = inventoryID;
                //                }
                //                refreshInterfaceNeeded = true;
                //            }
                //        }
                //    }
                //}

                //// Reload image, dictionary, and attachment data but only if needed...
                //if (refreshInterfaceNeeded)
                //{
                //    // Create a treeview of the files being attached to inventory...
                //    BuildTreeView();
                //    // Update the listview....
                //    BuildListView();
                //}

//                foreach (string fullPath in fullPaths)
//                {
//                    if (System.IO.File.Exists(fullPath))
//                    {
//                        System.IO.FileInfo fi = new System.IO.FileInfo(fullPath);
//                        int inventoryID = FindInventoryIDFromFileNameOrPath(fullPath);
//                        if (inventoryID > 0 &&
//                            !_attachmentDictionary.ContainsKey(fi.FullName))
//                        {
//                                            // Call the dataview to calculate the filepath of the attachments in the dictionary list...
//                DataSet dsFilePath = _sharedUtils.GetWebServiceData("inventory_attach_wizard_get_filepath", paramList, 0, 0);

//                // Process each of the entries in the dictionary list that has a valid inventory_id...
//                if (_inventoryAttach != null &&
//                    dsFilePath != null &&
//                    dsFilePath.Tables.Contains("inventory_attach_wizard_get_filepath") &&
//                    dsFilePath.Tables["inventory_attach_wizard_get_filepath"].Rows.Count > 0)
//                {
//                    DataTable inventoryAttachFilePath = dsFilePath.Tables["inventory_attach_wizard_get_filepath"];
//                }

//                            // Add the filepath and inventoryID to the attachment dictionary...
//                            _attachmentDictionary.Add(fi.FullName, inventoryID);
//                            // Add a new row to the _inventoryAttach table...
//                            string fullFileName = remoteFilePath["attach_filepath"].ToString() + fileName;
//                            // Add a new inventoryAttach record for this valid inventory_id...
//                            DataRow newInventoryAttachment = _inventoryAttach.NewRow();
//                            newInventoryAttachment["inventory_id"] = inventoryID;
//                            newInventoryAttachment["virtual_path"] = fullFileName;
//                            newInventoryAttachment["thumbnail_virtual_path"] = fullFileName.Insert(fullFileName.LastIndexOf('.'), "_thumbnail");
//                            //newInventoryAttachment["sort_order"] = 0;
//                            //newInventoryAttachment["title"] = 0;
//                            //newInventoryAttachment["description"] = 0;
//                            //newInventoryAttachment["content_type"] = "IMAGE";
//                            newInventoryAttachment["category_code"] = "IMAGE";
//                            //newInventoryAttachment["copyright_information"] = "";
//                            newInventoryAttachment["attach_cooperator_id"] = _sharedUtils.UserCooperatorID;
//                            newInventoryAttachment["is_web_visible"] = "Y";
//                            newInventoryAttachment["attach_date"] = DateTime.Now.Date;
//                            newInventoryAttachment["attach_date_code"] = "MM/dd/yyyy";
//                            //newInventoryAttachment["note"] = "";
//                            //newInventoryAttachment["image_file_physical_path"] = physicalPath;
//                            _inventoryAttach.Rows.Add(newInventoryAttachment);

//                        }
//                    }
//                }

//                TreeNode destinationNode = ((TreeView)sender).GetNodeAt(((TreeView)sender).PointToClient(new Point(e.X, e.Y)));
//                if (destinationNode == null) destinationNode = ((TreeView)sender).Nodes[0];

//                if (destinationNode != null)
//                {
//                    if (destinationNode.Tag.ToString().ToUpper().StartsWith("FOLDER"))
//                    {
//                        //inventoryID = destinationNode.Name;
//                    }
//                    else if (destinationNode.Tag.ToString().ToUpper().StartsWith("ATTACHMENT"))
//                    {
//                        // Get the inventory_id from the attachment record using the SE...
//                        DataSet ds = _sharedUtils.SearchWebService("@accession_inv_attach.accession_inv_attach_id = " + destinationNode.Name.ToString(), true, true, null, "inventory", 0, 0);

//                        if (ds.Tables.Contains("SearchResult") &&
//                            ds.Tables["SearchResult"].Rows.Count == 1)
//                        {
//                            //inventoryID = ds.Tables["SearchResult"].Rows[0]["ID"].ToString();
//                        }
//                    }
//                }
//                else
//                {
//                }
////// Process each subfolder in the collection...
////LoadAttachmentDictionary("");
////LoadImageList(_attachmentDictionary, _inventoryAttach);
////foreach (System.IO.DirectoryInfo di in subFolders)
////{
////    // Add all files in the subfolder to the dictionary...
////    AddAttachmentCollection(attachmentDictionary, di);
////}
            }

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void ux_listviewAttachments_ItemActivate(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;
            foreach (int i in lv.SelectedIndices)
            {
                string attachmentRemotePath = _inventoryAttach.DefaultView[i]["virtual_path"].ToString();
                if (_inventoryAttach.DefaultView[i]["category_code"].ToString().ToUpper() == "LINK")
                {
                    // This attachment is a link to data on another system so start the default
                    // browser with the URL virtual path...
                    System.Diagnostics.Process.Start(attachmentRemotePath);
                }
                else
                {
                    // This attachment is a file stored on a server that needs to be downloaded so
                    // download it now and display it in the application with this file extension support...
                    if (!Uri.IsWellFormedUriString(attachmentRemotePath, UriKind.Absolute)) attachmentRemotePath = "~/uploads/images/" + attachmentRemotePath;
                    //System.IO.FileInfo fi = new System.IO.FileInfo(attachmentRemotePath);
                    byte[] attachBytes = _sharedUtils.GetAttachment(attachmentRemotePath);
                    if (attachBytes != null && attachBytes.Length > 0)
                    {
                        string attachmentTempPath = System.IO.Path.GetTempFileName();  //System.Environment.GetFolderPath(Environment..SpecialFolder.InternetCache) + @"\GRIN-Global\Curator Tool";
                        System.IO.File.WriteAllBytes(attachmentTempPath, attachBytes);

                        if (System.IO.File.Exists(attachmentTempPath) &&
                            !string.IsNullOrEmpty(System.IO.Path.GetExtension(attachmentRemotePath)))
                        {
                            System.IO.File.Move(attachmentTempPath, attachmentTempPath.Replace(".tmp", System.IO.Path.GetExtension(attachmentRemotePath)));
                        }

                        System.Diagnostics.Process.Start(attachmentTempPath.Replace(".tmp", System.IO.Path.GetExtension(attachmentRemotePath)));
                    }
                }
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Options optionsDialog = new Options();
            optionsDialog.IncludeFilter = _includeFileFilter;
            optionsDialog.ExcludeFilter = _excludedFileFilter;
            if (DialogResult.OK == optionsDialog.ShowDialog())
            {
                _includeFileFilter = optionsDialog.IncludeFilter;
                _excludedFileFilter = optionsDialog.ExcludeFilter;
                _parseMethod = optionsDialog.ParseMethod;
                _useCaseSensitiveFileNames = optionsDialog.CaseSensitive;
                _delimiters = optionsDialog.Delimiters;
                _fixedFieldTokenLengths = optionsDialog.FixedFieldTokenLengths;
                _tokenPosition = optionsDialog.TokenPosition;
            }
        }
    }
}

