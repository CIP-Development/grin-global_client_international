﻿/*
Copyright (C) 2020 International Potato Centre (CIP)

This file is part of PrintWizard, developed by Carlos Velásquez.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation. You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

This Software and the work of the developers shall be expressly acknowledged in any modified or derivative product based on the Software.

This notice shall be included in all copies or substantial portions of the Software.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

International Potato Centre
Apartado 1558, Lima 12, Peru
cip@cgiar.org - www.cipotato.org
*/

using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CropTraitWizard
{
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    public partial class CropTraitWizard : Form, IGRINGlobalDataWizard
    {
        SharedUtils _sharedUtils;
        string _originalPKeys = string.Empty;
        string _cropTraitPKeys = string.Empty;

        BindingSource _cropTraitBindingSource;
        BindingSource _cropTraitLangBindingSource;
        BindingSource _cropTraitCodeBindingSource;
        BindingSource _cropTraitCodeLangBindingSource;
        BindingSource _cropTraitCodeAttachBindingSource;
        BindingSource _cropTraitAttachBindingSource;

        DataTable _dtCropTraitSearch;
        DataTable _dtCropTrait;
        DataTable _dtCropTraitLang;
        DataTable _dtCropTraitCode;
        DataTable _dtCropTraitCodeLang;
        DataTable _dtCropTraitCodeAttach;
        DataTable _dtCropTraitAttach;

        DataSet _changedRecords = new DataSet();
        char _lastDGVCharPressed;

        public string FormName
        {
            get
            {
                return "Trait Wizard";
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();
                if (_changedRecords.Tables.Contains(_dtCropTrait.TableName))
                {
                    dt = _changedRecords.Tables[_dtCropTrait.TableName].Copy();
                }
                return dt;
            }
        }

        public string PKeyName
        {
            get
            {
                return "crop_trait_id";
            }
        }

        public CropTraitWizard()
        {
            InitializeComponent();
        }

        public CropTraitWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();

            _cropTraitBindingSource = new BindingSource();
            //_cropTraitBindingSource.ListChanged += new ListChangedEventHandler(_mainBindingSource_ListChanged); //Commented by cv
            _cropTraitBindingSource.CurrentChanged += new EventHandler(_mainBindingSource_CurrentChanged);
            _cropTraitLangBindingSource = new BindingSource();
            _cropTraitCodeBindingSource = new BindingSource();
            _cropTraitCodeBindingSource.CurrentChanged += new EventHandler(_cropTraitCodeBindingSource_CurrentChanged);
            _cropTraitCodeLangBindingSource = new BindingSource();
            _cropTraitCodeAttachBindingSource = new BindingSource();
            _cropTraitAttachBindingSource = new BindingSource();

            _sharedUtils = sharedUtils;
            _originalPKeys = pKeys;

            foreach (string pkeyToken in pKeys.Split(';'))
            {
                switch (pkeyToken.Split('=')[0].Trim().ToUpper())
                {
                    case ":CROPTRAITID":
                        _cropTraitPKeys = pkeyToken;
                        break;
                }
            }
            if (string.IsNullOrEmpty(_cropTraitPKeys)) _cropTraitPKeys = ":croptraitid=";

            ux_datagridviewCropTraitLang.RowsDefaultCellStyle.WrapMode = DataGridViewTriState.True;
            ux_datagridviewCropTraitLang.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            ux_datagridviewCropTraitCode.RowsDefaultCellStyle.WrapMode = DataGridViewTriState.True;
            ux_datagridviewCropTraitCode.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            ux_datagridviewCropTraitCodeLang.RowsDefaultCellStyle.WrapMode = DataGridViewTriState.True;
            ux_datagridviewCropTraitCodeLang.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
        }

        private void CropTraitWizard_Load(object sender, EventArgs e)
        {
            // Get crop_trait_lang table for crop_trait searching
            DataSet dsSearch = _sharedUtils.GetWebServiceData("get_crop_trait_lang", ":croptraitlangid=", 0, 0);
            if (dsSearch.Tables.Contains("get_crop_trait_lang"))
            {
                _dtCropTraitSearch = dsSearch.Tables["get_crop_trait_lang"].Copy();
                _dtCropTraitSearch.Rows.Add(_dtCropTraitSearch.NewRow());
            }

            DataSet ds;
            // Get the crop_trait table and bind it to the main form on the General tabpage...
            ds = _sharedUtils.GetWebServiceData("get_crop_trait", _cropTraitPKeys, 0, 0);

            if (ds.Tables.Contains("get_crop_trait"))
            {
                _dtCropTrait = ds.Tables["get_crop_trait"].Copy();

                BuildCropTraitLangPage();
                BuildCropTraitCodePage();
                BuildCropTraitAttachPage();

                // Bind the bindingsource to the binding navigator toolstrip...
                _cropTraitBindingSource.DataSource = _dtCropTrait;
                ux_bindingnavigatorForm.BindingSource = _cropTraitBindingSource;
            }

            // Format the controls on this dialog...
            bindControls(this.Controls);
            formatControls(this.Controls);

            if (_cropTraitBindingSource.List.Count > 0)
            {
                ux_tabcontrolMain.Enabled = true;
            }
            else
            {
                ux_tabcontrolMain.Enabled = false;
            }
            /*
            if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);
            */
            // Force the row filters to be applied...
            _mainBindingSource_CurrentChanged(sender, e);

            if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);
        }

        void _mainBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            formatControls(this.Controls);
        }

        void _mainBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            if (_cropTraitBindingSource.List.Count > 0)
            {
                DataRowView drvCurrent = (DataRowView)_cropTraitBindingSource.Current;
                ux_tabcontrolMain.Enabled = true;
                // Update the row error message for this accession row...
                if (string.IsNullOrEmpty(((DataRowView)_cropTraitBindingSource.Current).Row.RowError))
                {
                    ux_textboxAccessionRowError.Visible = false;
                    ux_textboxAccessionRowError.Text = "";
                }
                else
                {
                    ux_textboxAccessionRowError.Visible = true;
                    ux_textboxAccessionRowError.ReadOnly = false;
                    ux_textboxAccessionRowError.Enabled = true;
                    ux_textboxAccessionRowError.Text = ((DataRowView)_cropTraitBindingSource.Current).Row.RowError;
                }

                // Update the Coded Name on the Navigator Bar...
                bindingNavigatorCropCodeName.Text = ((DataRowView)_cropTraitBindingSource.Current).Row["coded_name"].ToString();
                // Update the Crop on the Navigator Bar...
                bindingNavigatorCrop.Text = ux_textboxCropId.Text;
                // Update the Crop trait Id on the Navigator Bar...
                bindingNavigatorCropTraitId.Text = drvCurrent.Row["crop_trait_id"].ToString();

                // Change the row filter for the child tables to reflect the new CropTrait's primary key...
                string pkey = ((DataRowView)_cropTraitBindingSource.Current)[_dtCropTrait.PrimaryKey[0].ColumnName].ToString();
                if (_dtCropTraitLang != null && !string.IsNullOrEmpty(pkey) && _dtCropTraitLang.Columns.Contains("crop_trait_id")) _dtCropTraitLang.DefaultView.RowFilter = "crop_trait_id=" + pkey.Trim().ToLower();
                if (_dtCropTraitCode != null && !string.IsNullOrEmpty(pkey) && _dtCropTraitCode.Columns.Contains("crop_trait_id")) _dtCropTraitCode.DefaultView.RowFilter = "crop_trait_id=" + pkey.Trim().ToLower();
                if (_dtCropTraitAttach != null && !string.IsNullOrEmpty(pkey) && _dtCropTraitAttach.Columns.Contains("crop_trait_id")) _dtCropTraitAttach.DefaultView.RowFilter = "crop_trait_id=" + pkey.Trim().ToLower();

            }
            else
            {
                ux_tabcontrolMain.Enabled = false;
                bindingNavigatorCropCodeName.Text = string.Empty;
                bindingNavigatorCrop.Text = string.Empty;
                bindingNavigatorCropTraitId.Text = string.Empty;
            }
        }

        #region Tab Control Logic...
        private void ux_tabcontrolMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ux_tabcontrolMain.SelectedIndex == 0)
            {
                bindingNavigatorAddNewItem.Enabled = true;
                bindingNavigatorDeleteItem.Enabled = true;
            }
            else
            {
                bindingNavigatorAddNewItem.Enabled = false;
                bindingNavigatorDeleteItem.Enabled = false;
                foreach (DataRowView drv in _cropTraitBindingSource.List)
                {
                    if (drv.IsEdit ||
                        drv.Row.RowState == DataRowState.Added ||
                        drv.Row.RowState == DataRowState.Deleted ||
                        drv.Row.RowState == DataRowState.Detached ||
                        drv.Row.RowState == DataRowState.Modified)
                    {
                        drv.EndEdit();
                    }
                }
            }
        }
        #endregion

        #region Dynamic Controls logic...
        private void formatControls(Control.ControlCollection controlCollection)
        {
            foreach (Control ctrl in controlCollection)
            {
                if (ctrl != ux_bindingnavigatorForm)  // Leave the bindingnavigator alone
                {
                    // If the ctrl has children - set their edit mode too...
                    if (ctrl.Controls.Count > 0)
                    {
                        formatControls(ctrl.Controls);
                    }
                    // Set the edit mode for the control...
                    if (ctrl != null &&
                        ctrl.Tag != null &&
                        ctrl.Tag is string &&
                        _cropTraitBindingSource != null &&
                        _cropTraitBindingSource.DataSource is DataTable &&
                        ((DataTable)_cropTraitBindingSource.DataSource).Columns.Contains(ctrl.Tag.ToString().Trim().ToLower()))
                    {
                        // If the field is a ReadOnly field do not allow the tab key to be used to navigate to it...
                        ctrl.TabStop = !((DataTable)_cropTraitBindingSource.DataSource).Columns[ctrl.Tag.ToString().Trim().ToLower()].ReadOnly;
                        // Set the control's edit properties based on what type of control it is, what edit mode is current, and if the field is readonly...
                        if (ctrl is TextBox)
                        {
                            // TextBoxes have a ReadOnly property in addition to an Enabled property so we handle this one separate...
                            ((TextBox)ctrl).ReadOnly = ((DataTable)_cropTraitBindingSource.DataSource).Columns[ctrl.Tag.ToString().Trim().ToLower()].ReadOnly;
                        }
                        else if (ctrl is Label)
                        {
                            // Do nothing to the Label
                        }
                        else
                        {
                            // All other control types (ComboBox, CheckBox, DateTimePicker) except Labels...
                            ctrl.Enabled = !((DataTable)_cropTraitBindingSource.DataSource).Columns[ctrl.Tag.ToString().Trim().ToLower()].ReadOnly;
                        }
                    }
                }
            }
        }

        private void bindControls(Control.ControlCollection controlCollection)
        {
            foreach (Control ctrl in controlCollection)
            {
                if (ctrl != ux_bindingnavigatorForm)  // Leave the bindingnavigator alone
                {
                    // If the ctrl has children - bind them too...
                    if (ctrl.Controls.Count > 0)
                    {
                        bindControls(ctrl.Controls);
                    }
                    // Bind the control (by type)...
                    if (ctrl is ComboBox) bindComboBox((ComboBox)ctrl, _cropTraitBindingSource);
                    if (ctrl is TextBox) bindTextBox((TextBox)ctrl, _cropTraitBindingSource);
                    if (ctrl is CheckBox) bindCheckBox((CheckBox)ctrl, _cropTraitBindingSource);
                    if (ctrl is DateTimePicker) bindDateTimePicker((DateTimePicker)ctrl, _cropTraitBindingSource);
                    if (ctrl is Label) bindLabel((Label)ctrl, _cropTraitBindingSource);
                }
            }
        }

        private void bindComboBox(ComboBox comboBox, BindingSource bindingSource)
        {
            comboBox.DataBindings.Clear();
            comboBox.Enabled = false;
            if (comboBox != null &&
                comboBox.Tag != null &&
                comboBox.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(comboBox.Tag.ToString().Trim().ToLower()))
            {
                if (_sharedUtils != null)
                {
                    DataColumn dc = ((DataTable)bindingSource.DataSource).Columns[comboBox.Tag.ToString().Trim().ToLower()];
                    _sharedUtils.BindComboboxToCodeValue(comboBox, dc);
                    if (comboBox.DataSource.GetType() == typeof(DataTable))
                    {
                        // Calculate the maximum width needed for displaying the dropdown items and set the combobox property...
                        int maxWidth = comboBox.DropDownWidth;
                        foreach (DataRow dr in ((DataTable)comboBox.DataSource).Rows)
                        {
                            if (TextRenderer.MeasureText(dr["display_member"].ToString().Trim(), comboBox.Font).Width > maxWidth)
                            {
                                maxWidth = TextRenderer.MeasureText(dr["display_member"].ToString().Trim(), comboBox.Font).Width;
                            }
                        }
                        comboBox.DropDownWidth = maxWidth;
                    }

                    // Bind the SelectedValue property to the binding source...
                    comboBox.DataBindings.Add("SelectedValue", bindingSource, comboBox.Tag.ToString().Trim().ToLower(), true, DataSourceUpdateMode.OnPropertyChanged);

                    // Wire up to an event handler if this column is a date_code (format) field...
                    if (dc.ColumnName.Trim().ToLower().EndsWith("_code") &&
                        dc.Table.Columns.Contains(dc.ColumnName.Trim().ToLower().Substring(0, dc.ColumnName.Trim().ToLower().LastIndexOf("_code"))))
                    {
                        comboBox.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
                    }
                }
                else
                {
                    // Bind the Text property to the binding source...
                    comboBox.DataBindings.Add("Text", bindingSource, comboBox.Tag.ToString().Trim().ToLower(), true, DataSourceUpdateMode.OnPropertyChanged);
                }
            }
        }

        private void bindTextBox(TextBox textBox, BindingSource bindingSource)
        {
            textBox.DataBindings.Clear();
            textBox.ReadOnly = true;
            if (textBox != null &&
                textBox.Tag != null &&
                textBox.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(textBox.Tag.ToString().Trim().ToLower()))
            {
                DataTable dt = (DataTable)bindingSource.DataSource;
                DataColumn dc = dt.Columns[textBox.Tag.ToString().Trim().ToLower()];
                if (_sharedUtils.LookupTablesIsValidFKField(dc))
                {
                    // Create a new binding that handles display_member/value_member conversions...
                    Binding textBinding = new Binding("Text", bindingSource, textBox.Tag.ToString().Trim().ToLower());
                    textBinding.Format += new ConvertEventHandler(textLUBinding_Format);
                    textBinding.Parse += new ConvertEventHandler(textLUBinding_Parse);
                    textBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                    // Bind it to the textbox...
                    textBox.DataBindings.Add(textBinding);
                }
                else if (dc.DataType == typeof(DateTime))
                {
                    // Create a new binding that handles display_member/value_member conversions...
                    Binding textBinding = new Binding("Text", bindingSource, textBox.Tag.ToString().Trim().ToLower());
                    textBinding.Format += new ConvertEventHandler(textDateTimeBinding_Format);
                    textBinding.Parse += new ConvertEventHandler(textDateTimeBinding_Parse);
                    textBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                    // Bind it to the textbox...
                    textBox.DataBindings.Add(textBinding);
                }
                else
                {
                    // Bind to a plain-old text field in the database (no LU required)...
                    textBox.DataBindings.Add("Text", bindingSource, textBox.Tag.ToString().Trim().ToLower());
                }

                // Add an event handler for processing the first key press (to display the lookup picker dialog)...
                textBox.KeyDown += new KeyEventHandler(textBox_KeyDown);
                textBox.KeyPress += new KeyPressEventHandler(textBox_KeyPress);
            }
        }

        private void bindCheckBox(CheckBox checkBox, BindingSource bindingSource)
        {
            checkBox.DataBindings.Clear();
            checkBox.Enabled = false;
            if (checkBox != null &&
                checkBox.Tag != null &&
                checkBox.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(checkBox.Tag.ToString().Trim().ToLower()))
            {
                DataTable dt = (DataTable)bindingSource.DataSource;
                DataColumn dc = dt.Columns[checkBox.Tag.ToString().Trim().ToLower()];
                checkBox.Text = dc.Caption;
                Binding boolBinding = new Binding("Checked", bindingSource, checkBox.Tag.ToString().Trim().ToLower());
                boolBinding.Format += new ConvertEventHandler(boolBinding_Format);
                boolBinding.Parse += new ConvertEventHandler(boolBinding_Parse);
                boolBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                checkBox.DataBindings.Add(boolBinding);
            }
        }

        private void bindDateTimePicker(DateTimePicker dateTimePicker, BindingSource bindingSource)
        {
            dateTimePicker.DataBindings.Clear();
            dateTimePicker.Enabled = false;
            if (dateTimePicker != null &&
                dateTimePicker.Tag != null &&
                dateTimePicker.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(dateTimePicker.Tag.ToString().Trim().ToLower()))
            {
                // Now bind the control to the column in the bindingSource...
                dateTimePicker.DataBindings.Add("Text", bindingSource, dateTimePicker.Tag.ToString().Trim().ToLower(), true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        private void bindLabel(Label label, BindingSource bindingSource)
        {
            if (label != null &&
                label.Tag != null &&
                label.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(label.Tag.ToString().Trim().ToLower()))
            {
                label.Text = ((DataTable)bindingSource.DataSource).Columns[label.Tag.ToString().Trim().ToLower()].Caption;
            }
        }

        void boolBinding_Format(object sender, ConvertEventArgs e)
        {
            switch (e.Value.ToString().ToUpper())
            {
                case "Y":
                    e.Value = true;
                    break;
                case "N":
                    e.Value = false;
                    break;
                default:
                    e.Value = false;
                    break;
            }
        }

        void boolBinding_Parse(object sender, ConvertEventArgs e)
        {
            if (e.Value != null)
            {
                switch ((bool)e.Value)
                {
                    case true:
                        e.Value = "Y";
                        break;
                    case false:
                        e.Value = "N";
                        break;
                    default:
                        e.Value = "N";
                        break;
                }
            }
            else
            {
                e.Value = "N";
            }
        }

        void textDateTimeBinding_Format(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (dc.DataType == typeof(DateTime) &&
                !string.IsNullOrEmpty(e.Value.ToString()) &&
                dt.Columns.Contains(dc.ColumnName.Trim().ToLower() + "_code"))
            {
                DataRowView drv = (DataRowView)((BindingSource)b.DataSource).Current;
                string dateFormat = "MM/dd/yyyy";
                dateFormat = drv[dc.ColumnName + "_code"].ToString().Trim();
                e.Value = ((DateTime)e.Value).ToString(dateFormat);
            }
        }

        void textDateTimeBinding_Parse(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (dc.DataType == typeof(DateTime) &&
                !string.IsNullOrEmpty(e.Value.ToString()) &&
                dt.Columns.Contains(dc.ColumnName.Trim().ToLower() + "_code"))
            {
                DataRowView drv = (DataRowView)((BindingSource)b.DataSource).Current;
                string dateFormat = drv[dc.ColumnName + "_code"].ToString().Trim();
                DateTime parsedDateTime;
                if (DateTime.TryParseExact(e.Value.ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out parsedDateTime))
                {
                    e.Value = parsedDateTime;
                }
            }
        }

        void textLUBinding_Format(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (!string.IsNullOrEmpty(e.Value.ToString()))
            {
                e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());
            }
        }

        void textLUBinding_Parse(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (!string.IsNullOrEmpty(e.Value.ToString()))
            {
                e.Value = _sharedUtils.GetLookupValueMember(((DataRowView)((BindingSource)b.DataSource).Current).Row, dc.ExtendedProperties["foreign_key_dataview_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());
            }
        }

        void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            string dateColumnName = cb.Tag.ToString().Replace("_code", "");
            foreach (Binding b in cb.DataBindings)
            {
                foreach (Control ctrl in cb.Parent.Controls)
                {
                    if (ctrl.Tag.ToString() == dateColumnName &&
                        ctrl.GetType() == typeof(TextBox))
                    {
                        DataRowView drv = (DataRowView)((BindingSource)b.DataSource).Current;
                        string dateFormat = "MM/dd/yyyy";
                        dateFormat = drv[cb.Tag.ToString()].ToString().Trim();
                        DateTime dt;
                        if (DateTime.TryParse(drv[dateColumnName].ToString(), null, System.Globalization.DateTimeStyles.AssumeLocal, out dt))
                        {
                            ctrl.Text = ((DateTime)drv[dateColumnName]).ToString(dateFormat);
                        }
                    }
                }
            }
        }

        void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;

            if (!tb.ReadOnly)
            {
                foreach (Binding b in tb.DataBindings)
                {
                    if (b.BindingManagerBase != null &&
                        b.BindingManagerBase.Current != null &&
                        b.BindingManagerBase.Current is DataRowView &&
                        b.BindingMemberInfo.BindingField != null)
                    {
                        if (_sharedUtils.LookupTablesIsValidFKField(((DataRowView)b.BindingManagerBase.Current).Row.Table.Columns[b.BindingMemberInfo.BindingField]) &&
                            e.KeyChar != Convert.ToChar(Keys.Escape)) // Ignore the Escape key and process anything else...
                        {
                            string filterText = tb.Text;
                            if (System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), "[a-zA-Z0-9]"))
                            {
                                filterText = e.KeyChar.ToString();
                            }
                            LookupTablePicker ltp = new LookupTablePicker(_sharedUtils, tb.Tag.ToString(), ((DataRowView)b.BindingManagerBase.Current).Row, filterText);
                            //LookupTablePicker ltp = new LookupTablePicker(_sharedUtils, tb.Tag.ToString(), ((DataRowView)b.BindingManagerBase.Current).Row, tb.Text);
                            ltp.StartPosition = FormStartPosition.CenterParent;
                            if (DialogResult.OK == ltp.ShowDialog())
                            {
                                tb.Text = ltp.NewValue.Trim();
                            }
                            e.Handled = true;
                        }
                    }
                }
            }
        }

        void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) // Process the Delete key (since it is not passed on to the KeyPress event handler)...
            {
                TextBox tb = (TextBox)sender;

                if (!tb.ReadOnly)
                {

                    foreach (Binding b in tb.DataBindings)
                    {
                        if (b.BindingManagerBase != null &&
                            b.BindingManagerBase.Current != null &&
                            b.BindingManagerBase.Current is DataRowView &&
                            b.BindingMemberInfo.BindingField != null)
                        {
                            // Just in case the user selected only a part of the full text to delete - strip out the selected text and process normally...
                            string remainingText = tb.Text.Remove(tb.SelectionStart, tb.SelectionLength);
                            if (string.IsNullOrEmpty(remainingText))
                            {
                                // When a textbox is bound to a table - some datatypes will not revert to a DBNull via the bound control - so
                                // take control of the update and force the field back to a null (non-nullable fields should show up to the GUI with colored background)...
                                ((DataRowView)b.BindingManagerBase.Current).Row[b.BindingMemberInfo.BindingField] = DBNull.Value;
                                b.ReadValue();
                                e.Handled = true;
                            }
                            else
                            {
                                if (_sharedUtils.LookupTablesIsValidFKField(((DataRowView)b.BindingManagerBase.Current).Row.Table.Columns[b.BindingMemberInfo.BindingField]))
                                {
                                    LookupTablePicker ltp = new LookupTablePicker(_sharedUtils, tb.Tag.ToString(), ((DataRowView)b.BindingManagerBase.Current).Row, remainingText);
                                    ltp.StartPosition = FormStartPosition.CenterParent;
                                    if (DialogResult.OK == ltp.ShowDialog())
                                    {
                                        tb.Text = ltp.NewValue.Trim();
                                        b.WriteValue();
                                        e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region DGV control logic...
        private void ux_datagridview_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            // Get the defaultview of the datatable used by the dgv...
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }

            // Begin processing cell formatting based on the datatype of the field...
            if (dv != null &&
                e.ColumnIndex < dv.Table.Columns.Count && //cv
                e.ColumnIndex > -1 &&
                e.RowIndex > -1 &&
                e.RowIndex < dv.Count)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                // Format FKey fields...
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    e.RowIndex < dv.Count &&
                    dv[e.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (dv[e.RowIndex][e.ColumnIndex] != DBNull.Value)
                    {
                        e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim(), dv[e.RowIndex][e.ColumnIndex].ToString().Trim(), "", dv[e.RowIndex][e.ColumnIndex].ToString().Trim());
                    }
                    dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    e.FormattingApplied = true;
                }
                // Format date/time fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        e.Value = ((DateTime)dv[e.RowIndex][e.ColumnIndex]).ToString(dateFormat);
                        e.FormattingApplied = true;
                    }
                }
                // Format integer fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    (dc.DataType == typeof(int) ||
                    dc.DataType == typeof(Int16) ||
                    dc.DataType == typeof(Int32) ||
                    dc.DataType == typeof(Int64)))
                {
                    int junk;
                    if (!int.TryParse(e.Value.ToString(), out junk))
                    {
                        dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    }
                }

                if (dc.ReadOnly)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }

                if (dc.ExtendedProperties.Contains("is_nullable") &&
                    dc.ExtendedProperties["is_nullable"].ToString() == "N" &&
                    string.IsNullOrEmpty(dv[e.RowIndex][e.ColumnIndex].ToString()))
                {
                    e.CellStyle.BackColor = Color.Plum;
                }
            }
        }

        private void ux_datagridview_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }
            if (dv != null && e.ColumnIndex > -1
                && e.ColumnIndex < dv.Table.Columns.Count) //cv
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        DateTime formattedDate;
                        if (DateTime.TryParseExact(e.Value.ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out formattedDate))
                        {
                            e.Value = formattedDate;
                            e.ParsingApplied = true;
                        }
                    }
                }
            }
        }

        private void ux_datagridview_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            string errorMessage = e.Exception.Message;
            int columnWithError = -1;

            // Find the cell the error belongs to (don't use e.ColumnIndex because it points to the current cell *NOT* the offending cell)...
            foreach (DataGridViewColumn col in dgv.Columns)
            {
                if (errorMessage.Contains(col.Name))
                {
                    dgv[col.Name, e.RowIndex].ErrorText = errorMessage;
                    columnWithError = col.Index;
                }
            }
        }

        private void ux_datagridview_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataTable dt = (DataTable)((BindingSource)dgv.DataSource).DataSource;
            string columnName = dgv.CurrentCell.OwningColumn.Name;
            DataColumn dc = dt.Columns[columnName];
            DataRow dr;

            if (dc == null) //cv  dgv.CurrentCell.OwningColumn.Index >= dt.Columns.Count
            {
                if (dgv.CurrentCell.OwningColumn.Tag != null && dgv.CurrentCell.OwningColumn.Tag.ToString().ToUpperInvariant().Equals("FILE_PATH"))
                {
                    OpenFileDialog ofd = new OpenFileDialog();
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        dgv.CurrentCell.Value = ofd.FileName;
                        dgv.CurrentCell.OwningRow.Cells["virtual_path"].Value = Path.GetFileName(ofd.FileName);
                        dgv.CurrentCell.OwningRow.Cells["title"].Value = Path.GetFileNameWithoutExtension(ofd.FileName);
                    }
                    dgv.EndEdit();
                }
            }
            else if (_sharedUtils.LookupTablesIsValidFKField(dc))
            {
                //string luTableName = dc.ExtendedProperties["foreign_key_resultset_name"].ToString().Trim();
                string luTableName = dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim();
                dr = ((DataRowView)dgv.CurrentRow.DataBoundItem).Row;
                string suggestedFilter = dgv.CurrentCell.EditedFormattedValue.ToString();
                if (_lastDGVCharPressed > 0) suggestedFilter = _lastDGVCharPressed.ToString();
                GRINGlobal.Client.Common.LookupTablePicker ltp = new GRINGlobal.Client.Common.LookupTablePicker(_sharedUtils, columnName, dr, suggestedFilter);
                ltp.StartPosition = FormStartPosition.CenterParent;
                if (DialogResult.OK == ltp.ShowDialog())
                {
                    if (dr != null)
                    {
                        if (ltp.NewKey != null && dr[dgv.CurrentCell.ColumnIndex].ToString().Trim() != ltp.NewKey.Trim())
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = ltp.NewKey.Trim();
                            dgv.CurrentCell.Value = ltp.NewValue.Trim();
                        }
                        else if (ltp.NewKey == null)
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = DBNull.Value;
                            dgv.CurrentCell.Value = "";
                        }
                        dr.SetColumnError(dgv.CurrentCell.ColumnIndex, null);
                    }
                }
                dgv.EndEdit();
            }
        }

        private void ux_datagridview_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            // Reset the last char pressed global variable...
            _lastDGVCharPressed = (char)0;

            // Remember the keypress so that it can be passed into the Lookup Picker dialog...
            if (!e.Alt && !e.Control)
            {
                KeysConverter kc = new KeysConverter();
                string lastChar = kc.ConvertToString(e.KeyValue).Replace("NumPad", "");
                //string lastChar = Convert.ToChar(e.KeyCode).ToString();
                //string lastChar = e.KeyCode.ToString();
                if (lastChar.Length == 1)
                {
                    if (e.Shift)
                    {
                        _lastDGVCharPressed = lastChar.ToUpper()[0];
                    }
                    else
                    {
                        _lastDGVCharPressed = lastChar.ToLower()[0];
                    }
                }
            }
        }

        private void ux_datagridview_KeyDown(object sender, KeyEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }

            if (e.KeyCode == Keys.Delete && dgv.CurrentCell.OwningColumn.Index >= dv.Table.Columns.Count
                && dgv.SelectedRows.Count == 0) //cv For additional columns
            {
                // Change cursor to the wait cursor...
                Cursor origCursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;

                if (dgv.SelectedRows.Count == 0)
                {
                    // The user is deleting values from individual selected cells (not entire rows)...
                    foreach (DataGridViewCell dgvc in dgv.SelectedCells)
                    {
                        dgvc.Value = "";
                        dgv.UpdateCellValue(dgvc.ColumnIndex, dgvc.RowIndex);
                        //dgv[dgvc.ColumnIndex, dgvc.RowIndex].Style.BackColor = Color.Yellow;
                        //RefreshDGVRowFormatting(dgvc.OwningRow, ux_checkboxHighlightChanges.Checked);
                    }
                }

                // Restore cursor to default cursor...
                Cursor.Current = origCursor;

                RefreshDGVFormatting(dgv);
                return;
            }

            if (_sharedUtils.ProcessDGVEditShortcutKeys(dgv, e, _sharedUtils.UserCooperatorID))
            {
                RefreshDGVFormatting(dgv);
            }
        }

        private void RefreshDGVFormatting(DataGridView dgv)
        {
            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                RefreshDGVRowFormatting(dgvr);
            }

            // Show SortGlyphs for the column headers (this takes two steps)...
            // First reset them all to No Sort...
            foreach (DataGridViewColumn dgvc in dgv.Columns)
            {
                dgvc.HeaderCell.SortGlyphDirection = SortOrder.None;
            }

            // Now inspect the sort string from the datatable in use to set the SortGlyphs...
            string strOrder = "";
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                strOrder = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView.Sort;
            }
            else
            {
                strOrder = ((DataTable)dgv.DataSource).DefaultView.Sort;
            }
            char[] chararrDelimiters = { ',' };
            string[] strarrSortCols = strOrder.Split(chararrDelimiters);
            foreach (string strSortCol in strarrSortCols)
            {
                if (strSortCol.Contains("ASC"))
                {
                    if (dgv.Columns.Contains(strSortCol.Replace(" ASC", "").Replace("_sortable", "").Trim())) dgv.Columns[strSortCol.Replace(" ASC", "").Replace("_sortable", "").Trim()].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                }
                if (strSortCol.Contains("DESC"))
                {
                    if (dgv.Columns.Contains(strSortCol.Replace(" DESC", "").Replace("_sortable", "").Trim())) dgv.Columns[strSortCol.Replace(" DESC", "").Replace("_sortable", "").Trim()].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                }
            }
        }

        private void RefreshDGVRowFormatting(DataGridViewRow dgvr)
        {
            foreach (DataGridViewCell dgvc in dgvr.Cells)
            {
                // Reset the background and foreground color...
                dgvc.Style.BackColor = Color.Empty;
                dgvc.Style.ForeColor = Color.Empty;
                dgvc.Style.SelectionBackColor = Color.Empty;
                dgvc.Style.SelectionForeColor = Color.Empty;
            }
            // If the row has changes make each changed cell yellow...
            DataRow dr = ((DataRowView)dgvr.DataBoundItem).Row;
            if (dr.RowState == DataRowState.Modified)
            {
                foreach (DataGridViewCell dgvc in dgvr.Cells)
                {
                    if (dgvc.OwningColumn.Index >= dr.ItemArray.Count()) //cv skip additional columns
                        continue;

                    string dcName = dgvc.OwningColumn.Name;
                    // If the cell has been changed make it yellow...
                    if (dr[dcName, DataRowVersion.Original].ToString().Trim() != dr[dcName, DataRowVersion.Current].ToString().Trim())
                    {
                        dgvc.Style.BackColor = Color.Yellow;
                        dr.SetColumnError(dcName, null);
                    }
                    // Use default background color for this cell...
                    else
                    {
                        dgvc.Style.BackColor = Color.Empty;
                    }
                }
            }
        }
        #endregion

        #region CropTraitLang

        private void BuildCropTraitLangPage()
        {
            DataSet ds;
            // Get the crop_trait_lang table and bind it to the DGV on the Names tabpage...
            ds = _sharedUtils.GetWebServiceData("get_crop_trait_lang", _cropTraitPKeys, 0, 0);
            if (ds.Tables.Contains("get_crop_trait_lang"))
            {
                // Copy the crop_trait_lang table to a private variable...
                _dtCropTraitLang = ds.Tables["get_crop_trait_lang"].Copy();
                // Bind the DGV to the binding source...
                ux_datagridviewCropTraitLang.DataSource = _cropTraitLangBindingSource;
                // Build the DGV using the new table and bind it to the DGV's binding source (this happens in the Build method)...
                _sharedUtils.BuildEditDataGridView(ux_datagridviewCropTraitLang, _dtCropTraitLang);

                // Order and display the columns the way the user wants...
                int i = 0;
#if DEBUG
                if (ux_datagridviewCropTraitLang.Columns.Contains("crop_trait_lang_id")) ux_datagridviewCropTraitLang.Columns["crop_trait_lang_id"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitLang.Columns.Contains("crop_trait_id")) ux_datagridviewCropTraitLang.Columns["crop_trait_id"].DisplayIndex = i++;
#endif
                if (ux_datagridviewCropTraitLang.Columns.Contains("sys_lang_id")) ux_datagridviewCropTraitLang.Columns["sys_lang_id"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitLang.Columns.Contains("title")) ux_datagridviewCropTraitLang.Columns["title"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitLang.Columns.Contains("description"))
                {
                    ux_datagridviewCropTraitLang.Columns["description"].DisplayIndex = i++;
                    ux_datagridviewCropTraitLang.Columns["description"].Width = 300;
                }

                foreach (DataGridViewColumn dgvc in ux_datagridviewCropTraitLang.Columns)
                {
                    dgvc.SortMode = DataGridViewColumnSortMode.Automatic;
                    // Hide any columns not explicitly ordered in the above code...
                    if (dgvc.DisplayIndex >= i) dgvc.Visible = false;
                }
            }
            else
            {
                _dtCropTraitLang = new DataTable();
            }
        }

        private void ux_buttonNewCropTraitLangRow_Click(object sender, EventArgs e)
        {
            string pkey = ((DataRowView)_cropTraitBindingSource.Current)[_dtCropTrait.PrimaryKey[0].ColumnName].ToString();
            DataRow newCropTraitLang = _dtCropTraitLang.NewRow();
            newCropTraitLang["crop_trait_id"] = pkey;

            _dtCropTraitLang.Rows.Add(newCropTraitLang);
            int newRowIndex = ux_datagridviewCropTraitLang.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
            int newColIndex = ux_datagridviewCropTraitLang.Columns.GetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;
            for (int i = 0; i < ux_datagridviewCropTraitLang.Rows.Count; i++)
            {
                if (ux_datagridviewCropTraitLang["crop_trait_lang_id", i].Value.Equals(newCropTraitLang["crop_trait_lang_id"])) newRowIndex = i;
            }
            foreach (DataGridViewColumn dgvc in ux_datagridviewCropTraitLang.Columns)
            {
                if (dgvc.DisplayIndex == 0)
                {
                    newColIndex = dgvc.Index;
                    break;
                }
            }
            ux_datagridviewCropTraitLang.CurrentCell = ux_datagridviewCropTraitLang[newColIndex, newRowIndex];
        }

        #endregion

        #region CropTraitCodeTab

        private void BuildCropTraitCodePage()
        {
            DataSet ds;
            // Get the crop_trait_code table...
            ds = _sharedUtils.GetWebServiceData("get_crop_trait_code", _cropTraitPKeys, 0, 0);
            if (ds.Tables.Contains("get_crop_trait_code"))
            {
                // Copy the crop_trait_code table to a private variable...
                _dtCropTraitCode = ds.Tables["get_crop_trait_code"].Copy();
                // Bind the DGV to the binding source...
                ux_datagridviewCropTraitCode.DataSource = _cropTraitCodeBindingSource;
                // Build the DGV using the new table and bind it to the DGV's binding source (this happens in the Build method)...
                _sharedUtils.BuildEditDataGridView(ux_datagridviewCropTraitCode, _dtCropTraitCode);

                // Order and display the columns the way the user wants...
                int i = 0;
#if DEBUG
                if (ux_datagridviewCropTraitCode.Columns.Contains("crop_trait_code_id")) ux_datagridviewCropTraitCode.Columns["crop_trait_code_id"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCode.Columns.Contains("crop_trait_id")) ux_datagridviewCropTraitCode.Columns["crop_trait_id"].DisplayIndex = i++;
#endif
                if (ux_datagridviewCropTraitCode.Columns.Contains("code")) ux_datagridviewCropTraitCode.Columns["code"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCode.Columns.Contains("title")) ux_datagridviewCropTraitCode.Columns["title"].DisplayIndex = i++;

                foreach (DataGridViewColumn dgvc in ux_datagridviewCropTraitCode.Columns)
                {
                    dgvc.SortMode = DataGridViewColumnSortMode.Automatic;
                    // Hide any columns not explicitly ordered in the above code...
                    if (dgvc.DisplayIndex >= i) dgvc.Visible = false;
                }
            }
            else
            {
                _dtCropTraitCode = new DataTable();
            }

            // Get the crop_trait_code_lang table...
            ds = _sharedUtils.GetWebServiceData("get_crop_trait_code_lang", _cropTraitPKeys, 0, 0);
            if (ds.Tables.Contains("get_crop_trait_code_lang"))
            {
                // Copy the crop_trait_code_lang table to a private variable...
                _dtCropTraitCodeLang = ds.Tables["get_crop_trait_code_lang"].Copy();
                // Bind the DGV to the binding source...
                ux_datagridviewCropTraitCodeLang.DataSource = _cropTraitCodeLangBindingSource;
                // Build the DGV using the new table and bind it to the DGV's binding source (this happens in the Build method)...
                _sharedUtils.BuildEditDataGridView(ux_datagridviewCropTraitCodeLang, _dtCropTraitCodeLang);

                // Order and display the columns the way the user wants...
                int i = 0;
#if DEBUG
                if (ux_datagridviewCropTraitCodeLang.Columns.Contains("crop_trait_code_lang_id")) ux_datagridviewCropTraitCodeLang.Columns["crop_trait_code_lang_id"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeLang.Columns.Contains("crop_trait_code_id")) ux_datagridviewCropTraitCodeLang.Columns["crop_trait_code_id"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeLang.Columns.Contains("crop_trait_id")) ux_datagridviewCropTraitCodeLang.Columns["crop_trait_id"].DisplayIndex = i++;
#endif
                if (ux_datagridviewCropTraitCodeLang.Columns.Contains("sys_lang_id")) ux_datagridviewCropTraitCodeLang.Columns["sys_lang_id"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeLang.Columns.Contains("title")) ux_datagridviewCropTraitCodeLang.Columns["title"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeLang.Columns.Contains("description")) ux_datagridviewCropTraitCodeLang.Columns["description"].DisplayIndex = i++;

                foreach (DataGridViewColumn dgvc in ux_datagridviewCropTraitCodeLang.Columns)
                {
                    dgvc.SortMode = DataGridViewColumnSortMode.Automatic;
                    // Hide any columns not explicitly ordered in the above code...
                    if (dgvc.DisplayIndex >= i) dgvc.Visible = false;
                }
            }
            else
            {
                _dtCropTraitCodeLang = new DataTable();
            }

            // Get the crop_trait_code_attach table...
            ds = _sharedUtils.GetWebServiceData("get_crop_trait_code_attach", _cropTraitPKeys, 0, 0);
            if (ds.Tables.Contains("get_crop_trait_code_attach"))
            {
                // Copy the crop_trait_code_attach table to a private variable...
                _dtCropTraitCodeAttach = ds.Tables["get_crop_trait_code_attach"].Copy();
                // Bind the DGV to the binding source...
                ux_datagridviewCropTraitCodeAttach.DataSource = _cropTraitCodeAttachBindingSource;
                // Build the DGV using the new table and bind it to the DGV's binding source (this happens in the Build method)...
                _sharedUtils.BuildEditDataGridView(ux_datagridviewCropTraitCodeAttach, _dtCropTraitCodeAttach);

                // Order and display the columns the way the user wants...
                int i = 0;

                ux_datagridviewCropTraitCodeAttach.Columns.Add(new DataGridViewColumn()
                {
                    HeaderText = "Source File Path",
                    Name = "source_file_path",
                    CellTemplate = new DataGridViewTextBoxCell(),
                    Tag = "FILE_PATH"
                });
#if DEBUG
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("crop_trait_code_attach_id")) ux_datagridviewCropTraitCodeAttach.Columns["crop_trait_code_attach_id"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("crop_trait_code_id")) ux_datagridviewCropTraitCodeAttach.Columns["crop_trait_code_id"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("crop_trait_id")) ux_datagridviewCropTraitCodeAttach.Columns["crop_trait_id"].DisplayIndex = i++;
                
#endif
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("source_file_path")) ux_datagridviewCropTraitCodeAttach.Columns["source_file_path"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("virtual_path")) ux_datagridviewCropTraitCodeAttach.Columns["virtual_path"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("sort_order")) ux_datagridviewCropTraitCodeAttach.Columns["sort_order"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("title")) ux_datagridviewCropTraitCodeAttach.Columns["title"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("description")) ux_datagridviewCropTraitCodeAttach.Columns["description"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("content_type")) ux_datagridviewCropTraitCodeAttach.Columns["content_type"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("category_code")) ux_datagridviewCropTraitCodeAttach.Columns["category_code"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("is_web_visible")) ux_datagridviewCropTraitCodeAttach.Columns["is_web_visible"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("attach_cooperator_id")) ux_datagridviewCropTraitCodeAttach.Columns["attach_cooperator_id"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("attach_date_code")) ux_datagridviewCropTraitCodeAttach.Columns["attach_date_code"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("attach_date")) ux_datagridviewCropTraitCodeAttach.Columns["attach_date"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitCodeAttach.Columns.Contains("note")) ux_datagridviewCropTraitCodeAttach.Columns["note"].DisplayIndex = i++;

                foreach (DataGridViewColumn dgvc in ux_datagridviewCropTraitCodeAttach.Columns)
                {
                    dgvc.SortMode = DataGridViewColumnSortMode.Automatic;
                    // Hide any columns not explicitly ordered in the above code...
                    if (dgvc.DisplayIndex >= i) dgvc.Visible = false;
                }
            }
            else
            {
                _dtCropTraitCodeAttach = new DataTable();
            }
        }

        void _cropTraitCodeBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            if (_cropTraitCodeBindingSource.List.Count > 0)
            {
                // Make sure the child dgvs are not in edit mode (thus a new row might not be saved to the datatable yet)...
                ux_datagridviewCropTraitCodeLang.EndEdit();
                ux_datagridviewCropTraitCodeAttach.EndEdit();
                // Change the row filter for the child tables to reflect the new CropTraitCode's primary key...
                string pkey = ((DataRowView)_cropTraitCodeBindingSource.Current)[_dtCropTraitCode.PrimaryKey[0].ColumnName].ToString();
                if (_dtCropTraitCodeLang != null && !string.IsNullOrEmpty(pkey) && _dtCropTraitCodeLang.Columns.Contains("crop_trait_code_id")) _dtCropTraitCodeLang.DefaultView.RowFilter = "crop_trait_code_id=" + pkey.Trim().ToLower();
                if (_dtCropTraitCodeAttach != null && !string.IsNullOrEmpty(pkey) && _dtCropTraitCodeAttach.Columns.Contains("crop_trait_code_id")) _dtCropTraitCodeAttach.DefaultView.RowFilter = "crop_trait_code_id=" + pkey.Trim().ToLower();
            }
            else
            {
                if (_dtCropTraitCodeLang != null && _dtCropTraitCodeLang.Columns.Contains("crop_trait_code_id")) _dtCropTraitCodeLang.DefaultView.RowFilter = "crop_trait_code_id is null";
                if (_dtCropTraitCodeAttach != null && _dtCropTraitCodeAttach.Columns.Contains("crop_trait_code_id")) _dtCropTraitCodeAttach.DefaultView.RowFilter = "crop_trait_code_id is null";
            }
        }

        private void ux_buttonNewCropTraitCodeRow_Click(object sender, EventArgs e)
        {
            string pkey = ((DataRowView)_cropTraitBindingSource.Current)[_dtCropTrait.PrimaryKey[0].ColumnName].ToString();
            DataRow newCropTraitCode = _dtCropTraitCode.NewRow();
            newCropTraitCode["crop_trait_id"] = pkey;

            _dtCropTraitCode.Rows.Add(newCropTraitCode);
            int newRowIndex = ux_datagridviewCropTraitCode.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
            int newColIndex = ux_datagridviewCropTraitCode.Columns.GetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;
            for (int i = 0; i < ux_datagridviewCropTraitCode.Rows.Count; i++)
            {
                if (ux_datagridviewCropTraitCode["crop_trait_code_id", i].Value.Equals(newCropTraitCode["crop_trait_code_id"])) newRowIndex = i;
            }
            foreach (DataGridViewColumn dgvc in ux_datagridviewCropTraitCode.Columns)
            {
                if (dgvc.DisplayIndex == 0)
                {
                    newColIndex = dgvc.Index;
                    break;
                }
            }
            ux_datagridviewCropTraitCode.CurrentCell = ux_datagridviewCropTraitCode[newColIndex, newRowIndex];
        }

        private void ux_buttonNewropTraitCodeLangRow_Click(object sender, EventArgs e)
        {
            string pkey = ((DataRowView)_cropTraitCodeBindingSource.Current)[_dtCropTraitCode.PrimaryKey[0].ColumnName].ToString();
            DataRow newCropTraitCodeLang = _dtCropTraitCodeLang.NewRow();
            newCropTraitCodeLang["crop_trait_code_id"] = pkey;

            _dtCropTraitCodeLang.Rows.Add(newCropTraitCodeLang);
            int newRowIndex = ux_datagridviewCropTraitCodeLang.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
            int newColIndex = ux_datagridviewCropTraitCodeLang.Columns.GetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;
            for (int i = 0; i < ux_datagridviewCropTraitCodeLang.Rows.Count; i++)
            {
                if (ux_datagridviewCropTraitCodeLang["crop_trait_code_lang_id", i].Value.Equals(newCropTraitCodeLang["crop_trait_code_lang_id"])) newRowIndex = i;
            }
            foreach (DataGridViewColumn dgvc in ux_datagridviewCropTraitCodeLang.Columns)
            {
                if (dgvc.DisplayIndex == 0)
                {
                    newColIndex = dgvc.Index;
                    break;
                }
            }
            ux_datagridviewCropTraitCodeLang.CurrentCell = ux_datagridviewCropTraitCodeLang[newColIndex, newRowIndex];
        }

        private void ux_buttonNewCropTraitCodeAttachRow_Click(object sender, EventArgs e)
        {
            string pkey = ((DataRowView)_cropTraitCodeBindingSource.Current)[_dtCropTraitCode.PrimaryKey[0].ColumnName].ToString();
            DataRow newCropTraitCodeAttach = _dtCropTraitCodeAttach.NewRow();
            newCropTraitCodeAttach["crop_trait_code_id"] = pkey;
            newCropTraitCodeAttach["is_web_visible"] = "Y";
            newCropTraitCodeAttach["attach_cooperator_id"] = DBNull.Value;

            _dtCropTraitCodeAttach.Rows.Add(newCropTraitCodeAttach);
            int newRowIndex = ux_datagridviewCropTraitCodeAttach.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
            int newColIndex = ux_datagridviewCropTraitCodeAttach.Columns.GetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;
            for (int i = 0; i < ux_datagridviewCropTraitCodeAttach.Rows.Count; i++)
            {
                if (ux_datagridviewCropTraitCodeAttach["crop_trait_code_attach_id", i].Value.Equals(newCropTraitCodeAttach["crop_trait_code_attach_id"])) newRowIndex = i;
            }
            foreach (DataGridViewColumn dgvc in ux_datagridviewCropTraitCodeAttach.Columns)
            {
                if (dgvc.DisplayIndex == 0)
                {
                    newColIndex = dgvc.Index;
                    break;
                }
            }
            ux_datagridviewCropTraitCodeAttach.CurrentCell = ux_datagridviewCropTraitCodeAttach[newColIndex, newRowIndex];
        }

        #endregion

        #region CropTraitAttachTab

        private void BuildCropTraitAttachPage()
        {
            DataSet ds;
            // Get the crop_trait_attach table and bind it to the DGV on the Names tabpage...
            ds = _sharedUtils.GetWebServiceData("get_crop_trait_attach", _cropTraitPKeys, 0, 0);
            if (ds.Tables.Contains("get_crop_trait_attach"))
            {
                // Copy the crop_trait_attach table to a private variable...
                _dtCropTraitAttach = ds.Tables["get_crop_trait_attach"].Copy();
                // Bind the DGV to the binding source...
                ux_datagridviewCropTraitAttach.DataSource = _cropTraitAttachBindingSource;
                // Build the DGV using the new table and bind it to the DGV's binding source (this happens in the Build method)...
                _sharedUtils.BuildEditDataGridView(ux_datagridviewCropTraitAttach, _dtCropTraitAttach);

                // Order and display the columns the way the user wants...
                int i = 0;

                ux_datagridviewCropTraitAttach.Columns.Add(new DataGridViewColumn()
                {
                    HeaderText = "Source File Path",
                    Name = "source_file_path",
                    CellTemplate = new DataGridViewTextBoxCell(),
                    Tag = "FILE_PATH"
                });
#if DEBUG
                if (ux_datagridviewCropTraitAttach.Columns.Contains("crop_trait_attach_id")) ux_datagridviewCropTraitAttach.Columns["crop_trait_attach_id"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitAttach.Columns.Contains("crop_trait_id")) ux_datagridviewCropTraitAttach.Columns["crop_trait_id"].DisplayIndex = i++;
#endif
                if (ux_datagridviewCropTraitAttach.Columns.Contains("source_file_path")) ux_datagridviewCropTraitAttach.Columns["source_file_path"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitAttach.Columns.Contains("content_type")) ux_datagridviewCropTraitAttach.Columns["content_type"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitAttach.Columns.Contains("category_code")) ux_datagridviewCropTraitAttach.Columns["category_code"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitAttach.Columns.Contains("virtual_path"))
                {
                    ux_datagridviewCropTraitAttach.Columns["virtual_path"].DisplayIndex = i++;
                    ux_datagridviewCropTraitAttach.Columns["virtual_path"].Width = 300;
                }
                if (ux_datagridviewCropTraitAttach.Columns.Contains("sort_order")) ux_datagridviewCropTraitAttach.Columns["sort_order"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitAttach.Columns.Contains("title")) ux_datagridviewCropTraitAttach.Columns["title"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitAttach.Columns.Contains("description")) ux_datagridviewCropTraitAttach.Columns["description"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitAttach.Columns.Contains("is_web_visible")) ux_datagridviewCropTraitAttach.Columns["is_web_visible"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitAttach.Columns.Contains("attach_cooperator_id")) ux_datagridviewCropTraitAttach.Columns["attach_cooperator_id"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitAttach.Columns.Contains("attach_date_code")) ux_datagridviewCropTraitAttach.Columns["attach_date_code"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitAttach.Columns.Contains("attach_date")) ux_datagridviewCropTraitAttach.Columns["attach_date"].DisplayIndex = i++;
                if (ux_datagridviewCropTraitAttach.Columns.Contains("note")) ux_datagridviewCropTraitAttach.Columns["note"].DisplayIndex = i++;

                foreach (DataGridViewColumn dgvc in ux_datagridviewCropTraitAttach.Columns)
                {
                    dgvc.SortMode = DataGridViewColumnSortMode.Automatic;
                    // Hide any columns not explicitly ordered in the above code...
                    if (dgvc.DisplayIndex >= i) dgvc.Visible = false;
                }
            }
            else
            {
                _dtCropTraitAttach = new DataTable();
            }
        }

        private void ux_buttonNewCropTraitAttachRow_Click(object sender, EventArgs e)
        {
            string pkey = ((DataRowView)_cropTraitBindingSource.Current)[_dtCropTrait.PrimaryKey[0].ColumnName].ToString();
            DataRow newCropTraitAttach = _dtCropTraitAttach.NewRow();
            newCropTraitAttach["crop_trait_id"] = pkey;
            newCropTraitAttach["is_web_visible"] = "Y";
            newCropTraitAttach["attach_cooperator_id"] = DBNull.Value;

            _dtCropTraitAttach.Rows.Add(newCropTraitAttach);
            int newRowIndex = ux_datagridviewCropTraitAttach.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
            int newColIndex = ux_datagridviewCropTraitAttach.Columns.GetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;
            for (int i = 0; i < ux_datagridviewCropTraitAttach.Rows.Count; i++)
            {
                if (ux_datagridviewCropTraitAttach["crop_trait_attach_id", i].Value.Equals(newCropTraitAttach["crop_trait_attach_id"])) newRowIndex = i;
            }
            foreach (DataGridViewColumn dgvc in ux_datagridviewCropTraitAttach.Columns)
            {
                if (dgvc.DisplayIndex == 0)
                {
                    newColIndex = dgvc.Index;
                    break;
                }
            }
            ux_datagridviewCropTraitAttach.CurrentCell = ux_datagridviewCropTraitAttach[newColIndex, newRowIndex];
        }

        #endregion

        private void bindingNavigatorSearchButton_Click(object sender, EventArgs e)
        {
            if (_dtCropTraitSearch != null)
            {
                GGWizard.LookupTablePicker2 ltp = new GGWizard.LookupTablePicker2(_sharedUtils, "crop_trait_id", _dtCropTraitSearch.Rows[0], "", "crop_trait_lookup");
                ltp.StartPosition = FormStartPosition.CenterParent;
                if (DialogResult.OK == ltp.ShowDialog())
                {
                    GetData(":croptraitid=" + ltp.NewKey);
                }
            }
        }

        private void bindingNavigatorDuplicateButton_Click(object sender, EventArgs e)
        {
            //string pkey = ((DataRowView)_cropTraitBindingSource.Current)[_dtCropTrait.PrimaryKey[0].ColumnName].ToString();
            //newCropTrait.ItemArray = ((DataRowView)_cropTraitBindingSource.Current).Row.ItemArray;

            if (_cropTraitBindingSource.List.Count == 0) return;

            // Make sure the last edited row in the CropTrait Form has been commited to the datatable...
            _cropTraitBindingSource.EndEdit();

            // Make sure the navigator is not currently editing a cell...
            foreach (DataRowView drv in _cropTraitBindingSource.List)
            {
                if (drv.IsEdit ||
                    drv.Row.RowState == DataRowState.Added ||
                    drv.Row.RowState == DataRowState.Deleted ||
                    drv.Row.RowState == DataRowState.Detached ||
                    drv.Row.RowState == DataRowState.Modified)
                {
                    drv.EndEdit();
                }
            }

            //Copy crop_trait
            DataRow newCropTrait = _dtCropTrait.NewRow();
            newCropTrait["coded_name"] = ((DataRowView)_cropTraitBindingSource.Current).Row["coded_name"];
            newCropTrait["title"] = ((DataRowView)_cropTraitBindingSource.Current).Row["title"];
            newCropTrait["description"] = ((DataRowView)_cropTraitBindingSource.Current).Row["description"];
            newCropTrait["is_peer_reviewed"] = ((DataRowView)_cropTraitBindingSource.Current).Row["is_peer_reviewed"];
            newCropTrait["category_code"] = ((DataRowView)_cropTraitBindingSource.Current).Row["category_code"];
            newCropTrait["data_type_code"] = ((DataRowView)_cropTraitBindingSource.Current).Row["data_type_code"];
            newCropTrait["is_coded"] = ((DataRowView)_cropTraitBindingSource.Current).Row["is_coded"];
            newCropTrait["max_length"] = ((DataRowView)_cropTraitBindingSource.Current).Row["max_length"];
            newCropTrait["numeric_format"] = ((DataRowView)_cropTraitBindingSource.Current).Row["numeric_format"];
            newCropTrait["numeric_maximum"] = ((DataRowView)_cropTraitBindingSource.Current).Row["numeric_maximum"];
            newCropTrait["numeric_minimum"] = ((DataRowView)_cropTraitBindingSource.Current).Row["numeric_minimum"];
            newCropTrait["original_value_type_code"] = ((DataRowView)_cropTraitBindingSource.Current).Row["original_value_type_code"];
            newCropTrait["original_value_format"] = ((DataRowView)_cropTraitBindingSource.Current).Row["original_value_format"];
            newCropTrait["is_archived"] = ((DataRowView)_cropTraitBindingSource.Current).Row["is_archived"];
            newCropTrait["ontology_url"] = ((DataRowView)_cropTraitBindingSource.Current).Row["ontology_url"];
            newCropTrait["note"] = ((DataRowView)_cropTraitBindingSource.Current).Row["note"];
            _dtCropTrait.Rows.Add(newCropTrait);

            string newCropTraitId = newCropTrait["crop_trait_id"].ToString();
            //Copy crop_trait_lang
            foreach (DataRowView drvCropTraitLang in _dtCropTraitLang.DefaultView)
            {
                DataRow newCropTraitLang = _dtCropTraitLang.NewRow();
                newCropTraitLang["crop_trait_id"] = newCropTraitId;
                newCropTraitLang["sys_lang_id"] = drvCropTraitLang.Row["sys_lang_id"];
                newCropTraitLang["title"] = drvCropTraitLang.Row["title"];
                newCropTraitLang["description"] = drvCropTraitLang.Row["description"];
                _dtCropTraitLang.Rows.Add(newCropTraitLang);
            }

            //Copy crop_trait_code
            foreach (DataRowView drvCropTraitCode in _dtCropTraitCode.DefaultView)
            {
                DataRow newCropTraitCode = _dtCropTraitCode.NewRow();
                newCropTraitCode["crop_trait_id"] = newCropTraitId;
                newCropTraitCode["code"] = drvCropTraitCode.Row["code"];
                newCropTraitCode["title"] = drvCropTraitCode.Row["title"];
                _dtCropTraitCode.Rows.Add(newCropTraitCode);

                var cropTraitCodeLangList = _dtCropTraitCodeLang.AsEnumerable().Where(r => r.Field<int>("crop_trait_code_id") == drvCropTraitCode.Row.Field<int>("crop_trait_code_id")).ToArray();
                foreach (var drCropTraitCodeLang in cropTraitCodeLangList)
                {
                    DataRow newCropTraitCodeLang = _dtCropTraitCodeLang.NewRow();
                    newCropTraitCodeLang["crop_trait_code_id"] = newCropTraitCode["crop_trait_code_id"];
                    newCropTraitCodeLang["crop_trait_id"] = newCropTraitId;
                    newCropTraitCodeLang["sys_lang_id"] = drCropTraitCodeLang["sys_lang_id"];
                    newCropTraitCodeLang["title"] = drCropTraitCodeLang["title"];
                    newCropTraitCodeLang["description"] = drCropTraitCodeLang["description"];
                    _dtCropTraitCodeLang.Rows.Add(newCropTraitCodeLang);
                }
            }

            _cropTraitBindingSource.MoveLast();
        }

        private void bindingNavigatorSaveButton_Click(object sender, EventArgs e)
        {
            int errorCount = 0;
            errorCount = SaveCropTraitData();
            if (errorCount == 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Crop trait Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "AccessionWizard_bindingNavigatorSaveButtonMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();

                // Now reload all data associated with new and existing accessions 
                // because there are MT datatriggers that create new records during a save 
                // that the Accession Wizard will not have yet...
                // First gather up all accession_ids...
                _cropTraitPKeys = ":croptraitid=";
                foreach (DataRow dr in _dtCropTrait.Rows)
                {
                    _cropTraitPKeys += dr["crop_trait_id"].ToString() + ",";
                }
                _cropTraitPKeys = _cropTraitPKeys.TrimEnd(',');

                GetData(_cropTraitPKeys);
                //CropTraitWizard_Load(sender, e);
            }
            else
            {
                //MessageBox.Show(this, "The data being saved has errors that should be reviewed.\n\n  Error Count: " + errorCount, "Accession Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed.\n\n  Error Count: {0}", "Crop trait Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "AccessionWizard_bindingNavigatorSaveButtonMessage2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                //if (ggMessageBox.MessageText.Contains("{0}")) ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, errorCount);
                string[] argsArray = new string[100];
                argsArray[0] = errorCount.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }
            //RefreshAccessionData();
            //// Update the row error message for this accession row...
            //ux_labelAccessionRowError.Text = ((DataRowView)_accessionBindingSource.Current).Row.RowError;
            // Update the Accession Form (and child DataGridViews) data...
            _mainBindingSource_CurrentChanged(sender, e);
        }

        private void bindingNavigatorSaveAndExitButton_Click(object sender, EventArgs e)
        {
            int errorCount = 0;
            errorCount = SaveCropTraitData();
            if (errorCount == 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Crop trait Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "AccessionWizard_bindingNavigatorSaveButtonMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();

                this.Close();
            }
            else
            {
                //MessageBox.Show(this, "The data being saved has errors that should be reviewed.\n\n  Error Count: " + errorCount, "Accession Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed.\n\n  Error Count: {0}", "Crop trait Wizard Data Save Results", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "AccessionWizard_bindingNavigatorSaveButtonMessage3";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                //if (ggMessageBox.MessageText.Contains("{0}")) ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, errorCount);
                string[] argsArray = new string[100];
                argsArray[0] = errorCount.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                if (DialogResult.No == ggMessageBox.ShowDialog())
                {
                    this.Close();
                }
                else
                {
                    // Update the row error message for this crop_trait row...
                    if (string.IsNullOrEmpty(((DataRowView)_cropTraitBindingSource.Current).Row.RowError))
                    {
                        ux_textboxAccessionRowError.Visible = false;
                        ux_textboxAccessionRowError.Text = "";
                    }
                    else
                    {
                        ux_textboxAccessionRowError.Visible = true;
                        ux_textboxAccessionRowError.ReadOnly = false;
                        ux_textboxAccessionRowError.Enabled = true;
                        ux_textboxAccessionRowError.Text = ((DataRowView)_cropTraitBindingSource.Current).Row.RowError;
                    }
                }
            }
        }

        private int SaveCropTraitData()
        {
            int errorCount = 0;
            DataSet cropTraitChanges = new DataSet();
            DataSet cropTraitSaveResults = new DataSet();
            DataSet cropTraitLangChanges = new DataSet();
            DataSet cropTraitLangSaveResults = new DataSet();
            DataSet cropTraitCodeChanges = new DataSet();
            DataSet cropTraitCodeSaveResults = new DataSet();
            DataSet cropTraitCodeLangChanges = new DataSet();
            DataSet cropTraitCodeLangSaveResults = new DataSet();
            DataSet cropTraitCodeAttachChanges = new DataSet();
            DataSet cropTraitCodeAttachSaveResults = new DataSet();
            DataSet cropTraitAttachChanges = new DataSet();
            DataSet cropTraitAttachSaveResults = new DataSet();
            
            ux_datagridviewCropTraitAttach.EndEdit();
            ux_datagridviewCropTraitLang.EndEdit();
            
            ux_datagridviewCropTraitCode.EndEdit();
            ux_datagridviewCropTraitCodeAttach.EndEdit();
            ux_datagridviewCropTraitCodeLang.EndEdit();

            // Process CROP_TRAIT...
            // Make sure the last edited row in the Crop Trait Form has been commited to the datatable...
            _cropTraitBindingSource.EndEdit();

            // Make sure the navigator is not currently editing a cell...
            foreach (DataRowView drv in _cropTraitBindingSource.List)
            {
                if (drv.IsEdit ||
                    drv.Row.RowState == DataRowState.Added ||
                    drv.Row.RowState == DataRowState.Deleted ||
                    drv.Row.RowState == DataRowState.Detached ||
                    drv.Row.RowState == DataRowState.Modified)
                {
                    drv.EndEdit();
                    //drv.Row.ClearErrors();
                }
            }

            // Get the changes (if any) for the crop_trait table and commit them to the remote database...
            if (_dtCropTrait.GetChanges() != null)
            {
                cropTraitChanges.Tables.Add(_dtCropTrait.GetChanges());
                ScrubData(cropTraitChanges);
                // Save the changes to the remote server...
                cropTraitSaveResults = _sharedUtils.SaveWebServiceData(cropTraitChanges);
                if (cropTraitSaveResults.Tables.Contains(_dtCropTrait.TableName))
                {
                    errorCount += SyncSavedResults(_dtCropTrait, cropTraitSaveResults.Tables[_dtCropTrait.TableName]);
                }
            }

            // Process CROP_TRAIT_LANG...
            // Make sure the last edited row in the DGV has been commited to the datatable...
            _cropTraitLangBindingSource.EndEdit();

            // Get the changes (if any) for the crop_trait_lang table and commit them to the remote database...
            if (_dtCropTraitLang.GetChanges() != null)
            {
                // Before saving the results to the remote server check to see if any new rows in crop_trait_lang have
                // a FK related to a new row in the crop_trait table (pkey < 0).  If so get the new pkey returned from
                // the crop_trait save and update the records in crop_trait_lang...
                DataRow[] accessionSourceRowsWithNewParent = _dtCropTraitLang.Select("crop_trait_id<0");
                foreach (DataRow dr in accessionSourceRowsWithNewParent)
                {
                    // .Rows.Find(dr["OriginalPrimaryKeyID"])  dr["NewPrimaryKeyID"]
                    DataRow[] newParent = cropTraitSaveResults.Tables["get_crop_trait"].Select("OriginalPrimaryKeyID=" + dr["crop_trait_id"].ToString());
                    if (newParent != null && newParent.Length > 0)
                    {
                        dr["crop_trait_id"] = newParent[0]["NewPrimaryKeyID"];
                    }
                }
                cropTraitLangChanges.Tables.Add(_dtCropTraitLang.GetChanges());
                ScrubData(cropTraitLangChanges);
                // Now save the changes to the remote server...
                cropTraitLangSaveResults = _sharedUtils.SaveWebServiceData(cropTraitLangChanges);
                // Sync the saved results with the original table (to give user feedback about results of save)...
                if (cropTraitLangSaveResults.Tables.Contains(_dtCropTraitLang.TableName))
                {
                    errorCount += SyncSavedResults(_dtCropTraitLang, cropTraitLangSaveResults.Tables[_dtCropTraitLang.TableName]);
                }
            }

            // Process CROP_TRAIT_CODE...
            // Make sure the last edited row in the DGV has been commited to the datatable...
            _cropTraitCodeBindingSource.EndEdit();

            // Get the changes (if any) for the crop_trait_code table and commit them to the remote database...
            if (_dtCropTraitCode.GetChanges() != null)
            {
                // Before saving the results to the remote server check to see if any new rows in crop_trait_code have
                // a FK related to a new row in the crop_trait table (pkey < 0).  If so get the new pkey returned from
                // the get_crop_trait save and update the records in crop_trait_code...
                DataRow[] accessionSourceRowsWithNewParent = _dtCropTraitCode.Select("crop_trait_id<0");
                foreach (DataRow dr in accessionSourceRowsWithNewParent)
                {
                    // .Rows.Find(dr["OriginalPrimaryKeyID"])  dr["NewPrimaryKeyID"]
                    DataRow[] newParent = cropTraitSaveResults.Tables["get_crop_trait"].Select("OriginalPrimaryKeyID=" + dr["crop_trait_id"].ToString());
                    if (newParent != null && newParent.Length > 0)
                    {
                        dr["crop_trait_id"] = newParent[0]["NewPrimaryKeyID"];
                    }
                }
                cropTraitCodeChanges.Tables.Add(_dtCropTraitCode.GetChanges());
                ScrubData(cropTraitCodeChanges);
                // Now save the changes to the remote server...
                cropTraitCodeSaveResults = _sharedUtils.SaveWebServiceData(cropTraitCodeChanges);
                // Sync the saved results with the original table (to give user feedback about results of save)...
                if (cropTraitCodeSaveResults.Tables.Contains(_dtCropTraitCode.TableName))
                {
                    errorCount += SyncSavedResults(_dtCropTraitCode, cropTraitCodeSaveResults.Tables[_dtCropTraitCode.TableName]);
                }
            }

            // Process CROP_TRAIT_CODE_LANG...
            // Make sure the last edited row in the DGV has been commited to the datatable...
            _cropTraitCodeLangBindingSource.EndEdit();

            // Get the changes (if any) for the crop_trait_code_lang table and commit them to the remote database...
            if (_dtCropTraitCodeLang.GetChanges() != null)
            {
                // Before saving the results to the remote server check to see if any new rows in crop_trait_code_lang have
                // a FK related to a new row in the crop_trait_code table (pkey < 0).  If so get the new pkey returned from
                // the get_crop_trait_code save and update the records in crop_trait_code_lang...
                DataRow[] accessionSourceCooperatorRowsWithNewParent = _dtCropTraitCodeLang.Select("crop_trait_code_id<0");
                foreach (DataRow dr in accessionSourceCooperatorRowsWithNewParent)
                {
                    // .Rows.Find(dr["OriginalPrimaryKeyID"])  dr["NewPrimaryKeyID"]
                    DataRow[] newParent = cropTraitCodeSaveResults.Tables["get_crop_trait_code"].Select("OriginalPrimaryKeyID=" + dr["crop_trait_code_id"].ToString());
                    if (newParent != null && newParent.Length > 0)
                    {
                        dr["crop_trait_code_id"] = newParent[0]["NewPrimaryKeyID"];
                    }
                }
                cropTraitCodeLangChanges.Tables.Add(_dtCropTraitCodeLang.GetChanges());
                ScrubData(cropTraitCodeLangChanges);
                // Now save the changes to the remote server...
                cropTraitCodeLangSaveResults = _sharedUtils.SaveWebServiceData(cropTraitCodeLangChanges);
                // Sync the saved results with the original table (to give user feedback about results of save)...
                if (cropTraitCodeLangSaveResults.Tables.Contains(_dtCropTraitCodeLang.TableName))
                {
                    errorCount += SyncSavedResults(_dtCropTraitCodeLang, cropTraitCodeLangSaveResults.Tables[_dtCropTraitCodeLang.TableName]);
                }
            }

            // Process CROP_TRAIT_CODE_ATTACH...
            // Make sure the last edited row in the DGV has been commited to the datatable...
            _cropTraitCodeAttachBindingSource.EndEdit();

            // Upload Files before save crop_trait_code_attach
            foreach (DataGridViewRow dgr in ux_datagridviewCropTraitCodeAttach.Rows)
            {
                if (dgr.Cells["source_file_path"].Value != null && !string.IsNullOrEmpty(dgr.Cells["source_file_path"].Value.ToString()))
                {
                    var sourceFilePath = dgr.Cells["source_file_path"].Value.ToString();
                    var virtualPath = dgr.Cells["virtual_path"].Value.ToString();
                    string remotePath = string.Empty;
                    byte[] imageBytes = File.ReadAllBytes(sourceFilePath);

                    if (imageBytes != null)
                    {
                        string error = string.Empty;
                        try
                        {
                            //remotePath = _sharedUtils.SaveImage("/uploads/images/" + Guid.NewGuid() + Path.GetExtension(sourceFilePath), imageBytes, false, true);
                            remotePath = _sharedUtils.SaveAttachment("/" + virtualPath, imageBytes, false, true);
                            // If the upload was successful the remotePath will contain the destination path
                            if (!remotePath.Contains("/uploads/"))
                            {
                                error = "Error on SaveImage WebService\n" + remotePath;
                            }
                        }
                        catch (Exception eSaveImage)
                        {
                            error = eSaveImage.Message;
                        }
                        if (!string.IsNullOrEmpty(error))
                        {
                            errorCount++;
                            dgr.ErrorText = error;
                        }
                    }
                }
            }

            // Get the changes (if any) for the crop_trait_code_attach table and commit them to the remote database...
            if (_dtCropTraitCodeAttach.GetChanges() != null)
            {
                // Before saving the results to the remote server check to see if any new rows in crop_trait_code_attach have
                // a FK related to a new row in the crop_trait_code table (pkey < 0).  If so get the new pkey returned from
                // the get_crop_trait_code save and update the records in crop_trait_code_attach...
                DataRow[] accessionSourceCooperatorRowsWithNewParent = _dtCropTraitCodeAttach.Select("crop_trait_code_id<0");
                foreach (DataRow dr in accessionSourceCooperatorRowsWithNewParent)
                {
                    // .Rows.Find(dr["OriginalPrimaryKeyID"])  dr["NewPrimaryKeyID"]
                    DataRow[] newParent = cropTraitCodeSaveResults.Tables["get_crop_trait_code"].Select("OriginalPrimaryKeyID=" + dr["crop_trait_code_id"].ToString());
                    if (newParent != null && newParent.Length > 0)
                    {
                        dr["crop_trait_code_id"] = newParent[0]["NewPrimaryKeyID"];
                    }
                }
                cropTraitCodeAttachChanges.Tables.Add(_dtCropTraitCodeAttach.GetChanges());
                ScrubData(cropTraitCodeAttachChanges);
                // Now save the changes to the remote server...
                cropTraitCodeAttachSaveResults = _sharedUtils.SaveWebServiceData(cropTraitCodeAttachChanges);
                // Sync the saved results with the original table (to give user feedback about results of save)...
                if (cropTraitCodeAttachSaveResults.Tables.Contains(_dtCropTraitCodeAttach.TableName))
                {
                    errorCount += SyncSavedResults(_dtCropTraitCodeAttach, cropTraitCodeAttachSaveResults.Tables[_dtCropTraitCodeAttach.TableName]);
                }
            }

            // Process CROP_TRAIT_ATTACH...
            // Make sure the last edited row in the DGV has been commited to the datatable...
            _cropTraitAttachBindingSource.EndEdit();

            // Upload Files before save crop_trait_attach
            foreach (DataGridViewRow dgr in ux_datagridviewCropTraitAttach.Rows)
            {
                if (dgr.Cells["source_file_path"].Value != null && !string.IsNullOrEmpty(dgr.Cells["source_file_path"].Value.ToString()))
                {
                    string error = string.Empty;
                    try
                    {
                        var sourceFilePath = dgr.Cells["source_file_path"].Value.ToString();
                        var virtualPath = dgr.Cells["virtual_path"].Value.ToString();
                        string remotePath = string.Empty;
                        byte[] imageBytes = File.ReadAllBytes(sourceFilePath);

                        if (imageBytes != null)
                        {
                            //remotePath = _sharedUtils.SaveImage("/uploads/images/" + Guid.NewGuid() + Path.GetExtension(sourceFilePath), imageBytes, false, true);
                            remotePath = _sharedUtils.SaveAttachment("/" + virtualPath, imageBytes, false, true);
                            // If the upload was successful the remotePath will contain the destination path
                            if (!remotePath.Contains("/uploads/"))
                            {
                                error = "Error on SaveImage WebService\n" + remotePath;
                            }
                        }
                        else
                        {
                            error = "Error reading file\n";
                        }
                    }
                    catch (Exception eSaveImage)
                    {
                        error = eSaveImage.Message;
                    }
                    if (!string.IsNullOrEmpty(error))
                    {
                        errorCount++;
                        dgr.ErrorText = error;
                    }
                }
            }

            // Get the changes (if any) for the crop_trait_attach table and commit them to the remote database...
            if (_dtCropTraitAttach.GetChanges() != null)
            {
                // Before saving the results to the remote server check to see if any new rows in crop_trait_attach have
                // a FK related to a new row in the crop_trait table (pkey < 0).  If so get the new pkey returned from
                // the crop_trait save and update the records in crop_trait_attach...
                DataRow[] accessionSourceRowsWithNewParent = _dtCropTraitAttach.Select("crop_trait_id<0");
                foreach (DataRow dr in accessionSourceRowsWithNewParent)
                {
                    // .Rows.Find(dr["OriginalPrimaryKeyID"])  dr["NewPrimaryKeyID"]
                    DataRow[] newParent = cropTraitSaveResults.Tables["get_crop_trait"].Select("OriginalPrimaryKeyID=" + dr["crop_trait_id"].ToString());
                    if (newParent != null && newParent.Length > 0)
                    {
                        dr["crop_trait_id"] = newParent[0]["NewPrimaryKeyID"];
                    }
                }
                cropTraitAttachChanges.Tables.Add(_dtCropTraitAttach.GetChanges());
                ScrubData(cropTraitAttachChanges);
                // Now save the changes to the remote server...
                cropTraitAttachSaveResults = _sharedUtils.SaveWebServiceData(cropTraitAttachChanges);
                // Sync the saved results with the original table (to give user feedback about results of save)...
                if (cropTraitAttachSaveResults.Tables.Contains(_dtCropTraitAttach.TableName))
                {
                    errorCount += SyncSavedResults(_dtCropTraitAttach, cropTraitAttachSaveResults.Tables[_dtCropTraitAttach.TableName]);
                }
            }

            /**********************************************************************************************************/

            // Now add the new changes to the _changedRecords dataset (this data will be passed back to the calling program)...
            if (cropTraitSaveResults != null && cropTraitSaveResults.Tables.Contains(_dtCropTrait.TableName))
            {
                string pkeyName = cropTraitSaveResults.Tables[_dtCropTrait.TableName].PrimaryKey[0].ColumnName;
                bool origColumnReadOnlyValue = cropTraitSaveResults.Tables[_dtCropTrait.TableName].Columns[pkeyName].ReadOnly;
                foreach (DataRow dr in cropTraitSaveResults.Tables[_dtCropTrait.TableName].Rows)
                {
                    if (dr["SavedAction"].ToString().ToUpper() == "INSERT" &&
                        dr["SavedStatus"].ToString().ToUpper() == "SUCCESS")
                    {
                        dr.Table.Columns[pkeyName].ReadOnly = false;
                        dr[pkeyName] = dr["NewPrimaryKeyID"];
                        dr.AcceptChanges();
                    }
                }
                cropTraitSaveResults.Tables[_dtCropTrait.TableName].Columns[pkeyName].ReadOnly = origColumnReadOnlyValue;

                if (_changedRecords.Tables.Contains(_dtCropTrait.TableName))
                {
                    // If the saved results table exists - update or insert the new records...
                    _changedRecords.Tables[_dtCropTrait.TableName].Load(cropTraitSaveResults.Tables[_dtCropTrait.TableName].CreateDataReader(), LoadOption.Upsert);
                    _changedRecords.Tables[_dtCropTrait.TableName].AcceptChanges();

                }
                else
                {
                    // If the saved results table doesn't exist - create it (and include the new records)...
                    _changedRecords.Tables.Add(cropTraitSaveResults.Tables[_dtCropTrait.TableName].Copy());
                    _changedRecords.AcceptChanges();
                }
            }

            return errorCount;
        }

        private void ScrubData(DataSet ds)
        {
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        if (dc.ExtendedProperties.Contains("is_nullable") &&
                            dc.ExtendedProperties["is_nullable"].ToString().Trim().ToUpper() == "N" &&
                            dr.RowState != DataRowState.Deleted &&
                            dr[dc] == DBNull.Value)
                        {
                            if (dc.ExtendedProperties.Contains("default_value") &&
                                !string.IsNullOrEmpty(dc.ExtendedProperties["default_value"].ToString()) &&
                                dc.ExtendedProperties["default_value"].ToString().Trim().ToUpper() != "{DBNULL.VALUE}")
                            {
                                dr[dc] = dc.ExtendedProperties["default_value"].ToString();
                            }
                        }
                    }
                }
            }
        }

        private int SyncSavedResults(DataTable originalTable, DataTable savedResults)
        {
            int errorCount = 0;

            if (savedResults != null && savedResults.PrimaryKey.Length == 1)
            {
                string pKeyCol = savedResults.PrimaryKey[0].ColumnName.Trim().ToUpper();
                savedResults.Columns[pKeyCol].ReadOnly = false;
                foreach (DataRow dr in savedResults.Rows)
                {
                    DataRow originalRow = originalTable.Rows.Find(dr["OriginalPrimaryKeyID"]);

                    switch (dr["SavedAction"].ToString())
                    {
                        case "Insert":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the originalTable row's status for this new row to committed (and update the pkey with the int returned from the server DB)...
                                if (originalRow != null)
                                {
                                    bool origColumnReadOnlyValue = originalRow.Table.Columns[pKeyCol].ReadOnly;
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = false;
                                    originalRow[pKeyCol] = dr["NewPrimaryKeyID"];
                                    originalRow.AcceptChanges();
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = origColumnReadOnlyValue;
                                    originalRow.ClearErrors();
                                }
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Update":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                originalRow.AcceptChanges();
                                originalRow.ClearErrors();
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Delete":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the row's status for this deleted row to committed...
                                // To do this you must first find the deleted row (NOTE: datatable.rows.find() method does not work on deleted rows)...
                                foreach (DataRow deletedRow in originalTable.Rows)
                                {
                                    if (deletedRow.RowState == DataRowState.Deleted &&
                                        deletedRow[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                    {
                                        originalRow = deletedRow;
                                    }
                                }
                                if (originalRow != null)
                                {
                                    originalRow.AcceptChanges();
                                    originalRow.ClearErrors();
                                }
                            }
                            else
                            {
                                errorCount++;
                                // Find the deleted row (NOTE: datatable.rows.find() method does not work on deleted rows)...
                                foreach (DataRow deletedRow in originalTable.Rows)
                                {
                                    if (deletedRow[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                    {
                                        deletedRow.RejectChanges();
                                        deletedRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            return errorCount;
        }

        private void CropTraitWizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            // The user might be closing the form during the middle of edit changes in the datagridview - if so ask the
            // user if they would like to save their data...
            int intRowEdits = 0;

            _cropTraitBindingSource.EndEdit();
            if (_dtCropTrait.GetChanges() != null) intRowEdits = _dtCropTrait.GetChanges().Rows.Count;
            _cropTraitLangBindingSource.EndEdit();
            if (_dtCropTraitLang.GetChanges() != null) intRowEdits += _dtCropTraitLang.GetChanges().Rows.Count;

            _cropTraitCodeBindingSource.EndEdit();
            if (_dtCropTraitCode.GetChanges() != null) intRowEdits += _dtCropTraitCode.GetChanges().Rows.Count;
            _cropTraitCodeLangBindingSource.EndEdit();
            if (_dtCropTraitCodeLang.GetChanges() != null) intRowEdits += _dtCropTraitCodeLang.GetChanges().Rows.Count;
            _cropTraitCodeAttachBindingSource.EndEdit();
            if (_dtCropTraitCodeAttach.GetChanges() != null) intRowEdits += _dtCropTraitCodeAttach.GetChanges().Rows.Count;

            _cropTraitAttachBindingSource.EndEdit();
            if (_dtCropTraitAttach.GetChanges() != null) intRowEdits += _dtCropTraitAttach.GetChanges().Rows.Count;

            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} unsaved row change(s), are you sure you want to cancel your edits and close this window?", "Cancel Edits and Close", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "AccessionWizard_FormClosingMessage1";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            //if (ggMessageBox.MessageText.Contains("{0}")) ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, intRowEdits);
            string[] argsArray = new string[100];
            argsArray[0] = intRowEdits.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (intRowEdits > 0 && DialogResult.No == ggMessageBox.ShowDialog())
            {
                e.Cancel = true;
            }
        }

        private void CropTraitWizard_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void ux_comboboxDataTypeCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ux_comboboxDataTypeCode.DataSource != null)
            {
                if (ux_comboboxDataTypeCode.SelectedValue != null)
                {
                    if (ux_comboboxDataTypeCode.SelectedValue.ToString().Equals("NUMERIC"))
                    {
                        ux_textBoxMaxLength.ReadOnly = true;
                        ux_textBoxMaxLength.Clear();
                        ux_textBoxNumericMinimum.ReadOnly = false;
                        ux_textBoxNumericMaximum.ReadOnly = false;
                        ux_textBoxNumericFormat.ReadOnly = false;
                    }
                    else
                    {
                        ux_textBoxMaxLength.ReadOnly = false;
                        ux_textBoxNumericMinimum.ReadOnly = true;
                        ux_textBoxNumericMinimum.Clear();
                        ux_textBoxNumericMaximum.ReadOnly = true;
                        ux_textBoxNumericMaximum.Clear();
                        ux_textBoxNumericFormat.ReadOnly = true;
                        ux_textBoxNumericFormat.Clear();
                    }
                }
                else
                {
                    ux_textBoxMaxLength.ReadOnly = true;
                    ux_textBoxNumericMinimum.ReadOnly = true;
                    ux_textBoxNumericMaximum.ReadOnly = true;
                    ux_textBoxNumericFormat.ReadOnly = true;
                }
            }
        }

        private void GetData(string parameters)
        {
            DataSet ds = _sharedUtils.GetWebServiceData("get_crop_trait", parameters, 0, 0);
            if (ds.Tables.Contains("get_crop_trait"))
            {
                _dtCropTrait = ds.Tables["get_crop_trait"].Copy();
                _cropTraitBindingSource.DataSource = _dtCropTrait;
                //ux_bindingnavigatorForm.BindingSource = _accessionBindingSource;

                //Load CropTraitLang datagridview
                DataSet dsCropTraitLang = _sharedUtils.GetWebServiceData("get_crop_trait_lang", parameters, 0, 0);
                if (dsCropTraitLang.Tables.Contains("get_crop_trait_lang"))
                {
                    _dtCropTraitLang = dsCropTraitLang.Tables["get_crop_trait_lang"].Copy();
                    _cropTraitLangBindingSource.DataSource = _dtCropTraitLang;
                    //ux_datagridviewCropTraitLang.DataSource = _dtCropTraitLang;
                }
                //Load CropTraitCode datagridview
                DataSet dsCropTraitCode = _sharedUtils.GetWebServiceData("get_crop_trait_code", parameters, 0, 0);
                if (dsCropTraitCode.Tables.Contains("get_crop_trait_code"))
                {
                    _dtCropTraitCode = dsCropTraitCode.Tables["get_crop_trait_code"].Copy();
                    _cropTraitCodeBindingSource.DataSource = _dtCropTraitCode;
                }
                //Load CropTraitCodeLang datagridview
                ds = _sharedUtils.GetWebServiceData("get_crop_trait_code_lang", parameters, 0, 0);
                if (ds.Tables.Contains("get_crop_trait_code_lang"))
                {
                    _dtCropTraitCodeLang = ds.Tables["get_crop_trait_code_lang"].Copy();
                    _cropTraitCodeLangBindingSource.DataSource = _dtCropTraitCodeLang;
                }
                //Load CropTraitCodeAttach datagridview
                ds = _sharedUtils.GetWebServiceData("get_crop_trait_code_attach", parameters, 0, 0);
                if (ds.Tables.Contains("get_crop_trait_code_attach"))
                {
                    _dtCropTraitCodeAttach = ds.Tables["get_crop_trait_code_attach"].Copy();
                    _cropTraitCodeAttachBindingSource.DataSource = _dtCropTraitCodeAttach;
                }
                //Load CropTraitAttach datagridview
                ds = _sharedUtils.GetWebServiceData("get_crop_trait_attach", parameters, 0, 0);
                if (ds.Tables.Contains("get_crop_trait_attach"))
                {
                    _dtCropTraitAttach = ds.Tables["get_crop_trait_attach"].Copy();
                    _cropTraitAttachBindingSource.DataSource = _dtCropTraitAttach;
                }

                // Force the row filters to be applied...
                _mainBindingSource_CurrentChanged(null, null);
            }
            else
            {
                _dtCropTrait = null;
                _dtCropTraitLang = null;
                _dtCropTraitCode = null;
                _dtCropTraitCodeLang = null;
                _dtCropTraitCodeAttach = null;
                _dtCropTraitAttach = null;
            }
        }

        private void ux_buttonOpenCropTraitAttach_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow dgr = null;
                if (ux_datagridviewCropTraitAttach.SelectedRows.Count > 0)
                {
                    dgr = ux_datagridviewCropTraitAttach.SelectedRows[0];
                }

                if (dgr == null && ux_datagridviewCropTraitAttach.SelectedCells.Count > 0)
                {
                    dgr = ux_datagridviewCropTraitAttach.Rows[ux_datagridviewCropTraitAttach.SelectedCells[0].RowIndex];
                }

                if (dgr == null) return;

                if (dgr.Cells["virtual_path"].Value != DBNull.Value && !string.IsNullOrWhiteSpace(dgr.Cells["virtual_path"].Value.ToString()))
                {
                    string virtual_path = dgr.Cells["virtual_path"].Value.ToString();
                    string serverPath = _sharedUtils.Url.Replace("GUI.asmx", "");
                    string urlpath = serverPath + @"uploads/images/" + virtual_path;

                    Process cmd = new Process();
                    cmd.StartInfo.FileName = "cmd.exe";
                    cmd.StartInfo.RedirectStandardInput = true;
                    cmd.StartInfo.RedirectStandardOutput = true;
                    cmd.StartInfo.CreateNoWindow = true;
                    cmd.StartInfo.UseShellExecute = false;
                    cmd.Start();

                    cmd.StandardInput.WriteLine("explorer \"" + urlpath + "\"");
                    cmd.StandardInput.Flush();
                    cmd.StandardInput.Close();
                    cmd.WaitForExit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ux_buttonOpenCropTraitCodeAttach_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow dgr = null;
                if (ux_datagridviewCropTraitCodeAttach.SelectedRows.Count > 0)
                {
                    dgr = ux_datagridviewCropTraitCodeAttach.SelectedRows[0];
                }

                if (dgr == null && ux_datagridviewCropTraitCodeAttach.SelectedCells.Count > 0)
                {
                    dgr = ux_datagridviewCropTraitCodeAttach.Rows[ux_datagridviewCropTraitCodeAttach.SelectedCells[0].RowIndex];
                }

                if (dgr == null) return;

                if (dgr.Cells["virtual_path"].Value != DBNull.Value && !string.IsNullOrWhiteSpace(dgr.Cells["virtual_path"].Value.ToString()))
                {
                    string virtual_path = dgr.Cells["virtual_path"].Value.ToString();
                    string serverPath = _sharedUtils.Url.Replace("GUI.asmx", "");


                    string urlpath;
                    string categorycode;

                    categorycode = dgr.Cells["category_code"].Value.ToString();
                    categorycode = categorycode.ToLower();
                    
                    if (categorycode == "link") {
                        urlpath =  virtual_path;
                    } else {
                        urlpath = serverPath + @"uploads/images/" + virtual_path;
                    }
                    

                    Process cmd = new Process();
                    cmd.StartInfo.FileName = "cmd.exe";
                    cmd.StartInfo.RedirectStandardInput = true;
                    cmd.StartInfo.RedirectStandardOutput = true;
                    cmd.StartInfo.CreateNoWindow = true;
                    cmd.StartInfo.UseShellExecute = false;
                    cmd.Start();

                    cmd.StandardInput.WriteLine("explorer \"" + urlpath + "\"");
                    cmd.StandardInput.Flush();
                    cmd.StandardInput.Close();
                    cmd.WaitForExit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
