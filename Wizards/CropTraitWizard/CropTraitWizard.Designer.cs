﻿namespace CropTraitWizard
{
    partial class CropTraitWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CropTraitWizard));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ux_bindingnavigatorForm = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorSaveAndExitButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorSaveButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorDuplicateButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorSearchButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorCropCodeName = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorCrop = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorCropTraitId = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.ActionPage = new System.Windows.Forms.TabPage();
            this.ux_labelCropTraitAttachMessage = new System.Windows.Forms.Label();
            this.ux_buttonOpenCropTraitAttach = new System.Windows.Forms.Button();
            this.ux_datagridviewCropTraitAttach = new System.Windows.Forms.DataGridView();
            this.ux_buttonNewCropTraitAttachRow = new System.Windows.Forms.Button();
            this.SourcePage = new System.Windows.Forms.TabPage();
            this.ux_splitcontainerSourcePageHorizontal = new System.Windows.Forms.SplitContainer();
            this.ux_splitcontainerSourcePageVertical = new System.Windows.Forms.SplitContainer();
            this.ux_buttonNewCropTraitCodeRow = new System.Windows.Forms.Button();
            this.ux_datagridviewCropTraitCode = new System.Windows.Forms.DataGridView();
            this.ux_buttonNewropTraitCodeLangRow = new System.Windows.Forms.Button();
            this.ux_datagridviewCropTraitCodeLang = new System.Windows.Forms.DataGridView();
            this.ux_labelCropTraitCodeAttachMessage = new System.Windows.Forms.Label();
            this.ux_buttonOpenCropTraitCodeAttach = new System.Windows.Forms.Button();
            this.ux_buttonNewCropTraitCodeAttachRow = new System.Windows.Forms.Button();
            this.ux_datagridviewCropTraitCodeAttach = new System.Windows.Forms.DataGridView();
            this.AccessionPage = new System.Windows.Forms.TabPage();
            this.ux_textboxAccessionRowError = new System.Windows.Forms.TextBox();
            this.ux_groupbox1 = new System.Windows.Forms.GroupBox();
            this.ux_labelDescription = new System.Windows.Forms.Label();
            this.ux_textboxTitle = new System.Windows.Forms.TextBox();
            this.ux_textboxDescription = new System.Windows.Forms.TextBox();
            this.ux_labelTitle = new System.Windows.Forms.Label();
            this.ux_groupbox2 = new System.Windows.Forms.GroupBox();
            this.ux_buttonNewCropTraitLangRow = new System.Windows.Forms.Button();
            this.ux_datagridviewCropTraitLang = new System.Windows.Forms.DataGridView();
            this.ux_groupbox3 = new System.Windows.Forms.GroupBox();
            this.ux_textBoxNumericFormat = new System.Windows.Forms.TextBox();
            this.ux_labelNumericFormat = new System.Windows.Forms.Label();
            this.ux_textBoxNumericMaximum = new System.Windows.Forms.TextBox();
            this.ux_labelNumericMaximum = new System.Windows.Forms.Label();
            this.ux_textBoxNumericMinimum = new System.Windows.Forms.TextBox();
            this.ux_labelNumericMinimum = new System.Windows.Forms.Label();
            this.ux_textBoxMaxLength = new System.Windows.Forms.TextBox();
            this.ux_labelMaxLength = new System.Windows.Forms.Label();
            this.ux_labelOntologyUrl = new System.Windows.Forms.Label();
            this.ux_textboxOntologyUrl = new System.Windows.Forms.TextBox();
            this.ux_textboxNote = new System.Windows.Forms.TextBox();
            this.ux_labelNote = new System.Windows.Forms.Label();
            this.ux_textboxCropId = new System.Windows.Forms.TextBox();
            this.ux_labelTaxonomy = new System.Windows.Forms.Label();
            this.ux_checkboxIsArchived = new System.Windows.Forms.CheckBox();
            this.ux_checkboxIsCoded = new System.Windows.Forms.CheckBox();
            this.ux_checkboxIsPeerReviewed = new System.Windows.Forms.CheckBox();
            this.ux_textboxCodedName = new System.Windows.Forms.TextBox();
            this.ux_comboboxDataTypeCode = new System.Windows.Forms.ComboBox();
            this.ux_labelNumber = new System.Windows.Forms.Label();
            this.ux_comboboxCategoryCode = new System.Windows.Forms.ComboBox();
            this.ux_labelStatus = new System.Windows.Forms.Label();
            this.ux_labelInitialMaterialType = new System.Windows.Forms.Label();
            this.ux_tabcontrolMain = new System.Windows.Forms.TabControl();
            ((System.ComponentModel.ISupportInitialize)(this.ux_bindingnavigatorForm)).BeginInit();
            this.ux_bindingnavigatorForm.SuspendLayout();
            this.ActionPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewCropTraitAttach)).BeginInit();
            this.SourcePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerSourcePageHorizontal)).BeginInit();
            this.ux_splitcontainerSourcePageHorizontal.Panel1.SuspendLayout();
            this.ux_splitcontainerSourcePageHorizontal.Panel2.SuspendLayout();
            this.ux_splitcontainerSourcePageHorizontal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerSourcePageVertical)).BeginInit();
            this.ux_splitcontainerSourcePageVertical.Panel1.SuspendLayout();
            this.ux_splitcontainerSourcePageVertical.Panel2.SuspendLayout();
            this.ux_splitcontainerSourcePageVertical.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewCropTraitCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewCropTraitCodeLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewCropTraitCodeAttach)).BeginInit();
            this.AccessionPage.SuspendLayout();
            this.ux_groupbox1.SuspendLayout();
            this.ux_groupbox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewCropTraitLang)).BeginInit();
            this.ux_groupbox3.SuspendLayout();
            this.ux_tabcontrolMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_bindingnavigatorForm
            // 
            this.ux_bindingnavigatorForm.AddNewItem = this.bindingNavigatorAddNewItem;
            this.ux_bindingnavigatorForm.CountItem = this.bindingNavigatorCountItem;
            this.ux_bindingnavigatorForm.DeleteItem = this.bindingNavigatorDeleteItem;
            this.ux_bindingnavigatorForm.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ux_bindingnavigatorForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.bindingNavigatorSaveAndExitButton,
            this.toolStripSeparator7,
            this.bindingNavigatorSaveButton,
            this.toolStripSeparator1,
            this.bindingNavigatorDuplicateButton,
            this.toolStripSeparator9,
            this.bindingNavigatorSearchButton,
            this.toolStripSeparator8,
            this.bindingNavigatorProgressBar,
            this.toolStripSeparator3,
            this.toolStripLabel1,
            this.toolStripSeparator5,
            this.bindingNavigatorCropCodeName,
            this.toolStripSeparator4,
            this.bindingNavigatorCrop,
            this.toolStripSeparator2,
            this.bindingNavigatorCropTraitId,
            this.toolStripSeparator6});
            this.ux_bindingnavigatorForm.Location = new System.Drawing.Point(0, 0);
            this.ux_bindingnavigatorForm.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.ux_bindingnavigatorForm.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.ux_bindingnavigatorForm.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.ux_bindingnavigatorForm.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.ux_bindingnavigatorForm.Name = "ux_bindingnavigatorForm";
            this.ux_bindingnavigatorForm.PositionItem = this.bindingNavigatorPositionItem;
            this.ux_bindingnavigatorForm.Size = new System.Drawing.Size(841, 25);
            this.ux_bindingnavigatorForm.TabIndex = 24;
            this.ux_bindingnavigatorForm.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorSaveAndExitButton
            // 
            this.bindingNavigatorSaveAndExitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.bindingNavigatorSaveAndExitButton.Image = global::CropTraitWizard.Properties.Resources.GG_save;
            this.bindingNavigatorSaveAndExitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorSaveAndExitButton.Name = "bindingNavigatorSaveAndExitButton";
            this.bindingNavigatorSaveAndExitButton.Size = new System.Drawing.Size(96, 22);
            this.bindingNavigatorSaveAndExitButton.Text = "Save and Exit";
            this.bindingNavigatorSaveAndExitButton.Click += new System.EventHandler(this.bindingNavigatorSaveAndExitButton_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorSaveButton
            // 
            this.bindingNavigatorSaveButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.bindingNavigatorSaveButton.Image = global::CropTraitWizard.Properties.Resources.GG_save;
            this.bindingNavigatorSaveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorSaveButton.Name = "bindingNavigatorSaveButton";
            this.bindingNavigatorSaveButton.Size = new System.Drawing.Size(51, 22);
            this.bindingNavigatorSaveButton.Text = "Save";
            this.bindingNavigatorSaveButton.Click += new System.EventHandler(this.bindingNavigatorSaveButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorDuplicateButton
            // 
            this.bindingNavigatorDuplicateButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.bindingNavigatorDuplicateButton.Image = global::CropTraitWizard.Properties.Resources.i8_Copy;
            this.bindingNavigatorDuplicateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorDuplicateButton.Name = "bindingNavigatorDuplicateButton";
            this.bindingNavigatorDuplicateButton.Size = new System.Drawing.Size(77, 22);
            this.bindingNavigatorDuplicateButton.Text = "Duplicate";
            this.bindingNavigatorDuplicateButton.Click += new System.EventHandler(this.bindingNavigatorDuplicateButton_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorSearchButton
            // 
            this.bindingNavigatorSearchButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.bindingNavigatorSearchButton.Image = global::CropTraitWizard.Properties.Resources.i8_search;
            this.bindingNavigatorSearchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorSearchButton.Name = "bindingNavigatorSearchButton";
            this.bindingNavigatorSearchButton.Size = new System.Drawing.Size(62, 22);
            this.bindingNavigatorSearchButton.Text = "Search";
            this.bindingNavigatorSearchButton.Click += new System.EventHandler(this.bindingNavigatorSearchButton_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorProgressBar
            // 
            this.bindingNavigatorProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.bindingNavigatorProgressBar.AutoSize = false;
            this.bindingNavigatorProgressBar.Name = "bindingNavigatorProgressBar";
            this.bindingNavigatorProgressBar.Size = new System.Drawing.Size(100, 22);
            this.bindingNavigatorProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.bindingNavigatorProgressBar.Visible = false;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(37, 22);
            this.toolStripLabel1.Text = "          ";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorCropCodeName
            // 
            this.bindingNavigatorCropCodeName.Name = "bindingNavigatorCropCodeName";
            this.bindingNavigatorCropCodeName.Size = new System.Drawing.Size(73, 22);
            this.bindingNavigatorCropCodeName.Text = "coded name";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorCrop
            // 
            this.bindingNavigatorCrop.Name = "bindingNavigatorCrop";
            this.bindingNavigatorCrop.Size = new System.Drawing.Size(33, 22);
            this.bindingNavigatorCrop.Text = "Crop";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorCropTraitId
            // 
            this.bindingNavigatorCropTraitId.Name = "bindingNavigatorCropTraitId";
            this.bindingNavigatorCropTraitId.Size = new System.Drawing.Size(17, 22);
            this.bindingNavigatorCropTraitId.Text = "Id";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // ActionPage
            // 
            this.ActionPage.BackColor = System.Drawing.Color.Transparent;
            this.ActionPage.Controls.Add(this.ux_labelCropTraitAttachMessage);
            this.ActionPage.Controls.Add(this.ux_buttonOpenCropTraitAttach);
            this.ActionPage.Controls.Add(this.ux_datagridviewCropTraitAttach);
            this.ActionPage.Controls.Add(this.ux_buttonNewCropTraitAttachRow);
            this.ActionPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActionPage.Location = new System.Drawing.Point(4, 22);
            this.ActionPage.Name = "ActionPage";
            this.ActionPage.Padding = new System.Windows.Forms.Padding(3);
            this.ActionPage.Size = new System.Drawing.Size(833, 643);
            this.ActionPage.TabIndex = 10;
            // 
            // ux_labelCropTraitAttachMessage
            // 
            this.ux_labelCropTraitAttachMessage.AutoSize = true;
            this.ux_labelCropTraitAttachMessage.BackColor = System.Drawing.SystemColors.Info;
            this.ux_labelCropTraitAttachMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_labelCropTraitAttachMessage.Location = new System.Drawing.Point(339, 8);
            this.ux_labelCropTraitAttachMessage.Name = "ux_labelCropTraitAttachMessage";
            this.ux_labelCropTraitAttachMessage.Size = new System.Drawing.Size(237, 13);
            this.ux_labelCropTraitAttachMessage.TabIndex = 9;
            this.ux_labelCropTraitAttachMessage.Text = "Note: To attach a file, click in \"Source File Path\"";
            this.ux_labelCropTraitAttachMessage.Visible = false;
            // 
            // ux_buttonOpenCropTraitAttach
            // 
            this.ux_buttonOpenCropTraitAttach.Location = new System.Drawing.Point(173, 3);
            this.ux_buttonOpenCropTraitAttach.Name = "ux_buttonOpenCropTraitAttach";
            this.ux_buttonOpenCropTraitAttach.Size = new System.Drawing.Size(160, 23);
            this.ux_buttonOpenCropTraitAttach.TabIndex = 8;
            this.ux_buttonOpenCropTraitAttach.Text = "Open in browser";
            this.ux_buttonOpenCropTraitAttach.UseVisualStyleBackColor = true;
            this.ux_buttonOpenCropTraitAttach.Visible = false;
            this.ux_buttonOpenCropTraitAttach.Click += new System.EventHandler(this.ux_buttonOpenCropTraitAttach_Click);
            // 
            // ux_datagridviewCropTraitAttach
            // 
            this.ux_datagridviewCropTraitAttach.AllowUserToAddRows = false;
            this.ux_datagridviewCropTraitAttach.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_datagridviewCropTraitAttach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewCropTraitAttach.Location = new System.Drawing.Point(1, 32);
            this.ux_datagridviewCropTraitAttach.Name = "ux_datagridviewCropTraitAttach";
            this.ux_datagridviewCropTraitAttach.Size = new System.Drawing.Size(844, 611);
            this.ux_datagridviewCropTraitAttach.TabIndex = 7;
            this.ux_datagridviewCropTraitAttach.Visible = false;
            this.ux_datagridviewCropTraitAttach.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ux_datagridview_CellFormatting);
            this.ux_datagridviewCropTraitAttach.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.ux_datagridview_CellParsing);
            this.ux_datagridviewCropTraitAttach.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ux_datagridview_DataError);
            this.ux_datagridviewCropTraitAttach.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ux_datagridview_EditingControlShowing);
            this.ux_datagridviewCropTraitAttach.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ux_datagridview_KeyDown);
            // 
            // ux_buttonNewCropTraitAttachRow
            // 
            this.ux_buttonNewCropTraitAttachRow.Location = new System.Drawing.Point(7, 3);
            this.ux_buttonNewCropTraitAttachRow.Name = "ux_buttonNewCropTraitAttachRow";
            this.ux_buttonNewCropTraitAttachRow.Size = new System.Drawing.Size(160, 23);
            this.ux_buttonNewCropTraitAttachRow.TabIndex = 6;
            this.ux_buttonNewCropTraitAttachRow.Text = "New Crop Trait Attach";
            this.ux_buttonNewCropTraitAttachRow.UseVisualStyleBackColor = true;
            this.ux_buttonNewCropTraitAttachRow.Visible = false;
            this.ux_buttonNewCropTraitAttachRow.Click += new System.EventHandler(this.ux_buttonNewCropTraitAttachRow_Click);
            // 
            // SourcePage
            // 
            this.SourcePage.Controls.Add(this.ux_splitcontainerSourcePageHorizontal);
            this.SourcePage.Location = new System.Drawing.Point(4, 22);
            this.SourcePage.Name = "SourcePage";
            this.SourcePage.Size = new System.Drawing.Size(833, 643);
            this.SourcePage.TabIndex = 2;
            this.SourcePage.Text = "Crop Trait Code";
            this.SourcePage.UseVisualStyleBackColor = true;
            // 
            // ux_splitcontainerSourcePageHorizontal
            // 
            this.ux_splitcontainerSourcePageHorizontal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ux_splitcontainerSourcePageHorizontal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_splitcontainerSourcePageHorizontal.Location = new System.Drawing.Point(0, 0);
            this.ux_splitcontainerSourcePageHorizontal.Name = "ux_splitcontainerSourcePageHorizontal";
            this.ux_splitcontainerSourcePageHorizontal.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ux_splitcontainerSourcePageHorizontal.Panel1
            // 
            this.ux_splitcontainerSourcePageHorizontal.Panel1.Controls.Add(this.ux_splitcontainerSourcePageVertical);
            // 
            // ux_splitcontainerSourcePageHorizontal.Panel2
            // 
            this.ux_splitcontainerSourcePageHorizontal.Panel2.Controls.Add(this.ux_labelCropTraitCodeAttachMessage);
            this.ux_splitcontainerSourcePageHorizontal.Panel2.Controls.Add(this.ux_buttonOpenCropTraitCodeAttach);
            this.ux_splitcontainerSourcePageHorizontal.Panel2.Controls.Add(this.ux_buttonNewCropTraitCodeAttachRow);
            this.ux_splitcontainerSourcePageHorizontal.Panel2.Controls.Add(this.ux_datagridviewCropTraitCodeAttach);
            this.ux_splitcontainerSourcePageHorizontal.Size = new System.Drawing.Size(833, 643);
            this.ux_splitcontainerSourcePageHorizontal.SplitterDistance = 289;
            this.ux_splitcontainerSourcePageHorizontal.TabIndex = 8;
            // 
            // ux_splitcontainerSourcePageVertical
            // 
            this.ux_splitcontainerSourcePageVertical.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_splitcontainerSourcePageVertical.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ux_splitcontainerSourcePageVertical.Location = new System.Drawing.Point(0, 0);
            this.ux_splitcontainerSourcePageVertical.Name = "ux_splitcontainerSourcePageVertical";
            // 
            // ux_splitcontainerSourcePageVertical.Panel1
            // 
            this.ux_splitcontainerSourcePageVertical.Panel1.Controls.Add(this.ux_buttonNewCropTraitCodeRow);
            this.ux_splitcontainerSourcePageVertical.Panel1.Controls.Add(this.ux_datagridviewCropTraitCode);
            // 
            // ux_splitcontainerSourcePageVertical.Panel2
            // 
            this.ux_splitcontainerSourcePageVertical.Panel2.Controls.Add(this.ux_buttonNewropTraitCodeLangRow);
            this.ux_splitcontainerSourcePageVertical.Panel2.Controls.Add(this.ux_datagridviewCropTraitCodeLang);
            this.ux_splitcontainerSourcePageVertical.Size = new System.Drawing.Size(826, 282);
            this.ux_splitcontainerSourcePageVertical.SplitterDistance = 408;
            this.ux_splitcontainerSourcePageVertical.TabIndex = 0;
            // 
            // ux_buttonNewCropTraitCodeRow
            // 
            this.ux_buttonNewCropTraitCodeRow.Location = new System.Drawing.Point(8, 4);
            this.ux_buttonNewCropTraitCodeRow.Name = "ux_buttonNewCropTraitCodeRow";
            this.ux_buttonNewCropTraitCodeRow.Size = new System.Drawing.Size(126, 23);
            this.ux_buttonNewCropTraitCodeRow.TabIndex = 6;
            this.ux_buttonNewCropTraitCodeRow.Text = "New Crop Trait Code";
            this.ux_buttonNewCropTraitCodeRow.UseVisualStyleBackColor = true;
            this.ux_buttonNewCropTraitCodeRow.Click += new System.EventHandler(this.ux_buttonNewCropTraitCodeRow_Click);
            // 
            // ux_datagridviewCropTraitCode
            // 
            this.ux_datagridviewCropTraitCode.AllowUserToAddRows = false;
            this.ux_datagridviewCropTraitCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewCropTraitCode.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.ux_datagridviewCropTraitCode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ux_datagridviewCropTraitCode.DefaultCellStyle = dataGridViewCellStyle18;
            this.ux_datagridviewCropTraitCode.Location = new System.Drawing.Point(2, 33);
            this.ux_datagridviewCropTraitCode.Name = "ux_datagridviewCropTraitCode";
            this.ux_datagridviewCropTraitCode.Size = new System.Drawing.Size(399, 242);
            this.ux_datagridviewCropTraitCode.TabIndex = 7;
            this.ux_datagridviewCropTraitCode.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ux_datagridview_CellFormatting);
            this.ux_datagridviewCropTraitCode.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.ux_datagridview_CellParsing);
            this.ux_datagridviewCropTraitCode.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ux_datagridview_DataError);
            this.ux_datagridviewCropTraitCode.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ux_datagridview_EditingControlShowing);
            this.ux_datagridviewCropTraitCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ux_datagridview_KeyDown);
            // 
            // ux_buttonNewropTraitCodeLangRow
            // 
            this.ux_buttonNewropTraitCodeLangRow.Location = new System.Drawing.Point(10, 4);
            this.ux_buttonNewropTraitCodeLangRow.Name = "ux_buttonNewropTraitCodeLangRow";
            this.ux_buttonNewropTraitCodeLangRow.Size = new System.Drawing.Size(160, 23);
            this.ux_buttonNewropTraitCodeLangRow.TabIndex = 5;
            this.ux_buttonNewropTraitCodeLangRow.Text = "New Crop Trait Code Lang";
            this.ux_buttonNewropTraitCodeLangRow.UseVisualStyleBackColor = true;
            this.ux_buttonNewropTraitCodeLangRow.Click += new System.EventHandler(this.ux_buttonNewropTraitCodeLangRow_Click);
            // 
            // ux_datagridviewCropTraitCodeLang
            // 
            this.ux_datagridviewCropTraitCodeLang.AllowUserToAddRows = false;
            this.ux_datagridviewCropTraitCodeLang.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewCropTraitCodeLang.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.ux_datagridviewCropTraitCodeLang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ux_datagridviewCropTraitCodeLang.DefaultCellStyle = dataGridViewCellStyle20;
            this.ux_datagridviewCropTraitCodeLang.Location = new System.Drawing.Point(3, 33);
            this.ux_datagridviewCropTraitCodeLang.Name = "ux_datagridviewCropTraitCodeLang";
            this.ux_datagridviewCropTraitCodeLang.Size = new System.Drawing.Size(404, 242);
            this.ux_datagridviewCropTraitCodeLang.TabIndex = 4;
            this.ux_datagridviewCropTraitCodeLang.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ux_datagridview_CellFormatting);
            this.ux_datagridviewCropTraitCodeLang.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.ux_datagridview_CellParsing);
            this.ux_datagridviewCropTraitCodeLang.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ux_datagridview_DataError);
            this.ux_datagridviewCropTraitCodeLang.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ux_datagridview_EditingControlShowing);
            this.ux_datagridviewCropTraitCodeLang.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ux_datagridview_KeyDown);
            // 
            // ux_labelCropTraitCodeAttachMessage
            // 
            this.ux_labelCropTraitCodeAttachMessage.AutoSize = true;
            this.ux_labelCropTraitCodeAttachMessage.BackColor = System.Drawing.SystemColors.Info;
            this.ux_labelCropTraitCodeAttachMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_labelCropTraitCodeAttachMessage.Location = new System.Drawing.Point(343, 12);
            this.ux_labelCropTraitCodeAttachMessage.Name = "ux_labelCropTraitCodeAttachMessage";
            this.ux_labelCropTraitCodeAttachMessage.Size = new System.Drawing.Size(237, 13);
            this.ux_labelCropTraitCodeAttachMessage.TabIndex = 5;
            this.ux_labelCropTraitCodeAttachMessage.Text = "Note: To attach a file, click in \"Source File Path\"";
            // 
            // ux_buttonOpenCropTraitCodeAttach
            // 
            this.ux_buttonOpenCropTraitCodeAttach.Location = new System.Drawing.Point(177, 7);
            this.ux_buttonOpenCropTraitCodeAttach.Name = "ux_buttonOpenCropTraitCodeAttach";
            this.ux_buttonOpenCropTraitCodeAttach.Size = new System.Drawing.Size(160, 23);
            this.ux_buttonOpenCropTraitCodeAttach.TabIndex = 4;
            this.ux_buttonOpenCropTraitCodeAttach.Text = "Open in browser";
            this.ux_buttonOpenCropTraitCodeAttach.UseVisualStyleBackColor = true;
            this.ux_buttonOpenCropTraitCodeAttach.Click += new System.EventHandler(this.ux_buttonOpenCropTraitCodeAttach_Click);
            // 
            // ux_buttonNewCropTraitCodeAttachRow
            // 
            this.ux_buttonNewCropTraitCodeAttachRow.Location = new System.Drawing.Point(11, 7);
            this.ux_buttonNewCropTraitCodeAttachRow.Name = "ux_buttonNewCropTraitCodeAttachRow";
            this.ux_buttonNewCropTraitCodeAttachRow.Size = new System.Drawing.Size(160, 23);
            this.ux_buttonNewCropTraitCodeAttachRow.TabIndex = 3;
            this.ux_buttonNewCropTraitCodeAttachRow.Text = "New Crop Trait Code Attach";
            this.ux_buttonNewCropTraitCodeAttachRow.UseVisualStyleBackColor = true;
            this.ux_buttonNewCropTraitCodeAttachRow.Click += new System.EventHandler(this.ux_buttonNewCropTraitCodeAttachRow_Click);
            // 
            // ux_datagridviewCropTraitCodeAttach
            // 
            this.ux_datagridviewCropTraitCodeAttach.AllowUserToAddRows = false;
            this.ux_datagridviewCropTraitCodeAttach.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewCropTraitCodeAttach.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.ux_datagridviewCropTraitCodeAttach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ux_datagridviewCropTraitCodeAttach.DefaultCellStyle = dataGridViewCellStyle22;
            this.ux_datagridviewCropTraitCodeAttach.Location = new System.Drawing.Point(6, 36);
            this.ux_datagridviewCropTraitCodeAttach.Name = "ux_datagridviewCropTraitCodeAttach";
            this.ux_datagridviewCropTraitCodeAttach.Size = new System.Drawing.Size(820, 304);
            this.ux_datagridviewCropTraitCodeAttach.TabIndex = 2;
            this.ux_datagridviewCropTraitCodeAttach.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ux_datagridview_CellFormatting);
            this.ux_datagridviewCropTraitCodeAttach.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.ux_datagridview_CellParsing);
            this.ux_datagridviewCropTraitCodeAttach.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ux_datagridview_DataError);
            this.ux_datagridviewCropTraitCodeAttach.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ux_datagridview_EditingControlShowing);
            this.ux_datagridviewCropTraitCodeAttach.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ux_datagridview_KeyDown);
            // 
            // AccessionPage
            // 
            this.AccessionPage.Controls.Add(this.ux_textboxAccessionRowError);
            this.AccessionPage.Controls.Add(this.ux_groupbox1);
            this.AccessionPage.Controls.Add(this.ux_groupbox2);
            this.AccessionPage.Controls.Add(this.ux_groupbox3);
            this.AccessionPage.Location = new System.Drawing.Point(4, 22);
            this.AccessionPage.Name = "AccessionPage";
            this.AccessionPage.Padding = new System.Windows.Forms.Padding(3);
            this.AccessionPage.Size = new System.Drawing.Size(833, 643);
            this.AccessionPage.TabIndex = 0;
            this.AccessionPage.Text = "Crop Trait";
            this.AccessionPage.UseVisualStyleBackColor = true;
            // 
            // ux_textboxAccessionRowError
            // 
            this.ux_textboxAccessionRowError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxAccessionRowError.ForeColor = System.Drawing.Color.Red;
            this.ux_textboxAccessionRowError.Location = new System.Drawing.Point(11, 603);
            this.ux_textboxAccessionRowError.Multiline = true;
            this.ux_textboxAccessionRowError.Name = "ux_textboxAccessionRowError";
            this.ux_textboxAccessionRowError.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxAccessionRowError.Size = new System.Drawing.Size(812, 37);
            this.ux_textboxAccessionRowError.TabIndex = 107;
            this.ux_textboxAccessionRowError.TabStop = false;
            // 
            // ux_groupbox1
            // 
            this.ux_groupbox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupbox1.Controls.Add(this.ux_labelDescription);
            this.ux_groupbox1.Controls.Add(this.ux_textboxTitle);
            this.ux_groupbox1.Controls.Add(this.ux_textboxDescription);
            this.ux_groupbox1.Controls.Add(this.ux_labelTitle);
            this.ux_groupbox1.Location = new System.Drawing.Point(11, 0);
            this.ux_groupbox1.Name = "ux_groupbox1";
            this.ux_groupbox1.Size = new System.Drawing.Size(812, 104);
            this.ux_groupbox1.TabIndex = 103;
            this.ux_groupbox1.TabStop = false;
            // 
            // ux_labelDescription
            // 
            this.ux_labelDescription.AutoSize = true;
            this.ux_labelDescription.Location = new System.Drawing.Point(15, 52);
            this.ux_labelDescription.Name = "ux_labelDescription";
            this.ux_labelDescription.Size = new System.Drawing.Size(58, 13);
            this.ux_labelDescription.TabIndex = 113;
            this.ux_labelDescription.Tag = "description";
            this.ux_labelDescription.Text = "description";
            // 
            // ux_textboxTitle
            // 
            this.ux_textboxTitle.Location = new System.Drawing.Point(18, 32);
            this.ux_textboxTitle.Name = "ux_textboxTitle";
            this.ux_textboxTitle.Size = new System.Drawing.Size(328, 20);
            this.ux_textboxTitle.TabIndex = 200;
            this.ux_textboxTitle.Tag = "title";
            // 
            // ux_textboxDescription
            // 
            this.ux_textboxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxDescription.Location = new System.Drawing.Point(18, 68);
            this.ux_textboxDescription.Name = "ux_textboxDescription";
            this.ux_textboxDescription.Size = new System.Drawing.Size(777, 20);
            this.ux_textboxDescription.TabIndex = 13;
            this.ux_textboxDescription.Tag = "description";
            // 
            // ux_labelTitle
            // 
            this.ux_labelTitle.AutoSize = true;
            this.ux_labelTitle.Location = new System.Drawing.Point(15, 16);
            this.ux_labelTitle.Name = "ux_labelTitle";
            this.ux_labelTitle.Size = new System.Drawing.Size(23, 13);
            this.ux_labelTitle.TabIndex = 111;
            this.ux_labelTitle.Tag = "title";
            this.ux_labelTitle.Text = "title";
            // 
            // ux_groupbox2
            // 
            this.ux_groupbox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupbox2.Controls.Add(this.ux_buttonNewCropTraitLangRow);
            this.ux_groupbox2.Controls.Add(this.ux_datagridviewCropTraitLang);
            this.ux_groupbox2.Location = new System.Drawing.Point(11, 422);
            this.ux_groupbox2.Name = "ux_groupbox2";
            this.ux_groupbox2.Size = new System.Drawing.Size(812, 175);
            this.ux_groupbox2.TabIndex = 105;
            this.ux_groupbox2.TabStop = false;
            // 
            // ux_buttonNewCropTraitLangRow
            // 
            this.ux_buttonNewCropTraitLangRow.Location = new System.Drawing.Point(11, 12);
            this.ux_buttonNewCropTraitLangRow.Name = "ux_buttonNewCropTraitLangRow";
            this.ux_buttonNewCropTraitLangRow.Size = new System.Drawing.Size(109, 23);
            this.ux_buttonNewCropTraitLangRow.TabIndex = 10;
            this.ux_buttonNewCropTraitLangRow.Text = "New Trait Lang";
            this.ux_buttonNewCropTraitLangRow.UseVisualStyleBackColor = true;
            this.ux_buttonNewCropTraitLangRow.Click += new System.EventHandler(this.ux_buttonNewCropTraitLangRow_Click);
            // 
            // ux_datagridviewCropTraitLang
            // 
            this.ux_datagridviewCropTraitLang.AllowUserToAddRows = false;
            this.ux_datagridviewCropTraitLang.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewCropTraitLang.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.ux_datagridviewCropTraitLang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ux_datagridviewCropTraitLang.DefaultCellStyle = dataGridViewCellStyle24;
            this.ux_datagridviewCropTraitLang.Location = new System.Drawing.Point(6, 41);
            this.ux_datagridviewCropTraitLang.Name = "ux_datagridviewCropTraitLang";
            this.ux_datagridviewCropTraitLang.Size = new System.Drawing.Size(800, 128);
            this.ux_datagridviewCropTraitLang.TabIndex = 4;
            this.ux_datagridviewCropTraitLang.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ux_datagridview_CellFormatting);
            this.ux_datagridviewCropTraitLang.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.ux_datagridview_CellParsing);
            this.ux_datagridviewCropTraitLang.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ux_datagridview_DataError);
            this.ux_datagridviewCropTraitLang.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ux_datagridview_EditingControlShowing);
            this.ux_datagridviewCropTraitLang.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ux_datagridview_KeyDown);
            this.ux_datagridviewCropTraitLang.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.ux_datagridview_PreviewKeyDown);
            // 
            // ux_groupbox3
            // 
            this.ux_groupbox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupbox3.Controls.Add(this.ux_textBoxNumericFormat);
            this.ux_groupbox3.Controls.Add(this.ux_labelNumericFormat);
            this.ux_groupbox3.Controls.Add(this.ux_textBoxNumericMaximum);
            this.ux_groupbox3.Controls.Add(this.ux_labelNumericMaximum);
            this.ux_groupbox3.Controls.Add(this.ux_textBoxNumericMinimum);
            this.ux_groupbox3.Controls.Add(this.ux_labelNumericMinimum);
            this.ux_groupbox3.Controls.Add(this.ux_textBoxMaxLength);
            this.ux_groupbox3.Controls.Add(this.ux_labelMaxLength);
            this.ux_groupbox3.Controls.Add(this.ux_labelOntologyUrl);
            this.ux_groupbox3.Controls.Add(this.ux_textboxOntologyUrl);
            this.ux_groupbox3.Controls.Add(this.ux_textboxNote);
            this.ux_groupbox3.Controls.Add(this.ux_labelNote);
            this.ux_groupbox3.Controls.Add(this.ux_textboxCropId);
            this.ux_groupbox3.Controls.Add(this.ux_labelTaxonomy);
            this.ux_groupbox3.Controls.Add(this.ux_checkboxIsArchived);
            this.ux_groupbox3.Controls.Add(this.ux_checkboxIsCoded);
            this.ux_groupbox3.Controls.Add(this.ux_checkboxIsPeerReviewed);
            this.ux_groupbox3.Controls.Add(this.ux_textboxCodedName);
            this.ux_groupbox3.Controls.Add(this.ux_comboboxDataTypeCode);
            this.ux_groupbox3.Controls.Add(this.ux_labelNumber);
            this.ux_groupbox3.Controls.Add(this.ux_comboboxCategoryCode);
            this.ux_groupbox3.Controls.Add(this.ux_labelStatus);
            this.ux_groupbox3.Controls.Add(this.ux_labelInitialMaterialType);
            this.ux_groupbox3.Location = new System.Drawing.Point(11, 110);
            this.ux_groupbox3.Name = "ux_groupbox3";
            this.ux_groupbox3.Size = new System.Drawing.Size(812, 306);
            this.ux_groupbox3.TabIndex = 104;
            this.ux_groupbox3.TabStop = false;
            // 
            // ux_textBoxNumericFormat
            // 
            this.ux_textBoxNumericFormat.Location = new System.Drawing.Point(604, 109);
            this.ux_textBoxNumericFormat.Name = "ux_textBoxNumericFormat";
            this.ux_textBoxNumericFormat.Size = new System.Drawing.Size(160, 20);
            this.ux_textBoxNumericFormat.TabIndex = 127;
            this.ux_textBoxNumericFormat.Tag = "numeric_format";
            // 
            // ux_labelNumericFormat
            // 
            this.ux_labelNumericFormat.AutoSize = true;
            this.ux_labelNumericFormat.Location = new System.Drawing.Point(601, 93);
            this.ux_labelNumericFormat.Name = "ux_labelNumericFormat";
            this.ux_labelNumericFormat.Size = new System.Drawing.Size(79, 13);
            this.ux_labelNumericFormat.TabIndex = 130;
            this.ux_labelNumericFormat.Tag = "numeric_format";
            this.ux_labelNumericFormat.Text = "numeric_format";
            // 
            // ux_textBoxNumericMaximum
            // 
            this.ux_textBoxNumericMaximum.Location = new System.Drawing.Point(408, 109);
            this.ux_textBoxNumericMaximum.Name = "ux_textBoxNumericMaximum";
            this.ux_textBoxNumericMaximum.Size = new System.Drawing.Size(160, 20);
            this.ux_textBoxNumericMaximum.TabIndex = 127;
            this.ux_textBoxNumericMaximum.Tag = "numeric_maximum";
            // 
            // ux_labelNumericMaximum
            // 
            this.ux_labelNumericMaximum.AutoSize = true;
            this.ux_labelNumericMaximum.Location = new System.Drawing.Point(405, 93);
            this.ux_labelNumericMaximum.Name = "ux_labelNumericMaximum";
            this.ux_labelNumericMaximum.Size = new System.Drawing.Size(93, 13);
            this.ux_labelNumericMaximum.TabIndex = 130;
            this.ux_labelNumericMaximum.Tag = "numeric_maximum";
            this.ux_labelNumericMaximum.Text = "numeric_maximum";
            // 
            // ux_textBoxNumericMinimum
            // 
            this.ux_textBoxNumericMinimum.Location = new System.Drawing.Point(211, 109);
            this.ux_textBoxNumericMinimum.Name = "ux_textBoxNumericMinimum";
            this.ux_textBoxNumericMinimum.Size = new System.Drawing.Size(160, 20);
            this.ux_textBoxNumericMinimum.TabIndex = 126;
            this.ux_textBoxNumericMinimum.Tag = "numeric_minimum";
            // 
            // ux_labelNumericMinimum
            // 
            this.ux_labelNumericMinimum.AutoSize = true;
            this.ux_labelNumericMinimum.Location = new System.Drawing.Point(208, 93);
            this.ux_labelNumericMinimum.Name = "ux_labelNumericMinimum";
            this.ux_labelNumericMinimum.Size = new System.Drawing.Size(90, 13);
            this.ux_labelNumericMinimum.TabIndex = 129;
            this.ux_labelNumericMinimum.Tag = "numeric_minimum";
            this.ux_labelNumericMinimum.Text = "numeric_minimum";
            // 
            // ux_textBoxMaxLength
            // 
            this.ux_textBoxMaxLength.Location = new System.Drawing.Point(18, 109);
            this.ux_textBoxMaxLength.Name = "ux_textBoxMaxLength";
            this.ux_textBoxMaxLength.Size = new System.Drawing.Size(160, 20);
            this.ux_textBoxMaxLength.TabIndex = 125;
            this.ux_textBoxMaxLength.Tag = "max_length";
            // 
            // ux_labelMaxLength
            // 
            this.ux_labelMaxLength.AutoSize = true;
            this.ux_labelMaxLength.Location = new System.Drawing.Point(15, 93);
            this.ux_labelMaxLength.Name = "ux_labelMaxLength";
            this.ux_labelMaxLength.Size = new System.Drawing.Size(61, 13);
            this.ux_labelMaxLength.TabIndex = 128;
            this.ux_labelMaxLength.Tag = "max_length";
            this.ux_labelMaxLength.Text = "max_length";
            // 
            // ux_labelOntologyUrl
            // 
            this.ux_labelOntologyUrl.AutoSize = true;
            this.ux_labelOntologyUrl.Location = new System.Drawing.Point(15, 140);
            this.ux_labelOntologyUrl.Name = "ux_labelOntologyUrl";
            this.ux_labelOntologyUrl.Size = new System.Drawing.Size(64, 13);
            this.ux_labelOntologyUrl.TabIndex = 118;
            this.ux_labelOntologyUrl.Tag = "ontology_url";
            this.ux_labelOntologyUrl.Text = "ontology_url";
            // 
            // ux_textboxOntologyUrl
            // 
            this.ux_textboxOntologyUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxOntologyUrl.Location = new System.Drawing.Point(18, 156);
            this.ux_textboxOntologyUrl.Name = "ux_textboxOntologyUrl";
            this.ux_textboxOntologyUrl.Size = new System.Drawing.Size(777, 20);
            this.ux_textboxOntologyUrl.TabIndex = 8;
            this.ux_textboxOntologyUrl.Tag = "ontology_url";
            // 
            // ux_textboxNote
            // 
            this.ux_textboxNote.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxNote.Location = new System.Drawing.Point(18, 207);
            this.ux_textboxNote.Multiline = true;
            this.ux_textboxNote.Name = "ux_textboxNote";
            this.ux_textboxNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxNote.Size = new System.Drawing.Size(777, 84);
            this.ux_textboxNote.TabIndex = 9;
            this.ux_textboxNote.Tag = "note";
            // 
            // ux_labelNote
            // 
            this.ux_labelNote.AutoSize = true;
            this.ux_labelNote.Location = new System.Drawing.Point(15, 191);
            this.ux_labelNote.Name = "ux_labelNote";
            this.ux_labelNote.Size = new System.Drawing.Size(28, 13);
            this.ux_labelNote.TabIndex = 116;
            this.ux_labelNote.Tag = "note";
            this.ux_labelNote.Text = "note";
            // 
            // ux_textboxCropId
            // 
            this.ux_textboxCropId.BackColor = System.Drawing.Color.Plum;
            this.ux_textboxCropId.Location = new System.Drawing.Point(211, 30);
            this.ux_textboxCropId.Name = "ux_textboxCropId";
            this.ux_textboxCropId.Size = new System.Drawing.Size(160, 20);
            this.ux_textboxCropId.TabIndex = 2;
            this.ux_textboxCropId.Tag = "crop_id";
            // 
            // ux_labelTaxonomy
            // 
            this.ux_labelTaxonomy.AutoSize = true;
            this.ux_labelTaxonomy.Location = new System.Drawing.Point(218, 14);
            this.ux_labelTaxonomy.Name = "ux_labelTaxonomy";
            this.ux_labelTaxonomy.Size = new System.Drawing.Size(42, 13);
            this.ux_labelTaxonomy.TabIndex = 104;
            this.ux_labelTaxonomy.Tag = "crop_id";
            this.ux_labelTaxonomy.Text = "crop_id";
            // 
            // ux_checkboxIsArchived
            // 
            this.ux_checkboxIsArchived.Location = new System.Drawing.Point(211, 65);
            this.ux_checkboxIsArchived.Name = "ux_checkboxIsArchived";
            this.ux_checkboxIsArchived.Size = new System.Drawing.Size(120, 17);
            this.ux_checkboxIsArchived.TabIndex = 6;
            this.ux_checkboxIsArchived.Tag = "is_archived";
            this.ux_checkboxIsArchived.Text = "is_archived";
            this.ux_checkboxIsArchived.UseVisualStyleBackColor = true;
            // 
            // ux_checkboxIsCoded
            // 
            this.ux_checkboxIsCoded.Location = new System.Drawing.Point(408, 65);
            this.ux_checkboxIsCoded.Name = "ux_checkboxIsCoded";
            this.ux_checkboxIsCoded.Size = new System.Drawing.Size(117, 17);
            this.ux_checkboxIsCoded.TabIndex = 7;
            this.ux_checkboxIsCoded.Tag = "is_coded";
            this.ux_checkboxIsCoded.Text = "is_coded";
            this.ux_checkboxIsCoded.UseVisualStyleBackColor = true;
            // 
            // ux_checkboxIsPeerReviewed
            // 
            this.ux_checkboxIsPeerReviewed.Location = new System.Drawing.Point(18, 65);
            this.ux_checkboxIsPeerReviewed.Name = "ux_checkboxIsPeerReviewed";
            this.ux_checkboxIsPeerReviewed.Size = new System.Drawing.Size(139, 17);
            this.ux_checkboxIsPeerReviewed.TabIndex = 5;
            this.ux_checkboxIsPeerReviewed.Tag = "is_peer_reviewed";
            this.ux_checkboxIsPeerReviewed.Text = "is_peer_reviewed";
            this.ux_checkboxIsPeerReviewed.UseVisualStyleBackColor = true;
            // 
            // ux_textboxCodedName
            // 
            this.ux_textboxCodedName.Location = new System.Drawing.Point(18, 30);
            this.ux_textboxCodedName.Name = "ux_textboxCodedName";
            this.ux_textboxCodedName.Size = new System.Drawing.Size(160, 20);
            this.ux_textboxCodedName.TabIndex = 1;
            this.ux_textboxCodedName.Tag = "coded_name";
            // 
            // ux_comboboxDataTypeCode
            // 
            this.ux_comboboxDataTypeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboboxDataTypeCode.FormattingEnabled = true;
            this.ux_comboboxDataTypeCode.Location = new System.Drawing.Point(604, 30);
            this.ux_comboboxDataTypeCode.Name = "ux_comboboxDataTypeCode";
            this.ux_comboboxDataTypeCode.Size = new System.Drawing.Size(160, 21);
            this.ux_comboboxDataTypeCode.TabIndex = 4;
            this.ux_comboboxDataTypeCode.Tag = "data_type_code";
            this.ux_comboboxDataTypeCode.SelectedIndexChanged += new System.EventHandler(this.ux_comboboxDataTypeCode_SelectedIndexChanged);
            // 
            // ux_labelNumber
            // 
            this.ux_labelNumber.AutoSize = true;
            this.ux_labelNumber.Location = new System.Drawing.Point(15, 14);
            this.ux_labelNumber.Name = "ux_labelNumber";
            this.ux_labelNumber.Size = new System.Drawing.Size(69, 13);
            this.ux_labelNumber.TabIndex = 101;
            this.ux_labelNumber.Tag = "coded_name";
            this.ux_labelNumber.Text = "coded_name";
            // 
            // ux_comboboxCategoryCode
            // 
            this.ux_comboboxCategoryCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboboxCategoryCode.FormattingEnabled = true;
            this.ux_comboboxCategoryCode.Location = new System.Drawing.Point(408, 30);
            this.ux_comboboxCategoryCode.Name = "ux_comboboxCategoryCode";
            this.ux_comboboxCategoryCode.Size = new System.Drawing.Size(160, 21);
            this.ux_comboboxCategoryCode.TabIndex = 3;
            this.ux_comboboxCategoryCode.Tag = "category_code";
            // 
            // ux_labelStatus
            // 
            this.ux_labelStatus.AutoSize = true;
            this.ux_labelStatus.Location = new System.Drawing.Point(601, 14);
            this.ux_labelStatus.Name = "ux_labelStatus";
            this.ux_labelStatus.Size = new System.Drawing.Size(84, 13);
            this.ux_labelStatus.TabIndex = 103;
            this.ux_labelStatus.Tag = "data_type_code";
            this.ux_labelStatus.Text = "data_type_code";
            // 
            // ux_labelInitialMaterialType
            // 
            this.ux_labelInitialMaterialType.AutoSize = true;
            this.ux_labelInitialMaterialType.Location = new System.Drawing.Point(405, 14);
            this.ux_labelInitialMaterialType.Name = "ux_labelInitialMaterialType";
            this.ux_labelInitialMaterialType.Size = new System.Drawing.Size(78, 13);
            this.ux_labelInitialMaterialType.TabIndex = 107;
            this.ux_labelInitialMaterialType.Tag = "category_code";
            this.ux_labelInitialMaterialType.Text = "category_code";
            // 
            // ux_tabcontrolMain
            // 
            this.ux_tabcontrolMain.Controls.Add(this.AccessionPage);
            this.ux_tabcontrolMain.Controls.Add(this.SourcePage);
            this.ux_tabcontrolMain.Controls.Add(this.ActionPage);
            this.ux_tabcontrolMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tabcontrolMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_tabcontrolMain.Location = new System.Drawing.Point(0, 25);
            this.ux_tabcontrolMain.Name = "ux_tabcontrolMain";
            this.ux_tabcontrolMain.SelectedIndex = 0;
            this.ux_tabcontrolMain.Size = new System.Drawing.Size(841, 669);
            this.ux_tabcontrolMain.TabIndex = 203;
            this.ux_tabcontrolMain.SelectedIndexChanged += new System.EventHandler(this.ux_tabcontrolMain_SelectedIndexChanged);
            // 
            // CropTraitWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 694);
            this.Controls.Add(this.ux_tabcontrolMain);
            this.Controls.Add(this.ux_bindingnavigatorForm);
            this.Name = "CropTraitWizard";
            this.Text = "Trait Wizard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CropTraitWizard_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CropTraitWizard_FormClosed);
            this.Load += new System.EventHandler(this.CropTraitWizard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ux_bindingnavigatorForm)).EndInit();
            this.ux_bindingnavigatorForm.ResumeLayout(false);
            this.ux_bindingnavigatorForm.PerformLayout();
            this.ActionPage.ResumeLayout(false);
            this.ActionPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewCropTraitAttach)).EndInit();
            this.SourcePage.ResumeLayout(false);
            this.ux_splitcontainerSourcePageHorizontal.Panel1.ResumeLayout(false);
            this.ux_splitcontainerSourcePageHorizontal.Panel2.ResumeLayout(false);
            this.ux_splitcontainerSourcePageHorizontal.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerSourcePageHorizontal)).EndInit();
            this.ux_splitcontainerSourcePageHorizontal.ResumeLayout(false);
            this.ux_splitcontainerSourcePageVertical.Panel1.ResumeLayout(false);
            this.ux_splitcontainerSourcePageVertical.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerSourcePageVertical)).EndInit();
            this.ux_splitcontainerSourcePageVertical.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewCropTraitCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewCropTraitCodeLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewCropTraitCodeAttach)).EndInit();
            this.AccessionPage.ResumeLayout(false);
            this.AccessionPage.PerformLayout();
            this.ux_groupbox1.ResumeLayout(false);
            this.ux_groupbox1.PerformLayout();
            this.ux_groupbox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewCropTraitLang)).EndInit();
            this.ux_groupbox3.ResumeLayout(false);
            this.ux_groupbox3.PerformLayout();
            this.ux_tabcontrolMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator ux_bindingnavigatorForm;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorSaveAndExitButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton bindingNavigatorSaveButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripProgressBar bindingNavigatorProgressBar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCropCodeName;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCrop;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCropTraitId;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.TabPage ActionPage;
        private System.Windows.Forms.DataGridView ux_datagridviewCropTraitAttach;
        private System.Windows.Forms.Button ux_buttonNewCropTraitAttachRow;
        private System.Windows.Forms.TabPage SourcePage;
        private System.Windows.Forms.SplitContainer ux_splitcontainerSourcePageHorizontal;
        private System.Windows.Forms.Button ux_buttonNewCropTraitCodeAttachRow;
        private System.Windows.Forms.DataGridView ux_datagridviewCropTraitCodeAttach;
        private System.Windows.Forms.SplitContainer ux_splitcontainerSourcePageVertical;
        private System.Windows.Forms.Button ux_buttonNewCropTraitCodeRow;
        private System.Windows.Forms.DataGridView ux_datagridviewCropTraitCode;
        private System.Windows.Forms.Button ux_buttonNewropTraitCodeLangRow;
        private System.Windows.Forms.DataGridView ux_datagridviewCropTraitCodeLang;
        private System.Windows.Forms.TabPage AccessionPage;
        private System.Windows.Forms.TextBox ux_textboxAccessionRowError;
        private System.Windows.Forms.Label ux_labelNote;
        private System.Windows.Forms.TextBox ux_textboxNote;
        private System.Windows.Forms.GroupBox ux_groupbox1;
        private System.Windows.Forms.CheckBox ux_checkboxIsPeerReviewed;
        private System.Windows.Forms.ComboBox ux_comboboxDataTypeCode;
        private System.Windows.Forms.Label ux_labelStatus;
        private System.Windows.Forms.TextBox ux_textboxCodedName;
        private System.Windows.Forms.Label ux_labelNumber;
        private System.Windows.Forms.TextBox ux_textboxCropId;
        private System.Windows.Forms.Label ux_labelTaxonomy;
        private System.Windows.Forms.GroupBox ux_groupbox2;
        private System.Windows.Forms.TextBox ux_textboxTitle;
        private System.Windows.Forms.CheckBox ux_checkboxIsCoded;
        private System.Windows.Forms.CheckBox ux_checkboxIsArchived;
        private System.Windows.Forms.Label ux_labelDescription;
        private System.Windows.Forms.Label ux_labelTitle;
        private System.Windows.Forms.TextBox ux_textboxDescription;
        private System.Windows.Forms.GroupBox ux_groupbox3;
        private System.Windows.Forms.TabControl ux_tabcontrolMain;
        private System.Windows.Forms.Label ux_labelOntologyUrl;
        private System.Windows.Forms.TextBox ux_textboxOntologyUrl;
        private System.Windows.Forms.Button ux_buttonNewCropTraitLangRow;
        private System.Windows.Forms.DataGridView ux_datagridviewCropTraitLang;
        private System.Windows.Forms.ToolStripButton bindingNavigatorSearchButton;
        private System.Windows.Forms.ComboBox ux_comboboxCategoryCode;
        private System.Windows.Forms.Label ux_labelInitialMaterialType;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDuplicateButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.Button ux_buttonOpenCropTraitAttach;
        private System.Windows.Forms.Button ux_buttonOpenCropTraitCodeAttach;
        private System.Windows.Forms.TextBox ux_textBoxNumericMaximum;
        private System.Windows.Forms.Label ux_labelNumericMaximum;
        private System.Windows.Forms.TextBox ux_textBoxNumericMinimum;
        private System.Windows.Forms.Label ux_labelNumericMinimum;
        private System.Windows.Forms.TextBox ux_textBoxMaxLength;
        private System.Windows.Forms.Label ux_labelMaxLength;
        private System.Windows.Forms.TextBox ux_textBoxNumericFormat;
        private System.Windows.Forms.Label ux_labelNumericFormat;
        private System.Windows.Forms.Label ux_labelCropTraitCodeAttachMessage;
        private System.Windows.Forms.Label ux_labelCropTraitAttachMessage;
    }
}