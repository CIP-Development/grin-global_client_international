﻿namespace CooperatorWizard
{
    partial class CooperatorWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ux_tabcontrolMain = new System.Windows.Forms.TabControl();
            this.ux_tabpageCooperator = new System.Windows.Forms.TabPage();
            this.ux_splitcontainerCooperator = new System.Windows.Forms.SplitContainer();
            this.ux_buttonFindCooperator = new System.Windows.Forms.Button();
            this.ux_groupboxMatch = new System.Windows.Forms.GroupBox();
            this.ux_radiobuttonExactMatch = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonPartialMatch = new System.Windows.Forms.RadioButton();
            this.ux_textboxCooperatorID = new System.Windows.Forms.TextBox();
            this.ux_labelCooperatorID = new System.Windows.Forms.Label();
            this.ux_textboxPhoneNumber = new System.Windows.Forms.TextBox();
            this.ux_labelPhoneNumber = new System.Windows.Forms.Label();
            this.ux_textboxEMail = new System.Windows.Forms.TextBox();
            this.ux_labelEMail = new System.Windows.Forms.Label();
            this.ux_textboxOrganization = new System.Windows.Forms.TextBox();
            this.ux_labelOrganization = new System.Windows.Forms.Label();
            this.ux_labelSearch = new System.Windows.Forms.Label();
            this.ux_textboxFirstName = new System.Windows.Forms.TextBox();
            this.ux_labelFirstName = new System.Windows.Forms.Label();
            this.ux_textboxLastName = new System.Windows.Forms.TextBox();
            this.ux_labelLastName = new System.Windows.Forms.Label();
            this.ux_datagridviewCooperator = new System.Windows.Forms.DataGridView();
            this.ux_buttonNewCooperator = new System.Windows.Forms.Button();
            this.ux_tabpageWebCooperator = new System.Windows.Forms.TabPage();
            this.ux_splitcontainerWebCooperator = new System.Windows.Forms.SplitContainer();
            this.ux_checkboxWebCooperatorIncludeShippingAdresses = new System.Windows.Forms.CheckBox();
            this.ux_buttonFindWebCooperator = new System.Windows.Forms.Button();
            this.ux_groupboxWebMatch = new System.Windows.Forms.GroupBox();
            this.ux_radiobuttonExactWebMatch = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonPartialWebMatch = new System.Windows.Forms.RadioButton();
            this.ux_textboxWebCooperatorID = new System.Windows.Forms.TextBox();
            this.ux_labelWebCooperatorID = new System.Windows.Forms.Label();
            this.ux_textboxWebPhoneNumber = new System.Windows.Forms.TextBox();
            this.ux_labelWebPhoneNumber = new System.Windows.Forms.Label();
            this.ux_textboxWebEMail = new System.Windows.Forms.TextBox();
            this.ux_labelWebEMail = new System.Windows.Forms.Label();
            this.ux_textboxWebOrganization = new System.Windows.Forms.TextBox();
            this.ux_labelWebOrganization = new System.Windows.Forms.Label();
            this.ux_labelWebSearch = new System.Windows.Forms.Label();
            this.ux_textboxWebFirstName = new System.Windows.Forms.TextBox();
            this.ux_labelWebFirstName = new System.Windows.Forms.Label();
            this.ux_textboxWebLastName = new System.Windows.Forms.TextBox();
            this.ux_labelWebLastName = new System.Windows.Forms.Label();
            this.ux_splitcontainerWebCooperatorDataviews = new System.Windows.Forms.SplitContainer();
            this.ux_datagridviewWebCooperatorMaster = new System.Windows.Forms.DataGridView();
            this.ux_buttonCreateNewCooperator = new System.Windows.Forms.Button();
            this.ux_groupboxWebCooperatorMatchCriteria = new System.Windows.Forms.GroupBox();
            this.ux_checkboxWebCooperatorGeography = new System.Windows.Forms.CheckBox();
            this.ux_checkboxWebCooperatorAddressLine1 = new System.Windows.Forms.CheckBox();
            this.ux_checkboxWebCooperatorOrganization = new System.Windows.Forms.CheckBox();
            this.ux_checkboxWebCooperatorFirstName = new System.Windows.Forms.CheckBox();
            this.ux_checkboxWebCooperatorLastName = new System.Windows.Forms.CheckBox();
            this.ux_datagridviewWebCooperatorDetail = new System.Windows.Forms.DataGridView();
            this.ux_tabpageWebOrder = new System.Windows.Forms.TabPage();
            this.ux_splitcontainerWebOrder = new System.Windows.Forms.SplitContainer();
            this.ux_labelWebOrderCooperatorDetails = new System.Windows.Forms.Label();
            this.ux_panelWebOrderRequestSearchCriteria = new System.Windows.Forms.Panel();
            this.ux_textboxWebOrderRequestGeography = new System.Windows.Forms.TextBox();
            this.ux_textboxWebOrderRequestAddressLine3 = new System.Windows.Forms.TextBox();
            this.ux_textboxWebOrderRequestPostalIndex = new System.Windows.Forms.TextBox();
            this.ux_textboxWebOrderRequestEmail = new System.Windows.Forms.TextBox();
            this.ux_textboxWebOrderRequestOrganization = new System.Windows.Forms.TextBox();
            this.ux_textboxWebOrderRequestPrimaryPhone = new System.Windows.Forms.TextBox();
            this.ux_labelWebOrderRequestCity = new System.Windows.Forms.Label();
            this.ux_labelWebOrderRequestGeography = new System.Windows.Forms.Label();
            this.ux_textboxWebOrderRequestLastName = new System.Windows.Forms.TextBox();
            this.ux_labelWebOrderRequestPrimaryPhone = new System.Windows.Forms.Label();
            this.ux_labelWebOrderRequestPostalIndex = new System.Windows.Forms.Label();
            this.ux_labelWebOrderRequestEmail = new System.Windows.Forms.Label();
            this.ux_textboxWebOrderRequestAddressLine2 = new System.Windows.Forms.TextBox();
            this.ux_textboxWebOrderRequestCity = new System.Windows.Forms.TextBox();
            this.ux_textboxWebOrderRequestFirstName = new System.Windows.Forms.TextBox();
            this.ux_labelWebOrderRequestAddressLine3 = new System.Windows.Forms.Label();
            this.ux_labelWebOrderRequestAddressLine1 = new System.Windows.Forms.Label();
            this.ux_labelWebOrderRequestTitle = new System.Windows.Forms.Label();
            this.ux_labelWebOrderRequestAddressLine2 = new System.Windows.Forms.Label();
            this.ux_labelWebOrderRequestFirstName = new System.Windows.Forms.Label();
            this.ux_textboxWebOrderRequestAddressLine1 = new System.Windows.Forms.TextBox();
            this.ux_labelWebOrderRequestOrganization = new System.Windows.Forms.Label();
            this.ux_textboxWebOrderRequestTitle = new System.Windows.Forms.TextBox();
            this.ux_labelWebOrderRequestLastName = new System.Windows.Forms.Label();
            this.ux_buttonWebOrderCreateNewCooperator = new System.Windows.Forms.Button();
            this.ux_labelWebOrderInstructions = new System.Windows.Forms.Label();
            this.ux_groupboxWebOrderCooperatorMatchCriteria = new System.Windows.Forms.GroupBox();
            this.ux_checkboxWebOrderCooperatorGeography = new System.Windows.Forms.CheckBox();
            this.ux_checkboxWebOrderCooperatorAddressLine1 = new System.Windows.Forms.CheckBox();
            this.ux_checkboxWebOrderCooperatorOrganization = new System.Windows.Forms.CheckBox();
            this.ux_checkboxWebOrderCooperatorFirstName = new System.Windows.Forms.CheckBox();
            this.ux_checkboxWebOrderCooperatorLastName = new System.Windows.Forms.CheckBox();
            this.ux_labelSelectedCooperatorAddress = new System.Windows.Forms.Label();
            this.ux_labelWebOrderAddress = new System.Windows.Forms.Label();
            this.ux_richtextboxSelectedCooperatorAddress = new System.Windows.Forms.RichTextBox();
            this.ux_groupboxWebOrderAddress = new System.Windows.Forms.GroupBox();
            this.ux_radiobuttonShipToAddress = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonPrimaryAddress = new System.Windows.Forms.RadioButton();
            this.ux_richtextboxWebOrderAddress = new System.Windows.Forms.RichTextBox();
            this.ux_buttonFindWebOrderRequest = new System.Windows.Forms.Button();
            this.ux_textboxWebOrderNumber = new System.Windows.Forms.TextBox();
            this.ux_labelWebOrderNumber = new System.Windows.Forms.Label();
            this.ux_datagridviewWebOrderRequestCooperator = new System.Windows.Forms.DataGridView();
            this.ux_buttonSave = new System.Windows.Forms.Button();
            this.ux_buttonSaveAndExit = new System.Windows.Forms.Button();
            this.ux_tabcontrolMain.SuspendLayout();
            this.ux_tabpageCooperator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerCooperator)).BeginInit();
            this.ux_splitcontainerCooperator.Panel1.SuspendLayout();
            this.ux_splitcontainerCooperator.Panel2.SuspendLayout();
            this.ux_splitcontainerCooperator.SuspendLayout();
            this.ux_groupboxMatch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewCooperator)).BeginInit();
            this.ux_tabpageWebCooperator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerWebCooperator)).BeginInit();
            this.ux_splitcontainerWebCooperator.Panel1.SuspendLayout();
            this.ux_splitcontainerWebCooperator.Panel2.SuspendLayout();
            this.ux_splitcontainerWebCooperator.SuspendLayout();
            this.ux_groupboxWebMatch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerWebCooperatorDataviews)).BeginInit();
            this.ux_splitcontainerWebCooperatorDataviews.Panel1.SuspendLayout();
            this.ux_splitcontainerWebCooperatorDataviews.Panel2.SuspendLayout();
            this.ux_splitcontainerWebCooperatorDataviews.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewWebCooperatorMaster)).BeginInit();
            this.ux_groupboxWebCooperatorMatchCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewWebCooperatorDetail)).BeginInit();
            this.ux_tabpageWebOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerWebOrder)).BeginInit();
            this.ux_splitcontainerWebOrder.Panel1.SuspendLayout();
            this.ux_splitcontainerWebOrder.Panel2.SuspendLayout();
            this.ux_splitcontainerWebOrder.SuspendLayout();
            this.ux_panelWebOrderRequestSearchCriteria.SuspendLayout();
            this.ux_groupboxWebOrderCooperatorMatchCriteria.SuspendLayout();
            this.ux_groupboxWebOrderAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewWebOrderRequestCooperator)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_tabcontrolMain
            // 
            this.ux_tabcontrolMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_tabcontrolMain.Controls.Add(this.ux_tabpageCooperator);
            this.ux_tabcontrolMain.Controls.Add(this.ux_tabpageWebCooperator);
            this.ux_tabcontrolMain.Controls.Add(this.ux_tabpageWebOrder);
            this.ux_tabcontrolMain.Location = new System.Drawing.Point(3, 34);
            this.ux_tabcontrolMain.Name = "ux_tabcontrolMain";
            this.ux_tabcontrolMain.SelectedIndex = 0;
            this.ux_tabcontrolMain.Size = new System.Drawing.Size(778, 395);
            this.ux_tabcontrolMain.TabIndex = 0;
            // 
            // ux_tabpageCooperator
            // 
            this.ux_tabpageCooperator.Controls.Add(this.ux_splitcontainerCooperator);
            this.ux_tabpageCooperator.Location = new System.Drawing.Point(4, 22);
            this.ux_tabpageCooperator.Name = "ux_tabpageCooperator";
            this.ux_tabpageCooperator.Padding = new System.Windows.Forms.Padding(3);
            this.ux_tabpageCooperator.Size = new System.Drawing.Size(770, 369);
            this.ux_tabpageCooperator.TabIndex = 0;
            this.ux_tabpageCooperator.Text = "Cooperator";
            this.ux_tabpageCooperator.UseVisualStyleBackColor = true;
            // 
            // ux_splitcontainerCooperator
            // 
            this.ux_splitcontainerCooperator.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_splitcontainerCooperator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ux_splitcontainerCooperator.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.ux_splitcontainerCooperator.Location = new System.Drawing.Point(3, 3);
            this.ux_splitcontainerCooperator.Name = "ux_splitcontainerCooperator";
            // 
            // ux_splitcontainerCooperator.Panel1
            // 
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_buttonFindCooperator);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_groupboxMatch);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_textboxCooperatorID);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_labelCooperatorID);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_textboxPhoneNumber);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_labelPhoneNumber);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_textboxEMail);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_labelEMail);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_textboxOrganization);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_labelOrganization);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_labelSearch);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_textboxFirstName);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_labelFirstName);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_textboxLastName);
            this.ux_splitcontainerCooperator.Panel1.Controls.Add(this.ux_labelLastName);
            // 
            // ux_splitcontainerCooperator.Panel2
            // 
            this.ux_splitcontainerCooperator.Panel2.Controls.Add(this.ux_datagridviewCooperator);
            this.ux_splitcontainerCooperator.Panel2.Controls.Add(this.ux_buttonNewCooperator);
            this.ux_splitcontainerCooperator.Size = new System.Drawing.Size(767, 363);
            this.ux_splitcontainerCooperator.SplitterDistance = 175;
            this.ux_splitcontainerCooperator.TabIndex = 7;
            // 
            // ux_buttonFindCooperator
            // 
            this.ux_buttonFindCooperator.Location = new System.Drawing.Point(87, 11);
            this.ux_buttonFindCooperator.Name = "ux_buttonFindCooperator";
            this.ux_buttonFindCooperator.Size = new System.Drawing.Size(63, 48);
            this.ux_buttonFindCooperator.TabIndex = 17;
            this.ux_buttonFindCooperator.Text = "Find Coop";
            this.ux_buttonFindCooperator.UseVisualStyleBackColor = true;
            this.ux_buttonFindCooperator.Click += new System.EventHandler(this.ux_buttonFindCooperator_Click);
            // 
            // ux_groupboxMatch
            // 
            this.ux_groupboxMatch.Controls.Add(this.ux_radiobuttonExactMatch);
            this.ux_groupboxMatch.Controls.Add(this.ux_radiobuttonPartialMatch);
            this.ux_groupboxMatch.Location = new System.Drawing.Point(9, 6);
            this.ux_groupboxMatch.Name = "ux_groupboxMatch";
            this.ux_groupboxMatch.Size = new System.Drawing.Size(71, 53);
            this.ux_groupboxMatch.TabIndex = 16;
            this.ux_groupboxMatch.TabStop = false;
            this.ux_groupboxMatch.Text = "Match";
            // 
            // ux_radiobuttonExactMatch
            // 
            this.ux_radiobuttonExactMatch.AutoSize = true;
            this.ux_radiobuttonExactMatch.Checked = true;
            this.ux_radiobuttonExactMatch.Location = new System.Drawing.Point(6, 11);
            this.ux_radiobuttonExactMatch.Name = "ux_radiobuttonExactMatch";
            this.ux_radiobuttonExactMatch.Size = new System.Drawing.Size(52, 17);
            this.ux_radiobuttonExactMatch.TabIndex = 14;
            this.ux_radiobuttonExactMatch.TabStop = true;
            this.ux_radiobuttonExactMatch.Text = "Exact";
            this.ux_radiobuttonExactMatch.UseVisualStyleBackColor = true;
            // 
            // ux_radiobuttonPartialMatch
            // 
            this.ux_radiobuttonPartialMatch.AutoSize = true;
            this.ux_radiobuttonPartialMatch.Location = new System.Drawing.Point(6, 30);
            this.ux_radiobuttonPartialMatch.Name = "ux_radiobuttonPartialMatch";
            this.ux_radiobuttonPartialMatch.Size = new System.Drawing.Size(54, 17);
            this.ux_radiobuttonPartialMatch.TabIndex = 15;
            this.ux_radiobuttonPartialMatch.Text = "Partial";
            this.ux_radiobuttonPartialMatch.UseVisualStyleBackColor = true;
            // 
            // ux_textboxCooperatorID
            // 
            this.ux_textboxCooperatorID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxCooperatorID.Location = new System.Drawing.Point(8, 314);
            this.ux_textboxCooperatorID.Name = "ux_textboxCooperatorID";
            this.ux_textboxCooperatorID.Size = new System.Drawing.Size(156, 20);
            this.ux_textboxCooperatorID.TabIndex = 13;
            // 
            // ux_labelCooperatorID
            // 
            this.ux_labelCooperatorID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelCooperatorID.AutoSize = true;
            this.ux_labelCooperatorID.Location = new System.Drawing.Point(7, 298);
            this.ux_labelCooperatorID.Name = "ux_labelCooperatorID";
            this.ux_labelCooperatorID.Size = new System.Drawing.Size(73, 13);
            this.ux_labelCooperatorID.TabIndex = 12;
            this.ux_labelCooperatorID.Text = "Cooperator ID";
            // 
            // ux_textboxPhoneNumber
            // 
            this.ux_textboxPhoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxPhoneNumber.Location = new System.Drawing.Point(8, 269);
            this.ux_textboxPhoneNumber.Name = "ux_textboxPhoneNumber";
            this.ux_textboxPhoneNumber.Size = new System.Drawing.Size(156, 20);
            this.ux_textboxPhoneNumber.TabIndex = 11;
            // 
            // ux_labelPhoneNumber
            // 
            this.ux_labelPhoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelPhoneNumber.AutoSize = true;
            this.ux_labelPhoneNumber.Location = new System.Drawing.Point(7, 253);
            this.ux_labelPhoneNumber.Name = "ux_labelPhoneNumber";
            this.ux_labelPhoneNumber.Size = new System.Drawing.Size(78, 13);
            this.ux_labelPhoneNumber.TabIndex = 10;
            this.ux_labelPhoneNumber.Text = "Phone Number";
            // 
            // ux_textboxEMail
            // 
            this.ux_textboxEMail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxEMail.Location = new System.Drawing.Point(8, 227);
            this.ux_textboxEMail.Name = "ux_textboxEMail";
            this.ux_textboxEMail.Size = new System.Drawing.Size(156, 20);
            this.ux_textboxEMail.TabIndex = 9;
            // 
            // ux_labelEMail
            // 
            this.ux_labelEMail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelEMail.AutoSize = true;
            this.ux_labelEMail.Location = new System.Drawing.Point(7, 211);
            this.ux_labelEMail.Name = "ux_labelEMail";
            this.ux_labelEMail.Size = new System.Drawing.Size(36, 13);
            this.ux_labelEMail.TabIndex = 8;
            this.ux_labelEMail.Text = "E-Mail";
            // 
            // ux_textboxOrganization
            // 
            this.ux_textboxOrganization.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxOrganization.Location = new System.Drawing.Point(8, 186);
            this.ux_textboxOrganization.Name = "ux_textboxOrganization";
            this.ux_textboxOrganization.Size = new System.Drawing.Size(156, 20);
            this.ux_textboxOrganization.TabIndex = 7;
            // 
            // ux_labelOrganization
            // 
            this.ux_labelOrganization.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelOrganization.AutoSize = true;
            this.ux_labelOrganization.Location = new System.Drawing.Point(7, 170);
            this.ux_labelOrganization.Name = "ux_labelOrganization";
            this.ux_labelOrganization.Size = new System.Drawing.Size(66, 13);
            this.ux_labelOrganization.TabIndex = 6;
            this.ux_labelOrganization.Text = "Organization";
            // 
            // ux_labelSearch
            // 
            this.ux_labelSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelSearch.AutoSize = true;
            this.ux_labelSearch.Location = new System.Drawing.Point(19, 71);
            this.ux_labelSearch.Name = "ux_labelSearch";
            this.ux_labelSearch.Size = new System.Drawing.Size(62, 13);
            this.ux_labelSearch.TabIndex = 3;
            this.ux_labelSearch.Text = "Search For:";
            // 
            // ux_textboxFirstName
            // 
            this.ux_textboxFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxFirstName.Location = new System.Drawing.Point(8, 144);
            this.ux_textboxFirstName.Name = "ux_textboxFirstName";
            this.ux_textboxFirstName.Size = new System.Drawing.Size(156, 20);
            this.ux_textboxFirstName.TabIndex = 5;
            // 
            // ux_labelFirstName
            // 
            this.ux_labelFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelFirstName.AutoSize = true;
            this.ux_labelFirstName.Location = new System.Drawing.Point(7, 128);
            this.ux_labelFirstName.Name = "ux_labelFirstName";
            this.ux_labelFirstName.Size = new System.Drawing.Size(57, 13);
            this.ux_labelFirstName.TabIndex = 2;
            this.ux_labelFirstName.Text = "First Name";
            // 
            // ux_textboxLastName
            // 
            this.ux_textboxLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxLastName.Location = new System.Drawing.Point(8, 103);
            this.ux_textboxLastName.Name = "ux_textboxLastName";
            this.ux_textboxLastName.Size = new System.Drawing.Size(156, 20);
            this.ux_textboxLastName.TabIndex = 4;
            // 
            // ux_labelLastName
            // 
            this.ux_labelLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelLastName.AutoSize = true;
            this.ux_labelLastName.Location = new System.Drawing.Point(7, 87);
            this.ux_labelLastName.Name = "ux_labelLastName";
            this.ux_labelLastName.Size = new System.Drawing.Size(58, 13);
            this.ux_labelLastName.TabIndex = 1;
            this.ux_labelLastName.Text = "Last Name";
            // 
            // ux_datagridviewCooperator
            // 
            this.ux_datagridviewCooperator.AllowUserToAddRows = false;
            this.ux_datagridviewCooperator.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewCooperator.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ux_datagridviewCooperator.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ux_datagridviewCooperator.DefaultCellStyle = dataGridViewCellStyle2;
            this.ux_datagridviewCooperator.Location = new System.Drawing.Point(5, 32);
            this.ux_datagridviewCooperator.Name = "ux_datagridviewCooperator";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewCooperator.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.ux_datagridviewCooperator.Size = new System.Drawing.Size(576, 324);
            this.ux_datagridviewCooperator.TabIndex = 0;
            this.ux_datagridviewCooperator.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ux_datagridview_CellFormatting);
            this.ux_datagridviewCooperator.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.ux_datagridview_CellParsing);
            this.ux_datagridviewCooperator.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ux_datagridview_DataError);
            this.ux_datagridviewCooperator.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ux_datagridview_EditingControlShowing);
            this.ux_datagridviewCooperator.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ux_datagridview_KeyDown);
            this.ux_datagridviewCooperator.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.ux_datagridview_PreviewKeyDown);
            // 
            // ux_buttonNewCooperator
            // 
            this.ux_buttonNewCooperator.Location = new System.Drawing.Point(5, 5);
            this.ux_buttonNewCooperator.Name = "ux_buttonNewCooperator";
            this.ux_buttonNewCooperator.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonNewCooperator.TabIndex = 6;
            this.ux_buttonNewCooperator.Text = "New...";
            this.ux_buttonNewCooperator.UseVisualStyleBackColor = true;
            this.ux_buttonNewCooperator.Click += new System.EventHandler(this.ux_buttonNewCooperator_Click);
            // 
            // ux_tabpageWebCooperator
            // 
            this.ux_tabpageWebCooperator.Controls.Add(this.ux_splitcontainerWebCooperator);
            this.ux_tabpageWebCooperator.Location = new System.Drawing.Point(4, 22);
            this.ux_tabpageWebCooperator.Name = "ux_tabpageWebCooperator";
            this.ux_tabpageWebCooperator.Padding = new System.Windows.Forms.Padding(3);
            this.ux_tabpageWebCooperator.Size = new System.Drawing.Size(770, 369);
            this.ux_tabpageWebCooperator.TabIndex = 1;
            this.ux_tabpageWebCooperator.Text = "Web Cooperator";
            this.ux_tabpageWebCooperator.UseVisualStyleBackColor = true;
            // 
            // ux_splitcontainerWebCooperator
            // 
            this.ux_splitcontainerWebCooperator.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_splitcontainerWebCooperator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ux_splitcontainerWebCooperator.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.ux_splitcontainerWebCooperator.Location = new System.Drawing.Point(3, 3);
            this.ux_splitcontainerWebCooperator.Name = "ux_splitcontainerWebCooperator";
            // 
            // ux_splitcontainerWebCooperator.Panel1
            // 
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_checkboxWebCooperatorIncludeShippingAdresses);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_buttonFindWebCooperator);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_groupboxWebMatch);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_textboxWebCooperatorID);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_labelWebCooperatorID);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_textboxWebPhoneNumber);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_labelWebPhoneNumber);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_textboxWebEMail);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_labelWebEMail);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_textboxWebOrganization);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_labelWebOrganization);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_labelWebSearch);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_textboxWebFirstName);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_labelWebFirstName);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_textboxWebLastName);
            this.ux_splitcontainerWebCooperator.Panel1.Controls.Add(this.ux_labelWebLastName);
            // 
            // ux_splitcontainerWebCooperator.Panel2
            // 
            this.ux_splitcontainerWebCooperator.Panel2.Controls.Add(this.ux_splitcontainerWebCooperatorDataviews);
            this.ux_splitcontainerWebCooperator.Size = new System.Drawing.Size(767, 363);
            this.ux_splitcontainerWebCooperator.SplitterDistance = 175;
            this.ux_splitcontainerWebCooperator.TabIndex = 8;
            // 
            // ux_checkboxWebCooperatorIncludeShippingAdresses
            // 
            this.ux_checkboxWebCooperatorIncludeShippingAdresses.AutoSize = true;
            this.ux_checkboxWebCooperatorIncludeShippingAdresses.Location = new System.Drawing.Point(9, 65);
            this.ux_checkboxWebCooperatorIncludeShippingAdresses.Name = "ux_checkboxWebCooperatorIncludeShippingAdresses";
            this.ux_checkboxWebCooperatorIncludeShippingAdresses.Size = new System.Drawing.Size(157, 17);
            this.ux_checkboxWebCooperatorIncludeShippingAdresses.TabIndex = 18;
            this.ux_checkboxWebCooperatorIncludeShippingAdresses.Text = "Include Shipping Addresses";
            this.ux_checkboxWebCooperatorIncludeShippingAdresses.UseVisualStyleBackColor = true;
            // 
            // ux_buttonFindWebCooperator
            // 
            this.ux_buttonFindWebCooperator.Location = new System.Drawing.Point(87, 11);
            this.ux_buttonFindWebCooperator.Name = "ux_buttonFindWebCooperator";
            this.ux_buttonFindWebCooperator.Size = new System.Drawing.Size(63, 48);
            this.ux_buttonFindWebCooperator.TabIndex = 17;
            this.ux_buttonFindWebCooperator.Text = "Find Web Coop";
            this.ux_buttonFindWebCooperator.UseVisualStyleBackColor = true;
            this.ux_buttonFindWebCooperator.Click += new System.EventHandler(this.ux_buttonFindWebCooperator_Click);
            // 
            // ux_groupboxWebMatch
            // 
            this.ux_groupboxWebMatch.Controls.Add(this.ux_radiobuttonExactWebMatch);
            this.ux_groupboxWebMatch.Controls.Add(this.ux_radiobuttonPartialWebMatch);
            this.ux_groupboxWebMatch.Location = new System.Drawing.Point(9, 6);
            this.ux_groupboxWebMatch.Name = "ux_groupboxWebMatch";
            this.ux_groupboxWebMatch.Size = new System.Drawing.Size(71, 53);
            this.ux_groupboxWebMatch.TabIndex = 16;
            this.ux_groupboxWebMatch.TabStop = false;
            this.ux_groupboxWebMatch.Text = "Match";
            // 
            // ux_radiobuttonExactWebMatch
            // 
            this.ux_radiobuttonExactWebMatch.AutoSize = true;
            this.ux_radiobuttonExactWebMatch.Checked = true;
            this.ux_radiobuttonExactWebMatch.Location = new System.Drawing.Point(6, 11);
            this.ux_radiobuttonExactWebMatch.Name = "ux_radiobuttonExactWebMatch";
            this.ux_radiobuttonExactWebMatch.Size = new System.Drawing.Size(52, 17);
            this.ux_radiobuttonExactWebMatch.TabIndex = 14;
            this.ux_radiobuttonExactWebMatch.TabStop = true;
            this.ux_radiobuttonExactWebMatch.Text = "Exact";
            this.ux_radiobuttonExactWebMatch.UseVisualStyleBackColor = true;
            // 
            // ux_radiobuttonPartialWebMatch
            // 
            this.ux_radiobuttonPartialWebMatch.AutoSize = true;
            this.ux_radiobuttonPartialWebMatch.Location = new System.Drawing.Point(6, 30);
            this.ux_radiobuttonPartialWebMatch.Name = "ux_radiobuttonPartialWebMatch";
            this.ux_radiobuttonPartialWebMatch.Size = new System.Drawing.Size(54, 17);
            this.ux_radiobuttonPartialWebMatch.TabIndex = 15;
            this.ux_radiobuttonPartialWebMatch.Text = "Partial";
            this.ux_radiobuttonPartialWebMatch.UseVisualStyleBackColor = true;
            // 
            // ux_textboxWebCooperatorID
            // 
            this.ux_textboxWebCooperatorID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebCooperatorID.Location = new System.Drawing.Point(8, 331);
            this.ux_textboxWebCooperatorID.Name = "ux_textboxWebCooperatorID";
            this.ux_textboxWebCooperatorID.Size = new System.Drawing.Size(156, 20);
            this.ux_textboxWebCooperatorID.TabIndex = 13;
            // 
            // ux_labelWebCooperatorID
            // 
            this.ux_labelWebCooperatorID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebCooperatorID.AutoSize = true;
            this.ux_labelWebCooperatorID.Location = new System.Drawing.Point(7, 315);
            this.ux_labelWebCooperatorID.Name = "ux_labelWebCooperatorID";
            this.ux_labelWebCooperatorID.Size = new System.Drawing.Size(99, 13);
            this.ux_labelWebCooperatorID.TabIndex = 12;
            this.ux_labelWebCooperatorID.Text = "Web Cooperator ID";
            // 
            // ux_textboxWebPhoneNumber
            // 
            this.ux_textboxWebPhoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebPhoneNumber.Location = new System.Drawing.Point(8, 286);
            this.ux_textboxWebPhoneNumber.Name = "ux_textboxWebPhoneNumber";
            this.ux_textboxWebPhoneNumber.Size = new System.Drawing.Size(156, 20);
            this.ux_textboxWebPhoneNumber.TabIndex = 11;
            // 
            // ux_labelWebPhoneNumber
            // 
            this.ux_labelWebPhoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebPhoneNumber.AutoSize = true;
            this.ux_labelWebPhoneNumber.Location = new System.Drawing.Point(7, 270);
            this.ux_labelWebPhoneNumber.Name = "ux_labelWebPhoneNumber";
            this.ux_labelWebPhoneNumber.Size = new System.Drawing.Size(104, 13);
            this.ux_labelWebPhoneNumber.TabIndex = 10;
            this.ux_labelWebPhoneNumber.Text = "Web Phone Number";
            // 
            // ux_textboxWebEMail
            // 
            this.ux_textboxWebEMail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebEMail.Location = new System.Drawing.Point(8, 244);
            this.ux_textboxWebEMail.Name = "ux_textboxWebEMail";
            this.ux_textboxWebEMail.Size = new System.Drawing.Size(156, 20);
            this.ux_textboxWebEMail.TabIndex = 9;
            // 
            // ux_labelWebEMail
            // 
            this.ux_labelWebEMail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebEMail.AutoSize = true;
            this.ux_labelWebEMail.Location = new System.Drawing.Point(7, 228);
            this.ux_labelWebEMail.Name = "ux_labelWebEMail";
            this.ux_labelWebEMail.Size = new System.Drawing.Size(62, 13);
            this.ux_labelWebEMail.TabIndex = 8;
            this.ux_labelWebEMail.Text = "Web E-Mail";
            // 
            // ux_textboxWebOrganization
            // 
            this.ux_textboxWebOrganization.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrganization.Location = new System.Drawing.Point(8, 203);
            this.ux_textboxWebOrganization.Name = "ux_textboxWebOrganization";
            this.ux_textboxWebOrganization.Size = new System.Drawing.Size(156, 20);
            this.ux_textboxWebOrganization.TabIndex = 7;
            this.ux_textboxWebOrganization.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ux_textboxWebCooperatorFilterText_KeyPress);
            // 
            // ux_labelWebOrganization
            // 
            this.ux_labelWebOrganization.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrganization.AutoSize = true;
            this.ux_labelWebOrganization.Location = new System.Drawing.Point(7, 187);
            this.ux_labelWebOrganization.Name = "ux_labelWebOrganization";
            this.ux_labelWebOrganization.Size = new System.Drawing.Size(92, 13);
            this.ux_labelWebOrganization.TabIndex = 6;
            this.ux_labelWebOrganization.Text = "Web Organization";
            // 
            // ux_labelWebSearch
            // 
            this.ux_labelWebSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebSearch.AutoSize = true;
            this.ux_labelWebSearch.Location = new System.Drawing.Point(19, 88);
            this.ux_labelWebSearch.Name = "ux_labelWebSearch";
            this.ux_labelWebSearch.Size = new System.Drawing.Size(62, 13);
            this.ux_labelWebSearch.TabIndex = 3;
            this.ux_labelWebSearch.Text = "Search For:";
            // 
            // ux_textboxWebFirstName
            // 
            this.ux_textboxWebFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebFirstName.Location = new System.Drawing.Point(8, 161);
            this.ux_textboxWebFirstName.Name = "ux_textboxWebFirstName";
            this.ux_textboxWebFirstName.Size = new System.Drawing.Size(156, 20);
            this.ux_textboxWebFirstName.TabIndex = 5;
            this.ux_textboxWebFirstName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ux_textboxWebCooperatorFilterText_KeyPress);
            // 
            // ux_labelWebFirstName
            // 
            this.ux_labelWebFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebFirstName.AutoSize = true;
            this.ux_labelWebFirstName.Location = new System.Drawing.Point(7, 145);
            this.ux_labelWebFirstName.Name = "ux_labelWebFirstName";
            this.ux_labelWebFirstName.Size = new System.Drawing.Size(83, 13);
            this.ux_labelWebFirstName.TabIndex = 2;
            this.ux_labelWebFirstName.Text = "Web First Name";
            // 
            // ux_textboxWebLastName
            // 
            this.ux_textboxWebLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebLastName.Location = new System.Drawing.Point(8, 120);
            this.ux_textboxWebLastName.Name = "ux_textboxWebLastName";
            this.ux_textboxWebLastName.Size = new System.Drawing.Size(156, 20);
            this.ux_textboxWebLastName.TabIndex = 4;
            this.ux_textboxWebLastName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ux_textboxWebCooperatorFilterText_KeyPress);
            // 
            // ux_labelWebLastName
            // 
            this.ux_labelWebLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebLastName.AutoSize = true;
            this.ux_labelWebLastName.Location = new System.Drawing.Point(7, 104);
            this.ux_labelWebLastName.Name = "ux_labelWebLastName";
            this.ux_labelWebLastName.Size = new System.Drawing.Size(84, 13);
            this.ux_labelWebLastName.TabIndex = 1;
            this.ux_labelWebLastName.Text = "Web Last Name";
            // 
            // ux_splitcontainerWebCooperatorDataviews
            // 
            this.ux_splitcontainerWebCooperatorDataviews.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_splitcontainerWebCooperatorDataviews.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ux_splitcontainerWebCooperatorDataviews.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.ux_splitcontainerWebCooperatorDataviews.Location = new System.Drawing.Point(5, 6);
            this.ux_splitcontainerWebCooperatorDataviews.Name = "ux_splitcontainerWebCooperatorDataviews";
            this.ux_splitcontainerWebCooperatorDataviews.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ux_splitcontainerWebCooperatorDataviews.Panel1
            // 
            this.ux_splitcontainerWebCooperatorDataviews.Panel1.Controls.Add(this.ux_datagridviewWebCooperatorMaster);
            this.ux_splitcontainerWebCooperatorDataviews.Panel1.Controls.Add(this.ux_buttonCreateNewCooperator);
            // 
            // ux_splitcontainerWebCooperatorDataviews.Panel2
            // 
            this.ux_splitcontainerWebCooperatorDataviews.Panel2.Controls.Add(this.ux_groupboxWebCooperatorMatchCriteria);
            this.ux_splitcontainerWebCooperatorDataviews.Panel2.Controls.Add(this.ux_datagridviewWebCooperatorDetail);
            this.ux_splitcontainerWebCooperatorDataviews.Size = new System.Drawing.Size(573, 656);
            this.ux_splitcontainerWebCooperatorDataviews.SplitterDistance = 151;
            this.ux_splitcontainerWebCooperatorDataviews.TabIndex = 14;
            // 
            // ux_datagridviewWebCooperatorMaster
            // 
            this.ux_datagridviewWebCooperatorMaster.AllowUserToAddRows = false;
            this.ux_datagridviewWebCooperatorMaster.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewWebCooperatorMaster.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.ux_datagridviewWebCooperatorMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ux_datagridviewWebCooperatorMaster.DefaultCellStyle = dataGridViewCellStyle5;
            this.ux_datagridviewWebCooperatorMaster.Location = new System.Drawing.Point(3, 3);
            this.ux_datagridviewWebCooperatorMaster.MultiSelect = false;
            this.ux_datagridviewWebCooperatorMaster.Name = "ux_datagridviewWebCooperatorMaster";
            this.ux_datagridviewWebCooperatorMaster.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewWebCooperatorMaster.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.ux_datagridviewWebCooperatorMaster.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ux_datagridviewWebCooperatorMaster.Size = new System.Drawing.Size(563, 112);
            this.ux_datagridviewWebCooperatorMaster.TabIndex = 0;
            this.ux_datagridviewWebCooperatorMaster.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.ux_datagridviewWebCooperatorMaster_RowEnter);
            // 
            // ux_buttonCreateNewCooperator
            // 
            this.ux_buttonCreateNewCooperator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCreateNewCooperator.Location = new System.Drawing.Point(336, 120);
            this.ux_buttonCreateNewCooperator.Name = "ux_buttonCreateNewCooperator";
            this.ux_buttonCreateNewCooperator.Size = new System.Drawing.Size(230, 23);
            this.ux_buttonCreateNewCooperator.TabIndex = 13;
            this.ux_buttonCreateNewCooperator.Text = "Create New Cooperator from Selection";
            this.ux_buttonCreateNewCooperator.UseVisualStyleBackColor = true;
            this.ux_buttonCreateNewCooperator.Click += new System.EventHandler(this.ux_buttonCreateNewCooperator_Click);
            // 
            // ux_groupboxWebCooperatorMatchCriteria
            // 
            this.ux_groupboxWebCooperatorMatchCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxWebCooperatorMatchCriteria.Controls.Add(this.ux_checkboxWebCooperatorGeography);
            this.ux_groupboxWebCooperatorMatchCriteria.Controls.Add(this.ux_checkboxWebCooperatorAddressLine1);
            this.ux_groupboxWebCooperatorMatchCriteria.Controls.Add(this.ux_checkboxWebCooperatorOrganization);
            this.ux_groupboxWebCooperatorMatchCriteria.Controls.Add(this.ux_checkboxWebCooperatorFirstName);
            this.ux_groupboxWebCooperatorMatchCriteria.Controls.Add(this.ux_checkboxWebCooperatorLastName);
            this.ux_groupboxWebCooperatorMatchCriteria.Location = new System.Drawing.Point(3, 3);
            this.ux_groupboxWebCooperatorMatchCriteria.Name = "ux_groupboxWebCooperatorMatchCriteria";
            this.ux_groupboxWebCooperatorMatchCriteria.Size = new System.Drawing.Size(563, 40);
            this.ux_groupboxWebCooperatorMatchCriteria.TabIndex = 2;
            this.ux_groupboxWebCooperatorMatchCriteria.TabStop = false;
            this.ux_groupboxWebCooperatorMatchCriteria.Text = "Find Matches Based On:";
            // 
            // ux_checkboxWebCooperatorGeography
            // 
            this.ux_checkboxWebCooperatorGeography.AutoSize = true;
            this.ux_checkboxWebCooperatorGeography.Checked = true;
            this.ux_checkboxWebCooperatorGeography.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ux_checkboxWebCooperatorGeography.Location = new System.Drawing.Point(484, 17);
            this.ux_checkboxWebCooperatorGeography.Name = "ux_checkboxWebCooperatorGeography";
            this.ux_checkboxWebCooperatorGeography.Size = new System.Drawing.Size(78, 17);
            this.ux_checkboxWebCooperatorGeography.TabIndex = 4;
            this.ux_checkboxWebCooperatorGeography.Text = "Geography";
            this.ux_checkboxWebCooperatorGeography.UseVisualStyleBackColor = true;
            this.ux_checkboxWebCooperatorGeography.CheckedChanged += new System.EventHandler(this.ux_checkboxWebCooperatorFindMatchesBasedOn_CheckedChanged);
            // 
            // ux_checkboxWebCooperatorAddressLine1
            // 
            this.ux_checkboxWebCooperatorAddressLine1.AutoSize = true;
            this.ux_checkboxWebCooperatorAddressLine1.Location = new System.Drawing.Point(352, 17);
            this.ux_checkboxWebCooperatorAddressLine1.Name = "ux_checkboxWebCooperatorAddressLine1";
            this.ux_checkboxWebCooperatorAddressLine1.Size = new System.Drawing.Size(96, 17);
            this.ux_checkboxWebCooperatorAddressLine1.TabIndex = 3;
            this.ux_checkboxWebCooperatorAddressLine1.Text = "Address Line 1";
            this.ux_checkboxWebCooperatorAddressLine1.UseVisualStyleBackColor = true;
            this.ux_checkboxWebCooperatorAddressLine1.CheckedChanged += new System.EventHandler(this.ux_checkboxWebCooperatorFindMatchesBasedOn_CheckedChanged);
            // 
            // ux_checkboxWebCooperatorOrganization
            // 
            this.ux_checkboxWebCooperatorOrganization.AutoSize = true;
            this.ux_checkboxWebCooperatorOrganization.Location = new System.Drawing.Point(231, 17);
            this.ux_checkboxWebCooperatorOrganization.Name = "ux_checkboxWebCooperatorOrganization";
            this.ux_checkboxWebCooperatorOrganization.Size = new System.Drawing.Size(85, 17);
            this.ux_checkboxWebCooperatorOrganization.TabIndex = 2;
            this.ux_checkboxWebCooperatorOrganization.Text = "Organization";
            this.ux_checkboxWebCooperatorOrganization.UseVisualStyleBackColor = true;
            this.ux_checkboxWebCooperatorOrganization.CheckedChanged += new System.EventHandler(this.ux_checkboxWebCooperatorFindMatchesBasedOn_CheckedChanged);
            // 
            // ux_checkboxWebCooperatorFirstName
            // 
            this.ux_checkboxWebCooperatorFirstName.AutoSize = true;
            this.ux_checkboxWebCooperatorFirstName.Location = new System.Drawing.Point(119, 17);
            this.ux_checkboxWebCooperatorFirstName.Name = "ux_checkboxWebCooperatorFirstName";
            this.ux_checkboxWebCooperatorFirstName.Size = new System.Drawing.Size(76, 17);
            this.ux_checkboxWebCooperatorFirstName.TabIndex = 1;
            this.ux_checkboxWebCooperatorFirstName.Text = "First Name";
            this.ux_checkboxWebCooperatorFirstName.UseVisualStyleBackColor = true;
            this.ux_checkboxWebCooperatorFirstName.CheckedChanged += new System.EventHandler(this.ux_checkboxWebCooperatorFindMatchesBasedOn_CheckedChanged);
            // 
            // ux_checkboxWebCooperatorLastName
            // 
            this.ux_checkboxWebCooperatorLastName.AutoSize = true;
            this.ux_checkboxWebCooperatorLastName.Checked = true;
            this.ux_checkboxWebCooperatorLastName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ux_checkboxWebCooperatorLastName.Location = new System.Drawing.Point(6, 17);
            this.ux_checkboxWebCooperatorLastName.Name = "ux_checkboxWebCooperatorLastName";
            this.ux_checkboxWebCooperatorLastName.Size = new System.Drawing.Size(77, 17);
            this.ux_checkboxWebCooperatorLastName.TabIndex = 0;
            this.ux_checkboxWebCooperatorLastName.Text = "Last Name";
            this.ux_checkboxWebCooperatorLastName.UseVisualStyleBackColor = true;
            this.ux_checkboxWebCooperatorLastName.CheckedChanged += new System.EventHandler(this.ux_checkboxWebCooperatorFindMatchesBasedOn_CheckedChanged);
            // 
            // ux_datagridviewWebCooperatorDetail
            // 
            this.ux_datagridviewWebCooperatorDetail.AllowUserToAddRows = false;
            this.ux_datagridviewWebCooperatorDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewWebCooperatorDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.ux_datagridviewWebCooperatorDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ux_datagridviewWebCooperatorDetail.DefaultCellStyle = dataGridViewCellStyle8;
            this.ux_datagridviewWebCooperatorDetail.Location = new System.Drawing.Point(3, 48);
            this.ux_datagridviewWebCooperatorDetail.Name = "ux_datagridviewWebCooperatorDetail";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewWebCooperatorDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.ux_datagridviewWebCooperatorDetail.Size = new System.Drawing.Size(563, 145);
            this.ux_datagridviewWebCooperatorDetail.TabIndex = 1;
            this.ux_datagridviewWebCooperatorDetail.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ux_datagridview_CellFormatting);
            this.ux_datagridviewWebCooperatorDetail.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.ux_datagridview_CellParsing);
            this.ux_datagridviewWebCooperatorDetail.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ux_datagridview_DataError);
            this.ux_datagridviewWebCooperatorDetail.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ux_datagridview_EditingControlShowing);
            this.ux_datagridviewWebCooperatorDetail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ux_datagridview_KeyDown);
            this.ux_datagridviewWebCooperatorDetail.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.ux_datagridview_PreviewKeyDown);
            // 
            // ux_tabpageWebOrder
            // 
            this.ux_tabpageWebOrder.Controls.Add(this.ux_splitcontainerWebOrder);
            this.ux_tabpageWebOrder.Location = new System.Drawing.Point(4, 22);
            this.ux_tabpageWebOrder.Name = "ux_tabpageWebOrder";
            this.ux_tabpageWebOrder.Padding = new System.Windows.Forms.Padding(3);
            this.ux_tabpageWebOrder.Size = new System.Drawing.Size(770, 369);
            this.ux_tabpageWebOrder.TabIndex = 2;
            this.ux_tabpageWebOrder.Text = "Web Order";
            this.ux_tabpageWebOrder.UseVisualStyleBackColor = true;
            // 
            // ux_splitcontainerWebOrder
            // 
            this.ux_splitcontainerWebOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_splitcontainerWebOrder.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ux_splitcontainerWebOrder.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.ux_splitcontainerWebOrder.Location = new System.Drawing.Point(3, 3);
            this.ux_splitcontainerWebOrder.Name = "ux_splitcontainerWebOrder";
            // 
            // ux_splitcontainerWebOrder.Panel1
            // 
            this.ux_splitcontainerWebOrder.Panel1.Controls.Add(this.ux_labelWebOrderCooperatorDetails);
            this.ux_splitcontainerWebOrder.Panel1.Controls.Add(this.ux_panelWebOrderRequestSearchCriteria);
            // 
            // ux_splitcontainerWebOrder.Panel2
            // 
            this.ux_splitcontainerWebOrder.Panel2.Controls.Add(this.ux_buttonWebOrderCreateNewCooperator);
            this.ux_splitcontainerWebOrder.Panel2.Controls.Add(this.ux_labelWebOrderInstructions);
            this.ux_splitcontainerWebOrder.Panel2.Controls.Add(this.ux_groupboxWebOrderCooperatorMatchCriteria);
            this.ux_splitcontainerWebOrder.Panel2.Controls.Add(this.ux_labelSelectedCooperatorAddress);
            this.ux_splitcontainerWebOrder.Panel2.Controls.Add(this.ux_labelWebOrderAddress);
            this.ux_splitcontainerWebOrder.Panel2.Controls.Add(this.ux_richtextboxSelectedCooperatorAddress);
            this.ux_splitcontainerWebOrder.Panel2.Controls.Add(this.ux_groupboxWebOrderAddress);
            this.ux_splitcontainerWebOrder.Panel2.Controls.Add(this.ux_richtextboxWebOrderAddress);
            this.ux_splitcontainerWebOrder.Panel2.Controls.Add(this.ux_buttonFindWebOrderRequest);
            this.ux_splitcontainerWebOrder.Panel2.Controls.Add(this.ux_textboxWebOrderNumber);
            this.ux_splitcontainerWebOrder.Panel2.Controls.Add(this.ux_labelWebOrderNumber);
            this.ux_splitcontainerWebOrder.Panel2.Controls.Add(this.ux_datagridviewWebOrderRequestCooperator);
            this.ux_splitcontainerWebOrder.Size = new System.Drawing.Size(767, 363);
            this.ux_splitcontainerWebOrder.SplitterDistance = 175;
            this.ux_splitcontainerWebOrder.TabIndex = 25;
            // 
            // ux_labelWebOrderCooperatorDetails
            // 
            this.ux_labelWebOrderCooperatorDetails.AutoSize = true;
            this.ux_labelWebOrderCooperatorDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_labelWebOrderCooperatorDetails.Location = new System.Drawing.Point(9, 13);
            this.ux_labelWebOrderCooperatorDetails.Name = "ux_labelWebOrderCooperatorDetails";
            this.ux_labelWebOrderCooperatorDetails.Size = new System.Drawing.Size(90, 16);
            this.ux_labelWebOrderCooperatorDetails.TabIndex = 1;
            this.ux_labelWebOrderCooperatorDetails.Tag = "{0} Address";
            this.ux_labelWebOrderCooperatorDetails.Text = "{0} Address";
            // 
            // ux_panelWebOrderRequestSearchCriteria
            // 
            this.ux_panelWebOrderRequestSearchCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_textboxWebOrderRequestGeography);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_textboxWebOrderRequestAddressLine3);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_textboxWebOrderRequestPostalIndex);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_textboxWebOrderRequestEmail);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_textboxWebOrderRequestOrganization);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_textboxWebOrderRequestPrimaryPhone);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_labelWebOrderRequestCity);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_labelWebOrderRequestGeography);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_textboxWebOrderRequestLastName);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_labelWebOrderRequestPrimaryPhone);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_labelWebOrderRequestPostalIndex);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_labelWebOrderRequestEmail);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_textboxWebOrderRequestAddressLine2);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_textboxWebOrderRequestCity);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_textboxWebOrderRequestFirstName);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_labelWebOrderRequestAddressLine3);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_labelWebOrderRequestAddressLine1);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_labelWebOrderRequestTitle);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_labelWebOrderRequestAddressLine2);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_labelWebOrderRequestFirstName);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_textboxWebOrderRequestAddressLine1);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_labelWebOrderRequestOrganization);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_textboxWebOrderRequestTitle);
            this.ux_panelWebOrderRequestSearchCriteria.Controls.Add(this.ux_labelWebOrderRequestLastName);
            this.ux_panelWebOrderRequestSearchCriteria.Location = new System.Drawing.Point(4, 40);
            this.ux_panelWebOrderRequestSearchCriteria.Name = "ux_panelWebOrderRequestSearchCriteria";
            this.ux_panelWebOrderRequestSearchCriteria.Size = new System.Drawing.Size(164, 503);
            this.ux_panelWebOrderRequestSearchCriteria.TabIndex = 0;
            // 
            // ux_textboxWebOrderRequestGeography
            // 
            this.ux_textboxWebOrderRequestGeography.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrderRequestGeography.Location = new System.Drawing.Point(3, 389);
            this.ux_textboxWebOrderRequestGeography.Name = "ux_textboxWebOrderRequestGeography";
            this.ux_textboxWebOrderRequestGeography.ReadOnly = true;
            this.ux_textboxWebOrderRequestGeography.Size = new System.Drawing.Size(157, 20);
            this.ux_textboxWebOrderRequestGeography.TabIndex = 33;
            // 
            // ux_textboxWebOrderRequestAddressLine3
            // 
            this.ux_textboxWebOrderRequestAddressLine3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrderRequestAddressLine3.Location = new System.Drawing.Point(3, 265);
            this.ux_textboxWebOrderRequestAddressLine3.Name = "ux_textboxWebOrderRequestAddressLine3";
            this.ux_textboxWebOrderRequestAddressLine3.ReadOnly = true;
            this.ux_textboxWebOrderRequestAddressLine3.Size = new System.Drawing.Size(157, 20);
            this.ux_textboxWebOrderRequestAddressLine3.TabIndex = 27;
            // 
            // ux_textboxWebOrderRequestPostalIndex
            // 
            this.ux_textboxWebOrderRequestPostalIndex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrderRequestPostalIndex.Location = new System.Drawing.Point(3, 348);
            this.ux_textboxWebOrderRequestPostalIndex.Name = "ux_textboxWebOrderRequestPostalIndex";
            this.ux_textboxWebOrderRequestPostalIndex.ReadOnly = true;
            this.ux_textboxWebOrderRequestPostalIndex.Size = new System.Drawing.Size(157, 20);
            this.ux_textboxWebOrderRequestPostalIndex.TabIndex = 30;
            // 
            // ux_textboxWebOrderRequestEmail
            // 
            this.ux_textboxWebOrderRequestEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrderRequestEmail.Location = new System.Drawing.Point(3, 472);
            this.ux_textboxWebOrderRequestEmail.Name = "ux_textboxWebOrderRequestEmail";
            this.ux_textboxWebOrderRequestEmail.ReadOnly = true;
            this.ux_textboxWebOrderRequestEmail.Size = new System.Drawing.Size(157, 20);
            this.ux_textboxWebOrderRequestEmail.TabIndex = 36;
            // 
            // ux_textboxWebOrderRequestOrganization
            // 
            this.ux_textboxWebOrderRequestOrganization.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrderRequestOrganization.Location = new System.Drawing.Point(3, 143);
            this.ux_textboxWebOrderRequestOrganization.Name = "ux_textboxWebOrderRequestOrganization";
            this.ux_textboxWebOrderRequestOrganization.ReadOnly = true;
            this.ux_textboxWebOrderRequestOrganization.Size = new System.Drawing.Size(157, 20);
            this.ux_textboxWebOrderRequestOrganization.TabIndex = 27;
            // 
            // ux_textboxWebOrderRequestPrimaryPhone
            // 
            this.ux_textboxWebOrderRequestPrimaryPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrderRequestPrimaryPhone.Location = new System.Drawing.Point(3, 430);
            this.ux_textboxWebOrderRequestPrimaryPhone.Name = "ux_textboxWebOrderRequestPrimaryPhone";
            this.ux_textboxWebOrderRequestPrimaryPhone.ReadOnly = true;
            this.ux_textboxWebOrderRequestPrimaryPhone.Size = new System.Drawing.Size(157, 20);
            this.ux_textboxWebOrderRequestPrimaryPhone.TabIndex = 34;
            // 
            // ux_labelWebOrderRequestCity
            // 
            this.ux_labelWebOrderRequestCity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrderRequestCity.AutoSize = true;
            this.ux_labelWebOrderRequestCity.Location = new System.Drawing.Point(2, 290);
            this.ux_labelWebOrderRequestCity.Name = "ux_labelWebOrderRequestCity";
            this.ux_labelWebOrderRequestCity.Size = new System.Drawing.Size(24, 13);
            this.ux_labelWebOrderRequestCity.TabIndex = 26;
            this.ux_labelWebOrderRequestCity.Text = "City";
            // 
            // ux_labelWebOrderRequestGeography
            // 
            this.ux_labelWebOrderRequestGeography.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrderRequestGeography.AutoSize = true;
            this.ux_labelWebOrderRequestGeography.Location = new System.Drawing.Point(2, 373);
            this.ux_labelWebOrderRequestGeography.Name = "ux_labelWebOrderRequestGeography";
            this.ux_labelWebOrderRequestGeography.Size = new System.Drawing.Size(63, 13);
            this.ux_labelWebOrderRequestGeography.TabIndex = 31;
            this.ux_labelWebOrderRequestGeography.Text = "Geography*";
            // 
            // ux_textboxWebOrderRequestLastName
            // 
            this.ux_textboxWebOrderRequestLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrderRequestLastName.Location = new System.Drawing.Point(3, 19);
            this.ux_textboxWebOrderRequestLastName.Name = "ux_textboxWebOrderRequestLastName";
            this.ux_textboxWebOrderRequestLastName.ReadOnly = true;
            this.ux_textboxWebOrderRequestLastName.Size = new System.Drawing.Size(157, 20);
            this.ux_textboxWebOrderRequestLastName.TabIndex = 21;
            // 
            // ux_labelWebOrderRequestPrimaryPhone
            // 
            this.ux_labelWebOrderRequestPrimaryPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrderRequestPrimaryPhone.AutoSize = true;
            this.ux_labelWebOrderRequestPrimaryPhone.Location = new System.Drawing.Point(2, 411);
            this.ux_labelWebOrderRequestPrimaryPhone.Name = "ux_labelWebOrderRequestPrimaryPhone";
            this.ux_labelWebOrderRequestPrimaryPhone.Size = new System.Drawing.Size(75, 13);
            this.ux_labelWebOrderRequestPrimaryPhone.TabIndex = 32;
            this.ux_labelWebOrderRequestPrimaryPhone.Text = "Primary Phone";
            // 
            // ux_labelWebOrderRequestPostalIndex
            // 
            this.ux_labelWebOrderRequestPostalIndex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrderRequestPostalIndex.AutoSize = true;
            this.ux_labelWebOrderRequestPostalIndex.Location = new System.Drawing.Point(2, 332);
            this.ux_labelWebOrderRequestPostalIndex.Name = "ux_labelWebOrderRequestPostalIndex";
            this.ux_labelWebOrderRequestPostalIndex.Size = new System.Drawing.Size(64, 13);
            this.ux_labelWebOrderRequestPostalIndex.TabIndex = 29;
            this.ux_labelWebOrderRequestPostalIndex.Text = "Postal Code";
            // 
            // ux_labelWebOrderRequestEmail
            // 
            this.ux_labelWebOrderRequestEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrderRequestEmail.AutoSize = true;
            this.ux_labelWebOrderRequestEmail.Location = new System.Drawing.Point(2, 456);
            this.ux_labelWebOrderRequestEmail.Name = "ux_labelWebOrderRequestEmail";
            this.ux_labelWebOrderRequestEmail.Size = new System.Drawing.Size(32, 13);
            this.ux_labelWebOrderRequestEmail.TabIndex = 35;
            this.ux_labelWebOrderRequestEmail.Text = "Email";
            // 
            // ux_textboxWebOrderRequestAddressLine2
            // 
            this.ux_textboxWebOrderRequestAddressLine2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrderRequestAddressLine2.Location = new System.Drawing.Point(3, 226);
            this.ux_textboxWebOrderRequestAddressLine2.Name = "ux_textboxWebOrderRequestAddressLine2";
            this.ux_textboxWebOrderRequestAddressLine2.ReadOnly = true;
            this.ux_textboxWebOrderRequestAddressLine2.Size = new System.Drawing.Size(157, 20);
            this.ux_textboxWebOrderRequestAddressLine2.TabIndex = 30;
            // 
            // ux_textboxWebOrderRequestCity
            // 
            this.ux_textboxWebOrderRequestCity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrderRequestCity.Location = new System.Drawing.Point(3, 306);
            this.ux_textboxWebOrderRequestCity.Name = "ux_textboxWebOrderRequestCity";
            this.ux_textboxWebOrderRequestCity.ReadOnly = true;
            this.ux_textboxWebOrderRequestCity.Size = new System.Drawing.Size(157, 20);
            this.ux_textboxWebOrderRequestCity.TabIndex = 28;
            // 
            // ux_textboxWebOrderRequestFirstName
            // 
            this.ux_textboxWebOrderRequestFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrderRequestFirstName.Location = new System.Drawing.Point(3, 102);
            this.ux_textboxWebOrderRequestFirstName.Name = "ux_textboxWebOrderRequestFirstName";
            this.ux_textboxWebOrderRequestFirstName.ReadOnly = true;
            this.ux_textboxWebOrderRequestFirstName.Size = new System.Drawing.Size(157, 20);
            this.ux_textboxWebOrderRequestFirstName.TabIndex = 24;
            // 
            // ux_labelWebOrderRequestAddressLine3
            // 
            this.ux_labelWebOrderRequestAddressLine3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrderRequestAddressLine3.AutoSize = true;
            this.ux_labelWebOrderRequestAddressLine3.Location = new System.Drawing.Point(2, 249);
            this.ux_labelWebOrderRequestAddressLine3.Name = "ux_labelWebOrderRequestAddressLine3";
            this.ux_labelWebOrderRequestAddressLine3.Size = new System.Drawing.Size(77, 13);
            this.ux_labelWebOrderRequestAddressLine3.TabIndex = 25;
            this.ux_labelWebOrderRequestAddressLine3.Text = "Address Line 3";
            // 
            // ux_labelWebOrderRequestAddressLine1
            // 
            this.ux_labelWebOrderRequestAddressLine1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrderRequestAddressLine1.AutoSize = true;
            this.ux_labelWebOrderRequestAddressLine1.Location = new System.Drawing.Point(2, 168);
            this.ux_labelWebOrderRequestAddressLine1.Name = "ux_labelWebOrderRequestAddressLine1";
            this.ux_labelWebOrderRequestAddressLine1.Size = new System.Drawing.Size(81, 13);
            this.ux_labelWebOrderRequestAddressLine1.TabIndex = 26;
            this.ux_labelWebOrderRequestAddressLine1.Text = "Address Line 1*";
            // 
            // ux_labelWebOrderRequestTitle
            // 
            this.ux_labelWebOrderRequestTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrderRequestTitle.AutoSize = true;
            this.ux_labelWebOrderRequestTitle.Location = new System.Drawing.Point(2, 44);
            this.ux_labelWebOrderRequestTitle.Name = "ux_labelWebOrderRequestTitle";
            this.ux_labelWebOrderRequestTitle.Size = new System.Drawing.Size(27, 13);
            this.ux_labelWebOrderRequestTitle.TabIndex = 20;
            this.ux_labelWebOrderRequestTitle.Text = "Title";
            // 
            // ux_labelWebOrderRequestAddressLine2
            // 
            this.ux_labelWebOrderRequestAddressLine2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrderRequestAddressLine2.AutoSize = true;
            this.ux_labelWebOrderRequestAddressLine2.Location = new System.Drawing.Point(2, 210);
            this.ux_labelWebOrderRequestAddressLine2.Name = "ux_labelWebOrderRequestAddressLine2";
            this.ux_labelWebOrderRequestAddressLine2.Size = new System.Drawing.Size(77, 13);
            this.ux_labelWebOrderRequestAddressLine2.TabIndex = 29;
            this.ux_labelWebOrderRequestAddressLine2.Text = "Address Line 2";
            // 
            // ux_labelWebOrderRequestFirstName
            // 
            this.ux_labelWebOrderRequestFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrderRequestFirstName.AutoSize = true;
            this.ux_labelWebOrderRequestFirstName.Location = new System.Drawing.Point(2, 86);
            this.ux_labelWebOrderRequestFirstName.Name = "ux_labelWebOrderRequestFirstName";
            this.ux_labelWebOrderRequestFirstName.Size = new System.Drawing.Size(61, 13);
            this.ux_labelWebOrderRequestFirstName.TabIndex = 23;
            this.ux_labelWebOrderRequestFirstName.Text = "First Name*";
            // 
            // ux_textboxWebOrderRequestAddressLine1
            // 
            this.ux_textboxWebOrderRequestAddressLine1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrderRequestAddressLine1.Location = new System.Drawing.Point(3, 184);
            this.ux_textboxWebOrderRequestAddressLine1.Name = "ux_textboxWebOrderRequestAddressLine1";
            this.ux_textboxWebOrderRequestAddressLine1.ReadOnly = true;
            this.ux_textboxWebOrderRequestAddressLine1.Size = new System.Drawing.Size(157, 20);
            this.ux_textboxWebOrderRequestAddressLine1.TabIndex = 28;
            // 
            // ux_labelWebOrderRequestOrganization
            // 
            this.ux_labelWebOrderRequestOrganization.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrderRequestOrganization.AutoSize = true;
            this.ux_labelWebOrderRequestOrganization.Location = new System.Drawing.Point(2, 127);
            this.ux_labelWebOrderRequestOrganization.Name = "ux_labelWebOrderRequestOrganization";
            this.ux_labelWebOrderRequestOrganization.Size = new System.Drawing.Size(70, 13);
            this.ux_labelWebOrderRequestOrganization.TabIndex = 25;
            this.ux_labelWebOrderRequestOrganization.Text = "Organization*";
            // 
            // ux_textboxWebOrderRequestTitle
            // 
            this.ux_textboxWebOrderRequestTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxWebOrderRequestTitle.Location = new System.Drawing.Point(3, 60);
            this.ux_textboxWebOrderRequestTitle.Name = "ux_textboxWebOrderRequestTitle";
            this.ux_textboxWebOrderRequestTitle.ReadOnly = true;
            this.ux_textboxWebOrderRequestTitle.Size = new System.Drawing.Size(157, 20);
            this.ux_textboxWebOrderRequestTitle.TabIndex = 22;
            // 
            // ux_labelWebOrderRequestLastName
            // 
            this.ux_labelWebOrderRequestLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelWebOrderRequestLastName.AutoSize = true;
            this.ux_labelWebOrderRequestLastName.Location = new System.Drawing.Point(2, 3);
            this.ux_labelWebOrderRequestLastName.Name = "ux_labelWebOrderRequestLastName";
            this.ux_labelWebOrderRequestLastName.Size = new System.Drawing.Size(62, 13);
            this.ux_labelWebOrderRequestLastName.TabIndex = 19;
            this.ux_labelWebOrderRequestLastName.Text = "Last Name*";
            // 
            // ux_buttonWebOrderCreateNewCooperator
            // 
            this.ux_buttonWebOrderCreateNewCooperator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonWebOrderCreateNewCooperator.Location = new System.Drawing.Point(505, 140);
            this.ux_buttonWebOrderCreateNewCooperator.Name = "ux_buttonWebOrderCreateNewCooperator";
            this.ux_buttonWebOrderCreateNewCooperator.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonWebOrderCreateNewCooperator.TabIndex = 26;
            this.ux_buttonWebOrderCreateNewCooperator.Text = "New...";
            this.ux_buttonWebOrderCreateNewCooperator.UseVisualStyleBackColor = true;
            this.ux_buttonWebOrderCreateNewCooperator.Click += new System.EventHandler(this.ux_buttonWebOrderCreateNewCooperator_Click);
            // 
            // ux_labelWebOrderInstructions
            // 
            this.ux_labelWebOrderInstructions.AutoSize = true;
            this.ux_labelWebOrderInstructions.Location = new System.Drawing.Point(109, 142);
            this.ux_labelWebOrderInstructions.Name = "ux_labelWebOrderInstructions";
            this.ux_labelWebOrderInstructions.Size = new System.Drawing.Size(350, 13);
            this.ux_labelWebOrderInstructions.TabIndex = 25;
            this.ux_labelWebOrderInstructions.Tag = "Double-click a record from the grid below to accept as the \'{0}\' address...";
            this.ux_labelWebOrderInstructions.Text = "Double-click a record from the grid below to accept as the \'{0}\' address...";
            // 
            // ux_groupboxWebOrderCooperatorMatchCriteria
            // 
            this.ux_groupboxWebOrderCooperatorMatchCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxWebOrderCooperatorMatchCriteria.Controls.Add(this.ux_checkboxWebOrderCooperatorGeography);
            this.ux_groupboxWebOrderCooperatorMatchCriteria.Controls.Add(this.ux_checkboxWebOrderCooperatorAddressLine1);
            this.ux_groupboxWebOrderCooperatorMatchCriteria.Controls.Add(this.ux_checkboxWebOrderCooperatorOrganization);
            this.ux_groupboxWebOrderCooperatorMatchCriteria.Controls.Add(this.ux_checkboxWebOrderCooperatorFirstName);
            this.ux_groupboxWebOrderCooperatorMatchCriteria.Controls.Add(this.ux_checkboxWebOrderCooperatorLastName);
            this.ux_groupboxWebOrderCooperatorMatchCriteria.Location = new System.Drawing.Point(5, 33);
            this.ux_groupboxWebOrderCooperatorMatchCriteria.Name = "ux_groupboxWebOrderCooperatorMatchCriteria";
            this.ux_groupboxWebOrderCooperatorMatchCriteria.Size = new System.Drawing.Size(575, 40);
            this.ux_groupboxWebOrderCooperatorMatchCriteria.TabIndex = 24;
            this.ux_groupboxWebOrderCooperatorMatchCriteria.TabStop = false;
            this.ux_groupboxWebOrderCooperatorMatchCriteria.Text = "Find Matches Based On:";
            // 
            // ux_checkboxWebOrderCooperatorGeography
            // 
            this.ux_checkboxWebOrderCooperatorGeography.AutoSize = true;
            this.ux_checkboxWebOrderCooperatorGeography.Checked = true;
            this.ux_checkboxWebOrderCooperatorGeography.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ux_checkboxWebOrderCooperatorGeography.Location = new System.Drawing.Point(484, 17);
            this.ux_checkboxWebOrderCooperatorGeography.Name = "ux_checkboxWebOrderCooperatorGeography";
            this.ux_checkboxWebOrderCooperatorGeography.Size = new System.Drawing.Size(78, 17);
            this.ux_checkboxWebOrderCooperatorGeography.TabIndex = 4;
            this.ux_checkboxWebOrderCooperatorGeography.Text = "Geography";
            this.ux_checkboxWebOrderCooperatorGeography.UseVisualStyleBackColor = true;
            this.ux_checkboxWebOrderCooperatorGeography.CheckedChanged += new System.EventHandler(this.ux_checkboxWebOrderCooperatorFindMatchesBasedOn_CheckedChanged);
            // 
            // ux_checkboxWebOrderCooperatorAddressLine1
            // 
            this.ux_checkboxWebOrderCooperatorAddressLine1.AutoSize = true;
            this.ux_checkboxWebOrderCooperatorAddressLine1.Location = new System.Drawing.Point(352, 17);
            this.ux_checkboxWebOrderCooperatorAddressLine1.Name = "ux_checkboxWebOrderCooperatorAddressLine1";
            this.ux_checkboxWebOrderCooperatorAddressLine1.Size = new System.Drawing.Size(96, 17);
            this.ux_checkboxWebOrderCooperatorAddressLine1.TabIndex = 3;
            this.ux_checkboxWebOrderCooperatorAddressLine1.Text = "Address Line 1";
            this.ux_checkboxWebOrderCooperatorAddressLine1.UseVisualStyleBackColor = true;
            this.ux_checkboxWebOrderCooperatorAddressLine1.CheckedChanged += new System.EventHandler(this.ux_checkboxWebOrderCooperatorFindMatchesBasedOn_CheckedChanged);
            // 
            // ux_checkboxWebOrderCooperatorOrganization
            // 
            this.ux_checkboxWebOrderCooperatorOrganization.AutoSize = true;
            this.ux_checkboxWebOrderCooperatorOrganization.Location = new System.Drawing.Point(231, 17);
            this.ux_checkboxWebOrderCooperatorOrganization.Name = "ux_checkboxWebOrderCooperatorOrganization";
            this.ux_checkboxWebOrderCooperatorOrganization.Size = new System.Drawing.Size(85, 17);
            this.ux_checkboxWebOrderCooperatorOrganization.TabIndex = 2;
            this.ux_checkboxWebOrderCooperatorOrganization.Text = "Organization";
            this.ux_checkboxWebOrderCooperatorOrganization.UseVisualStyleBackColor = true;
            this.ux_checkboxWebOrderCooperatorOrganization.CheckedChanged += new System.EventHandler(this.ux_checkboxWebOrderCooperatorFindMatchesBasedOn_CheckedChanged);
            // 
            // ux_checkboxWebOrderCooperatorFirstName
            // 
            this.ux_checkboxWebOrderCooperatorFirstName.AutoSize = true;
            this.ux_checkboxWebOrderCooperatorFirstName.Location = new System.Drawing.Point(119, 17);
            this.ux_checkboxWebOrderCooperatorFirstName.Name = "ux_checkboxWebOrderCooperatorFirstName";
            this.ux_checkboxWebOrderCooperatorFirstName.Size = new System.Drawing.Size(76, 17);
            this.ux_checkboxWebOrderCooperatorFirstName.TabIndex = 1;
            this.ux_checkboxWebOrderCooperatorFirstName.Text = "First Name";
            this.ux_checkboxWebOrderCooperatorFirstName.UseVisualStyleBackColor = true;
            this.ux_checkboxWebOrderCooperatorFirstName.CheckedChanged += new System.EventHandler(this.ux_checkboxWebOrderCooperatorFindMatchesBasedOn_CheckedChanged);
            // 
            // ux_checkboxWebOrderCooperatorLastName
            // 
            this.ux_checkboxWebOrderCooperatorLastName.AutoSize = true;
            this.ux_checkboxWebOrderCooperatorLastName.Checked = true;
            this.ux_checkboxWebOrderCooperatorLastName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ux_checkboxWebOrderCooperatorLastName.Location = new System.Drawing.Point(6, 17);
            this.ux_checkboxWebOrderCooperatorLastName.Name = "ux_checkboxWebOrderCooperatorLastName";
            this.ux_checkboxWebOrderCooperatorLastName.Size = new System.Drawing.Size(77, 17);
            this.ux_checkboxWebOrderCooperatorLastName.TabIndex = 0;
            this.ux_checkboxWebOrderCooperatorLastName.Text = "Last Name";
            this.ux_checkboxWebOrderCooperatorLastName.UseVisualStyleBackColor = true;
            this.ux_checkboxWebOrderCooperatorLastName.CheckedChanged += new System.EventHandler(this.ux_checkboxWebOrderCooperatorFindMatchesBasedOn_CheckedChanged);
            // 
            // ux_labelSelectedCooperatorAddress
            // 
            this.ux_labelSelectedCooperatorAddress.AutoSize = true;
            this.ux_labelSelectedCooperatorAddress.Location = new System.Drawing.Point(123, 105);
            this.ux_labelSelectedCooperatorAddress.Name = "ux_labelSelectedCooperatorAddress";
            this.ux_labelSelectedCooperatorAddress.Size = new System.Drawing.Size(62, 13);
            this.ux_labelSelectedCooperatorAddress.TabIndex = 23;
            this.ux_labelSelectedCooperatorAddress.Text = "Cooperator:";
            // 
            // ux_labelWebOrderAddress
            // 
            this.ux_labelWebOrderAddress.AutoSize = true;
            this.ux_labelWebOrderAddress.Location = new System.Drawing.Point(97, 84);
            this.ux_labelWebOrderAddress.Name = "ux_labelWebOrderAddress";
            this.ux_labelWebOrderAddress.Size = new System.Drawing.Size(88, 13);
            this.ux_labelWebOrderAddress.TabIndex = 22;
            this.ux_labelWebOrderAddress.Text = "Web Cooperator:";
            // 
            // ux_richtextboxSelectedCooperatorAddress
            // 
            this.ux_richtextboxSelectedCooperatorAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_richtextboxSelectedCooperatorAddress.BackColor = System.Drawing.SystemColors.Control;
            this.ux_richtextboxSelectedCooperatorAddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ux_richtextboxSelectedCooperatorAddress.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_richtextboxSelectedCooperatorAddress.Location = new System.Drawing.Point(193, 104);
            this.ux_richtextboxSelectedCooperatorAddress.Multiline = false;
            this.ux_richtextboxSelectedCooperatorAddress.Name = "ux_richtextboxSelectedCooperatorAddress";
            this.ux_richtextboxSelectedCooperatorAddress.ReadOnly = true;
            this.ux_richtextboxSelectedCooperatorAddress.Size = new System.Drawing.Size(387, 15);
            this.ux_richtextboxSelectedCooperatorAddress.TabIndex = 21;
            this.ux_richtextboxSelectedCooperatorAddress.Text = "Test Text";
            // 
            // ux_groupboxWebOrderAddress
            // 
            this.ux_groupboxWebOrderAddress.Controls.Add(this.ux_radiobuttonShipToAddress);
            this.ux_groupboxWebOrderAddress.Controls.Add(this.ux_radiobuttonPrimaryAddress);
            this.ux_groupboxWebOrderAddress.Location = new System.Drawing.Point(6, 79);
            this.ux_groupboxWebOrderAddress.Name = "ux_groupboxWebOrderAddress";
            this.ux_groupboxWebOrderAddress.Size = new System.Drawing.Size(85, 81);
            this.ux_groupboxWebOrderAddress.TabIndex = 20;
            this.ux_groupboxWebOrderAddress.TabStop = false;
            this.ux_groupboxWebOrderAddress.Text = "Web Address";
            // 
            // ux_radiobuttonShipToAddress
            // 
            this.ux_radiobuttonShipToAddress.AutoSize = true;
            this.ux_radiobuttonShipToAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_radiobuttonShipToAddress.Location = new System.Drawing.Point(9, 42);
            this.ux_radiobuttonShipToAddress.Name = "ux_radiobuttonShipToAddress";
            this.ux_radiobuttonShipToAddress.Size = new System.Drawing.Size(69, 17);
            this.ux_radiobuttonShipToAddress.TabIndex = 1;
            this.ux_radiobuttonShipToAddress.Text = "Ship To";
            this.ux_radiobuttonShipToAddress.UseVisualStyleBackColor = true;
            this.ux_radiobuttonShipToAddress.CheckedChanged += new System.EventHandler(this.ux_radiobuttonShipToAddress_CheckedChanged);
            // 
            // ux_radiobuttonPrimaryAddress
            // 
            this.ux_radiobuttonPrimaryAddress.AutoSize = true;
            this.ux_radiobuttonPrimaryAddress.Checked = true;
            this.ux_radiobuttonPrimaryAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_radiobuttonPrimaryAddress.Location = new System.Drawing.Point(9, 19);
            this.ux_radiobuttonPrimaryAddress.Name = "ux_radiobuttonPrimaryAddress";
            this.ux_radiobuttonPrimaryAddress.Size = new System.Drawing.Size(66, 17);
            this.ux_radiobuttonPrimaryAddress.TabIndex = 0;
            this.ux_radiobuttonPrimaryAddress.TabStop = true;
            this.ux_radiobuttonPrimaryAddress.Text = "Primary";
            this.ux_radiobuttonPrimaryAddress.UseVisualStyleBackColor = true;
            this.ux_radiobuttonPrimaryAddress.CheckedChanged += new System.EventHandler(this.ux_radiobuttonPrimaryAddress_CheckedChanged);
            // 
            // ux_richtextboxWebOrderAddress
            // 
            this.ux_richtextboxWebOrderAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_richtextboxWebOrderAddress.BackColor = System.Drawing.SystemColors.Control;
            this.ux_richtextboxWebOrderAddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ux_richtextboxWebOrderAddress.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_richtextboxWebOrderAddress.Location = new System.Drawing.Point(193, 83);
            this.ux_richtextboxWebOrderAddress.Multiline = false;
            this.ux_richtextboxWebOrderAddress.Name = "ux_richtextboxWebOrderAddress";
            this.ux_richtextboxWebOrderAddress.ReadOnly = true;
            this.ux_richtextboxWebOrderAddress.Size = new System.Drawing.Size(387, 15);
            this.ux_richtextboxWebOrderAddress.TabIndex = 19;
            this.ux_richtextboxWebOrderAddress.Text = "Test Text";
            // 
            // ux_buttonFindWebOrderRequest
            // 
            this.ux_buttonFindWebOrderRequest.Location = new System.Drawing.Point(177, 5);
            this.ux_buttonFindWebOrderRequest.Name = "ux_buttonFindWebOrderRequest";
            this.ux_buttonFindWebOrderRequest.Size = new System.Drawing.Size(105, 23);
            this.ux_buttonFindWebOrderRequest.TabIndex = 18;
            this.ux_buttonFindWebOrderRequest.Text = "Find Web Order";
            this.ux_buttonFindWebOrderRequest.UseVisualStyleBackColor = true;
            this.ux_buttonFindWebOrderRequest.Click += new System.EventHandler(this.ux_buttonFindWebOrderRequest_Click);
            // 
            // ux_textboxWebOrderNumber
            // 
            this.ux_textboxWebOrderNumber.Location = new System.Drawing.Point(71, 7);
            this.ux_textboxWebOrderNumber.Name = "ux_textboxWebOrderNumber";
            this.ux_textboxWebOrderNumber.Size = new System.Drawing.Size(100, 20);
            this.ux_textboxWebOrderNumber.TabIndex = 17;
            // 
            // ux_labelWebOrderNumber
            // 
            this.ux_labelWebOrderNumber.AutoSize = true;
            this.ux_labelWebOrderNumber.Location = new System.Drawing.Point(5, 10);
            this.ux_labelWebOrderNumber.Name = "ux_labelWebOrderNumber";
            this.ux_labelWebOrderNumber.Size = new System.Drawing.Size(62, 13);
            this.ux_labelWebOrderNumber.TabIndex = 14;
            this.ux_labelWebOrderNumber.Text = "Web Order:";
            // 
            // ux_datagridviewWebOrderRequestCooperator
            // 
            this.ux_datagridviewWebOrderRequestCooperator.AllowUserToAddRows = false;
            this.ux_datagridviewWebOrderRequestCooperator.AllowUserToDeleteRows = false;
            this.ux_datagridviewWebOrderRequestCooperator.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewWebOrderRequestCooperator.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.ux_datagridviewWebOrderRequestCooperator.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ux_datagridviewWebOrderRequestCooperator.DefaultCellStyle = dataGridViewCellStyle11;
            this.ux_datagridviewWebOrderRequestCooperator.Location = new System.Drawing.Point(5, 167);
            this.ux_datagridviewWebOrderRequestCooperator.MultiSelect = false;
            this.ux_datagridviewWebOrderRequestCooperator.Name = "ux_datagridviewWebOrderRequestCooperator";
            this.ux_datagridviewWebOrderRequestCooperator.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewWebOrderRequestCooperator.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.ux_datagridviewWebOrderRequestCooperator.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ux_datagridviewWebOrderRequestCooperator.Size = new System.Drawing.Size(575, 187);
            this.ux_datagridviewWebOrderRequestCooperator.TabIndex = 13;
            this.ux_datagridviewWebOrderRequestCooperator.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Ux_datagridviewWebOrderRequestCooperator_CellDoubleClick);
            this.ux_datagridviewWebOrderRequestCooperator.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Ux_datagridviewWebOrderRequestCooperator_CellMouseDoubleClick);
            this.ux_datagridviewWebOrderRequestCooperator.SelectionChanged += new System.EventHandler(this.Ux_datagridviewWebOrderRequestCooperator_SelectionChanged);
            this.ux_datagridviewWebOrderRequestCooperator.DoubleClick += new System.EventHandler(this.Ux_datagridviewWebOrderRequestCooperator_DoubleClick);
            this.ux_datagridviewWebOrderRequestCooperator.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Ux_datagridviewWebOrderRequestCooperator_MouseDoubleClick);
            // 
            // ux_buttonSave
            // 
            this.ux_buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSave.Location = new System.Drawing.Point(555, 5);
            this.ux_buttonSave.Name = "ux_buttonSave";
            this.ux_buttonSave.Size = new System.Drawing.Size(110, 23);
            this.ux_buttonSave.TabIndex = 1;
            this.ux_buttonSave.Text = "Save";
            this.ux_buttonSave.UseVisualStyleBackColor = true;
            this.ux_buttonSave.Click += new System.EventHandler(this.ux_buttonSave_Click);
            // 
            // ux_buttonSaveAndExit
            // 
            this.ux_buttonSaveAndExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSaveAndExit.Location = new System.Drawing.Point(671, 5);
            this.ux_buttonSaveAndExit.Name = "ux_buttonSaveAndExit";
            this.ux_buttonSaveAndExit.Size = new System.Drawing.Size(110, 23);
            this.ux_buttonSaveAndExit.TabIndex = 2;
            this.ux_buttonSaveAndExit.Text = "Save and Exit";
            this.ux_buttonSaveAndExit.UseVisualStyleBackColor = true;
            this.ux_buttonSaveAndExit.Click += new System.EventHandler(this.ux_buttonSaveAndExit_Click);
            // 
            // CooperatorWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 429);
            this.Controls.Add(this.ux_buttonSaveAndExit);
            this.Controls.Add(this.ux_buttonSave);
            this.Controls.Add(this.ux_tabcontrolMain);
            this.Name = "CooperatorWizard";
            this.Text = "Cooperator Wizard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CooperatorWizard_FormClosing);
            this.Load += new System.EventHandler(this.CooperatorWizard_Load);
            this.ux_tabcontrolMain.ResumeLayout(false);
            this.ux_tabpageCooperator.ResumeLayout(false);
            this.ux_splitcontainerCooperator.Panel1.ResumeLayout(false);
            this.ux_splitcontainerCooperator.Panel1.PerformLayout();
            this.ux_splitcontainerCooperator.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerCooperator)).EndInit();
            this.ux_splitcontainerCooperator.ResumeLayout(false);
            this.ux_groupboxMatch.ResumeLayout(false);
            this.ux_groupboxMatch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewCooperator)).EndInit();
            this.ux_tabpageWebCooperator.ResumeLayout(false);
            this.ux_splitcontainerWebCooperator.Panel1.ResumeLayout(false);
            this.ux_splitcontainerWebCooperator.Panel1.PerformLayout();
            this.ux_splitcontainerWebCooperator.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerWebCooperator)).EndInit();
            this.ux_splitcontainerWebCooperator.ResumeLayout(false);
            this.ux_groupboxWebMatch.ResumeLayout(false);
            this.ux_groupboxWebMatch.PerformLayout();
            this.ux_splitcontainerWebCooperatorDataviews.Panel1.ResumeLayout(false);
            this.ux_splitcontainerWebCooperatorDataviews.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerWebCooperatorDataviews)).EndInit();
            this.ux_splitcontainerWebCooperatorDataviews.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewWebCooperatorMaster)).EndInit();
            this.ux_groupboxWebCooperatorMatchCriteria.ResumeLayout(false);
            this.ux_groupboxWebCooperatorMatchCriteria.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewWebCooperatorDetail)).EndInit();
            this.ux_tabpageWebOrder.ResumeLayout(false);
            this.ux_splitcontainerWebOrder.Panel1.ResumeLayout(false);
            this.ux_splitcontainerWebOrder.Panel1.PerformLayout();
            this.ux_splitcontainerWebOrder.Panel2.ResumeLayout(false);
            this.ux_splitcontainerWebOrder.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerWebOrder)).EndInit();
            this.ux_splitcontainerWebOrder.ResumeLayout(false);
            this.ux_panelWebOrderRequestSearchCriteria.ResumeLayout(false);
            this.ux_panelWebOrderRequestSearchCriteria.PerformLayout();
            this.ux_groupboxWebOrderCooperatorMatchCriteria.ResumeLayout(false);
            this.ux_groupboxWebOrderCooperatorMatchCriteria.PerformLayout();
            this.ux_groupboxWebOrderAddress.ResumeLayout(false);
            this.ux_groupboxWebOrderAddress.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewWebOrderRequestCooperator)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl ux_tabcontrolMain;
        private System.Windows.Forms.TabPage ux_tabpageCooperator;
        private System.Windows.Forms.DataGridView ux_datagridviewCooperator;
        private System.Windows.Forms.TabPage ux_tabpageWebCooperator;
        private System.Windows.Forms.Button ux_buttonNewCooperator;
        private System.Windows.Forms.TextBox ux_textboxFirstName;
        private System.Windows.Forms.TextBox ux_textboxLastName;
        private System.Windows.Forms.Label ux_labelSearch;
        private System.Windows.Forms.Label ux_labelFirstName;
        private System.Windows.Forms.Label ux_labelLastName;
        private System.Windows.Forms.Button ux_buttonSave;
        private System.Windows.Forms.Button ux_buttonSaveAndExit;
        private System.Windows.Forms.Button ux_buttonCreateNewCooperator;
        private System.Windows.Forms.TabPage ux_tabpageWebOrder;
        private System.Windows.Forms.TextBox ux_textboxWebOrderNumber;
        private System.Windows.Forms.Label ux_labelWebOrderNumber;
        private System.Windows.Forms.DataGridView ux_datagridviewWebOrderRequestCooperator;
        private System.Windows.Forms.SplitContainer ux_splitcontainerCooperator;
        private System.Windows.Forms.Button ux_buttonFindCooperator;
        private System.Windows.Forms.GroupBox ux_groupboxMatch;
        private System.Windows.Forms.RadioButton ux_radiobuttonExactMatch;
        private System.Windows.Forms.RadioButton ux_radiobuttonPartialMatch;
        private System.Windows.Forms.TextBox ux_textboxCooperatorID;
        private System.Windows.Forms.Label ux_labelCooperatorID;
        private System.Windows.Forms.TextBox ux_textboxPhoneNumber;
        private System.Windows.Forms.Label ux_labelPhoneNumber;
        private System.Windows.Forms.TextBox ux_textboxEMail;
        private System.Windows.Forms.Label ux_labelEMail;
        private System.Windows.Forms.TextBox ux_textboxOrganization;
        private System.Windows.Forms.Label ux_labelOrganization;
        private System.Windows.Forms.SplitContainer ux_splitcontainerWebCooperator;
        private System.Windows.Forms.Button ux_buttonFindWebCooperator;
        private System.Windows.Forms.GroupBox ux_groupboxWebMatch;
        private System.Windows.Forms.RadioButton ux_radiobuttonExactWebMatch;
        private System.Windows.Forms.RadioButton ux_radiobuttonPartialWebMatch;
        private System.Windows.Forms.TextBox ux_textboxWebCooperatorID;
        private System.Windows.Forms.Label ux_labelWebCooperatorID;
        private System.Windows.Forms.TextBox ux_textboxWebPhoneNumber;
        private System.Windows.Forms.Label ux_labelWebPhoneNumber;
        private System.Windows.Forms.TextBox ux_textboxWebEMail;
        private System.Windows.Forms.Label ux_labelWebEMail;
        private System.Windows.Forms.TextBox ux_textboxWebOrganization;
        private System.Windows.Forms.Label ux_labelWebOrganization;
        private System.Windows.Forms.Label ux_labelWebSearch;
        private System.Windows.Forms.TextBox ux_textboxWebFirstName;
        private System.Windows.Forms.Label ux_labelWebFirstName;
        private System.Windows.Forms.TextBox ux_textboxWebLastName;
        private System.Windows.Forms.Label ux_labelWebLastName;
        private System.Windows.Forms.DataGridView ux_datagridviewWebCooperatorMaster;
        private System.Windows.Forms.SplitContainer ux_splitcontainerWebCooperatorDataviews;
        private System.Windows.Forms.DataGridView ux_datagridviewWebCooperatorDetail;
        private System.Windows.Forms.SplitContainer ux_splitcontainerWebOrder;
        private System.Windows.Forms.TextBox ux_textboxWebOrderRequestFirstName;
        private System.Windows.Forms.Label ux_labelWebOrderRequestLastName;
        private System.Windows.Forms.Label ux_labelWebOrderRequestFirstName;
        private System.Windows.Forms.TextBox ux_textboxWebOrderRequestLastName;
        private System.Windows.Forms.TextBox ux_textboxWebOrderRequestTitle;
        private System.Windows.Forms.Label ux_labelWebOrderRequestTitle;
        private System.Windows.Forms.Panel ux_panelWebOrderRequestSearchCriteria;
        private System.Windows.Forms.TextBox ux_textboxWebOrderRequestAddressLine3;
        private System.Windows.Forms.TextBox ux_textboxWebOrderRequestPostalIndex;
        private System.Windows.Forms.TextBox ux_textboxWebOrderRequestOrganization;
        private System.Windows.Forms.Label ux_labelWebOrderRequestCity;
        private System.Windows.Forms.Label ux_labelWebOrderRequestPostalIndex;
        private System.Windows.Forms.TextBox ux_textboxWebOrderRequestAddressLine2;
        private System.Windows.Forms.TextBox ux_textboxWebOrderRequestCity;
        private System.Windows.Forms.Label ux_labelWebOrderRequestAddressLine3;
        private System.Windows.Forms.Label ux_labelWebOrderRequestAddressLine1;
        private System.Windows.Forms.Label ux_labelWebOrderRequestAddressLine2;
        private System.Windows.Forms.TextBox ux_textboxWebOrderRequestAddressLine1;
        private System.Windows.Forms.Label ux_labelWebOrderRequestOrganization;
        private System.Windows.Forms.TextBox ux_textboxWebOrderRequestGeography;
        private System.Windows.Forms.TextBox ux_textboxWebOrderRequestEmail;
        private System.Windows.Forms.TextBox ux_textboxWebOrderRequestPrimaryPhone;
        private System.Windows.Forms.Label ux_labelWebOrderRequestGeography;
        private System.Windows.Forms.Label ux_labelWebOrderRequestPrimaryPhone;
        private System.Windows.Forms.Label ux_labelWebOrderRequestEmail;
        private System.Windows.Forms.Button ux_buttonFindWebOrderRequest;
        private System.Windows.Forms.GroupBox ux_groupboxWebCooperatorMatchCriteria;
        private System.Windows.Forms.CheckBox ux_checkboxWebCooperatorGeography;
        private System.Windows.Forms.CheckBox ux_checkboxWebCooperatorAddressLine1;
        private System.Windows.Forms.CheckBox ux_checkboxWebCooperatorOrganization;
        private System.Windows.Forms.CheckBox ux_checkboxWebCooperatorFirstName;
        private System.Windows.Forms.CheckBox ux_checkboxWebCooperatorLastName;
        private System.Windows.Forms.Label ux_labelWebOrderCooperatorDetails;
        private System.Windows.Forms.Label ux_labelSelectedCooperatorAddress;
        private System.Windows.Forms.Label ux_labelWebOrderAddress;
        private System.Windows.Forms.RichTextBox ux_richtextboxSelectedCooperatorAddress;
        private System.Windows.Forms.GroupBox ux_groupboxWebOrderAddress;
        private System.Windows.Forms.RadioButton ux_radiobuttonShipToAddress;
        private System.Windows.Forms.RadioButton ux_radiobuttonPrimaryAddress;
        private System.Windows.Forms.RichTextBox ux_richtextboxWebOrderAddress;
        private System.Windows.Forms.GroupBox ux_groupboxWebOrderCooperatorMatchCriteria;
        private System.Windows.Forms.CheckBox ux_checkboxWebOrderCooperatorGeography;
        private System.Windows.Forms.CheckBox ux_checkboxWebOrderCooperatorAddressLine1;
        private System.Windows.Forms.CheckBox ux_checkboxWebOrderCooperatorOrganization;
        private System.Windows.Forms.CheckBox ux_checkboxWebOrderCooperatorFirstName;
        private System.Windows.Forms.CheckBox ux_checkboxWebOrderCooperatorLastName;
        private System.Windows.Forms.Label ux_labelWebOrderInstructions;
        private System.Windows.Forms.Button ux_buttonWebOrderCreateNewCooperator;
        private System.Windows.Forms.CheckBox ux_checkboxWebCooperatorIncludeShippingAdresses;
    }
}

