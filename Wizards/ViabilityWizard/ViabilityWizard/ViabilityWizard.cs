﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GRINGlobal.Client.Common;

namespace ViabilityWizard
{
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    public partial class ViabilityWizard : Form, IGRINGlobalDataWizard
    {
        SharedUtils _sharedUtils;
        DataSet _dsI = new DataSet();
        DataSet _dsIV = new DataSet();
        DataSet _dsIVD = new DataSet();
        DataSet _dsIVR = new DataSet();
        DataSet _dsIVRM = new DataSet();
        DataTable _dtIVR = new DataTable();
        string[] _viabilityIDTokens = new string[] { };
        int _currentCountNumber = 1;
        string _currentViabilityRuleFilter = "";

        public ViabilityWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();

            _sharedUtils = sharedUtils;
        }

        public string FormName
        {
            get
            {
                return "Viability Wizard";
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();
                //if (_changedRecords.Tables.Contains(_accession.TableName))
                //{
                //    dt = _changedRecords.Tables[_accession.TableName].Copy();
                //}
                return dt;
            }
        }

        public string PKeyName
        {
            get
            {
                return "inventory_viability_id";
            }
        }

        private void ViabilityWizard_Load(object sender, EventArgs e)
        {
            //// Initialize controls in the GUI...
            //ux_buttonSaveIVDChanges.Visible = true;
            //ux_buttonSaveIVDChanges.Enabled = false;
            //ux_buttonCancelIVDChanges.Visible = true;
            //ux_buttonCancelIVDChanges.Enabled = false;
            //ux_buttonSaveIVChanges.Visible = true;
            //ux_buttonSaveIVChanges.Enabled = false;
            //ux_buttonCancelIVChanges.Visible = true;
            //ux_buttonCancelIVChanges.Enabled = false;
            //ux_buttonChangeDate.Visible = false;
            //ux_buttonCreateFromInventory.Visible = true;
            //ux_buttonCreateFromOrder.Visible = true;
            //ux_buttonCreateViabilityDataRecords.Visible = false;
            //ux_buttonGetInventoryViabilityTest.Visible = true;
            //ux_buttonMakeNewInventoryViabilityData.Visible = false;
            //ux_buttonPostFinalResults.Visible = true;
            //ux_buttonPostFinalResults.Enabled = false;
            //ux_buttonPrintLabels.Visible = false;
            //ux_groupboxCreateViabilityTest.Enabled = true;
            //ux_groupboxGetViabilityTest.Enabled = true;
            //ux_groupboxViabilityData.Enabled = false;
            //ux_groupboxViabilityDate.Enabled = false;
            //ux_groupboxViabilityRule.Enabled = false;
            //ux_groupboxViabilitySummary.Enabled = false;
            //ux_labelMessageDisplay.Visible = true;
            //ux_labelMessageDisplay.ForeColor = Color.Empty;
            //ux_labelMessageDisplay.Text = "";

            //// Remove all unused controls that are dynamically built when valid viability data has been loaded...
            //RemoveViabilityDateControls();
            //RemoveViabilityDataControls();
            //RemoveViabilitySummaryControls();


            // Load the full inventory_viability_rule table into a dataset for quick access when needed...
            _dsIVR = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability_rule", "", 0, 0);

            // Load the datasource for the Inventory Viability Rules dropdown combobox ...
            // 1) Gather up the IDs (from _dsIVR) used by this wizard for entering/creating/updating viability tests...
            string ivrSQL = "SELECT * FROM inventory_viability_rule_lookup";
            if (_dsIVR != null &&
                _dsIVR.Tables.Contains("viability_wizard_get_inventory_viability_rule") &&
                _dsIVR.Tables["viability_wizard_get_inventory_viability_rule"].Columns.Contains("inventory_viability_rule_id") &&
                _dsIVR.Tables["viability_wizard_get_inventory_viability_rule"].Rows.Count > 0)
            {
                string ivrIDs = " WHERE value_member IN (";
                foreach(DataRow dr in _dsIVR.Tables["viability_wizard_get_inventory_viability_rule"].Rows)
                {
                    ivrIDs += dr["inventory_viability_rule_id"].ToString().Trim() + ",";
                }
                ivrSQL += ivrIDs.TrimEnd(new char[] { ',' }) + ")";
            }
            if (_sharedUtils.LocalDatabaseTableExists("inventory_viability_rule_lookup")) _dtIVR = _sharedUtils.GetLocalData(ivrSQL, "");
            ux_comboboxViabilityRule.DisplayMember = "display_member";
            ux_comboboxViabilityRule.ValueMember = "value_member";
            ux_comboboxViabilityRule.DataSource = _dtIVR;
            ux_comboboxViabilityRule.SelectedIndex = -1;

            // Load the full inventory_viability_rule_map table into a dataset for quick access when needed...
            _dsIVRM = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability_rule_map", "", 0, 0);

            // Load the list of Crystal Reports...
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.Windows.Forms.Application.StartupPath + "\\Reports");
            foreach (string reportName in _sharedUtils.GetAppSettingValue("ViabilityWizardCrystalReports").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                foreach (System.IO.FileInfo fi in di.GetFiles(reportName.Trim(), System.IO.SearchOption.AllDirectories))
                {
                    if (fi.Name.Trim().ToUpper() == reportName.Trim().ToUpper())
                    {
                        ux_comboboxCrystalReports.Items.Add(fi.Name);
                    }
                }
            }
            if (ux_comboboxCrystalReports.Items.Count > 0) ux_comboboxCrystalReports.SelectedIndex = 0;

            // Get language translations for the components and controls in this applicataion...
            if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);

            // Reset all controls in the GUI...
            ResetGUI();
        }

        private void ViabilityWizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            // The user might be closing the form with unsaved edits - if so ask the
            // user if they would like to save their data before closing...
            int intRowEdits = 0;
            if (_dsI.Tables.Contains("viability_wizard_get_inventory") && _dsI.Tables["viability_wizard_get_inventory"].GetChanges() != null) intRowEdits = _dsI.Tables["viability_wizard_get_inventory"].GetChanges().Rows.Count;
            if (_dsIV.Tables.Contains("viability_wizard_get_inventory_viability") && _dsIV.Tables["viability_wizard_get_inventory_viability"].GetChanges() != null) intRowEdits += _dsIV.Tables["viability_wizard_get_inventory_viability"].GetChanges().Rows.Count;
            if (_dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") && _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].GetChanges() != null) intRowEdits += _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].GetChanges().Rows.Count;
            if (_dsIVR.Tables.Contains("viability_wizard_get_inventory_viability_rule") && _dsIVR.Tables["viability_wizard_get_inventory_viability_rule"].GetChanges() != null) intRowEdits += _dsIVR.Tables["viability_wizard_get_inventory_viability_rule"].GetChanges().Rows.Count;
            if (_dsIVRM.Tables.Contains("viability_wizard_get_inventory_viability_rule_map") && _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].GetChanges() != null) intRowEdits += _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].GetChanges().Rows.Count;

            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} unsaved row change(s), are you sure you want to cancel your edits and close this window?", "Cancel Edits and Close", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "ViabilityWizard_FormClosingMessage1";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = intRowEdits.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (intRowEdits > 0 && DialogResult.No == ggMessageBox.ShowDialog())
            {
                e.Cancel = true;
            }
        }

        private void ux_buttonMakeNewInventoryViabilityData_Click(object sender, EventArgs e)
        {
            NewViabilityTestForm nvtf = new NewViabilityTestForm(_sharedUtils);
            nvtf.StartPosition = FormStartPosition.CenterParent;
            if (DialogResult.OK == nvtf.ShowDialog())
            {
                MessageBox.Show(nvtf.NewInventoryViabilityKeys);
            }

        }

//        private void ux_buttonGetInventoryViabilityTest_Click(object sender, EventArgs e)
//        {
//            // Let the parsing nightmare begin.....
//            // Keep in mind that a viability can be:
//            //   *  The PKEY integer or it could have an appended '.x' to indicate the Replication Number
//            //      For example it could be "1881784.2" or simply "1881784"
//            //   *  The Inventory Number (friendly name) 
//            //      For example it could be "PI 550473 12ncai01 SD" 
//            //
//            // So now we must parse the text in the textbox to determine if the user has input a Viability Test Number
//            // or an Inventory Number (lot code) that will need to be resolved to a inventory_viability_id
//            double viabilityORinventoryNum = 0;
//            DataSet temp_dsI = new DataSet();
//            DataSet temp_dsIV = new DataSet();
//            DataSet temp_dsIVD = new DataSet();
//            DataSet temp_dsIVR = new DataSet();

//            string[] temp_ivRecordTokens = ux_textboxInventoryViabilityID.Text.Trim().Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

//            // If the user only changed the suffix of the viability information do not requery the database - continue
//            // using the existing data and adjust the table row with the focus...
//            if (_dsIV != null &&
//                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
//                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
//                _dsIVD != null &&
//                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
//                _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count > 0 &&
//                temp_ivRecordTokens.Length > 0 &&
//                _ivRecordTokens.Length > 0 &&
//                temp_ivRecordTokens[0].Trim().ToLower() == _ivRecordTokens[0].Trim().ToLower())
//            {
//                _ivRecordTokens = ux_textboxInventoryViabilityID.Text.Trim().Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                
//                // Rebuild the viability data GUI...
//                BuildViabilityDataPage(_currentCountNumber);
//                // Rebuild the viability test date radio buttons...
//                UpdateCountNumberRadioButtons();
//                // Rebuild the viability summary data...
//                RefreshStats();
//                // If the user input included the Rep number in the textbox string set focus to that record...
//                if (_ivRecordTokens.Length == 2)
//                {
//                    SetTLPControlFocus();
//                }

//                // Now that we have updated the control with focus return back to user input....
//                return;
//            }

//            if (temp_ivRecordTokens.Length != 1 &&
//                temp_ivRecordTokens.Length != 2)
//            {
//                // Could not parse the user input mostly likely an empty textbox so bail out now...
//                // First clear any data that might be lingering about...
//                _dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryviabilityid=-1", 0, 0);
//                _dsIVD = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability_data", ":inventoryviabilityid=-1", 0, 0);
//                _dsIVR = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability_rule", ":inventoryviabilityid=-1", 0, 0);
//                // Next clear the global variable that stores the last used inv/via id to be retrieved...
//                _ivRecordTokens = new string[] { };
//                // Clear all of the controls from the form so the user cannot edit the data...
//                RemoveViabilityDateControls();
//                RemoveViabilityDataControls();
//                RemoveViabilitySummaryControls();
//                ux_textboxCreateFromOrder.Text = "";
//                ux_textboxCreateFromInventory.Text = "";
//                ux_textboxInventoryViabilityID.Text = "";
//                ux_textboxInventoryID.Text = "";
//                ux_textboxPercentViable.Text = "";
//                ux_textboxViabilityTestedDate.Text = "";
//                ux_comboboxViabilityRule.SelectedItem = null;
//                ux_textboxNumberOfReplicates.Text = "";
//                ux_textboxTotalSeeds.Text = "";
//                // Reset the controls to default state...
//                ux_groupboxGetExistingViabilityTest.Enabled = true;
//                ux_groupboxCreateNewViabilityTest.Enabled = true;
//                ux_buttonSaveIVDChanges.Visible = false;
//                ux_buttonCancelIVDChanges.Visible = false;
//                if (ViabilityTestingComplete())
//                {
//                    ux_buttonPostFinalResults.Visible = true;
//                }
//                else
//                {
//                    ux_buttonPostFinalResults.Visible = false;
//                }

//                // Now return...
//                return;
//            }

//            if (double.TryParse(ux_textboxInventoryViabilityID.Text.Trim(), out viabilityORinventoryNum))
//            {
//                // No chars in the string (so this appears to be an inventory_viablity_id) - check to see if a viability test record exists with this PKEY...
//                temp_dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryviabilityid=" + temp_ivRecordTokens[0], 0, 0);
//                if (temp_dsIV != null &&
//                    temp_dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
//                    temp_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1)
//                {
//                    // Looks like we found a viability test record - but check to see if the user input can also be 
//                    // an Inventory Number (and prompt the user to choose if there is also a matching inventory record)...
//                    string inventoryID = _sharedUtils.GetLookupValueMember(null, "inventory_lookup", ux_textboxInventoryViabilityID.Text.Trim(), null, "!Error!");
//                    if (inventoryID.ToUpper() != "!ERROR!")
//                    {
//                        temp_dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryid=" + inventoryID.Trim(), 0, 0);
//                        // Well this is interesting...  Apparently the user input can be either resolved to a viability PKEY or an inventory PKEY
//                        // so make the user choose...
//                        if (temp_dsIV != null &&
//                            temp_dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
//                            temp_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count > 0)
//                        {
//                            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data entered matches both an Inventory Viability record and an Inventory Lot Code.\n\n  Is this an Inventory Viability Record?", "Viability Wizard Data Retrieve Conflict", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
//                            ggMessageBox.Name = "ViabilityWizard_ux_buttonGetInventoryViabilityData1";
//                            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
//                            string[] argsArray = new string[100];
//                            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
//                            if (DialogResult.No == ggMessageBox.ShowDialog())
//                            {
//                                // The user has indicated this is an inventory record
//                                // so update the ivRecordTokens to reflect this...
//                                string ivMaxPKEY = temp_dsIV.Tables["viability_wizard_get_inventory_viability"].Compute("MAX(inventory_viability_id)", null).ToString();
//                                temp_ivRecordTokens = new string[] { ivMaxPKEY };
//                            }
//                        }
//                    }
//                }
//                else
//                {
//                    // The user's input does not match any existing inventory_viability_id records in the table so 
//                    // try interpretting the input as an inventory_id instead...
//                    string inventoryID = _sharedUtils.GetLookupValueMember(null, "inventory_lookup", ux_textboxInventoryViabilityID.Text.Trim(), null, "!Error!");
//                    if (inventoryID.ToUpper() != "!ERROR!")
//                    {
//                        temp_dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryid=" + inventoryID.Trim(), 0, 0);
//                        if (temp_dsIV != null &&
//                            temp_dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
//                            temp_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count > 0)
//                        {
//                            // There are inventory_viability records related to this inventory_id so get the most recent viability record and use that
//                            // to update the ivRecordTokens...
//                            string ivMaxPKEY = temp_dsIV.Tables["viability_wizard_get_inventory_viability"].Compute("MAX(inventory_viability_id)", null).ToString();
//                            temp_ivRecordTokens = new string[] { ivMaxPKEY };
//                        }
//                    }
//                    else
//                    {
//                        return;
//                    }
//                }
//            }
//            else
//            {
//                // The user input contains alpha characters in the string - this must not be an Inventory Viabilty Number - so attempt to
//                // interpret it as an Inventory Number instead...
//                string inventoryID = _sharedUtils.GetLookupValueMember(null, "inventory_lookup", ux_textboxInventoryViabilityID.Text.Trim(), null, "!Error!");
//                if (inventoryID.ToUpper() != "!ERROR!")
//                {
//                    temp_dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryid=" + inventoryID.Trim(), 0, 0);
//                    if (temp_dsIV != null &&
//                        temp_dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
//                        temp_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count > 0)
//                    {
//                        // There are inventory_viability records related to this inventory_id so get the most recent viability record and use that
//                        // to update the ivRecordTokens...
//                        string ivMaxPKEY = temp_dsIV.Tables["viability_wizard_get_inventory_viability"].Compute("MAX(inventory_viability_id)", null).ToString();
//                        temp_ivRecordTokens = new string[] { ivMaxPKEY };
//                    }
//                }
//                else
//                {
//                    return;
//                }
//            }

//            // If we are here in the code then the text in the textbox is new and we need to update the global variable for remembering
//            // which viability test the user is working on...
//            _ivRecordTokens = temp_ivRecordTokens;
            
//            //Update the textbox with the inventory_viability_id + rep number 
//            // to handle the situation where the user input a inventory number instead...
//            string ivIDRep = "";
//            foreach (string token in _ivRecordTokens)
//            {
//                ivIDRep += token + ".";
//            }
//            ux_textboxInventoryViabilityID.Text = ivIDRep.TrimEnd('.');
            
//            // Now that we remember the user input for the test to process, go get the data and load it into the global datasets...
//            _dsI = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory", ":inventoryviabilityid=" + _ivRecordTokens[0], 0, 0);
//            _dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryviabilityid=" + _ivRecordTokens[0], 0, 0);
//            _dsIVD = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability_data", ":inventoryviabilityid=" + _ivRecordTokens[0], 0, 0);

//            if (_dsIV != null &&
//                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
//                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
//                (string.IsNullOrEmpty(_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"].ToString())) ||
//                string.IsNullOrEmpty(_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["total_tested_count"].ToString()) ||
//                string.IsNullOrEmpty(_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"].ToString()))
//            {
//                ux_groupboxViabilityRule.Enabled = true;
//                return;
//            }

//            if (_dsI != null &&
//                _dsI.Tables.Contains("viability_wizard_get_inventory") &&
//                _dsI.Tables["viability_wizard_get_inventory"].Rows.Count == 1 && 
//                _dsIV != null &&
//                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
//                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
//                _dsIVD != null &&
//                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
//                _dsIVR != null &&
//                _dsIVR.Tables.Contains("viability_wizard_get_inventory_viability_rule"))
//            {
//                string currentRadioButtonCountNumber = "ux_radiobuttonCount1";
//                DataTable dtIVD = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"];

//                // Update the inventory specific controls...
//                ux_textboxInventoryID.Text = "";
//                ux_textboxPercentViable.Text = "";
//                ux_textboxViabilityTestedDate.Text = "";
//                if (_dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("inventory_id") &&
//                    _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["inventory_id"] != DBNull.Value) ux_textboxInventoryID.Text = _sharedUtils.GetLookupDisplayMember("inventory_lookup", _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["inventory_id"].ToString(), "", "!Error!");
//                if (_dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("percent_viable") &&
//                    _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["percent_viable"] != DBNull.Value) ux_textboxPercentViable.Text = _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["percent_viable"].ToString() + "%";
//                if (_dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("tested_date") &&
//                    _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["tested_date"] != DBNull.Value) ux_textboxViabilityTestedDate.Text = ((DateTime)_dsI.Tables["viability_wizard_get_inventory"].Rows[0]["tested_date"]).ToString("d");
//                if (_dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("viability_tested_date") &&
//                    _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["viability_tested_date"] != DBNull.Value) ux_textboxViabilityTestedDate.Text = ((DateTime)_dsI.Tables["viability_wizard_get_inventory"].Rows[0]["viability_tested_date"]).ToString("d");

//                // Update the inventory_viability_rule specific controls...
//                if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("inventory_viability_rule_id"))
//                {
//                    if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"] != DBNull.Value)
//                    {
//                        ux_comboboxViabilityRule.SelectedValue = _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"].ToString();
//                    }
//                    else
//                    {
//                        ux_comboboxViabilityRule.SelectedItem = null;
//                    }
//                }
//                // Check to see if setting the value of combobox viability_rule above inadvertently modified the existing inventory_viability 
//                // record's other fields (the rule and # reps and # seeds are interconnected via events)...
//                if (_dsIV.GetChanges() != null)
//                {
//                    _dsIV.RejectChanges();
//                }
//                // Update the total seed count...
//                if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("total_tested_count"))
//                {
//                    if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["total_tested_count"] != DBNull.Value)
//                    {
//                        ux_textboxTotalSeeds.Text = _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["total_tested_count"].ToString();
//                    }
//                    else
//                    {
//                        //ux_textboxTotalSeeds.Text = "";
//                    }
//                }
//                // Update the number of replicates...
//                if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("replication_count"))
//                {
//                    if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"] != DBNull.Value)
//                    {
//                        ux_textboxNumberOfReplicates.Text = _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"].ToString();
//                    }
//                    else
//                    {
//                        //ux_textboxNumberOfReplicates.Text = "";
//                    }
//                }

//                // Update the radio buttons and count data controls to match the vaules for this viability test...
//                if (_dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count == 0)
//                {
//                    // No count data is saved for this viability test and no viability rule has been chosen so clear the form...
//                    // Clear all of the controls used to update the counts (lingering info from the last viability test)...
//                    RemoveViabilityDataControls();
//                    RemoveViabilitySummaryControls();

//                    // This is a new viability test that does not have any raw data counts saved yet
//                    // so first clear all the count radio buttons and then update the interface...
//                    for (int i = 1; i <= 8; i++)
//                    {
//                        string radioButtonName = "ux_radiobuttonCount" + i.ToString();
//                        // Find the actual radio button control by name and update the text with the count date...
//                        Control[] rb = Controls.Find(radioButtonName, true);
//                        if (rb.Length == 1)
//                        {
//                            // Check to see if this radio button is checked and if so uncheck it 
//                            // (this ensures the radio button event will fire when the user clicks the radiobutton)...
//                            if (((RadioButton)rb[0]).Checked) ((RadioButton)rb[0]).Checked = false;
//                        }
//                    }

//                    // Update the radiobutton controls to reflect the current status of this viability test...
//                    UpdateCountNumberRadioButtons();
//                }
//                else
//                {
//                    // Update the count_number radio buttons visibility and text to reflect
//                    // this new inventory_viability data just retrieved...
//                    UpdateCountNumberRadioButtons();
//                }

//                // Calculate the maximum count_number in this viability test...
//                int MaxCountNumber = 0;
//                if (dtIVD.Rows.Count > 0)
//                {
//                    MaxCountNumber = Convert.ToInt32(dtIVD.Compute("MAX(count_number)", null));
//                }

//                // Filter the table to view only tests conducted less than 24 hours ago...
//dtIVD.DefaultView.RowFilter = "count_date >= '" + DateTime.Today.Date.ToString() + "'";
////dtIVD.DefaultView.RowFilter = "count_date >= '" + DateTime.Now.Subtract(TimeSpan.FromMinutes(5.0)).ToString() + "'";
                
//                // Calculate the name of the radio button to make current...
//                if (dtIVD.DefaultView.Count == 0)
//                {
//                    // Looks like there has not been a count started in the last day so consider
//                    // this to be a new "count date" and calculate the lowest unused count_number...
//                    if(!ViabilityTestingComplete()) MaxCountNumber++;
//                    currentRadioButtonCountNumber = "ux_radiobuttonCount" + MaxCountNumber.ToString();
//                }
//                else
//                {
//                    // This data was entered today so consider this a "continuation" of data entry
//                    // for the current date and active the radio button associate with this date
//                    // Use the highest value for count_number to set the radio button...
//                    //currentRadioButtonCountNumber = "ux_radiobuttonCount" + dtIVD.DefaultView[0]["count_number"].ToString();
//                    currentRadioButtonCountNumber = "ux_radiobuttonCount" + MaxCountNumber.ToString();
//                }
                
//                // Find the actual radio button control by name and check it...
//                Control[] currentCountNumber = Controls.Find(currentRadioButtonCountNumber, true);
//                if (currentCountNumber.Length == 1)
//                {
//                    if (!string.IsNullOrEmpty(ux_textboxNumberOfReplicates.Text) &&
//                        !string.IsNullOrEmpty(ux_textboxTotalSeeds.Text))
//                    {
//                        // If this radiobutton is currently checked - uncheck it so it can receive a new 'checked event'...
//                        if (((RadioButton)currentCountNumber[0]).Checked) ((RadioButton)currentCountNumber[0]).Checked = false;
//                        // Check the radio button associated with the current count number...
//                        ((RadioButton)currentCountNumber[0]).Checked = true;
//                    }
//                    else
//                    {
//                        // Leave the button unchecked for now...
//                        ((RadioButton)currentCountNumber[0]).Checked = false;
//                    }
//                }

//                // If the user input included the Rep number in the string set focus to that record...
//                if (_ivRecordTokens.Length == 2)
//                {
//                    SetTLPControlFocus();
//                }

//                // Disable groups that should not be used during viability data entry...
//                ux_groupboxCreateNewViabilityTest.Enabled = false;

//                // Enable groups and controls that should/need to be used during viability data entry...
//                ux_groupboxViabilityData.Enabled = true;
//                ux_groupboxViabilityDate.Enabled = true;
//                ux_buttonCancelIVDChanges.Visible = true;

//                // Check to see if Save/Cancel buttons should be visible...
//                if (_dsIV != null &&
//                    _dsIV.GetChanges() != null)
//                {
//                    ux_buttonSaveIVChanges.Visible = true;
//                    ux_buttonCancelIVChanges.Visible = true;
//                }
//                else
//                {
//                    ux_buttonSaveIVChanges.Visible = false;
//                    ux_buttonCancelIVChanges.Visible = false;
//                }
//                if (_dsIVD != null &&
//                    _dsIVD.GetChanges() != null)
//                {
//                    ux_buttonSaveIVDChanges.Visible = true;
//                    ux_buttonCancelIVDChanges.Visible = true;
//                }
//                else
//                {
//                    ux_buttonSaveIVDChanges.Visible = false;
//                    //ux_buttonCancelIVDChanges.Visible = false;
//                }
//            }
//        }

        private void ux_textboxInventoryViabilityID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                ux_buttonGetInventoryViabilityTest.PerformClick();
            }
        }

        private void ux_textboxInventoryViabilityID_TextChanged(object sender, EventArgs e)
        {
            //if (System.Text.RegularExpressions.Regex.Match(ux_textboxInventoryViabilityID.Text.Trim(), @"^\s*(?:like\s+|LIKE\s+)\s*").Success)
            {
            }
            //if (IsValidInventoryNumber(ux_textboxInventoryViabilityID.Text.Trim()) ||
            //    IsValidViabilityID(ux_textboxInventoryViabilityID.Text.Trim()))
            //{
            //}
        }

        private void ux_buttonGetInventoryViabilityTest_Click(object sender, EventArgs e)
        {
            // Let the parsing nightmare begin.....
            // Keep in mind that a viability can be:
            //   *  The PKEY integer or it could have an appended '.x' to indicate the Replication Number
            //      For example it could be "1881784.2" or simply "1881784"
            //   *  The Inventory Number (friendly name) 
            //      For example it could be "PI 550473 12ncai01 SD" 
            //
            // So now we must parse the text in the textbox to determine if the user has input a Viability Test Number
            // or an Inventory Number (lot code) that will need to be resolved to a inventory_viability_id

            string[] temp_ViabilityIDTokens = new string[] { };

            // Parse the user input to get a valid inventory_viability_id...
            if (IsValidInventoryNumber(ux_textboxInventoryViabilityID.Text.Trim()))
            {
                // This is an inventory lot code - so find the most likely viability test that 
                // is associated with this inventory number and populate the GUI with the viability id...
                string viabilityID = "";
                viabilityID = ConvertInventoryNumberToViabilityID(ux_textboxInventoryViabilityID.Text.Trim());
                temp_ViabilityIDTokens = new string[] { viabilityID, "1" };
                ux_textboxInventoryViabilityID.Text = viabilityID + ".1";
            }
            else if (IsValidViabilityID(ux_textboxInventoryViabilityID.Text.Trim()))
            {
                // This is a viability id so process it directly...
                temp_ViabilityIDTokens = ux_textboxInventoryViabilityID.Text.Trim().Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                // Could not parse the user input mostly likely an empty textbox so bail out now...
                                // First clear any data that might be lingering about...
                _dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryviabilityid=-1", 0, 0);
                _dsIVD = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability_data", ":inventoryviabilityid=-1", 0, 0);
                // Next clear the global variable that stores the last used inv/via id to be retrieved...
                _viabilityIDTokens = new string[] { };
                // Reset the GUI to the default empty state...
                ResetGUI();
                // Now return...
                return;
            }

            // If we made it here in the code then the text in the textbox is resolvable to a inventory_viability_id - so we need to 
            // update the global variable for remembering which viability test the user is working on...
            _viabilityIDTokens = temp_ViabilityIDTokens;

            if (IsViabilityDataLoaded(_viabilityIDTokens[0]))
            {
                // If the user only changed the suffix of the viability information do not requery the database - continue
                // using the existing data and adjust the GUI focus to point at the correct table row...

                // This is a sanity check (this count date/number should exist but who knows how long it has been between rep counts)
                // so just in case the tester took longer than the 'normal' amount of time (24 hours) to count between reps - create a new
                // count date to reflect the elasped time required to count all of the reps...
                if (!IsCountDataAvailable(_currentCountNumber))
                {
                    CreateNewViabilityDataRecords(_currentCountNumber);
                }
                // Rebuild the viability data GUI...
                BuildViabilityDataPage(_currentCountNumber);
                UpdateViabilityRule();
                // Rebuild the viability test date radio buttons...
                UpdateViabilityDate();
                // Rebuild the viability summary data...
                UpdateViabilitySummary();
                // If the user input included the Rep number in the textbox string set focus to that record...
                if (_viabilityIDTokens.Length == 2)
                {
                    SetTLPControlFocus();
                }

                // Now that we have updated the control with focus return back to user input....
                return;
            }
            else
            {
                // The user is requesting data for a different viability test - so before getting the new data make sure current data is saved...
                if (!IsViabilityDataSaved())
                {
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have unsaved viability data - would you like to save those changes now before loading the new data?", "Save Edits", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "ViabilityWizard_FormClosingMessage1";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    string[] argsArray = new string[100];
                    argsArray[0] = "";
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    if (DialogResult.Yes == ggMessageBox.ShowDialog())
                    {
                        //SaveViabilityData();
                        // Instead of attempting to save all 4 areas of the data - return the user to the interface to decide what to save...
                        return;
                    }
                }

                // Now we can safely go get the new data and load it into the global datasets...
                _dsI = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory", ":inventoryviabilityid=" + _viabilityIDTokens[0], 0, 0);
                _dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryviabilityid=" + _viabilityIDTokens[0], 0, 0);
                _dsIVD = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability_data", ":inventoryviabilityid=" + _viabilityIDTokens[0], 0, 0);

                // Reset and clear all of the dynamic controls...
                ResetViabilityRule();
                ResetViabilityDate();
                ResetViabilityData();
            }

            // If any of the vital test parameters are missing put the user back into the GUI to complete that before creating new viability test rows...
            if (_dsIV != null &&
                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1)
            {
                // Update the GUI for the IV rules based on current data loaded...
                UpdateViabilityRule();
                // Now apply the IV data to the controls...
                ux_comboboxViabilityRule.SelectedValue = _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"];
                ux_textboxNumberOfReplicates.Text = _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"].ToString();
                ux_textboxTotalSeeds.Text = _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["total_tested_count"].ToString();
                // Since the viability rule controls have been populated with exactly what is in the record - disable the save/cancel buttons...
                ux_buttonSaveIVChanges.Visible = true;
                ux_buttonSaveIVChanges.Enabled = false;
                ux_buttonCancelIVChanges.Visible = true;
                ux_buttonCancelIVChanges.Enabled = false;
                // If either the number of seeds or number of reps is missing in the IV record - enable the groupbox for user edits...
                if (string.IsNullOrEmpty(_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["total_tested_count"].ToString()) ||
                    string.IsNullOrEmpty(_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"].ToString()))
                {
                    ux_groupboxViabilityRule.Enabled = true;
                    UpdateGetViabilityTest();
                    UpdateCreateViabilityTest();
                    UpdateViabilityRule();
                    UpdateViabilityData();
                    UpdateViabilityDate();
                    UpdateViabilitySummary();

                    return;
                }
                else
                {
                    // Make sure that the IVD datarows exist and that they are less than 24 hours old
                    int lastCount = GetMaxCount();
                    DateTime lastCountDate = default(DateTime);

                    if (lastCount > 0)
                    {
                        // Get the date the last count was done on...
                        DataRow[] lastCountDataRows = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Select("count_number=" + lastCount, "replication_number DESC");
                        if (lastCountDataRows.Length > 0)
                        {
                            string lastCountDateString = lastCountDataRows[0]["count_date"].ToString();
                            if (!DateTime.TryParse(lastCountDateString, out lastCountDate))
                            {
                                lastCountDate = default(DateTime);
                            }
                            if (DateTime.Now.Subtract(lastCountDate).Days > 0 &&
                                !IsViabilityTestingComplete(false))
                            {
                                _currentCountNumber = lastCount + 1;
                            }
                            else
                            {
                                _currentCountNumber = lastCount;
                            }
                        }
                    }
                    else
                    {
                        // There is no raw-data IVD rows for this viability test - so create the first count rows now...
                        _currentCountNumber = 1;
                        CreateNewViabilityDataRecords(_currentCountNumber);
                    }
                }
            }

            if (IsCountDataAvailable(_currentCountNumber))
            {
                // Rebuild the viability data GUI...
                BuildViabilityDataPage(_currentCountNumber);
                // If the user input included the Rep number in the textbox string set focus to that record...
                if (_viabilityIDTokens.Length == 2)
                {
                    SetTLPControlFocus();
                }
            }

            if (_dsI != null &&
                _dsI.Tables.Contains("viability_wizard_get_inventory") &&
                _dsI.Tables["viability_wizard_get_inventory"].Rows.Count == 1 &&
                _dsIV != null &&
                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
                _dsIVD != null &&
                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                _dsIVR != null &&
                _dsIVR.Tables.Contains("viability_wizard_get_inventory_viability_rule"))
            {
                UpdateGetViabilityTest();
                UpdateCreateViabilityTest();
                UpdateViabilityRule();
                UpdateViabilityData();
                UpdateViabilityDate();
                UpdateViabilitySummary();

                // Check the radio button associated with the currently active test date...
                Control[] currentDateRadioButton = Controls.Find("ux_radiobuttonCount" + _currentCountNumber.ToString(), true);
                if (currentDateRadioButton.Length == 1)
                {
                    // If this radiobutton is currently checked - uncheck it so it can receive a new 'checked event'...
                    if (((RadioButton)currentDateRadioButton[0]).Checked) ((RadioButton)currentDateRadioButton[0]).Checked = false;
                    // Check the radio button associated with the current count number...
                    ((RadioButton)currentDateRadioButton[0]).Checked = true;
                }
            }
        }

       private void CreateNewViabilityDataRecords(int countNumber)
        {
            // Create new inventory viability data records for the countNumber...
            int NumReps = 0;
            int SeedsPerRep = 0;

            NumReps = (int)_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"];
            SeedsPerRep = (int)_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["total_tested_count"] / (int)_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"];

            // Make the empty rows (one for each Replicate)...
            DateTime countDate = DateTime.Now;
            for (int i = 1; i <= NumReps; i++)
            {
                DataRow dr = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].NewRow();
                int ivID = -1;
                if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count > 0 &&
                    _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("inventory_viability_id") &&
                    _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_id"] != DBNull.Value)
                {
                    ivID = (int)_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_id"];
                }
                dr["inventory_viability_id"] = ivID;
                dr["counter_cooperator_id"] = _sharedUtils.UserCooperatorID;
                dr["replication_number"] = i;
                dr["count_number"] = countNumber;
                dr["count_date"] = countDate;
                dr["normal_count"] = 0;
                // To populate replicaton_count - use default seeds per replicate amount to start with...
                dr["replication_count"] = SeedsPerRep;
                // But override replication_count with value from previous count (if different from default)...
                if (countNumber - 1 > 0)
                {
                    DataRow[] drs = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Select("count_number=" + (countNumber - 1).ToString() + " AND replication_number=" + i.ToString());
                    int lastSeedsPerRep;
                    if (drs.Length == 1 &&
                        int.TryParse(drs[0]["replication_count"].ToString(), out lastSeedsPerRep) &&
                        lastSeedsPerRep != SeedsPerRep)
                    {
                        dr["replication_count"] = lastSeedsPerRep;
                    }
                }
                _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Add(dr);
            }
        }

        private bool IsCountDataAvailable(int countNumber)
        {
            bool countDataAvailable = false;
            if (_dsIVD != null &&
                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count > 0)
            {
                DataRow[] countNumberRecords =_dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Select("count_number=" + countNumber.ToString(), "replication_number DESC");
                if (countNumberRecords != null &&
                    countNumberRecords.Length > 0)
                {
                    countDataAvailable = true;
                }
            }
            return countDataAvailable;
        }

        private int GetMaxRepNum()
        {
            int maxRepNum = 0;

            if (_dsIVD != null &&
                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count > 0)
            {
                string maxReplicationNumber = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Compute("MAX(replication_number)", null).ToString();
                if (!string.IsNullOrEmpty(maxReplicationNumber) &&
                    int.TryParse(maxReplicationNumber, out maxRepNum))
                {
                }
                else
                {
                    maxRepNum = 0;
                }
            }

            return maxRepNum;
        }

        private int GetMaxCount()
        {
            int maxCount = 0;

            if (_dsIVD != null &&
                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count > 0)
            {
                string maxCountNumber = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Compute("MAX(count_number)", null).ToString();
                if (!string.IsNullOrEmpty(maxCountNumber) &&
                    int.TryParse(maxCountNumber, out maxCount))
                {
                }
                else
                {
                    maxCount = 0;
                }
            }

            return maxCount;
        }

        private bool IsViabilityDataSaved()
        {
            bool dataSaved = true;
            if (_dsIV != null &&
                _dsIV.GetChanges() != null)
            {
                dataSaved = false;
            }
            if (_dsIVD != null &&
                _dsIVD.GetChanges() != null)
            {
                dataSaved = false;
            }
            // Check to see if the user has updated anything about the viability test parameters (or notes)...
            if (_dsIV != null &&
                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
                ((ux_comboboxViabilityRule.SelectedValue != null && ux_comboboxViabilityRule.SelectedValue.ToString().Trim() != _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"].ToString().Trim()) ||
                 ux_textboxTotalSeeds.Text.Trim() != _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["total_tested_count"].ToString().Trim() ||
                 ux_textboxNumberOfReplicates.Text.Trim() != _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"].ToString().Trim() ||
                 ux_textboxViabilityNotes.Text.Trim() != _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["note"].ToString().Trim()))
            {
                dataSaved = false;
            }

            return dataSaved;
        }

        private bool IsViabilityDataLoaded(string viabilityID)
        {
            bool viabilityDataLoaded = false;
            if (_dsIV != null &&
                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("inventory_viability_id") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_id"].ToString().ToUpper().Trim() == viabilityID.ToUpper().Trim() &&
                _dsIVD != null &&
                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count > 0)
            {
                viabilityDataLoaded = true;
            }
            return viabilityDataLoaded;
        }

        private string ConvertInventoryNumberToViabilityID(string inventoryNum)
        {
            string inventoryID = _sharedUtils.GetLookupValueMember(null, "inventory_lookup", inventoryNum, null, "!Error!");
            DataSet temp_dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryid=" + inventoryID.Trim(), 0, 0);

            if (temp_dsIV != null &&
                temp_dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                temp_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count > 0)
            {
                // The user has indicated this is an inventory record
                // so update the ivRecordTokens to reflect this...
                string ivMaxPKEY = temp_dsIV.Tables["viability_wizard_get_inventory_viability"].Compute("MAX(inventory_viability_id)", null).ToString();
                return ivMaxPKEY;
            }

            return "";
        }

        private bool IsValidViabilityID(string viabilityID)
        {
            bool validViabilityID = false;
            double viabilityID_rep = 0.0;
            if (double.TryParse(viabilityID, out viabilityID_rep))
            {
                string[] temp_ViabilityID_RepTokens = viabilityID.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                // No chars in the string (so this appears to be an inventory_viablity_id) - check to see if a viability test record exists with this PKEY...
                DataSet temp_dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryviabilityid=" + temp_ViabilityID_RepTokens[0], 0, 0);
                if (temp_dsIV != null &&
                    temp_dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                    temp_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1)
                {
                    validViabilityID = true;
                }
            }

            return validViabilityID;
        }

        private bool IsValidInventoryNumber(string inventoryNum)
        {
            bool validInventoryNumber = false;
            string inventoryID = _sharedUtils.GetLookupValueMember(null, "inventory_lookup", inventoryNum, null, "!Error!");
            if (inventoryID.ToUpper() != "!ERROR!")
            {
                validInventoryNumber = true;
            }

            return validInventoryNumber;
        }

        private bool IsTaxonomyMappedToRule()
        {
            bool isTaxonomyMapped = true;
            if (_dsI != null &&
                _dsI.Tables.Contains("viability_wizard_get_inventory") &&
                _dsI.Tables["viability_wizard_get_inventory"].Rows.Count == 1 &&
                _dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("taxonomy_species_id") &&
                _dsIVRM != null &&
                _dsIVRM.Tables.Contains("viability_wizard_get_inventory_viability_rule_map") &&
                _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].Columns.Contains("taxonomy_species_id") &&
                _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].Columns.Contains("inventory_viability_rule_id") &&
                _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].Rows.Count > 0 &&
                ux_comboboxViabilityRule.SelectedValue != null)
            {
                string ivrmFilter = _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView.RowFilter;
                _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView.RowFilter = "inventory_viability_rule_id=" + ux_comboboxViabilityRule.SelectedValue.ToString() + " AND taxonomy_species_id=" + _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["taxonomy_species_id"].ToString();
                if (_dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView.Count > 0)
                {
                    isTaxonomyMapped = true;
                }
                else
                {
                    isTaxonomyMapped = false;
                }
                _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView.RowFilter = ivrmFilter;
            }
            return isTaxonomyMapped;
        }
        
        private void SetTLPControlFocus()
        {
            // Set the focal point for data entry...
            foreach (Control ctrl in ux_groupboxViabilityData.Controls)
            {
                // There should be one TableLayoutPanel per replicate in the currently active viability test...
                if (ctrl is TableLayoutPanel)
                {
                    foreach (Control rowCTRL in ctrl.Controls)
                    {
                        if (rowCTRL.Name.ToLower() == "replication_number" &&
                            _viabilityIDTokens.Length == 2 &&
                            rowCTRL.Text.ToLower() == _viabilityIDTokens[1].ToLower())
                        {
                            // Enable this TLP for editing...
                            ctrl.Enabled = true;
                            foreach (Control ctrlGettingFocus in ctrl.Controls)
                            {
                                if (ctrlGettingFocus.Name.ToLower() == "normal_count")
                                {
                                    ctrlGettingFocus.Focus();
                                    break;
                                }
                            }
                            break;
                        }
                        else
                        {
                            // Disable this TLP for editing....
                            ctrl.Enabled = false;
                        }
                    }
                }
            }
        }

        private void ResetGUI()
        {
            // Clear all of the dynamic controls from the form so the user cannot edit the data...
            RemoveViabilityDateControls();
            RemoveViabilityDataControls();
            RemoveViabilitySummaryControls();

            // Clear data from perm control...
            ResetCreateViabilityTest();
            ResetGetViabilityTest();
            ResetViabilityRule();

            // Clear the Message Display......
            ux_labelMessageDisplay.Visible = true;
            ux_labelMessageDisplay.ForeColor = Color.Empty;
            ux_labelMessageDisplay.Text = "";
            
            // Reset the perm controls to default enabled/visible state...
            ux_buttonSaveIVDChanges.Visible = true;
            ux_buttonSaveIVDChanges.Enabled = false;
            ux_buttonCancelIVDChanges.Visible = true;
            ux_buttonCancelIVDChanges.Enabled = false;
            if (IsViabilityTestingComplete(true))
            {
                ux_buttonPostFinalResults.Visible = true;
                ux_buttonPostFinalResults.Enabled = true;
            }
            else
            {
                ux_buttonPostFinalResults.Visible = true;
                ux_buttonPostFinalResults.Enabled = false;
            }
        }

        private void ResetCreateViabilityTest()
        {
            ux_textboxCreateFromOrder.Text = "";
            ux_textboxCreateFromInventory.Text = "";
            ux_buttonCreateFromOrder.Enabled = true;
            ux_buttonCreateFromInventory.Enabled = true;
            ux_groupboxCreateViabilityTest.Enabled = true;
        }

        private void ResetGetViabilityTest()
        {
            ux_textboxInventoryViabilityID.Text = "";
            ux_textboxInventoryID.Text = "";
            ux_textboxPercentViable.Text = "";
            ux_textboxViabilityTestedDate.Text = "";
            ux_buttonGetInventoryViabilityTest.Enabled = true;
            ux_groupboxGetViabilityTest.Enabled = true;
        }

        private void ResetViabilityRule()
        {
            ux_comboboxViabilityRule.SelectedItem = null;
            ux_textboxNumberOfReplicates.Text = "";
            ux_textboxTotalSeeds.Text = "";
            ux_textboxSubstrata.Text = "";
            ux_textboxMoisture.Text = "";
            ux_textboxPrechill.Text = "";
            ux_textboxTemperature.Text = "";
            ux_textboxLighting.Text = "";
            ux_textboxRuleNotes.Text = "";
            ux_textboxTaxonomyNotes.Text = "";
            ux_textboxViabilityNotes.Text = "";
            ux_buttonSaveIVChanges.Visible = true;
            ux_buttonSaveIVChanges.Enabled = false;
            ux_buttonCancelIVChanges.Visible = true;
            ux_buttonCancelIVChanges.Enabled = false;
            ux_buttonCreateViabilityDataRecords.Visible = true;
            ux_buttonCreateViabilityDataRecords.Enabled = false;
            ux_buttonPrintLabels.Visible = true;
            ux_buttonPrintLabels.Enabled = false;
            ux_comboboxCrystalReports.Visible = true;
            ux_comboboxCrystalReports.Enabled = false;
            ux_groupboxViabilityRule.Enabled = true;
        }

        private void ResetViabilityDate()
        {
            RemoveViabilityDateControls();
            ux_groupboxViabilityDate.Enabled = false;
        }

        private void ResetViabilityData()
        {
            RemoveViabilityDataControls();
            ux_groupboxViabilityData.Enabled = false;
        }




        private void UpdateGetViabilityTest()
        {
            // Update the inventory specific controls...
            ux_textboxInventoryID.Text = "";
            ux_textboxPercentViable.Text = "";
            ux_textboxViabilityTestedDate.Text = "";
            if (_dsI != null &&
                _dsI.Tables.Contains("viability_wizard_get_inventory") &&
                _dsI.Tables["viability_wizard_get_inventory"].Rows.Count == 1)
            {
                if (_dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("inventory_id") &&
                    _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["inventory_id"] != DBNull.Value) ux_textboxInventoryID.Text = _sharedUtils.GetLookupDisplayMember("inventory_lookup", _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["inventory_id"].ToString(), "", "!Error!");
                if (_dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("percent_viable") &&
                    _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["percent_viable"] != DBNull.Value) ux_textboxPercentViable.Text = _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["percent_viable"].ToString() + "%";
                if (_dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("tested_date") &&
                    _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["tested_date"] != DBNull.Value) ux_textboxViabilityTestedDate.Text = ((DateTime)_dsI.Tables["viability_wizard_get_inventory"].Rows[0]["tested_date"]).ToString("d");
                if (_dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("viability_tested_date") &&
                    _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["viability_tested_date"] != DBNull.Value) ux_textboxViabilityTestedDate.Text = ((DateTime)_dsI.Tables["viability_wizard_get_inventory"].Rows[0]["viability_tested_date"]).ToString("d");
                if (_dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("taxonomy_species_id") &&
                    _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["taxonomy_species_id"] != DBNull.Value) ux_textboxTaxon.Text = _sharedUtils.GetLookupDisplayMember("taxonomy_species_lookup", _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["taxonomy_species_id"].ToString(), "", "!Error!");
                if (_dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("taxonomy_species_name") &&
                    _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["taxonomy_species_name"] != DBNull.Value) ux_textboxTaxon.Text = _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["taxonomy_species_name"].ToString();
            }
            if (_dsIV != null &&
                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1)
            {
                if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("note") &&
                    _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["note"] != DBNull.Value) ux_textboxViabilityNotes.Text = _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["note"].ToString().Trim();
            }
        }

        private void UpdateCreateViabilityTest()
        {
            //ux_textboxCreateFromOrder.Text = "";
            //ux_textboxCreateFromInventory.Text = "";
            //ux_buttonCreateFromOrder.Enabled = true;
            //ux_buttonCreateFromInventory.Enabled = true;
            if (IsViabilityDataLoaded(_viabilityIDTokens[0]))
            {
                ux_groupboxCreateViabilityTest.Enabled = false;
            }
            else
            {
                ux_groupboxCreateViabilityTest.Enabled = true;
            }
        }

        private void UpdateViabilityRule()
        {
            DataRow drIV = null;

            if (_dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("inventory_viability_rule_id") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("total_tested_count") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("replication_count"))
            {
                drIV = _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0];
            }

            // Check to see if the IV record's ivrID is the same as the ivrID choosen from the combobox...
            if (ux_comboboxViabilityRule.SelectedValue != null &&
                drIV != null &&
                (drIV["inventory_viability_rule_id"].ToString().Trim() != ux_comboboxViabilityRule.SelectedValue.ToString().Trim() ||
                drIV["replication_count"].ToString().Trim() != ux_textboxNumberOfReplicates.Text.Trim() ||
                drIV["total_tested_count"].ToString().Trim() != ux_textboxTotalSeeds.Text.Trim()))
            {
                // The IVR choosen in the combobox is different than the IVR in the IV record so display/enable the Save/Cancel buttons...
                ux_buttonSaveIVChanges.Visible = true;
                ux_buttonSaveIVChanges.Enabled = true;
                ux_buttonCancelIVChanges.Visible = true;
                ux_buttonCancelIVChanges.Enabled = true;
            }

            // Update the rules dropdown filter...
            if (_dsI.Tables.Contains("viability_wizard_get_inventory") &&
                _dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("taxonomy_species_name") &&
                _dsI.Tables["viability_wizard_get_inventory"].Rows.Count == 1 &&
                _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["taxonomy_species_id"] != DBNull.Value &&
                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("inventory_viability_rule_id") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1)
            {
                _currentViabilityRuleFilter = GetViabilityRuleFilter(_dsI.Tables["viability_wizard_get_inventory"].Rows[0]["taxonomy_species_id"].ToString(), _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"].ToString());
            }
            else
            {
                _currentViabilityRuleFilter = "";
            }

            // Store off the currently selected IVR...
            int currentIVR = -1;
            if (ux_comboboxViabilityRule.SelectedItem != null)
            {
                currentIVR = (int)ux_comboboxViabilityRule.SelectedValue;
            }

            // Apply the rule filter to the databound combobox now...
            ((DataTable)ux_comboboxViabilityRule.DataSource).DefaultView.RowFilter = _currentViabilityRuleFilter;
            if (ux_comboboxViabilityRule.SelectedItem != null &&
                ux_comboboxViabilityRule.SelectedValue.ToString() != currentIVR.ToString()) ux_comboboxViabilityRule.SelectedItem = null;

            ux_groupboxViabilityRule.Enabled = true;

            if (_dsIV.GetChanges() != null ||
                !IsViabilityDataSaved() ||
                !IsTaxonomyMappedToRule() ||
                string.IsNullOrEmpty(ux_textboxTotalSeeds.Text) ||
                string.IsNullOrEmpty(ux_textboxNumberOfReplicates.Text) ||
                ux_comboboxViabilityRule.SelectedValue == null)
            {
                // There are changes to the IV record so enable/show the Save/Cancel buttons...
                ux_buttonSaveIVChanges.Visible = true;
                ux_buttonSaveIVChanges.Enabled = true;
                ux_buttonCancelIVChanges.Visible = true;
                ux_buttonCancelIVChanges.Enabled = true;
                // Show but disable the create IVD records button...
                ux_buttonCreateViabilityDataRecords.Visible = true;
                ux_buttonCreateViabilityDataRecords.Enabled = false;
                // Show but disable the print labels button...
                ux_buttonPrintLabels.Visible = true;
                ux_buttonPrintLabels.Enabled = false;
                // Enable the textboxes and combobox for user edits...
                ux_textboxNumberOfReplicates.Enabled = true;
                ux_textboxTotalSeeds.Enabled = true;
                ux_comboboxViabilityRule.Enabled = true;
            }
            else
            {
                // There are NO changes to the IV record so disable/hide the Save/Cancel buttons...
                ux_buttonSaveIVChanges.Visible = true;
                ux_buttonSaveIVChanges.Enabled = false;
                ux_buttonCancelIVChanges.Visible = true;
                ux_buttonCancelIVChanges.Enabled = false;
                // Enable the textboxes and combobox for user edits...
                ux_textboxNumberOfReplicates.Enabled = true;
                ux_textboxTotalSeeds.Enabled = true;
                ux_comboboxViabilityRule.Enabled = true;
                if (_dsIVD != null &&
                    _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                    _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count > 0)
                {
                    // There are existing IVD records so Show/Disable the create IVD records button...
                    ux_buttonCreateViabilityDataRecords.Visible = true;
                    ux_buttonCreateViabilityDataRecords.Enabled = false;
                }
                else
                {
                    // There are NO existing IVD records so Show/Enable the create IVD records button...
                    if (!string.IsNullOrEmpty(ux_textboxNumberOfReplicates.Text) &&
                        !string.IsNullOrEmpty(ux_textboxTotalSeeds.Text))
                    {
                        ux_buttonCreateViabilityDataRecords.Visible = true;
                        ux_buttonCreateViabilityDataRecords.Enabled = true;
                    }
                    else
                    {
                        ux_buttonCreateViabilityDataRecords.Visible = true;
                        ux_buttonCreateViabilityDataRecords.Enabled = false;
                    }
                }
            }
            // Show/Enable the print labels button...
            if (!string.IsNullOrEmpty(ux_textboxNumberOfReplicates.Text) &&
                !string.IsNullOrEmpty(ux_textboxTotalSeeds.Text))
            {
                ux_buttonPrintLabels.Visible = true;
                ux_buttonPrintLabels.Enabled = true;
                ux_comboboxCrystalReports.Visible = true;
                ux_comboboxCrystalReports.Enabled = true;
            }
            else
            {
                ux_buttonPrintLabels.Visible = true;
                ux_buttonPrintLabels.Enabled = false;
                ux_comboboxCrystalReports.Visible = true;
                ux_comboboxCrystalReports.Enabled = false;
            }
        }

        private string GetViabilityRuleFilter(string taxonomySpeciesID, string additionalForcedIVRIDs)
        {
            string filter = "value_member in (NULL)";
            string inventoryViabilityRuleIDs = "";

            if (_dsIVRM != null &&
                _dsIVRM.Tables.Contains("viability_wizard_get_inventory_viability_rule_map") &&
                _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].Columns.Contains("inventory_viability_rule_id") &&
                _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].Rows.Count > 0)
            {
                string ivrmFilter = _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView.RowFilter;
                _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView.RowFilter = "taxonomy_species_id=" + taxonomySpeciesID;
                foreach (DataRowView drv in _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView)
                {
                    inventoryViabilityRuleIDs += drv["inventory_viability_rule_id"].ToString().Trim() + ",";
                }
                _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView.RowFilter = ivrmFilter;
            }

            if (!string.IsNullOrEmpty(additionalForcedIVRIDs))
            {
                // Break the list of additional IVRIDs up into a string array and process...
                string[] additionalIVRIDs = additionalForcedIVRIDs.Split(new char[] { ' ', ';', ',', '\t', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string additionIVRID in additionalIVRIDs)
                {
                    inventoryViabilityRuleIDs += additionIVRID.Trim() + ",";
                }
            }

            // If there are valid IVRIDs in the filter apply it to the returned filter string...
            if (!string.IsNullOrEmpty(inventoryViabilityRuleIDs))
            {
                filter = filter.Replace("(NULL)", "(" + inventoryViabilityRuleIDs.TrimEnd(new char[] { ',' }) + ")");
            }
            
            // Clear the table filter if the 'Show All Rules' checkbox is checked...
            if (ux_checkboxShowAllRules.Checked) filter = "";
            
            return filter;
        }

        private void UpdateViabilityDate()
        {
            string radioButtonText = "ux_radiobuttonCount1";
            DataTable dtIVD = null;
            if (_dsIVD != null &&
                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data"))
            {
                dtIVD = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"];
            }

            int MaxCountNumber = 0;
            if (dtIVD != null &&
                dtIVD.Rows.Count > 0)
            {
                // Calculate the maximum count_number in this viability test...
                MaxCountNumber = GetMaxCount();  // Convert.ToInt32(dtIVD.Compute("MAX(count_number)", null));

                // Calcuate the radiobutton text (date counted) and update all radio button text...
                for (int i = 1; i <= 8; i++)
                {
                    radioButtonText = "ux_radiobuttonCount" + i.ToString();
                    // Find the actual radio button control by name and update the text with the count date...
                    Control[] rb = Controls.Find(radioButtonText, true);
                    if (rb.Length == 1)
                    {
                        // Filter the table to view only tests associated with this count_number (so we can get the date it was counted)...
                        dtIVD.DefaultView.RowFilter = "count_number=" + i.ToString();
                        if (dtIVD.DefaultView.Count > 0)
                        {
                            // Found records using this count_number so use the count_date as the text in the radio button...
                            string dateFormatString = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
                            DateTime countDate;
                            if (DateTime.TryParse(dtIVD.DefaultView[0]["count_date"].ToString(), out countDate))
                            {
                                ((RadioButton)rb[0]).Text = countDate.ToString(dateFormatString);
                            }
                        }
                        else
                        {
                            // This count_number has not been used yet so leave it with the default text...
                            ((RadioButton)rb[0]).Text = "Count " + i.ToString();
                        }

                        // Show only radio buttons that have been used or are 'next in line' to be used...
                        if (i <= (MaxCountNumber + 1))
                        {
                            ((RadioButton)rb[0]).Show();
                        }
                        else
                        {
                            ((RadioButton)rb[0]).Hide();
                        }
                    }
                }
            }

            // Enable the groupbox if data is being edited right now...
            if (ux_groupboxViabilityData.Enabled)
            {
                ux_groupboxViabilityDate.Enabled = true;
            }
            else
            {
                ux_groupboxViabilityDate.Enabled = false;
            }
        }

        private void UpdateViabilityData()
        {
            if (_dsIVD != null &&
                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count > 0 &&
                ux_groupboxViabilityData.Contains(ux_buttonChangeDate))
            {
                ux_groupboxViabilityData.Enabled = true;
                ux_buttonChangeDate.Visible = true;
                ux_buttonChangeDate.Enabled = true;
            }

            if (_dsIVD != null &&
                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count > 0 &&
                _dsIVD.GetChanges() != null)
            {
                ux_buttonSaveIVDChanges.Visible = true;
                ux_buttonSaveIVDChanges.Enabled = true;
                ux_buttonCancelIVDChanges.Visible = true;
                ux_buttonCancelIVDChanges.Enabled = true;
            }
            else
            {
                ux_buttonSaveIVDChanges.Visible = true;
                ux_buttonSaveIVDChanges.Enabled = false;
                ux_buttonCancelIVDChanges.Visible = true;
                ux_buttonCancelIVDChanges.Enabled = false;
            }
        }

        private void UpdateViabilitySummary()
        {
            bool errorInSummaryData = false;
            DataTable dtIVD = null;
            if (_dsIVD != null &&
                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data"))
            {
                dtIVD = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Copy();
            }

            int MaxRepNumber = 0;
            if (dtIVD != null &&
                dtIVD.Rows.Count > 0)
            {
                // Calculate the maximum count_number in this viability test...
                MaxRepNumber = Convert.ToInt32(dtIVD.Compute("MAX(replication_number)", null));

                // Calculate the percent complete for each replication row...
                foreach (Control ctrl in ux_groupboxViabilityData.Controls)
                {
                    if (ctrl is TableLayoutPanel)
                    {
                        TableLayoutPanel tlpRow = (TableLayoutPanel)ctrl;
                        if (tlpRow.Tag != null)
                        {
                            if (dtIVD.Rows.Contains((int)tlpRow.Tag))
                            {
                                DataRow dr = dtIVD.Rows.Find((int)tlpRow.Tag);
                                if (dr != null &&
                                    dr.Table.Columns.Contains("replication_number") &&
                                    dr["replication_number"] != DBNull.Value &&
                                    dr.Table.Columns.Contains("replication_count") &&
                                    dr["replication_count"] != DBNull.Value)
                                {
                                    int repNum = (int)dr["replication_number"];
                                    int repCount = (int)dr["replication_count"];
                                    if (repCount < 1) repCount = 50;
                                    double totalRepCount = 0F;
                                    foreach (Control rowCTRL in tlpRow.Controls)
                                    {
                                        if (rowCTRL.Name.ToLower().EndsWith("_count"))
                                        {
                                            if (rowCTRL.Name.ToLower() != "replication_count" &&
                                                rowCTRL.Name.ToLower() != "treated_dormant_count")
                                            {
                                                string sum = dtIVD.Compute("SUM(" + rowCTRL.Name.ToLower().Trim() + ")", "replication_number=" + repNum.ToString()).ToString();
                                                if (!string.IsNullOrEmpty(sum))
                                                {
                                                    totalRepCount += Convert.ToDouble(sum);
                                                }
                                            }
                                        }
                                    }
                                    if (tlpRow.Controls.ContainsKey("percent_complete_placeholder"))
                                    {
                                        double repPercentComplete = ((double)(totalRepCount / (double)repCount));
                                        tlpRow.Controls["percent_complete_placeholder"].Text = repPercentComplete.ToString("##0.0%");
                                        if (repPercentComplete > 1.0)
                                        {
                                            errorInSummaryData = true;
                                            tlpRow.Controls["percent_complete_placeholder"].BackColor = Color.Red;
                                        }
                                        else
                                        {
                                            tlpRow.Controls["percent_complete_placeholder"].BackColor = Color.Empty;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Calculate the sums and percent complete for the whole test...
                foreach (Control ctrl in ux_groupboxViabilitySummary.Controls)
                {
                    if (ctrl is TableLayoutPanel)
                    {
                        TableLayoutPanel tlpRow = (TableLayoutPanel)ctrl;
                        if (tlpRow.Tag != null)
                        {
                            double totalCount = 0F;
                            double totalSeeds = 200F;
                            foreach (Control rowCTRL in tlpRow.Controls)
                            {
                                if (rowCTRL.Name.ToLower().EndsWith("_count"))
                                {
                                    if (rowCTRL.Name.ToLower() != "replication_count" &&
                                        rowCTRL.Name.ToLower() != "treated_dormant_count")
                                    {
                                        // Sum the counts across all count records...
                                        string sum = dtIVD.Compute("SUM(" + rowCTRL.Name.ToLower().Trim() + ")", null).ToString();
                                        if (!string.IsNullOrEmpty(sum))
                                        {
                                            totalCount += Convert.ToDouble(sum);
                                            rowCTRL.Text = sum;
                                        }
                                    }
                                    else if (rowCTRL.Name.ToLower() == "replication_count")
                                    {
                                        // Get the maximum rep count...
                                        int MaxCountNumber = GetMaxCount();
                                        // Sum the replication count numbers from the newest count records...
                                        string sum = dtIVD.Compute("SUM(" + rowCTRL.Name.ToLower().Trim() + ")", "count_number=" + MaxCountNumber.ToString()).ToString();
                                        if (!string.IsNullOrEmpty(sum))
                                        {
                                            totalSeeds = Convert.ToDouble(sum);
                                            if (totalSeeds < 1) totalSeeds = 200F;
                                            rowCTRL.Text = totalSeeds.ToString();
                                        }
                                    }
                                    else if (rowCTRL.Name.ToLower() == "treated_dormant_count")
                                    {
                                        // Sum the counts across all count records...
                                        double totalTreated = 0F;
                                        string sum = dtIVD.Compute("SUM(" + rowCTRL.Name.ToLower().Trim() + ")", null).ToString();
                                        if (!string.IsNullOrEmpty(sum))
                                        {
                                            totalTreated += Convert.ToDouble(sum);
                                            if (totalTreated > 0F)
                                            {
                                                ux_buttonCalcDormancyEstimatesNow.Visible = true;
                                                ux_buttonCalcDormancyEstimatesNow.Enabled = true;
                                            }
                                            else
                                            {
                                                ux_buttonCalcDormancyEstimatesNow.Visible = false;
                                                ux_buttonCalcDormancyEstimatesNow.Enabled = false;
                                            }
                                        }
                                        else
                                        {
                                            ux_buttonCalcDormancyEstimatesNow.Visible = false;
                                            ux_buttonCalcDormancyEstimatesNow.Enabled = false;
                                        }
                                    }
                                }
                            }
                            if (tlpRow.Controls.ContainsKey("percent_complete_placeholder"))
                            {
                                tlpRow.Controls["percent_complete_placeholder"].Text = ((double)(totalCount / totalSeeds)).ToString("##0.0%");
                            }
                        }
                    }
                }
            }

            // Notify the user if there are errors...
            if (errorInSummaryData)
            {
                ux_labelMessageDisplay.Visible = true;
                ux_labelMessageDisplay.ForeColor = Color.Red;
                ux_labelMessageDisplay.Text = "There are errors in the summary data for this viability test - please review.";
            }
            else
            {
                ux_labelMessageDisplay.Visible = true;
                ux_labelMessageDisplay.ForeColor = Color.Empty;
                ux_labelMessageDisplay.Text = "";
            }
        }

        private void RemoveViabilitySummaryControls()
        {
            // Clear all controls from the Summary groupbox...
            ux_groupboxViabilitySummary.Controls.Clear();
        }

        private void RemoveViabilityDataControls()
        {
            // Clear all controls from the Summary groupbox...
            ux_groupboxViabilityData.Controls.Clear();
        }

        private void RemoveViabilityDateControls()
        {
            // Hide all radio buttons in the Date groupbox...
            for (int i = 1; i <= 8; i++)
            {
                string radioButtonText = "ux_radiobuttonCount" + i.ToString();
                // Find the actual radio button control by name and hide it...
                Control[] rb = Controls.Find(radioButtonText, true);
                if (rb.Length == 1)
                {
                    ((RadioButton)rb[0]).Hide();
                }
            }
        }

        private void BuildViabilityDataPage(int countNumber)
        {
            DataTable dtIVD = null;
            if (_dsIVD != null &&
                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count > 0)
            {
                dtIVD = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"];
                // Filter the table to view only tests associated with countNumber parameter...
                dtIVD.DefaultView.RowFilter = "count_number=" + countNumber.ToString();
            }

            // Clear all of the controls from the form...
            RemoveViabilityDataControls();
            RemoveViabilitySummaryControls();

            // Build the TabelLayoutPanels for each row and place them in the Viability Data groupbox...
            // First make the row header...
            if (dtIVD != null &&
                dtIVD.DefaultView.Count > 0)
            {
                ux_groupboxViabilityData.Controls.Add(ux_buttonChangeDate);
                ux_groupboxViabilityData.Controls.Add(ux_datetimepickerChangeViabilityDate);
                ux_groupboxViabilityData.Controls.Add(ux_buttonCalcDormancyEstimatesNow);
                int RepCount = 0;
                TableLayoutPanel ivdRowHeader = CreateTLPRow(dtIVD.Rows[0]);
                ivdRowHeader.Left = 10;
                ivdRowHeader.Top = 50 + (RepCount++ * (ivdRowHeader.Height + 5));
                ivdRowHeader.Width = ux_groupboxViabilityData.Width - 15;
                ivdRowHeader.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
                ux_groupboxViabilityData.Controls.Add(ivdRowHeader);
                ivdRowHeader.Tag = null;
                ivdRowHeader.TabStop = false;
                // Make the row header read-only...
                foreach (Control ctrl in ivdRowHeader.Controls)
                {
                    if (ctrl is TextBox)
                    {
                        ((TextBox)ctrl).ReadOnly = true;
                        ctrl.TabStop = false;
                        if (ctrl.Name != "percent_complete_placeholder" && ctrl.Name != "empty_cell_placeholder")
                        {
                            ctrl.Text = dtIVD.Columns[ctrl.Name].ExtendedProperties["title"].ToString();
                        }
                    }
                }
                // Make the rest of the rows normally for actual counts (first count is the planting - so no counting is done)...
                //if (countNumber > 1) //cv bug: dont add TLP Rows when first date is seleted
                //{
                    foreach (DataRowView drvIVD in dtIVD.DefaultView)
                    {
                        TableLayoutPanel ivdRow = CreateTLPRow(drvIVD.Row);
                        ivdRow.Left = 10;
                        ivdRow.Top = 50 + (RepCount++ * (ivdRow.Height + 5));
                        ivdRow.Width = ux_groupboxViabilityData.Width - 15;
                        ivdRow.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
                        ux_groupboxViabilityData.Controls.Add(ivdRow);
                    }
                //}

                // Build the TabelLayoutPanels summary row and place it in the Viability Summary groupbox...
                RepCount = 0;
                TableLayoutPanel ivdSummaryRow = CreateTLPRow(dtIVD.Rows[0]);
                ivdSummaryRow.Left = 10;
                ivdSummaryRow.Top = 15 + (RepCount++ * (ivdSummaryRow.Height + 5));
                ivdSummaryRow.Width = ux_groupboxViabilitySummary.Width - 15;
                ivdSummaryRow.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
                ux_groupboxViabilitySummary.Controls.Add(ivdSummaryRow);
                foreach (Control ctrl in ivdSummaryRow.Controls)
                {
                    if (ctrl is TextBox)
                    {
                        ((TextBox)ctrl).ReadOnly = true;
                        ctrl.TabStop = false;
                        ctrl.Text = "";
                    }
                }
            }
        }

        private TableLayoutPanel CreateTLPRow(DataRow drIVD)
        {
            // Create a inventory_viability_data TableLayoutPanel row...
            TableLayoutPanel ivdRowTemplate = new TableLayoutPanel();
            // Tag it with the PKEY for the inventory_viability_data row it represents...
            ivdRowTemplate.Tag = drIVD["inventory_viability_data_id"];
            // Get the user setting for which fields to show...
            string userFieldsSettings = _sharedUtils.GetAppSettingValue("ViabilityWizardVisibleCountFields");
            if (string.IsNullOrEmpty(userFieldsSettings)) userFieldsSettings = "replication_number;replication_count;percent_complete_placeholder;normal_count;abnormal_count;dormant_count;hard_count;empty_count;infested_count;dead_count;unknown_count;estimated_dormant_count;treated_dormant_count;confirmed_dormant_count;note";
//Stack<string> shownFields = new Stack<string>(userFieldsSettings.Split(';').ToList().Reverse<string>());
            Stack<string> shownFields = new Stack<string>();
            foreach (string fieldName in userFieldsSettings.Split(';'))
            {
                if (drIVD.Table.Columns.Contains(fieldName.Trim().ToLower()) ||
                    fieldName == "empty_cell_placeholder" ||
                    fieldName == "percent_complete_placeholder")
                {
                    shownFields.Push(fieldName.Trim().ToLower());
                }
            }
            // Reverse the stack so it pops the fields in the correct sorted order...
            shownFields = new Stack<string>(shownFields);
            // Build the cells and put textboxes in them...
            int cellHeight = 20;
            int cellWidth = 50;
            ivdRowTemplate.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            ivdRowTemplate.Margin = new Padding(0);
            ivdRowTemplate.RowCount = 1;
            ivdRowTemplate.ColumnCount = shownFields.Count;
            ivdRowTemplate.Top = 0;
            ivdRowTemplate.Left = 0;
            ivdRowTemplate.Height = cellHeight * ivdRowTemplate.RowCount + (ivdRowTemplate.RowCount + 1);
            ivdRowTemplate.Width = cellWidth * ivdRowTemplate.ColumnCount + (ivdRowTemplate.ColumnCount + 1);
            ivdRowTemplate.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            for (int col = 0; col < ivdRowTemplate.ColumnCount; col++)
            {
                string fieldName = shownFields.Pop();
                if (fieldName != "empty_cell_placeholder")
                {
                    TextBox tb = new TextBox();
                    tb.Name = fieldName;
                    tb.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
                    tb.TextAlign = HorizontalAlignment.Center;
                    tb.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                    tb.Multiline = false;
                    tb.Top = 0;
                    tb.Left = 0;
                    tb.Height = cellHeight;
                    tb.Width = cellWidth;
                    if (col == ivdRowTemplate.ColumnCount - 1)
                    {
                        ivdRowTemplate.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                    }
                    else
                    {
                        ivdRowTemplate.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, (float)(cellWidth)));
                    }
                    if (fieldName != "percent_complete_placeholder")
                    {
                        tb.Text = drIVD[fieldName].ToString();
                        if (fieldName == "replication_number" ||
                            fieldName == "replication_count")
                        {
                            tb.TabStop = false;
                        }
                    }
                    else
                    {
                        tb.ReadOnly = true;
                        tb.TabStop = false;
                    }
                    tb.TextChanged += ux_textboxCounts_TextChanged;
                    tb.GotFocus += ux_textboxCounts_GotFocus;
                    ivdRowTemplate.Controls.Add(tb, col, 0);
                }
            }
            return ivdRowTemplate;
        }

        void ux_textboxCounts_GotFocus(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
//tb.SelectionStart = tb.Text.Length;
tb.SelectAll();
        }

        void ux_textboxCounts_TextChanged(object sender, EventArgs e)
        {
            int count = 0;
            TextBox tb = (TextBox)sender;

            // This method will perform shortcut navigation of the form
            // based on special characters passed into the textbox (NOTE: before
            // returning the special characters are removed)...
            ProcessSpecialKeystrokes(tb);

            if (!tb.ReadOnly)
            {
                if (int.TryParse(tb.Text, out count))
                {
                    // The input value is an interger so attempt to update the IVD record...
                    if ((tb.Parent != null && tb.Parent.Tag != null) &&
                        _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data"))
                    {
                        DataTable dt = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"];
                        DataRow[] dr = dt.Select("inventory_viability_data_id=" + tb.Parent.Tag.ToString());
                        if (dr.Length == 1 &&
                            dr[0].Table.Columns.Contains(tb.Name))
                        {
                            // This is a valid integer count for this replicate - so update the IVD record...
                            if (dr[0][tb.Name].ToString() != count.ToString())
                            {
                                dr[0][tb.Name] = count;
                                // Re-calculate the statistics for the row and the test summary...
                                UpdateViabilitySummary();
                            }
                            // If the user is changing the total number of seeds tested in this replication ask the
                            // user if they want all the count records for this replication updated as well...
                            if (tb.Name.Trim().ToLower() == "replication_count")
                            {
                                // First get the number of counts performed on this replicate...
                                DataRow[] replicate_records = dt.Select("replication_number=" + dr[0]["replication_number"].ToString());
                                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There are currently {0} counts for this replicate.\n\nWould you like to update the 'Total Seeds Tested' for ALL of the records associated with this replicate?", "Update All Replicate Count Records", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                                ggMessageBox.Name = "ViabilityWizard_ux_textboxCounts_TextChangedMessage1";
                                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                                string[] argsArray = new string[100];
                                argsArray[0] = replicate_records.Length.ToString();
                                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
//                              if (replicate_records.Length > 1 && DialogResult.Yes == ggMessageBox.ShowDialog())
                                if (replicate_records.Length > 1)
                                {
                                    foreach (DataRow rep_dr in replicate_records)
                                    {
                                        rep_dr[tb.Name] = count;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if ((tb.Parent != null && tb.Parent.Tag != null) &&
                      _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data"))
                    {
                        DataTable dt = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"];
                        DataRow[] dr = dt.Select("inventory_viability_data_id=" + tb.Parent.Tag.ToString());
                        if (dr.Length == 1 &&
                            dr[0].Table.Columns.Contains(tb.Name))
                        {
                            if (string.IsNullOrEmpty(tb.Text))
                            {
                                // Nothing in the textbox so NULL out that IVD field...
                                if (dr[0][tb.Name] != DBNull.Value)
                                {
                                    dr[0][tb.Name] = DBNull.Value;
                                    // Re-calculate the statistics for the row and the test summary...
                                    UpdateViabilitySummary();
                                }
                            }
                            else
                            {
                                // This is a non-integer string - so update the IVD record...
                                if (dr[0].Table.Columns[tb.Name].DataType == typeof(string) &&
                                    dr[0][tb.Name].ToString() != tb.Text)
                                {
                                    dr[0][tb.Name] = tb.Text;
                                    // Re-calculate the statistics for the row and the test summary...
                                    UpdateViabilitySummary();
                                }
                            }
                        }
                    }
                }
            }

            // Check to see if there are unsaved edits to the count data and if so update the controls
            // to reflect that...
            if (_dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].GetChanges() != null)
            {
                ux_groupboxGetViabilityTest.Enabled = false;
//ux_buttonMakeNewInventoryViabilityData.Enabled = false;
ux_groupboxCreateViabilityTest.Enabled = false;
                ux_buttonSaveIVDChanges.Visible = true;
                ux_buttonSaveIVDChanges.Enabled = true;
                ux_buttonCancelIVDChanges.Visible = true;
                ux_buttonCancelIVDChanges.Enabled = true;
                ux_buttonPostFinalResults.Visible = true;
                ux_buttonPostFinalResults.Enabled = false;
            }
        }

        private void ProcessSpecialKeystrokes(TextBox tb)
        {
            if (tb.Text.Contains("VVVVVVVVVVVVV"))
            {
                // The input value is not an integer so check to see if it is a special character used
                // for quickly navigating around the form...
                // Clear the 'special characters' text from this textbox now (before saving data)...
                tb.Text = tb.Text.Replace("VVVVVVVVVVVVV", "");
                // User wants to save the data NOW (this is supposed to be a barcode version of 'Alt V')....
                SaveViabilityData();
                // Once the data has been save the controls need to be rebuilt to reflect any
                // new rows that were saved and now have a valid PKEY...
                BuildViabilityDataPage(_currentCountNumber);
                // Update the controls on the form...
                ux_groupboxGetViabilityTest.Enabled = true;
//ux_buttonMakeNewInventoryViabilityData.Enabled = true;
ux_groupboxCreateViabilityTest.Enabled = true;
                ux_textboxInventoryViabilityID.Focus();
                ux_textboxInventoryViabilityID.SelectAll();
            }
            else if (tb.Text.Contains("EEEEEEEEEEEEE"))
            {
                // Clear the 'special characters' text from this textbox now (before moving to the next tab stop)...
                tb.Text = tb.Text.Replace("EEEEEEEEEEEEE", "");
                tb.Parent.Parent.SelectNextControl(tb, true, true, true, true);
            }
        }

        private bool IsViabilityTestingComplete(bool checkForErrors)
        {
            bool testingComplete = false;

            double totalCounted = 0.0;
            double totalSeedsTested = 0.0;
            int MaxCountNumber = 0;

            // Calculate the maximum count_number in this viability test...  
            if (_dsIVD != null &&
                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count > 0)
            {
                if (_dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Columns.Contains("count_number"))
                {
                    MaxCountNumber = GetMaxCount();  // Convert.ToInt32(_dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Compute("MAX(count_number)", null));
                }

                foreach (DataColumn dc in _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Columns)
                {
                    if (dc.ColumnName.EndsWith("_count"))
                    {
                        if (dc.ColumnName.ToLower() != "replication_count" &&
                            dc.ColumnName.ToLower() != "treated_dormant_count")
                        {
                            string sum = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Compute("SUM(" + dc.ColumnName.ToLower().Trim() + ")", null).ToString();
                            if (!string.IsNullOrEmpty(sum))
                            {
                                totalCounted += Convert.ToDouble(sum);
                            }
                        }
                        else if (dc.ColumnName.ToLower() == "replication_count")
                        {
                            string sum = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Compute("SUM(" + dc.ColumnName.ToLower().Trim() + ")", "count_number=" + MaxCountNumber).ToString();
                            if (!string.IsNullOrEmpty(sum))
                            {
                                totalSeedsTested = Convert.ToSingle(sum);
                                if (totalSeedsTested < 1) totalSeedsTested = 200;
                            }
                        }
                    }
                }

                if (checkForErrors &&
                    totalCounted == totalSeedsTested)
                {
                    testingComplete = true;
                }
                else if (!checkForErrors &&
                    totalCounted >= totalSeedsTested)
                {
                    testingComplete = true;
                }
            }
            return testingComplete;
        }

        private void ux_buttonSaveIVDChanges_Click(object sender, EventArgs e)
        {
            // If there are unsaved changes in the parent IV table - save them now...
            if (ux_buttonSaveIVChanges.Enabled) ux_buttonSaveIVChanges.PerformClick();
            // Now save the rest of the IV test data...
            int errorCount = SaveViabilityData();
            if (errorCount == 0)
            {
                // Reset the controls to default state...
                ux_groupboxGetViabilityTest.Enabled = true;
                ux_groupboxCreateViabilityTest.Enabled = true;
                ux_buttonSaveIVDChanges.Visible = true;
                ux_buttonSaveIVDChanges.Enabled = false;
                ux_buttonCancelIVDChanges.Visible = true;
                ux_buttonCancelIVDChanges.Enabled = false;
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Viability Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ViabilityWizard_ux_buttonSave1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();

                // Enable the Post Final Results button if the counts are complete...
                if (IsViabilityTestingComplete(true))
                {
                    ux_buttonPostFinalResults.Visible = true;
                    ux_buttonPostFinalResults.Enabled = true;
                    //BuildViabilityDataPage(_currentCountNumber);
                }
                else
                {
                    ux_buttonPostFinalResults.Visible = true;
                    ux_buttonPostFinalResults.Enabled = false;
                    // Clear all of the controls from the form...
                    RemoveViabilityDateControls();
                    RemoveViabilityDataControls();
                    RemoveViabilitySummaryControls();
                    ux_textboxInventoryViabilityID.Focus();
                    ux_textboxInventoryViabilityID.SelectAll();
                }
            }
            else
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed.\n\n  Error Count: {0}", "Viability Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ViabilityWizard_ux_buttonSave2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = errorCount.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }
        }

        private void ux_buttonCancelIVDChanges_Click(object sender, EventArgs e)
        {
            // The user might have accidently hit the cancel button on the form with unsaved edits - if so ask the
            // user if they would like to save their data...
            int intRowEdits = 0;
            if (_dsI.Tables.Contains("viability_wizard_get_inventory") && _dsI.Tables["viability_wizard_get_inventory"].GetChanges() != null) intRowEdits = _dsI.Tables["viability_wizard_get_inventory"].GetChanges().Rows.Count;
            if (_dsIV.Tables.Contains("viability_wizard_get_inventory_viability") && _dsIV.Tables["viability_wizard_get_inventory_viability"].GetChanges() != null) intRowEdits += _dsIV.Tables["viability_wizard_get_inventory_viability"].GetChanges().Rows.Count;
            if (_dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") && _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].GetChanges() != null) intRowEdits += _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].GetChanges().Rows.Count;
            if (_dsIVR.Tables.Contains("viability_wizard_get_inventory_viability_rule") && _dsIVR.Tables["viability_wizard_get_inventory_viability_rule"].GetChanges() != null) intRowEdits += _dsIVR.Tables["viability_wizard_get_inventory_viability_rule"].GetChanges().Rows.Count;
            if (_dsIVRM.Tables.Contains("viability_wizard_get_inventory_viability_rule_map") && _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].GetChanges() != null) intRowEdits += _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].GetChanges().Rows.Count;

            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} unsaved row change(s), are you sure you want to cancel your edits and close this window?", "Cancel Edits and Close", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "ViabilityWizard_FormClosingMessage1";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = intRowEdits.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (intRowEdits > 0 && DialogResult.Yes == ggMessageBox.ShowDialog())
            {
                // The user does not want to save data so reset the Wizard....
                _dsI = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory", ":inventoryviabilityid=-1", 0, 0);
                _dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryviabilityid=-1", 0, 0);
                _dsIVD = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability_data", ":inventoryviabilityid=-1", 0, 0);
                ResetGetViabilityTest();
                ResetCreateViabilityTest();
                ResetViabilityRule();
                ResetViabilityDate();
                ResetViabilityData();
                UpdateGetViabilityTest();
                UpdateCreateViabilityTest();
                UpdateViabilityRule();
                UpdateViabilityData();
                UpdateViabilityDate();
                UpdateViabilitySummary();
                // Clear all of the controls from the form...
                RemoveViabilityDateControls();
                RemoveViabilityDataControls();
                RemoveViabilitySummaryControls();
                ux_textboxInventoryViabilityID.Focus();
                ux_textboxInventoryViabilityID.SelectAll();
            }
        }

        private int SaveViabilityData()
        {
            int errorCount = 0;
            DataSet inventoryViabilityChanges = new DataSet();
            DataSet inventoryViabilitySaveResults = new DataSet();
            DataSet inventoryViabilityRuleChanges = new DataSet();
            DataSet inventoryViabilityRuleSaveResults = new DataSet();
            DataSet inventoryViabilityDataChanges = new DataSet();
            DataSet inventoryViabilityDataSaveResults = new DataSet();

            // Get the changes (if any) for the inventory_viability table and commit them to the remote database...
            if (_dsIV != null && _dsIV.GetChanges() != null)
            {
                DataTable dtIV = _dsIV.Tables["viability_wizard_get_inventory_viability"];
                inventoryViabilityChanges.Tables.Add(dtIV.GetChanges());
                ScrubData(inventoryViabilityChanges);
                // Save the changes to the remote server...
                inventoryViabilitySaveResults = _sharedUtils.SaveWebServiceData(inventoryViabilityChanges);
                if (inventoryViabilitySaveResults.Tables.Contains(dtIV.TableName))
                {
                    errorCount += SyncSavedResults(dtIV, inventoryViabilitySaveResults.Tables[dtIV.TableName]);
                }
            }

            // Get the changes (if any) for the inventory_viability_rule table and commit them to the remote database...
            if (_dsIVR != null && _dsIVR.GetChanges() != null)
            {
                DataTable dtIVR = _dsIVR.Tables["viability_wizard_get_inventory_viability_rule"];
                inventoryViabilityRuleChanges.Tables.Add(dtIVR.GetChanges());
                ScrubData(inventoryViabilityRuleChanges);
                // Save the changes to the remote server...
                inventoryViabilityRuleSaveResults = _sharedUtils.SaveWebServiceData(inventoryViabilityRuleChanges);
                if (inventoryViabilityRuleSaveResults.Tables.Contains(dtIVR.TableName))
                {
                    errorCount += SyncSavedResults(dtIVR, inventoryViabilityRuleSaveResults.Tables[dtIVR.TableName]);
                }
            }

            // Get the changes (if any) for the inventory_viability_data table and commit them to the remote database...
            
            if (_dsIVD != null && _dsIVD.GetChanges() != null)
            {
                bool bonafideChangesExist = false;
                int intNewRows = 0;
                // Before sending datarows to the server for saving - make sure that each datarow that is marked as modified
                // has at least one field that has been modified from it's original value.  We have to do this to handle the 
                // case where the user has changed a value in a field but then changed it back to the original value - when this
                // happens the RowState status is not changed back to DataRowState.Unchanged by the .NET framework 
                // so we will do it on behalf of the .NET framework...
                foreach (DataRow dr in _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows)
                {
                    if (dr.RowState == DataRowState.Modified)
                    {
                        int changedFields = 0;
                        foreach(DataColumn dc in dr.Table.Columns)
                        {
                            if (dr[dc, DataRowVersion.Original].ToString().Trim() != dr[dc, DataRowVersion.Current].ToString().Trim()) changedFields++;
                        }
                        // If there are 0 fields that have an original value different than the current value then 
                        // reset the row status to unchanged
                        if (changedFields == 0) dr.RejectChanges();
                    }
                }

                DataTable dtIVD = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"];
                inventoryViabilityDataChanges.Tables.Add(dtIVD.GetChanges());
                foreach (DataRow dr in inventoryViabilityDataChanges.Tables["viability_wizard_get_inventory_viability_data"].Rows)
                {
                    if (dr.RowState == DataRowState.Modified)
                    {
                        bonafideChangesExist = true;
                    }
                    else if(dr.RowState == DataRowState.Added)
                    {
                        string columnNamesToCheck = "normal_count; abnormal_count; dormant_count; empty_count; infested_count; dead_count; unknown_count; note";
                        foreach (string columnName in columnNamesToCheck.Split(';'))
                        {
                            if (dr.Table.Columns.Contains(columnName.Trim()))
                            {
                                if(columnName.Trim() == "normal_count")
                                {
                                    if (dr[columnName.Trim()].ToString().Trim() != "0")
                                    {
                                        bonafideChangesExist = true;
                                        break;
                                    }
                                }
                                else if (!string.IsNullOrEmpty(dr[columnName.Trim()].ToString().Trim()))
                                {
                                    bonafideChangesExist = true;
                                    break;
                                }
                            }
                        }
                        // Keep count of the new rows added for warning message below...
                        intNewRows++;
                    }
                }

                // If this is the planting date - there will be no counts so force this to a changes exist event...
                if (GetMaxCount() == 1) bonafideChangesExist = true;

                if (!bonafideChangesExist &&
                    intNewRows > 0)
                {
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} new replication rows - but no count data has been entered.  Would you like to save these new count rows?", "Cancel Edits and Close", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "ViabilityWizard_SaveViabilityData1";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    string[] argsArray = new string[100];
                    argsArray[0] = intNewRows.ToString();
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    if (intNewRows > 0 && DialogResult.Yes == ggMessageBox.ShowDialog())
                    {
                        // The user indicates this is bona fide data - so who are we to argue...
                        bonafideChangesExist = true;
                    }
                }
                if (bonafideChangesExist)
                {
                    ScrubData(inventoryViabilityDataChanges);
                    // Save the changes to the remote server...
                    inventoryViabilityDataSaveResults = _sharedUtils.SaveWebServiceData(inventoryViabilityDataChanges);
                    if (inventoryViabilityDataSaveResults.Tables.Contains(dtIVD.TableName))
                    {
                        errorCount += SyncSavedResults(dtIVD, inventoryViabilityDataSaveResults.Tables[dtIVD.TableName]);
                    }
                }
            }

            return errorCount;
        }

        private void ScrubData(DataSet ds)
        {
            // Make sure all non-nullable fields do not contain a null value - if they do, replace it with the default value...
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr.RowState != DataRowState.Deleted)
                    {
                        foreach (DataColumn dc in dt.Columns)
                        {
                            if (dc.ExtendedProperties.Contains("is_nullable") &&
                                dc.ExtendedProperties["is_nullable"].ToString().Trim().ToUpper() == "N" &&
                                dr[dc] == DBNull.Value)
                            {
                                if (dc.ExtendedProperties.Contains("default_value") &&
                                    !string.IsNullOrEmpty(dc.ExtendedProperties["default_value"].ToString()) &&
                                    dc.ExtendedProperties["default_value"].ToString().Trim().ToUpper() != "{DBNULL.VALUE}")
                                {
                                    dr[dc] = dc.ExtendedProperties["default_value"].ToString();
                                }
                            }
                        }
                    }
                }
            }
        }

        private int SyncSavedResults(DataTable originalTable, DataTable savedResults)
        {
            int errorCount = 0;

            if (savedResults != null && savedResults.PrimaryKey.Length == 1)
            {
                string pKeyCol = savedResults.PrimaryKey[0].ColumnName.Trim().ToUpper();
                savedResults.Columns[pKeyCol].ReadOnly = false;
                foreach (DataRow dr in savedResults.Rows)
                {
                    DataRow originalRow = originalTable.Rows.Find(dr["OriginalPrimaryKeyID"]);

                    switch (dr["SavedAction"].ToString())
                    {
                        case "Insert":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the originalTable row's status for this new row to committed (and update the pkey with the int returned from the server DB)...
                                if (originalRow != null)
                                {
                                    // Get the new row from the database (because it will have created_by and owned_by data - which is needed for future updates)...
                                    DataSet ds = _sharedUtils.GetWebServiceData(originalTable.TableName, ":" + pKeyCol.Replace("_", "").ToLower() + "=" + dr["NewPrimaryKeyID"].ToString(), 0, 0);
                                    if (ds.Tables.Contains(originalTable.TableName) &&
                                        ds.Tables[originalTable.TableName].Rows.Count > 0)
                                    {
                                        foreach (DataColumn dc in ds.Tables[originalTable.TableName].Columns)
                                        {
                                            if (dc.ReadOnly)
                                            {
                                                originalRow.Table.Columns[dc.ColumnName].ReadOnly = false;
                                                originalRow[dc.ColumnName] = ds.Tables[originalTable.TableName].Rows[0][dc.ColumnName];
                                                originalRow.Table.Columns[dc.ColumnName].ReadOnly = true;
                                            }
                                            else
                                            {
                                                originalRow[dc.ColumnName] = ds.Tables[originalTable.TableName].Rows[0][dc.ColumnName];
                                            }
                                        }
                                        originalRow.AcceptChanges();
                                        originalRow.ClearErrors();
                                    }
                                    else
                                    {
                                        bool origColumnReadOnlyValue = originalRow.Table.Columns[pKeyCol].ReadOnly;
                                        originalRow.Table.Columns[pKeyCol].ReadOnly = false;
                                        originalRow[pKeyCol] = dr["NewPrimaryKeyID"];
                                        originalRow.AcceptChanges();
                                        originalRow.Table.Columns[pKeyCol].ReadOnly = origColumnReadOnlyValue;
                                        originalRow.ClearErrors();
                                    }
                                }
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Update":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                originalRow.AcceptChanges();
                                originalRow.ClearErrors();
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Delete":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the row's status for this deleted row to committed...
                                if (originalRow != null)
                                {
                                    originalRow.AcceptChanges();
                                    originalRow.ClearErrors();
                                }
                                else
                                {
                                    //DataTable dt = originalTable.GetChanges(DataRowState.Deleted);
                                    //DataRow deletedRow = null;
                                    //foreach (DataRow deldr in dt.Rows)
                                    //{
                                    //    if (deldr[0, DataRowVersion.Original].ToString() == dr["OriginalPrimaryKeyID"].ToString())
                                    //    {
                                    //        deletedRow = deldr;
                                    //    }
                                    //}
                                    DataRow deletedRow = null;
                                    foreach (DataRow deleteddr in originalTable.Rows)
                                    {
                                        if (deleteddr.RowState == DataRowState.Deleted && deleteddr[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                        {
                                            deletedRow = deleteddr;
                                        }
                                    }
                                    deletedRow.AcceptChanges();
                                    deletedRow.ClearErrors();
                                }
                            }
                            else
                            {
                                errorCount++;
                                // Find the deleted row (NOTE: datatable.rows.find() method does not work on deleted rows)...
                                foreach (DataRow deletedRow in originalTable.Rows)
                                {
                                    if (deletedRow[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                    {
                                        deletedRow.RejectChanges();
                                        deletedRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            return errorCount;
        }

        private void ux_radiobuttonCount_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            if (rb.Checked)
            {
                // Remember which count is currently being viewed/edited...
                _currentCountNumber = Convert.ToInt32(rb.Tag);

                // Build the inventory_viability_data controls to show data for the newly selected
                // count_number (NOTE: this may actually create new inventory_viability_data if
                // the count_number has not been used before...
                if (!IsCountDataAvailable(_currentCountNumber))
                {
                    CreateNewViabilityDataRecords(_currentCountNumber);
                }
                BuildViabilityDataPage(_currentCountNumber);

                // Set the radio buttons and count data controls to match the vaules for this viability test...
                if (_dsIVD != null &&
                    _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                    _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count == 0)
                {
                    // The method call above should have created rows for this table
                    // so since they are no rows either rep count or seeds/rep data could not be found...
                    rb.Checked = false;
                }
                else
                {
                    // Update the count_number radio buttons visibility and text to reflect
                    // this new inventory_viability_data just displayed...
                    UpdateViabilityData();
                    UpdateViabilityDate();
                    // Re-calculate the statistics for the row and the test summary...
                    UpdateViabilitySummary();
                    // If the user input included the Rep number in the textbox string set focus to that record...
                    if (_viabilityIDTokens.Length == 2)
                    {
                        SetTLPControlFocus();
                    }
                }
            }
        }

        private void ux_buttonChangeDate_Click(object sender, EventArgs e)
        {

            if (ux_datetimepickerChangeViabilityDate.Visible == false)
            {
                ux_datetimepickerChangeViabilityDate.Visible = true;
                ux_buttonChangeDate.Text = "Done";
            }
            else
            {
                ux_datetimepickerChangeViabilityDate.Visible = false;
                ux_buttonChangeDate.Text = "Change Date";

                DataTable dtIVD = null;
                if (_dsIVD != null &&
                    _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data"))
                {
                    dtIVD = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"];
                }

                // Filter the table to view only tests associated with this count_number (so we can get the date it was counted)...
                dtIVD.DefaultView.RowFilter = "count_number=" + _currentCountNumber.ToString();
                if (dtIVD.DefaultView.Count > 0)
                {
                    // Found records using this count_number so use the count_date as the text in the radio button...
                    string dateFormatString = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
                    DateTime countDate;
                    if (DateTime.TryParse(ux_datetimepickerChangeViabilityDate.Value.ToString(dateFormatString), out countDate))
                    {
                        foreach (DataRowView drv in dtIVD.DefaultView)
                        {
                            drv["count_date"] = ux_datetimepickerChangeViabilityDate.Value.ToString(dateFormatString);
                        }
                        Control[] currentRadioButton = ux_groupboxViabilityDate.Controls.Find("ux_radiobuttonCount" + _currentCountNumber.ToString(), true); //.Text = countDate.ToString(dateFormatString);
                        if (currentRadioButton.Length == 1)
                        {
                            currentRadioButton[0].Text = ux_datetimepickerChangeViabilityDate.Value.ToString(dateFormatString);
                        }
                    }
                }
            }
        }

        private void ux_buttonCalcDormancyEstimatesNow_Click(object sender, EventArgs e)
        {
            if(_dsIVD != null &&
                _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count > 0)
            {
                double totalTreatedSum = 0F;
                double confirmedDormantSum = 0F;
                string totalTreated = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Compute("SUM(treated_dormant_count)", null).ToString();
                string confirmedDormant = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Compute("SUM(confirmed_dormant_count)", null).ToString();
                if (!string.IsNullOrEmpty(totalTreated) && !string.IsNullOrEmpty(confirmedDormant) &&
                    double.TryParse(totalTreated, out totalTreatedSum) &&
                    double.TryParse(confirmedDormant, out confirmedDormantSum))
                {
                    double percentDormant = confirmedDormantSum / totalTreatedSum;
                    int maxCount = GetMaxCount();
                    //                 normal_count, abnormal_count, dormant_count, hard_count, empty_count, infested_count, dead_count, unknown_count, estimated_dormant_count, treated_dormant_count, confirmed_dormant_count, replication_count
                    string colNames = "normal_count, abnormal_count, dormant_count, hard_count, empty_count, infested_count, dead_count, unknown_count, confirmed_dormant_count";
                    string[] colNameArr = colNames.Split(',');
                    // Iterate thru all records to find the most recent count date for each replicate...
                    foreach (DataRow dr in _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows)
                    {
                        // Found the most recent (max) count date for one of the replicates...
                        if (dr["count_number"].ToString() == maxCount.ToString())
                        {
                            int expectedRepTotalCount = (int)dr["replication_count"];
                            int currentRepTotalCount = 0;
                            // Do a sanity check and scrub for missing dead and/or confirmed count data entry...
                            int treatedDormCount = 0;
                            int confirmedDormCount = 0;
                            int deadCount = 0;
                            if (!string.IsNullOrEmpty(dr["treated_dormant_count"].ToString()) &&
                                !int.TryParse(dr["treated_dormant_count"].ToString(), out treatedDormCount)) treatedDormCount = 0;
                            if (!string.IsNullOrEmpty(dr["confirmed_dormant_count"].ToString()) &&
                                !int.TryParse(dr["confirmed_dormant_count"].ToString(), out confirmedDormCount)) confirmedDormCount = 0;
                            if (!string.IsNullOrEmpty(dr["dead_count"].ToString()) &&
                                !int.TryParse(dr["dead_count"].ToString(), out deadCount)) deadCount = 0;

                            // Check to see if dormancy treatments were performed on this replicate...
                            if(treatedDormCount > 0)
                            {
                                // If the user forgot to enter the number dead from the treatment test...
                                if (confirmedDormCount > 0 &&
                                    treatedDormCount >= confirmedDormCount &&
                                    deadCount == 0) dr["dead_count"] = treatedDormCount - confirmedDormCount;
                                // If the user forgot to enter the number confirmed from the treatment test...
                                else if (deadCount > 0 &&
                                    treatedDormCount >= deadCount &&
                                    confirmedDormCount == 0) dr["confirmed_dormant_count"] = treatedDormCount - deadCount;
                            }

                            // Now that the records have been scrubed for missing confirmed and/or dead count data - process the remaining 
                            // seeds that have not been counted and estimate dormant and dead based on results
                            // of the treated and confirmed numbers...
                            foreach (string colName in colNameArr)
                            {
                                int sum = 0;
                                string summation =_dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Compute("SUM(" + colName.Trim() + ")", "replication_number=" + (int)dr["replication_number"]).ToString();
                                if(!string.IsNullOrEmpty(summation) &&
                                    int.TryParse(summation, out sum))
                                {
                                    currentRepTotalCount += sum;
                                }
                            }
                            // Calculate the prorated dead and estimated dormant counts for all reps in the viability test
                            // based on the a subsample (aka treated dormant and confirmed dormant) testing outcome
                            dr["estimated_dormant_count"] = Math.Round((double)(expectedRepTotalCount - currentRepTotalCount) * percentDormant, 0, MidpointRounding.AwayFromZero);
                            if(string.IsNullOrEmpty(dr["dead_count"].ToString().Trim()))
                            {
                                dr["dead_count"] = expectedRepTotalCount - currentRepTotalCount - (int)dr["estimated_dormant_count"];
                            }
                            else
                            {
                                dr["dead_count"] = (int)dr["dead_count"] + expectedRepTotalCount - currentRepTotalCount - (int)dr["estimated_dormant_count"];
                            }
                        }
                    }
                    // Rebuild the viability data GUI...
                    BuildViabilityDataPage(_currentCountNumber);
                    UpdateViabilityRule();
                    // Rebuild the viability test date radio buttons...
                    UpdateViabilityDate();
                    // Rebuild the viability summary data...
                    UpdateViabilitySummary();
                    // If the user input included the Rep number in the textbox string set focus to that record...
                    if (_viabilityIDTokens.Length == 2)
                    {
                        SetTLPControlFocus();
                    }
                }
            }
        }

        private void ux_buttonCreateFromOrder_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            if (!string.IsNullOrEmpty(ux_textboxCreateFromOrder.Text))
            {
                int orderRequestID;
                if (int.TryParse(ux_textboxCreateFromOrder.Text, out orderRequestID))
                {
                    // Invoke the dialog for creating new viability tests - passing in the Inventory PKEYS...
                    Form vwnt = new ViabilityWizardNewTest(_sharedUtils, "", orderRequestID.ToString());
                    if (vwnt.ShowDialog() != DialogResult.Cancel)
                    {
                        ux_textboxCreateFromOrder.Clear();
                    }
                }
            }
        }


        private void ux_buttonCreateFromInventory_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            if (!string.IsNullOrEmpty(ux_textboxCreateFromInventory.Text))
            {
                string inventoryIDLookup = _sharedUtils.GetLookupValueMember(null, "inventory_lookup", ux_textboxCreateFromInventory.Text.Trim(), null, "!Error!");
                int inventoryID;

                if (int.TryParse(inventoryIDLookup, out inventoryID))
                {
                    ds = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory", ":inventoryid=" + inventoryID.ToString(), 0, 0);
                }
            }

            if (ds != null &&
                ds.Tables.Count > 0 &&
                ds.Tables.Contains("viability_wizard_get_inventory") &&
                ds.Tables["viability_wizard_get_inventory"].Rows.Count > 0)
            {
                // First gather up the parameters list of inventory_ids...
                string inventoryList = "";
                foreach (DataRow dr in ds.Tables["viability_wizard_get_inventory"].Rows)
                {
                    inventoryList += dr["inventory_id"].ToString() + ",";
                }
                // Invoke the dialog for creating new viability tests - passing in the Inventory PKEYS...
                Form vwnt = new ViabilityWizardNewTest(_sharedUtils, inventoryList.TrimEnd(','), "");
                if (vwnt.ShowDialog() != DialogResult.Cancel)
                {
                    ux_textboxCreateFromInventory.Clear();
                }
            }
        }

        private void ux_buttonPrintLabels_Click(object sender, EventArgs e)
        {
            string fullPathName = System.Windows.Forms.Application.StartupPath + "\\Reports\\" + ux_comboboxCrystalReports.Text;
//string fullPathName = System.Windows.Forms.Application.StartupPath + "\\Reports\\1x3_Dymo_Viability_Label.rpt";
            if (System.IO.File.Exists(fullPathName))
            {
                // Build the datatable to pass to the report...
                string[] inventoryViabilityTokens = ux_textboxInventoryViabilityID.Text.Trim().ToLower().Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                string inventoryViabilityID = "-1";
                double viabilityORinventoryNum = 1.0;
                if (inventoryViabilityTokens.Length > 0)
                {
                    if (double.TryParse(inventoryViabilityTokens[0], out viabilityORinventoryNum))
                    {
                        // The user input contains numbers only - so interpret this as an inventory_viability_id..
                        inventoryViabilityID = inventoryViabilityTokens[0];
                    }
                    else
                    {
                        // The user input contains alpha characters in the string - this must not be an Inventory Viabilty Number - so attempt to
                        // interpret it as an Inventory Number instead and convert the number to its inventory_id...
                        string inventoryID = _sharedUtils.GetLookupValueMember(null, "inventory_lookup", ux_textboxInventoryViabilityID.Text.Trim(), null, "!Error!");
                        if (inventoryID.ToUpper() != "!ERROR!")
                        {
                            DataSet temp_dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryid=" + inventoryID.Trim(), 0, 0);
                            if (temp_dsIV != null &&
                                temp_dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                                temp_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count > 0)
                            {
                                // There are inventory_viability records related to this inventory_id so get the most recent viability record and use that
                                // to update the ivRecordTokens...
                                string ivMaxPKEY = temp_dsIV.Tables["viability_wizard_get_inventory_viability"].Compute("MAX(inventory_viability_id)", null).ToString();
                                inventoryViabilityID = ivMaxPKEY;
                            }
                        }
                    }
                }

                if (inventoryViabilityID != "-1")
                {
                    DataTable dtLabelData = BuildViabilityTestLabelData(inventoryViabilityID, _sharedUtils);
                    // Resolve the FKEY and code_value data in the Inventory table to 'friendly names'...
                    DataGridView dgv = new DataGridView();
                    _sharedUtils.BuildReadOnlyDataGridView(dgv, dtLabelData);
                    if (dgv != null &&
                        dgv.DataSource != null &&
                        dgv.DataSource.GetType() == typeof(DataTable))
                    {
                        // Okay it looks like we have a datagridview with a datasource = datatable (with FKeys and code_values resolved) - so extract it and use in the report...
                        DataTable dt = (DataTable)dgv.DataSource;
                        // Got the data so now we can generate the Crystal Report...
                        GRINGlobal.Client.Common.ReportForm crustyReport = new ReportForm(dt, fullPathName);
                        crustyReport.StartPosition = FormStartPosition.CenterParent;
                        _sharedUtils.UpdateControls(crustyReport.Controls, crustyReport.Name);
                        crustyReport.ShowDialog();
                    }
                }
            }
        }

        static public DataTable BuildViabilityTestLabelData(string csvInventoryViabilityIDs, SharedUtils _su)
        {
            DataSet dsLabelData = _su.GetWebServiceData("viability_wizard_get_inventory_viability_label", ":inventoryviabilityid=" + csvInventoryViabilityIDs, 0, 0);
            DataTable dtLabelData = null;

            if (dsLabelData != null &&
                dsLabelData.Tables.Contains("viability_wizard_get_inventory_viability_label") &&
                dsLabelData.Tables["viability_wizard_get_inventory_viability_label"].Rows.Count > 0)
            {
                dtLabelData = dsLabelData.Tables["viability_wizard_get_inventory_viability_label"];
            }
            else
            {
                // Go get the inventory record(s) associated with this viability test...
                DataSet dsInventory = _su.GetWebServiceData("viability_wizard_get_inventory", ":inventoryviabilityid=" + csvInventoryViabilityIDs, 0, 0);
                dtLabelData = dsInventory.Tables["viability_wizard_get_inventory"];
                // Go get the viability record(s)....
                DataSet dsViabilityTest = _su.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryviabilityid=" + csvInventoryViabilityIDs, 0, 0);
                DataTable dtViabilityTest = dsViabilityTest.Tables["viability_wizard_get_inventory_viability"];
                // Add columns to the label data table for building the barcode text...
                dtLabelData.Columns.Add("inventory_viability_id", typeof(Int32));
                dtLabelData.Columns["inventory_viability_id"].ExtendedProperties.Add("gui_hint", "INTEGER_CONTROL");
                dtLabelData.Columns.Add("replication_number", typeof(Int32));
                dtLabelData.Columns["replication_number"].ExtendedProperties.Add("gui_hint", "INTEGER_CONTROL");
                foreach (DataRow drViabilityTest in dtViabilityTest.Rows)
                {
                    int Reps = 1;  // Default to 1 replication
                    // Find the inventory record associated with this viability test...
                    DataRow drLabelData = dtLabelData.Rows.Find(drViabilityTest["inventory_id"]);

                    if (drViabilityTest["replication_count"] != DBNull.Value)
                    {
                        //  If there is a value for the number of reps used for this test use it instead of the default...
                        Reps = (int)drViabilityTest["replication_count"];
                    }

                    // Build the barcode text for the first replication...
                    drLabelData["inventory_viability_id"] = drViabilityTest["inventory_viability_id"].ToString().Trim();
                    drLabelData["replication_number"] = "1";

                    // Build barcode text for the remaining replications...
                    for (int i = 2; i <= Reps; i++)
                    {
                        DataRow newDR = dtLabelData.NewRow();
                        // Remember the new rows autoincrement PKEY...
                        int newPKEY = (int)newDR[dtLabelData.PrimaryKey[0].ColumnName];
                        // Populate the new row with a copy of the existing data...
                        newDR.ItemArray = drLabelData.ItemArray;
                        // Replace the PKEY value from the existing data with the autoincrement PKEY...
                        newDR[dtLabelData.PrimaryKey[0].ColumnName] = newPKEY;
                        newDR["inventory_viability_id"] = drViabilityTest["inventory_viability_id"].ToString().Trim();
                        newDR["replication_number"] = i.ToString();
                        dtLabelData.Rows.Add(newDR);
                    }
                }
                // Commit all changes to the table...
                dtLabelData.AcceptChanges();
            }

            // Sort the data...
            dtLabelData.DefaultView.Sort = "inventory_viability_id ASC, replication_number ASC";

            // Return a newly built table (using records in sorted order from the above default view)...
            return dtLabelData.DefaultView.ToTable();
        }

        private void ux_buttonCreateViabilityDataRecords_Click(object sender, EventArgs e)
        {
            CreateNewViabilityDataRecords(1);
            BuildViabilityDataPage(1);
            UpdateViabilityRule();
            UpdateViabilityData();
            UpdateViabilityDate();
        }

        private void ux_buttonSaveIVChanges_Click(object sender, EventArgs e)
        {
            int errorCount = 0;
            // Compare and/or update the IV fields with the data in GUI controls...
            if (ux_textboxNumberOfReplicates.Text.Trim().ToUpper() != _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"].ToString().Trim().ToUpper())
            {
                // Update the replication count...
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"] = ux_textboxNumberOfReplicates.Text.Trim();
            }
            if (ux_textboxTotalSeeds.Text.Trim().ToUpper() != _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["total_tested_count"].ToString().Trim().ToUpper())
            {
                // Update the total tested count...
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["total_tested_count"] = ux_textboxTotalSeeds.Text.Trim();
            }
            if (ux_comboboxViabilityRule.SelectedValue != null &&
                ux_comboboxViabilityRule.SelectedValue.ToString().Trim().ToUpper() != _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"].ToString().Trim().ToUpper())
            {
                // Update the viability rule...
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"] = ux_comboboxViabilityRule.SelectedValue.ToString().Trim();
            }
            if (ux_textboxViabilityNotes.Text.Trim().ToUpper() != _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["note"].ToString().Trim().ToUpper())
            {
                // Update the notes...
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["note"] = ux_textboxViabilityNotes.Text.Trim();
            }

            // Get the changes (if any) for the inventory_viability table and commit them to the remote database...
            if (_dsIV != null && _dsIV.GetChanges() != null)
            {
                DataSet inventoryViabilityChanges = new DataSet();
                DataTable dtIV = _dsIV.Tables["viability_wizard_get_inventory_viability"];
                inventoryViabilityChanges.Tables.Add(dtIV.GetChanges());
                ScrubData(inventoryViabilityChanges);
                // Save the changes to the remote server...
                DataSet inventoryViabilitySaveResults = _sharedUtils.SaveWebServiceData(inventoryViabilityChanges);
                if (inventoryViabilitySaveResults.Tables.Contains(dtIV.TableName))
                {
                    errorCount += SyncSavedResults(dtIV, inventoryViabilitySaveResults.Tables[dtIV.TableName]);
                }
            }

            // Get the changes (if any) for the inventory_viability_rule table and commit them to the remote database...
            if (_dsIVR != null && _dsIVR.GetChanges() != null)
            {
                DataSet inventoryViabilityRuleChanges = new DataSet();
                DataTable dtIVR = _dsIVR.Tables["viability_wizard_get_inventory_viability_rule"];
                inventoryViabilityRuleChanges.Tables.Add(dtIVR.GetChanges());
                ScrubData(inventoryViabilityRuleChanges);
                // Save the changes to the remote server...
                DataSet inventoryViabilityRuleSaveResults = _sharedUtils.SaveWebServiceData(inventoryViabilityRuleChanges);
                if (inventoryViabilityRuleSaveResults.Tables.Contains(dtIVR.TableName))
                {
                    errorCount += SyncSavedResults(dtIVR, inventoryViabilityRuleSaveResults.Tables[dtIVR.TableName]);
                }
            }

            // Check to see if the rule chosen for this viability test is mapped to the taxonomy for the inventory...
//if (_dsI != null &&
//    _dsI.Tables.Contains("viability_wizard_get_inventory") &&
//    _dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("taxonomy_species_id") &&
//    _dsI.Tables["viability_wizard_get_inventory"].Rows.Count == 1 &&
//    _dsIV != null &&
//    _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
//    _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
//    _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("inventory_viability_rule_id") &&
//    _dsIVRM != null &&
//    _dsIVRM.Tables.Contains("viability_wizard_get_inventory_viability_rule") &&
//    _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule"].Columns.Contains("taxonomy_species_id") &&
//    _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule"].Columns.Contains("inventory_viability_rule_id") &&
//    _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule"].Rows.Count > 0)
//{
//    // Filter the IVR records to those associated with this taxonomy and viability rule...
//    _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule"].DefaultView.RowFilter = "taxonomy_species_id=" + _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["taxonomy_species_id"].ToString() + "inventory_viability_rule_id=" + _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"].ToString();
//    if (_dsIVR.Tables["viability_wizard_get_inventory_viability_rule"].DefaultView.Count <= 0)
//    {
//        // No mapping between the taxonomy and viability rule exists - so ask the user if they would like to create a mapping now...
//        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The chosen viability rule is not currently associated with this taxonomy.  Would you like to create an association now?", "Create Viability Rule - Taxonomy Association", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
//        ggMessageBox.Name = "ux_buttonSaveIVChanges1";
//        _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
//        string[] argsArray = new string[100];
//        ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
//        if (DialogResult.Yes == ggMessageBox.ShowDialog())
//        {
//        }
//    }
//}
            if (_dsI != null &&
                _dsI.Tables.Contains("viability_wizard_get_inventory") &&
                _dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("taxonomy_species_id") &&
                _dsI.Tables["viability_wizard_get_inventory"].Rows.Count == 1 &&
                _dsIV != null &&
                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("inventory_viability_rule_id"))
            {
                string taxonomySpeciesID = _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["taxonomy_species_id"].ToString();
                string inventoryViabilityRuleID = _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"].ToString();
                if (!GetViabilityRuleFilter(taxonomySpeciesID, null).Contains(inventoryViabilityRuleID))
                {
                    // No mapping between the taxonomy and viability rule exists - so ask the user if they would like to create a mapping now...
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The chosen viability rule is not currently associated with this taxonomy.  Would you like to create an association now?", "Create Viability Rule - Taxonomy Association", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "ux_buttonSaveIVChanges1";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    string[] argsArray = new string[100];
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    if (DialogResult.Yes == ggMessageBox.ShowDialog())
                    {
//DataSet dsIVRM = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability_rule_map", ":taxonomyspeciesid=-1", 0, 0);

                        if (_dsIVRM != null &&
                            _dsIVRM.Tables.Contains("viability_wizard_get_inventory_viability_rule_map") &&
                            _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].Columns.Contains("taxonomy_species_id") &&
                            _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].Columns.Contains("inventory_viability_rule_id") &&
                            _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].Columns.Contains("note"))
                        {
                            DataRow newIVRM = _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].NewRow();
                            newIVRM["taxonomy_species_id"] = taxonomySpeciesID;
                            newIVRM["inventory_viability_rule_id"] = inventoryViabilityRuleID;
                            newIVRM["note"] = ux_textboxTaxonomyNotes.Text;
                            _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].Rows.Add(newIVRM);
                            ScrubData(_dsIVRM);
                            DataSet dsIVRMSaveResults =_sharedUtils.SaveWebServiceData(_dsIVRM);
                        }
                    }
                }
            }


            if (errorCount == 0 && (_dsIV.GetChanges() == null || _dsIVR.GetChanges() == null))
            {
                // Disable the save and cancel buttons for the viability test (rule)...
                ux_buttonSaveIVChanges.Visible = true;
                ux_buttonSaveIVChanges.Enabled = false;
                ux_buttonCancelIVChanges.Visible = true;
                ux_buttonCancelIVChanges.Enabled = false;
                // Enable the print labels button...
                ux_buttonPrintLabels.Visible = true;
                ux_buttonPrintLabels.Enabled = true;
                ux_comboboxCrystalReports.Visible = true;
                ux_comboboxCrystalReports.Enabled = true;
                // If no count records exist for this test yet - enable the button to create new count records now...
                if (_dsIVD != null &&
                    _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
                    _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count < 1)
                {
                    ux_buttonCreateViabilityDataRecords.Visible = true;
                    ux_buttonCreateViabilityDataRecords.Enabled = true;
                }
            }
        }

        private void ux_buttonCancelIVChanges_Click(object sender, EventArgs e)
        {
            int intRowEdits = 0;
            if (_dsIV.Tables.Contains("viability_wizard_get_inventory_viability") && _dsIV.Tables["viability_wizard_get_inventory_viability"].GetChanges() != null) intRowEdits += _dsIV.Tables["viability_wizard_get_inventory_viability"].GetChanges().Rows.Count;

            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} unsaved row change(s), are you sure you want to cancel your edits?", "Cancel Edits", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "ViabilityWizard_CancelIVChangesMessage1";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = intRowEdits.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (DialogResult.Yes == ggMessageBox.ShowDialog())
            {
                // The user does not want to save data so:
                // 1) Reject the changes to the viability test...
                if(intRowEdits > 0) _dsIV.Tables["viability_wizard_get_inventory_viability"].RejectChanges();

                // 2) Update the inventory_viability_rule specific controls...
                UpdateViabilityRule();
                //if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("inventory_viability_rule_id"))
                //{
                //    if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"] != DBNull.Value)
                //    {
                //        if (ux_comboboxViabilityRule.SelectedValue.ToString().Trim().ToUpper() != _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"].ToString().Trim().ToUpper())
                //        {
                //            ux_comboboxViabilityRule.SelectedValue = _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"].ToString();
                //        }
                //    }
                //    else
                //    {
                //        ux_comboboxViabilityRule.SelectedItem = null;
                //    }
                //}
                //if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("total_tested_count"))
                //{
                //    if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["total_tested_count"] != DBNull.Value)
                //    {
                //        if (ux_textboxTotalSeeds.Text.Trim().ToUpper() != _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["total_tested_count"].ToString().Trim().ToUpper())
                //        {
                //            ux_textboxTotalSeeds.Text = _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["total_tested_count"].ToString();
                //        }
                //    }
                //    else
                //    {
                //        //ux_textboxTotalSeeds.Text = "";
                //    }
                //}
                //if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("replication_count"))
                //{
                //    if (_dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"] != DBNull.Value)
                //    {
                //        if (ux_textboxNumberOfReplicates.Text.Trim().ToUpper() != _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"].ToString().Trim().ToUpper())
                //        {
                //            ux_textboxNumberOfReplicates.Text = _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["replication_count"].ToString();
                //        }
                //    }
                //    else
                //    {
                //        //ux_textboxNumberOfReplicates.Text = "";
                //    }
                //}

                // 3) Disable the save and cancel buttons for the viability test (rule)...
                ux_buttonSaveIVChanges.Visible = true;
                ux_buttonSaveIVChanges.Enabled = false;
                ux_buttonCancelIVChanges.Visible = true;
                ux_buttonCancelIVChanges.Enabled = false;
            }
        }

        private void ux_comboboxViabilityRule_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow dr = null;

            // Update the IV parameters based on the current IV test data loaded...
            UpdateViabilityRule();

            // Get the test rule record from the rules table that is associated with the rule chosen in the combobox...
            if (_dsIVR != null &&
                _dsIVR.Tables.Contains("viability_wizard_get_inventory_viability_rule"))
            {
                dr = _dsIVR.Tables["viability_wizard_get_inventory_viability_rule"].Rows.Find(ux_comboboxViabilityRule.SelectedValue);
            }

            // Apply the settings from the rule to the controls on the GUI...
            if (dr != null &&
                dr.Table.Columns.Contains("seeds_per_replicate") &&
                dr.Table.Columns.Contains("number_of_replicates"))
            {
                // Start with the default Total Tested Count...
                string TotalTestedCount = "";
                // Override with new rule settings (if they are populated)... 
                if (dr["seeds_per_replicate"] != DBNull.Value && dr["number_of_replicates"] != DBNull.Value)
                {
                    double spr;
                    double nor;
                    if (double.TryParse(dr["seeds_per_replicate"].ToString(), out spr) &&
                        double.TryParse(dr["number_of_replicates"].ToString(), out nor))
                    {
                        TotalTestedCount = ((int)(spr * nor)).ToString();
                    }
                }
                // Set the textbox control with the TotalTestedCount...
                ux_textboxTotalSeeds.Text = TotalTestedCount;

                // Default Number Of Replicates...
                string NumberOfReplicates = "";
                // Override with new rule settings (if they are populated)... 
                if (dr["number_of_replicates"] != DBNull.Value)
                {
                    int nor;
                    if (int.TryParse(dr["number_of_replicates"].ToString(), out nor))
                    {
                        NumberOfReplicates = nor.ToString();
                    }
                }
                // Set the textbox with the NumberOfReplicates...
                ux_textboxNumberOfReplicates.Text = NumberOfReplicates;

                // Display Substrata....
                if (dr.Table.Columns.Contains("substrata")) ux_textboxSubstrata.Text = dr["substrata"].ToString();

                // Display Moisture....
                if (dr.Table.Columns.Contains("moisture")) ux_textboxMoisture.Text = dr["moisture"].ToString();

                // Display Prechill....
                if (dr.Table.Columns.Contains("prechill")) ux_textboxPrechill.Text = dr["prechill"].ToString();

                // Display Temperature....
                if (dr.Table.Columns.Contains("temperature_range")) ux_textboxTemperature.Text = dr["temperature_range"].ToString();

                // Display Lighting....
                if (dr.Table.Columns.Contains("lighting")) ux_textboxLighting.Text = dr["lighting"].ToString();

                // Display Requirements....
                if (dr.Table.Columns.Contains("requirements")) ux_textboxRuleNotes.Text = "Requirements: " + dr["requirements"].ToString();

                // Display Notes....
                if (dr.Table.Columns.Contains("note")) ux_textboxRuleNotes.Text = ux_textboxRuleNotes.Text + "\r\nNotes: " + dr["note"].ToString();

                // Display Taxonomy Notes......
                if (IsTaxonomyMappedToRule())
                {
                    // Disable edits to the textbox for the Taxonomy note...
                    ux_textboxTaxonomyNotes.ReadOnly = true;
                    if (_dsI != null &&
                        _dsI.Tables.Contains("viability_wizard_get_inventory") &&
                        _dsI.Tables["viability_wizard_get_inventory"].Rows.Count == 1 &&
                        _dsI.Tables["viability_wizard_get_inventory"].Columns.Contains("taxonomy_species_id") &&
                        _dsIVRM != null &&
                        _dsIVRM.Tables.Contains("viability_wizard_get_inventory_viability_rule_map") &&
                        _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].Columns.Contains("taxonomy_species_id") &&
                        _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].Columns.Contains("inventory_viability_rule_id") &&
                        _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].Rows.Count > 0)
                    {
                        string ivrmFilter = _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView.RowFilter;
                        _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView.RowFilter = "inventory_viability_rule_id=" + ux_comboboxViabilityRule.SelectedValue.ToString() + " AND taxonomy_species_id=" + _dsI.Tables["viability_wizard_get_inventory"].Rows[0]["taxonomy_species_id"].ToString();
                        if (_dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView.Count == 1)
                        {
                            // Update the taxonomy note textbox...
                            ux_textboxTaxonomyNotes.Text = _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView[0]["note"].ToString();
                        }
                        else
                        {
                            // Clear the taxonomy note textbox...
                            ux_textboxTaxonomyNotes.Text = "";
                        }
                        _dsIVRM.Tables["viability_wizard_get_inventory_viability_rule_map"].DefaultView.RowFilter = ivrmFilter;
                    }
                }
                else
                {
                    // Clear the textbox and enable edits for the taxonomy note textbox...
                    ux_textboxTaxonomyNotes.ReadOnly = false;
                    ux_textboxTaxonomyNotes.Text = "";
                }
            }
            else
            {
                // Clear the textbox for TotalTestedCount...
                ux_textboxTotalSeeds.Text = "";
                // Clear the textbox for number of replicates...
                ux_textboxNumberOfReplicates.Text = "";
            }

//if (_dsIV != null &&
//    _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
//    _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
//    _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("inventory_viability_rule_id"))
//{
//    if (ux_comboboxViabilityRule.SelectedItem == null ||
//        _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"].ToString() != ux_comboboxViabilityRule.SelectedValue.ToString())
//    {
//        ux_buttonSaveIVChanges.Visible = true;
//        ux_buttonCancelIVChanges.Visible = true;
//    }
//}

            //// Show/Enable the print labels button...
            //if (!string.IsNullOrEmpty(ux_textboxNumberOfReplicates.Text) &&
            //    !string.IsNullOrEmpty(ux_textboxTotalSeeds.Text))
            //{
            //    ux_buttonPrintLabels.Visible = true;
            //    ux_buttonPrintLabels.Enabled = true;
            //}
            //else
            //{
            //    ux_buttonPrintLabels.Visible = true;
            //    ux_buttonPrintLabels.Enabled = false;
            //}
            //// There are NO existing IVD records so Show/Enable the create IVD records button...
            //if (!string.IsNullOrEmpty(ux_textboxNumberOfReplicates.Text) &&
            //    !string.IsNullOrEmpty(ux_textboxTotalSeeds.Text) &&
            //    _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count <= 0)
            //{
            //    ux_buttonCreateViabilityDataRecords.Visible = true;
            //    ux_buttonCreateViabilityDataRecords.Enabled = true;
            //}
            //else
            //{
            //    ux_buttonCreateViabilityDataRecords.Visible = true;
            //    ux_buttonCreateViabilityDataRecords.Enabled = false;
            //}

//            if (_dsIV != null &&
//                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
//                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
//                _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("inventory_viability_rule_id"))
//            {
//                if (ux_comboboxViabilityRule.SelectedValue != null &&
//                    ux_comboboxViabilityRule.SelectedValue.ToString().Trim() != _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"].ToString().Trim())
//                {
//                    // Update the viability rule...
//                    _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows[0]["inventory_viability_rule_id"] = ux_comboboxViabilityRule.SelectedValue.ToString();

//                    if (_dsIV.GetChanges() != null)
//                    {
//                        // Show the save and cancel buttons for the viability test (rule)...
//                        ux_buttonSaveIVChanges.Visible = true;
//                        ux_buttonCancelIVChanges.Visible = true;
//                    }

//                    // Set the radio buttons and count data controls to match the vaules for this viability test...
//                    if (_dsIVD != null &&
//                        _dsIVD.Tables.Contains("viability_wizard_get_inventory_viability_data") &&
//                        _dsIVD.Tables["viability_wizard_get_inventory_viability_data"].Rows.Count == 0)
//                    {
//                        // Clear all of the controls used to update the counts (lingering info from the last viability test)...
//                        RemoveViabilityDateControls();
//                        RemoveViabilityDataControls();
//                        RemoveViabilitySummaryControls();

////// This is a new viability test that does not have any raw data counts saved yet
////// so first clear all the count radio buttons and then update the interface...
////for (int i = 1; i <= 8; i++)
////{
////    string radioButtonName = "ux_radiobuttonCount" + i.ToString();
////    // Find the actual radio button control by name and update the text with the count date...
////    Control[] rb = Controls.Find(radioButtonName, true);
////    if (rb.Length == 1)
////    {
////        // Check to see if this radio button is checked and if so uncheck it 
////        // (this ensures the radio button event will fire when the user clicks the radiobutton)...
////        if (((RadioButton)rb[0]).Checked) ((RadioButton)rb[0]).Checked = false;
////    }
////    // Update the radiobutton controls to reflect the current status of this viability test...
////    UpdateCountDateControls();
////}

//                        if (_dsIVD.GetChanges() != null)
//                        {
//                            // Show the save and cancel buttons for the viability test data...
//                            ux_buttonSaveIVDChanges.Visible = true;
//                            ux_buttonCancelIVDChanges.Visible = true;
//                        }

//                        if (_dsIV != null &&
//                            _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
//                            _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1)
//                        {
//                            ux_buttonCreateViabilityDataRecords.Visible = true;
//                        }
//                    }
//                }
//            }
        }

        private void ux_textboxNumberOfReplicates_TextChanged(object sender, EventArgs e)
        {
            if (_dsIV != null &&
                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("replication_count"))
            {
                int NumberOfReplicates = 4;
                if (int.TryParse(ux_textboxNumberOfReplicates.Text, out NumberOfReplicates))
                {
                    ux_buttonSaveIVChanges.Visible = true;
                    ux_buttonSaveIVChanges.Enabled = true;
                    ux_buttonCancelIVChanges.Visible = true;
                    ux_buttonCancelIVChanges.Enabled = true;
                }
            }
        }

        private void ux_textboxTotalSeeds_TextChanged(object sender, EventArgs e)
        {
            if (_dsIV != null &&
                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("total_tested_count"))
            {
                int TotalTestedCount = 200;
                if (int.TryParse(ux_textboxTotalSeeds.Text, out TotalTestedCount))
                {
                    ux_buttonSaveIVChanges.Visible = true;
                    ux_buttonSaveIVChanges.Enabled = true;
                    ux_buttonCancelIVChanges.Visible = true;
                    ux_buttonCancelIVChanges.Enabled = true;
                }
            }
        }

        private void ux_textboxViabilityNotes_TextChanged(object sender, EventArgs e)
        {
            if (_dsIV != null &&
                _dsIV.Tables.Contains("viability_wizard_get_inventory_viability") &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Rows.Count == 1 &&
                _dsIV.Tables["viability_wizard_get_inventory_viability"].Columns.Contains("note"))
            {
                ux_buttonSaveIVChanges.Visible = true;
                ux_buttonSaveIVChanges.Enabled = true;
                ux_buttonCancelIVChanges.Visible = true;
                ux_buttonCancelIVChanges.Enabled = true;
            }
        }

        private void ux_checkboxShowAllRules_CheckedChanged(object sender, EventArgs e)
        {
            //if (ux_checkboxShowAllRules.Checked)
            //{
            //    //((DataTable)ux_comboboxViabilityRule.DataSource).DefaultView.RowFilter = "";
            //    _currentViabilityRuleFilter = "";
            //}
            //else
            //{
            //    //((DataTable)ux_comboboxViabilityRule.DataSource).DefaultView.RowFilter = GetViabilityRuleFilter(_dsI.Tables["viability_wizard_get_inventory"].Rows[0]["taxonomy_species_id"].ToString());
int currentViabilityRuleIDSelected = -1;
if (ux_comboboxViabilityRule.SelectedItem != null)
{
    currentViabilityRuleIDSelected = (int)ux_comboboxViabilityRule.SelectedValue;
}
//_currentViabilityRuleFilter = GetViabilityRuleFilter(_dsI.Tables["viability_wizard_get_inventory"].Rows[0]["taxonomy_species_id"].ToString());
UpdateViabilityRule();
ux_comboboxViabilityRule.SelectedValue = currentViabilityRuleIDSelected;
            //}
        }

        private int ComputeColumnSum(DataTable table, string columnName, string filter)
        {
            string sum = "0";

            // Calculate the sum of the table column using the row filter...
            if (table.Columns.Contains(columnName))
            {
                sum = table.Compute("SUM(" + columnName + ")", filter).ToString();
                if (string.IsNullOrEmpty(sum))
                {
                    sum = "0";
                }
            }
            
            return Convert.ToInt32(sum);
        }

        private DataRow ComputeAggregateViabilityStats(DataTable dtReplicateTotals)
        {
            DataRow returnViabilityStats = dtReplicateTotals.NewRow();

            // Create a table for storing intermediate values for the roundup/rounddown stats...
            DataTable intermediateStats = new DataTable();
            
            intermediateStats.Columns.Add("count_name", Type.GetType("System.String"));
            intermediateStats.PrimaryKey = new DataColumn[] { intermediateStats.Columns["count_name"] };
            intermediateStats.Columns.Add("integer_portion", Type.GetType("System.Int64"));
            intermediateStats.Columns.Add("remainder_portion", Type.GetType("System.Int64"));
            intermediateStats.Columns.Add("remainder_plus_aosa_rank", Type.GetType("System.Int64"));
            intermediateStats.Columns.Add("rounded", Type.GetType("System.Int64"));
            intermediateStats.Columns.Add("reported", Type.GetType("System.Int64"));

            // Gather the column summations across the replicates...
            string colName = "";
            int totalSeeds = 200;
            if (dtReplicateTotals.Columns.Contains("replication_count"))
            {
                totalSeeds = ComputeColumnSum(dtReplicateTotals, "replication_count", null);
            }
            int cumCount = 0;
            DataRow newDR = null;
            foreach (DataColumn dc in dtReplicateTotals.Columns)
            {
                colName = dc.ColumnName.ToLower().Trim();
                if (colName.StartsWith("percent_") &&
                    colName != "percent_viable")
                {
                    cumCount = ComputeColumnSum(dtReplicateTotals, colName, null);
                    newDR = intermediateStats.NewRow();
                    newDR["count_name"] = colName;
                    newDR["integer_portion"] = cumCount * 100 / totalSeeds;
                    Int64 tempRemainder = (Int64)Math.Truncate((((double)cumCount * 100.0 / (double)totalSeeds) - (cumCount * 100 / totalSeeds)) * totalSeeds);
                    newDR["remainder_portion"] = tempRemainder;  //Math.IEEERemainder((double)cumPercent, (double)repCount);
                    newDR["rounded"] = Math.Truncate(((double)cumCount * 100.0 / (double)totalSeeds) + 0.5);
                    newDR["reported"] = newDR["integer_portion"];
                    // Calculate a 'ranking' of the count data by combining the remainder with the AOSA 'tie-breaker' order of counts...
                    switch (colName)
                    {
                        case "percent_normal":
                            newDR["remainder_plus_aosa_rank"] = (tempRemainder * 100) + 8;
                            // Before summing the percent counts for the rest of the data - round up the percent for 'normal' counts 
                            // if the normal percent has a decimal fraction of 0.5 or larger...
                            newDR["reported"] = newDR["rounded"];
                            break;
                        case "percent_abnormal":
                            newDR["remainder_plus_aosa_rank"] = (tempRemainder * 100) + 7;
                            break;
                        case "percent_hard":
                            newDR["remainder_plus_aosa_rank"] = (tempRemainder * 100) + 6;
                            break;
                        case "percent_dormant":
                            newDR["remainder_plus_aosa_rank"] = (tempRemainder * 100) + 5;
                            break;
                        case "percent_dead":
                            newDR["remainder_plus_aosa_rank"] = (tempRemainder * 100) + 4;
                            break;
                        case "percent_empty":
                            newDR["remainder_plus_aosa_rank"] = (tempRemainder * 100) + 3;
                            break;
                        case "percent_infested":
                            newDR["remainder_plus_aosa_rank"] = (tempRemainder * 100) + 2;
                            break;
                        case "percent_unknown":
                            newDR["remainder_plus_aosa_rank"] = (tempRemainder * 100) + 1;
                            break;
                        default:
                            break;
                    }

                    intermediateStats.Rows.Add(newDR);
                }
            }

            // Sort the table by remainder + AOSA ranking in descending order...
            intermediateStats.DefaultView.Sort = "remainder_plus_aosa_rank DESC";
            int currentCountIndex = 0;
            // If the truncated sum of the counts does not add up to 100% - then increase the highest ranked percent calculation by 1% - then sum the count percents to check to see 
            // if the percent sums total 100% - if not repeat with the next highest ranked count percent....
            while (ComputeColumnSum(intermediateStats, "reported", null) < 100 && currentCountIndex < intermediateStats.Rows.Count)
            {
                if ((Int64)intermediateStats.DefaultView[currentCountIndex]["reported"] < (Int64)intermediateStats.DefaultView[currentCountIndex]["rounded"])
                {
                    intermediateStats.DefaultView[currentCountIndex]["reported"] = intermediateStats.DefaultView[currentCountIndex]["rounded"];
                }
                currentCountIndex++;
            }

            // Transfer the data from the intermediateStats table to the return datarow......
            returnViabilityStats["percent_normal"] = intermediateStats.Rows.Find("percent_normal")["reported"];
            returnViabilityStats["percent_abnormal"] = intermediateStats.Rows.Find("percent_abnormal")["reported"];
            returnViabilityStats["percent_dormant"] = intermediateStats.Rows.Find("percent_dormant")["reported"];
//returnViabilityStats["percent_viable"] = (Int64)intermediateStats.Rows.Find("percent_normal")["reported"] + (Int64)intermediateStats.Rows.Find("percent_dormant")["reported"];
returnViabilityStats["percent_viable"] = (Int64)intermediateStats.Rows.Find("percent_normal")["reported"] + (Int64)intermediateStats.Rows.Find("percent_hard")["reported"] + (Int64)intermediateStats.Rows.Find("percent_dormant")["reported"];
            returnViabilityStats["percent_hard"] = intermediateStats.Rows.Find("percent_hard")["reported"];
            returnViabilityStats["percent_empty"] = intermediateStats.Rows.Find("percent_empty")["reported"];
            returnViabilityStats["percent_infested"] = intermediateStats.Rows.Find("percent_infested")["reported"];
            returnViabilityStats["percent_dead"] = intermediateStats.Rows.Find("percent_dead")["reported"];
            returnViabilityStats["percent_unknown"] = intermediateStats.Rows.Find("percent_unknown")["reported"];

            return returnViabilityStats;
        }

        private void ux_buttonPostFinalResults_Click(object sender, EventArgs e)
        {
            DataTable dtIV = _dsIV.Tables["viability_wizard_get_inventory_viability"];
            DataTable dtIVD = _dsIVD.Tables["viability_wizard_get_inventory_viability_data"];

            if (dtIV != null &&
                dtIV.Rows.Count == 1 &&
                dtIVD != null &&
                dtIVD.Rows.Count > 0)
            {
                // Gather up the various counts...
                string maxRepNum = dtIVD.Compute("MAX(replication_number)", null).ToString();
                string maxViabilityCounts = GetMaxCount().ToString();  // dtIVD.Compute("MAX(count_number)", null).ToString();
                int totalNumberOfReps = 1;
                int numViabilityCounts = 1;
                if (!string.IsNullOrEmpty(maxRepNum)) totalNumberOfReps = Convert.ToInt32(maxRepNum);
                if (!string.IsNullOrEmpty(maxViabilityCounts)) numViabilityCounts = Convert.ToInt32(maxViabilityCounts);
                int total_tested_count = ComputeColumnSum(dtIVD, "replication_count", "count_number=" + numViabilityCounts.ToString());
                DataTable dtReplicateTotals = dtIV.Clone();

                for (int rep = 1; rep <= totalNumberOfReps; rep++)
                {
                    // Compute the count totals for each replication...
                    int replication_count = ComputeColumnSum(dtIVD, "replication_count", "replication_number=" + rep.ToString() + " AND count_number=" + numViabilityCounts);
                    int rep_normal_count = ComputeColumnSum(dtIVD, "normal_count", "replication_number=" + rep.ToString());
                    int rep_abnormal_count = ComputeColumnSum(dtIVD, "abnormal_count", "replication_number=" + rep.ToString());
                    int rep_dormant_count = ComputeColumnSum(dtIVD, "dormant_count", "replication_number=" + rep.ToString());
                    int rep_hard_count = ComputeColumnSum(dtIVD, "hard_count", "replication_number=" + rep.ToString());
                    int rep_empty_count = ComputeColumnSum(dtIVD, "empty_count", "replication_number=" + rep.ToString());
                    int rep_infested_count = ComputeColumnSum(dtIVD, "infested_count", "replication_number=" + rep.ToString());
                    int rep_dead_count = ComputeColumnSum(dtIVD, "dead_count", "replication_number=" + rep.ToString());
                    int rep_unknown_count = ComputeColumnSum(dtIVD, "unknown_count", "replication_number=" + rep.ToString());
                    int rep_estimated_dormant_count = ComputeColumnSum(dtIVD, "estimated_dormant_count", "replication_number=" + rep.ToString());
                    int rep_confirmed_dormant_count = ComputeColumnSum(dtIVD, "confirmed_dormant_count", "replication_number=" + rep.ToString());

                    DataRow repTotals = dtReplicateTotals.NewRow();

                    // Populate the cumulative counts that will be used to update the inventory viability record
                    // NOTE: even though these are counts and not percentages they are still integer and upcoming method call
                    //       requires counts for calculating the posted percents...
                    repTotals["replication_count"] = replication_count;
                    repTotals["percent_normal"] = rep_normal_count;
                    repTotals["percent_abnormal"] = rep_abnormal_count;
                    repTotals["percent_dormant"] = rep_dormant_count + rep_estimated_dormant_count + rep_confirmed_dormant_count;
                    repTotals["percent_hard"] = rep_hard_count;
                    repTotals["percent_empty"] = rep_empty_count;
                    repTotals["percent_infested"] = rep_infested_count;
                    repTotals["percent_dead"] = rep_dead_count;
                    repTotals["percent_unknown"] = rep_unknown_count;
                    repTotals["percent_viable"] = rep_normal_count + rep_hard_count + rep_dormant_count + rep_estimated_dormant_count + rep_confirmed_dormant_count;

                    // Add the totals record to the temp totals table...
                    dtReplicateTotals.Rows.Add(repTotals);
                }

                // Calculate the percentages based on the counts summed above...
                DataRow aggregatedPercentViabilityStats = ComputeAggregateViabilityStats(dtReplicateTotals);

                dtIV.Rows[0]["tested_date"] = DateTime.Now;
                dtIV.Rows[0]["tested_date_code"] = "MM/dd/yyyy";
                dtIV.Rows[0]["percent_normal"] = aggregatedPercentViabilityStats["percent_normal"].ToString();
                dtIV.Rows[0]["percent_abnormal"] = aggregatedPercentViabilityStats["percent_abnormal"].ToString();
                dtIV.Rows[0]["percent_dormant"] = aggregatedPercentViabilityStats["percent_dormant"].ToString();
                dtIV.Rows[0]["percent_viable"] = aggregatedPercentViabilityStats["percent_viable"].ToString();
                dtIV.Rows[0]["vigor_rating_code"] = null;
                dtIV.Rows[0]["total_tested_count"] = total_tested_count;
                dtIV.Rows[0]["replication_count"] = totalNumberOfReps;
                dtIV.Rows[0]["percent_hard"] = aggregatedPercentViabilityStats["percent_hard"].ToString();
                dtIV.Rows[0]["percent_empty"] = aggregatedPercentViabilityStats["percent_empty"].ToString();
                dtIV.Rows[0]["percent_infested"] = aggregatedPercentViabilityStats["percent_infested"].ToString();
                dtIV.Rows[0]["percent_dead"] = aggregatedPercentViabilityStats["percent_dead"].ToString();
                dtIV.Rows[0]["percent_unknown"] = aggregatedPercentViabilityStats["percent_unknown"].ToString();

                // Save the updated percentages to the remote database...
                int errorCount = SaveViabilityData();

                // Update the form controls to reflect the update to the viability record...
                if (errorCount == 0)
                {
                    // Reset the controls to default state...
                    ux_groupboxGetViabilityTest.Enabled = true;
                    ux_groupboxCreateViabilityTest.Enabled = true;
                    ux_buttonSaveIVDChanges.Visible = true;
                    ux_buttonSaveIVDChanges.Enabled = false;
                    ux_buttonCancelIVDChanges.Visible = true;
                    ux_buttonCancelIVDChanges.Enabled = false;
                    ux_buttonPostFinalResults.Visible = true;
                    ux_buttonPostFinalResults.Enabled = false;

                    // Clear all data in the global datasets and then reset the GUI
                    _dsI = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory", ":inventoryviabilityid=-1", 0, 0);
                    _dsIV = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryviabilityid=-1", 0, 0);
                    _dsIVD = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability_data", ":inventoryviabilityid=-1", 0, 0);
                    ResetGUI();
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Viability Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "ViabilityWizard_ux_buttonSave1";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    ggMessageBox.ShowDialog();
                    ux_textboxInventoryViabilityID.Focus();
                }
                else
                {
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed.\n\n  Error Count: {0}", "Viability Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "ViabilityWizard_ux_buttonSave2";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    string[] argsArray = new string[100];
                    argsArray[0] = errorCount.ToString();
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    ggMessageBox.ShowDialog();
                }
            }
        }
    }
}
