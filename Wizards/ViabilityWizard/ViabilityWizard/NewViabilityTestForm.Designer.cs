﻿namespace ViabilityWizard
{
    partial class NewViabilityTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewViabilityTestForm));
            this.ux_labelOrderInventoryNumber = new System.Windows.Forms.Label();
            this.ux_textboxOrderInventoryNumber = new System.Windows.Forms.TextBox();
            this.ux_buttonGo = new System.Windows.Forms.Button();
            this.ux_textboxInventoryID = new System.Windows.Forms.TextBox();
            this.ux_labelInventoryID = new System.Windows.Forms.Label();
            this.ux_textboxTaxonomySpeciesID = new System.Windows.Forms.TextBox();
            this.ux_labelTaxonomySpeciesID = new System.Windows.Forms.Label();
            this.ux_textboxInventoryMaintPolicyID = new System.Windows.Forms.TextBox();
            this.ux_labelInventoryMaintPolicyID = new System.Windows.Forms.Label();
            this.ux_textboxPercentViable = new System.Windows.Forms.TextBox();
            this.ux_labelPercentViable = new System.Windows.Forms.Label();
            this.ux_textboxViabilityTestedDate = new System.Windows.Forms.TextBox();
            this.ux_labelViabilityTestedDate = new System.Windows.Forms.Label();
            this.ux_groupboxInventoryData = new System.Windows.Forms.GroupBox();
            this.ux_groupboxViabilityTestParameters = new System.Windows.Forms.GroupBox();
            this.ux_comboboxViabilityRule = new System.Windows.Forms.ComboBox();
            this.ux_checkboxShowAllRules = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ux_textboxSeedPerReplicate = new System.Windows.Forms.TextBox();
            this.ux_labelSeedPerReplicate = new System.Windows.Forms.Label();
            this.ux_textboxReplicates = new System.Windows.Forms.TextBox();
            this.ux_labelReplicates = new System.Windows.Forms.Label();
            this.ux_bindingnavigatorInventory = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ux_buttonOK = new System.Windows.Forms.Button();
            this.ux_buttonCancel = new System.Windows.Forms.Button();
            this.ux_groupboxSearchCriteria = new System.Windows.Forms.GroupBox();
            this.ux_groupboxInventoryData.SuspendLayout();
            this.ux_groupboxViabilityTestParameters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_bindingnavigatorInventory)).BeginInit();
            this.ux_bindingnavigatorInventory.SuspendLayout();
            this.ux_groupboxSearchCriteria.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_labelOrderInventoryNumber
            // 
            this.ux_labelOrderInventoryNumber.AutoSize = true;
            this.ux_labelOrderInventoryNumber.Location = new System.Drawing.Point(6, 21);
            this.ux_labelOrderInventoryNumber.Name = "ux_labelOrderInventoryNumber";
            this.ux_labelOrderInventoryNumber.Size = new System.Drawing.Size(125, 13);
            this.ux_labelOrderInventoryNumber.TabIndex = 0;
            this.ux_labelOrderInventoryNumber.Text = "Order/Inventory Number:";
            this.ux_labelOrderInventoryNumber.Click += new System.EventHandler(this.ux_labelOrderInventoryNumber_Click);
            // 
            // ux_textboxOrderInventoryNumber
            // 
            this.ux_textboxOrderInventoryNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxOrderInventoryNumber.Location = new System.Drawing.Point(139, 19);
            this.ux_textboxOrderInventoryNumber.Name = "ux_textboxOrderInventoryNumber";
            this.ux_textboxOrderInventoryNumber.Size = new System.Drawing.Size(237, 20);
            this.ux_textboxOrderInventoryNumber.TabIndex = 1;
            this.ux_textboxOrderInventoryNumber.TextChanged += new System.EventHandler(this.ux_textboxOrderInventoryNumber_TextChanged);
            // 
            // ux_buttonGo
            // 
            this.ux_buttonGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonGo.Location = new System.Drawing.Point(382, 16);
            this.ux_buttonGo.Name = "ux_buttonGo";
            this.ux_buttonGo.Size = new System.Drawing.Size(51, 23);
            this.ux_buttonGo.TabIndex = 3;
            this.ux_buttonGo.Text = "Go!";
            this.ux_buttonGo.UseVisualStyleBackColor = true;
            this.ux_buttonGo.Click += new System.EventHandler(this.ux_buttonGo_Click);
            // 
            // ux_textboxInventoryID
            // 
            this.ux_textboxInventoryID.Location = new System.Drawing.Point(9, 41);
            this.ux_textboxInventoryID.Name = "ux_textboxInventoryID";
            this.ux_textboxInventoryID.Size = new System.Drawing.Size(124, 20);
            this.ux_textboxInventoryID.TabIndex = 5;
            this.ux_textboxInventoryID.Tag = "inventory_id";
            // 
            // ux_labelInventoryID
            // 
            this.ux_labelInventoryID.AutoSize = true;
            this.ux_labelInventoryID.Location = new System.Drawing.Point(6, 25);
            this.ux_labelInventoryID.Name = "ux_labelInventoryID";
            this.ux_labelInventoryID.Size = new System.Drawing.Size(68, 13);
            this.ux_labelInventoryID.TabIndex = 4;
            this.ux_labelInventoryID.Tag = "inventory_id";
            this.ux_labelInventoryID.Text = "Inventory ID:";
            // 
            // ux_textboxTaxonomySpeciesID
            // 
            this.ux_textboxTaxonomySpeciesID.Location = new System.Drawing.Point(139, 41);
            this.ux_textboxTaxonomySpeciesID.Name = "ux_textboxTaxonomySpeciesID";
            this.ux_textboxTaxonomySpeciesID.Size = new System.Drawing.Size(124, 20);
            this.ux_textboxTaxonomySpeciesID.TabIndex = 7;
            this.ux_textboxTaxonomySpeciesID.Tag = "taxonomy_species_id";
            // 
            // ux_labelTaxonomySpeciesID
            // 
            this.ux_labelTaxonomySpeciesID.AutoSize = true;
            this.ux_labelTaxonomySpeciesID.Location = new System.Drawing.Point(136, 25);
            this.ux_labelTaxonomySpeciesID.Name = "ux_labelTaxonomySpeciesID";
            this.ux_labelTaxonomySpeciesID.Size = new System.Drawing.Size(114, 13);
            this.ux_labelTaxonomySpeciesID.TabIndex = 6;
            this.ux_labelTaxonomySpeciesID.Tag = "taxonomy_species_id";
            this.ux_labelTaxonomySpeciesID.Text = "Taxonomy Species ID:";
            // 
            // ux_textboxInventoryMaintPolicyID
            // 
            this.ux_textboxInventoryMaintPolicyID.Location = new System.Drawing.Point(269, 41);
            this.ux_textboxInventoryMaintPolicyID.Name = "ux_textboxInventoryMaintPolicyID";
            this.ux_textboxInventoryMaintPolicyID.Size = new System.Drawing.Size(124, 20);
            this.ux_textboxInventoryMaintPolicyID.TabIndex = 9;
            this.ux_textboxInventoryMaintPolicyID.Tag = "inventory_maint_policy_id";
            // 
            // ux_labelInventoryMaintPolicyID
            // 
            this.ux_labelInventoryMaintPolicyID.AutoSize = true;
            this.ux_labelInventoryMaintPolicyID.Location = new System.Drawing.Point(266, 25);
            this.ux_labelInventoryMaintPolicyID.Name = "ux_labelInventoryMaintPolicyID";
            this.ux_labelInventoryMaintPolicyID.Size = new System.Drawing.Size(128, 13);
            this.ux_labelInventoryMaintPolicyID.TabIndex = 8;
            this.ux_labelInventoryMaintPolicyID.Tag = "inventory_maint_policy_id";
            this.ux_labelInventoryMaintPolicyID.Text = "Inventory Maint Policy ID:";
            // 
            // ux_textboxPercentViable
            // 
            this.ux_textboxPercentViable.Location = new System.Drawing.Point(9, 80);
            this.ux_textboxPercentViable.Name = "ux_textboxPercentViable";
            this.ux_textboxPercentViable.Size = new System.Drawing.Size(124, 20);
            this.ux_textboxPercentViable.TabIndex = 11;
            this.ux_textboxPercentViable.Tag = "percent_viable";
            // 
            // ux_labelPercentViable
            // 
            this.ux_labelPercentViable.AutoSize = true;
            this.ux_labelPercentViable.Location = new System.Drawing.Point(6, 64);
            this.ux_labelPercentViable.Name = "ux_labelPercentViable";
            this.ux_labelPercentViable.Size = new System.Drawing.Size(79, 13);
            this.ux_labelPercentViable.TabIndex = 10;
            this.ux_labelPercentViable.Tag = "percent_viable";
            this.ux_labelPercentViable.Text = "Percent Viable:";
            // 
            // ux_textboxViabilityTestedDate
            // 
            this.ux_textboxViabilityTestedDate.Location = new System.Drawing.Point(139, 80);
            this.ux_textboxViabilityTestedDate.Name = "ux_textboxViabilityTestedDate";
            this.ux_textboxViabilityTestedDate.Size = new System.Drawing.Size(124, 20);
            this.ux_textboxViabilityTestedDate.TabIndex = 13;
            this.ux_textboxViabilityTestedDate.Tag = "viability_tested_date";
            // 
            // ux_labelViabilityTestedDate
            // 
            this.ux_labelViabilityTestedDate.AutoSize = true;
            this.ux_labelViabilityTestedDate.Location = new System.Drawing.Point(136, 64);
            this.ux_labelViabilityTestedDate.Name = "ux_labelViabilityTestedDate";
            this.ux_labelViabilityTestedDate.Size = new System.Drawing.Size(105, 13);
            this.ux_labelViabilityTestedDate.TabIndex = 12;
            this.ux_labelViabilityTestedDate.Tag = "viability_tested_date";
            this.ux_labelViabilityTestedDate.Text = "Viablity Tested Date:";
            // 
            // ux_groupboxInventoryData
            // 
            this.ux_groupboxInventoryData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxInventoryData.Controls.Add(this.ux_textboxInventoryID);
            this.ux_groupboxInventoryData.Controls.Add(this.ux_textboxViabilityTestedDate);
            this.ux_groupboxInventoryData.Controls.Add(this.ux_labelInventoryID);
            this.ux_groupboxInventoryData.Controls.Add(this.ux_labelViabilityTestedDate);
            this.ux_groupboxInventoryData.Controls.Add(this.ux_labelTaxonomySpeciesID);
            this.ux_groupboxInventoryData.Controls.Add(this.ux_textboxPercentViable);
            this.ux_groupboxInventoryData.Controls.Add(this.ux_textboxTaxonomySpeciesID);
            this.ux_groupboxInventoryData.Controls.Add(this.ux_labelPercentViable);
            this.ux_groupboxInventoryData.Controls.Add(this.ux_labelInventoryMaintPolicyID);
            this.ux_groupboxInventoryData.Controls.Add(this.ux_textboxInventoryMaintPolicyID);
            this.ux_groupboxInventoryData.Location = new System.Drawing.Point(12, 86);
            this.ux_groupboxInventoryData.Name = "ux_groupboxInventoryData";
            this.ux_groupboxInventoryData.Size = new System.Drawing.Size(440, 115);
            this.ux_groupboxInventoryData.TabIndex = 14;
            this.ux_groupboxInventoryData.TabStop = false;
            this.ux_groupboxInventoryData.Text = "Inventory Data";
            // 
            // ux_groupboxViabilityTestParameters
            // 
            this.ux_groupboxViabilityTestParameters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxViabilityTestParameters.Controls.Add(this.ux_comboboxViabilityRule);
            this.ux_groupboxViabilityTestParameters.Controls.Add(this.ux_checkboxShowAllRules);
            this.ux_groupboxViabilityTestParameters.Controls.Add(this.label1);
            this.ux_groupboxViabilityTestParameters.Controls.Add(this.ux_textboxSeedPerReplicate);
            this.ux_groupboxViabilityTestParameters.Controls.Add(this.ux_labelSeedPerReplicate);
            this.ux_groupboxViabilityTestParameters.Controls.Add(this.ux_textboxReplicates);
            this.ux_groupboxViabilityTestParameters.Controls.Add(this.ux_labelReplicates);
            this.ux_groupboxViabilityTestParameters.Location = new System.Drawing.Point(12, 208);
            this.ux_groupboxViabilityTestParameters.Name = "ux_groupboxViabilityTestParameters";
            this.ux_groupboxViabilityTestParameters.Size = new System.Drawing.Size(440, 93);
            this.ux_groupboxViabilityTestParameters.TabIndex = 15;
            this.ux_groupboxViabilityTestParameters.TabStop = false;
            this.ux_groupboxViabilityTestParameters.Text = "Viability Test Parameters";
            // 
            // ux_comboboxViabilityRule
            // 
            this.ux_comboboxViabilityRule.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_comboboxViabilityRule.FormattingEnabled = true;
            this.ux_comboboxViabilityRule.Location = new System.Drawing.Point(10, 38);
            this.ux_comboboxViabilityRule.Name = "ux_comboboxViabilityRule";
            this.ux_comboboxViabilityRule.Size = new System.Drawing.Size(298, 21);
            this.ux_comboboxViabilityRule.TabIndex = 19;
            this.ux_comboboxViabilityRule.Tag = "name";
            // 
            // ux_checkboxShowAllRules
            // 
            this.ux_checkboxShowAllRules.AutoSize = true;
            this.ux_checkboxShowAllRules.Enabled = false;
            this.ux_checkboxShowAllRules.Location = new System.Drawing.Point(10, 65);
            this.ux_checkboxShowAllRules.Name = "ux_checkboxShowAllRules";
            this.ux_checkboxShowAllRules.Size = new System.Drawing.Size(97, 17);
            this.ux_checkboxShowAllRules.TabIndex = 18;
            this.ux_checkboxShowAllRules.Text = "Show All Rules";
            this.ux_checkboxShowAllRules.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 14;
            this.label1.Tag = "name";
            this.label1.Text = "Viability Rule:";
            // 
            // ux_textboxSeedPerReplicate
            // 
            this.ux_textboxSeedPerReplicate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxSeedPerReplicate.Location = new System.Drawing.Point(377, 38);
            this.ux_textboxSeedPerReplicate.Name = "ux_textboxSeedPerReplicate";
            this.ux_textboxSeedPerReplicate.Size = new System.Drawing.Size(57, 20);
            this.ux_textboxSeedPerReplicate.TabIndex = 17;
            this.ux_textboxSeedPerReplicate.Tag = "seeds_per_replicate";
            this.ux_textboxSeedPerReplicate.Text = "50";
            // 
            // ux_labelSeedPerReplicate
            // 
            this.ux_labelSeedPerReplicate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelSeedPerReplicate.AutoSize = true;
            this.ux_labelSeedPerReplicate.Location = new System.Drawing.Point(374, 22);
            this.ux_labelSeedPerReplicate.Name = "ux_labelSeedPerReplicate";
            this.ux_labelSeedPerReplicate.Size = new System.Drawing.Size(40, 13);
            this.ux_labelSeedPerReplicate.TabIndex = 16;
            this.ux_labelSeedPerReplicate.Tag = "seeds_per_replicate";
            this.ux_labelSeedPerReplicate.Text = "Seeds:";
            // 
            // ux_textboxReplicates
            // 
            this.ux_textboxReplicates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxReplicates.Location = new System.Drawing.Point(314, 38);
            this.ux_textboxReplicates.Name = "ux_textboxReplicates";
            this.ux_textboxReplicates.Size = new System.Drawing.Size(57, 20);
            this.ux_textboxReplicates.TabIndex = 15;
            this.ux_textboxReplicates.Tag = "replicates";
            this.ux_textboxReplicates.Text = "4";
            // 
            // ux_labelReplicates
            // 
            this.ux_labelReplicates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelReplicates.AutoSize = true;
            this.ux_labelReplicates.Location = new System.Drawing.Point(311, 22);
            this.ux_labelReplicates.Name = "ux_labelReplicates";
            this.ux_labelReplicates.Size = new System.Drawing.Size(60, 13);
            this.ux_labelReplicates.TabIndex = 14;
            this.ux_labelReplicates.Tag = "replicates";
            this.ux_labelReplicates.Text = "Replicates:";
            // 
            // ux_bindingnavigatorInventory
            // 
            this.ux_bindingnavigatorInventory.AddNewItem = this.bindingNavigatorAddNewItem;
            this.ux_bindingnavigatorInventory.CountItem = this.bindingNavigatorCountItem;
            this.ux_bindingnavigatorInventory.DeleteItem = this.bindingNavigatorDeleteItem;
            this.ux_bindingnavigatorInventory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
            this.ux_bindingnavigatorInventory.Location = new System.Drawing.Point(0, 0);
            this.ux_bindingnavigatorInventory.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.ux_bindingnavigatorInventory.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.ux_bindingnavigatorInventory.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.ux_bindingnavigatorInventory.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.ux_bindingnavigatorInventory.Name = "ux_bindingnavigatorInventory";
            this.ux_bindingnavigatorInventory.PositionItem = this.bindingNavigatorPositionItem;
            this.ux_bindingnavigatorInventory.Size = new System.Drawing.Size(464, 25);
            this.ux_bindingnavigatorInventory.TabIndex = 16;
            this.ux_bindingnavigatorInventory.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Enabled = false;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.Visible = false;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Enabled = false;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            this.bindingNavigatorDeleteItem.Visible = false;
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // ux_buttonOK
            // 
            this.ux_buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ux_buttonOK.Location = new System.Drawing.Point(296, 317);
            this.ux_buttonOK.Name = "ux_buttonOK";
            this.ux_buttonOK.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonOK.TabIndex = 17;
            this.ux_buttonOK.Text = "OK";
            this.ux_buttonOK.UseVisualStyleBackColor = true;
            this.ux_buttonOK.Click += new System.EventHandler(this.ux_buttonOK_Click);
            // 
            // ux_buttonCancel
            // 
            this.ux_buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ux_buttonCancel.Location = new System.Drawing.Point(377, 317);
            this.ux_buttonCancel.Name = "ux_buttonCancel";
            this.ux_buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonCancel.TabIndex = 18;
            this.ux_buttonCancel.Text = "Cancel";
            this.ux_buttonCancel.UseVisualStyleBackColor = true;
            // 
            // ux_groupboxSearchCriteria
            // 
            this.ux_groupboxSearchCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxSearchCriteria.Controls.Add(this.ux_textboxOrderInventoryNumber);
            this.ux_groupboxSearchCriteria.Controls.Add(this.ux_labelOrderInventoryNumber);
            this.ux_groupboxSearchCriteria.Controls.Add(this.ux_buttonGo);
            this.ux_groupboxSearchCriteria.Location = new System.Drawing.Point(13, 28);
            this.ux_groupboxSearchCriteria.Name = "ux_groupboxSearchCriteria";
            this.ux_groupboxSearchCriteria.Size = new System.Drawing.Size(439, 52);
            this.ux_groupboxSearchCriteria.TabIndex = 19;
            this.ux_groupboxSearchCriteria.TabStop = false;
            this.ux_groupboxSearchCriteria.Text = "Search Criteria";
            // 
            // NewViabilityTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 352);
            this.Controls.Add(this.ux_groupboxSearchCriteria);
            this.Controls.Add(this.ux_buttonCancel);
            this.Controls.Add(this.ux_buttonOK);
            this.Controls.Add(this.ux_bindingnavigatorInventory);
            this.Controls.Add(this.ux_groupboxViabilityTestParameters);
            this.Controls.Add(this.ux_groupboxInventoryData);
            this.Name = "NewViabilityTestForm";
            this.Text = "New Viability Test";
            this.Load += new System.EventHandler(this.NewViabilityForm_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NewViabilityForm_KeyPress);
            this.ux_groupboxInventoryData.ResumeLayout(false);
            this.ux_groupboxInventoryData.PerformLayout();
            this.ux_groupboxViabilityTestParameters.ResumeLayout(false);
            this.ux_groupboxViabilityTestParameters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_bindingnavigatorInventory)).EndInit();
            this.ux_bindingnavigatorInventory.ResumeLayout(false);
            this.ux_bindingnavigatorInventory.PerformLayout();
            this.ux_groupboxSearchCriteria.ResumeLayout(false);
            this.ux_groupboxSearchCriteria.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ux_labelOrderInventoryNumber;
        private System.Windows.Forms.TextBox ux_textboxOrderInventoryNumber;
        private System.Windows.Forms.Button ux_buttonGo;
        private System.Windows.Forms.TextBox ux_textboxInventoryID;
        private System.Windows.Forms.Label ux_labelInventoryID;
        private System.Windows.Forms.TextBox ux_textboxTaxonomySpeciesID;
        private System.Windows.Forms.Label ux_labelTaxonomySpeciesID;
        private System.Windows.Forms.TextBox ux_textboxInventoryMaintPolicyID;
        private System.Windows.Forms.Label ux_labelInventoryMaintPolicyID;
        private System.Windows.Forms.TextBox ux_textboxPercentViable;
        private System.Windows.Forms.Label ux_labelPercentViable;
        private System.Windows.Forms.TextBox ux_textboxViabilityTestedDate;
        private System.Windows.Forms.Label ux_labelViabilityTestedDate;
        private System.Windows.Forms.GroupBox ux_groupboxInventoryData;
        private System.Windows.Forms.GroupBox ux_groupboxViabilityTestParameters;
        private System.Windows.Forms.TextBox ux_textboxSeedPerReplicate;
        private System.Windows.Forms.Label ux_labelSeedPerReplicate;
        private System.Windows.Forms.TextBox ux_textboxReplicates;
        private System.Windows.Forms.Label ux_labelReplicates;
        private System.Windows.Forms.BindingNavigator ux_bindingnavigatorInventory;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.Button ux_buttonOK;
        private System.Windows.Forms.Button ux_buttonCancel;
        private System.Windows.Forms.GroupBox ux_groupboxSearchCriteria;
        private System.Windows.Forms.CheckBox ux_checkboxShowAllRules;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ux_comboboxViabilityRule;
    }
}