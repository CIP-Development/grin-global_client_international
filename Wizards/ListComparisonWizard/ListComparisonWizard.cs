﻿using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ListComparisonWizard
{
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    public partial class ListComparisonWizard : Form, IGRINGlobalDataWizard
    {
        SharedUtils _sharedUtils;

        string _pathSeparator = "|";
        string _usernameCooperatorID;
        TabControl _ux_NavigatorTabControl = null;
        int _maxPathLength = 300;  // This is the column size of LIST_NAME in GG v1.0
        List<string> _list1 = new List<string>();
        List<string> _list2 = new List<string>();
        List<string> _listResult = new List<string>();
        TreeNode _treeviewList1 = null;
        TreeNode _treeviewList2 = null;
        TreeNode _treeviewResultList = null;

        public string FormName
        {
            get
            {
                return "List Wizard";
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();
                /*
                if (_changedRecords.Tables.Contains(_cooperator.TableName))
                {
                    dt = _changedRecords.Tables[_cooperator.TableName].Copy();
                }
                */
                return dt;
            }
        }

        public string PKeyName
        {
            get
            {
                return "accession_id";
            }
        }

        public ListComparisonWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();
            
            _sharedUtils = sharedUtils;
        }

        private DataTable GetUserItemList(int cooperatorID)
        {
            DataTable returnList = new DataTable();

            DataSet UserListItems = _sharedUtils.GetWebServiceData("get_lists", ":cooperatorid=" + cooperatorID.ToString(), 0, 0);
            if (UserListItems != null &&
                UserListItems.Tables.Contains("get_lists"))
            {
                // First get and save the max path length allowed for a user list item...
                if (UserListItems.Tables["get_lists"].Columns.Contains("LIST_NAME") &&
                    UserListItems.Tables["get_lists"].Columns["LIST_NAME"].ExtendedProperties.Contains("max_length"))
                {
                    int maxPathLength = -1;
                    if (int.TryParse(UserListItems.Tables["get_lists"].Columns["LIST_NAME"].ExtendedProperties["max_length"].ToString(), out maxPathLength))
                    {
                        _maxPathLength = maxPathLength;
                    }
                    else
                    {
                        _maxPathLength = 300;  // This is the column size of LIST_NAME in GG v1.0
                    }
                }

                returnList = UserListItems.Tables["get_lists"].Copy();

                foreach (DataRow dr in returnList.Rows)
                {
                    string[] propertyTokens = dr["PROPERTIES"].ToString().Split(';');
                    if (propertyTokens == null ||
                        propertyTokens.Length < 3 ||
                        !Regex.Match(propertyTokens[0], @"\s*\S+(?:_ID|_id)\s*").Success)
                    {
                        // Convert legacy sys_user_item_list records to new format...
                        switch (dr["ID_TYPE"].ToString().Trim().ToUpper())
                        {
                            case "ACCESSION_ID":
                                dr["PROPERTIES"] = "ACCESSION_ID; :accessionid=" + dr["ID_NUMBER"].ToString() + "; @accession.accession_id=" + dr["ID_NUMBER"].ToString() + "; " + dr["PROPERTIES"].ToString();
                                break;
                            case "INVENTORY_ID":
                                dr["PROPERTIES"] = "INVENTORY_ID; :inventoryid=" + dr["ID_NUMBER"].ToString() + "; @inventory.inventory_id=" + dr["ID_NUMBER"].ToString() + "; " + dr["PROPERTIES"].ToString();
                                break;
                            case "ORDER_REQUEST_ID":
                                dr["PROPERTIES"] = "ORDER_REQUEST_ID; :orderrequestid=" + dr["ID_NUMBER"].ToString() + "; @order_request.order_request_id=" + dr["ID_NUMBER"].ToString() + "; " + dr["PROPERTIES"].ToString();
                                break;
                            case "COOPERATOR_ID":
                                dr["PROPERTIES"] = "COOPERATOR_ID; :cooperatorid=" + dr["ID_NUMBER"].ToString() + "; @cooperator.cooperator_id=" + dr["ID_NUMBER"].ToString() + "; " + dr["PROPERTIES"].ToString();
                                break;
                            case "GEOGRAPHY_ID":
                                dr["PROPERTIES"] = "GEOGRAPHY_ID; :geographyid=" + dr["ID_NUMBER"].ToString() + "; @geography.geography_id=" + dr["ID_NUMBER"].ToString() + "; " + dr["PROPERTIES"].ToString();
                                break;
                            case "TAXONOMY_GENUS_ID":
                                dr["PROPERTIES"] = "TAXONOMY_GENUS_ID; :taxonomygenusid=" + dr["ID_NUMBER"].ToString() + "; @taxonomy_genus.taxonomy_genus_id=" + dr["ID_NUMBER"].ToString() + "; " + dr["PROPERTIES"].ToString();
                                break;
                            case "CROP_ID":
                                dr["PROPERTIES"] = "CROP_ID; :cropid=" + dr["ID_NUMBER"].ToString() + "; @crop.crop_id=" + dr["ID_NUMBER"].ToString() + "; " + dr["PROPERTIES"].ToString();
                                break;
                            case "FOLDER":
                                //if (!dr["PROPERTIES"].ToString().Contains("FOLDER; DYNAMIC_FOLDER_SEARCH_CRITERIA=;") &&
                                //    !dr["PROPERTIES"].ToString().Contains("QUERY; DYNAMIC_FOLDER_SEARCH_CRITERIA="))
                                if (!dr["PROPERTIES"].ToString().Contains("FOLDER; DYNAMIC_FOLDER_") &&
                                    !dr["PROPERTIES"].ToString().Contains("QUERY; DYNAMIC_FOLDER_"))
                                {
                                    dr["PROPERTIES"] = "FOLDER; DYNAMIC_FOLDER_SEARCH_CRITERIA=; DYNAMIC_FOLDER_RESOLVE_TO=; " + dr["PROPERTIES"].ToString();
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            UserListItems.Dispose();

            return returnList;
        }

        private TabPage BuildTabPageAndTreeView(string tabPageName, DataTable treeviewItems)
        {
            TabPage newTabPage = new TabPage();
            TreeView newTabPageTreeView = new TreeView();

            newTabPage.Text = tabPageName;
            newTabPage.Name = tabPageName;

            TreeView tvNew = new TreeView();
            tvNew.Name = newTabPage.Name + "TreeView";

            // Set the location and size of the tree view...
            tvNew.PathSeparator = _pathSeparator;
            tvNew.ImageList = BuildTreeviewImageList();
            tvNew.ImageKey = "inactive_unknown";
            tvNew.SelectedImageKey = "active_unknown";
            tvNew.Location = new System.Drawing.Point(0, 0);
            tvNew.Size = new System.Drawing.Size(newTabPage.Size.Width, newTabPage.Size.Height);
            tvNew.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
            tvNew.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            //tvNew.AfterSelect += new TreeViewEventHandler(this.treeView_AfterSelect);
            //tvNew.BeforeExpand += new TreeViewCancelEventHandler(this.treeView_BeforeExpand);
            //tvNew.AfterLabelEdit += new NodeLabelEditEventHandler(this.treeView_AfterLabelEdit);
            //tvNew.MouseClick += new MouseEventHandler(this.treeView_MouseClick);
            //tvNew.AllowDrop = true;
            //tvNew.ItemDrag += new ItemDragEventHandler(this.treeView_ItemDrag);
            //tvNew.DragOver += new DragEventHandler(this.treeView_DragOver);
            //tvNew.DragDrop += new DragEventHandler(this.treeView_DragDrop);
            //tvNew.BeforeExpand += new TreeViewCancelEventHandler(this.treeView_BeforeExpand);

            // Bind a context menu to the tree view...
            //tvNew.ContextMenuStrip = BuildTreeviewContextMenuStrip();
            

            if (treeviewItems != null &&
                treeviewItems.Rows.Count > 0)
            {
                // Create the treeview folders and items...
                treeviewItems.DefaultView.RowFilter = "TAB_NAME='" + tabPageName.Replace("'", "''") + "'";
                //treeviewItems.DefaultView.Sort = "app_user_item_list_id ASC";
                DataTable sortedTreeviewItems = treeviewItems.DefaultView.ToTable();

                // Step 1 - create the root folders...
                sortedTreeviewItems.DefaultView.RowFilter = "LIST_NAME='{DBNull.Value}' AND ID_TYPE='FOLDER'";
                sortedTreeviewItems.DefaultView.Sort = "LIST_NAME ASC, SORT_ORDER ASC";
                foreach (DataRowView drv in sortedTreeviewItems.DefaultView)
                {
                    TreeNode rootFolder = new TreeNode();
                    rootFolder.Name = drv["TITLE"].ToString();
                    rootFolder.Text = drv["TITLE"].ToString();
                    //rootFolder.Tag = "FOLDER";
                    rootFolder.Tag = drv["PROPERTIES"].ToString();
                    rootFolder.ImageKey = "inactive_folder";
                    rootFolder.SelectedImageKey = "active_folder";
                    if (rootFolder.Tag.ToString().Trim().ToUpper().StartsWith("QUERY"))
                    {
                        rootFolder.ImageKey = "inactive_dynamic_folder";
                        rootFolder.SelectedImageKey = "active_dynamic_folder";
                    }
                    rootFolder.ToolTipText = drv[0].ToString();
                    tvNew.Nodes.Add(rootFolder);
                }

                // Step 2 - create all remaining folders...
                sortedTreeviewItems.DefaultView.RowFilter = "LIST_NAME<>'{DBNull.Value}' AND ID_TYPE='FOLDER'";
                sortedTreeviewItems.DefaultView.Sort = "LIST_NAME ASC, SORT_ORDER ASC";
                foreach (DataRowView drv in sortedTreeviewItems.DefaultView)
                {
                    string[] nodePathNames = drv["LIST_NAME"].ToString().Split('|');
                    if (nodePathNames.Length > 0)
                    {
                        // Ensure the root folder is there...
                        if (!tvNew.Nodes.ContainsKey(nodePathNames[0]))
                        {
                            TreeNode rootFolder = new TreeNode();
                            rootFolder.Name = nodePathNames[0];
                            rootFolder.Text = nodePathNames[0];
                            rootFolder.Tag = "FOLDER; DYNAMIC_FOLDER_SEARCH_CRITERIA=; ";
                            rootFolder.ImageKey = "inactive_folder";
                            rootFolder.SelectedImageKey = "active_folder";
                            if (rootFolder.Tag.ToString().Trim().ToUpper().StartsWith("QUERY"))
                            {
                                rootFolder.ImageKey = "inactive_dynamic_folder";
                                rootFolder.SelectedImageKey = "active_dynamic_folder";
                            }
                            rootFolder.ToolTipText = "";
                            tvNew.Nodes.Add(rootFolder);
                        }
                        TreeNode currentNode = tvNew.Nodes[nodePathNames[0]];
                        for (int i = 1; i < nodePathNames.Length; i++)
                        {
                            // Ensure all other parent folders are there...
                            if (!currentNode.Nodes.ContainsKey(nodePathNames[i]))
                            {
                                // This sub-folder is missing so create it and make it the current folder...
                                TreeNode newFolder = new TreeNode();
                                newFolder.Name = nodePathNames[i];
                                newFolder.Text = nodePathNames[i];
                                //newFolder.Tag = "FOLDER";
                                newFolder.Tag = drv["PROPERTIES"].ToString();
                                newFolder.ImageKey = "inactive_folder";
                                newFolder.SelectedImageKey = "active_folder";
                                if (newFolder.Tag.ToString().Trim().ToUpper().StartsWith("QUERY"))
                                {
                                    newFolder.ImageKey = "inactive_dynamic_folder";
                                    newFolder.SelectedImageKey = "active_dynamic_folder";
                                }
                                //newFolder.ToolTipText = "-1";
                                newFolder.ToolTipText = "";
                                currentNode.Nodes.Add(newFolder);
                                currentNode = currentNode.Nodes[nodePathNames[i]];
                            }
                            else
                            {
                                // Found the folder - so make it the current folder...
                                currentNode = currentNode.Nodes[nodePathNames[i]];
                            }
                        }
                        // Create the new folder...
                        if (!currentNode.Nodes.ContainsKey(drv["TITLE"].ToString()))
                        {
                            TreeNode rootFolder = new TreeNode();
                            rootFolder.Name = drv["TITLE"].ToString();
                            rootFolder.Text = drv["TITLE"].ToString();
                            //rootFolder.Tag = "FOLDER";
                            rootFolder.Tag = drv["PROPERTIES"].ToString();
                            rootFolder.ImageKey = "inactive_folder";
                            rootFolder.SelectedImageKey = "active_folder";
                            if (rootFolder.Tag.ToString().Trim().ToUpper().StartsWith("QUERY"))
                            {
                                rootFolder.ImageKey = "inactive_dynamic_folder";
                                rootFolder.SelectedImageKey = "active_dynamic_folder";
                            }
                            rootFolder.ToolTipText = drv[0].ToString();
                            currentNode.Nodes.Add(rootFolder);

                            /* kz */
                            rootFolder.ContextMenuStrip = _ux_treeview_cms_Navigator;
                        }
                        else
                        {
                            // Found the folder - so make it the current folder...
                            currentNode = currentNode.Nodes[drv["TITLE"].ToString()];
                            // Now update all of the node properties to reflect the saved folder properties...
                            currentNode.Name = drv["TITLE"].ToString();
                            currentNode.Text = drv["TITLE"].ToString();
                            //rootFolder.Tag = "FOLDER";
                            currentNode.Tag = drv["PROPERTIES"].ToString();
                            currentNode.ImageKey = "inactive_folder";
                            currentNode.SelectedImageKey = "active_folder";
                            if (currentNode.Tag.ToString().Trim().ToUpper().StartsWith("QUERY"))
                            {
                                currentNode.ImageKey = "inactive_dynamic_folder";
                                currentNode.SelectedImageKey = "active_dynamic_folder";
                            }
                            currentNode.ToolTipText = drv[0].ToString();
                        }
                    }
                }

                // Step 3 - create all remaining items...
                sortedTreeviewItems.DefaultView.RowFilter = "ID_TYPE<>'FOLDER'";
                sortedTreeviewItems.DefaultView.Sort = "LIST_NAME ASC, SORT_ORDER ASC";
                foreach (DataRowView drv in sortedTreeviewItems.DefaultView)
                {
                    string[] nodePathNames = drv["LIST_NAME"].ToString().Split('|');
                    if (nodePathNames.Length > 0 &&
                        tvNew.Nodes.ContainsKey(nodePathNames[0]))
                    {
                        TreeNode currentNode = tvNew.Nodes[nodePathNames[0]];
                        for (int i = 1; i < nodePathNames.Length; i++)
                        {
                            if (currentNode != null &&
                                currentNode.Nodes.ContainsKey(nodePathNames[i]))
                            {
                                // Found the folder - so make it the current folder...
                                currentNode = currentNode.Nodes[nodePathNames[i]];
                            }
                            else
                            {
                                // For some reason the folder record does not exist - but this item references it so create it...
                                TreeNode newFolder = new TreeNode();
                                newFolder.Name = nodePathNames[i];
                                newFolder.Text = nodePathNames[i];
                                newFolder.Tag = "FOLDER; DYNAMIC_FOLDER_SEARCH_CRITERIA=; ";
                                newFolder.ImageKey = "inactive_folder";
                                newFolder.SelectedImageKey = "active_folder";
                                //newFolder.ToolTipText = "-1";
                                newFolder.ToolTipText = "";
                                currentNode.Nodes.Add(newFolder);
                                currentNode = currentNode.Nodes[nodePathNames[i]];
                            }
                        }

                        // Now that we are in the right folder - add the item...
                        TreeNode newNode = new TreeNode();

                        newNode.Name = drv["PROPERTIES"].ToString().Split(';')[1].Trim();
                        newNode.Text = drv["TITLE"].ToString();
                        newNode.Tag = drv["PROPERTIES"].ToString();
                        newNode.ImageKey = "inactive_" + drv["ID_TYPE"].ToString();
                        newNode.SelectedImageKey = "active_" + drv["ID_TYPE"].ToString();
                        newNode.ToolTipText = drv[0].ToString();

                        // Now check to see if virtual nodes are wanted for this new node...
                        //if(_virtualNodeTypes.ContainsKey(drv["PROPERTIES"].ToString().Split(';')[0].Trim().ToUpper()))


                        /*
                         * // kz no crear dummy items
                        if (!string.IsNullOrEmpty(_sharedUtils.GetAppSettingValue(drv["PROPERTIES"].ToString().Split(';')[0].Trim().ToUpper() + "_VIRTUAL_NODE_DATAVIEW")))
                        {
                            TreeNode dummyNode = new TreeNode();
                            string virtualQuery = _sharedUtils.GetAppSettingValue(drv["PROPERTIES"].ToString().Split(';')[0].Trim().ToUpper() + "_VIRTUAL_NODE_DATAVIEW") + ", ";
                            virtualQuery += drv["PROPERTIES"].ToString().Split(';')[1].Trim().ToLower();

                            dummyNode.Text = "!!DUMMYNODE!!";
                            dummyNode.Name = "!!DUMMYNODE!!"; ;
                            dummyNode.Tag = virtualQuery;
                            //dummyNode.ImageKey = "inactive_" + virtualQuery;
                            //dummyNode.SelectedImageKey = "active_" + virtualQuery;
                            newNode.Nodes.Add(dummyNode);
                        }
                        */
                        currentNode.Nodes.Add(newNode);
                    }
                }
            }
            else
            {
                // Build the root node...
                TreeNode tnNewRootNode = new TreeNode();
                tnNewRootNode.Name = "{DBNull.Value}";
                tnNewRootNode.Text = newTabPage.Text + " Root Folder";
                tnNewRootNode.Tag = "FOLDER; DYNAMIC_FOLDER_SEARCH_CRITERIA=; ";
                tnNewRootNode.ImageKey = "inactive_folder";
                tnNewRootNode.SelectedImageKey = "active_folder";
                tvNew.Nodes.Add(tnNewRootNode);
                // Build the first list node...
                TreeNode tnNewListNode = new TreeNode();
                tnNewListNode.Name = tnNewRootNode.FullPath;
                tnNewListNode.Text = "New List";
                tnNewListNode.Tag = "FOLDER; DYNAMIC_FOLDER_SEARCH_CRITERIA=; ";
                tnNewListNode.ImageKey = "inactive_folder";
                tnNewListNode.SelectedImageKey = "active_folder";
                //tnNewListNode.ToolTipText = "-1";
                tnNewListNode.ToolTipText = "";
                tnNewRootNode.Nodes.Add(tnNewListNode);
            }

            // Add the treeview to the tab...
            newTabPage.Controls.Add(tvNew);

            // Select the active node on the treeview by getting the last node saved in user settings...
            //            string selectedNodeFullPath = _sharedUtils.GetUserSetting(tvNew.Name, "SelectedNode.FullPath", "");
            //            SelectNodeByPath(selectedNodeFullPath, tvNew);

            return newTabPage;
        }


        public TabControl BuildTabControl(DataTable userItemList)
        {
            TabControl newTabControl = new TabControl();

            // Set the properties for the tab control...
            newTabControl.Name = "ux_tabcontrolGroupListNavigator";
            //newTabControl.ContextMenuStrip = BuildTabControlContextMenuStrip();
            newTabControl.AllowDrop = true;
            //newTabControl.DragOver += new DragEventHandler(ux_tabControl_DragOver);
            //newTabControl.DragDrop += new DragEventHandler(ux_tabControl_DragDrop);
            //newTabControl.MouseDown += new MouseEventHandler(ux_tabControl_MouseDown);
            newTabControl.SelectedIndexChanged += new EventHandler(ux_TabControl_SelectedIndexChanged);
            //newTabControl.ImageList = BuildTabControlImageList();

            // Create sorted list of user items...
            userItemList.DefaultView.Sort = "tab_name ASC, list_name ASC, sort_order ASC";
            //            userItemList.DefaultView.Sort = "app_user_item_list_id ASC";
            DataTable sortedUserItemList = userItemList.DefaultView.ToTable();

            // Get the names of the tabs from the user items list and iterate through them to create the tabs...
            // This is a neat trick to get the distinct values from a table (taking advantage of the distinct param of the ToTable method)...
            DataTable distinctTABNAME = userItemList.DefaultView.ToTable(true, "TAB_NAME");
            if (distinctTABNAME.Rows.Count > 0)
            {
                foreach (DataRow dr in distinctTABNAME.Rows)
                {
                    sortedUserItemList.DefaultView.RowFilter = "TAB_NAME='" + dr["TAB_NAME"].ToString().Replace("'", "''") + "'";
                    TabPage newTabPage = BuildTabPageAndTreeView(dr["TAB_NAME"].ToString(), sortedUserItemList.DefaultView.ToTable());
                    newTabControl.TabPages.Add(newTabPage);
                }
            }
            else
            {
                sortedUserItemList.DefaultView.RowFilter = "TAB_NAME='Tab 1'";
                TabPage newTabPage = BuildTabPageAndTreeView("Tab 1", sortedUserItemList.DefaultView.ToTable());
                newTabControl.TabPages.Add(newTabPage);
            }

            // Step 4 - cache a dictionary of tabpages to the tab control (for enabling tab page hide/show)
            //          and add each tab to the context menu 'Show Tab' list...
            Dictionary<string, TabPage> userTabPages = new Dictionary<string, TabPage>();
            foreach (TabPage tp in newTabControl.TabPages)
            {
                userTabPages.Add(tp.Name, tp);
                // Create the new tool strip menu item and bind it to the click event handler...
                //ToolStripMenuItem tsmiNew = new ToolStripMenuItem(tp.Text, null, ux_tab_tsmi_NavigatorShowTabsItem_Click, tp.Name);
                //tsmiNew.Checked = false;
                //((ToolStripMenuItem)newTabControl.ContextMenuStrip.Items["ux_tab_cms_NavigatorShowTab"]).DropDownItems.Add(tsmiNew);
            }
            newTabControl.Tag = userTabPages;

            // Step 5 - remove all tabpages and re-add them in the order the user prefers...
            // First get the user setting for visible tabs (and their order)...
            string userTabSettings = _sharedUtils.GetUserSetting("", newTabControl.Name, "TabPages.Order", "");
            // Then split the space delimited tabpage order list into an array...
            if (userTabSettings.Length > 0 
                 /* kz && ux_comboboxCNO.SelectedValue.ToString() == _usernameCooperatorID */
                )
            {
                string[] tabOrder = userTabSettings.Split(new string[] { _pathSeparator }, StringSplitOptions.None);
                // Remove all tabpages from the tab control...
                newTabControl.TabPages.Clear();
                // Now re-add the ones the user wants to see (in the preferred order)...
                foreach (string tabName in tabOrder)
                {
                    if (userTabPages.ContainsKey(tabName))
                    {
                        newTabControl.TabPages.Add(userTabPages[tabName]);
                        //((ToolStripMenuItem)((ToolStripMenuItem)newTabControl.ContextMenuStrip.Items["ux_tab_cms_NavigatorShowTab"]).DropDownItems[tabName]).Checked = true;
                    }
                }
                // If for some reason the user's visible tabs preference setting does not reference any tabs in the collection
                // there will be no tabs present in the control - so re-add all tabs to the control as a fail-safe measure...
                if (newTabControl.TabCount == 0)
                {
                    foreach (string tabName in userTabPages.Keys)
                    {
                        newTabControl.TabPages.Add(userTabPages[tabName]);
                        //((ToolStripMenuItem)((ToolStripMenuItem)newTabControl.ContextMenuStrip.Items["ux_tab_cms_NavigatorShowTab"]).DropDownItems[tabName]).Checked = true;
                    }
                }
            }
            else
            {
                // Since there is no user preference for hidden/shown tabs - leave them all as shown and update the checkmark on the list...
                foreach (TabPage tp in newTabControl.TabPages)
                {
                    //((ToolStripMenuItem)((ToolStripMenuItem)newTabControl.ContextMenuStrip.Items["ux_tab_cms_NavigatorShowTab"]).DropDownItems[tp.Name]).Checked = true;
                }
            }

            /* kz
            // Step 6 - create the 'New Tab...' tabpage and add it to the tabcontrol...
            if (!newTabControl.TabPages.ContainsKey("ux_tabpageGroupListNavigatorNewTab"))
            {
                TabPage tp = new TabPage();
                tp.Name = "ux_tabpageGroupListNavigatorNewTab";
                tp.Text = "...";
                tp.ToolTipText = "New Tab...";
                newTabControl.TabPages.Add(tp);
                // For some reason you have to add the tabpage to the tabcontrol *before* setting the imagekey
                // in order for the binding of the image to occur properly...
                tp.ImageKey = "new_tab";
                // Wire up the click event handler for this tab...
                
                //tp.Click += new System.EventHandler(this.ux_tab_cms_NavigatorNewTab_Click);
            }*/

            return newTabControl;
        }

        private void ux_TabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabControl tc = (TabControl)sender;
            ContextMenuStrip cms = tc.ContextMenuStrip;

            if (tc.SelectedIndex > -1)
            {
                if (tc.SelectedTab.Name == "ux_tabpageGroupListNavigatorNewTab")
                {
                    /*
                    int indexOfNewTab = tc.SelectedIndex;
                    NavigatorTabProperties newTabDialog = new NavigatorTabProperties(_pathSeparator, "_", _sharedUtils);
                    newTabDialog.StartPosition = FormStartPosition.CenterParent;
                    if (newTabDialog.ShowDialog(this) == DialogResult.OK)
                    {
                        string uniqueTabText = EnsureUniqueTabText(tc, newTabDialog.TabText);
                        TabPage tp = BuildTabPageAndTreeView(uniqueTabText, null);

                        if (tp != null)
                        {
                            tc.TabPages.Insert(indexOfNewTab, tp);
                            tc.SelectedIndex = indexOfNewTab;

                            // Create the new tool strip menu item and bind it to the click event handler...
                            ToolStripMenuItem tsmi = new ToolStripMenuItem(tp.Text, null, ux_tab_tsmi_NavigatorShowTabsItem_Click, tp.Text);
                            tsmi.Checked = true;
                            ((ToolStripMenuItem)cms.Items["ux_tab_cms_NavigatorShowTab"]).DropDownItems.Add(tsmi);

                            // Add this new tabpage to the tabcontrol's dictionary list (for hide/show functionality)...
                            if (tc != null &&
                                tc.Tag != null &&
                                tc.Tag.GetType() == typeof(Dictionary<string, TabPage>) &&
                                !((Dictionary<string, TabPage>)tc.Tag).ContainsKey(tp.Name))
                            {
                                ((Dictionary<string, TabPage>)tc.Tag).Add(tp.Name, tp);
                            }

                            if (((ToolStripMenuItem)cms.Items["ux_tab_cms_NavigatorShowTab"]).DropDownItems.Count > 1)
                            {
                                // We have more than one tab in the ShowTabs list so enable some menu choices...
                                ((ToolStripMenuItem)cms.Items["ux_tab_cms_NavigatorHideTab"]).Enabled = true;
                                ((ToolStripMenuItem)cms.Items["ux_tab_cms_NavigatorDeleteTab"]).Enabled = true;
                                ((ToolStripMenuItem)cms.Items["ux_tab_cms_NavigatorShowTab"]).Enabled = true;
                            }
                        }
                        else
                        {
                            tc.DeselectTab(indexOfNewTab);
                        }
                    }
                    else
                    {
                        tc.DeselectTab(indexOfNewTab);
                    }
                    */
                }
                else
                {
                    // Set focus on the treeview node for this treeview if it is not set...
                    foreach (Control ctrl in tc.SelectedTab.Controls)
                    {
                        if (ctrl.GetType() == typeof(TreeView))
                        {
                            TreeView tv = (TreeView)ctrl;
                            if (tv.SelectedNode == null)
                            {
                                RestoreActiveNode(tv);
                            }
                        }
                    }
                }
            }
        }

        private void RestoreActiveNode(TreeView tv)
        {
            TreeNode activeNode = null;
            string activeNodeFullPath = _sharedUtils.GetUserSetting("", tv.Name, "SelectedNode.FullPath", "");
            if (!string.IsNullOrEmpty(activeNodeFullPath))
            {
                string[] activeNodePathTokens = activeNodeFullPath.Trim().Split(new string[] { _pathSeparator }, StringSplitOptions.None);
                // Start the path from the treeview Nodes collection...
                if (activeNodePathTokens.Length > 0 &&
                    tv.Nodes.ContainsKey(activeNodePathTokens[0]))
                {
                    activeNode = tv.Nodes[activeNodePathTokens[0]];
                }
                // Iterate through the rest of the full path using the active Node object...
                for (int i = 1; i < activeNodePathTokens.Length; i++)
                {
                    if (activeNode != null &&
                        activeNode.Nodes.ContainsKey(activeNodePathTokens[i]))
                    {
                        activeNode = activeNode.Nodes[activeNodePathTokens[i]];
                    }
                }
            }
            if (activeNode != null)
            {
                tv.SelectedNode = activeNode;
            }
            else
            {
                tv.SelectedNode = tv.Nodes[0];
            }
        }

        private void ListComparisonWizard_Load(object sender, EventArgs e)
        {
            _usernameCooperatorID = _sharedUtils.UserCooperatorID;

            /* Populate comboboxOperator */
            List<KeyValuePair<string, string>> operators = new List<KeyValuePair<string, string>>();
            operators.Add(new KeyValuePair<string, string>("Except1", "List 1 not in List 2"));
            operators.Add(new KeyValuePair<string, string>("Except2", "List 2 not in List 1"));
            operators.Add(new KeyValuePair<string, string>("Intersect", "Intersect"));
            ux_comboboxOperator.ValueMember = "Key";
            ux_comboboxOperator.DisplayMember = "Value";
            ux_comboboxOperator.DataSource = operators;
            /* Populate TabControl and treeviews */

            // 1) get the treeview lists for the currently selected cooperator...
            DataTable userListItems = GetUserItemList(int.Parse(_sharedUtils.UserCooperatorID));
            // 2) Build the Navigator Tab Control with the new user item list...
            _ux_NavigatorTabControl = BuildTabControl(userListItems);

            TreeView tv = (TreeView)_ux_NavigatorTabControl.SelectedTab.Controls[_ux_NavigatorTabControl.SelectedTab.Name + "TreeView"];
            if (tv.SelectedNode == null)
            {
                RestoreActiveNode(tv);
            }

            // Place the Navigator TabControl in the left pane..
            ux_panelLeft.Controls.Clear();
            ux_panelLeft.Controls.Add(_ux_NavigatorTabControl);
            _ux_NavigatorTabControl.Size = ux_panelLeft.Size;
            _ux_NavigatorTabControl.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;

            // Get language translations
            if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);
        }

        private bool isFolderTreeNode(TreeNode tvn) {
            string[] currentPropertyTokens = tvn.Tag.ToString().Split(';');
            bool isFolder = false;
            foreach (string propertyToken in currentPropertyTokens)
            {
                if (propertyToken.Equals("FOLDER"))
                {
                    isFolder = true;
                    break;
                }
            }
            return isFolder;
        }

        private void ux_buttonPickList1_Click(object sender, EventArgs e)
        {
            TreeView tv = (TreeView)_ux_NavigatorTabControl.SelectedTab.Controls[_ux_NavigatorTabControl.SelectedTab.Name + "TreeView"];
            if (tv.SelectedNode != null)
            {
                //Clean
                ux_textboxList1.Text = "";
                _treeviewList1 = null;
                foreach (DataGridViewRow myRow in ux_datagridviewResult.Rows)
                {
                    myRow.Cells[0].Value = null;
                }
                //Validate folder node
                if (!isFolderTreeNode(tv.SelectedNode)) return;

                ux_textboxList1.Text = tv.SelectedNode.FullPath.Replace("|"," | ") + " ( "  + tv.SelectedNode.Nodes.Count + " items )" ;
                _treeviewList1 = tv.SelectedNode;

                if(ux_datagridviewResult.Rows.Count < tv.SelectedNode.Nodes.Count)
                    ux_datagridviewResult.Rows.Insert(ux_datagridviewResult.Rows.Count, tv.SelectedNode.Nodes.Count - ux_datagridviewResult.Rows.Count);
                
                int iRow = 0;
                _list1.Clear();
                foreach (TreeNode node in tv.SelectedNode.Nodes)
                {
                    StringBuilder sb = new StringBuilder(node.Text);
                    sb.Replace('_', ' ', 0, 4);
                    sb.Replace('_', '.');
                    _list1.Add(sb.ToString());
                    ux_datagridviewResult[0, iRow++].Value = sb.ToString();
                }
            }
        }

        private void ux_buttonPickList2_Click(object sender, EventArgs e)
        {
            TreeView tv = (TreeView)_ux_NavigatorTabControl.SelectedTab.Controls[_ux_NavigatorTabControl.SelectedTab.Name + "TreeView"];
            if (tv.SelectedNode != null)
            {
                //Clean
                ux_textboxList2.Text = "";
                _treeviewList2 = null;
                foreach (DataGridViewRow myRow in ux_datagridviewResult.Rows)
                {
                    myRow.Cells[1].Value = null;
                }

                //Validate folder node
                if (!isFolderTreeNode(tv.SelectedNode)) return;

                ux_textboxList2.Text = tv.SelectedNode.FullPath.Replace("|", " | ") + " ( " + tv.SelectedNode.Nodes.Count + " items )";
                _treeviewList2 = tv.SelectedNode;

                if (ux_datagridviewResult.Rows.Count < tv.SelectedNode.Nodes.Count)
                    ux_datagridviewResult.Rows.Insert(ux_datagridviewResult.Rows.Count, tv.SelectedNode.Nodes.Count - ux_datagridviewResult.Rows.Count);
                
                int iRow = 0;
                _list2.Clear();
                foreach (TreeNode node in tv.SelectedNode.Nodes)
                {
                    StringBuilder sb = new StringBuilder(node.Text);
                    sb.Replace('_', ' ', 0, 4);
                    sb.Replace('_', '.');
                    _list2.Add(sb.ToString());
                    ux_datagridviewResult[1, iRow++].Value = sb.ToString();
                }
            }
        }

        private void ux_buttonPickList3_Click(object sender, EventArgs e)
        {
            TreeView tv = (TreeView)_ux_NavigatorTabControl.SelectedTab.Controls[_ux_NavigatorTabControl.SelectedTab.Name + "TreeView"];
            if (tv.SelectedNode != null)
            {
                //Clean
                ux_textboxListResultPath.Text = "";
                _treeviewResultList = null;
                //Validate folder node
                if (!isFolderTreeNode(tv.SelectedNode)) return;

                ux_textboxListResultPath.Text = tv.SelectedNode.FullPath.Replace("|", " | ");
                _treeviewResultList = tv.SelectedNode;
            }
        }

        private ImageList BuildTreeviewImageList()
        {
            //if (_treeviewImageList == null)
            //{
                ImageList treeviewImageList = new ImageList();

                // Load the images for the tree view(s)...
                treeviewImageList.ColorDepth = ColorDepth.Depth32Bit;
                
                // Load the icons from the image sub-directrory (this directory is found under the directory the Curator Tool was launched from)...
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.IO.Directory.GetCurrentDirectory());
                System.IO.FileInfo[] iconFiles = di.GetFiles("Images\\TreeView\\*.ico", System.IO.SearchOption.TopDirectoryOnly);
                if (iconFiles != null && iconFiles.Length > 0)
                {
                    for (int i = 0; i < iconFiles.Length; i++)
                    {
                        //System.Reflection.Assembly newAssembly = System.Reflection.Assembly.LoadFile(iconFiles[i].FullName);
                        string junk = iconFiles[i].Name.TrimEnd('.', 'i', 'c', 'o');
                        treeviewImageList.Images.Add(iconFiles[i].Name.TrimEnd('.', 'i', 'c', 'o'), Icon.ExtractAssociatedIcon(iconFiles[i].FullName));
                    }
                }

            // Cache the list so you don't have to load it again...
            //_treeviewImageList = treeviewImageList;
            //}

            //return _treeviewImageList;
            return treeviewImageList;
        }

        private void ux_buttonCompare_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow myRow in ux_datagridviewResult.Rows)
            {
                myRow.Cells[2].Value = null;
            }

            _listResult = new List<string>();

            switch (ux_comboboxOperator.SelectedValue.ToString())
            {
                case "Except1":
                    _listResult = _list1.Except(_list2).ToList();
                    break;
                case "Except2":
                    _listResult = _list2.Except(_list1).ToList();
                    break;
                case "Intersect":
                    _listResult = _list1.Intersect(_list2).ToList();
                    break;
            }

            int iRow = 0;
            foreach (var name in _listResult)
            {
                ux_datagridviewResult[2, iRow++].Value = name;
            }
        }

        private void ux_buttonSave_Click(object sender, EventArgs e)
        {
            if (_treeviewResultList == null)
            {
                MessageBox.Show("New List Path is blank");
                return;
            }
            if (string.IsNullOrWhiteSpace(ux_textboxListResultName.Text))
            {
                MessageBox.Show("New List Name is blank");
                return;
            }
            if (_listResult == null || _listResult.Count == 0)
            {
                MessageBox.Show("The result of comparison is null");
                return;
            }

            if (_treeviewResultList != null && !string.IsNullOrWhiteSpace(ux_textboxListResultName.Text))
            {
                TreeView tv = (TreeView)_ux_NavigatorTabControl.SelectedTab.Controls[_ux_NavigatorTabControl.SelectedTab.Name + "TreeView"];
                TreeNode _treeviewNewList = null;
                TreeNode SourceList = null;
                
                switch (ux_comboboxOperator.SelectedValue.ToString())
                {
                    case "Except1":
                        SourceList = _treeviewList1;
                        break;
                    case "Except2":
                        SourceList = _treeviewList2;
                        break;
                    case "Intersect":
                        if(_list1.Count < _list2.Count)
                            SourceList = _treeviewList1;
                        else
                            SourceList = _treeviewList2;
                        break;
                }

                if (_listResult.Count > 0 && SourceList != null) {
                    _treeviewNewList = new TreeNode(ux_textboxListResultName.Text);
                    // Let's make sure the new node name is unique..
                    string newNodeText = EnsureUniqueNodeText(_treeviewResultList, _treeviewNewList);
                    ux_textboxListResultName.Text = newNodeText;

                    _treeviewNewList.Name = newNodeText;
                    _treeviewNewList.Text = newNodeText;
                    _treeviewNewList.Tag = "FOLDER; DYNAMIC_FOLDER_SEARCH_CRITERIA=; ";
                    _treeviewNewList.ImageKey = "inactive_folder";
                    _treeviewNewList.SelectedImageKey = "active_folder";
                    _treeviewResultList.Nodes.Add(_treeviewNewList);

                    foreach (TreeNode node in SourceList.Nodes)
                    {
                        StringBuilder sb = new StringBuilder(node.Text);
                        sb.Replace('_', ' ', 0, 4);
                        sb.Replace('_', '.');
                        if (_listResult.Contains(sb.ToString()))
                        {
                            _treeviewNewList.Nodes.Add((TreeNode)node.Clone());
                        }
                    }
                    tv.SelectedNode = _treeviewNewList;

                    //Save list to remote server
                    DataSet UserListItems = _sharedUtils.GetWebServiceData("get_lists", ":cooperatorid=-1", 0, 0);
                    if (UserListItems != null &&
                        UserListItems.Tables.Contains("get_lists"))
                    {
                        //Create Folder
                        DataRow dtFolder = UserListItems.Tables["get_lists"].NewRow();
                        dtFolder["cooperator_id"] = _sharedUtils.UserCooperatorID;
                        dtFolder["tab_name"] = _ux_NavigatorTabControl.SelectedTab.Text;
                        dtFolder["list_name"] = _treeviewResultList.FullPath;
                        dtFolder["id_number"] = DBNull.Value;
                        dtFolder["id_type"] = "FOLDER";
                        dtFolder["sort_order"] = _treeviewResultList.Nodes.Count - 1;
                        dtFolder["title"] = _treeviewNewList.Text;
                        dtFolder["description"] = DBNull.Value;
                        dtFolder["properties"] = _treeviewResultList.Tag.ToString();
                        dtFolder["created_date"] = DateTime.Now;
                        dtFolder["created_by"] = _sharedUtils.UserCooperatorID;
                        dtFolder["modified_date"] = DBNull.Value;
                        dtFolder["modified_by"] = DBNull.Value;
                        dtFolder["owned_date"] = DateTime.Now;
                        dtFolder["owned_by"] = _sharedUtils.UserCooperatorID;

                        UserListItems.Tables["get_lists"].Rows.Add(dtFolder);

                        //Create items
                        int sort = 0;
                        foreach (TreeNode node in _treeviewNewList.Nodes)
                        {
                            string[] currentPropertyTokens = node.Tag.ToString().Split(';'); //ACCESSION_ID;:accessionid=29245;@accession.accession_id=29245
                            int accession_id = -1;
                            foreach (string propertyToken in currentPropertyTokens)
                            {
                                if (propertyToken.Contains(":accessionid="))
                                {
                                    int.TryParse(propertyToken.Split('=')[1], out accession_id);
                                    break;
                                }
                            }
                            if (accession_id == -1) //Save error
                                continue;
                            DataRow dt = UserListItems.Tables["get_lists"].NewRow();
                            dt["cooperator_id"] = _sharedUtils.UserCooperatorID;
                            dt["tab_name"] = _ux_NavigatorTabControl.SelectedTab.Text;
                            dt["list_name"] = _treeviewNewList.FullPath;
                            dt["id_number"] = accession_id;
                            dt["id_type"] = "ACCESSION_ID";
                            dt["sort_order"] = sort++;
                            dt["title"] = node.Text;
                            dt["description"] = DBNull.Value;
                            dt["properties"] = node.Tag.ToString();
                            dt["created_date"] = DateTime.Now;
                            dt["created_by"] = _sharedUtils.UserCooperatorID;
                            dt["modified_date"] = DBNull.Value;
                            dt["modified_by"] = DBNull.Value;
                            dt["owned_date"] = DateTime.Now;
                            dt["owned_by"] = _sharedUtils.UserCooperatorID;

                            UserListItems.Tables["get_lists"].Rows.Add(dt);
                        }
                    }

                    DataSet saveErrors = new DataSet();
                    saveErrors = _sharedUtils.SaveWebServiceData(UserListItems);
                    if (saveErrors != null &&
                                saveErrors.Tables.Contains("get_lists"))
                    {
                        foreach (DataRow dr in saveErrors.Tables["get_lists"].Rows)
                        {
                            if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                            {
                                MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                    , dr["TITLE"].ToString(), dr["ExceptionMessage"].ToString()));
                            }
                            else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                            {
                                MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}"
                                    , dr["TITLE"].ToString(), dr["ExceptionMessage"].ToString()));
                            }
                            else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() != "Success")
                            {
                                MessageBox.Show(string.Format("The {0} item could not be successfully deleted from your list.\n\nError message:\n\n{1}"
                                    , dr["TITLE"].ToString(), dr["ExceptionMessage"].ToString()));
                            }
                        }
                    }
                }
            }   
        }

        private string EnsureUniqueNodeText(TreeNode destinationTreeNode, TreeNode newNode)
        {
            // Now we can get a unique name for this tree node (starting with the default node name passed in)...
            String uniqueNodeText = newNode.Text;

            // Let's make sure the new node name is unique..
            bool duplicateText = true;
            int i = 1;
            while (duplicateText)
            {
                // Assume this name is unique (until proven otherwise)
                duplicateText = false;
                foreach (TreeNode tn in destinationTreeNode.Nodes)
                {
                    if (tn.Text.Equals(uniqueNodeText) && tn != newNode)
                    {
                        // Nope...  This is not a unique node name so increment a counter until it is unique...
                        uniqueNodeText = newNode.Text + " (" + i++.ToString() + ")";
                        duplicateText = true;
                    }
                }
            }
            return uniqueNodeText;
        }

        private void ux_treeview_cms_NavigatorList1_Click(object sender, EventArgs e)
        {
            ux_buttonPickList1_Click(null, null);
        }

        private void ux_treeview_cms_NavigatorList2_Click(object sender, EventArgs e)
        {
            ux_buttonPickList2_Click(null, null);
        }

        private void ux_treeview_cms_NavigatorListResult_Click(object sender, EventArgs e)
        {
            ux_buttonPickList3_Click(null, null);
        }

        //end_class
    }
}
