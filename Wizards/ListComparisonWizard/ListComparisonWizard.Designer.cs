﻿namespace ListComparisonWizard
{
    partial class ListComparisonWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ux_labelList1 = new System.Windows.Forms.Label();
            this.ux_labelList2 = new System.Windows.Forms.Label();
            this.ux_comboboxOperator = new System.Windows.Forms.ComboBox();
            this.ux_labelOperator = new System.Windows.Forms.Label();
            this.ux_labelResult = new System.Windows.Forms.Label();
            this.ux_textboxListResultPath = new System.Windows.Forms.TextBox();
            this.ux_buttonCompare = new System.Windows.Forms.Button();
            this.ux_panelContent = new System.Windows.Forms.Panel();
            this.ux_labelMessage = new System.Windows.Forms.Label();
            this.ux_textboxListResultName = new System.Windows.Forms.TextBox();
            this.ux_labelListResultName = new System.Windows.Forms.Label();
            this.ux_textboxList2 = new System.Windows.Forms.TextBox();
            this.ux_textboxList1 = new System.Windows.Forms.TextBox();
            this.ux_buttonSave = new System.Windows.Forms.Button();
            this.ux_datagridviewResult = new System.Windows.Forms.DataGridView();
            this.List1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.List2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Result = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ux_buttonPickList3 = new System.Windows.Forms.Button();
            this.ux_buttonPickList2 = new System.Windows.Forms.Button();
            this.ux_buttonPickList1 = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ux_panelLeft = new System.Windows.Forms.Panel();
            this._ux_treeview_cms_Navigator = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ux_treeview_cms_NavigatorList1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_treeview_cms_NavigatorList2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_treeview_cms_NavigatorListResult = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_panelContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this._ux_treeview_cms_Navigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_labelList1
            // 
            this.ux_labelList1.AutoSize = true;
            this.ux_labelList1.Location = new System.Drawing.Point(8, 21);
            this.ux_labelList1.Name = "ux_labelList1";
            this.ux_labelList1.Size = new System.Drawing.Size(41, 13);
            this.ux_labelList1.TabIndex = 0;
            this.ux_labelList1.Text = "List 1 : ";
            // 
            // ux_labelList2
            // 
            this.ux_labelList2.AutoSize = true;
            this.ux_labelList2.Location = new System.Drawing.Point(8, 48);
            this.ux_labelList2.Name = "ux_labelList2";
            this.ux_labelList2.Size = new System.Drawing.Size(41, 13);
            this.ux_labelList2.TabIndex = 2;
            this.ux_labelList2.Text = "List 2 : ";
            // 
            // ux_comboboxOperator
            // 
            this.ux_comboboxOperator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_comboboxOperator.FormattingEnabled = true;
            this.ux_comboboxOperator.Location = new System.Drawing.Point(130, 72);
            this.ux_comboboxOperator.Name = "ux_comboboxOperator";
            this.ux_comboboxOperator.Size = new System.Drawing.Size(427, 21);
            this.ux_comboboxOperator.TabIndex = 5;
            // 
            // ux_labelOperator
            // 
            this.ux_labelOperator.AutoSize = true;
            this.ux_labelOperator.Location = new System.Drawing.Point(8, 75);
            this.ux_labelOperator.Name = "ux_labelOperator";
            this.ux_labelOperator.Size = new System.Drawing.Size(110, 13);
            this.ux_labelOperator.TabIndex = 4;
            this.ux_labelOperator.Text = "Comparison Method : ";
            // 
            // ux_labelResult
            // 
            this.ux_labelResult.AutoSize = true;
            this.ux_labelResult.Location = new System.Drawing.Point(8, 102);
            this.ux_labelResult.Name = "ux_labelResult";
            this.ux_labelResult.Size = new System.Drawing.Size(82, 13);
            this.ux_labelResult.TabIndex = 6;
            this.ux_labelResult.Text = "New List Path : ";
            // 
            // ux_textboxListResultPath
            // 
            this.ux_textboxListResultPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxListResultPath.Location = new System.Drawing.Point(130, 99);
            this.ux_textboxListResultPath.Name = "ux_textboxListResultPath";
            this.ux_textboxListResultPath.ReadOnly = true;
            this.ux_textboxListResultPath.Size = new System.Drawing.Size(427, 20);
            this.ux_textboxListResultPath.TabIndex = 7;
            // 
            // ux_buttonCompare
            // 
            this.ux_buttonCompare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCompare.AutoSize = true;
            this.ux_buttonCompare.Location = new System.Drawing.Point(11, 152);
            this.ux_buttonCompare.Name = "ux_buttonCompare";
            this.ux_buttonCompare.Size = new System.Drawing.Size(102, 23);
            this.ux_buttonCompare.TabIndex = 8;
            this.ux_buttonCompare.Text = "Compare Lists";
            this.ux_buttonCompare.UseVisualStyleBackColor = true;
            this.ux_buttonCompare.Click += new System.EventHandler(this.ux_buttonCompare_Click);
            // 
            // ux_panelContent
            // 
            this.ux_panelContent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ux_panelContent.Controls.Add(this.ux_labelMessage);
            this.ux_panelContent.Controls.Add(this.ux_textboxListResultName);
            this.ux_panelContent.Controls.Add(this.ux_labelListResultName);
            this.ux_panelContent.Controls.Add(this.ux_textboxList2);
            this.ux_panelContent.Controls.Add(this.ux_textboxList1);
            this.ux_panelContent.Controls.Add(this.ux_buttonSave);
            this.ux_panelContent.Controls.Add(this.ux_datagridviewResult);
            this.ux_panelContent.Controls.Add(this.ux_buttonPickList3);
            this.ux_panelContent.Controls.Add(this.ux_buttonPickList2);
            this.ux_panelContent.Controls.Add(this.ux_buttonPickList1);
            this.ux_panelContent.Controls.Add(this.ux_comboboxOperator);
            this.ux_panelContent.Controls.Add(this.ux_labelList1);
            this.ux_panelContent.Controls.Add(this.ux_buttonCompare);
            this.ux_panelContent.Controls.Add(this.ux_textboxListResultPath);
            this.ux_panelContent.Controls.Add(this.ux_labelList2);
            this.ux_panelContent.Controls.Add(this.ux_labelResult);
            this.ux_panelContent.Controls.Add(this.ux_labelOperator);
            this.ux_panelContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_panelContent.Location = new System.Drawing.Point(0, 0);
            this.ux_panelContent.Name = "ux_panelContent";
            this.ux_panelContent.Size = new System.Drawing.Size(677, 571);
            this.ux_panelContent.TabIndex = 9;
            // 
            // ux_labelMessage
            // 
            this.ux_labelMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ux_labelMessage.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ux_labelMessage.Location = new System.Drawing.Point(0, 549);
            this.ux_labelMessage.Name = "ux_labelMessage";
            this.ux_labelMessage.Size = new System.Drawing.Size(673, 18);
            this.ux_labelMessage.TabIndex = 18;
            this.ux_labelMessage.Text = "To see new lists in Curator Tool, It\'s necessary to refresh \"List Panel\" by selec" +
    "ting your username in users dropdownlist";
            // 
            // ux_textboxListResultName
            // 
            this.ux_textboxListResultName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxListResultName.Location = new System.Drawing.Point(130, 125);
            this.ux_textboxListResultName.Name = "ux_textboxListResultName";
            this.ux_textboxListResultName.Size = new System.Drawing.Size(427, 20);
            this.ux_textboxListResultName.TabIndex = 17;
            // 
            // ux_labelListResultName
            // 
            this.ux_labelListResultName.AutoSize = true;
            this.ux_labelListResultName.Location = new System.Drawing.Point(8, 128);
            this.ux_labelListResultName.Name = "ux_labelListResultName";
            this.ux_labelListResultName.Size = new System.Drawing.Size(88, 13);
            this.ux_labelListResultName.TabIndex = 16;
            this.ux_labelListResultName.Text = "New List Name : ";
            // 
            // ux_textboxList2
            // 
            this.ux_textboxList2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxList2.Location = new System.Drawing.Point(130, 45);
            this.ux_textboxList2.Name = "ux_textboxList2";
            this.ux_textboxList2.ReadOnly = true;
            this.ux_textboxList2.Size = new System.Drawing.Size(427, 20);
            this.ux_textboxList2.TabIndex = 15;
            // 
            // ux_textboxList1
            // 
            this.ux_textboxList1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxList1.Location = new System.Drawing.Point(130, 18);
            this.ux_textboxList1.Name = "ux_textboxList1";
            this.ux_textboxList1.ReadOnly = true;
            this.ux_textboxList1.Size = new System.Drawing.Size(427, 20);
            this.ux_textboxList1.TabIndex = 14;
            // 
            // ux_buttonSave
            // 
            this.ux_buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSave.AutoSize = true;
            this.ux_buttonSave.Location = new System.Drawing.Point(561, 123);
            this.ux_buttonSave.Name = "ux_buttonSave";
            this.ux_buttonSave.Size = new System.Drawing.Size(102, 23);
            this.ux_buttonSave.TabIndex = 13;
            this.ux_buttonSave.Text = "Create List";
            this.ux_buttonSave.UseVisualStyleBackColor = true;
            this.ux_buttonSave.Click += new System.EventHandler(this.ux_buttonSave_Click);
            // 
            // ux_datagridviewResult
            // 
            this.ux_datagridviewResult.AllowUserToAddRows = false;
            this.ux_datagridviewResult.AllowUserToDeleteRows = false;
            this.ux_datagridviewResult.AllowUserToOrderColumns = true;
            this.ux_datagridviewResult.AllowUserToResizeRows = false;
            this.ux_datagridviewResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_datagridviewResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.List1,
            this.List2,
            this.Result});
            this.ux_datagridviewResult.Location = new System.Drawing.Point(3, 181);
            this.ux_datagridviewResult.Name = "ux_datagridviewResult";
            this.ux_datagridviewResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.ColumnHeaderSelect;
            this.ux_datagridviewResult.Size = new System.Drawing.Size(667, 365);
            this.ux_datagridviewResult.TabIndex = 12;
            // 
            // List1
            // 
            this.List1.HeaderText = "List 1";
            this.List1.Name = "List1";
            this.List1.ReadOnly = true;
            this.List1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // List2
            // 
            this.List2.HeaderText = "List 2";
            this.List2.Name = "List2";
            this.List2.ReadOnly = true;
            this.List2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Result
            // 
            this.Result.HeaderText = "Result";
            this.Result.Name = "Result";
            this.Result.ReadOnly = true;
            this.Result.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ux_buttonPickList3
            // 
            this.ux_buttonPickList3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonPickList3.Location = new System.Drawing.Point(561, 97);
            this.ux_buttonPickList3.Name = "ux_buttonPickList3";
            this.ux_buttonPickList3.Size = new System.Drawing.Size(102, 23);
            this.ux_buttonPickList3.TabIndex = 11;
            this.ux_buttonPickList3.Text = "Pick";
            this.ux_buttonPickList3.UseVisualStyleBackColor = true;
            this.ux_buttonPickList3.Click += new System.EventHandler(this.ux_buttonPickList3_Click);
            // 
            // ux_buttonPickList2
            // 
            this.ux_buttonPickList2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonPickList2.Location = new System.Drawing.Point(561, 43);
            this.ux_buttonPickList2.Name = "ux_buttonPickList2";
            this.ux_buttonPickList2.Size = new System.Drawing.Size(102, 23);
            this.ux_buttonPickList2.TabIndex = 10;
            this.ux_buttonPickList2.Text = "Pick";
            this.ux_buttonPickList2.UseVisualStyleBackColor = true;
            this.ux_buttonPickList2.Click += new System.EventHandler(this.ux_buttonPickList2_Click);
            // 
            // ux_buttonPickList1
            // 
            this.ux_buttonPickList1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonPickList1.Location = new System.Drawing.Point(561, 15);
            this.ux_buttonPickList1.Name = "ux_buttonPickList1";
            this.ux_buttonPickList1.Size = new System.Drawing.Size(102, 23);
            this.ux_buttonPickList1.TabIndex = 9;
            this.ux_buttonPickList1.Text = "Pick";
            this.ux_buttonPickList1.UseVisualStyleBackColor = true;
            this.ux_buttonPickList1.Click += new System.EventHandler(this.ux_buttonPickList1_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ux_panelLeft);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ux_panelContent);
            this.splitContainer1.Size = new System.Drawing.Size(895, 571);
            this.splitContainer1.SplitterDistance = 214;
            this.splitContainer1.TabIndex = 10;
            // 
            // ux_panelLeft
            // 
            this.ux_panelLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ux_panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_panelLeft.Location = new System.Drawing.Point(0, 0);
            this.ux_panelLeft.Name = "ux_panelLeft";
            this.ux_panelLeft.Size = new System.Drawing.Size(214, 571);
            this.ux_panelLeft.TabIndex = 11;
            // 
            // _ux_treeview_cms_Navigator
            // 
            this._ux_treeview_cms_Navigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ux_treeview_cms_NavigatorList1,
            this.ux_treeview_cms_NavigatorList2,
            this.ux_treeview_cms_NavigatorListResult});
            this._ux_treeview_cms_Navigator.Name = "_ux_treeview_cms_Navigator";
            this._ux_treeview_cms_Navigator.Size = new System.Drawing.Size(186, 70);
            // 
            // ux_treeview_cms_NavigatorList1
            // 
            this.ux_treeview_cms_NavigatorList1.Name = "ux_treeview_cms_NavigatorList1";
            this.ux_treeview_cms_NavigatorList1.Size = new System.Drawing.Size(185, 22);
            this.ux_treeview_cms_NavigatorList1.Text = "Pick as List 1";
            this.ux_treeview_cms_NavigatorList1.Click += new System.EventHandler(this.ux_treeview_cms_NavigatorList1_Click);
            // 
            // ux_treeview_cms_NavigatorList2
            // 
            this.ux_treeview_cms_NavigatorList2.Name = "ux_treeview_cms_NavigatorList2";
            this.ux_treeview_cms_NavigatorList2.Size = new System.Drawing.Size(185, 22);
            this.ux_treeview_cms_NavigatorList2.Text = "Pick as List 2";
            this.ux_treeview_cms_NavigatorList2.Click += new System.EventHandler(this.ux_treeview_cms_NavigatorList2_Click);
            // 
            // ux_treeview_cms_NavigatorListResult
            // 
            this.ux_treeview_cms_NavigatorListResult.Name = "ux_treeview_cms_NavigatorListResult";
            this.ux_treeview_cms_NavigatorListResult.Size = new System.Drawing.Size(185, 22);
            this.ux_treeview_cms_NavigatorListResult.Text = "Pick as New List Path";
            this.ux_treeview_cms_NavigatorListResult.Click += new System.EventHandler(this.ux_treeview_cms_NavigatorListResult_Click);
            // 
            // ListComparisonWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 571);
            this.Controls.Add(this.splitContainer1);
            this.Name = "ListComparisonWizard";
            this.Text = "List Wizard";
            this.Load += new System.EventHandler(this.ListComparisonWizard_Load);
            this.ux_panelContent.ResumeLayout(false);
            this.ux_panelContent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewResult)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this._ux_treeview_cms_Navigator.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label ux_labelList1;
        private System.Windows.Forms.Label ux_labelList2;
        private System.Windows.Forms.ComboBox ux_comboboxOperator;
        private System.Windows.Forms.Label ux_labelOperator;
        private System.Windows.Forms.Label ux_labelResult;
        private System.Windows.Forms.TextBox ux_textboxListResultPath;
        private System.Windows.Forms.Button ux_buttonCompare;
        private System.Windows.Forms.Panel ux_panelContent;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel ux_panelLeft;
        private System.Windows.Forms.Button ux_buttonPickList3;
        private System.Windows.Forms.Button ux_buttonPickList2;
        private System.Windows.Forms.Button ux_buttonPickList1;
        private System.Windows.Forms.DataGridView ux_datagridviewResult;
        private System.Windows.Forms.Button ux_buttonSave;
        private System.Windows.Forms.ContextMenuStrip _ux_treeview_cms_Navigator;
        private System.Windows.Forms.ToolStripMenuItem ux_treeview_cms_NavigatorList1;
        private System.Windows.Forms.ToolStripMenuItem ux_treeview_cms_NavigatorList2;
        private System.Windows.Forms.ToolStripMenuItem ux_treeview_cms_NavigatorListResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn List1;
        private System.Windows.Forms.DataGridViewTextBoxColumn List2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Result;
        private System.Windows.Forms.TextBox ux_textboxList2;
        private System.Windows.Forms.TextBox ux_textboxList1;
        private System.Windows.Forms.TextBox ux_textboxListResultName;
        private System.Windows.Forms.Label ux_labelListResultName;
        private System.Windows.Forms.Label ux_labelMessage;
    }
}

