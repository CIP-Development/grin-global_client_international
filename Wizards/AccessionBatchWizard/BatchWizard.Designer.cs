﻿namespace AccessionBatchWizard
{
    partial class BatchWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BatchWizard));
            this.ux_Button_Close = new System.Windows.Forms.Button();
            this.ux_Button_Add_Descriptor = new System.Windows.Forms.Button();
            this.ux_listBoxCategory = new System.Windows.Forms.ListBox();
            this.ux_listBoxTrait = new System.Windows.Forms.ListBox();
            this.ux_groupBox_Descriptors = new System.Windows.Forms.GroupBox();
            this.ux_textboxFindDescriptor = new System.Windows.Forms.TextBox();
            this.ux_groupBox_SelectedDescriptors = new System.Windows.Forms.Label();
            this.ux_labelDescriptor = new System.Windows.Forms.Label();
            this.ux_labelCategory = new System.Windows.Forms.Label();
            this.ux_labelCrop = new System.Windows.Forms.Label();
            this.ux_listBoxCrop = new System.Windows.Forms.ListBox();
            this.ux_buttonRemoveDescriptor = new System.Windows.Forms.Button();
            this.ux_listBoxDescriptors = new System.Windows.Forms.ListBox();
            this.ux_buttonShowDefinition = new System.Windows.Forms.Button();
            this.ux_progressBarWorking = new System.Windows.Forms.ProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ux_labelMessage = new System.Windows.Forms.Label();
            this.ux_buttonValidateData = new System.Windows.Forms.Button();
            this.ux_buttonClear = new System.Windows.Forms.Button();
            this.ux_buttonSaveData = new System.Windows.Forms.Button();
            this.ux_buttonTemplate = new System.Windows.Forms.Button();
            this.ux_datagridviewSheet = new System.Windows.Forms.DataGridView();
            this.ux_groupboxSheet = new System.Windows.Forms.GroupBox();
            this.ux_checkBoxMatchCase = new System.Windows.Forms.CheckBox();
            this.ux_buttonExportExcel = new System.Windows.Forms.Button();
            this.ux_labelCellInfo = new System.Windows.Forms.Label();
            this.ux_groupBoxSavingSummary = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ux_labelSavingUnmodifiedCount = new System.Windows.Forms.Label();
            this.ux_pictureBoxSavingUnmodified = new System.Windows.Forms.PictureBox();
            this.ux_labelSavingUnmodified = new System.Windows.Forms.Label();
            this.ux_pictureBoxSavingOk = new System.Windows.Forms.PictureBox();
            this.ux_pictureBoxSavingError = new System.Windows.Forms.PictureBox();
            this.ux_labelSavingOk = new System.Windows.Forms.Label();
            this.ux_labelSavingError = new System.Windows.Forms.Label();
            this.ux_labelSavingOkCount = new System.Windows.Forms.Label();
            this.ux_labelSavingErrorCount = new System.Windows.Forms.Label();
            this.ux_groupBoxValidationSummary = new System.Windows.Forms.GroupBox();
            this.ux_tableLayoutPanelSummary = new System.Windows.Forms.TableLayoutPanel();
            this.ux_labelValidationOkWithoutChangesCount = new System.Windows.Forms.Label();
            this.ux_pictureBoxValidationOkWithoutChanges = new System.Windows.Forms.PictureBox();
            this.ux_labelValidationOkWithoutChanges = new System.Windows.Forms.Label();
            this.ux_pictureBoxValidationOkWithChanges = new System.Windows.Forms.PictureBox();
            this.ux_pictureBoxValidationError = new System.Windows.Forms.PictureBox();
            this.ux_labelValidationOkWithChanges = new System.Windows.Forms.Label();
            this.ux_labelValidationError = new System.Windows.Forms.Label();
            this.ux_labelValidationOkWithChangesCount = new System.Windows.Forms.Label();
            this.ux_labelValidationErrorCount = new System.Windows.Forms.Label();
            this.ux_labelRows = new System.Windows.Forms.Label();
            this.ux_comboboxMethod = new System.Windows.Forms.ComboBox();
            this.ux_checkBoxShowOnlyRowsWithError = new System.Windows.Forms.CheckBox();
            this.ux_checkboxIsArchived = new System.Windows.Forms.CheckBox();
            this.ux_labelMethod = new System.Windows.Forms.Label();
            this.contextMenuStripMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showDBValuetoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.loadCurrentValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.copyHeadersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateLookupTablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showHiddenColumnsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeAllColumnsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_groupBox_Descriptors.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewSheet)).BeginInit();
            this.ux_groupboxSheet.SuspendLayout();
            this.ux_groupBoxSavingSummary.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxSavingUnmodified)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxSavingOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxSavingError)).BeginInit();
            this.ux_groupBoxValidationSummary.SuspendLayout();
            this.ux_tableLayoutPanelSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxValidationOkWithoutChanges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxValidationOkWithChanges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxValidationError)).BeginInit();
            this.contextMenuStripMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_Button_Close
            // 
            this.ux_Button_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Button_Close.Location = new System.Drawing.Point(834, 6);
            this.ux_Button_Close.Name = "ux_Button_Close";
            this.ux_Button_Close.Size = new System.Drawing.Size(90, 23);
            this.ux_Button_Close.TabIndex = 0;
            this.ux_Button_Close.Text = "Close";
            this.ux_Button_Close.UseVisualStyleBackColor = true;
            this.ux_Button_Close.Click += new System.EventHandler(this.ux_Button_Close_Click);
            // 
            // ux_Button_Add_Descriptor
            // 
            this.ux_Button_Add_Descriptor.Location = new System.Drawing.Point(344, 166);
            this.ux_Button_Add_Descriptor.Name = "ux_Button_Add_Descriptor";
            this.ux_Button_Add_Descriptor.Size = new System.Drawing.Size(116, 23);
            this.ux_Button_Add_Descriptor.TabIndex = 2;
            this.ux_Button_Add_Descriptor.Text = "Add Descriptor";
            this.ux_Button_Add_Descriptor.UseVisualStyleBackColor = true;
            this.ux_Button_Add_Descriptor.Click += new System.EventHandler(this.ux_Button_Add_Descriptor_Click);
            // 
            // ux_listBoxCategory
            // 
            this.ux_listBoxCategory.FormattingEnabled = true;
            this.ux_listBoxCategory.Location = new System.Drawing.Point(173, 39);
            this.ux_listBoxCategory.Name = "ux_listBoxCategory";
            this.ux_listBoxCategory.Size = new System.Drawing.Size(155, 121);
            this.ux_listBoxCategory.TabIndex = 9;
            this.ux_listBoxCategory.SelectedIndexChanged += new System.EventHandler(this.ux_listBoxCategory_SelectedIndexChanged);
            // 
            // ux_listBoxTrait
            // 
            this.ux_listBoxTrait.FormattingEnabled = true;
            this.ux_listBoxTrait.Location = new System.Drawing.Point(344, 39);
            this.ux_listBoxTrait.Name = "ux_listBoxTrait";
            this.ux_listBoxTrait.Size = new System.Drawing.Size(282, 121);
            this.ux_listBoxTrait.TabIndex = 10;
            this.ux_listBoxTrait.DoubleClick += new System.EventHandler(this.ux_listBoxTrait_DoubleClick);
            // 
            // ux_groupBox_Descriptors
            // 
            this.ux_groupBox_Descriptors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_textboxFindDescriptor);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_groupBox_SelectedDescriptors);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_labelDescriptor);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_labelCategory);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_labelCrop);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_listBoxCrop);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_buttonRemoveDescriptor);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_listBoxDescriptors);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_listBoxTrait);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_buttonShowDefinition);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_Button_Add_Descriptor);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_listBoxCategory);
            this.ux_groupBox_Descriptors.Location = new System.Drawing.Point(13, 12);
            this.ux_groupBox_Descriptors.Name = "ux_groupBox_Descriptors";
            this.ux_groupBox_Descriptors.Size = new System.Drawing.Size(911, 197);
            this.ux_groupBox_Descriptors.TabIndex = 11;
            this.ux_groupBox_Descriptors.TabStop = false;
            this.ux_groupBox_Descriptors.Text = "Select descriptors";
            // 
            // ux_textboxFindDescriptor
            // 
            this.ux_textboxFindDescriptor.Location = new System.Drawing.Point(457, 19);
            this.ux_textboxFindDescriptor.Name = "ux_textboxFindDescriptor";
            this.ux_textboxFindDescriptor.Size = new System.Drawing.Size(169, 20);
            this.ux_textboxFindDescriptor.TabIndex = 22;
            this.ux_textboxFindDescriptor.TextChanged += new System.EventHandler(this.ux_textboxFindDescriptor_TextChanged);
            // 
            // ux_groupBox_SelectedDescriptors
            // 
            this.ux_groupBox_SelectedDescriptors.AutoSize = true;
            this.ux_groupBox_SelectedDescriptors.Location = new System.Drawing.Point(645, 23);
            this.ux_groupBox_SelectedDescriptors.Name = "ux_groupBox_SelectedDescriptors";
            this.ux_groupBox_SelectedDescriptors.Size = new System.Drawing.Size(49, 13);
            this.ux_groupBox_SelectedDescriptors.TabIndex = 21;
            this.ux_groupBox_SelectedDescriptors.Text = "Selected";
            // 
            // ux_labelDescriptor
            // 
            this.ux_labelDescriptor.AutoSize = true;
            this.ux_labelDescriptor.Location = new System.Drawing.Point(341, 23);
            this.ux_labelDescriptor.Name = "ux_labelDescriptor";
            this.ux_labelDescriptor.Size = new System.Drawing.Size(87, 13);
            this.ux_labelDescriptor.TabIndex = 21;
            this.ux_labelDescriptor.Text = "Trait / Descriptor";
            // 
            // ux_labelCategory
            // 
            this.ux_labelCategory.AutoSize = true;
            this.ux_labelCategory.Location = new System.Drawing.Point(170, 23);
            this.ux_labelCategory.Name = "ux_labelCategory";
            this.ux_labelCategory.Size = new System.Drawing.Size(36, 13);
            this.ux_labelCategory.TabIndex = 20;
            this.ux_labelCategory.Text = "Group";
            // 
            // ux_labelCrop
            // 
            this.ux_labelCrop.AutoSize = true;
            this.ux_labelCrop.Location = new System.Drawing.Point(14, 23);
            this.ux_labelCrop.Name = "ux_labelCrop";
            this.ux_labelCrop.Size = new System.Drawing.Size(29, 13);
            this.ux_labelCrop.TabIndex = 19;
            this.ux_labelCrop.Text = "Crop";
            // 
            // ux_listBoxCrop
            // 
            this.ux_listBoxCrop.FormattingEnabled = true;
            this.ux_listBoxCrop.Location = new System.Drawing.Point(17, 39);
            this.ux_listBoxCrop.Name = "ux_listBoxCrop";
            this.ux_listBoxCrop.Size = new System.Drawing.Size(143, 121);
            this.ux_listBoxCrop.TabIndex = 13;
            this.ux_listBoxCrop.SelectedIndexChanged += new System.EventHandler(this.ux_listBoxCrop_SelectedIndexChanged);
            // 
            // ux_buttonRemoveDescriptor
            // 
            this.ux_buttonRemoveDescriptor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonRemoveDescriptor.Location = new System.Drawing.Point(770, 166);
            this.ux_buttonRemoveDescriptor.Name = "ux_buttonRemoveDescriptor";
            this.ux_buttonRemoveDescriptor.Size = new System.Drawing.Size(127, 23);
            this.ux_buttonRemoveDescriptor.TabIndex = 12;
            this.ux_buttonRemoveDescriptor.Text = "Remove Descriptor";
            this.ux_buttonRemoveDescriptor.UseVisualStyleBackColor = true;
            this.ux_buttonRemoveDescriptor.Click += new System.EventHandler(this.ux_buttonRemoveDescriptor_Click);
            // 
            // ux_listBoxDescriptors
            // 
            this.ux_listBoxDescriptors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_listBoxDescriptors.FormattingEnabled = true;
            this.ux_listBoxDescriptors.Location = new System.Drawing.Point(648, 39);
            this.ux_listBoxDescriptors.Name = "ux_listBoxDescriptors";
            this.ux_listBoxDescriptors.Size = new System.Drawing.Size(248, 121);
            this.ux_listBoxDescriptors.TabIndex = 11;
            // 
            // ux_buttonShowDefinition
            // 
            this.ux_buttonShowDefinition.Location = new System.Drawing.Point(510, 166);
            this.ux_buttonShowDefinition.Name = "ux_buttonShowDefinition";
            this.ux_buttonShowDefinition.Size = new System.Drawing.Size(116, 23);
            this.ux_buttonShowDefinition.TabIndex = 2;
            this.ux_buttonShowDefinition.Text = "Show Definition";
            this.ux_buttonShowDefinition.UseVisualStyleBackColor = true;
            this.ux_buttonShowDefinition.Click += new System.EventHandler(this.ux_buttonShowDefinition_Click);
            // 
            // ux_progressBarWorking
            // 
            this.ux_progressBarWorking.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_progressBarWorking.Location = new System.Drawing.Point(556, 6);
            this.ux_progressBarWorking.Name = "ux_progressBarWorking";
            this.ux_progressBarWorking.Size = new System.Drawing.Size(167, 23);
            this.ux_progressBarWorking.TabIndex = 15;
            this.ux_progressBarWorking.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ux_labelMessage);
            this.panel1.Controls.Add(this.ux_buttonValidateData);
            this.panel1.Controls.Add(this.ux_buttonClear);
            this.panel1.Controls.Add(this.ux_Button_Close);
            this.panel1.Controls.Add(this.ux_progressBarWorking);
            this.panel1.Controls.Add(this.ux_buttonSaveData);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 628);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(937, 36);
            this.panel1.TabIndex = 17;
            // 
            // ux_labelMessage
            // 
            this.ux_labelMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_labelMessage.AutoSize = true;
            this.ux_labelMessage.Location = new System.Drawing.Point(400, 11);
            this.ux_labelMessage.MinimumSize = new System.Drawing.Size(150, 0);
            this.ux_labelMessage.Name = "ux_labelMessage";
            this.ux_labelMessage.Size = new System.Drawing.Size(150, 13);
            this.ux_labelMessage.TabIndex = 19;
            // 
            // ux_buttonValidateData
            // 
            this.ux_buttonValidateData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_buttonValidateData.Location = new System.Drawing.Point(56, 6);
            this.ux_buttonValidateData.Name = "ux_buttonValidateData";
            this.ux_buttonValidateData.Size = new System.Drawing.Size(96, 23);
            this.ux_buttonValidateData.TabIndex = 24;
            this.ux_buttonValidateData.Text = "Validate Data";
            this.ux_buttonValidateData.UseVisualStyleBackColor = true;
            this.ux_buttonValidateData.Click += new System.EventHandler(this.ux_buttonValidateData_Click);
            // 
            // ux_buttonClear
            // 
            this.ux_buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonClear.Location = new System.Drawing.Point(733, 6);
            this.ux_buttonClear.Name = "ux_buttonClear";
            this.ux_buttonClear.Size = new System.Drawing.Size(95, 23);
            this.ux_buttonClear.TabIndex = 23;
            this.ux_buttonClear.Text = "Clear";
            this.ux_buttonClear.UseVisualStyleBackColor = true;
            this.ux_buttonClear.Click += new System.EventHandler(this.ux_buttonClear_Click);
            // 
            // ux_buttonSaveData
            // 
            this.ux_buttonSaveData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_buttonSaveData.Location = new System.Drawing.Point(298, 6);
            this.ux_buttonSaveData.Name = "ux_buttonSaveData";
            this.ux_buttonSaveData.Size = new System.Drawing.Size(96, 23);
            this.ux_buttonSaveData.TabIndex = 18;
            this.ux_buttonSaveData.Text = "Save Data";
            this.ux_buttonSaveData.UseVisualStyleBackColor = true;
            this.ux_buttonSaveData.Click += new System.EventHandler(this.ux_buttonSaveData_Click);
            // 
            // ux_buttonTemplate
            // 
            this.ux_buttonTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_buttonTemplate.Location = new System.Drawing.Point(766, 345);
            this.ux_buttonTemplate.Name = "ux_buttonTemplate";
            this.ux_buttonTemplate.Size = new System.Drawing.Size(131, 23);
            this.ux_buttonTemplate.TabIndex = 22;
            this.ux_buttonTemplate.Text = "Manage Templates";
            this.ux_buttonTemplate.UseVisualStyleBackColor = true;
            this.ux_buttonTemplate.Visible = false;
            this.ux_buttonTemplate.Click += new System.EventHandler(this.ux_buttonTemplate_Click);
            // 
            // ux_datagridviewSheet
            // 
            this.ux_datagridviewSheet.AllowUserToAddRows = false;
            this.ux_datagridviewSheet.AllowUserToOrderColumns = true;
            this.ux_datagridviewSheet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_datagridviewSheet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewSheet.Location = new System.Drawing.Point(6, 43);
            this.ux_datagridviewSheet.Name = "ux_datagridviewSheet";
            this.ux_datagridviewSheet.Size = new System.Drawing.Size(891, 262);
            this.ux_datagridviewSheet.TabIndex = 2;
            this.ux_datagridviewSheet.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ux_datagridviewSheet_CellMouseDown);
            this.ux_datagridviewSheet.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ux_datagridviewSheet_ColumnHeaderMouseClick);
            this.ux_datagridviewSheet.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.ux_datagridviewSheet_RowsAdded);
            this.ux_datagridviewSheet.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.ux_datagridviewSheet_RowsRemoved);
            this.ux_datagridviewSheet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ux_datagridviewMain2_KeyDown);
            this.ux_datagridviewSheet.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ux_datagridviewSheet_MouseUp);
            // 
            // ux_groupboxSheet
            // 
            this.ux_groupboxSheet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxSheet.Controls.Add(this.ux_checkBoxMatchCase);
            this.ux_groupboxSheet.Controls.Add(this.ux_buttonTemplate);
            this.ux_groupboxSheet.Controls.Add(this.ux_buttonExportExcel);
            this.ux_groupboxSheet.Controls.Add(this.ux_labelCellInfo);
            this.ux_groupboxSheet.Controls.Add(this.ux_groupBoxSavingSummary);
            this.ux_groupboxSheet.Controls.Add(this.ux_groupBoxValidationSummary);
            this.ux_groupboxSheet.Controls.Add(this.ux_labelRows);
            this.ux_groupboxSheet.Controls.Add(this.ux_comboboxMethod);
            this.ux_groupboxSheet.Controls.Add(this.ux_datagridviewSheet);
            this.ux_groupboxSheet.Controls.Add(this.ux_checkBoxShowOnlyRowsWithError);
            this.ux_groupboxSheet.Controls.Add(this.ux_checkboxIsArchived);
            this.ux_groupboxSheet.Controls.Add(this.ux_labelMethod);
            this.ux_groupboxSheet.Location = new System.Drawing.Point(13, 208);
            this.ux_groupboxSheet.Name = "ux_groupboxSheet";
            this.ux_groupboxSheet.Size = new System.Drawing.Size(911, 414);
            this.ux_groupboxSheet.TabIndex = 18;
            this.ux_groupboxSheet.TabStop = false;
            // 
            // ux_checkBoxMatchCase
            // 
            this.ux_checkBoxMatchCase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_checkBoxMatchCase.AutoSize = true;
            this.ux_checkBoxMatchCase.Checked = true;
            this.ux_checkBoxMatchCase.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ux_checkBoxMatchCase.Location = new System.Drawing.Point(640, 315);
            this.ux_checkBoxMatchCase.Name = "ux_checkBoxMatchCase";
            this.ux_checkBoxMatchCase.Size = new System.Drawing.Size(82, 17);
            this.ux_checkBoxMatchCase.TabIndex = 33;
            this.ux_checkBoxMatchCase.Text = "Match case";
            this.ux_checkBoxMatchCase.UseVisualStyleBackColor = true;
            // 
            // ux_buttonExportExcel
            // 
            this.ux_buttonExportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonExportExcel.Location = new System.Drawing.Point(640, 345);
            this.ux_buttonExportExcel.Name = "ux_buttonExportExcel";
            this.ux_buttonExportExcel.Size = new System.Drawing.Size(95, 23);
            this.ux_buttonExportExcel.TabIndex = 24;
            this.ux_buttonExportExcel.Text = "Export to excel";
            this.ux_buttonExportExcel.UseVisualStyleBackColor = true;
            this.ux_buttonExportExcel.Visible = false;
            this.ux_buttonExportExcel.Click += new System.EventHandler(this.ux_buttonExportExcel_Click);
            // 
            // ux_labelCellInfo
            // 
            this.ux_labelCellInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_labelCellInfo.BackColor = System.Drawing.SystemColors.Info;
            this.ux_labelCellInfo.Location = new System.Drawing.Point(463, 316);
            this.ux_labelCellInfo.Name = "ux_labelCellInfo";
            this.ux_labelCellInfo.Size = new System.Drawing.Size(163, 95);
            this.ux_labelCellInfo.TabIndex = 32;
            this.ux_labelCellInfo.Visible = false;
            // 
            // ux_groupBoxSavingSummary
            // 
            this.ux_groupBoxSavingSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_groupBoxSavingSummary.Controls.Add(this.tableLayoutPanel1);
            this.ux_groupBoxSavingSummary.Location = new System.Drawing.Point(234, 311);
            this.ux_groupBoxSavingSummary.Name = "ux_groupBoxSavingSummary";
            this.ux_groupBoxSavingSummary.Size = new System.Drawing.Size(222, 100);
            this.ux_groupBoxSavingSummary.TabIndex = 31;
            this.ux_groupBoxSavingSummary.TabStop = false;
            this.ux_groupBoxSavingSummary.Text = "Saving Summary";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Controls.Add(this.ux_labelSavingUnmodifiedCount, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.ux_pictureBoxSavingUnmodified, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ux_labelSavingUnmodified, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.ux_pictureBoxSavingOk, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ux_pictureBoxSavingError, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.ux_labelSavingOk, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.ux_labelSavingError, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.ux_labelSavingOkCount, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.ux_labelSavingErrorCount, 2, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(210, 75);
            this.tableLayoutPanel1.TabIndex = 28;
            // 
            // ux_labelSavingUnmodifiedCount
            // 
            this.ux_labelSavingUnmodifiedCount.AutoSize = true;
            this.ux_labelSavingUnmodifiedCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelSavingUnmodifiedCount.Location = new System.Drawing.Point(163, 0);
            this.ux_labelSavingUnmodifiedCount.Name = "ux_labelSavingUnmodifiedCount";
            this.ux_labelSavingUnmodifiedCount.Size = new System.Drawing.Size(44, 25);
            this.ux_labelSavingUnmodifiedCount.TabIndex = 3;
            this.ux_labelSavingUnmodifiedCount.Text = "----";
            this.ux_labelSavingUnmodifiedCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_pictureBoxSavingUnmodified
            // 
            this.ux_pictureBoxSavingUnmodified.BackColor = System.Drawing.Color.White;
            this.ux_pictureBoxSavingUnmodified.Location = new System.Drawing.Point(3, 3);
            this.ux_pictureBoxSavingUnmodified.Name = "ux_pictureBoxSavingUnmodified";
            this.ux_pictureBoxSavingUnmodified.Size = new System.Drawing.Size(19, 19);
            this.ux_pictureBoxSavingUnmodified.TabIndex = 0;
            this.ux_pictureBoxSavingUnmodified.TabStop = false;
            // 
            // ux_labelSavingUnmodified
            // 
            this.ux_labelSavingUnmodified.AutoSize = true;
            this.ux_labelSavingUnmodified.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelSavingUnmodified.Location = new System.Drawing.Point(28, 0);
            this.ux_labelSavingUnmodified.Name = "ux_labelSavingUnmodified";
            this.ux_labelSavingUnmodified.Size = new System.Drawing.Size(129, 25);
            this.ux_labelSavingUnmodified.TabIndex = 1;
            this.ux_labelSavingUnmodified.Text = "Changes not found";
            this.ux_labelSavingUnmodified.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ux_pictureBoxSavingOk
            // 
            this.ux_pictureBoxSavingOk.BackColor = System.Drawing.Color.LightBlue;
            this.ux_pictureBoxSavingOk.Location = new System.Drawing.Point(3, 28);
            this.ux_pictureBoxSavingOk.Name = "ux_pictureBoxSavingOk";
            this.ux_pictureBoxSavingOk.Size = new System.Drawing.Size(19, 19);
            this.ux_pictureBoxSavingOk.TabIndex = 2;
            this.ux_pictureBoxSavingOk.TabStop = false;
            // 
            // ux_pictureBoxSavingError
            // 
            this.ux_pictureBoxSavingError.BackColor = System.Drawing.Color.Red;
            this.ux_pictureBoxSavingError.Location = new System.Drawing.Point(3, 53);
            this.ux_pictureBoxSavingError.Name = "ux_pictureBoxSavingError";
            this.ux_pictureBoxSavingError.Size = new System.Drawing.Size(19, 19);
            this.ux_pictureBoxSavingError.TabIndex = 2;
            this.ux_pictureBoxSavingError.TabStop = false;
            // 
            // ux_labelSavingOk
            // 
            this.ux_labelSavingOk.AutoSize = true;
            this.ux_labelSavingOk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelSavingOk.Location = new System.Drawing.Point(28, 25);
            this.ux_labelSavingOk.Name = "ux_labelSavingOk";
            this.ux_labelSavingOk.Size = new System.Drawing.Size(129, 25);
            this.ux_labelSavingOk.TabIndex = 1;
            this.ux_labelSavingOk.Text = "Saved";
            this.ux_labelSavingOk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ux_labelSavingError
            // 
            this.ux_labelSavingError.AutoSize = true;
            this.ux_labelSavingError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelSavingError.Location = new System.Drawing.Point(28, 50);
            this.ux_labelSavingError.Name = "ux_labelSavingError";
            this.ux_labelSavingError.Size = new System.Drawing.Size(129, 25);
            this.ux_labelSavingError.TabIndex = 1;
            this.ux_labelSavingError.Text = "Saving error";
            this.ux_labelSavingError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ux_labelSavingOkCount
            // 
            this.ux_labelSavingOkCount.AutoSize = true;
            this.ux_labelSavingOkCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelSavingOkCount.Location = new System.Drawing.Point(163, 25);
            this.ux_labelSavingOkCount.Name = "ux_labelSavingOkCount";
            this.ux_labelSavingOkCount.Size = new System.Drawing.Size(44, 25);
            this.ux_labelSavingOkCount.TabIndex = 3;
            this.ux_labelSavingOkCount.Text = "----";
            this.ux_labelSavingOkCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_labelSavingErrorCount
            // 
            this.ux_labelSavingErrorCount.AutoSize = true;
            this.ux_labelSavingErrorCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelSavingErrorCount.Location = new System.Drawing.Point(163, 50);
            this.ux_labelSavingErrorCount.Name = "ux_labelSavingErrorCount";
            this.ux_labelSavingErrorCount.Size = new System.Drawing.Size(44, 25);
            this.ux_labelSavingErrorCount.TabIndex = 3;
            this.ux_labelSavingErrorCount.Text = "----";
            this.ux_labelSavingErrorCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_groupBoxValidationSummary
            // 
            this.ux_groupBoxValidationSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_groupBoxValidationSummary.Controls.Add(this.ux_tableLayoutPanelSummary);
            this.ux_groupBoxValidationSummary.Location = new System.Drawing.Point(6, 311);
            this.ux_groupBoxValidationSummary.Name = "ux_groupBoxValidationSummary";
            this.ux_groupBoxValidationSummary.Size = new System.Drawing.Size(222, 100);
            this.ux_groupBoxValidationSummary.TabIndex = 30;
            this.ux_groupBoxValidationSummary.TabStop = false;
            this.ux_groupBoxValidationSummary.Text = "Validation Summary";
            // 
            // ux_tableLayoutPanelSummary
            // 
            this.ux_tableLayoutPanelSummary.ColumnCount = 3;
            this.ux_tableLayoutPanelSummary.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.ux_tableLayoutPanelSummary.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_tableLayoutPanelSummary.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.ux_tableLayoutPanelSummary.Controls.Add(this.ux_labelValidationOkWithoutChangesCount, 2, 0);
            this.ux_tableLayoutPanelSummary.Controls.Add(this.ux_pictureBoxValidationOkWithoutChanges, 0, 0);
            this.ux_tableLayoutPanelSummary.Controls.Add(this.ux_labelValidationOkWithoutChanges, 1, 0);
            this.ux_tableLayoutPanelSummary.Controls.Add(this.ux_pictureBoxValidationOkWithChanges, 0, 1);
            this.ux_tableLayoutPanelSummary.Controls.Add(this.ux_pictureBoxValidationError, 0, 2);
            this.ux_tableLayoutPanelSummary.Controls.Add(this.ux_labelValidationOkWithChanges, 1, 1);
            this.ux_tableLayoutPanelSummary.Controls.Add(this.ux_labelValidationError, 1, 2);
            this.ux_tableLayoutPanelSummary.Controls.Add(this.ux_labelValidationOkWithChangesCount, 2, 1);
            this.ux_tableLayoutPanelSummary.Controls.Add(this.ux_labelValidationErrorCount, 2, 2);
            this.ux_tableLayoutPanelSummary.Location = new System.Drawing.Point(6, 17);
            this.ux_tableLayoutPanelSummary.Name = "ux_tableLayoutPanelSummary";
            this.ux_tableLayoutPanelSummary.RowCount = 3;
            this.ux_tableLayoutPanelSummary.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.ux_tableLayoutPanelSummary.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.ux_tableLayoutPanelSummary.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.ux_tableLayoutPanelSummary.Size = new System.Drawing.Size(213, 75);
            this.ux_tableLayoutPanelSummary.TabIndex = 27;
            // 
            // ux_labelValidationOkWithoutChangesCount
            // 
            this.ux_labelValidationOkWithoutChangesCount.AutoSize = true;
            this.ux_labelValidationOkWithoutChangesCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelValidationOkWithoutChangesCount.Location = new System.Drawing.Point(166, 0);
            this.ux_labelValidationOkWithoutChangesCount.Name = "ux_labelValidationOkWithoutChangesCount";
            this.ux_labelValidationOkWithoutChangesCount.Size = new System.Drawing.Size(44, 25);
            this.ux_labelValidationOkWithoutChangesCount.TabIndex = 3;
            this.ux_labelValidationOkWithoutChangesCount.Text = "----";
            this.ux_labelValidationOkWithoutChangesCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_pictureBoxValidationOkWithoutChanges
            // 
            this.ux_pictureBoxValidationOkWithoutChanges.BackColor = System.Drawing.Color.White;
            this.ux_pictureBoxValidationOkWithoutChanges.Location = new System.Drawing.Point(3, 3);
            this.ux_pictureBoxValidationOkWithoutChanges.Name = "ux_pictureBoxValidationOkWithoutChanges";
            this.ux_pictureBoxValidationOkWithoutChanges.Size = new System.Drawing.Size(19, 19);
            this.ux_pictureBoxValidationOkWithoutChanges.TabIndex = 0;
            this.ux_pictureBoxValidationOkWithoutChanges.TabStop = false;
            // 
            // ux_labelValidationOkWithoutChanges
            // 
            this.ux_labelValidationOkWithoutChanges.AutoSize = true;
            this.ux_labelValidationOkWithoutChanges.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelValidationOkWithoutChanges.Location = new System.Drawing.Point(28, 0);
            this.ux_labelValidationOkWithoutChanges.Name = "ux_labelValidationOkWithoutChanges";
            this.ux_labelValidationOkWithoutChanges.Size = new System.Drawing.Size(132, 25);
            this.ux_labelValidationOkWithoutChanges.TabIndex = 1;
            this.ux_labelValidationOkWithoutChanges.Text = "Data without changes";
            this.ux_labelValidationOkWithoutChanges.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ux_pictureBoxValidationOkWithChanges
            // 
            this.ux_pictureBoxValidationOkWithChanges.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ux_pictureBoxValidationOkWithChanges.Location = new System.Drawing.Point(3, 28);
            this.ux_pictureBoxValidationOkWithChanges.Name = "ux_pictureBoxValidationOkWithChanges";
            this.ux_pictureBoxValidationOkWithChanges.Size = new System.Drawing.Size(19, 19);
            this.ux_pictureBoxValidationOkWithChanges.TabIndex = 2;
            this.ux_pictureBoxValidationOkWithChanges.TabStop = false;
            // 
            // ux_pictureBoxValidationError
            // 
            this.ux_pictureBoxValidationError.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.ux_pictureBoxValidationError.Location = new System.Drawing.Point(3, 53);
            this.ux_pictureBoxValidationError.Name = "ux_pictureBoxValidationError";
            this.ux_pictureBoxValidationError.Size = new System.Drawing.Size(19, 19);
            this.ux_pictureBoxValidationError.TabIndex = 2;
            this.ux_pictureBoxValidationError.TabStop = false;
            // 
            // ux_labelValidationOkWithChanges
            // 
            this.ux_labelValidationOkWithChanges.AutoSize = true;
            this.ux_labelValidationOkWithChanges.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelValidationOkWithChanges.Location = new System.Drawing.Point(28, 25);
            this.ux_labelValidationOkWithChanges.Name = "ux_labelValidationOkWithChanges";
            this.ux_labelValidationOkWithChanges.Size = new System.Drawing.Size(132, 25);
            this.ux_labelValidationOkWithChanges.TabIndex = 1;
            this.ux_labelValidationOkWithChanges.Text = "Data with changes";
            this.ux_labelValidationOkWithChanges.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ux_labelValidationError
            // 
            this.ux_labelValidationError.AutoSize = true;
            this.ux_labelValidationError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelValidationError.Location = new System.Drawing.Point(28, 50);
            this.ux_labelValidationError.Name = "ux_labelValidationError";
            this.ux_labelValidationError.Size = new System.Drawing.Size(132, 25);
            this.ux_labelValidationError.TabIndex = 1;
            this.ux_labelValidationError.Text = "Data with errors";
            this.ux_labelValidationError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ux_labelValidationOkWithChangesCount
            // 
            this.ux_labelValidationOkWithChangesCount.AutoSize = true;
            this.ux_labelValidationOkWithChangesCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelValidationOkWithChangesCount.Location = new System.Drawing.Point(166, 25);
            this.ux_labelValidationOkWithChangesCount.Name = "ux_labelValidationOkWithChangesCount";
            this.ux_labelValidationOkWithChangesCount.Size = new System.Drawing.Size(44, 25);
            this.ux_labelValidationOkWithChangesCount.TabIndex = 3;
            this.ux_labelValidationOkWithChangesCount.Text = "----";
            this.ux_labelValidationOkWithChangesCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_labelValidationErrorCount
            // 
            this.ux_labelValidationErrorCount.AutoSize = true;
            this.ux_labelValidationErrorCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelValidationErrorCount.Location = new System.Drawing.Point(166, 50);
            this.ux_labelValidationErrorCount.Name = "ux_labelValidationErrorCount";
            this.ux_labelValidationErrorCount.Size = new System.Drawing.Size(44, 25);
            this.ux_labelValidationErrorCount.TabIndex = 3;
            this.ux_labelValidationErrorCount.Text = "----";
            this.ux_labelValidationErrorCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_labelRows
            // 
            this.ux_labelRows.AutoSize = true;
            this.ux_labelRows.Location = new System.Drawing.Point(6, 21);
            this.ux_labelRows.Name = "ux_labelRows";
            this.ux_labelRows.Size = new System.Drawing.Size(119, 13);
            this.ux_labelRows.TabIndex = 29;
            this.ux_labelRows.Tag = "Showing rows {0} of {1}";
            this.ux_labelRows.Text = "Showing rows {0} of {1}";
            // 
            // ux_comboboxMethod
            // 
            this.ux_comboboxMethod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_comboboxMethod.FormattingEnabled = true;
            this.ux_comboboxMethod.Location = new System.Drawing.Point(501, 18);
            this.ux_comboboxMethod.Name = "ux_comboboxMethod";
            this.ux_comboboxMethod.Size = new System.Drawing.Size(285, 21);
            this.ux_comboboxMethod.TabIndex = 24;
            this.ux_comboboxMethod.SelectedIndexChanged += new System.EventHandler(this.ux_comboboxMethod_SelectedIndexChanged);
            // 
            // ux_checkBoxShowOnlyRowsWithError
            // 
            this.ux_checkBoxShowOnlyRowsWithError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_checkBoxShowOnlyRowsWithError.AutoSize = true;
            this.ux_checkBoxShowOnlyRowsWithError.Location = new System.Drawing.Point(751, 315);
            this.ux_checkBoxShowOnlyRowsWithError.Name = "ux_checkBoxShowOnlyRowsWithError";
            this.ux_checkBoxShowOnlyRowsWithError.Size = new System.Drawing.Size(146, 17);
            this.ux_checkBoxShowOnlyRowsWithError.TabIndex = 0;
            this.ux_checkBoxShowOnlyRowsWithError.Text = "Show only rows with error";
            this.ux_checkBoxShowOnlyRowsWithError.UseVisualStyleBackColor = true;
            this.ux_checkBoxShowOnlyRowsWithError.CheckedChanged += new System.EventHandler(this.ux_checkBoxShowOnlyRowsWithError_CheckedChanged);
            // 
            // ux_checkboxIsArchived
            // 
            this.ux_checkboxIsArchived.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_checkboxIsArchived.AutoSize = true;
            this.ux_checkboxIsArchived.Location = new System.Drawing.Point(818, 20);
            this.ux_checkboxIsArchived.Name = "ux_checkboxIsArchived";
            this.ux_checkboxIsArchived.Size = new System.Drawing.Size(79, 17);
            this.ux_checkboxIsArchived.TabIndex = 23;
            this.ux_checkboxIsArchived.Text = "Is Archived";
            this.ux_checkboxIsArchived.UseVisualStyleBackColor = true;
            // 
            // ux_labelMethod
            // 
            this.ux_labelMethod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelMethod.AutoSize = true;
            this.ux_labelMethod.Location = new System.Drawing.Point(446, 21);
            this.ux_labelMethod.Name = "ux_labelMethod";
            this.ux_labelMethod.Size = new System.Drawing.Size(49, 13);
            this.ux_labelMethod.TabIndex = 22;
            this.ux_labelMethod.Text = "Method :";
            // 
            // contextMenuStripMenu
            // 
            this.contextMenuStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showDBValuetoolStripMenuItem,
            this.toolStripSeparator1,
            this.loadCurrentValueToolStripMenuItem,
            this.toolStripMenuItem1,
            this.copyHeadersToolStripMenuItem,
            this.copyAllToolStripMenuItem,
            this.removeAllColumnsToolStripMenuItem,
            this.updateLookupTablesToolStripMenuItem,
            this.showHiddenColumnsMenuItem});
            this.contextMenuStripMenu.Name = "contextMenuStripMenu";
            this.contextMenuStripMenu.Size = new System.Drawing.Size(193, 192);
            // 
            // showDBValuetoolStripMenuItem
            // 
            this.showDBValuetoolStripMenuItem.Name = "showDBValuetoolStripMenuItem";
            this.showDBValuetoolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.showDBValuetoolStripMenuItem.Text = "Show current value";
            this.showDBValuetoolStripMenuItem.Click += new System.EventHandler(this.showDBValuetoolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(189, 6);
            // 
            // loadCurrentValueToolStripMenuItem
            // 
            this.loadCurrentValueToolStripMenuItem.Name = "loadCurrentValueToolStripMenuItem";
            this.loadCurrentValueToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.loadCurrentValueToolStripMenuItem.Text = "Load current value(s)";
            this.loadCurrentValueToolStripMenuItem.Click += new System.EventHandler(this.loadCurrentValueToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(189, 6);
            // 
            // copyHeadersToolStripMenuItem
            // 
            this.copyHeadersToolStripMenuItem.Name = "copyHeadersToolStripMenuItem";
            this.copyHeadersToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.copyHeadersToolStripMenuItem.Text = "Copy headers";
            this.copyHeadersToolStripMenuItem.Click += new System.EventHandler(this.copyHeadersToolStripMenuItem_Click);
            // 
            // copyAllToolStripMenuItem
            // 
            this.copyAllToolStripMenuItem.Name = "copyAllToolStripMenuItem";
            this.copyAllToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.copyAllToolStripMenuItem.Text = "Copy all";
            this.copyAllToolStripMenuItem.Click += new System.EventHandler(this.copyAllToolStripMenuItem_Click);
            // 
            // updateLookupTablesToolStripMenuItem
            // 
            this.updateLookupTablesToolStripMenuItem.Name = "updateLookupTablesToolStripMenuItem";
            this.updateLookupTablesToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.updateLookupTablesToolStripMenuItem.Text = "Update lookup tables";
            this.updateLookupTablesToolStripMenuItem.Visible = false;
            this.updateLookupTablesToolStripMenuItem.Click += new System.EventHandler(this.updateLookupTablesToolStripMenuItem_Click);
            // 
            // showHiddenColumnsMenuItem
            // 
            this.showHiddenColumnsMenuItem.Name = "showHiddenColumnsMenuItem";
            this.showHiddenColumnsMenuItem.Size = new System.Drawing.Size(192, 22);
            this.showHiddenColumnsMenuItem.Text = "Show hidden columns";
            this.showHiddenColumnsMenuItem.Visible = false;
            this.showHiddenColumnsMenuItem.Click += new System.EventHandler(this.showHiddenColumnsMenuItem_Click);
            // 
            // removeAllColumnsToolStripMenuItem
            // 
            this.removeAllColumnsToolStripMenuItem.Name = "removeAllColumnsToolStripMenuItem";
            this.removeAllColumnsToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.removeAllColumnsToolStripMenuItem.Text = "Remove all columns";
            this.removeAllColumnsToolStripMenuItem.Click += new System.EventHandler(this.removeAllColumnsToolStripMenuItem_Click);
            // 
            // BatchWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 664);
            this.Controls.Add(this.ux_groupboxSheet);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ux_groupBox_Descriptors);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BatchWizard";
            this.Text = "Update Wizard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AccessionBatchWizard_FormClosing);
            this.Load += new System.EventHandler(this.AccessionBatchWizard_Load);
            this.ux_groupBox_Descriptors.ResumeLayout(false);
            this.ux_groupBox_Descriptors.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewSheet)).EndInit();
            this.ux_groupboxSheet.ResumeLayout(false);
            this.ux_groupboxSheet.PerformLayout();
            this.ux_groupBoxSavingSummary.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxSavingUnmodified)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxSavingOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxSavingError)).EndInit();
            this.ux_groupBoxValidationSummary.ResumeLayout(false);
            this.ux_tableLayoutPanelSummary.ResumeLayout(false);
            this.ux_tableLayoutPanelSummary.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxValidationOkWithoutChanges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxValidationOkWithChanges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxValidationError)).EndInit();
            this.contextMenuStripMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ux_Button_Close;
        private System.Windows.Forms.Button ux_Button_Add_Descriptor;
        private System.Windows.Forms.ListBox ux_listBoxCategory;
        private System.Windows.Forms.ListBox ux_listBoxTrait;
        private System.Windows.Forms.GroupBox ux_groupBox_Descriptors;
        private System.Windows.Forms.Button ux_buttonRemoveDescriptor;
        private System.Windows.Forms.ListBox ux_listBoxDescriptors;
        private System.Windows.Forms.ListBox ux_listBoxCrop;
        private System.Windows.Forms.ProgressBar ux_progressBarWorking;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label ux_labelDescriptor;
        private System.Windows.Forms.Label ux_labelCategory;
        private System.Windows.Forms.Label ux_labelCrop;
        private System.Windows.Forms.Button ux_buttonSaveData;
        private System.Windows.Forms.DataGridView ux_datagridviewSheet;
        private System.Windows.Forms.Label ux_labelMessage;
        private System.Windows.Forms.Button ux_buttonTemplate;
        private System.Windows.Forms.Button ux_buttonClear;
        private System.Windows.Forms.GroupBox ux_groupboxSheet;
        private System.Windows.Forms.CheckBox ux_checkBoxShowOnlyRowsWithError;
        private System.Windows.Forms.Label ux_labelMethod;
        private System.Windows.Forms.CheckBox ux_checkboxIsArchived;
        private System.Windows.Forms.ComboBox ux_comboboxMethod;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripMenu;
        private System.Windows.Forms.ToolStripMenuItem copyHeadersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateLookupTablesToolStripMenuItem;
        private System.Windows.Forms.Button ux_buttonValidateData;
        private System.Windows.Forms.TableLayoutPanel ux_tableLayoutPanelSummary;
        private System.Windows.Forms.PictureBox ux_pictureBoxValidationOkWithoutChanges;
        private System.Windows.Forms.Label ux_labelValidationOkWithoutChanges;
        private System.Windows.Forms.PictureBox ux_pictureBoxValidationOkWithChanges;
        private System.Windows.Forms.PictureBox ux_pictureBoxValidationError;
        private System.Windows.Forms.Label ux_labelValidationOkWithChanges;
        private System.Windows.Forms.Label ux_labelValidationError;
        private System.Windows.Forms.Label ux_labelValidationOkWithoutChangesCount;
        private System.Windows.Forms.Label ux_labelValidationOkWithChangesCount;
        private System.Windows.Forms.Label ux_labelValidationErrorCount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label ux_labelSavingUnmodifiedCount;
        private System.Windows.Forms.PictureBox ux_pictureBoxSavingUnmodified;
        private System.Windows.Forms.Label ux_labelSavingUnmodified;
        private System.Windows.Forms.PictureBox ux_pictureBoxSavingOk;
        private System.Windows.Forms.PictureBox ux_pictureBoxSavingError;
        private System.Windows.Forms.Label ux_labelSavingOk;
        private System.Windows.Forms.Label ux_labelSavingError;
        private System.Windows.Forms.Label ux_labelSavingOkCount;
        private System.Windows.Forms.Label ux_labelSavingErrorCount;
        private System.Windows.Forms.Button ux_buttonShowDefinition;
        private System.Windows.Forms.Label ux_labelRows;
        private System.Windows.Forms.ToolStripMenuItem showHiddenColumnsMenuItem;
        private System.Windows.Forms.GroupBox ux_groupBoxSavingSummary;
        private System.Windows.Forms.GroupBox ux_groupBoxValidationSummary;
        private System.Windows.Forms.Label ux_labelCellInfo;
        private System.Windows.Forms.ToolStripMenuItem showDBValuetoolStripMenuItem;
        private System.Windows.Forms.Button ux_buttonExportExcel;
        private System.Windows.Forms.Label ux_groupBox_SelectedDescriptors;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem loadCurrentValueToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.CheckBox ux_checkBoxMatchCase;
        private System.Windows.Forms.TextBox ux_textboxFindDescriptor;
        private System.Windows.Forms.ToolStripMenuItem copyAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeAllColumnsToolStripMenuItem;
    }
}