﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AccessionBatchWizard
{
    public partial class DescriptorDetailForm : Form
    {
        GRINGlobal.Client.Common.SharedUtils _sharedUtils;
        
        public DescriptorDetailForm(GRINGlobal.Client.Common.SharedUtils sharedUtils, DataTable data)
        {
            InitializeComponent();

            _sharedUtils = sharedUtils;

            if (data.TableName.Equals("acc_batch_wizard_get_mcpd_detail")) //PASSPORT
            {
                ux_textBoxTraitName.Text = data.Rows[0]["descriptor_sname"].ToString();
                ux_textBoxDefinition.Text = data.Rows[0]["descriptor_definition"].ToString();
                ux_textBoxCrop.Text = data.Rows[0]["crop_name"].ToString();
                ux_textBoxCategory.Text = data.Rows[0]["category_name"].ToString();

                ux_labelIsCoded.Visible = false;
                ux_textBoxIsCoded.Visible = false;

                ux_labelDataType.Visible = false;
                ux_textBoxDataType.Visible = false;

                ux_labelMaximumLength.Visible = false;
                ux_textBoxMaximumLength.Visible = false;

                ux_labelNumericMinimum.Visible = false;
                ux_textBoxNumericMinimum.Visible = false;

                ux_labelNumericMaximum.Visible = false;
                ux_textBoxNumericMaximum.Visible = false;

                ux_labelNumericFormat.Visible = false;
                ux_textBoxNumericFormat.Visible = false;

                ux_textBoxTraitCodes.Visible = false;
                ux_labelTraitCodes.Visible = false;

                ux_labelOntologyUrl.Visible = false;
                ux_textBoxOntologyURL.Visible = false;
            }
            else
            {
                ux_textBoxTraitName.Text = data.Rows[0]["descriptor_sname"].ToString();
                ux_textBoxDefinition.Text = data.Rows[0]["descriptor_definition"].ToString();
                ux_textBoxCrop.Text = data.Rows[0]["crop_name"].ToString();
                ux_textBoxCategory.Text = data.Rows[0]["category_name"].ToString();
                ux_textBoxIsCoded.Text = data.Rows[0]["is_coded"].ToString();
                ux_textBoxDataType.Text = data.Rows[0]["data_type"].ToString();
                ux_textBoxMaximumLength.Text = data.Rows[0]["maximum_length"].ToString();
                ux_textBoxOntologyURL.Text = data.Rows[0]["ontology_url"].ToString();
                ux_textBoxNumericMinimum.Text = data.Rows[0]["numeric_minimum"].ToString();
                ux_textBoxNumericMaximum.Text = data.Rows[0]["numeric_maximum"].ToString();
                ux_textBoxNumericFormat.Text = data.Rows[0]["numeric_format"].ToString();
                ux_textBoxTraitCodes.Text = data.Rows[0]["crop_trait_codes"].ToString().Replace(";", Environment.NewLine);

                if (data.Rows[0]["data_type_code"].ToString().Equals("NUMERIC"))
                {
                    ux_labelMaximumLength.Visible = false;
                    ux_textBoxMaximumLength.Visible = false;
                    if (!string.IsNullOrEmpty(ux_textBoxNumericFormat.Text))
                    {
                        if (!data.Rows[0].IsNull("numeric_minimum"))
                            ux_textBoxNumericMinimum.Text = data.Rows[0].Field<decimal>("numeric_minimum").ToString(ux_textBoxNumericFormat.Text);
                        if (!data.Rows[0].IsNull("numeric_maximum"))
                            ux_textBoxNumericMaximum.Text = data.Rows[0].Field<decimal>("numeric_maximum").ToString(ux_textBoxNumericFormat.Text);
                    }
                }
                else
                {
                    ux_labelNumericMinimum.Visible = false;
                    ux_textBoxNumericMinimum.Visible = false;

                    ux_labelNumericMaximum.Visible = false;
                    ux_textBoxNumericMaximum.Visible = false;

                    ux_labelNumericFormat.Visible = false;
                    ux_textBoxNumericFormat.Visible = false;
                }

                if (data.Rows[0].Field<string>("is_coded").Equals("N"))
                {
                    ux_textBoxTraitCodes.Visible = false;
                    ux_labelTraitCodes.Visible = false;
                }
                else if (data.Rows[0].Field<string>("is_coded").Equals("Y"))
                {
                    ux_labelMaximumLength.Visible = false;
                    ux_textBoxMaximumLength.Visible = false;

                    ux_labelNumericMinimum.Visible = false;
                    ux_textBoxNumericMinimum.Visible = false;

                    ux_labelNumericMaximum.Visible = false;
                    ux_textBoxNumericMaximum.Visible = false;

                    ux_labelNumericFormat.Visible = false;
                    ux_textBoxNumericFormat.Visible = false;
                }
            }
            // Get language translations
            if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);
        }
        
        private void ux_buttonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
