﻿using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace AccessionBatchWizard
{
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }
    
    public partial class BatchWizard : Form, IGRINGlobalDataWizard
    {
        SharedUtils _sharedUtils;
        DataSet _changedRecords = new DataSet();
        
        Dictionary<string, string> _descriptors = new Dictionary<string, string>();
        BindingList<KeyValuePair<string, string>> _descriptors2 = new BindingList<KeyValuePair<string, string>>();
        
        DataTable _main2;

        string crop_id = string.Empty;
        string _method_id = "0";
        DataTable dtPassportCriteria;

        int startDescriptorsIndex = 4; //En caso que hubieran otras columnas fijas aparte de "ACCENUMB" como accession_id o inventory_id

        private class GroupColumn {
            public string[] RequiredColumns { set; get; }
            public bool Processed { set; get; }
            public GroupColumn(string[] requiredColumns) { RequiredColumns = requiredColumns; Processed = false; }
        }
        Dictionary<string, GroupColumn> GroupColumnMan = new Dictionary<string, GroupColumn>();

        int validationErrorCount = 0, validationOKWithChangesCount = 0, validationOKWithoutChangesCount = 0;
        int savingErrorCount = 0, savingOKCount = 0, savingUnmodifiedCount = 0;
        DataSet _dsObservationUpdate;
        DataSet _dsCropTraitIsCoded;
        
        DataSet _dsCooperator;
        DataTable _dtCooperator;
        DataSet _dsAccessionSourceMap;
        DataTable _dtAcessionSourceMap;
        DataSet _dsAccessionSourceMapExt;
        DataTable _dtAccessionSourceMapExt;
        DataSet _dsAccessionInvName;
        DataTable _dtAccessionInvName;
        DataSet _dsAccession;
        DataTable _dtAcccession;
        DataSet _dsAccessionIpr;
        DataTable _dtAccessionIpr;
        DataSet _dsAccessionPedigree;
        DataTable _dtAccessionPedigree;
        DataSet _dsAccessionSource;
        DataTable _dtAccessionSource;

        private bool _isCustomModificationNeeded = false;

        private Timer textChangeDelayTimer = new Timer();

        public string FormName
        {
            get
            {
                return "Update Wizard";
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }

        public string PKeyName
        {
            get
            {
                return "accession_id";
            }
        }

        public BatchWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();
            _sharedUtils = sharedUtils;

            StyleColors.ValidationError = ux_pictureBoxValidationError.BackColor;
            StyleColors.ValidationOKWithChanges = ux_pictureBoxValidationOkWithChanges.BackColor;
            StyleColors.ValidationOKWithoutChanges = ux_pictureBoxValidationOkWithoutChanges.BackColor;

            StyleColors.SavingError = ux_pictureBoxSavingError.BackColor;
            StyleColors.SavingOk = ux_pictureBoxSavingOk.BackColor;
            StyleColors.SavingUnmodified = ux_pictureBoxSavingUnmodified.BackColor;
        }

        private void AccessionBatchWizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void ux_Button_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AccessionBatchWizard_Load(object sender, EventArgs e)
        {
            try
            {
                _isCustomModificationNeeded = _sharedUtils.GetAppSettingValue("INSTITUTE").Equals("CIP") ? true : false;
                
                _main2 = new DataTable();
                _main2.Columns.Add("ACCENUMB");
                _main2.Columns.Add("accession_id");
                _main2.Columns.Add("inventory_id");
                _main2.Columns.Add("source_type_code");

                _main2.Columns["source_type_code"].Caption = "Source type code";

                ux_datagridviewSheet.DataSource = _main2;
                ux_datagridviewSheet.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
                ux_datagridviewSheet.Columns["source_type_code"].HeaderText = "Source type code";

                ux_datagridviewSheet.Columns["accession_id"].Visible = false;
                ux_datagridviewSheet.Columns["inventory_id"].Visible = false;
                ux_datagridviewSheet.Columns["source_type_code"].Visible = false;
                ux_datagridviewSheet.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;

                ux_datagridviewSheet.Columns["ACCENUMB"].HeaderText = _sharedUtils.GetLookupDisplayMember("code_value_lookup", "ACCENUMB", "MULTI_CROP_PASSPORT_DESCRIPTORS", "Accession number (02.)");
                /**/

                //Load passport datatable
                DataSet passportCriteria = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_passport_columns", "", 0, 0);
                if (passportCriteria != null && passportCriteria.Tables.Contains("acc_batch_wizard_get_passport_columns"))
                {
                    dtPassportCriteria = passportCriteria.Tables["acc_batch_wizard_get_passport_columns"];
                    //Make sure display_member column is editable
                    dtPassportCriteria.Columns["display_member"].ReadOnly = false;
                    //Update display_member based on value_member
                    foreach (DataRow drDescriptor in dtPassportCriteria.Rows)
                    {
                        drDescriptor["display_member"] = _sharedUtils.GetLookupDisplayMember("code_value_lookup", drDescriptor.Field<string>("value_member"), 
                                                                                                "MULTI_CROP_PASSPORT_DESCRIPTORS", 
                                                                                                drDescriptor["display_member"].ToString());
                    }
                    dtPassportCriteria.DefaultView.Sort = "display_member";
                }

                // Populate the dropdown list of 'Crop names'
                DataTable crop = _sharedUtils.GetLocalData("SELECT value_member, display_member FROM crop_lookup", "");
                if (crop.Rows.Count > 0)
                {
                    crop.DefaultView.Sort = "display_member ASC";
                    ux_listBoxCrop.ValueMember = "value_member";
                    ux_listBoxCrop.DisplayMember = "display_member";
                    ux_listBoxCrop.DataSource = crop;
                }

                GroupColumnMan.Add(PassportCode.ACCESSION_SOURCE_GEOGRAPHY, new GroupColumn(new string[] { "13. Country of origin", "Adm1", "Adm2", "Adm3" }));

                //Load method
                DataSet dsMethod = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_method", "", 0, 0);
                if (dsMethod != null && dsMethod.Tables.Contains("acc_batch_wizard_get_method"))
                {
                    ux_comboboxMethod.DataSource = dsMethod.Tables["acc_batch_wizard_get_method"];
                    ux_comboboxMethod.ValueMember = "method_id";
                    ux_comboboxMethod.DisplayMember = "name";
                    ux_comboboxMethod.SelectedItem = null;
                }

                textChangeDelayTimer.Tick += new EventHandler(timerDelay_Tick);

                // Get language translations
                if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
                if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);
                // Save messages translations 
                if (!string.IsNullOrWhiteSpace(ux_labelRows.Text)) ux_labelRows.Tag = ux_labelRows.Text;

                ux_labelRows.Text = string.Format(ux_labelRows.Tag.ToString(), 0, 0);
            }
            catch(Exception ex)
            {
                GGMessageBox mb = new GGMessageBox("\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }


        private void setListByValue(ref System.Windows.Forms.ComboBox objList, string strValue)
        {
            //int i =0;

            for (int i = 0; i < objList.Items.Count; i++)
            {
                string value = objList.GetItemText(objList.Items[i]);
                if (value.ToUpper() == strValue.ToUpper())
                {
                    objList.SelectedIndex = i;
                    break;
                }
            }


        }

        private void ux_listBoxCrop_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ux_listBoxCrop.DataSource == null) return;

                _descriptors.Clear();
                ux_listBoxDescriptors.DataSource = null;

                for (int i = _main2.Columns.Count - 1; i >= startDescriptorsIndex; i--)
                {
                    _main2.Columns.RemoveAt(i);
                }
                _main2.Rows.Clear();

                crop_id = ux_listBoxCrop.SelectedValue.ToString();

                string query = "select distinct category, b.display_member from acc_batch_wizard_crop_trait_lookup a join code_value_lookup b on a.category = b.value_member and b.group_name = 'DESCRIPTOR_CATEGORY' where crop_id = ";
                DataTable localDBCodeCategoryTable = _sharedUtils.GetLocalData(query + ux_listBoxCrop.SelectedValue.ToString(), "");
                if(localDBCodeCategoryTable.Columns.Count == 0)
                {
                    localDBCodeCategoryTable.Columns.Add("category", typeof(string));
                    localDBCodeCategoryTable.Columns.Add("display_member", typeof(string));
                }
                DataRow nc = localDBCodeCategoryTable.NewRow();
                nc["category"] = "PASSPORT";
                nc["display_member"] = "Passport Descriptors";
                localDBCodeCategoryTable.Rows.Add(nc);

                if (localDBCodeCategoryTable.Rows.Count > 0)
                {
                    ux_listBoxCategory.ValueMember = "category";
                    ux_listBoxCategory.DisplayMember = "display_member";
                    ux_listBoxCategory.DataSource = localDBCodeCategoryTable.DefaultView;
                }
            }
            catch (Exception ex)
            {
                GGMessageBox mb = new GGMessageBox("\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_listBoxCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ux_listBoxTrait.DataSource = null;
                ux_listBoxTrait.DataBindings.Clear();

                if (ux_listBoxCategory.SelectedValue.ToString().Equals("PASSPORT"))
                {
                    if (dtPassportCriteria != null)
                    {
                        //filter list
                        if (string.IsNullOrEmpty(ux_textboxFindDescriptor.Text))
                            dtPassportCriteria.DefaultView.RowFilter = string.Empty;
                        else
                            dtPassportCriteria.DefaultView.RowFilter = "display_member like '%" + ux_textboxFindDescriptor.Text + "%'";

                        ux_listBoxTrait.ValueMember = "value_member";
                        ux_listBoxTrait.DisplayMember = "display_member";
                        ux_listBoxTrait.DataSource = dtPassportCriteria.DefaultView;
                    }
                }
                else
                {
                    DataTable localDBTraitTable = _sharedUtils.GetLocalData("select * from acc_batch_wizard_crop_trait_lookup where crop_id = " + ux_listBoxCrop.SelectedValue.ToString() + " and category = '" + ux_listBoxCategory.SelectedValue.ToString() + "'", "");
                    if (localDBTraitTable.Rows.Count > 0)
                    {
                        //filter list
                        if (string.IsNullOrEmpty(ux_textboxFindDescriptor.Text))
                            localDBTraitTable.DefaultView.RowFilter = string.Empty;
                        else
                            localDBTraitTable.DefaultView.RowFilter = "display_member like '%" + ux_textboxFindDescriptor.Text + "%'";
                        
                        localDBTraitTable.DefaultView.Sort = "display_member ASC";
                        ux_listBoxTrait.ValueMember = "value_member";
                        ux_listBoxTrait.DisplayMember = "display_member";
                        ux_listBoxTrait.DataSource = localDBTraitTable.DefaultView;
                    }
                }

            }
            catch (Exception ex)
            {
                GGMessageBox mb = new GGMessageBox("\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_Button_Add_Descriptor_Click(object sender, EventArgs e)
        {
            if (ux_listBoxTrait.SelectedItem == null) return;

            if (ux_listBoxCategory.SelectedValue.ToString().Equals("PASSPORT"))
            {
                if (!_descriptors.ContainsKey(ux_listBoxTrait.SelectedValue.ToString()))
                {
                    string selectedValue = ux_listBoxTrait.SelectedValue.ToString();
                    _descriptors.Add(selectedValue, ux_listBoxTrait.Text);

                    if (GroupColumnMan.ContainsKey(selectedValue)) {
                        foreach (var column_name in GroupColumnMan[selectedValue].RequiredColumns)
                        {
                            _main2.Columns.Add(column_name);
                            _main2.Columns[column_name].Caption = column_name;
                            ux_datagridviewSheet.Columns[column_name].Tag = new ColumnTag("GROUP_TRAIT");
                            ux_datagridviewSheet.Columns[column_name].SortMode = DataGridViewColumnSortMode.Programmatic;
                        }
                    }
                    else
                    {
                        _main2.Columns.Add(selectedValue);
                        _main2.Columns[selectedValue].Caption = ux_listBoxTrait.Text;
                        ux_datagridviewSheet.Columns[selectedValue].HeaderText = ux_listBoxTrait.Text;
                        
                        ux_datagridviewSheet.Columns[selectedValue].Tag = new ColumnTag("PASSPORT");
                        ux_datagridviewSheet.Columns[selectedValue].SortMode = DataGridViewColumnSortMode.Programmatic;
                    }

                    if (ux_listBoxTrait.SelectedValue.ToString().Equals(PassportCode.ACCESSION_SOURCE_GEOGRAPHY))
                    {
                        ux_datagridviewSheet.Columns["source_type_code"].Visible = true;
                    }

                    ux_listBoxDescriptors.DataSource = null;
                    ux_listBoxDescriptors.DataSource = new BindingSource(_descriptors, null);
                    ux_listBoxDescriptors.ValueMember = "Key";
                    ux_listBoxDescriptors.DisplayMember = "Value";
                }
            }
            else
            {
                if (!_descriptors.ContainsKey(ux_listBoxTrait.SelectedValue.ToString()))
                {
                    _descriptors.Add(ux_listBoxTrait.SelectedValue.ToString(), ux_listBoxTrait.Text);

                    ux_listBoxDescriptors.DataSource = null;
                    ux_listBoxDescriptors.DataSource = new BindingSource(_descriptors, null);
                    ux_listBoxDescriptors.ValueMember = "Key";
                    ux_listBoxDescriptors.DisplayMember = "Value";
                    
                    DataRowView desc = (DataRowView)ux_listBoxTrait.SelectedItem;
                    
                    /**/
                    _main2.Columns.Add(ux_listBoxTrait.SelectedValue.ToString());
                    _main2.Columns[ux_listBoxTrait.SelectedValue.ToString()].Caption = ux_listBoxTrait.Text;
                    ux_datagridviewSheet.Columns[ux_listBoxTrait.SelectedValue.ToString()].HeaderText = ux_listBoxTrait.Text;

                    ux_datagridviewSheet.Columns[ux_listBoxTrait.SelectedValue.ToString()].Tag = new ColumnTag("CROP_TRAIT");
                    ux_datagridviewSheet.Columns[ux_listBoxTrait.SelectedValue.ToString()].SortMode = DataGridViewColumnSortMode.Programmatic;
                }
            }
        }

        private void ux_buttonRemoveDescriptor_Click(object sender, EventArgs e)
        {
            if (ux_listBoxDescriptors.SelectedItem == null) return;

            string key = ux_listBoxDescriptors.SelectedValue.ToString();
            if (_descriptors.Remove(key)) {
                ux_listBoxDescriptors.DataSource = null;
                
                if(_descriptors.Count > 0) { 
                    ux_listBoxDescriptors.DataSource = new BindingSource(_descriptors, null);
                    ux_listBoxDescriptors.ValueMember = "Key";
                    ux_listBoxDescriptors.DisplayMember = "Value";
                }

                if (GroupColumnMan.ContainsKey(key))
                {
                    foreach (var column_name in GroupColumnMan[key].RequiredColumns)
                    {
                        _main2.Columns.Remove(column_name);
                    }
                }
                else
                    _main2.Columns.Remove(key);

                if (key.Equals(PassportCode.ACCESSION_SOURCE_GEOGRAPHY))
                {
                    ux_datagridviewSheet.Columns["source_type_code"].Visible = false;
                }
            }
        }
        
        public bool ProcessDGVEditShortcutKeys(DataGridView dgv, KeyEventArgs e, string cno, LookupTables lookupTables) {
            bool keyProcessed = false;

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            if (e.KeyCode == Keys.V && e.Control)
            {
                IDataObject dataObj = Clipboard.GetDataObject();
                string pasteText = "";
                
                if (dataObj.GetDataPresent(System.Windows.Forms.DataFormats.UnicodeText))
                {
                    char[] rowDelimiters = new char[] { '\r', '\n' };
                    char[] columnDelimiters = new char[] { '\t' };
                    int badRows = 0;
                    int missingRows = 0;
                    bool importSuccess = false;

                    // Processing keystroke...
                    keyProcessed = true;

                    pasteText = dataObj.GetData(DataFormats.UnicodeText).ToString();
                    
                    DataTable dt = (DataTable)dgv.DataSource;
                    importSuccess = ImportTextToDataTableUsingKeys(pasteText, dt, rowDelimiters, columnDelimiters, out badRows, out missingRows, lookupTables);
                    if (!importSuccess)
                    {
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("An error has been ocurred", "Error", MessageBoxButtons.OKCancel, MessageBoxDefaultButton.Button2);
                        ggMessageBox.ShowDialog();
                    }
                    
                }
            }

            if (e.KeyCode == Keys.C && e.Control)
            {
                StringBuilder copyString = new StringBuilder();
                // First we need to get the min/max rows and columns for the selected cells...
                int minCol = dgv.Columns.Count;
                int maxCol = -1;
                int minRow = dgv.Rows.Count;
                int maxRow = -1;

                // Processing keystroke...
                keyProcessed = true;

                foreach (DataGridViewCell dgvc in dgv.SelectedCells)
                {
                    if (dgvc.ColumnIndex < minCol) minCol = dgvc.ColumnIndex;
                    if (dgvc.ColumnIndex > maxCol) maxCol = dgvc.ColumnIndex;
                    if (dgvc.RowIndex < minRow) minRow = dgvc.RowIndex;
                    if (dgvc.RowIndex > maxRow) maxRow = dgvc.RowIndex;
                }

                // First, gather the column headers (but only if the entire row was selected)...
                if (dgv.SelectedRows.Count != 0)
                {
                    for (int i = minCol; i <= maxCol; i++)
                    {
                        copyString.Append(dgv.Columns[i].HeaderText);
                        if(i == maxCol)
                            copyString.Append("\r\n");
                        else
                            copyString.Append("\t");
                    }
                }
                // Now build the string to pass to the clipboard...
                for (int i = minRow; i <= maxRow; i++)
                {
                    for (int j = minCol; j <= maxCol; j++)
                    {
                        switch (dgv[j, i].FormattedValueType.Name)
                        {
                            case "Boolean":
                                copyString.Append(dgv[j, i].Value.ToString());
                                break;
                            default:
                                if (dgv[j, i].FormattedValue == null || dgv[j, i].FormattedValue.ToString().ToLower() == "[null]")
                                {
                                    copyString.Append("");
                                }
                                else
                                {
                                    copyString.Append( dgv[j, i].FormattedValue.ToString());
                                }
                                break;
                        }

                        if (j == maxCol) {
                            if(i < maxRow)
                                copyString.Append("\r\n");
                        }
                        else
                            copyString.Append("\t");
                    }
                }

                // Pass the new string to the clipboard...
                Clipboard.SetDataObject(copyString, false, 1, 1000);
                
            }

            if (e.KeyCode == Keys.Delete)
            {
                // Processing keystroke...
                keyProcessed = true;

                if (dgv.SelectedRows.Count == 0)
                {
                    // The user is deleting values from individual selected cells (not entire rows)...
                    foreach (DataGridViewCell dgvc in dgv.SelectedCells)
                    {
                        DataRowView drv = (DataRowView)dgvc.OwningRow.DataBoundItem;
                        if (drv == null) //if (dgv.Rows[row].IsNewRow)
                        {
                            dgvc.Value = "";
                            dgv.UpdateCellValue(dgvc.ColumnIndex, dgvc.RowIndex);
                        }
                        else
                        {
                            if (!drv[dgvc.OwningColumn.Index].Equals(DBNull.Value))
                            {
                                if (!dgvc.ReadOnly)
                                {
                                    // Edit the DataRow (not the DataRowView) so that row state is changed...
                                    drv.Row[dgvc.OwningColumn.Index] = DBNull.Value;
                                    // For unbound text cells we have to manually clear the cell's text...
                                    if (string.IsNullOrEmpty(dgvc.OwningColumn.DataPropertyName)) dgvc.Value = "";
                                    dgv.UpdateCellValue(dgvc.ColumnIndex, dgvc.RowIndex);
                                }
                            }
                        }
                    }
                }
                else
                {
                    // The user is attempting to delete entire rows from the datagridview...
                    
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You are about to remove {0} rows!\n\nAre you sure you want to do this?", "Rows Remove Confirmation", MessageBoxButtons.OKCancel, MessageBoxDefaultButton.Button2);
                    ggMessageBox.Name = "UserInterfaceUtils_ProcessDGVEditShortcutKeysMessage1";
                    
                    string[] argsArray = new string[100];

                    int rowCount = 0;
                    foreach (DataGridViewRow dgvr in dgv.SelectedRows)
                    {
                        if (!dgvr.IsNewRow && dgvr.Visible)
                            rowCount++;
                    }
                    argsArray[0] = rowCount.ToString();
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    
                    if (DialogResult.OK == ggMessageBox.ShowDialog())
                    {
                        foreach (DataGridViewRow dgvr in dgv.SelectedRows)
                        {
                            if(!dgvr.IsNewRow && dgvr.Visible)
                                dgv.Rows.Remove(dgvr);
                        }

                        //ux_labelRows.Text = string.Format(ux_labelRows.Tag.ToString(), ux_datagridviewSheet.Rows.Count, ux_datagridviewSheet.Rows.Count);
                    }
                    e.Handled = true;
                }
            }

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;

            return keyProcessed;
        }

        public bool ImportTextToDataTableUsingKeys(string rawImportText, DataTable destinationTable, char[] rowDelimiters, char[] columnDelimiters, out int badRows, out int missingRows, LookupTables lookupTables)
        {
            
            string scrubbedRawImportText = "";

            // Attempting to remove new lines with no matching carriage return (ex. \n with no leading \r) and vice versa (ex. \r with no trailing \n)...
            // First protect the well-formed carriage return line feed (\r\n) by temp. removing it and substituting a placeholder (to aid in weeding out lone \n and \r)...
            scrubbedRawImportText = rawImportText.Replace("\r\n", "***well-formed carriage return line feed***").Replace("\n\r", "***well-formed carriage return line feed***");
            // Now remove any remaining lone \n or \r that cannot be processed properly...
            
            // Next return the well-formed carriage return line feeds back where they belong...
            scrubbedRawImportText = scrubbedRawImportText.Replace("***well-formed carriage return line feed***", "\r\n");
            
            string[] rawImportRows = scrubbedRawImportText.Split(rowDelimiters, StringSplitOptions.RemoveEmptyEntries);
            
            bool primaryKeyFound = false;
            bool processedImportSuccessfully = true;
            int primaryKeyIndex = -1;
            badRows = 0;
            missingRows = 0;
            
            if (rawImportRows == null || rawImportRows.Length <= 0) return false;

            try
            {
                string[] importColumnNames = rawImportRows[0].Split(columnDelimiters, StringSplitOptions.None);
                System.Collections.Generic.Dictionary<int, int> importedColumns = new System.Collections.Generic.Dictionary<int, int>();

                for (int i = 0; i < importColumnNames.Length; i++)
                {
                    if (!primaryKeyFound && importColumnNames[i].ToUpper().Equals(ux_datagridviewSheet.Columns["ACCENUMB"].HeaderText.ToUpper()))
                    {
                        primaryKeyFound = true;
                        primaryKeyIndex = i;
                        importedColumns.Add(i, 0);
                        continue;
                    }

                    for (int j = 1; j < destinationTable.Columns.Count; j++)
                    {
                        if (importColumnNames[i].ToUpper().Trim().Equals(destinationTable.Columns[j].Caption.ToUpper().Trim()))
                        {
                            importedColumns.Add(i, j);
                            break;
                        }
                    }
                }

                for (int iRow = 1; iRow < rawImportRows.Length; iRow++)
                {
                    DataRow toInsert;

                    string[] sourceRow = rawImportRows[iRow].Split(columnDelimiters, StringSplitOptions.None);


                    DataRow[] Rows = destinationTable.Select("[ACCENUMB]='" + sourceRow[primaryKeyIndex] + "'");
                    if (!Rows.Any())
                    {
                        toInsert = destinationTable.NewRow();
                    }
                    else
                    {
                        toInsert = Rows[0];
                    }

                    foreach (var col in importedColumns)
                    {
                        toInsert[col.Value] = sourceRow[col.Key];
                    }

                    if (!Rows.Any())
                        destinationTable.Rows.Add(toInsert);
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.Write(ex.Message);
                processedImportSuccessfully = false;
            }
            
            return processedImportSuccessfully;
        }

        private void ux_listBoxTrait_DoubleClick(object sender, EventArgs e)
        {
            ux_Button_Add_Descriptor_Click(null, null);
        }
        
        private void ux_buttonSaveData_Click(object sender, EventArgs e)
        {
            try
            {
                //Check validation error
                if (validationErrorCount != 0)
                    throw new WizardException("Fix validation errors before saving", "ux_textboxErrorFixErrorsBeforeSaving");
                if (validationOKWithChangesCount == 0)
                    throw new WizardException("No changes detected", "ux_textboxErrorNoModificationsFound");

                ux_labelMessage.Text = string.Empty;
                ux_buttonSaveData.Enabled = false;
                ux_progressBarWorking.Visible = true;
                ux_progressBarWorking.Style = ProgressBarStyle.Marquee;
                
                System.Threading.Thread thread =
                  new System.Threading.Thread(new System.Threading.ThreadStart(saveData2));
                thread.Start();
            }
            catch (WizardException wex)
            {
                Cursor.Current = Cursors.Arrow;
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "BatchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                GGMessageBox mb = new GGMessageBox("\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accnumb"></param>
        /// <returns></returns>
        /// <exception cref="Exception">If accnumb doesnt match regular expression.</exception>
        private string getAccessionNumber(string accnumb) {
            
            Regex rgx = new Regex(@"^[a-zA-Z0-9]+ [0-9]+( [A-Za-z0-9]*)?$");
            if (! rgx.IsMatch(accnumb))
                throw new WizardException("Regular expression is not matched");

            StringBuilder sb = new StringBuilder();
            int index1 = accnumb.IndexOf(" ");
            if (index1 > -1)
            {
                sb.Append(accnumb.Substring(0, index1 + 1));
                int index2 = accnumb.IndexOf(" ", index1 + 1);
                if (index2 > -1)
                {
                    int part2 = int.Parse(accnumb.Substring(index1 + 1, index2 - (index1 + 1)));
                    sb.Append(part2);
                    if (accnumb.Length > index2 + 1)
                    {
                        sb.Append(" " + accnumb.Substring(index2 + 1, accnumb.Length - (index2 + 1)));
                    }
                    else
                        throw new WizardException("part3 is not found");
                }
                else
                {
                    if (accnumb.Length > index1 + 1)
                    {
                        int part2 = int.Parse(accnumb.Substring(index1 + 1, accnumb.Length - (index1 + 1)));
                        sb.Append(part2);
                    }
                    else
                    {
                        throw new WizardException("part 2 is not found");
                    }
                }
            }
            else
                throw new WizardException("1st separator is not found");
            
            return sb.ToString();
        }

        
        private void SaveAccessionInvName(DataGridView datagridviewContainer, DataTable dtTableUpdate, string category_code, int? plant_name_rank) {
            var accessionInvNameCultivarRows = dtTableUpdate.Select("table_name = 'accession_inv_name' and filter_value = '" + category_code + "'");
            if (accessionInvNameCultivarRows.Count() > 0)
            {
                List<int> inventoryList = new List<int>();
                foreach (DataRow r in accessionInvNameCultivarRows)
                {
                    inventoryList.Add(r.Field<int>("table_id"));
                }
                string columnName = accessionInvNameCultivarRows[0].Field<DataGridViewCell>("cell").OwningColumn.Name;
                //int columnIndex = accessionInvNameCultivarRows[0].Field<DataGridViewCell>("cell").ColumnIndex;

                DataSet dsAccessionInvName = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_acc_inv_name_update", ":inventoryid=" + string.Join(",", inventoryList) + ";:categorycode=" + category_code, 0, 0);
                if (dsAccessionInvName != null && dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"] != null)
                {
                    DataTable dtAccessionInvName = dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"];
                    foreach (DataRow r in accessionInvNameCultivarRows)
                    {
                        DataRow[] rupdate = dtAccessionInvName.Select("inventory_id = " + r.Field<int>("table_id"));
                        if (rupdate.Count() == 0)
                        {
                            DataRow dr = dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"].NewRow();
                            dr["inventory_id"] = r.Field<int>("table_id");
                            dr["category_code"] = category_code;
                            dr["plant_name"] = r.Field<string>("string_value");
                            dr.SetField<int?>("plant_name_rank", plant_name_rank);
                            dr["is_web_visible"] = "Y";
                            dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"].Rows.Add(dr);

                            (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(r.Field<string>("string_value"))){
                                rupdate[0].Delete();
                            }
                            else if (!rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                            {
                                rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                            }
                        }
                    }

                    DataSet saveErrors = new DataSet();
                    saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionInvName);
                    if (saveErrors != null && saveErrors.Tables.Contains("acc_batch_wizard_get_acc_inv_name_update"))
                    {
                        foreach (DataRow dr in saveErrors.Tables["acc_batch_wizard_get_acc_inv_name_update"].Rows)
                        {
                            DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                            if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                            {
                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                savingErrorCount++;
                                /*MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                    , "", dr["ExceptionMessage"].ToString()));*/
                            }
                            else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                            {
                                row.Cells[columnName].Style.BackColor = StyleColors.SavingOk;
                                row.Cells[columnName].ErrorText = string.Empty;
                                savingOKCount++;
                            }
                            if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                            {
                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                savingErrorCount++;
                            }
                            else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                            {
                                row.Cells[columnName].Style.BackColor = StyleColors.SavingOk;
                                row.Cells[columnName].ErrorText = string.Empty;
                                savingOKCount++;
                            }
                            if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() != "Success")
                            {
                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                savingErrorCount++;
                            }
                            else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() == "Success")
                            {
                                row.Cells[columnName].Style.BackColor = StyleColors.SavingOk;
                                row.Cells[columnName].ErrorText = string.Empty;
                                savingOKCount++;
                            }
                        }
                    }
                }
            }
        }

        private void ux_datagridviewMain2_KeyDown(object sender, KeyEventArgs e)
        {
            ProcessDGVEditShortcutKeys(ux_datagridviewSheet, e, "", null);
        }

        private void ux_buttonTemplate_Click(object sender, EventArgs e)
        {
            TemplateWizard tw = new TemplateWizard("", _sharedUtils);
            tw.Show();
        }

        private void ux_buttonClear_Click(object sender, EventArgs e)
        {
            _main2.Rows.Clear();
            ClearCounters();
        }

        private void ux_checkBoxShowOnlyRowsWithError_CheckedChanged(object sender, EventArgs e)
        {
            if (ux_checkBoxShowOnlyRowsWithError.Checked)
            {
                CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[ux_datagridviewSheet.DataSource];
                currencyManager1.SuspendBinding();
                int errorRowCount = 0;
                foreach (DataGridViewRow gvrow in ux_datagridviewSheet.Rows)
                {   
                    if (gvrow.ErrorText.Equals(""))
                    {
                        bool hasError = false;
                        foreach (DataGridViewCell cell in gvrow.Cells)
                        {
                            if (!cell.ErrorText.Equals(""))
                            {
                                hasError = true;
                                errorRowCount++;
                                break;
                            }
                        }
                        if (!hasError)
                            gvrow.Visible = false;
                    }
                    else
                    {
                        errorRowCount++;
                    }
                }
                currencyManager1.ResumeBinding();

                ux_labelRows.Text = string.Format(ux_labelRows.Tag.ToString(), errorRowCount, ux_datagridviewSheet.Rows.Count);
            }
            else {
                foreach (DataGridViewRow gvrow in ux_datagridviewSheet.Rows)
                {
                    gvrow.Visible = true;
                }
                ux_labelRows.Text = string.Format(ux_labelRows.Tag.ToString(), ux_datagridviewSheet.Rows.Count, ux_datagridviewSheet.Rows.Count);
            }
        }

        private void ux_comboboxMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ux_comboboxMethod.DataSource == null) return;
            if (ux_comboboxMethod.SelectedItem != null)
                _method_id = ux_comboboxMethod.SelectedValue.ToString();
            else
                _method_id = "-1";
        }

        private void ux_datagridviewSheet_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStripMenu.Show(ux_datagridviewSheet, new Point(e.X, e.Y));
            }
        }

        private void copyHeadersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder builder = new StringBuilder();

            var columnsInOrder = ux_datagridviewSheet.Columns.Cast<DataGridViewColumn>().OrderBy(x => x.DisplayIndex);
            foreach (var column in columnsInOrder)
            {
                if (column.Visible)
                    builder.Append(column.HeaderText).Append("\t");
            }
            Clipboard.SetText(builder.ToString().TrimEnd(new char[] { '\t' }));
        }

        private void updateLookupTablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _sharedUtils.LookupTablesLoadTableFromDatabase("accession_lookup");
            _sharedUtils.LookupTablesLoadTableFromDatabase("inventory_lookup");
        }

        private void ux_buttonShowDefinition_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (ux_listBoxTrait.SelectedItem == null) return;

                if (ux_listBoxCategory.SelectedValue.ToString().Equals("PASSPORT"))
                {
                    DataSet descriptorDetail = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_mcpd_detail", ":mcpd=" + ux_listBoxTrait.SelectedValue.ToString(), 0, 0);
                    if (descriptorDetail != null && descriptorDetail.Tables.Contains("acc_batch_wizard_get_mcpd_detail"))
                    {
                        if (descriptorDetail.Tables["acc_batch_wizard_get_mcpd_detail"].Rows.Count > 0)
                        {
                            descriptorDetail.Tables["acc_batch_wizard_get_mcpd_detail"].Columns.Add("crop_name");
                            descriptorDetail.Tables["acc_batch_wizard_get_mcpd_detail"].Rows[0].SetField<string>("crop_name", ux_listBoxCrop.Text);


                            DescriptorDetailForm dd = new DescriptorDetailForm(_sharedUtils, descriptorDetail.Tables["acc_batch_wizard_get_mcpd_detail"]);
                            dd.Show();
                        }
                    }
                }
                else
                {
                    DataSet descriptorDetail = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_descriptor_detail", ":descriptorid=" + ux_listBoxTrait.SelectedValue.ToString(), 0, 0);
                    if (descriptorDetail != null && descriptorDetail.Tables.Contains("acc_batch_wizard_get_descriptor_detail"))
                    {
                        if (descriptorDetail.Tables["acc_batch_wizard_get_descriptor_detail"].Rows.Count > 0)
                        {
                            DescriptorDetailForm dd = new DescriptorDetailForm(_sharedUtils, descriptorDetail.Tables["acc_batch_wizard_get_descriptor_detail"]);
                            dd.Show();
                        }
                    }
                }
                Cursor.Current = Cursors.Arrow;
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Arrow;
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void ux_buttonValidateData_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if(ux_comboboxMethod.SelectedItem == null)
                {
                    foreach (DataGridViewColumn gridColumn in ux_datagridviewSheet.Columns)
                    {
                        if (gridColumn.Tag != null && (gridColumn.Tag as ColumnTag).Category.Equals("CROP_TRAIT"))
                            throw new WizardException("Method is empty", "ux_textboxErrorMethodIsEmpty");
                    }
                }
                
                _dsObservationUpdate = null;

                ClearCounters();
                bool showCooperatorWarning = false;

                DataGridView datagridviewContainer = ux_datagridviewSheet;
                DataTable dtSourceContainer = _main2;
                var watch = System.Diagnostics.Stopwatch.StartNew();

                List<string> AccessionNumbers = new List<string>();
                List<int> crop_trait_ids = new List<int>();
                
                //Obtain all crop_trait_ids from columns
                foreach (DataGridViewColumn col in datagridviewContainer.Columns)
                {
                    if (col.Index < startDescriptorsIndex) continue;
                    int crop_trait_id = -1;
                    int.TryParse(col.Name, out crop_trait_id);
                    if (crop_trait_id != -1)
                        crop_trait_ids.Add(crop_trait_id);
                }
                
                foreach (DataGridViewRow row in datagridviewContainer.Rows)
                {
                    for (int iCol = 0; iCol < ux_datagridviewSheet.Columns.Count; iCol++)
                    {
                        row.Cells[iCol].Style.BackColor = Color.LightGray;
                        if(row.Cells[iCol].Value != null)
                        {
                            row.Cells[iCol].Value = row.Cells[iCol].Value.ToString().Trim();
                        }
                    }
                }

                DataTable newAccessions = null;
                List<DataGridViewRow> dgvrowsNewAccessions = new List<DataGridViewRow>();
                DataSet dsAccession = _sharedUtils.GetWebServiceData("accession_batch_wizard_get_accession", ":accessionid=-1", 0, 0);
                if (dsAccession != null && dsAccession.Tables["accession_batch_wizard_get_accession"] != null)
                    newAccessions = dsAccession.Tables["accession_batch_wizard_get_accession"];
                else
                {
                    throw new WizardException("Error ocurred getting dataview \"accession_batch_wizard_get_accession\"", "ux_textboxErrorGettingDataview", "accession_batch_wizard_get_accession");
                }

                List<string> cooperatorFAONumbers = new List<string>();
                // Identify existing and new accessions

                foreach (DataGridViewRow row in datagridviewContainer.Rows)
                {
                    if (row.Cells[0].Value == null || row.IsNewRow) continue;
                    if (string.IsNullOrEmpty(row.Cells[0].Value.ToString().Trim())) continue;

                    // Add cipnumbers to check if exist
                    row.Cells[0].Value = row.Cells[0].Value.ToString().Trim();
                    AccessionNumbers.Add("'" + row.Cells[0].Value.ToString() + "'");
                    
                    //Get all FAO Numbers
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.COLLCODE) && !string.IsNullOrEmpty(row.Cells[PassportCode.COLLCODE].Value.ToString()))
                    {
                        var faoNumbers = row.Cells[PassportCode.COLLCODE].Value.ToString().Split(new char[] { ';' });
                        foreach (var item in faoNumbers)
                        {
                            cooperatorFAONumbers.Add("'" + item + "'");
                        }
                    }
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.BREDCODE) && !string.IsNullOrEmpty(row.Cells[PassportCode.BREDCODE].Value.ToString()))
                    {
                        var faoNumbers = row.Cells[PassportCode.BREDCODE].Value.ToString().Split(new char[] { ';' });
                        foreach (var item in faoNumbers)
                        {
                            cooperatorFAONumbers.Add("'" + item + "'");
                        }
                    }
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.DONORCODE) && !string.IsNullOrEmpty(row.Cells[PassportCode.DONORCODE].Value.ToString()))
                    {
                        var faoNumbers = row.Cells[PassportCode.DONORCODE].Value.ToString().Split(new char[] { ';' });
                        foreach (var item in faoNumbers)
                        {
                            cooperatorFAONumbers.Add("'" + item + "'");
                        }
                    }
                    
                }

                if (AccessionNumbers.Count > 0)
                {
                    //Get all accession_ids and inventory_ids
                    DataSet dsAccessionInventoryLookup = _sharedUtils.GetWebServiceData("accession_batch_wizard_get_inventoryid", ":accnumber=" + string.Join(",", AccessionNumbers), 0, 0);
                    DataTable dtAccessionInventoryLookup = null;
                    if (dsAccessionInventoryLookup != null && dsAccessionInventoryLookup.Tables.Contains("accession_batch_wizard_get_inventoryid"))
                    {
                        dtAccessionInventoryLookup = dsAccessionInventoryLookup.Tables["accession_batch_wizard_get_inventoryid"];
                    }
                    else
                    {
                        throw new WizardException("An error ocurred while getting accession_ids, inventory_ids", "ux_textboxErrorGettingDataview", "accession_batch_wizard_get_inventoryid");
                    }
                    #region ACCNUMB
                    //Validate Accession numbers
                    foreach (DataGridViewRow row in datagridviewContainer.Rows)
                    {
                        if (row.IsNewRow) continue;

                        //Clean errors
                        row.ErrorText = string.Empty;
                        //row.Cells[0].Style.BackColor = Color.White;
                        row.Cells[0].ErrorText = "";

                        if (row.Cells[0].Value == null || string.IsNullOrEmpty(row.Cells[0].Value.ToString()))
                        {
                            row.ErrorText = "Accession Number is blank";
                            row.Cells[0].ErrorText = "Accession Number is blank";
                            row.Cells[0].Style.BackColor = StyleColors.ValidationError;
                            validationErrorCount++;
                            continue;
                        }
                        
                        string ACCNumber = row.Cells[0].Value.ToString().Trim();

                        var resp = dtAccessionInventoryLookup.Select("AccNumber = '" + ACCNumber + "'");
                        if (resp != null && resp.Any())
                        {
                            row.Cells[0].ErrorText = string.Empty;
                            row.Cells[0].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                            #region accession_number_exists
                            //copy accession_id and inventory_id
                            row.Cells["accession_id"].Value = resp[0].Field<int>("accession_id");
                            row.Cells["inventory_id"].Value = resp[0].Field<int>("inventory_id");
                            #endregion
                        }
                        else
                        {
                            row.Cells[0].ErrorText = "Accession not found\n\nIf you want to update Accession Number use \"get_accession\" dataview or Accession Wizard";
                            row.Cells[0].Style.BackColor = StyleColors.ValidationError;
                            validationErrorCount++;
                        }
                    }
                    #endregion

                    List<int> accessionIds = new List<int>();
                    List<int> inventoryIds = new List<int>();
                    foreach (DataRow r in dtAccessionInventoryLookup.Rows) {
                        accessionIds.Add(r.Field<int>("accession_id"));
                        inventoryIds.Add(r.Field<int>("inventory_id"));
                    }

                    #region Get_crop_trait
                    _dsCropTraitIsCoded = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_crop_trait_type", ":croptraitid=" + string.Join(",", crop_trait_ids), 0, 0);
                    DataTable dtCropTraitIsCoded = null;
                    if (_dsCropTraitIsCoded != null && _dsCropTraitIsCoded.Tables.Contains("acc_batch_wizard_get_crop_trait_type"))
                    {
                        dtCropTraitIsCoded = _dsCropTraitIsCoded.Tables["acc_batch_wizard_get_crop_trait_type"];
                    }
                    else
                    {
                        throw new WizardException("Error ocurred getting dataview \"acc_batch_wizard_get_crop_trait_type\"", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_crop_trait_type");
                    }
                    #endregion

                    #region Get_current_descriptor_values

                    //crop_trait_observation
                    DataTable dtCropTraitObservationUpdate = null;
                    if (crop_trait_ids.Count > 0)
                    {
                        string parameters = ":inventoryid=" + string.Join(",", inventoryIds) + ";:croptraitid=" + string.Join(",", crop_trait_ids) + ";:methodid=" + _method_id;
                        string dataviewCropTraitObservation = "accession_batch_wizard_get_crop_trait_observation";
                        _dsObservationUpdate = _sharedUtils.GetWebServiceData(dataviewCropTraitObservation, parameters, 0, 0);
                        if (_dsObservationUpdate != null && _dsObservationUpdate.Tables.Contains(dataviewCropTraitObservation))
                        {
                            dtCropTraitObservationUpdate = _dsObservationUpdate.Tables[dataviewCropTraitObservation];
                        }
                        else
                            throw new WizardException("Error ocurred getting dataview \"" + dataviewCropTraitObservation + "\"", "ux_textboxErrorGettingDataview", dataviewCropTraitObservation);
                    }

                    #region accession_inv_name
                    string accessionInvNameCategoryCode = string.Empty;
                    /*if (ux_datagridviewSheet.Columns.Contains(PassportCode.PUID)) accessionInvNameCategoryCode += (!string.IsNullOrEmpty(accessionInvNameCategoryCode) ? "," : "") + "'DOI'";
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.ACCENAME)) accessionInvNameCategoryCode += (!string.IsNullOrEmpty(accessionInvNameCategoryCode) ? "," : "") + "'CULTIVAR'";
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.OTHERNUMB)) accessionInvNameCategoryCode += (!string.IsNullOrEmpty(accessionInvNameCategoryCode) ? "," : "") + "'OTHER'";
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.COLLMISSID)) accessionInvNameCategoryCode += (!string.IsNullOrEmpty(accessionInvNameCategoryCode) ? "," : "") + "'EXPLOREID'";
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.DONORNUMB)) accessionInvNameCategoryCode += (!string.IsNullOrEmpty(accessionInvNameCategoryCode) ? "," : "") + "'DONOR'";
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.COLLNUMB)) accessionInvNameCategoryCode += (!string.IsNullOrEmpty(accessionInvNameCategoryCode) ? "," : "") + "'COLLECTOR'";
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.BREEDER_CODE)) accessionInvNameCategoryCode += (!string.IsNullOrEmpty(accessionInvNameCategoryCode) ? "," : "") + "'BREEDER'";
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.BIOSAFETY_CODE)) accessionInvNameCategoryCode += (!string.IsNullOrEmpty(accessionInvNameCategoryCode) ? "," : "") + "'BIOSAFETY'";
                    */
                    foreach (DataGridViewColumn col in ux_datagridviewSheet.Columns)
                    {
                        var accessionNameColumn = dtPassportCriteria.AsEnumerable().FirstOrDefault(x => x.Field<string>("value_member").Equals(col.Name) && x.Field<string>("table_name").Equals("accession_inv_name"));
                        if (accessionNameColumn != null)
                        {
                            accessionInvNameCategoryCode += (!string.IsNullOrEmpty(accessionInvNameCategoryCode) ? "," : "") + "'" + accessionNameColumn.Field<string>("category_code") + "'";
                        }
                    }

                    if (!string.IsNullOrEmpty(accessionInvNameCategoryCode))
                    {
                        _dsAccessionInvName = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_acc_inv_name_update", ":inventoryid=" + string.Join(",", inventoryIds) + ";:categorycode=" + accessionInvNameCategoryCode, 0, 0);
                        if (_dsAccessionInvName != null && _dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"] != null)
                        {
                            _dtAccessionInvName = _dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"];
                        }
                        else
                        {
                            throw new WizardException("Error ocurred getting dataview \"acc_batch_wizard_get_acc_inv_name_update\"", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_acc_inv_name_update");
                        }
                    }
                    #endregion

                    //cooperator for accession_source_map
                    if (cooperatorFAONumbers.Count > 0)
                    {
                        _dsCooperator = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_cooperator", ":secondaryorganizationabbrev=" + string.Join(",", cooperatorFAONumbers), 0, 0);
                        if (_dsCooperator != null && _dsCooperator.Tables.Contains("acc_batch_wizard_get_cooperator"))
                            _dtCooperator = _dsCooperator.Tables["acc_batch_wizard_get_cooperator"];
                        else
                            throw new WizardException("Error ocurred getting dataview \"acc_batch_wizard_get_cooperator\"", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_cooperator");
                    }
                    //accession_source_map
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.COLLCODE) || ux_datagridviewSheet.Columns.Contains(PassportCode.BREDCODE) || ux_datagridviewSheet.Columns.Contains(PassportCode.DONORCODE))
                    {
                        _dsAccessionSourceMap = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_acc_source_map", ":accessionid=" + string.Join(",", accessionIds), 0, 0);
                        if (_dsAccessionSourceMap != null && _dsAccessionSourceMap.Tables.Contains("acc_batch_wizard_get_acc_source_map"))
                            _dtAcessionSourceMap = _dsAccessionSourceMap.Tables["acc_batch_wizard_get_acc_source_map"];

                        _dsAccessionSourceMapExt = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_acc_source_map_ext", ":accessionid=" + string.Join(",", accessionIds), 0, 0);
                        if(_dsAccessionSourceMapExt!= null && _dsAccessionSourceMapExt.Tables.Contains("acc_batch_wizard_get_acc_source_map_ext"))
                        {
                            _dtAccessionSourceMapExt = _dsAccessionSourceMapExt.Tables["acc_batch_wizard_get_acc_source_map_ext"];
                        }
                        else
                            throw new WizardException("Error ocurred getting dataview \"acc_batch_wizard_get_acc_source_map_ext\"", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_acc_source_map_ext");
                    }
                    //accession
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.ACQDATE) || ux_datagridviewSheet.Columns.Contains(PassportCode.SAMPSTAT) || ux_datagridviewSheet.Columns.Contains(PassportCode.REMARKS)
                        || ux_datagridviewSheet.Columns.Contains(PassportCode.ACCESSION_STATUS) || ux_datagridviewSheet.Columns.Contains(PassportCode.ACCESSION_TAXON) 
                        || (!_isCustomModificationNeeded && ux_datagridviewSheet.Columns.Contains(PassportCode.PUID)) )
                    {
                        _dsAccession = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_accession_update", ":accessionid=" + string.Join(",", accessionIds), 0, 0);
                        if (_dsAccession != null && _dsAccession.Tables.Contains("acc_batch_wizard_get_accession_update"))
                        {
                            _dtAcccession = _dsAccession.Tables["acc_batch_wizard_get_accession_update"];
                        }
                        else
                            throw new WizardException("Error ocurred getting dataview \"acc_batch_wizard_get_accession_update\"", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_accession_update");
                    }
                    //accession_ipr
                    if (ux_datagridviewSheet.Columns.Contains(PassportCode.MLSSTAT))
                    {
                        _dsAccessionIpr = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_accession_ipr", ":accessionid=" + string.Join(",", accessionIds), 0, 0);
                        if(_dsAccessionIpr != null && _dsAccessionIpr.Tables.Contains("acc_batch_wizard_get_accession_ipr"))
                        {
                            _dtAccessionIpr = _dsAccessionIpr.Tables["acc_batch_wizard_get_accession_ipr"];
                        }
                        else
                            throw new WizardException("Error ocurred getting dataview \"acc_batch_wizard_get_accession_ipr\"", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_accession_ipr");
                    }
                    //accession_pedigree
                    if(ux_datagridviewSheet.Columns.Contains(PassportCode.ANCEST) || ux_datagridviewSheet.Columns.Contains(PassportCode.PARENT_FEMALE)
                        || ux_datagridviewSheet.Columns.Contains(PassportCode.PARENT_MALE) || ux_datagridviewSheet.Columns.Contains(PassportCode.PARENT_FEMALE_ACCENUMB) || ux_datagridviewSheet.Columns.Contains(PassportCode.PARENT_MALE_ACCENUM))
                    {
                        _dsAccessionPedigree = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_accession_pedigree_update", ":accessionid=" + string.Join(",", accessionIds), 0 , 0);
                        if (_dsAccessionPedigree != null && _dsAccessionPedigree.Tables.Contains("acc_batch_wizard_get_accession_pedigree_update"))
                        {
                            _dtAccessionPedigree = _dsAccessionPedigree.Tables["acc_batch_wizard_get_accession_pedigree_update"];
                        }
                        else
                            throw new WizardException("Error ocurred getting dataview \"acc_batch_wizard_get_accession_pedigree_update\"", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_accession_pedigree_update");
                    }
                    //accession_source
                    if (ux_datagridviewSheet.Columns.Contains("13. Country of origin") || ux_datagridviewSheet.Columns.Contains(PassportCode.DECLATITUDE) 
                        || ux_datagridviewSheet.Columns.Contains(PassportCode.DECLONGITUDE) || ux_datagridviewSheet.Columns.Contains(PassportCode.ELEVATION) || ux_datagridviewSheet.Columns.Contains(PassportCode.COLLDATE)
                        || ux_datagridviewSheet.Columns.Contains(PassportCode.ACCESSION_SOURCE_FORMATTED_LOCALITY) || ux_datagridviewSheet.Columns.Contains(PassportCode.ACCESSION_SOURCE_VERBATIM_LOCALITY)
                        || ux_datagridviewSheet.Columns.Contains(PassportCode.COORDDATUM) || ux_datagridviewSheet.Columns.Contains(PassportCode.COORDUNCERT)
                        || ux_datagridviewSheet.Columns.Contains(PassportCode.GEOREFMETH) || ux_datagridviewSheet.Columns.Contains(PassportCode.ACCESSION_SOURCE_NOTE))
                    {
                        _dsAccessionSource = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_accession_source_update", ":accessionid=" + string.Join(",", accessionIds), 0, 0);
                        if (_dsAccessionSource != null && _dsAccessionSource.Tables.Contains("acc_batch_wizard_get_accession_source_update"))
                        {
                            _dtAccessionSource = _dsAccessionSource.Tables["acc_batch_wizard_get_accession_source_update"];
                        }
                        else
                            throw new WizardException("Error ocurred getting dataview \"acc_batch_wizard_get_accession_source_update\"", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_accession_source_update");
                    }

                    #endregion

                    //Validate descriptors values
                    foreach (DataGridViewRow row in datagridviewContainer.Rows)
                    {
                        if (row.Cells[0].Value == null || row.IsNewRow) continue;
                        if (string.IsNullOrEmpty(row.Cells[0].Value.ToString().Trim())) continue;

                        int rowInventoryId = -1;
                        int rowAcessionId = -1;
                        if( row.Cells["inventory_id"].Value != null && !string.IsNullOrEmpty(row.Cells["inventory_id"].Value.ToString())) //Existing accession
                        {
                            rowAcessionId = int.Parse(row.Cells["accession_id"].Value.ToString());
                            rowInventoryId = int.Parse(row.Cells["inventory_id"].Value.ToString());
                        }
                        else //new accession
                        {
                            continue;
                        }

                        foreach (DataGridViewColumn column in ux_datagridviewSheet.Columns)
                        {
                            if (column.Tag != null)
                            {
                                ColumnTag colTag = (ColumnTag)column.Tag;
                                DataGridViewCell cell = row.Cells[column.Name];
                                
                                if (colTag.Category.Equals("CROP_TRAIT"))
                                {
                                    #region CropTrait
                                    //set string.empty to null values
                                    if (row.Cells[column.Name].Value == null)
                                        row.Cells[column.Name].Value = string.Empty;
                                    string cellValue = row.Cells[column.Name].Value.ToString().Trim();

                                    int columnCropTraitId = int.Parse(column.Name);
                                    if(rowInventoryId > 0) {
                                        ///Validate format
                                        var dtCropTrait = _sharedUtils.GetLocalData("select value_member, type, max_length, numeric_maximum, numeric_minimum, numeric_format from acc_batch_wizard_crop_trait_lookup where value_member = " + columnCropTraitId, "");
                                        switch (dtCropTrait.Rows[0].Field<string>("type"))
                                        {
                                            case "CODED":
                                                var dtCropTraitCode = _sharedUtils.GetLocalData("select value_member, code from acc_batch_wizard_crop_trait_code_lookup where crop_trait_id = " + columnCropTraitId, "");
                                                if (!string.IsNullOrEmpty(row.Cells[column.Name].Value.ToString()) &&
                                                    !dtCropTraitCode.AsEnumerable().Any(x => string.Equals(x.Field<string>("code"), cellValue, ux_checkBoxMatchCase.Checked ? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase)))
                                                {
                                                    row.Cells[column.Name].ErrorText = "Code value not valid";
                                                    row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                    validationErrorCount++;
                                                    continue;
                                                }
                                                break;
                                            case "NUMERIC":
                                                decimal decimalValue = 0;
                                                if (!string.IsNullOrEmpty(cellValue))
                                                {
                                                    if (!decimal.TryParse(cellValue, out decimalValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = "Value format is not valid\n\n" + column.HeaderText + " must be numeric";
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                        continue;
                                                    }
                                                    if (!dtCropTrait.Rows[0].IsNull("numeric_minimum"))
                                                    {
                                                        if (decimalValue < decimal.Parse(dtCropTrait.Rows[0].Field<string>("numeric_minimum")))
                                                        {
                                                            row.Cells[column.Name].ErrorText = "Minimum value is " + dtCropTrait.Rows[0].Field<string>("numeric_minimum");
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                            continue;
                                                        }
                                                    }
                                                    if (!dtCropTrait.Rows[0].IsNull("numeric_maximum"))
                                                    {
                                                        if (decimalValue > decimal.Parse(dtCropTrait.Rows[0].Field<string>("numeric_maximum")))
                                                        {
                                                            row.Cells[column.Name].ErrorText = "Maximum value is " + dtCropTrait.Rows[0].Field<string>("numeric_maximum");
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                            continue;
                                                        }
                                                    }
                                                    if (!dtCropTrait.Rows[0].IsNull("numeric_format"))
                                                    {
                                                        string numericFormat = dtCropTrait.Rows[0].Field<string>("numeric_format").ToUpperInvariant();
                                                        if(numericFormat.Length>0 && numericFormat.Length <= 2)
                                                        {
                                                            if (numericFormat[0] == 'F' && char.IsDigit(numericFormat[1]))
                                                            {
                                                                int decimalPlaces = (int)char.GetNumericValue(numericFormat[1]);
                                                                if (Decimal.Round(decimalValue, decimalPlaces) != decimalValue)
                                                                {
                                                                    row.Cells[column.Name].ErrorText = "Decimal places allowed for \"" + column.HeaderText + "\" is " + decimalPlaces;
                                                                    row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                                    continue;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                            default: //"CHAR","UPPER","LOWER"
                                                if (!dtCropTrait.Rows[0].IsNull("max_length"))
                                                {
                                                    if (cellValue.Length > dtCropTrait.Rows[0].Field<int>("max_length"))
                                                    {
                                                        row.Cells[column.Name].ErrorText = "Max length is " + dtCropTrait.Rows[0].Field<int>("max_length");
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                        continue;
                                                    }
                                                }
                                                break;
                                        }

                                        //Compare values
                                        var dbValue = dtCropTraitObservationUpdate.AsEnumerable().FirstOrDefault(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<int>("crop_trait_id") == columnCropTraitId);
                                        if (dbValue != null)
                                        {
                                            //depends on is_coded and data_type_code of crop_trait
                                            //var dtCropTrait = _sharedUtils.GetLocalData("select value_member, type, max_length, numeric_maximum, numeric_minimum from acc_batch_wizard_crop_trait_lookup where value_member = " + columnCropTraitId,"");

                                            string strVal = string.Empty;
                                            switch (dtCropTrait.Rows[0].Field<string>("type")) {
                                                case "CODED":
                                                    if (dbValue["code"] == DBNull.Value)
                                                    {
                                                        strVal = string.Empty;
                                                    }
                                                    else
                                                        strVal = dbValue["code"].ToString();

                                                    if (string.Equals(cellValue, strVal, ux_checkBoxMatchCase.Checked ? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                    break;
                                                case "NUMERIC":
                                                    if (dbValue.IsNull("numeric_value"))
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        }
                                                    }
                                                    else {
                                                        decimal decDbValue = dbValue.Field<decimal>("numeric_value");
                                                        if (!string.IsNullOrEmpty(cellValue))
                                                        {
                                                            if (decimal.Parse(cellValue) == decDbValue)
                                                            {
                                                                row.Cells[column.Name].ErrorText = string.Empty;
                                                                row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            }
                                                            else
                                                            {
                                                                row.Cells[column.Name].ErrorText = string.Empty;
                                                                row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        }
                                                    }
                                                    break;
                                                default: //"CHAR","UPPER","LOWER"
                                                    if (dbValue["string_value"] == DBNull.Value)
                                                    {
                                                        strVal = string.Empty;
                                                    }
                                                    else
                                                        strVal = dbValue["string_value"].ToString();

                                                    if (string.Equals(cellValue, strVal, ux_checkBoxMatchCase.Checked ? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            if (string.IsNullOrEmpty(row.Cells[column.Name].Value.ToString()))
                                            {
                                                row.Cells[column.Name].ErrorText = string.Empty;
                                                row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                validationOKWithoutChangesCount++;
                                            }
                                            else
                                            {
                                                row.Cells[column.Name].ErrorText = string.Empty;
                                                row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                validationOKWithChangesCount++;
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else if (colTag.Category.Equals("PASSPORT")) //PASSPORT
                                {
                                    #region Passport
                                    //set string.empty to null values
                                    if (row.Cells[column.Name].Value == null)
                                        row.Cells[column.Name].Value = string.Empty;

                                    string cellValue = row.Cells[column.Name].Value.ToString();
                                    switch (column.Name)
                                    {
                                        case PassportCode.COLLCODE: //COLLCODE
                                            #region COLLCODE
                                            ValidateInstitutionCodeCell(row.Cells[column.Name], rowAcessionId, "COLLECTED");
                                            if (row.Cells[column.Name].Style.BackColor == StyleColors.ValidationOKWithChanges)
                                                showCooperatorWarning = true;
                                            break;
                                            #endregion
                                        case PassportCode.BREDCODE:
                                            #region BREDCODE
                                            ValidateInstitutionCodeCell(row.Cells[column.Name], rowAcessionId, "DEVELOPED");
                                            if (row.Cells[column.Name].Style.BackColor == StyleColors.ValidationOKWithChanges)
                                                showCooperatorWarning = true;
                                            break;
                                            #endregion
                                        case PassportCode.DONORCODE:
                                            #region DONORCODE
                                            ValidateInstitutionCodeCell(row.Cells[column.Name], rowAcessionId, "DONATED");
                                            if (row.Cells[column.Name].Style.BackColor == StyleColors.ValidationOKWithChanges)
                                                showCooperatorWarning = true;
                                            break;
                                        #endregion

                                        case PassportCode.PUID: // 0. Persistent unique identifier
                                            #region DOI
                                            if (!string.IsNullOrEmpty(cellValue) && !cellValue.StartsWith("10.18730/"))
                                            {
                                                row.Cells[column.Name].ErrorText = "Persistent unique identifier must be start with \"10.18730/\"";
                                                row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                validationErrorCount++;
                                                continue;
                                            }

                                            if (_isCustomModificationNeeded)
                                            {
                                                ValidateAccessionInvNameCell(row.Cells[column.Name], rowInventoryId, "DOI");
                                            }
                                            else
                                            {
                                                var dbValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("doi"))
                                                    {
                                                        string strValue = dbValue.Field<string>("doi");
                                                        if (string.Equals(strValue, cellValue, ux_checkBoxMatchCase.Checked ? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        }
                                                    }
                                                }
                                            }
                                            break;
                                        #endregion
                                        case PassportCode.COLLNUMB:
                                            ValidateAccessionInvNameCell(row.Cells[column.Name], rowInventoryId, "COLLECTOR");
                                            break;
                                        case PassportCode.COLLMISSID:
                                            ValidateAccessionInvNameCell(row.Cells[column.Name], rowInventoryId, "EXPLOREID");
                                            break;
                                        case PassportCode.BREEDER_CODE:
                                            ValidateAccessionInvNameCell(row.Cells[column.Name], rowInventoryId, "BREEDER");
                                            break;
                                        case PassportCode.ACCENAME:
                                            ValidateAccessionInvNameCell(row.Cells[column.Name], rowInventoryId, "CULTIVAR");
                                            break;
                                        case PassportCode.DONORNUMB:
                                            ValidateAccessionInvNameCell(row.Cells[column.Name], rowInventoryId, "DONOR");
                                            break;
                                        case PassportCode.BIOSAFETY_CODE:
                                            ValidateAccessionInvNameCell(row.Cells[column.Name], rowInventoryId, "BIOSAFETY");
                                            break;
                                        case PassportCode.OTHERNUMB:
                                            ValidateAccessionInvNameCell(row.Cells[column.Name], rowInventoryId, "OTHER");
                                            break;

                                        case PassportCode.ACQDATE: //12. Acquisition date
                                            #region AdquisitionDate
                                            if (true)
                                            {
                                                string source_date = string.Empty;
                                                if (cellValue != "")
                                                {
                                                    try
                                                    {
                                                        string dateValue = cell.Value.ToString();
                                                        string source_date_code = string.Empty;
                                                        if(cellValue.Length != 8)
                                                        {
                                                            row.Cells[column.Name].ErrorText = "Invalid format\n\nValue must be YYYYMMDD format";
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                            continue;
                                                        }
                                                        if (dateValue.EndsWith("0000"))
                                                        {
                                                            source_date_code = "'yyyy'";
                                                            source_date = dateValue.Substring(0, 4) + "0101";
                                                        }
                                                        else if (dateValue.EndsWith("00"))
                                                        {
                                                            source_date_code = "MM/yyyy";
                                                            source_date = dateValue.Substring(0, 6) + "01";
                                                        }
                                                        else
                                                        {
                                                            source_date_code = "MM/dd/yyyy";
                                                            source_date = dateValue;
                                                        }
                                                        DateTime.ParseExact(source_date, "yyyyMMdd", null);
                                                    }
                                                    catch(Exception ex1)
                                                    {
                                                        cell.ErrorText = ex1.Message;
                                                        cell.Style.BackColor = StyleColors.ValidationError;
                                                        validationErrorCount++;
                                                        continue;
                                                    }
                                                }

                                                var dbValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("initial_received_date"))
                                                    {
                                                        DateTime dbDate = dbValue.Field<DateTime>("initial_received_date");
                                                        string strDBDate = dbDate.ToString("yyyyMMdd");
                                                        if (!dbValue.IsNull("initial_received_date_code"))
                                                        {
                                                            switch (dbValue.Field<string>("initial_received_date_code"))
                                                            {
                                                                case "yyyy":
                                                                    strDBDate = strDBDate.Substring(0, 4) + "0000";
                                                                    break;
                                                                case "MM/yyyy":
                                                                    strDBDate = strDBDate.Substring(0, 6) + "00";
                                                                    break;
                                                                case "MM/dd/yyyy":
                                                                    break;
                                                            }
                                                        };

                                                        if (strDBDate.Equals(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                            }
                                            break;
                                        #endregion
                                        case PassportCode.SAMPSTAT: //19. Biological status of accession 
                                            #region SAMPSTAT
                                            if (true)
                                            {
                                                //Format
                                                if (!string.IsNullOrEmpty(cellValue))
                                                {
                                                    DataTable dtCodeValue = _sharedUtils.GetLocalData("select display_member from code_value_lookup where group_name = 'IMPROVEMENT_LEVEL' and value_member = '"+ cellValue + "'", "");
                                                    if(dtCodeValue == null || dtCodeValue.Rows.Count == 0)
                                                    {
                                                        row.Cells[column.Name].ErrorText = cellValue + " not found\n\nValue must be registered in \"IMPROVEMENT_LEVEL\" code group";
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                        continue;
                                                    }
                                                }

                                                var dbValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("improvement_status_code"))
                                                    {
                                                        string strValue = dbValue.Field<string>("improvement_status_code");
                                                        if (strValue.Equals(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                            }
                                            break;
                                            #endregion
                                        case  PassportCode.REMARKS: //28. Remarks
                                            #region REMARKS
                                            if (true)
                                            {
                                                var dbValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("note"))
                                                    {
                                                        string strValue = dbValue.Field<string>("note");
                                                        if (string.Equals(strValue, cellValue, ux_checkBoxMatchCase.Checked? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                            }
                                            break;
                                            #endregion
                                        case PassportCode.ACCESSION_STATUS: //Accession status
                                            #region AccessionStatus
                                            if (true)
                                            {
                                                //Format
                                                if (!string.IsNullOrEmpty(cellValue))
                                                {
                                                    DataTable dtCodeValue = _sharedUtils.GetLocalData("select display_member from code_value_lookup where group_name = 'ACCESSION_STATUS' and value_member = '" + cellValue + "'", "");
                                                    if (dtCodeValue == null || dtCodeValue.Rows.Count == 0)
                                                    {
                                                        row.Cells[column.Name].ErrorText = cellValue + " not found\n\nValue must be registered in \"ACCESSION_STATUS\" code group";
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                        continue;
                                                    }
                                                }
                                                else
                                                {
                                                    row.Cells[column.Name].ErrorText = column.HeaderText + " is empty";
                                                    row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                    continue;
                                                }

                                                var dbValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("status_code"))
                                                    {
                                                        string strValue = dbValue.Field<string>("status_code");
                                                        if (string.Equals(strValue, cellValue, ux_checkBoxMatchCase.Checked ? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                            }
                                            break;
                                        #endregion
                                        case PassportCode.ACCESSION_TAXON: //Taxon Name
                                            #region TaxonName
                                            if (string.IsNullOrEmpty(row.Cells[PassportCode.ACCESSION_TAXON].Value.ToString()))
                                            {
                                                row.Cells[column.Name].ErrorText = column.HeaderText + " is empty";
                                                row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                validationErrorCount++;
                                            }
                                            else
                                            {
                                                var sqltsl = "select value_member from taxonomy_species_lookup where display_member = '" + row.Cells[PassportCode.ACCESSION_TAXON].Value + "'";
                                                DataTable dtTaxonomySpecies = _sharedUtils.GetLocalData(sqltsl, "");
                                                if (dtTaxonomySpecies != null && dtTaxonomySpecies.Rows.Count > 0)
                                                {
                                                    var dbValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                    if (dbValue != null)
                                                    {
                                                        int strValue = dbValue.Field<int>("taxonomy_species_id");
                                                        if (strValue == dtTaxonomySpecies.Rows[0].Field<int>("value_member"))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    row.Cells[column.Name].ErrorText = column.HeaderText + " not found\n\n" +
                                                        "GENUS, SPECIES, SPAUTHOR, SUBTAXA, SUBTAUTHOR must be registered in \"get_taxonomy_genus\" and \"get_taxonomy_species\" dataviews ";
                                                    row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                    validationErrorCount++;
                                                }
                                            }
                                            break;
                                        #endregion

                                        case PassportCode.MLSSTAT: //27. MLS status of the accession 
                                            #region MLSSTAT
                                            if(true)
                                            {
                                                var dbValue = _dtAccessionIpr.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("ipr_number"))
                                                    {
                                                        string strValue = dbValue.Field<string>("ipr_number");
                                                        if (strValue.Equals(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;

                                        case PassportCode.ANCEST:
                                            break;
                                        case PassportCode.PARENT_FEMALE: //20.1 Parent Female
                                            #region ParentFemale
                                            if (true)
                                            {
                                                var dbValue = _dtAccessionPedigree.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("female_external_accession"))
                                                    {
                                                        string strValue = dbValue.Field<string>("female_external_accession");
                                                        if (string.Equals(strValue, cellValue, ux_checkBoxMatchCase.Checked ? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.PARENT_MALE: //20.2 Parent Male
                                            #region ParentMale
                                            if (true)
                                            {
                                                var dbValue = _dtAccessionPedigree.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("male_external_accession"))
                                                    {
                                                        string strValue = dbValue.Field<string>("male_external_accession");
                                                        if (string.Equals(strValue, cellValue, ux_checkBoxMatchCase.Checked ? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.PARENT_FEMALE_ACCENUMB: //20.3 Parent Female (Accenumb)
                                            #region ParentFemaleAccenumb
                                            if (true)
                                            {
                                                int accessionId = -1;
                                                if (!string.IsNullOrEmpty(cellValue))
                                                {
                                                    DataTable dtAccession = _sharedUtils.GetLocalData("select value_member from accession_lookup where display_member = '" + cellValue + "'", "");
                                                    if (dtAccession.Rows.Count > 0)
                                                    {
                                                        accessionId = dtAccession.Rows[0].Field<int>("value_member");
                                                    }
                                                    else
                                                    {
                                                        cell.ErrorText = cellValue + " is not registered";
                                                        cell.Style.BackColor = StyleColors.ValidationError;
                                                        continue;
                                                    }
                                                }
                                                //Compare values
                                                var dbValue = _dtAccessionPedigree.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("female_accession_id"))
                                                    {
                                                        int intValue = dbValue.Field<int>("female_accession_id");
                                                        if (intValue == accessionId)
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.PARENT_MALE_ACCENUM: //20.4 Parent Male (Accenumb)
                                            #region ParentMaleAccenumb
                                            if (true)
                                            {
                                                int accessionId = -1;
                                                if (!string.IsNullOrEmpty(cellValue))
                                                {
                                                    DataTable dtAccession = _sharedUtils.GetLocalData("select value_member from accession_lookup where display_member = '" + cellValue + "'", "");
                                                    if (dtAccession.Rows.Count > 0)
                                                    {
                                                        accessionId = dtAccession.Rows[0].Field<int>("value_member");
                                                    }
                                                    else
                                                    {
                                                        cell.ErrorText = cellValue + " is not registered";
                                                        cell.Style.BackColor = StyleColors.ValidationError;
                                                        continue;
                                                    }
                                                }
                                                //Compare values
                                                var dbValue = _dtAccessionPedigree.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("male_accession_id"))
                                                    {
                                                        int intValue = dbValue.Field<int>("male_accession_id");
                                                        if (intValue == accessionId)
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;

                                        case PassportCode.DECLATITUDE: //15.1 Latitude of collecting site
                                            #region DECLATITUDE
                                            if (true)
                                            {
                                                decimal cellDecimalValue;
                                                if (!string.IsNullOrEmpty(cellValue))
                                                {
                                                    if(decimal.TryParse(cellValue, out cellDecimalValue))
                                                    {
                                                        if(cellDecimalValue < -90 || cellDecimalValue > 90)
                                                        {
                                                            cell.ErrorText = "Value must be between -90 and 90";
                                                            cell.Style.BackColor = StyleColors.ValidationError;
                                                            continue;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cell.ErrorText = "Invalid format\n\nValue must be decimal";
                                                        cell.Style.BackColor = StyleColors.ValidationError;
                                                        continue;
                                                    }
                                                }
                                                //Compare
                                                var dbValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("latitude"))
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                            continue;
                                                        }

                                                        decimal dbDecimalValue = dbValue.Field<decimal>("latitude");
                                                        if (dbDecimalValue == decimal.Parse(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.DECLONGITUDE: //15.3 Longitude of collecting site
                                            #region DECLONGITUDE
                                            if (true)
                                            {
                                                decimal cellDecimalValue;
                                                if (!string.IsNullOrEmpty(cellValue))
                                                {
                                                    if (decimal.TryParse(cellValue, out cellDecimalValue))
                                                    {
                                                        if (cellDecimalValue < -180 || cellDecimalValue > 180)
                                                        {
                                                            cell.ErrorText = "Value must be between -180 and 180";
                                                            cell.Style.BackColor = StyleColors.ValidationError;
                                                            continue;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cell.ErrorText = "Invalid format\n\nValue must be decimal";
                                                        cell.Style.BackColor = StyleColors.ValidationError;
                                                        continue;
                                                    }
                                                }
                                                //Compare
                                                var dbValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("longitude"))
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                            continue;
                                                        }

                                                        decimal dbDecimalValue = dbValue.Field<decimal>("longitude");
                                                        if (dbDecimalValue == decimal.Parse(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.ELEVATION: //16. Elevation of collecting site
                                            #region ELEVATION
                                            if (true)
                                            {
                                                if (!string.IsNullOrEmpty(cellValue))
                                                {
                                                    int cellIntValue;
                                                    if (int.TryParse(cellValue, out cellIntValue))
                                                    {
                                                        if (cellIntValue < -300 || cellIntValue > 6000)
                                                        {
                                                            cell.ErrorText = "Value must be between -300 and 6000";
                                                            cell.Style.BackColor = StyleColors.ValidationError;
                                                            continue;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cell.ErrorText = "Invalid format\n\nValue must be integer";
                                                        cell.Style.BackColor = StyleColors.ValidationError;
                                                        continue;
                                                    }
                                                }
                                                //Compare
                                                var dbValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("elevation_meters"))
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                            continue;
                                                        }

                                                        int dbIntValue = dbValue.Field<int>("elevation_meters");
                                                        if (dbIntValue == int.Parse(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.COLLDATE: //17. Collecting date of sample
                                            #region COLLDATE
                                            if (true)
                                            {
                                                string source_date = string.Empty;
                                                if (!string.IsNullOrEmpty(cellValue))
                                                {
                                                    try
                                                    {
                                                        string dateValue = cell.Value.ToString();
                                                        string source_date_code = string.Empty;
                                                        if (cellValue.Length != 8)
                                                        {
                                                            row.Cells[column.Name].ErrorText = "Invalid format\n\nValue must be YYYYMMDD format";
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                            continue;
                                                        }

                                                        if (dateValue.EndsWith("0000"))
                                                        {
                                                            source_date_code = "yyyy";
                                                            source_date = dateValue.Substring(0, 4) + "0101";
                                                        }
                                                        else if (dateValue.EndsWith("00"))
                                                        {
                                                            source_date_code = "MM/yyyy";
                                                            source_date = dateValue.Substring(0, 6) + "01";
                                                        }
                                                        else
                                                        {
                                                            source_date_code = "MM/dd/yyyy";
                                                            source_date = dateValue;
                                                        }
                                                        DateTime.ParseExact(source_date, "yyyyMMdd", null);
                                                    }
                                                    catch (Exception ex1)
                                                    {
                                                        cell.ErrorText = ex1.Message;
                                                        cell.Style.BackColor = StyleColors.ValidationError;
                                                        validationErrorCount++;
                                                        continue;
                                                    }
                                                }
                                                //Compare
                                                var dbValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("source_date"))
                                                    {
                                                        DateTime dbDate = dbValue.Field<DateTime>("source_date");
                                                        string strDBDate = dbDate.ToString("yyyyMMdd");
                                                        if (!dbValue.IsNull("source_date_code"))
                                                        {
                                                            switch (dbValue.Field<string>("source_date_code"))
                                                            {
                                                                case "yyyy":
                                                                    strDBDate = strDBDate.Substring(0, 4) + "0000";
                                                                    break;
                                                                case "MM/yyyy":
                                                                    strDBDate = strDBDate.Substring(0, 6) + "00";
                                                                    break;
                                                                case "MM/dd/yyyy":
                                                                    break;
                                                            }
                                                        };

                                                        if (strDBDate.Equals(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            validationOKWithoutChangesCount++;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            validationOKWithChangesCount++;
                                                        }
                                                    }
                                                }
                                                else //dbValue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.ACCESSION_SOURCE_FORMATTED_LOCALITY: //formatted locality 
                                            #region COLLSITE
                                            if (true)
                                            {
                                                var dbValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("formatted_locality"))
                                                    {
                                                        string strValue = dbValue.Field<string>("formatted_locality");
                                                        if (string.Equals(strValue, cellValue, ux_checkBoxMatchCase.Checked ? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        }
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.ACCESSION_SOURCE_VERBATIM_LOCALITY: //collector_verbatim_locality
                                            #region collector_verbatim_locality
                                            if (true)
                                            {
                                                var dbValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("collector_verbatim_locality"))
                                                    {
                                                        string strValue = dbValue.Field<string>("collector_verbatim_locality");
                                                        if (string.Equals(strValue, cellValue, ux_checkBoxMatchCase.Checked ? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        }
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.COORDDATUM: //15.6 Coordinate datum 
                                            #region COORDDATUM
                                            if (true)
                                            {
                                                var dbValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null)
                                                {
                                                    if (!dbValue.IsNull("georeference_datum"))
                                                    {
                                                        string strValue = dbValue.Field<string>("georeference_datum");
                                                        if (string.Equals(strValue, cellValue, ux_checkBoxMatchCase.Checked ? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        if (string.IsNullOrEmpty(cellValue))
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        }
                                                        else
                                                        {
                                                            row.Cells[column.Name].ErrorText = string.Empty;
                                                            row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        }
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.COORDUNCERT: //15.5 Coordinate uncertainty
                                            #region COORDUNCERT
                                            if (true)
                                            {
                                                if (!string.IsNullOrEmpty(cellValue))
                                                {
                                                    int cellIntValue;
                                                    if (int.TryParse(cellValue, out cellIntValue) == false)
                                                    {
                                                        cell.ErrorText = "Invalid format\n\nValue must be integer";
                                                        cell.Style.BackColor = StyleColors.ValidationError;
                                                        continue;
                                                    }
                                                }
                                                //Compare
                                                var dbValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null && !dbValue.IsNull("uncertainty"))
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                        continue;
                                                    }

                                                    int dbIntValue = dbValue.Field<int>("uncertainty");
                                                    if (dbIntValue == int.Parse(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.GEOREFMETH: //15.7 Georeferencing method
                                            #region GEOREFMETH
                                            if (true)
                                            {
                                                //Format
                                                if (!string.IsNullOrEmpty(cellValue))
                                                {
                                                    DataTable dtCodeValue = _sharedUtils.GetLocalData("select display_member from code_value_lookup where group_name = 'GEOREFERENCE_PROTOCOL' and value_member = '" + cellValue + "'", "");
                                                    if (dtCodeValue == null || dtCodeValue.Rows.Count == 0)
                                                    {
                                                        row.Cells[column.Name].ErrorText = cellValue + " not found\n\nValue must be registered in \"GEOREFERENCE_PROTOCOL\" code group";
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationError;
                                                        continue;
                                                    }
                                                }

                                                var dbValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null && !dbValue.IsNull("georeference_protocol_code"))
                                                {
                                                    string strValue = dbValue.Field<string>("georeference_protocol_code");
                                                    if (string.Equals(strValue, cellValue, ux_checkBoxMatchCase.Checked ? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                        validationOKWithoutChangesCount++;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        validationOKWithChangesCount++;
                                                    }
                                                }
                                            }
                                            break;
                                        #endregion
                                        case PassportCode.ACCESSION_SOURCE_NOTE: //Accession source note
                                            #region ACCESSION_SOURCE_NOTE
                                            if (true)
                                            {
                                                var dbValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                if (dbValue != null && !dbValue.IsNull("note"))
                                                {
                                                    string strValue = dbValue.Field<string>("note");
                                                    if (string.Equals(strValue, cellValue, ux_checkBoxMatchCase.Checked ? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                    }
                                                }
                                                else //dbvalue is empty
                                                {
                                                    if (string.IsNullOrEmpty(cellValue))
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                    }
                                                    else
                                                    {
                                                        row.Cells[column.Name].ErrorText = string.Empty;
                                                        row.Cells[column.Name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;

                                        default:
                                            //Check if custom descriptor is accession_inv_name
                                            var drCustomDescriptor = dtPassportCriteria.AsEnumerable().FirstOrDefault(x => x.Field<string>("value_member").Equals(column.Name) && x.Field<string>("table_name").Equals("accession_inv_name"));
                                            if (drCustomDescriptor != null)
                                            {
                                                ValidateAccessionInvNameCell(row.Cells[column.Name], rowInventoryId, drCustomDescriptor.Field<string>("category_code"));
                                            }
                                            break;
                                        }
                                    #endregion
                                }
                                else if(colTag.Category.Equals("GROUP_TRAIT"))
                                {
                                    switch (column.Name)
                                    {
                                        case "13. Country of origin":
                                            #region accession_source_geography_id
                                            

                                            if (!string.IsNullOrEmpty(row.Cells["13. Country of origin"].Value.ToString()) || !string.IsNullOrEmpty(row.Cells["Adm1"].Value.ToString())
                                                || !string.IsNullOrEmpty(row.Cells["Adm2"].Value.ToString()) || !string.IsNullOrEmpty(row.Cells["Adm3"].Value.ToString()))
                                            {
                                                if (row.Cells["source_type_code"].Value == null || string.IsNullOrEmpty(row.Cells["source_type_code"].Value.ToString()))
                                                {
                                                    row.Cells["source_type_code"].ErrorText = "Source type code is empty\n\nValid values: COLLECTED, DEVELOPED, DONATED\nIts neccessary to indicate accession source type.";
                                                    row.Cells["source_type_code"].Style.BackColor = StyleColors.ValidationError;
                                                    validationErrorCount++;
                                                }
                                                else
                                                {
                                                    row.Cells["source_type_code"].ErrorText = string.Empty;
                                                    row.Cells["source_type_code"].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                }

                                                string country_code = row.Cells["13. Country of origin"].Value.ToString();
                                                string adm1 = row.Cells["Adm1"].Value.ToString();
                                                string adm2 = row.Cells["Adm2"].Value.ToString();
                                                string adm3 = row.Cells["Adm3"].Value.ToString();
                                                string geography_name = country_code + (string.IsNullOrEmpty(adm1) ? "" : ", " + adm1) +
                                                    (string.IsNullOrEmpty(adm2) ? "" : ", " + adm2) + (string.IsNullOrEmpty(adm3) ? "" : ", " + adm3);


                                                var sqlgeography_id = "select value_member as geography_id from geography_lookup where display_member = '" + geography_name + "'";
                                                DataTable dtGeographyId = _sharedUtils.GetLocalData(sqlgeography_id, "");

                                                if (dtGeographyId != null && dtGeographyId.Rows.Count == 1)
                                                {
                                                    //Compare
                                                    var dbValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAcessionId);
                                                    if (dbValue != null)
                                                    {
                                                        if (!dbValue.IsNull("geography_id"))
                                                        {
                                                            int dbIntValue = dbValue.Field<int>("geography_id");
                                                            if (dbIntValue == dtGeographyId.Rows[0].Field<int>("geography_id"))
                                                            {
                                                                foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                                {
                                                                    row.Cells[column_name].ErrorText = string.Empty;
                                                                    row.Cells[column_name].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                                }
                                                                row.Cells["source_type_code"].Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                                                            }
                                                            else
                                                            {
                                                                foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                                {
                                                                    row.Cells[column_name].ErrorText = string.Empty;
                                                                    row.Cells[column_name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                                }
                                                                row.Cells["source_type_code"].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            }
                                                        }
                                                        else //dbvalue is empty
                                                        {
                                                            foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                            {
                                                                row.Cells[column_name].ErrorText = string.Empty;
                                                                row.Cells[column_name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                            }
                                                            row.Cells["source_type_code"].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        }
                                                    }
                                                    else //dbvalue is empty
                                                    {
                                                        foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                        {
                                                            row.Cells[column_name].ErrorText = string.Empty;
                                                            row.Cells[column_name].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                        }
                                                        row.Cells["source_type_code"].Style.BackColor = StyleColors.ValidationOKWithChanges;
                                                    }
                                                    
                                                }
                                                else if (dtGeographyId.Rows.Count == 0)
                                                {
                                                    foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                    {
                                                        row.Cells[column_name].ErrorText = "Geography record not found\n\nTo update \"Country of Origin\" is neccessary register the location in \"get_geography\" dataview first";
                                                        row.Cells[column_name].Style.BackColor = StyleColors.ValidationError;
                                                        validationErrorCount++;
                                                    }
                                                }
                                                else if (dtGeographyId.Rows.Count > 1)
                                                    foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                    {
                                                        row.Cells[column_name].ErrorText = "More than one Geography record found";
                                                        row.Cells[column_name].Style.BackColor = StyleColors.ValidationError;
                                                        validationErrorCount++;
                                                    }
                                                else
                                                    foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                    {
                                                        row.Cells[column_name].ErrorText = "There was an error searching Geography id";
                                                        row.Cells[column_name].Style.BackColor = StyleColors.ValidationError;
                                                        validationErrorCount++;
                                                    }
                                                
                                            }
                                            else {
                                                foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                {
                                                    row.Cells[column_name].ErrorText = "Country of Origin, Adm1, Adm2, Adm3 are empty";
                                                    row.Cells[column_name].Style.BackColor = StyleColors.ValidationError;
                                                    validationErrorCount++;
                                                }
                                            }
                                            #endregion
                                            break;
                                        case "":
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    
                }

                //Update summary table
                validationErrorCount = 0;
                validationOKWithChangesCount = 0;
                validationOKWithoutChangesCount = 0;
                foreach (DataGridViewRow row in ux_datagridviewSheet.Rows)
                {
                    for (int iCol = 0; iCol < ux_datagridviewSheet.Columns.Count; iCol++)
                    {
                        if (row.Cells[iCol].Style.BackColor == StyleColors.ValidationError)
                        {
                            validationErrorCount++;
                        }
                        else if (row.Cells[iCol].Style.BackColor == StyleColors.ValidationOKWithChanges && iCol > 0)
                        {
                            validationOKWithChangesCount++;
                        }
                        else if (row.Cells[iCol].Style.BackColor == StyleColors.ValidationOKWithoutChanges && iCol > 0)
                        {
                            validationOKWithoutChangesCount++;
                        }
                    }
                }
                ux_labelValidationErrorCount.Text = validationErrorCount.ToString();
                ux_labelValidationOkWithoutChangesCount.Text = validationOKWithoutChangesCount.ToString();
                ux_labelValidationOkWithChangesCount.Text = validationOKWithChangesCount.ToString();

                Cursor.Current = Cursors.Arrow;

                if (showCooperatorWarning)
                {
                    GGMessageBox mb = new GGMessageBox(@"Warning

Some FAO WIEWS INSTCODES will be updated.
Please make sure that organizations name and address are updated.

To obtain the latest information of FAO WIEWS INSTCODES you can go ""http://www.fao.org/wiews/data/organizations/en"" to search by organization
Or you can also download all data of FAO VIEWS INSTCODES at this link ""http://www.fao.org/wiews-archive/export_c.zip""

To register new organizations use Cooperator Wizard or get_cooperator dataview
To update existing organizations use get_cooperator dataview
", this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    mb.ShowDialog();
                }
            }
            catch (WizardException wex)
            {
                Cursor.Current = Cursors.Arrow;
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "BatchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                validationOKWithChangesCount = 0;
                validationOKWithoutChangesCount = 0;
                validationErrorCount = 0;

                Cursor.Current = Cursors.Arrow;

                GGMessageBox mb = new GGMessageBox("Error : " + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        public void saveData2()
        {
            try
            {
                DataGridView datagridviewContainer = ux_datagridviewSheet;
                DataTable dtSourceContainer = _main2;
                var watch = System.Diagnostics.Stopwatch.StartNew();
                
                savingErrorCount = 0;
                savingUnmodifiedCount = validationOKWithoutChangesCount;
                savingOKCount = 0;

                DataTable dtTableUpdate = new DataTable();
                dtTableUpdate.Columns.Add("table_name", typeof(string));
                dtTableUpdate.Columns.Add("table_id", typeof(Int32));
                dtTableUpdate.Columns.Add("filter_column", typeof(string));
                dtTableUpdate.Columns.Add("filter_value", typeof(string));
                dtTableUpdate.Columns.Add("column_name", typeof(string));
                dtTableUpdate.Columns.Add("column_type", typeof(string));
                dtTableUpdate.Columns.Add("int_value", typeof(Int32));
                dtTableUpdate.Columns.Add("string_value", typeof(string));
                dtTableUpdate.Columns.Add("decimal_value", typeof(Decimal));
                dtTableUpdate.Columns.Add("datetime_value", typeof(DateTime));
                dtTableUpdate.Columns.Add("cell", typeof(DataGridViewCell));
                dtTableUpdate.Columns.Add("second_table_id", typeof(Int32));
                dtTableUpdate.Columns.Add("datetime_format_code", typeof(string));

                #region CropTrait
                DataTable dtObservationUpdate = null;
                if (_dsObservationUpdate != null && _dsObservationUpdate.Tables.Contains("accession_batch_wizard_get_crop_trait_observation"))
                {
                    dtObservationUpdate = _dsObservationUpdate.Tables["accession_batch_wizard_get_crop_trait_observation"];
                }

                DataTable dtCropTraitIsCoded = null;
                if (_dsCropTraitIsCoded != null && _dsCropTraitIsCoded.Tables.Contains("acc_batch_wizard_get_crop_trait_type"))
                {
                    dtCropTraitIsCoded = _dsCropTraitIsCoded.Tables["acc_batch_wizard_get_crop_trait_type"];
                }

                string parameters = ":inventoryid=;:croptraitid=;:methodid=" + _method_id;
                DataSet dsObservationInsert = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_cto_insert", parameters, 0, 0);
                DataTable dtObservationInsert = null;
                if (dsObservationInsert != null && dsObservationInsert.Tables.Contains("acc_batch_wizard_get_cto_insert"))
                {
                    dtObservationInsert = dsObservationInsert.Tables["acc_batch_wizard_get_cto_insert"];
                }

                #endregion

                DataTable dtAccessionInvName = null;
                if (_dsAccessionInvName != null && _dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"] != null)
                {
                    dtAccessionInvName = _dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"];
                }

                #region AppendChanges
                //Indicate Changes
                foreach (DataGridViewRow row in datagridviewContainer.Rows)
                {
                    if (row.Cells[0].Value == null || row.IsNewRow)
                        continue;

                    if (row.ErrorText == "")
                    {
                        for (int iCol = startDescriptorsIndex; iCol < row.Cells.Count; iCol++)
                        {
                            DataGridViewCell cell = row.Cells[iCol];
                            string inventory_id = row.Cells["inventory_id"].Value.ToString();
                            string crop_trait_id = cell.OwningColumn.Name;
                            string cellValue = cell.Value.ToString();
                            int rowAccessionId = int.Parse(row.Cells["accession_id"].Value.ToString());

                            //cell.Style.BackColor = Color.White;
                            //cell.ErrorText = "";
                            if (row.Cells[iCol].Style.BackColor != StyleColors.ValidationOKWithChanges)
                                continue;

                            string strVal = string.Empty;
                            switch (((ColumnTag)cell.OwningColumn.Tag).Category)
                            {
                                case "GROUP_TRAIT":
                                    switch (crop_trait_id)
                                    {
                                        case "13. Country of origin":
                                            #region accession_source_geography_id
                                            if (!string.IsNullOrEmpty(row.Cells["13. Country of origin"].Value.ToString()) || !string.IsNullOrEmpty(row.Cells["Adm1"].Value.ToString())
                                                || !string.IsNullOrEmpty(row.Cells["Adm2"].Value.ToString()) || !string.IsNullOrEmpty(row.Cells["Adm3"].Value.ToString()))
                                            {
                                                string country_code = row.Cells["13. Country of origin"].Value.ToString();
                                                string adm1 = row.Cells["Adm1"].Value.ToString();
                                                string adm2 = row.Cells["Adm2"].Value.ToString();
                                                string adm3 = row.Cells["Adm3"].Value.ToString();
                                                string geography_name = country_code + (string.IsNullOrEmpty(adm1) ? "" : ", " + adm1) +
                                                    (string.IsNullOrEmpty(adm2) ? "" : ", " + adm2) + (string.IsNullOrEmpty(adm3) ? "" : ", " + adm3);


                                                var sqlgeography_id = "select value_member as geography_id from geography_lookup where display_member = '" + geography_name + "'";
                                                DataTable dtGeographyId = _sharedUtils.GetLocalData(sqlgeography_id, "");

                                                if (dtGeographyId != null && dtGeographyId.Rows.Count == 1)
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_source";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["filter_column"] = "source_type_code";
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                    dr["column_name"] = "geography_id";
                                                    dr["column_type"] = "int";
                                                    dr["int_value"] = dtGeographyId.Rows[0].Field<int>("geography_id");
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);

                                                }
                                                else if (dtGeographyId.Rows.Count == 0)
                                                {
                                                    foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                        row.Cells[column_name].ErrorText = "Geography record not found";
                                                }
                                                else if (dtGeographyId.Rows.Count > 1)
                                                    foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                        row.Cells[column_name].ErrorText = "More than one Geography record found";
                                                else
                                                    foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                        row.Cells[column_name].ErrorText = "There was an error searching Geography id";

                                            }
                                            #endregion
                                            break;
                                        case "":
                                            break;
                                    }

                                    break;
                                case "PASSPORT":
                                    #region PASSPORT
                                    switch (crop_trait_id)
                                    {
                                        case PassportCode.BREDCODE:
                                            SaveInstitutionCodeCell(cell, rowAccessionId, "DEVELOPED");
                                            break;
                                        case PassportCode.DONORCODE:
                                            SaveInstitutionCodeCell(cell, rowAccessionId, "DONATED");
                                            break;
                                        case PassportCode.COLLCODE:
                                            SaveInstitutionCodeCell(cell, rowAccessionId, "COLLECTED");
                                            #region COLLCODE
                                            /*List<int> cellCooperatorIds = new List<int>();
                                            var faoNumbers = cellValue.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                                            string errorText = string.Empty;
                                            foreach (var faoNumber in faoNumbers)
                                            {
                                                DataRow drValue = _dtCooperator.AsEnumerable().FirstOrDefault(x => x.Field<string>("secondary_organization_abbrev").Equals(faoNumber));
                                                if (drValue == null)
                                                {
                                                    errorText += faoNumber + " is not found" + "\n";
                                                }
                                                else
                                                    cellCooperatorIds.Add(drValue.Field<int>("cooperator_id"));
                                            }
                                            if (!string.IsNullOrEmpty(errorText))
                                            {
                                                cell.ErrorText = errorText + "\nUse Cooperator Wizard to register new organizations";
                                                cell.Style.BackColor = StyleColors.SavingError;
                                                savingErrorCount++;
                                                continue;
                                            }
                                            
                                            //Update dataviews
                                            var accSourceMapRows = _dtAcessionSourceMap.AsEnumerable().Where(x => x.RowState != DataRowState.Deleted && x.Field<int>("accession_id") == rowAccessionId);
                                            if (accSourceMapRows == null || accSourceMapRows.Count() == 0) //dbValue is empty
                                            {
                                                //register all coopetators
                                                foreach (var cooperatorId in cellCooperatorIds)
                                                {
                                                    int minId;
                                                    if(_dtAcessionSourceMap.Rows.Count == 0)
                                                    {
                                                        minId = -1;
                                                    }else
                                                    {
                                                        minId = _dtAcessionSourceMap.AsEnumerable().Min(x => x.Field<int>("accession_source_map_id"));
                                                        if (minId > 0)
                                                        {
                                                            minId = -1;
                                                        }
                                                        else
                                                        {
                                                            minId--;
                                                        }
                                                    }
                                                    
                                                    DataRow drNew = _dtAcessionSourceMap.NewRow();
                                                    drNew["accession_source_map_id"] = minId;
                                                    drNew["accession_source_id"] = -1; //Update later
                                                    drNew["cooperator_id"] = cooperatorId;
                                                    drNew["created_date"] = DateTime.Now;
                                                    drNew["created_by"] = _sharedUtils.UserCooperatorID;
                                                    drNew["accession_id"] = rowAccessionId;
                                                    _dtAcessionSourceMap.Rows.Add(drNew);
                                                }
                                            }
                                            else //dbValue is not empty
                                            {
                                                int accessionSourceId = accSourceMapRows.First().Field<int>("accession_source_id");
                                                //register new ones
                                                foreach (var cellCooperatorId in cellCooperatorIds)
                                                {
                                                    if (!accSourceMapRows.Any(x => x.Field<int>("cooperator_id") == cellCooperatorId))
                                                    {
                                                        DataRow drNew = _dtAcessionSourceMap.NewRow();
                                                        drNew["accession_source_map_id"] = -1;
                                                        drNew["accession_source_id"] = accessionSourceId;
                                                        drNew["cooperator_id"] = cellCooperatorId;
                                                        drNew["created_date"] = DateTime.Now;
                                                        drNew["created_by"] = _sharedUtils.UserCooperatorID;
                                                        drNew["accession_id"] = rowAccessionId;
                                                        _dtAcessionSourceMap.Rows.Add(drNew);
                                                    }
                                                }
                                                //deleted not found
                                                var deleteRows = accSourceMapRows.Where(x => !cellCooperatorIds.Any(y => y == x.Field<int>("cooperator_id")));
                                                foreach (var dr in deleteRows)
                                                {
                                                    dr.Delete();
                                                }
                                            }
                                            cell.Style.BackColor = Color.Orange;*/
                                            break;
                                        #endregion
                                        case PassportCode.PUID: //0. DOI
                                            #region 0.
                                            /*
                                            var dbValue = dtAccessionInvName.AsEnumerable().FirstOrDefault(x => x.Field<int>("inventory_id") == int.Parse(inventory_id) && x.Field<string>("category_code").Equals("DOI"));
                                            if (dbValue != null)
                                            {
                                                if (!rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                                {
                                                    rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                            }
                                            else
                                            {
                                                //add
                                                DataRow dr = dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"].NewRow();
                                                dr["inventory_id"] = r.Field<int>("table_id");
                                                dr["category_code"] = category_code;
                                                dr["plant_name"] = r.Field<string>("string_value");
                                                dr["plant_name_rank"] = plant_name_rank;
                                                dr["is_web_visible"] = "Y";
                                                dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"].Rows.Add(dr);

                                                (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                            }*/
                                            if (!_isCustomModificationNeeded)
                                            {
                                                DataRow dr1 = dtTableUpdate.NewRow();
                                                dr1["table_name"] = "accession";
                                                dr1["table_id"] = row.Cells["accession_id"].Value;
                                                dr1["column_name"] = "doi";
                                                dr1["column_type"] = "string";
                                                dr1["string_value"] = cell.Value.ToString();
                                                dr1["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr1);
                                            }
                                            else
                                            {
                                                DataRow dr1 = dtTableUpdate.NewRow();
                                                dr1["table_name"] = "accession_inv_name";
                                                dr1["table_id"] = row.Cells["inventory_id"].Value;
                                                dr1["filter_column"] = "category_code";
                                                dr1["filter_value"] = "DOI";
                                                dr1["column_name"] = "plant_name";
                                                dr1["column_type"] = "string";
                                                dr1["string_value"] = cell.Value.ToString();
                                                dr1["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr1);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.COLLNUMB: //03.1.1 Collecting number
                                            #region 03.1.1
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_inv_name";
                                                dr["table_id"] = row.Cells["inventory_id"].Value;
                                                dr["filter_column"] = "category_code";
                                                dr["filter_value"] = "COLLECTOR";
                                                dr["column_name"] = "plant_name";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.BREEDER_CODE: //03.1.2 Breeder code
                                            #region 03.1.2
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_inv_name";
                                                dr["table_id"] = row.Cells["inventory_id"].Value;
                                                dr["filter_column"] = "category_code";
                                                dr["filter_value"] = "BREEDER";
                                                dr["column_name"] = "plant_name";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.BIOSAFETY_CODE: //03.1.3 Biosafety code
                                            #region 03.1.3
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_inv_name";
                                                dr["table_id"] = row.Cells["inventory_id"].Value;
                                                dr["filter_column"] = "category_code";
                                                dr["filter_value"] = "BIOSAFETY";
                                                dr["column_name"] = "plant_name";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.COLLMISSID: //04.2 Collecting mission identifier
                                            #region 04.2
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_inv_name";
                                                dr["table_id"] = row.Cells["inventory_id"].Value;
                                                dr["filter_column"] = "category_code";
                                                dr["filter_value"] = "EXPLOREID";
                                                dr["column_name"] = "plant_name";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.ACCENAME: //11. Accession name
                                            #region 11.
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_inv_name";
                                                dr["table_id"] = row.Cells["inventory_id"].Value;
                                                dr["filter_column"] = "category_code";
                                                dr["filter_value"] = "CULTIVAR";
                                                dr["column_name"] = "plant_name";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.OTHERNUMB: //24. Other identification associated with the accession
                                            #region 24.
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_inv_name";
                                                dr["table_id"] = row.Cells["inventory_id"].Value;
                                                dr["filter_column"] = "category_code";
                                                dr["filter_value"] = "OTHER";
                                                dr["column_name"] = "plant_name";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.DONORNUMB: //23. Donor accession number
                                            #region 23.
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_inv_name";
                                                dr["table_id"] = row.Cells["inventory_id"].Value;
                                                dr["filter_column"] = "category_code";
                                                dr["filter_value"] = "DONOR";
                                                dr["column_name"] = "plant_name";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;

                                        case PassportCode.DECLATITUDE: //15.1 Latitude of collecting site
                                            #region 15.1
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_source";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["filter_column"] = "source_type_code";
                                                if (row.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(row.Cells["source_type_code"].Value.ToString()))
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                else
                                                    dr["filter_value"] = "COLLECTED";
                                                dr["column_name"] = "latitude";
                                                dr["column_type"] = "decimal";
                                                if (string.IsNullOrEmpty(cellValue))
                                                    dr["decimal_value"] = DBNull.Value;
                                                else
                                                    dr["decimal_value"] = decimal.Parse(cell.Value.ToString());
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.DECLONGITUDE: //15.3 Longitude of collecting site
                                            #region 15.3
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_source";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["filter_column"] = "source_type_code";
                                                if (row.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(row.Cells["source_type_code"].Value.ToString()))
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                else
                                                    dr["filter_value"] = "COLLECTED";
                                                dr["column_name"] = "longitude";
                                                dr["column_type"] = "decimal";
                                                if (string.IsNullOrEmpty(cellValue))
                                                    dr["decimal_value"] = DBNull.Value;
                                                else
                                                    dr["decimal_value"] = decimal.Parse(cell.Value.ToString());
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.ELEVATION: //16. Elevation of collecting site
                                            #region 16.
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_source";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["filter_column"] = "source_type_code";
                                                if (row.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(row.Cells["source_type_code"].Value.ToString()))
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                else
                                                    dr["filter_value"] = "COLLECTED";
                                                dr["column_name"] = "elevation_meters";
                                                dr["column_type"] = "int";
                                                if(string.IsNullOrEmpty(cellValue))
                                                    dr["int_value"] = DBNull.Value;
                                                else
                                                    dr["int_value"] = int.Parse(cell.Value.ToString());
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.COLLDATE: //17. Collecting date of sample
                                            #region 17.
                                            if (!string.IsNullOrEmpty(cellValue))
                                            {
                                                string dateValue = cell.Value.ToString();
                                                string source_date = string.Empty;
                                                string source_date_code = string.Empty;

                                                if (dateValue.EndsWith("0000"))
                                                {
                                                    source_date_code = "yyyy";
                                                    source_date = dateValue.Substring(0, 4) + "0101";
                                                }
                                                else if (dateValue.EndsWith("00"))
                                                {
                                                    source_date_code = "MM/yyyy";
                                                    source_date = dateValue.Substring(0, 6) + "01";
                                                }
                                                else
                                                {
                                                    source_date_code = "MM/dd/yyyy";
                                                    source_date = dateValue;
                                                }
                                                //source_date  datetime  , source_date_code string
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_source";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["filter_column"] = "source_type_code";
                                                if (row.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(row.Cells["source_type_code"].Value.ToString()))
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                else
                                                    dr["filter_value"] = "COLLECTED";
                                                dr["column_name"] = "source_date";
                                                dr["column_type"] = "datetime";
                                                dr["datetime_value"] = DateTime.ParseExact(source_date, "yyyyMMdd", null);
                                                dr["datetime_format_code"] = source_date_code;
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            else
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_source";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["filter_column"] = "source_type_code";
                                                if (row.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(row.Cells["source_type_code"].Value.ToString()))
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                else
                                                    dr["filter_value"] = "COLLECTED";
                                                dr["column_name"] = "source_date";
                                                dr["column_type"] = "datetime";
                                                dr["datetime_value"] = DBNull.Value;
                                                dr["datetime_format_code"] = string.Empty;
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.ACCESSION_SOURCE_FORMATTED_LOCALITY: //formatted_locality
                                            #region COLLSITE
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_source";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["filter_column"] = "source_type_code";
                                                if (row.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(row.Cells["source_type_code"].Value.ToString()))
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                else
                                                    dr["filter_value"] = "COLLECTED";
                                                dr["column_name"] = "formatted_locality";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.ACCESSION_SOURCE_VERBATIM_LOCALITY: //collector_verbatim_locality
                                            #region collector_verbatim_locality
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_source";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["filter_column"] = "source_type_code";
                                                if (row.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(row.Cells["source_type_code"].Value.ToString()))
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                else
                                                    dr["filter_value"] = "COLLECTED";
                                                dr["column_name"] = "collector_verbatim_locality";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.COORDUNCERT: //15.5 Coordinate uncertainty
                                            #region COORDUNCERT
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_source";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["filter_column"] = "source_type_code";
                                                if (row.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(row.Cells["source_type_code"].Value.ToString()))
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                else
                                                    dr["filter_value"] = "COLLECTED";
                                                dr["column_name"] = "uncertainty";
                                                dr["column_type"] = "int";
                                                if (string.IsNullOrEmpty(cellValue))
                                                    dr["int_value"] = DBNull.Value;
                                                else
                                                    dr["int_value"] = int.Parse(cell.Value.ToString());
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.COORDDATUM: //15.6 Coordinate datum 
                                            #region COORDDATUM
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_source";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["filter_column"] = "source_type_code";
                                                if (row.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(row.Cells["source_type_code"].Value.ToString()))
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                else
                                                    dr["filter_value"] = "COLLECTED";
                                                dr["column_name"] = "georeference_datum";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.GEOREFMETH: //15.7 Georeferencing method 
                                            #region GEOREFMETH
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_source";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["filter_column"] = "source_type_code";
                                                if (row.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(row.Cells["source_type_code"].Value.ToString()))
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                else
                                                    dr["filter_value"] = "COLLECTED";
                                                dr["column_name"] = "georeference_protocol_code";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.ACCESSION_SOURCE_NOTE: //Accession source note
                                            #region ACCESSION_SOURCE_NOTE
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_source";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["filter_column"] = "source_type_code";
                                                if (row.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(row.Cells["source_type_code"].Value.ToString()))
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                else
                                                    dr["filter_value"] = "COLLECTED";
                                                dr["column_name"] = "note";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        //source_type_code COLLECTED

                                        case PassportCode.BREDNAME: // 18.1 Breeding institute name
                                            #region 18.1
                                            /*if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                            {
                                                if (dtCooperatorOrganization != null && dtCooperatorOrganization.Rows.Count > 0)
                                                {
                                                    var rows = dtCooperatorOrganization.Select("organization  = '" + cell.Value + "'");

                                                    if (rows != null && rows.Count() == 1)
                                                    {
                                                        DataRow dr = dtTableUpdate.NewRow();
                                                        dr["table_name"] = "accession_source_map";
                                                        dr["table_id"] = row.Cells["accession_id"].Value;
                                                        dr["column_name"] = "cooperator_id";
                                                        dr["column_type"] = "int";
                                                        dr["int_value"] = rows[0].Field<int>("cooperator_id");
                                                        dr["cell"] = cell;
                                                        dr["second_table_id"] = -1;
                                                        dtTableUpdate.Rows.Add(dr);

                                                        cell.Style.BackColor = Color.Yellow;
                                                    }
                                                    else if (rows.Count() == 0)
                                                    {
                                                        cell.ErrorText = "There was an error saving cooperator";
                                                    }
                                                    else if (rows.Count() > 1)
                                                        cell.ErrorText = "More than one cooperator found";
                                                    else
                                                        cell.ErrorText = "There was an error searching cooperator id";
                                                }
                                                else
                                                    cell.ErrorText = "There was an error searching Institute name";
                                            }*/
                                            #endregion
                                            break;
                                        //source_type_code DEVELOPED


                                        case PassportCode.PARENT_FEMALE: //20.1 Parent Female
                                            #region 20.1
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_pedigree";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["column_name"] = "female_external_accession";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.PARENT_MALE: //20.2 Parent Male
                                            #region 20.2
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_pedigree";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["column_name"] = "male_external_accession";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.PARENT_FEMALE_ACCENUMB: //20.3 Parent Female (Accenumb)
                                            #region 20.3
                                            /*if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                            {
                                                var accessionLookupRows = dtAccessionInventoryLookup.Select("AccNumber = '" + cell.Value.ToString().Replace("CIP ", "") + "'");

                                                if (accessionLookupRows != null && accessionLookupRows.Count() == 1)
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_pedigree";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "female_accession_id";
                                                    dr["column_type"] = "int";
                                                    dr["int_value"] = accessionLookupRows[0].Field<int>("accession_id");
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                else if (accessionLookupRows.Count() == 0)
                                                {
                                                    cell.ErrorText = "Accession not found";
                                                }
                                                else if (accessionLookupRows.Count() > 1)
                                                    cell.ErrorText = "More than one Accession found";
                                                else
                                                    cell.ErrorText = "There was an error searching accession id";
                                            }*/
                                            #endregion
                                            break;
                                        case PassportCode.PARENT_MALE_ACCENUM: //20.4 Parent Male (Accenumb)
                                            #region 20.4
                                            /*if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                            {
                                                var accessionLookupRows = dtAccessionInventoryLookup.Select("AccNumber = '" + cell.Value.ToString().Replace("CIP ", "") + "'");

                                                if (accessionLookupRows != null && accessionLookupRows.Count() == 1)
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_pedigree";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "male_accession_id";
                                                    dr["column_type"] = "int";
                                                    dr["int_value"] = accessionLookupRows[0].Field<int>("accession_id");
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                else if (accessionLookupRows.Count() == 0)
                                                {
                                                    cell.ErrorText = "Accession not found";
                                                }
                                                else if (accessionLookupRows.Count() > 1)
                                                    cell.ErrorText = "More than one Accession found";
                                                else
                                                    cell.ErrorText = "There was an error searching accession id";
                                            }*/
                                            #endregion
                                            break;


                                        case PassportCode.ACQDATE: //12. Acquisition date
                                            #region 12.
                                            if (!string.IsNullOrEmpty(cellValue))
                                            {
                                                string dateValue = cell.Value.ToString();
                                                string source_date = string.Empty;
                                                string source_date_code = string.Empty;

                                                if (dateValue.EndsWith("0000"))
                                                {
                                                    source_date_code = "yyyy";
                                                    source_date = dateValue.Substring(0, 4) + "0101";
                                                }
                                                else if (dateValue.EndsWith("00"))
                                                {
                                                    source_date_code = "MM/yyyy";
                                                    source_date = dateValue.Substring(0, 6) + "01";
                                                }
                                                else
                                                {
                                                    source_date_code = "MM/dd/yyyy";
                                                    source_date = dateValue;
                                                }
                                                //initial_received_date  datetime  , initial_received_date_code string
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["column_name"] = "initial_received_date";
                                                dr["column_type"] = "datetime";
                                                dr["datetime_value"] = DateTime.ParseExact(source_date, "yyyyMMdd", null);
                                                dr["datetime_format_code"] = source_date_code;
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            else
                                            {
                                                //initial_received_date  datetime  , initial_received_date_code string
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["column_name"] = "initial_received_date";
                                                dr["column_type"] = "datetime";
                                                dr["datetime_value"] = DBNull.Value;
                                                dr["datetime_format_code"] = string.Empty;
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.SAMPSTAT: //19. Biological Status
                                            #region 19.
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["column_name"] = "improvement_status_code";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.REMARKS: //28. Remarks
                                            #region 28.
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["column_name"] = "note";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.ACCESSION_STATUS: //Genebank Accession Status
                                            #region 00.1
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                dr["column_name"] = "status_code";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;

                                        case PassportCode.MLSSTAT: //27. MLS status of the accession
                                            #region 27.
                                            if (true)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_ipr";
                                                dr["table_id"] = row.Cells["accession_id"].Value;
                                                /*dr["filter_column"] = "type_code";
                                                dr["filter_value"] = "MLS";*/
                                                dr["column_name"] = "ipr_number";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            #endregion
                                            break;
                                        case PassportCode.ACCESSION_TAXON:
                                            #region TaxonName
                                            if (true)
                                            {
                                                if (row.Cells[PassportCode.ACCESSION_TAXON].Value != null && !string.IsNullOrEmpty(row.Cells[PassportCode.ACCESSION_TAXON].Value.ToString()))
                                                {
                                                    var sqltsl = "select value_member from taxonomy_species_lookup where display_member = '" + row.Cells[PassportCode.ACCESSION_TAXON].Value + "'";
                                                    DataTable dtTaxonomySpecies = _sharedUtils.GetLocalData(sqltsl, "");

                                                    if (dtTaxonomySpecies != null && dtTaxonomySpecies.Rows.Count == 1)
                                                    {
                                                        //insert table update
                                                        DataRow dr = dtTableUpdate.NewRow();
                                                        dr["table_name"] = "accession";
                                                        dr["table_id"] = row.Cells["accession_id"].Value;
                                                        dr["column_name"] = "taxonomy_species_id";
                                                        dr["column_type"] = "int";
                                                        dr["int_value"] = dtTaxonomySpecies.Rows[0].Field<int>("value_member");
                                                        dr["cell"] = row.Cells[PassportCode.ACCESSION_TAXON];
                                                        dtTableUpdate.Rows.Add(dr);
                                                        //row.Cells[PassportCode.ACCESSION_TAXON].Style.BackColor = Color.Orange;
                                                    }
                                                    else if (dtTaxonomySpecies.Rows.Count == 0)
                                                    {
                                                        row.ErrorText = "Species not found";
                                                    }
                                                    else if (dtTaxonomySpecies.Rows.Count > 1)
                                                        row.ErrorText = "More than one species found";
                                                    else
                                                        row.ErrorText = "There was an error searching taxonomy species id";
                                                }
                                            }
                                            #endregion
                                            break;

                                        default:
                                            //Check if custom descriptor is accession_inv_name
                                            var drCustomDescriptor = dtPassportCriteria.AsEnumerable().FirstOrDefault(x => x.Field<string>("value_member").Equals(crop_trait_id) && x.Field<string>("table_name").Equals("accession_inv_name"));
                                            if (drCustomDescriptor != null)
                                            {
                                                DataRow dr = dtTableUpdate.NewRow();
                                                dr["table_name"] = "accession_inv_name";
                                                dr["table_id"] = row.Cells["inventory_id"].Value;
                                                dr["filter_column"] = "category_code";
                                                dr["filter_value"] = drCustomDescriptor.Field<string>("category_code");
                                                dr["column_name"] = "plant_name";
                                                dr["column_type"] = "string";
                                                dr["string_value"] = cell.Value.ToString();
                                                dr["cell"] = cell;
                                                dtTableUpdate.Rows.Add(dr);
                                            }
                                            break;
                                    }
                                    #endregion
                                    break;
                                case "CROP_TRAIT":
                                    #region crop_trait_observation
                                    var observation = dtObservationUpdate.Select("inventory_id = " + inventory_id + " and crop_trait_id = " + crop_trait_id);

                                    if( observation != null && observation.Count() > 0 && string.IsNullOrEmpty(cell.Value.ToString()))
                                    {
                                        //delete crop_trait_observation
                                        observation[0].Delete();
//                                        dtObservationUpdate.Rows.Remove(observation[0]);
                                    }
                                    else if (observation != null && observation.Count() == 1 && !string.IsNullOrEmpty(cell.Value.ToString()))
                                    {
                                        var crop_trait_type = dtCropTraitIsCoded.Select("crop_trait_id = " + crop_trait_id);

                                        observation[0].SetField<string>("is_archived", ux_checkboxIsArchived.Checked ? "Y" : "N");

                                        string type_column = string.Empty;
                                        switch (crop_trait_type[0].Field<string>("type"))
                                        {
                                            case "CODED":
                                                DataTable dtCropTraitCode = _sharedUtils.GetLocalData("select value_member as crop_trait_code_id from acc_batch_wizard_crop_trait_code_lookup where crop_trait_id = " +
                                                    crop_trait_id + " and code = '" + cell.Value.ToString() + "'", "");
                                                int new_crop_trait_code_id = -1;
                                                if (dtCropTraitCode != null && dtCropTraitCode.Rows.Count == 1)
                                                {
                                                    new_crop_trait_code_id = dtCropTraitCode.Rows[0].Field<int>("crop_trait_code_id");

                                                    int code_trait_code_id = -1;
                                                    try
                                                    {
                                                        code_trait_code_id = observation[0].Field<int>("crop_trait_code_id");
                                                    }
                                                    catch
                                                    { code_trait_code_id = -1; }

                                                    if (code_trait_code_id == -1 || code_trait_code_id != new_crop_trait_code_id)
                                                    {
                                                        cell.Style.BackColor = Color.Orange;
                                                        cell.Tag = "MODIFIED";
                                                        observation[0].SetField<int>("crop_trait_code_id", new_crop_trait_code_id);
                                                    }
                                                }
                                                else
                                                {
                                                    cell.ErrorText = "Value is not accepted";
                                                    continue;
                                                }
                                                type_column = "crop_trait_code_id";

                                                break;
                                            case "CHAR":
                                                type_column = "string_value";
                                                var string_value = observation[0].Field<string>("string_value");
                                                var new_string_value = cell.Value.ToString();
                                                if (!new_string_value.Trim().Equals(string_value.Trim()))
                                                {
                                                    cell.Style.BackColor = Color.Orange;
                                                    cell.Tag = "MODIFIED";
                                                    observation[0].SetField<string>("string_value", new_string_value);
                                                }
                                                break;
                                            case "NUMERIC":
                                                type_column = "numeric_value";
                                                try
                                                {
                                                    var numeric_value = observation[0].Field<decimal>("numeric_value");
                                                    var new_numeric_value = decimal.Parse(cell.Value.ToString());
                                                    if (new_numeric_value != numeric_value)
                                                    {
                                                        cell.Style.BackColor = Color.Orange;
                                                        cell.Tag = "MODIFIED";
                                                        observation[0].SetField<decimal>("numeric_value", new_numeric_value);
                                                    }
                                                }
                                                catch (Exception numeric_error)
                                                {
                                                    cell.ErrorText = numeric_error.Message;
                                                    continue;
                                                }
                                                break;
                                        }
                                    }
                                    else if ((observation == null || observation.Count() == 0) && !string.IsNullOrEmpty(cell.Value.ToString()))
                                    {
                                        // insert observation
                                        DataRow dr = dtObservationInsert.NewRow();

                                        dr["inventory_id"] = inventory_id;
                                        dr["crop_trait_id"] = crop_trait_id;
                                        dr["crop_trait_code_id"] = DBNull.Value;
                                        dr["numeric_value"] = DBNull.Value;
                                        dr["string_value"] = DBNull.Value;
                                        dr["method_id"] = _method_id;
                                        dr["is_archived"] = ux_checkboxIsArchived.Checked ? "Y" : "N";
                                        string sNow = DateTime.Now.ToString();
                                        dr["created_by"] = _sharedUtils.UserCooperatorID;
                                        dr["created_date"] = sNow;
                                        dr["owned_by"] = _sharedUtils.UserCooperatorID;
                                        dr["owned_date"] = sNow;

                                        var crop_trait_type = dtCropTraitIsCoded.Select("crop_trait_id = " + crop_trait_id);

                                        string type_column = string.Empty;
                                        switch (crop_trait_type[0].Field<string>("type"))
                                        {
                                            case "CODED":
                                                DataTable dtCropTraitCode = _sharedUtils.GetLocalData("select value_member as crop_trait_code_id from acc_batch_wizard_crop_trait_code_lookup where crop_trait_id = " +
                                                    crop_trait_id + " and code = '" + cell.Value.ToString() + "'", "");

                                                if (dtCropTraitCode != null && dtCropTraitCode.Rows.Count == 1)
                                                    dr["crop_trait_code_id"] = dtCropTraitCode.Rows[0].Field<int>("crop_trait_code_id");
                                                else
                                                {
                                                    cell.ErrorText = "Value is not accepted";
                                                    continue;
                                                }
                                                break;
                                            case "CHAR":
                                                dr["string_value"] = cell.Value.ToString();
                                                break;
                                            case "NUMERIC":
                                                dr["numeric_value"] = decimal.Parse(cell.Value.ToString());
                                                break;
                                        }

                                        dtObservationInsert.Rows.Add(dr);

                                        cell.Style.BackColor = Color.Orange;
                                        cell.Tag = "MODIFIED";
                                    }
                                    #endregion
                                    break;
                            }
                        }
                    }//end_if
                }//end_foreach
                #endregion

                //Check if there are accessions updates
                #region save_table_accession
                var accessionUpdateRows = dtTableUpdate.Select("table_name = 'accession'");
                if (accessionUpdateRows.Count() > 0)
                {
                    string dataview_name = "acc_batch_wizard_get_accession_update";
                    List<string> columnNames = new List<string>() { PassportCode.ACCESSION_TAXON, PassportCode.ACQDATE, PassportCode.SAMPSTAT, PassportCode.REMARKS, PassportCode.ACCESSION_STATUS };
                    if (!_isCustomModificationNeeded) columnNames.Add(PassportCode.PUID);
                    string search_index_column = "accession_id";

                    List<int> accessionListUpdate = new List<int>();
                    foreach (DataRow r in accessionUpdateRows) accessionListUpdate.Add(r.Field<int>("table_id"));

                    DataSet dsAccessionUpdate = _sharedUtils.GetWebServiceData(dataview_name, ":accessionid=" + string.Join(",", accessionListUpdate), 0, 0);
                    if (dsAccessionUpdate != null && dsAccessionUpdate.Tables[dataview_name] != null)
                    {
                        DataTable dtAccessionUpdate = dsAccessionUpdate.Tables[dataview_name];
                        foreach (DataRow r in accessionUpdateRows)
                        {
                            DataRow rupdate = dtAccessionUpdate.Select("accession_id = " + r.Field<int>("table_id"))[0];
                            switch (r.Field<string>("column_type"))
                            {
                                case "int":
                                    if (rupdate.ItemArray[dtAccessionUpdate.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                        || rupdate.Field<int>(r.Field<string>("column_name")) != r.Field<int>("int_value"))
                                    {
                                        rupdate.SetField<int>(r.Field<string>("column_name"), r.Field<int>("int_value"));
                                        (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                    }
                                    break;
                                case "string":
                                    if (string.IsNullOrEmpty(rupdate.Field<string>(r.Field<string>("column_name")))
                                        || !rupdate.Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                    {
                                        rupdate.SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                        (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                    }
                                    break;
                                case "decimal":
                                    if (rupdate.ItemArray[dtAccessionUpdate.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                        || rupdate.Field<decimal>(r.Field<string>("column_name")) != r.Field<decimal>("decimal_value"))
                                    {
                                        rupdate.SetField<decimal>(r.Field<string>("column_name"), r.Field<decimal>("decimal_value"));
                                        (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                    }
                                    break;
                                case "datetime":
                                    if (r.IsNull("datetime_value"))
                                    {
                                        rupdate[r.Field<string>("column_name")] = DBNull.Value;
                                        rupdate[r.Field<string>("column_name") + "_code"] = DBNull.Value;
                                        (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                    }
                                    else if (rupdate.ItemArray[dtAccessionUpdate.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                        || rupdate.Field<DateTime>(r.Field<string>("column_name")) != r.Field<DateTime>("datetime_value"))
                                    {
                                        rupdate.SetField<DateTime>(r.Field<string>("column_name"), r.Field<DateTime>("datetime_value"));
                                        rupdate[r.Field<string>("column_name") + "_code"] = r.Field<string>("datetime_format_code");
                                        (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                    }
                                    break;
                            }
                        }

                        DataSet saveErrors = new DataSet();
                        saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionUpdate);
                        if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                        {
                            foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                            {
                                if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();

                                    foreach (var columnName in columnNames)
                                        if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor != StyleColors.ValidationOKWithoutChanges)
                                        {
                                            row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                            row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                        }
                                }
                                else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();
                                    foreach (var columnName in columnNames)
                                        if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor != StyleColors.ValidationOKWithoutChanges)
                                        {
                                            row.Cells[columnName].Style.BackColor = StyleColors.SavingOk;
                                            row.Cells[columnName].ErrorText = string.Empty;
                                        }
                                }
                            }
                        }
                    }
                }
                #endregion

                //send updates
                #region save_crop_trait_observation_updates
                if (true) // check if updates exists
                {
                    DataSet saveErrors = new DataSet();
                    saveErrors = _sharedUtils.SaveWebServiceData(_dsObservationUpdate);
                    if (saveErrors != null && saveErrors.Tables.Contains("accession_batch_wizard_get_crop_trait_observation"))
                    {
                        foreach (DataRow dr in saveErrors.Tables["accession_batch_wizard_get_crop_trait_observation"].Rows)
                        {
                            if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                            {
                                MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                    , /*dr["cipnumber"].ToString()*/"", dr["ExceptionMessage"].ToString()));
                            }
                            else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                            {
                                DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                row.Cells[dr["crop_trait_id"].ToString()].ErrorText = dr["ExceptionMessage"].ToString();
                                row.Cells[dr["crop_trait_id"].ToString()].Style.BackColor = StyleColors.SavingError;
                                savingErrorCount++;

                                /*MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}"
                                    , "", dr["ExceptionMessage"].ToString()));*/
                            }
                            else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() != "Success")
                            {
                                /*MessageBox.Show(string.Format("The {0} item could not be successfully deleted from your list.\n\nError message:\n\n{1}"
                                    , "", dr["ExceptionMessage"].ToString()));*/
                                DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                row.Cells[dr["crop_trait_id"].ToString()].ErrorText = dr["ExceptionMessage"].ToString();
                                row.Cells[dr["crop_trait_id"].ToString()].Style.BackColor = StyleColors.SavingError;
                                savingErrorCount++;
                            }
                            else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() == "Success")
                            {
                                DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                row.Cells[dr["crop_trait_id"].ToString()].Style.BackColor = StyleColors.SavingOk;
                                savingOKCount++;
                            }
                            else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                            {
                                DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                row.Cells[dr["crop_trait_id"].ToString()].Style.BackColor = StyleColors.SavingOk;
                                savingOKCount++;
                            }
                        }
                    }
                }
                #endregion
                //send inserts
                #region save_crop_trait_observation_inserts
                if (dtObservationInsert.Rows.Count > 0) // check if updates exists
                {
                    DataSet saveErrors = new DataSet();
                    saveErrors = _sharedUtils.SaveWebServiceData(dsObservationInsert);
                    if (saveErrors != null && saveErrors.Tables.Contains("acc_batch_wizard_get_cto_insert"))
                    {
                        foreach (DataRow dr in saveErrors.Tables["acc_batch_wizard_get_cto_insert"].Rows)
                        {
                            if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                            {
                                DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                row.Cells[dr["crop_trait_id"].ToString()].ErrorText = dr["ExceptionMessage"].ToString();
                                row.Cells[dr["crop_trait_id"].ToString()].Style.BackColor = StyleColors.SavingError;
                                savingErrorCount++;

                                /*MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                    , "", dr["ExceptionMessage"].ToString()));*/
                            }
                            else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                            {
                                /*MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}"
                                    , "", dr["ExceptionMessage"].ToString()));*/
                            }
                            else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() != "Success")
                            {
                                /*MessageBox.Show(string.Format("The {0} item could not be successfully deleted from your list.\n\nError message:\n\n{1}"
                                    , "", dr["ExceptionMessage"].ToString()));*/
                            }
                            else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                            {
                                DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                row.Cells[dr["crop_trait_id"].ToString()].Style.BackColor = StyleColors.SavingOk;
                                savingOKCount++;
                            }
                        }
                    }
                }
                #endregion

                //send accession_source_map changes
                #region AccesionSourceMapExt
                if(true)
                {
                    if (_dtAccessionSourceMapExt != null)
                    {
                        //Check if accession_source_id need update
                        var accessionIds = _dtAccessionSourceMapExt.AsEnumerable().Where(y => y.RowState != DataRowState.Deleted && y.Field<int>("accession_source_id") < 0).Select(x => x.Field<int>("accession_id")).ToList();
                        if (accessionIds.Count > 0)
                        {
                            DataSet dsAccessionSource = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_accession_source_update", ":accessionid=" + string.Join(",", accessionIds), 0, 0);
                            DataTable dtAccessionSource = null;
                            if (dsAccessionSource != null && dsAccessionSource.Tables.Contains("acc_batch_wizard_get_accession_source_update"))
                            {
                                dtAccessionSource = dsAccessionSource.Tables["acc_batch_wizard_get_accession_source_update"];
                            }

                            DataTable dtAccessionSourceNew = dtAccessionSource.Clone();
                            foreach (DataRow drAccSourceMapExt in _dtAccessionSourceMapExt.Rows)
                            {
                                if (drAccSourceMapExt.RowState != DataRowState.Deleted && drAccSourceMapExt.Field<int>("accession_source_id") < 0)
                                {
                                    var drAccessionSource = dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == drAccSourceMapExt.Field<int>("accession_id")
                                                                                                            && x.Field<string>("source_type_code").Equals(drAccSourceMapExt.Field<string>("source_type_code")));
                                    if (drAccessionSource != null) //update accession_source_id
                                    {
                                        drAccSourceMapExt.SetField<int>("accession_source_id", drAccessionSource.Field<int>("accession_source_id"));
                                    }
                                    else //Add to create accession_source
                                    {
                                        int minId;
                                        if (dtAccessionSourceNew.Rows.Count == 0)
                                        {
                                            minId = -1;
                                        }
                                        else
                                        {
                                            minId = dtAccessionSourceNew.AsEnumerable().Min(x => x.Field<int>("accession_source_id"));
                                            if (minId > 0)
                                            {
                                                minId = -1;
                                            }
                                            else
                                            {
                                                minId--;
                                            }
                                        }

                                        DataRow drNew = dtAccessionSourceNew.NewRow();
                                        drNew["accession_source_id"] = minId;
                                        drNew["accession_id"] = drAccSourceMapExt.Field<int>("accession_id");
                                        drNew["source_type_code"] = drAccSourceMapExt.Field<string>("source_type_code");
                                        drNew["is_origin"] = "Y";
                                        drNew["is_web_visible"] = "Y";
                                        dtAccessionSourceNew.Rows.Add(drNew);
                                    }
                                }
                            }
                            if (dtAccessionSourceNew.Rows.Count > 0)
                            {
                                dsAccessionSource.Tables.Remove(dtAccessionSource);
                                dsAccessionSource.Tables.Add(dtAccessionSourceNew);
                                DataSet dsAccessionSourceErrors = _sharedUtils.SaveWebServiceData(dsAccessionSource);

                                if (dsAccessionSourceErrors != null && dsAccessionSourceErrors.Tables.Contains("acc_batch_wizard_get_accession_source_update"))
                                {
                                    foreach (DataRow dr in dsAccessionSourceErrors.Tables["acc_batch_wizard_get_accession_source_update"].Rows)
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["accession_id"].Value.ToString().Equals(dr["accession_id"].ToString())).First();
                                        string columnName = string.Empty;
                                        switch (dr.Field<string>("source_type_code"))
                                        {
                                            case "COLLECTED": columnName = PassportCode.COLLCODE; break;
                                            case "DEVELOPED": columnName = PassportCode.BREDCODE; break;
                                            case "DONATED": columnName = PassportCode.DONORCODE; break;
                                        }
                                        if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                        {
                                            if (row.Cells[columnName].Style.BackColor == StyleColors.SavingError)
                                            {

                                            }
                                            else if (row.Cells[columnName].Style.BackColor == StyleColors.SavingOk)
                                            {
                                                row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                                savingErrorCount++;
                                                savingOKCount--;
                                            }
                                            else
                                            {
                                                row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                                savingErrorCount++;
                                            }
                                            row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString() + "\n";
                                        }
                                        else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                        {
                                            //Update accession_source_id
                                            foreach (DataRow drAccSourceMap in _dtAccessionSourceMapExt.Rows)
                                            {
                                                if (drAccSourceMap.RowState != DataRowState.Deleted && drAccSourceMap.Field<int>("accession_source_id") < 0 
                                                    && drAccSourceMap.Field<int>("accession_id") == dr.Field<int>("accession_id")
                                                    && drAccSourceMap.Field<string>("source_type_code") == dr.Field<string>("source_type_code"))
                                                {
                                                    drAccSourceMap.SetField<int>("accession_source_id", dr.Field<int>("NewPrimaryKeyID"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //DataSet dsChanges = _dsAccessionSourceMap.GetChanges();
                        DataSet saveErrors = new DataSet();
                        saveErrors = _sharedUtils.SaveWebServiceData(_dsAccessionSourceMapExt);

                        if (saveErrors != null && saveErrors.Tables.Contains("acc_batch_wizard_get_acc_source_map_ext"))
                        {
                            foreach (DataRow dr in saveErrors.Tables["acc_batch_wizard_get_acc_source_map_ext"].Rows)
                            {
                                DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["accession_id"].Value.ToString().Equals(dr["accession_id"].ToString())).First();
                                string columnName = string.Empty;
                                switch (dr.Field<string>("source_type_code"))
                                {
                                    case "COLLECTED": columnName = PassportCode.COLLCODE; break;
                                    case "DEVELOPED": columnName = PassportCode.BREDCODE; break;
                                    case "DONATED": columnName = PassportCode.DONORCODE; break;
                                }
                                if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    if (row.Cells[columnName].Style.BackColor == StyleColors.SavingError)
                                    {

                                    }
                                    else if (row.Cells[columnName].Style.BackColor == StyleColors.SavingOk)
                                    {
                                        row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                        savingErrorCount++;
                                        savingOKCount--;
                                    }
                                    else //Changes detected
                                    {
                                        row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                        savingErrorCount++;
                                    }
                                    row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString() + "\n";
                                }
                                else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    if (row.Cells[columnName].Style.BackColor == StyleColors.SavingError)
                                    {

                                    }
                                    else if (row.Cells[columnName].Style.BackColor == StyleColors.SavingOk)
                                    {
                                        row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                        savingErrorCount++;
                                        savingOKCount--;
                                    }
                                    else //Changes detected
                                    {
                                        row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                        savingErrorCount++;
                                    }
                                    row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString() + "\n";
                                } // Success
                                else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    if (row.Cells[columnName].Style.BackColor == StyleColors.SavingError)
                                    {

                                    }
                                    else if (row.Cells[columnName].Style.BackColor == StyleColors.SavingOk)
                                    {

                                    }
                                    else //Changes detected
                                    {
                                        row.Cells[columnName].Style.BackColor = StyleColors.SavingOk;
                                        savingOKCount++;
                                    }
                                }
                                else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    if (row.Cells[columnName].Style.BackColor == StyleColors.SavingError)
                                    {

                                    }
                                    else if (row.Cells[columnName].Style.BackColor == StyleColors.SavingOk)
                                    {

                                    }
                                    else //Changes detected
                                    {
                                        row.Cells[columnName].Style.BackColor = StyleColors.SavingOk;
                                        savingOKCount++;
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
                
                //save accession_inv_name
                foreach (DataGridViewColumn col in ux_datagridviewSheet.Columns)
                {
                    var accessionNameColumn = dtPassportCriteria.AsEnumerable().FirstOrDefault(x => x.Field<string>("value_member").Equals(col.Name) && x.Field<string>("table_name").Equals("accession_inv_name"));
                    if (accessionNameColumn != null)
                    {
                        int? plantNameRank = string.IsNullOrEmpty(accessionNameColumn.Field<string>("plant_name_rank")) ? null : (int?)int.Parse(accessionNameColumn.Field<string>("plant_name_rank"));
                        SaveAccessionInvName(datagridviewContainer, dtTableUpdate, accessionNameColumn.Field<string>("category_code"), plantNameRank);
                    }
                }

                //save accession_inv_name "27. MLS status of the accession" updates and inserts
                #region save_accession_ipr
                if (true)
                {
                    var accessionIPRRows = dtTableUpdate.Select("table_name = 'accession_IPR'");
                    string dataview_name = "acc_batch_wizard_get_accession_ipr";
                    if (accessionIPRRows.Count() > 0)
                    {
                        string columnName = accessionIPRRows[0].Field<DataGridViewCell>("cell").OwningColumn.Name;
                        string search_index_column = "accession_id";

                        List<int> accessionList = new List<int>();
                        foreach (DataRow r in accessionIPRRows)
                            accessionList.Add(r.Field<int>("table_id"));

                        DataSet dsAccessionIPR = _sharedUtils.GetWebServiceData(dataview_name, ":accessionid=" + string.Join(",", accessionList), 0, 0);
                        if (dsAccessionIPR != null && dsAccessionIPR.Tables[dataview_name] != null)
                        {
                            DataTable dtAccessionIPR = dsAccessionIPR.Tables[dataview_name];
                            foreach (DataRow r in accessionIPRRows)
                            {
                                DataRow[] rupdate = dtAccessionIPR.Select(search_index_column + " = " + r.Field<int>("table_id"));
                                if (rupdate.Count() == 0)
                                {
                                    DataRow dr = dsAccessionIPR.Tables[dataview_name].NewRow();
                                    dr[search_index_column] = r.Field<int>("table_id"); //accession_id = 
                                    dr["type_code"] = "MLS"; // usar filter_col & filter_val
                                    dr[r.Field<string>("column_name")] = r.Field<string>("string_value"); // ipr_number =
                                    dsAccessionIPR.Tables[dataview_name].Rows.Add(dr);

                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                }
                                else
                                {
                                    if (!rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                    {
                                        rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                        (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                    }
                                }

                            }
                        }

                        DataSet saveErrors = new DataSet();
                        saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionIPR);
                        if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                        {
                            foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                            {
                                DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();
                                if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                    row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                }
                                else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    row.Cells[columnName].Style.BackColor = StyleColors.SavingOk;
                                    row.Cells[columnName].ErrorText = string.Empty;
                                }
                                if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                    row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                }
                                else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    row.Cells[columnName].Style.BackColor = StyleColors.SavingOk;
                                    row.Cells[columnName].ErrorText = string.Empty;
                                }
                            }
                        }
                    }
                }
                #endregion

                //save accession_pedigree
                #region save_accession_pedigree
                if (true)
                {
                    var accessionPedigreeRows = dtTableUpdate.Select("table_name = 'accession_pedigree'");
                    string dataview_name = "acc_batch_wizard_get_accession_pedigree_update";
                    if (accessionPedigreeRows.Count() > 0)
                    {
                        string[] columnNames = new string[] { PassportCode.PARENT_FEMALE, PassportCode.PARENT_MALE, PassportCode.PARENT_FEMALE_ACCENUMB, PassportCode.PARENT_MALE_ACCENUM };
                        string search_index_column = "accession_id";

                        List<int> accessionList = new List<int>();
                        foreach (DataRow r in accessionPedigreeRows) accessionList.Add(r.Field<int>("table_id"));

                        DataSet dsAccessionPedigree = _sharedUtils.GetWebServiceData(dataview_name, ":accessionid=" + string.Join(",", accessionList), 0, 0);
                        if (dsAccessionPedigree != null && dsAccessionPedigree.Tables[dataview_name] != null)
                        {
                            DataTable dtAccessionPedigree = dsAccessionPedigree.Tables[dataview_name];
                            foreach (DataRow r in accessionPedigreeRows)
                            {
                                DataRow[] rupdate = dtAccessionPedigree.Select(search_index_column + " = " + r.Field<int>("table_id"));
                                if (rupdate.Count() == 0) //insert
                                {
                                    DataRow dr = dsAccessionPedigree.Tables[dataview_name].NewRow();
                                    dr[search_index_column] = r.Field<int>("table_id"); //accession_id = 
                                    switch (r.Field<string>("column_type"))
                                    {
                                        case "int":
                                            dr[r.Field<string>("column_name")] = r.Field<int>("int_value"); // male_accession_id, female_accession_id
                                            break;
                                        case "string":
                                            dr[r.Field<string>("column_name")] = r.Field<string>("string_value"); // male_external_accession, female_external_accession
                                            break;
                                    }
                                    dsAccessionPedigree.Tables[dataview_name].Rows.Add(dr);

                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                }
                                else //update
                                {
                                    switch (r.Field<string>("column_type"))
                                    {
                                        case "int":
                                            if (rupdate[0].ItemArray[dtAccessionPedigree.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                                || rupdate[0].Field<int>(r.Field<string>("column_name")) != r.Field<int>("int_value"))
                                            {
                                                rupdate[0].SetField<int>(r.Field<string>("column_name"), r.Field<int>("int_value")); // male_accession_id, female_accession_id
                                                (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                            }
                                            break;
                                        case "string":
                                            if (string.IsNullOrEmpty(rupdate[0].Field<string>(r.Field<string>("column_name")))
                                                || !rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                            {
                                                rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value")); // male_external_accession, female_external_accession
                                                (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                            }
                                            break;
                                    }
                                }
                            }
                        }

                        DataSet saveErrors = new DataSet();
                        saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionPedigree);
                        if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                        {
                            foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                            {
                                DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();
                                if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    foreach (var columnName in columnNames)
                                    {
                                        if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor != StyleColors.ValidationOKWithoutChanges)
                                        {
                                            row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                            row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                        }
                                    }
                                }
                                else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    foreach (var columnName in columnNames)
                                        if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor != StyleColors.ValidationOKWithoutChanges)
                                        {
                                            row.Cells[columnName].Style.BackColor = StyleColors.SavingOk;
                                            row.Cells[columnName].ErrorText = string.Empty;
                                        }
                                }
                                if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    foreach (var columnName in columnNames)
                                        if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor != StyleColors.ValidationOKWithoutChanges)
                                        {
                                            row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                            row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        }
                                }
                                else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    foreach (var columnName in columnNames)
                                        if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor != StyleColors.ValidationOKWithoutChanges)
                                        {
                                            row.Cells[columnName].Style.BackColor = StyleColors.SavingOk;
                                            row.Cells[columnName].ErrorText = string.Empty;
                                        }
                                }
                            }
                        }
                    }
                }
                #endregion

                //save accession_source
                #region save_accession_source
                if (true)
                {
                    var accessionPedigreeRows = dtTableUpdate.Select("table_name = 'accession_source'");
                    string dataview_name = "acc_batch_wizard_get_accession_source_update";
                    if (accessionPedigreeRows.Count() > 0)
                    {
                        string[] columnNames = new string[] { PassportCode.ORIGCTY, PassportCode.DECLATITUDE, PassportCode.DECLONGITUDE, PassportCode.ELEVATION, PassportCode.COLLDATE, 
                            "13. Country of origin", "Adm1", "Adm2", "Adm3","source_type_code", PassportCode.ACCESSION_SOURCE_FORMATTED_LOCALITY, PassportCode.ACCESSION_SOURCE_VERBATIM_LOCALITY,
                            PassportCode.COORDDATUM, PassportCode.COORDUNCERT, PassportCode.GEOREFMETH, PassportCode.ACCESSION_SOURCE_NOTE};
                        string search_index_column = "accession_id";

                        List<int> accessionList = new List<int>();
                        foreach (DataRow r in accessionPedigreeRows) accessionList.Add(r.Field<int>("table_id"));

                        DataSet dsAccessionPedigree = _sharedUtils.GetWebServiceData(dataview_name, ":accessionid=" + string.Join(",", accessionList), 0, 0);
                        if (dsAccessionPedigree != null && dsAccessionPedigree.Tables[dataview_name] != null)
                        {
                            DataTable dtAccessionPedigree = dsAccessionPedigree.Tables[dataview_name];
                            foreach (DataRow r in accessionPedigreeRows)
                            {
                                DataRow[] rupdate = dtAccessionPedigree.Select(search_index_column + " = " + r.Field<int>("table_id"));
                                if (rupdate.Count() == 0) //insert
                                {
                                    DataGridViewRow dgvRow = r.Field<DataGridViewCell>("cell").OwningRow;
                                    
                                    if (!r.IsNull("filter_value") && !string.IsNullOrEmpty(r.Field<string>("filter_value")))
                                    {
                                        DataRow dr = dsAccessionPedigree.Tables[dataview_name].NewRow();
                                        dr[search_index_column] = r.Field<int>("table_id"); //accession_id
                                        dr["source_type_code"] = r.Field<string>("filter_value");
                                        dr["is_origin"] = "Y";
                                        dr["is_web_visible"] = "Y";

                                        switch (r.Field<string>("column_type"))
                                        {
                                            case "int":
                                                if (r.IsNull("int_value"))
                                                    dr[r.Field<string>("column_name")] = DBNull.Value;
                                                else
                                                    dr[r.Field<string>("column_name")] = r.Field<int>("int_value");
                                                break;
                                            case "string":
                                                if (r.IsNull("string_value"))
                                                    dr[r.Field<string>("column_name")] = DBNull.Value;
                                                else
                                                    dr[r.Field<string>("column_name")] = r.Field<string>("string_value");
                                                break;
                                            case "decimal":
                                                if (r.IsNull("decimal_value"))
                                                    dr[r.Field<string>("column_name")] = DBNull.Value;
                                                else
                                                    dr[r.Field<string>("column_name")] = r.Field<decimal>("decimal_value");
                                                break;
                                            case "datetime":
                                                if (r.IsNull("datetime_value"))
                                                {
                                                    dr[r.Field<string>("column_name")] = DBNull.Value;
                                                    dr[r.Field<string>("column_name") + "_code"] = DBNull.Value;
                                                }
                                                else
                                                {
                                                    dr[r.Field<string>("column_name")] = r.Field<DateTime>("datetime_value");
                                                    dr[r.Field<string>("column_name") + "_code"] = r.Field<string>("datetime_format_code");
                                                }
                                                break;
                                        }
                                        dsAccessionPedigree.Tables[dataview_name].Rows.Add(dr);

                                        if (r.Field<string>("column_name").Equals("geography_id")) //GroupColumnMan.ContainsKey(table_name + column_name)
                                        {
                                            DataGridViewRow row = r.Field<DataGridViewCell>("cell").OwningRow;
                                            foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                row.Cells[column_name].Style.BackColor = Color.Orange;
                                        }
                                        else
                                            r.Field<DataGridViewCell>("cell").Style.BackColor = Color.Orange;
                                    }
                                    else
                                        r.Field<DataGridViewCell>("cell").ErrorText = "source_type_code must be COLLECTED, DEVELOPED or DONATED";
                                }
                                else //update
                                {
                                    switch (r.Field<string>("column_type"))
                                    {
                                        case "int":
                                            if (r.IsNull("int_value"))
                                            {
                                                rupdate[0][r.Field<string>("column_name")] = DBNull.Value; break;
                                            }
                                            if (rupdate[0].ItemArray[dtAccessionPedigree.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                                || rupdate[0].Field<int>(r.Field<string>("column_name")) != r.Field<int>("int_value"))
                                            {
                                                rupdate[0].SetField<int>(r.Field<string>("column_name"), r.Field<int>("int_value"));

                                                if (r.Field<string>("column_name").Equals("geography_id")) //GroupColumnMan.ContainsKey(table_name + column_name)
                                                {
                                                    DataGridViewRow row = r.Field<DataGridViewCell>("cell").OwningRow;
                                                    foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                        row.Cells[column_name].Style.BackColor = Color.Orange;
                                                }
                                                else
                                                    r.Field<DataGridViewCell>("cell").Style.BackColor = Color.Orange;
                                            }
                                            break;
                                        case "string":
                                            if (r.IsNull("string_value"))
                                            {
                                                rupdate[0][r.Field<string>("column_name")] = DBNull.Value; break;
                                            }
                                            if (string.IsNullOrEmpty(rupdate[0].Field<string>(r.Field<string>("column_name")))
                                                || !rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                            {
                                                rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                                (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                            }
                                            break;
                                        case "decimal":
                                            if (r.IsNull("decimal_value"))
                                            {
                                                rupdate[0][r.Field<string>("column_name")] = DBNull.Value; break;
                                            }
                                            if (rupdate[0].ItemArray[dtAccessionPedigree.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                                || rupdate[0].Field<decimal>(r.Field<string>("column_name")) != r.Field<decimal>("decimal_value"))
                                            {
                                                rupdate[0].SetField<decimal>(r.Field<string>("column_name"), r.Field<decimal>("decimal_value"));
                                                (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                            }
                                            break;
                                        case "datetime":
                                            if (r.IsNull("datetime_value"))
                                            {
                                                rupdate[0][r.Field<string>("column_name")] = DBNull.Value;
                                                rupdate[0][r.Field<string>("column_name") + "_code"] = DBNull.Value;
                                                break;
                                            }
                                            if (rupdate[0].ItemArray[dtAccessionPedigree.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                                || rupdate[0].Field<DateTime>(r.Field<string>("column_name")) != r.Field<DateTime>("datetime_value"))
                                            {
                                                rupdate[0].SetField<DateTime>(r.Field<string>("column_name"), r.Field<DateTime>("datetime_value"));
                                                rupdate[0][r.Field<string>("column_name") + "_code"] = r.Field<string>("datetime_format_code");
                                                (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                            }
                                            break;
                                    }
                                }
                            }
                        }

                        DataSet saveErrors = new DataSet();
                        saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionPedigree);
                        if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                        {
                            foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                            {
                                DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();
                                if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    foreach (var columnName in columnNames)
                                        if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor != StyleColors.ValidationOKWithoutChanges)
                                        {
                                            row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                            row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                        }
                                }
                                else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    foreach (var columnName in columnNames)
                                        if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor != StyleColors.ValidationOKWithoutChanges)
                                        {
                                            row.Cells[columnName].Style.BackColor = StyleColors.SavingOk;
                                            row.Cells[columnName].ErrorText = string.Empty;
                                        }
                                }
                                if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    foreach (var columnName in columnNames)
                                        if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor != StyleColors.ValidationOKWithoutChanges)
                                        {
                                            row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                            row.Cells[columnName].Style.BackColor = StyleColors.SavingError;
                                        }
                                }
                                else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    foreach (var columnName in columnNames)
                                        if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor != StyleColors.ValidationOKWithoutChanges)
                                        {
                                            row.Cells[columnName].Style.BackColor = StyleColors.SavingOk;
                                            row.Cells[columnName].ErrorText = string.Empty;
                                        }
                                }
                            }
                        }
                    }
                }
                #endregion

                watch.Stop();
                
                this.Invoke((MethodInvoker)delegate
                {
                    ux_progressBarWorking.Visible = false;
                    ux_buttonSaveData.Enabled = true;
                    ux_labelMessage.Text = "Elapsed time : " + watch.ElapsedMilliseconds + " ms";

                    savingErrorCount = 0;
                    savingOKCount = 0;
                    foreach (DataGridViewRow row in ux_datagridviewSheet.Rows)
                    {
                        for (int iCol = 1; iCol < ux_datagridviewSheet.Columns.Count; iCol++)
                        {
                            if (row.Cells[iCol].Style.BackColor == StyleColors.SavingError)
                            {
                                savingOKCount++;
                            }
                            else if (row.Cells[iCol].Style.BackColor == StyleColors.SavingOk)
                            {
                                savingOKCount++;
                            }
                        }
                    }

                    ux_labelSavingErrorCount.Text = savingErrorCount.ToString();
                    ux_labelSavingOkCount.Text = savingOKCount.ToString();
                    ux_labelSavingUnmodifiedCount.Text = savingUnmodifiedCount.ToString();

                    validationErrorCount = 0;
                    validationOKWithChangesCount = 0;
                    validationOKWithoutChangesCount = 0;
                    ux_labelValidationErrorCount.Text = "--";
                    ux_labelValidationOkWithChangesCount.Text = "--";
                    ux_labelValidationOkWithoutChangesCount.Text = "--";
                });
            }
            catch (Exception ex)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    ux_progressBarWorking.Visible = false;
                    ux_buttonSaveData.Enabled = true;
                    ux_labelMessage.Text = string.Empty;

                    savingErrorCount = 0;
                    savingUnmodifiedCount = 0;
                    savingOKCount = 0;

                    ux_labelSavingErrorCount.Text = "--";
                    ux_labelSavingOkCount.Text = "--";
                    ux_labelSavingUnmodifiedCount.Text = "--";
                });
                GGMessageBox mb = new GGMessageBox("\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        public void saveData()
        {
            try
            {
                DataGridView datagridviewContainer = ux_datagridviewSheet;
                DataTable dtSourceContainer = _main2;
                var watch = System.Diagnostics.Stopwatch.StartNew();

                List<string> AccessionNumbers = new List<string>();
                List<int> crop_trait_ids = new List<int>();
                //Obtain all crop_trait_ids from columns
                foreach (DataGridViewColumn col in datagridviewContainer.Columns)
                {
                    if (col.Index < startDescriptorsIndex) continue;
                    int crop_trait_id = -1;
                    int.TryParse(col.Name, out crop_trait_id);
                    if (crop_trait_id != -1)
                        crop_trait_ids.Add(crop_trait_id);
                }
                //Get all cooperator organizations
                DataSet dsCooperatorOrganization = null;
                DataTable dtCooperatorOrganization = null;
                if (dtSourceContainer.Columns.Contains(PassportCode.BREDNAME))
                {
                    dsCooperatorOrganization = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_cooperator_org", "", 0, 0);
                    if (dsCooperatorOrganization != null && dsCooperatorOrganization.Tables.Contains("acc_batch_wizard_get_cooperator_org"))
                        dtCooperatorOrganization = dsCooperatorOrganization.Tables["acc_batch_wizard_get_cooperator_org"];
                }
                //Fix some values depend on column name
                foreach (DataGridViewRow row in datagridviewContainer.Rows)
                {
                    if (row.Cells[0].Value == null || row.IsNewRow) continue;
                    if (string.IsNullOrEmpty(row.Cells[0].Value.ToString().Trim())) continue;
                    // Add cipnumbers to check if exist

                    row.Cells[0].Value = row.Cells[0].Value.ToString().Trim();
                    AccessionNumbers.Add("'" + row.Cells[0].Value.ToString() + "'");

                    if (dtSourceContainer.Columns.Contains(PassportCode.PARENT_FEMALE_ACCENUMB))
                    {
                        AccessionNumbers.Add("'" + row.Cells[PassportCode.PARENT_FEMALE_ACCENUMB].Value.ToString() + "'");
                    }
                    if (dtSourceContainer.Columns.Contains(PassportCode.PARENT_MALE_ACCENUM))
                    {
                        AccessionNumbers.Add("'" + row.Cells[PassportCode.PARENT_MALE_ACCENUM].Value.ToString() + "'");
                    }
                    // Create cooperator organization if not exist
                    if (dtSourceContainer.Columns.Contains(PassportCode.BREDNAME) && !string.IsNullOrEmpty(row.Cells[PassportCode.BREDNAME].Value.ToString()))
                    {
                        if (dtCooperatorOrganization != null)
                        {
                            var rows = dtCooperatorOrganization.Select("organization = '" + row.Cells[PassportCode.BREDNAME].Value + "'");
                            if (rows != null && !rows.Any())
                            {// insert cooperator
                                DataRow d = dtCooperatorOrganization.NewRow();
                                d["organization"] = row.Cells[PassportCode.BREDNAME].Value.ToString();
                                d["status_code"] = "ACTIVE";
                                d["sys_lang_id"] = _sharedUtils.UserLanguageCode;
                                dtCooperatorOrganization.Rows.Add(d);
                            }
                        }
                    }
                }
                //Save cooperator changes
                _sharedUtils.SaveWebServiceData(dsCooperatorOrganization);
                // kz-pending usar dsError en vez de llamar denuevo a acc_batch_wizard_get_cooperator_org
                //Get all cooperator organization again
                dsCooperatorOrganization = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_cooperator_org", "", 0, 0);
                if (dsCooperatorOrganization != null && dsCooperatorOrganization.Tables.Contains("acc_batch_wizard_get_cooperator_org"))
                    dtCooperatorOrganization = dsCooperatorOrganization.Tables["acc_batch_wizard_get_cooperator_org"];


                if (AccessionNumbers.Count > 0)
                {  //Get all accession_ids and inventory_ids
                    DataSet dsAccessionInventoryLookup = _sharedUtils.GetWebServiceData("accession_batch_wizard_get_inventoryid", ":accnumber=" + string.Join(",", AccessionNumbers), 0, 0);
                    DataTable dtAccessionInventoryLookup = null;
                    if (dsAccessionInventoryLookup != null && dsAccessionInventoryLookup.Tables.Contains("accession_batch_wizard_get_inventoryid"))
                    {
                        dtAccessionInventoryLookup = dsAccessionInventoryLookup.Tables["accession_batch_wizard_get_inventoryid"];
                    }
                    else
                    {
                        throw new WizardException("An error ocurred while getting accession_ids, inventory_ids");
                    }

                    List<int> inventory_ids = new List<int>();
                    foreach (DataRow r in dtAccessionInventoryLookup.Rows)
                        inventory_ids.Add(r.Field<int>("inventory_id"));

                    string[] string_inventory_ids = inventory_ids.ConvertAll(x => x.ToString()).ToArray();
                    string[] string_crop_trait_ids = crop_trait_ids.ConvertAll(x => x.ToString()).ToArray();

                    //Get all observations by method_id
                    string parameters = ":inventoryid=" + string.Join(",", string_inventory_ids) + ";:croptraitid=" + string.Join(",", string_crop_trait_ids) + ";:methodid=" + _method_id;
                    string get_crop_trait_observation_update = "accession_batch_wizard_get_crop_trait_observation";
                    DataSet dsObservationUpdate = _sharedUtils.GetWebServiceData(get_crop_trait_observation_update, parameters, 0, 0);
                    DataTable dtObservationUpdate = null;
                    if (dsObservationUpdate != null && dsObservationUpdate.Tables.Contains(get_crop_trait_observation_update))
                    {
                        dtObservationUpdate = dsObservationUpdate.Tables[get_crop_trait_observation_update];
                    }
                    DataSet dsCropTraitIsCoded = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_crop_trait_type", ":croptraitid=" + string.Join(",", string_crop_trait_ids), 0, 0);
                    DataTable dtCropTraitIsCoded = null;
                    if (dsCropTraitIsCoded != null && dsCropTraitIsCoded.Tables.Contains("acc_batch_wizard_get_crop_trait_type"))
                    {
                        dtCropTraitIsCoded = dsCropTraitIsCoded.Tables["acc_batch_wizard_get_crop_trait_type"];
                    }

                    DataSet dsAccession = _sharedUtils.GetWebServiceData("accession_batch_wizard_get_accession", ":accessionid=-1", 0, 0);
                    DataTable newAccessions = null;
                    List<DataGridViewRow> dgvrowsNewAccessions = new List<DataGridViewRow>();
                    if (dsAccession != null && dsAccession.Tables["accession_batch_wizard_get_accession"] != null)
                        newAccessions = dsAccession.Tables["accession_batch_wizard_get_accession"];

                    DataTable dtTableUpdate = new DataTable();
                    dtTableUpdate.Columns.Add("table_name", typeof(string));
                    dtTableUpdate.Columns.Add("table_id", typeof(Int32));
                    dtTableUpdate.Columns.Add("filter_column", typeof(string));
                    dtTableUpdate.Columns.Add("filter_value", typeof(string));
                    dtTableUpdate.Columns.Add("column_name", typeof(string));
                    dtTableUpdate.Columns.Add("column_type", typeof(string));
                    dtTableUpdate.Columns.Add("int_value", typeof(Int32));
                    dtTableUpdate.Columns.Add("string_value", typeof(string));
                    dtTableUpdate.Columns.Add("decimal_value", typeof(Decimal));
                    dtTableUpdate.Columns.Add("datetime_value", typeof(DateTime));
                    dtTableUpdate.Columns.Add("cell", typeof(DataGridViewCell));
                    dtTableUpdate.Columns.Add("second_table_id", typeof(Int32));

                    #region ValidateAccNumbers
                    foreach (DataGridViewRow row in datagridviewContainer.Rows)
                    {
                        if (row.IsNewRow) continue;

                        row.ErrorText = string.Empty;
                        if (row.Cells[0].Value == null)
                        {
                            row.ErrorText = "Accession Number is blank";
                            continue;
                        }

                        if (string.IsNullOrEmpty(row.Cells[0].Value.ToString().Trim()))
                        {
                            row.ErrorText = "Accession Number is blank";
                            continue;
                        }

                        string ACCNumber = row.Cells[0].Value.ToString().Trim();
                        try
                        {
                            ACCNumber = getAccessionNumber(ACCNumber);
                        }
                        catch (Exception e)
                        {
                            row.ErrorText = e.Message;
                            continue;
                        }

                        var resp = dtAccessionInventoryLookup.Select("AccNumber = '" + ACCNumber + "'");
                        if (resp == null || !resp.Any()) //Create Accession
                        {
                            #region create_accession
                            DataTable dtTaxonomySpecies = null;
                            if (row.Cells[PassportCode.ACCESSION_TAXON].Value == null)
                                row.ErrorText = "taxonomy_species_name not found";
                            else
                            {
                                var sqltsl = "select value_member from taxonomy_species_lookup where display_member = '" + row.Cells[PassportCode.ACCESSION_TAXON].Value + "'";
                                dtTaxonomySpecies = _sharedUtils.GetLocalData(sqltsl, "");
                            }
                            if (dtTaxonomySpecies != null && dtTaxonomySpecies.Rows.Count == 1)
                            {
                                DataRow dr = newAccessions.NewRow();
                                string[] accNumberParts = ACCNumber.Split(' ');

                                dr["accession_number_part1"] = accNumberParts[0];
                                dr["accession_number_part2"] = accNumberParts.Length > 1 ? accNumberParts[1] : "";
                                dr["accession_number_part3"] = accNumberParts.Length > 2 ? accNumberParts[2] : "";
                                dr["is_backed_up"] = "N";
                                dr["is_core"] = "N";
                                dr["is_web_visible"] = "N";
                                dr["status_code"] = "Not Defined";
                                dr["taxonomy_species_id"] = dtTaxonomySpecies.Rows[0].Field<int>("value_member");
                                string sNow = DateTime.Now.ToString();
                                dr["created_by"] = _sharedUtils.UserCooperatorID;
                                dr["created_date"] = sNow;
                                dr["owned_by"] = _sharedUtils.UserCooperatorID;
                                dr["owned_date"] = sNow;

                                newAccessions.Rows.Add(dr);
                                dgvrowsNewAccessions.Add(row);
                            }
                            else if (dtTaxonomySpecies.Rows.Count == 0)
                            {
                                row.ErrorText = "Species not found";
                            }
                            else if (dtTaxonomySpecies.Rows.Count > 1)
                                row.ErrorText = "More than one species found";
                            else
                                row.ErrorText = "There was an error searching taxonomy species id";
                            #endregion
                        }
                        else
                        {
                            #region accession_exists
                            //copy accession_id and inventory_id
                            row.Cells["accession_id"].Value = resp[0].Field<int>("accession_id");
                            row.Cells["inventory_id"].Value = resp[0].Field<int>("inventory_id");

                            //check if taxonomy_species_name is changed
                            DataTable dtTaxonomySpecies = null;
                            row.Cells[PassportCode.ACCESSION_TAXON].Style.BackColor = Color.White;

                            if (row.Cells[PassportCode.ACCESSION_TAXON].Value != null && !string.IsNullOrEmpty(row.Cells[PassportCode.ACCESSION_TAXON].Value.ToString()))
                            {
                                var sqltsl = "select value_member from taxonomy_species_lookup where display_member = '" + row.Cells[PassportCode.ACCESSION_TAXON].Value + "'";
                                dtTaxonomySpecies = _sharedUtils.GetLocalData(sqltsl, "");

                                if (dtTaxonomySpecies != null && dtTaxonomySpecies.Rows.Count == 1)
                                {
                                    //insert table update
                                    DataRow dr = dtTableUpdate.NewRow();
                                    dr["table_name"] = "accession";
                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                    dr["column_name"] = "taxonomy_species_id";
                                    dr["column_type"] = "int";
                                    dr["int_value"] = dtTaxonomySpecies.Rows[0].Field<int>("value_member");
                                    dr["cell"] = row.Cells[PassportCode.ACCESSION_TAXON];
                                    dtTableUpdate.Rows.Add(dr);
                                    //row.Cells[PassportCode.ACCESSION_TAXON].Style.BackColor = Color.Orange;
                                }
                                else if (dtTaxonomySpecies.Rows.Count == 0)
                                {
                                    row.ErrorText = "Species not found";
                                }
                                else if (dtTaxonomySpecies.Rows.Count > 1)
                                    row.ErrorText = "More than one species found";
                                else
                                    row.ErrorText = "There was an error searching taxonomy species id";
                            }
                            #endregion
                        }
                    }
                    #endregion
                    
                    //check if necessary to create accessions
                    #region save_accessions
                    if (newAccessions.Rows.Count > 0)
                    {
                        DataSet saveErrors = new DataSet();
                        saveErrors = _sharedUtils.SaveWebServiceData(dsAccession);
                        if (saveErrors != null && saveErrors.Tables.Contains("accession_batch_wizard_get_accession"))
                        {
                            foreach (DataRow dr in saveErrors.Tables["accession_batch_wizard_get_accession"].Rows)
                            {
                                if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[0].Value.ToString().Equals("CIP " + dr["cipnumber"].ToString())).First();
                                    if (row != null) row.ErrorText = dr["ExceptionMessage"].ToString();
                                    MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                        , dr["cipnumber"].ToString(), dr["ExceptionMessage"].ToString()));
                                }
                                else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}"
                                        , dr["cipnumber"].ToString(), dr["ExceptionMessage"].ToString()));
                                }
                                else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    MessageBox.Show(string.Format("The {0} item could not be successfully deleted from your list.\n\nError message:\n\n{1}"
                                        , dr["cipnumber"].ToString(), dr["ExceptionMessage"].ToString()));
                                }
                            }
                        }
                        foreach (var dgvr in dgvrowsNewAccessions)
                        {
                            if (dgvr.ErrorText == "")
                            {
                                string get_accession_inventory_params = ":accnumber=" + getAccessionNumber(dgvr.Cells[0].Value.ToString());
                                DataSet dstemp = _sharedUtils.GetWebServiceData("accession_batch_wizard_get_inventoryid", get_accession_inventory_params, 0, 0);
                                if (dstemp != null && dstemp.Tables["accession_batch_wizard_get_inventoryid"] != null &&
                                    dstemp.Tables["accession_batch_wizard_get_inventoryid"].Rows.Count > 0)
                                {
                                    dgvr.Cells["accession_id"].Value = dstemp.Tables["accession_batch_wizard_get_inventoryid"].Rows[0].Field<int>("accession_id");
                                    dgvr.Cells["inventory_id"].Value = dstemp.Tables["accession_batch_wizard_get_inventoryid"].Rows[0].Field<int>("inventory_id");
                                }
                                else
                                    dgvr.ErrorText = "An error ocurred while saving accession";
                            }
                        }
                    }
                    #endregion

                    DataSet dsObservationInsert = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_cto_insert", parameters, 0, 0);
                    DataTable dtObservationInsert = null;
                    if (dsObservationInsert != null && dsObservationInsert.Tables.Contains("acc_batch_wizard_get_cto_insert"))
                    {
                        dtObservationInsert = dsObservationInsert.Tables["acc_batch_wizard_get_cto_insert"];
                    }

                    /* At this moment every valid cipnumber must have an accession_id and inventory_id */
                    //Indicate Changes
                    foreach (DataGridViewRow row in datagridviewContainer.Rows)
                    {
                        if (row.Cells[0].Value == null || row.IsNewRow)
                            continue;

                        if (row.ErrorText == "")
                        {
                            for (int i = startDescriptorsIndex; i < row.Cells.Count; i++)
                            {
                                DataGridViewCell cell = row.Cells[i];
                                string inventory_id = row.Cells["inventory_id"].Value.ToString();
                                string crop_trait_id = cell.OwningColumn.Name;

                                cell.Style.BackColor = Color.White;
                                cell.ErrorText = "";
                                //Identify if column is a real crop_trait_id(Crop_trait record) or not (PASSPORT trait)
                                //Passport Trait Columns have a ColumnTag.Category = "PASSPORT"

                                switch (((ColumnTag)cell.OwningColumn.Tag).Category)
                                {
                                    case "GROUP_TRAIT":
                                        switch (crop_trait_id)
                                        {
                                            case "13. Country of origin":
                                                #region accession_source_geography_id
                                                if (!string.IsNullOrEmpty(row.Cells["13. Country of origin"].Value.ToString()) || !string.IsNullOrEmpty(row.Cells["Adm1"].Value.ToString())
                                                    || !string.IsNullOrEmpty(row.Cells["Adm2"].Value.ToString()) || !string.IsNullOrEmpty(row.Cells["Adm3"].Value.ToString()))
                                                {
                                                    string country_code = row.Cells["13. Country of origin"].Value.ToString();
                                                    string adm1 = row.Cells["Adm1"].Value.ToString();
                                                    string adm2 = row.Cells["Adm2"].Value.ToString();
                                                    string adm3 = row.Cells["Adm3"].Value.ToString();
                                                    string geography_name = country_code + (string.IsNullOrEmpty(adm1) ? "" : ", " + adm1) +
                                                        (string.IsNullOrEmpty(adm2) ? "" : ", " + adm2) + (string.IsNullOrEmpty(adm3) ? "" : ", " + adm3);


                                                    var sqlgeography_id = "select value_member as geography_id from geography_lookup where display_member = '" + geography_name + "'";
                                                    DataTable dtGeographyId = _sharedUtils.GetLocalData(sqlgeography_id, "");

                                                    if (dtGeographyId != null && dtGeographyId.Rows.Count == 1)
                                                    {
                                                        DataRow dr = dtTableUpdate.NewRow();
                                                        dr["table_name"] = "accession_source";
                                                        dr["table_id"] = row.Cells["accession_id"].Value;
                                                        dr["filter_column"] = "source_type_code";
                                                        dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                        dr["column_name"] = "geography_id";
                                                        dr["column_type"] = "int";
                                                        dr["int_value"] = dtGeographyId.Rows[0].Field<int>("geography_id");
                                                        dr["cell"] = cell;
                                                        dtTableUpdate.Rows.Add(dr);

                                                    }
                                                    else if (dtGeographyId.Rows.Count == 0)
                                                    {
                                                        foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                            row.Cells[column_name].ErrorText = "Geography record not found";
                                                    }
                                                    else if (dtGeographyId.Rows.Count > 1)
                                                        foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                            row.Cells[column_name].ErrorText = "More than one Geography record found";
                                                    else
                                                        foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                            row.Cells[column_name].ErrorText = "There was an error searching Geography id";

                                                }
                                                #endregion
                                                break;
                                            case "":
                                                break;
                                        }

                                        break;
                                    case "PASSPORT":
                                        #region PASSPORT
                                        switch (crop_trait_id)
                                        {
                                            case PassportCode.PUID: //0. DOI
                                                #region 0.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "DOI";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.COLLNUMB: //03.1.1 Collecting number
                                                #region 03.1.1
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "COLLECTOR";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.BREEDER_CODE: //03.1.2 Breeder code
                                                #region 03.1.2
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "BREEDER";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.BIOSAFETY_CODE: //03.1.3 Biosafety code
                                                #region 03.1.3
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "ACCESSION";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.COLLMISSID: //04.2 Collecting mission identifier
                                                #region 04.2
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "EXPLOREID";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.ACCENAME: //11. Accession name
                                                #region 11.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "CULTIVAR";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.OTHERNUMB: //24. Other identification associated with the accession
                                                #region 24.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "OTHER";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.DONORNUMB: //23. Donor accession number
                                                #region 23.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "DONOR";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;

                                            case PassportCode.DECLATITUDE: //15.1 Latitude of collecting site
                                                #region 15.1
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_source";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["filter_column"] = "source_type_code";
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                    dr["column_name"] = "latitude";
                                                    dr["column_type"] = "decimal";
                                                    dr["decimal_value"] = decimal.Parse(cell.Value.ToString());
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.DECLONGITUDE: //15.3 Longitude of collecting site
                                                #region 15.3
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_source";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["filter_column"] = "source_type_code";
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                    dr["column_name"] = "longitude";
                                                    dr["column_type"] = "decimal";
                                                    dr["decimal_value"] = decimal.Parse(cell.Value.ToString());
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.ELEVATION: //16. Elevation of collecting site
                                                #region 16.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_source";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["filter_column"] = "source_type_code";
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                    dr["column_name"] = "elevation_meters";
                                                    dr["column_type"] = "int";
                                                    dr["int_value"] = int.Parse(cell.Value.ToString());
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.COLLDATE: //17. Collecting date of sample
                                                #region 17.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    string dateValue = cell.Value.ToString();
                                                    string source_date = string.Empty;
                                                    string source_date_code = string.Empty;

                                                    if (dateValue.EndsWith("0000"))
                                                    {
                                                        source_date_code = "'yyyy'";
                                                        source_date = dateValue.Substring(0, 4) + "0101";
                                                    }
                                                    else if (dateValue.EndsWith("00"))
                                                    {
                                                        source_date_code = "MM/yyyy";
                                                        source_date = dateValue.Substring(0, 6) + "01";
                                                    }
                                                    else
                                                    {
                                                        source_date_code = "MM/dd/yyyy";
                                                        source_date = dateValue;
                                                    }
                                                    //source_date  datetime  , source_date_code string
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_source";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["filter_column"] = "source_type_code";
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                    dr["column_name"] = "source_date";
                                                    dr["column_type"] = "datetime";
                                                    dr["datetime_value"] = DateTime.ParseExact(source_date, "yyyyMMdd", null);
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            //source_type_code COLLECTED

                                            case PassportCode.BREDNAME: // 18.1 Breeding institute name
                                                #region 18.1
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    if (dtCooperatorOrganization != null && dtCooperatorOrganization.Rows.Count > 0)
                                                    {
                                                        var rows = dtCooperatorOrganization.Select("organization  = '" + cell.Value + "'");

                                                        if (rows != null && rows.Count() == 1)
                                                        {
                                                            DataRow dr = dtTableUpdate.NewRow();
                                                            dr["table_name"] = "accession_source_map";
                                                            dr["table_id"] = row.Cells["accession_id"].Value;
                                                            dr["column_name"] = "cooperator_id";
                                                            dr["column_type"] = "int";
                                                            dr["int_value"] = rows[0].Field<int>("cooperator_id");
                                                            dr["cell"] = cell;
                                                            dr["second_table_id"] = -1;
                                                            dtTableUpdate.Rows.Add(dr);

                                                            cell.Style.BackColor = Color.Yellow;
                                                        }
                                                        else if (rows.Count() == 0)
                                                        {
                                                            cell.ErrorText = "There was an error saving cooperator";
                                                        }
                                                        else if (rows.Count() > 1)
                                                            cell.ErrorText = "More than one cooperator found";
                                                        else
                                                            cell.ErrorText = "There was an error searching cooperator id";
                                                    }
                                                    else
                                                        cell.ErrorText = "There was an error searching Institute name";
                                                }
                                                #endregion
                                                break;
                                            //source_type_code DEVELOPED


                                            case PassportCode.PARENT_FEMALE: //20.1 Parent Female
                                                #region 20.1
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_pedigree";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "female_external_accession";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.PARENT_MALE: //20.2 Parent Male
                                                #region 20.2
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_pedigree";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "male_external_accession";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.PARENT_FEMALE_ACCENUMB: //20.3 Parent Female (Accenumb)
                                                #region 20.3
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    var accessionLookupRows = dtAccessionInventoryLookup.Select("AccNumber = '" + cell.Value.ToString().Replace("CIP ", "") + "'");

                                                    if (accessionLookupRows != null && accessionLookupRows.Count() == 1)
                                                    {
                                                        DataRow dr = dtTableUpdate.NewRow();
                                                        dr["table_name"] = "accession_pedigree";
                                                        dr["table_id"] = row.Cells["accession_id"].Value;
                                                        dr["column_name"] = "female_accession_id";
                                                        dr["column_type"] = "int";
                                                        dr["int_value"] = accessionLookupRows[0].Field<int>("accession_id");
                                                        dr["cell"] = cell;
                                                        dtTableUpdate.Rows.Add(dr);
                                                    }
                                                    else if (accessionLookupRows.Count() == 0)
                                                    {
                                                        cell.ErrorText = "Accession not found";
                                                    }
                                                    else if (accessionLookupRows.Count() > 1)
                                                        cell.ErrorText = "More than one Accession found";
                                                    else
                                                        cell.ErrorText = "There was an error searching accession id";
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.PARENT_MALE_ACCENUM: //20.4 Parent Male (Accenumb)
                                                #region 20.4
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    var accessionLookupRows = dtAccessionInventoryLookup.Select("AccNumber = '" + cell.Value.ToString().Replace("CIP ", "") + "'");

                                                    if (accessionLookupRows != null && accessionLookupRows.Count() == 1)
                                                    {
                                                        DataRow dr = dtTableUpdate.NewRow();
                                                        dr["table_name"] = "accession_pedigree";
                                                        dr["table_id"] = row.Cells["accession_id"].Value;
                                                        dr["column_name"] = "male_accession_id";
                                                        dr["column_type"] = "int";
                                                        dr["int_value"] = accessionLookupRows[0].Field<int>("accession_id");
                                                        dr["cell"] = cell;
                                                        dtTableUpdate.Rows.Add(dr);
                                                    }
                                                    else if (accessionLookupRows.Count() == 0)
                                                    {
                                                        cell.ErrorText = "Accession not found";
                                                    }
                                                    else if (accessionLookupRows.Count() > 1)
                                                        cell.ErrorText = "More than one Accession found";
                                                    else
                                                        cell.ErrorText = "There was an error searching accession id";
                                                }
                                                #endregion
                                                break;


                                            case PassportCode.ACQDATE: //12. Acquisition date
                                                #region 12.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    string dateValue = cell.Value.ToString();
                                                    string source_date = string.Empty;
                                                    string source_date_code = string.Empty;

                                                    if (dateValue.EndsWith("0000"))
                                                    {
                                                        source_date_code = "'yyyy'";
                                                        source_date = dateValue.Substring(0, 4) + "0101";
                                                    }
                                                    else if (dateValue.EndsWith("00"))
                                                    {
                                                        source_date_code = "MM/yyyy";
                                                        source_date = dateValue.Substring(0, 6) + "01";
                                                    }
                                                    else
                                                    {
                                                        source_date_code = "MM/dd/yyyy";
                                                        source_date = dateValue;
                                                    }
                                                    //initial_received_date  datetime  , initial_received_date_code string
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "initial_received_date";
                                                    dr["column_type"] = "datetime";
                                                    dr["datetime_value"] = DateTime.ParseExact(source_date, "yyyyMMdd", null);
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.SAMPSTAT: //19. Biological Status
                                                #region 19.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "improvement_status_code";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.REMARKS: //28. Remarks
                                                #region 28.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "note";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case PassportCode.ACCESSION_STATUS: //Genebank Accession Status
                                                #region 00.1
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "status_code";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;

                                            case PassportCode.MLSSTAT: //27. MLS status of the accession
                                                #region 27.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_ipr";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    /*dr["filter_column"] = "type_code";
                                                    dr["filter_value"] = "MLS";*/
                                                    dr["column_name"] = "ipr_number";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "": //
                                                break;
                                        }
                                        #endregion
                                        break;
                                    case "CROP_TRAIT":
                                        #region crop_trait_observation
                                        var observation = dtObservationUpdate.Select("inventory_id = " + inventory_id + " and crop_trait_id = " + crop_trait_id);

                                        if (observation != null && observation.Count() == 1 && !string.IsNullOrEmpty(cell.Value.ToString()))
                                        {
                                            var crop_trait_type = dtCropTraitIsCoded.Select("crop_trait_id = " + crop_trait_id);

                                            observation[0].SetField<string>("is_archived", ux_checkboxIsArchived.Checked ? "Y" : "N");

                                            string type_column = string.Empty;
                                            switch (crop_trait_type[0].Field<string>("type"))
                                            {
                                                case "CODED":
                                                    DataTable dtCropTraitCode = _sharedUtils.GetLocalData("select value_member as crop_trait_code_id from acc_batch_wizard_crop_trait_code_lookup where crop_trait_id = " +
                                                        crop_trait_id + " and code = '" + cell.Value.ToString() + "'", "");
                                                    int new_crop_trait_code_id = -1;
                                                    if (dtCropTraitCode != null && dtCropTraitCode.Rows.Count == 1)
                                                    {
                                                        new_crop_trait_code_id = dtCropTraitCode.Rows[0].Field<int>("crop_trait_code_id");

                                                        int code_trait_code_id = -1;
                                                        try
                                                        {
                                                            code_trait_code_id = observation[0].Field<int>("crop_trait_code_id");
                                                        }
                                                        catch
                                                        { code_trait_code_id = -1; }

                                                        if (code_trait_code_id == -1 || code_trait_code_id != new_crop_trait_code_id)
                                                        {
                                                            cell.Style.BackColor = Color.Orange;
                                                            cell.Tag = "MODIFIED";
                                                            observation[0].SetField<int>("crop_trait_code_id", new_crop_trait_code_id);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cell.ErrorText = "Value is not accepted";
                                                        continue;
                                                    }
                                                    type_column = "crop_trait_code_id";

                                                    break;
                                                case "CHAR":
                                                    type_column = "string_value";
                                                    var string_value = observation[0].Field<string>("string_value");
                                                    var new_string_value = cell.Value.ToString();
                                                    if (!new_string_value.Trim().Equals(string_value.Trim()))
                                                    {
                                                        cell.Style.BackColor = Color.Orange;
                                                        cell.Tag = "MODIFIED";
                                                        observation[0].SetField<string>("string_value", new_string_value);
                                                    }
                                                    break;
                                                case "NUMERIC":
                                                    type_column = "numeric_value";
                                                    try
                                                    {
                                                        var numeric_value = observation[0].Field<decimal>("numeric_value");
                                                        var new_numeric_value = decimal.Parse(cell.Value.ToString());
                                                        if (new_numeric_value != numeric_value)
                                                        {
                                                            cell.Style.BackColor = Color.Orange;
                                                            cell.Tag = "MODIFIED";
                                                            observation[0].SetField<decimal>("numeric_value", new_numeric_value);
                                                        }
                                                    }
                                                    catch (Exception numeric_error)
                                                    {
                                                        cell.ErrorText = numeric_error.Message;
                                                        continue;
                                                    }
                                                    break;
                                            }
                                        }
                                        else if ((observation == null || observation.Count() == 0) && !string.IsNullOrEmpty(cell.Value.ToString()))
                                        {
                                            // insert observation
                                            DataRow dr = dtObservationInsert.NewRow();

                                            dr["inventory_id"] = inventory_id;
                                            dr["crop_trait_id"] = crop_trait_id;
                                            dr["crop_trait_code_id"] = DBNull.Value;
                                            dr["numeric_value"] = DBNull.Value;
                                            dr["string_value"] = DBNull.Value;
                                            dr["method_id"] = _method_id;
                                            dr["is_archived"] = ux_checkboxIsArchived.Checked ? "Y" : "N";
                                            string sNow = DateTime.Now.ToString();
                                            dr["created_by"] = _sharedUtils.UserCooperatorID;
                                            dr["created_date"] = sNow;
                                            dr["owned_by"] = _sharedUtils.UserCooperatorID;
                                            dr["owned_date"] = sNow;

                                            var crop_trait_type = dtCropTraitIsCoded.Select("crop_trait_id = " + crop_trait_id);

                                            string type_column = string.Empty;
                                            switch (crop_trait_type[0].Field<string>("type"))
                                            {
                                                case "CODED":
                                                    DataTable dtCropTraitCode = _sharedUtils.GetLocalData("select value_member as crop_trait_code_id from acc_batch_wizard_crop_trait_code_lookup where crop_trait_id = " +
                                                        crop_trait_id + " and code = '" + cell.Value.ToString() + "'", "");

                                                    if (dtCropTraitCode != null && dtCropTraitCode.Rows.Count == 1)
                                                        dr["crop_trait_code_id"] = dtCropTraitCode.Rows[0].Field<int>("crop_trait_code_id");
                                                    else
                                                    {
                                                        cell.ErrorText = "Value is not accepted";
                                                        continue;
                                                    }
                                                    break;
                                                case "CHAR":
                                                    dr["string_value"] = cell.Value.ToString();
                                                    break;
                                                case "NUMERIC":
                                                    dr["numeric_value"] = decimal.Parse(cell.Value.ToString());
                                                    break;
                                            }

                                            dtObservationInsert.Rows.Add(dr);

                                            cell.Style.BackColor = Color.Orange;
                                            cell.Tag = "MODIFIED";
                                        }
                                        #endregion
                                        break;
                                }

                                /*
                                if (crop_trait_id.IndexOf('.') > -1)
                                { // fake crop_trait_id has at least one '.'
                                    
                                }
                                else
                                { // real crop_trait_id is a integer
                                    
                                }*/

                            }
                        }//end_if
                    }//end_foreach

                    //Check if there are accessions updates
                    #region save_table_accession
                    var accessionUpdateRows = dtTableUpdate.Select("table_name = 'accession'");
                    if (accessionUpdateRows.Count() > 0)
                    {
                        string dataview_name = "acc_batch_wizard_get_accession_update";
                        string[] columnNames = new string[] { PassportCode.ACCESSION_TAXON, PassportCode.ACQDATE, PassportCode.SAMPSTAT, PassportCode.REMARKS, PassportCode.ACCESSION_STATUS };
                        string search_index_column = "accession_id";

                        List<int> accessionListUpdate = new List<int>();
                        foreach (DataRow r in accessionUpdateRows) accessionListUpdate.Add(r.Field<int>("table_id"));

                        DataSet dsAccessionUpdate = _sharedUtils.GetWebServiceData(dataview_name, ":accessionid=" + string.Join(",", accessionListUpdate), 0, 0);
                        if (dsAccessionUpdate != null && dsAccessionUpdate.Tables[dataview_name] != null)
                        {
                            DataTable dtAccessionUpdate = dsAccessionUpdate.Tables[dataview_name];
                            foreach (DataRow r in accessionUpdateRows)
                            {
                                DataRow rupdate = dtAccessionUpdate.Select("accession_id = " + r.Field<int>("table_id"))[0];
                                switch (r.Field<string>("column_type"))
                                {
                                    case "int":
                                        if (rupdate.ItemArray[dtAccessionUpdate.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                            || rupdate.Field<int>(r.Field<string>("column_name")) != r.Field<int>("int_value"))
                                        {
                                            rupdate.SetField<int>(r.Field<string>("column_name"), r.Field<int>("int_value"));
                                            (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                        }
                                        break;
                                    case "string":
                                        if (string.IsNullOrEmpty(rupdate.Field<string>(r.Field<string>("column_name")))
                                            || !rupdate.Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                        {
                                            rupdate.SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                            (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                        }
                                        break;
                                    case "decimal":
                                        if (rupdate.ItemArray[dtAccessionUpdate.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                            || rupdate.Field<decimal>(r.Field<string>("column_name")) != r.Field<decimal>("decimal_value"))
                                        {
                                            rupdate.SetField<decimal>(r.Field<string>("column_name"), r.Field<decimal>("decimal_value"));
                                            (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                        }
                                        break;
                                    case "datetime":
                                        if (rupdate.ItemArray[dtAccessionUpdate.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                            || rupdate.Field<DateTime>(r.Field<string>("column_name")) != r.Field<DateTime>("datetime_value"))
                                        {
                                            rupdate.SetField<DateTime>(r.Field<string>("column_name"), r.Field<DateTime>("datetime_value"));
                                            (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                        }
                                        break;
                                }
                            }

                            DataSet saveErrors = new DataSet();
                            saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionUpdate);
                            if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                            {
                                foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                                {
                                    if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();

                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName))
                                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                    }
                                    else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor == Color.Orange)
                                            {
                                                row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                                row.Cells[columnName].ErrorText = string.Empty;
                                            }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    //send updates
                    #region save_crop_trait_observation_updates
                    if (true) // check if updates exists
                    {
                        DataSet saveErrors = new DataSet();
                        saveErrors = _sharedUtils.SaveWebServiceData(dsObservationUpdate);
                        if (saveErrors != null && saveErrors.Tables.Contains(get_crop_trait_observation_update))
                        {
                            foreach (DataRow dr in saveErrors.Tables[get_crop_trait_observation_update].Rows)
                            {
                                if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                        , /*dr["cipnumber"].ToString()*/"", dr["ExceptionMessage"].ToString()));
                                }
                                else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                    row.Cells[dr["crop_trait_id"].ToString()].ErrorText = dr["ExceptionMessage"].ToString();

                                    /*MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}"
                                        , "", dr["ExceptionMessage"].ToString()));*/
                                }
                                else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    MessageBox.Show(string.Format("The {0} item could not be successfully deleted from your list.\n\nError message:\n\n{1}"
                                        , "", dr["ExceptionMessage"].ToString()));
                                }
                                else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                    row.Cells[dr["crop_trait_id"].ToString()].Style.BackColor = Color.LightBlue;

                                }
                            }
                        }
                    }
                    #endregion
                    //send inserts
                    #region save_crop_trait_observation_inserts
                    if (dtObservationInsert.Rows.Count > 0) // check if updates exists
                    {
                        DataSet saveErrors = new DataSet();
                        saveErrors = _sharedUtils.SaveWebServiceData(dsObservationInsert);
                        if (saveErrors != null && saveErrors.Tables.Contains("acc_batch_wizard_get_cto_insert"))
                        {
                            foreach (DataRow dr in saveErrors.Tables["acc_batch_wizard_get_cto_insert"].Rows)
                            {
                                if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                    row.Cells[dr["crop_trait_id"].ToString()].ErrorText = dr["ExceptionMessage"].ToString();

                                    /*MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                        , "", dr["ExceptionMessage"].ToString()));*/
                                }
                                else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    /*MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}"
                                        , "", dr["ExceptionMessage"].ToString()));*/
                                }
                                else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    /*MessageBox.Show(string.Format("The {0} item could not be successfully deleted from your list.\n\nError message:\n\n{1}"
                                        , "", dr["ExceptionMessage"].ToString()));*/
                                }
                                else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                    //row.Cells[dr["crop_trait_id"].ToString()].Style.BackColor = Color.Green;
                                    row.Cells[dr["crop_trait_id"].ToString()].Style.BackColor = Color.LightBlue;
                                }
                            }
                        }
                    }
                    #endregion

                    //save accession_inv_name "0. DOI" updates and inserts
                    #region save_accession_inv_name_BREEDER
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "DOI", 6);
                    #endregion

                    //save accession_inv_name "11. Accession name" updates and inserts
                    #region save_accession_inv_name_CULTIVAR
                    var accessionInvNameCultivarRows = dtTableUpdate.Select("table_name = 'accession_inv_name' and filter_value = 'CULTIVAR'");
                    if (accessionInvNameCultivarRows.Count() > 0)
                    {
                        List<int> inventoryList = new List<int>();
                        foreach (DataRow r in accessionInvNameCultivarRows)
                        {
                            inventoryList.Add(r.Field<int>("table_id"));
                        }
                        DataSet dsAccessionInvName = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_acc_inv_name_update", ":inventoryid=" + string.Join(",", inventoryList) + ";:categorycode=CULTIVAR", 0, 0);
                        if (dsAccessionInvName != null && dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"] != null)
                        {
                            DataTable dtAccessionInvName = dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"];
                            foreach (DataRow r in accessionInvNameCultivarRows)
                            {
                                DataRow[] rupdate = dtAccessionInvName.Select("inventory_id = " + r.Field<int>("table_id"));
                                if (rupdate.Count() == 0)
                                {
                                    DataRow dr = dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"].NewRow();
                                    dr["inventory_id"] = r.Field<int>("table_id");
                                    dr["category_code"] = "CULTIVAR";
                                    dr["plant_name"] = r.Field<string>("string_value");
                                    dr["plant_name_rank"] = 0;
                                    dr["is_web_visible"] = "Y";
                                    dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"].Rows.Add(dr);

                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                }
                                else
                                {
                                    if (!rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                    {
                                        rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                        (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                    }
                                }

                            }

                            DataSet saveErrors = new DataSet();
                            saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionInvName);
                            if (saveErrors != null && saveErrors.Tables.Contains("acc_batch_wizard_get_acc_inv_name_update"))
                            {
                                foreach (DataRow dr in saveErrors.Tables["acc_batch_wizard_get_acc_inv_name_update"].Rows)
                                {
                                    if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                        row.Cells[PassportCode.ACCENAME].ErrorText = dr["ExceptionMessage"].ToString();
                                        MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                            , dr["cipnumber"].ToString(), dr["ExceptionMessage"].ToString()));
                                    }
                                    else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                        //row.Cells[PassportCode.ACCENAME].Style.BackColor = Color.Green;
                                        row.Cells[PassportCode.ACCENAME].Style.BackColor = Color.LightBlue;
                                        row.Cells[PassportCode.ACCENAME].ErrorText = string.Empty;
                                    }
                                    if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                        row.Cells[PassportCode.ACCENAME].ErrorText = dr["ExceptionMessage"].ToString();
                                        MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}"
                                            , "", dr["ExceptionMessage"].ToString()));
                                    }
                                    else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                        row.Cells[PassportCode.ACCENAME].Style.BackColor = Color.LightBlue;
                                        row.Cells[PassportCode.ACCENAME].ErrorText = string.Empty;
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    //save accession_inv_name "03.1.1 Collecting number" updates and inserts
                    #region save_accession_inv_name_COLLECTOR
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "COLLECTOR", 3);
                    #endregion

                    //save accession_inv_name "03.1.2 Breeder code" updates and inserts
                    #region save_accession_inv_name_BREEDER
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "BREEDER", 3);
                    #endregion

                    //save accession_inv_name "03.1.3 Biosafety code" updates and inserts
                    #region save_accession_inv_name_ACCESSION
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "ACCESSION", 3);
                    #endregion

                    //save accession_inv_name "04.2 Collecting mission identifier" updates and inserts
                    #region save_accession_inv_name_EXPLOREID
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "EXPLOREID", 5);
                    #endregion

                    //save accession_inv_name "24. Other identification associated with the accession" updates and inserts
                    #region save_accession_inv_name_DONOR
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "DONOR", 1);
                    #endregion

                    //save accession_inv_name "24. Other identification associated with the accession" updates and inserts
                    #region save_accession_inv_name_OTHER
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "OTHER", 2);
                    #endregion

                    //save accession_inv_name "27. MLS status of the accession" updates and inserts
                    #region save_accession_ipr
                    if (true)
                    {
                        var accessionIPRRows = dtTableUpdate.Select("table_name = 'accession_IPR'");
                        string dataview_name = "acc_batch_wizard_get_accession_ipr";
                        if (accessionIPRRows.Count() > 0)
                        {
                            string columnName = accessionIPRRows[0].Field<DataGridViewCell>("cell").OwningColumn.Name;
                            string search_index_column = "accession_id";

                            List<int> accessionList = new List<int>();
                            foreach (DataRow r in accessionIPRRows)
                                accessionList.Add(r.Field<int>("table_id"));

                            DataSet dsAccessionIPR = _sharedUtils.GetWebServiceData(dataview_name, ":accessionid=" + string.Join(",", accessionList), 0, 0);
                            if (dsAccessionIPR != null && dsAccessionIPR.Tables[dataview_name] != null)
                            {
                                DataTable dtAccessionIPR = dsAccessionIPR.Tables[dataview_name];
                                foreach (DataRow r in accessionIPRRows)
                                {
                                    DataRow[] rupdate = dtAccessionIPR.Select(search_index_column + " = " + r.Field<int>("table_id"));
                                    if (rupdate.Count() == 0)
                                    {
                                        DataRow dr = dsAccessionIPR.Tables[dataview_name].NewRow();
                                        dr[search_index_column] = r.Field<int>("table_id"); //accession_id = 
                                        dr["type_code"] = "MLS"; // usar filter_col & filter_val
                                        dr[r.Field<string>("column_name")] = r.Field<string>("string_value"); // ipr_number =
                                        dsAccessionIPR.Tables[dataview_name].Rows.Add(dr);

                                        (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                    }
                                    else
                                    {
                                        if (!rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                        {
                                            rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                            (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                        }
                                    }

                                }
                            }

                            DataSet saveErrors = new DataSet();
                            saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionIPR);
                            if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                            {
                                foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();
                                    if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}", "", dr["ExceptionMessage"].ToString()));
                                    }
                                    else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        //row.Cells[columnName].Style.BackColor = Color.Green;
                                        row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                        row.Cells[columnName].ErrorText = string.Empty;
                                    }
                                    if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}", "", dr["ExceptionMessage"].ToString()));
                                    }
                                    else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                        row.Cells[columnName].ErrorText = string.Empty;
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    //save accession_pedigree
                    #region save_accession_pedigree
                    if (true)
                    {
                        var accessionPedigreeRows = dtTableUpdate.Select("table_name = 'accession_pedigree'");
                        string dataview_name = "acc_batch_wizard_get_accession_pedigree_update";
                        if (accessionPedigreeRows.Count() > 0)
                        {
                            string[] columnNames = new string[] { PassportCode.PARENT_FEMALE, PassportCode.PARENT_MALE, PassportCode.PARENT_FEMALE_ACCENUMB, PassportCode.PARENT_MALE_ACCENUM };
                            string search_index_column = "accession_id";

                            List<int> accessionList = new List<int>();
                            foreach (DataRow r in accessionPedigreeRows) accessionList.Add(r.Field<int>("table_id"));

                            DataSet dsAccessionPedigree = _sharedUtils.GetWebServiceData(dataview_name, ":accessionid=" + string.Join(",", accessionList), 0, 0);
                            if (dsAccessionPedigree != null && dsAccessionPedigree.Tables[dataview_name] != null)
                            {
                                DataTable dtAccessionPedigree = dsAccessionPedigree.Tables[dataview_name];
                                foreach (DataRow r in accessionPedigreeRows)
                                {
                                    DataRow[] rupdate = dtAccessionPedigree.Select(search_index_column + " = " + r.Field<int>("table_id"));
                                    if (rupdate.Count() == 0) //insert
                                    {
                                        DataRow dr = dsAccessionPedigree.Tables[dataview_name].NewRow();
                                        dr[search_index_column] = r.Field<int>("table_id"); //accession_id = 
                                        switch (r.Field<string>("column_type"))
                                        {
                                            case "int":
                                                dr[r.Field<string>("column_name")] = r.Field<int>("int_value"); // male_accession_id, female_accession_id
                                                break;
                                            case "string":
                                                dr[r.Field<string>("column_name")] = r.Field<string>("string_value"); // male_external_accession, female_external_accession
                                                break;
                                        }
                                        dsAccessionPedigree.Tables[dataview_name].Rows.Add(dr);

                                        (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                    }
                                    else //update
                                    {
                                        switch (r.Field<string>("column_type"))
                                        {
                                            case "int":
                                                if (rupdate[0].ItemArray[dtAccessionPedigree.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                                    || rupdate[0].Field<int>(r.Field<string>("column_name")) != r.Field<int>("int_value"))
                                                {
                                                    rupdate[0].SetField<int>(r.Field<string>("column_name"), r.Field<int>("int_value")); // male_accession_id, female_accession_id
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                                break;
                                            case "string":
                                                if (string.IsNullOrEmpty(rupdate[0].Field<string>(r.Field<string>("column_name")))
                                                    || !rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                                {
                                                    rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value")); // male_external_accession, female_external_accession
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                                break;
                                        }
                                    }
                                }
                            }

                            DataSet saveErrors = new DataSet();
                            saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionPedigree);
                            if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                            {
                                foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();
                                    if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName)) row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        /*MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}", "", dr["ExceptionMessage"].ToString()));*/
                                    }
                                    else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor == Color.Orange)
                                            {
                                                //row.Cells[columnName].Style.BackColor = Color.Green;
                                                row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                                row.Cells[columnName].ErrorText = string.Empty;
                                            }
                                    }
                                    if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName))
                                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        /*MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}", "", dr["ExceptionMessage"].ToString()));*/
                                    }
                                    else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor == Color.Orange)
                                            {
                                                row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                                row.Cells[columnName].ErrorText = string.Empty;
                                            }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    //save accession_source_map
                    #region save_accession_source_map
                    if (true)
                    {
                        var accessionAccessionSourceMapRows = dtTableUpdate.Select("table_name = 'accession_source_map'");
                        //validar accession_source_ids

                        string dataview_name_acc_source = "acc_batch_wizard_get_accession_source_update";
                        if (accessionAccessionSourceMapRows.Count() > 0)
                        {
                            string search_index_column_acc_source = "accession_id";

                            List<int> accessionList = new List<int>();
                            foreach (DataRow r in accessionAccessionSourceMapRows) accessionList.Add(r.Field<int>("table_id"));

                            DataSet dsAccessionSource = _sharedUtils.GetWebServiceData(dataview_name_acc_source, ":accessionid=" + string.Join(",", accessionList), 0, 0);
                            if (dsAccessionSource != null && dsAccessionSource.Tables[dataview_name_acc_source] != null)
                            {
                                DataTable dtAccessionSource = dsAccessionSource.Tables[dataview_name_acc_source];
                                foreach (DataRow r in accessionAccessionSourceMapRows)
                                {
                                    DataRow[] rupdate = dtAccessionSource.Select(search_index_column_acc_source + " = " + r.Field<int>("table_id")); //search accesion_source by accession_id
                                    if (rupdate.Count() == 0) //insert
                                    {
                                        DataGridViewRow dgvRow = r.Field<DataGridViewCell>("cell").OwningRow;
                                        if (dgvRow.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(dgvRow.Cells["source_type_code"].Value.ToString()))
                                        {
                                            DataRow dr = dsAccessionSource.Tables[dataview_name_acc_source].NewRow();
                                            dr[search_index_column_acc_source] = r.Field<int>("table_id"); //accession_id
                                            dr["source_type_code"] = dgvRow.Cells["source_type_code"].Value;
                                            dr["is_origin"] = "Y";
                                            dr["is_web_visible"] = "Y";
                                            dsAccessionSource.Tables[dataview_name_acc_source].Rows.Add(dr);
                                        }
                                        else
                                        {
                                            r.Field<DataGridViewCell>("cell").ErrorText = "source_type_code must be COLLECTED, DEVELOPED or DONATED";
                                            r.SetField<int>("table_id", -1); //not valid accession_id
                                        }
                                    }
                                    else // exists
                                    {
                                        r.Field<DataGridViewCell>("cell").Style.BackColor = Color.LightGoldenrodYellow;
                                    }
                                }
                            }

                            _sharedUtils.SaveWebServiceData(dsAccessionSource);
                            dsAccessionSource = _sharedUtils.GetWebServiceData(dataview_name_acc_source, ":accessionid=" + string.Join(",", accessionList), 0, 0);
                            /* Change accession_ids by acession_source_ids */
                            if (dsAccessionSource != null && dsAccessionSource.Tables[dataview_name_acc_source] != null)
                            {
                                DataTable dtAccessionSource = dsAccessionSource.Tables[dataview_name_acc_source];
                                foreach (DataRow r in accessionAccessionSourceMapRows)
                                {
                                    if (string.IsNullOrEmpty(r.Field<DataGridViewCell>("cell").ErrorText))
                                    {
                                        var rAccSource = dtAccessionSource.Select(search_index_column_acc_source + " = " + r.Field<int>("table_id")); //search accesion_source by accession_id
                                        if (rAccSource.Count() == 1)
                                        {
                                            r.SetField<int>("second_table_id", rAccSource[0].Field<int>("accession_source_id")); //now table_id = accession_source_id
                                            r.Field<DataGridViewCell>("cell").Style.BackColor = Color.LightGoldenrodYellow;
                                        }
                                        else
                                        {
                                            r.Field<DataGridViewCell>("cell").ErrorText = "There was an error saving Accession Source";
                                        }
                                    }
                                }
                                #region save_accession_source_map_reg

                                /****** accession_source_map ********/
                                string dataview_name = "acc_batch_wizard_get_accession_source_map";
                                string columnName = accessionAccessionSourceMapRows[0].Field<DataGridViewCell>("cell").OwningColumn.Name;
                                string search_index_column = "accession_source_id";

                                List<int> accessionSourceList = new List<int>();
                                foreach (DataRow r in accessionAccessionSourceMapRows)
                                {
                                    if (string.IsNullOrEmpty(r.Field<DataGridViewCell>("cell").ErrorText))
                                        accessionSourceList.Add(r.Field<int>("second_table_id"));
                                }

                                DataSet dsAccessionSourceMap = _sharedUtils.GetWebServiceData(dataview_name, ":accessionsourceid=" + string.Join(",", accessionSourceList), 0, 0);
                                if (dsAccessionSourceMap != null && dsAccessionSourceMap.Tables[dataview_name] != null)
                                {
                                    DataTable dtAccessionSourceMap = dsAccessionSourceMap.Tables[dataview_name];
                                    foreach (DataRow r in accessionAccessionSourceMapRows)
                                    {
                                        if (string.IsNullOrEmpty(r.Field<DataGridViewCell>("cell").ErrorText))
                                        {
                                            DataRow[] rupdate = dtAccessionSourceMap.Select(search_index_column + " = " + r.Field<int>("second_table_id"));
                                            if (rupdate.Count() == 0)
                                            {
                                                DataRow dr = dsAccessionSourceMap.Tables[dataview_name].NewRow();
                                                dr[search_index_column] = r.Field<int>("second_table_id"); // accession_source_id
                                                dr[r.Field<string>("column_name")] = r.Field<int>("int_value"); // cooperator_id
                                                dsAccessionSourceMap.Tables[dataview_name].Rows.Add(dr);

                                                (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                            }
                                            else
                                            {
                                                if (rupdate[0].Field<int>(r.Field<string>("column_name")) != r.Field<int>("int_value"))
                                                {
                                                    rupdate[0].SetField<int>(r.Field<string>("column_name"), r.Field<int>("int_value"));
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                                else
                                                {
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.White;
                                                }
                                            }
                                        }
                                    }
                                }

                                DataSet saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionSourceMap);
                                if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                                {
                                    foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                                    {
                                        var asm = accessionAccessionSourceMapRows.Where(r => r.Field<int>("second_table_id") == int.Parse(dr["accession_source_id"].ToString())).First();
                                        DataGridViewRow row = asm.Field<DataGridViewCell>("cell").OwningRow;
                                        if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                        {
                                            row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        }
                                        else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                        {
                                            //row.Cells[columnName].Style.BackColor = Color.Green;
                                            row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                            row.Cells[columnName].ErrorText = string.Empty;
                                        }
                                        if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                        {
                                            row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        }
                                        else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                        {
                                            row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                            row.Cells[columnName].ErrorText = string.Empty;
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                    #endregion

                    //save accession_source
                    #region save_accession_source
                    if (true)
                    {
                        var accessionPedigreeRows = dtTableUpdate.Select("table_name = 'accession_source'");
                        string dataview_name = "acc_batch_wizard_get_accession_source_update";
                        if (accessionPedigreeRows.Count() > 0)
                        {
                            string[] columnNames = new string[] { PassportCode.ORIGCTY, PassportCode.DECLATITUDE, PassportCode.DECLONGITUDE, PassportCode.ELEVATION, PassportCode.COLLDATE, "13. Country of origin", "Adm1", "Adm2", "Adm3", PassportCode.ACCESSION_SOURCE_FORMATTED_LOCALITY, PassportCode.ACCESSION_SOURCE_VERBATIM_LOCALITY };
                            string search_index_column = "accession_id";

                            List<int> accessionList = new List<int>();
                            foreach (DataRow r in accessionPedigreeRows) accessionList.Add(r.Field<int>("table_id"));

                            DataSet dsAccessionPedigree = _sharedUtils.GetWebServiceData(dataview_name, ":accessionid=" + string.Join(",", accessionList), 0, 0);
                            if (dsAccessionPedigree != null && dsAccessionPedigree.Tables[dataview_name] != null)
                            {
                                DataTable dtAccessionPedigree = dsAccessionPedigree.Tables[dataview_name];
                                foreach (DataRow r in accessionPedigreeRows)
                                {
                                    DataRow[] rupdate = dtAccessionPedigree.Select(search_index_column + " = " + r.Field<int>("table_id"));
                                    if (rupdate.Count() == 0) //insert
                                    {
                                        DataGridViewRow dgvRow = r.Field<DataGridViewCell>("cell").OwningRow;
                                        if (dgvRow.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(dgvRow.Cells["source_type_code"].Value.ToString()))
                                        {
                                            DataRow dr = dsAccessionPedigree.Tables[dataview_name].NewRow();
                                            dr[search_index_column] = r.Field<int>("table_id"); //accession_id
                                            dr["source_type_code"] = r.Field<string>("");
                                            dr["is_origin"] = "Y";
                                            dr["is_web_visible"] = "Y";

                                            switch (r.Field<string>("column_type"))
                                            {
                                                case "int":
                                                    dr[r.Field<string>("column_name")] = r.Field<int>("int_value");
                                                    break;
                                                case "string":
                                                    dr[r.Field<string>("column_name")] = r.Field<string>("string_value");
                                                    break;
                                                case "decimal":
                                                    dr[r.Field<string>("column_name")] = r.Field<decimal>("decimal_value");
                                                    break;
                                                case "datetime":
                                                    dr[r.Field<string>("column_name")] = r.Field<DateTime>("datetime_value");
                                                    break;
                                            }
                                            dsAccessionPedigree.Tables[dataview_name].Rows.Add(dr);

                                            if (r.Field<string>("column_name").Equals("geography_id")) //GroupColumnMan.ContainsKey(table_name + column_name)
                                            {
                                                DataGridViewRow row = r.Field<DataGridViewCell>("cell").OwningRow;
                                                foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                    row.Cells[column_name].Style.BackColor = Color.Orange;
                                            }
                                            else
                                                r.Field<DataGridViewCell>("cell").Style.BackColor = Color.Orange;
                                        }
                                        else
                                            r.Field<DataGridViewCell>("cell").ErrorText = "source_type_code must be COLLECTED, DEVELOPED or DONATED";
                                    }
                                    else //update
                                    {
                                        switch (r.Field<string>("column_type"))
                                        {
                                            case "int":
                                                if (rupdate[0].ItemArray[dtAccessionPedigree.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                                    || rupdate[0].Field<int>(r.Field<string>("column_name")) != r.Field<int>("int_value"))
                                                {
                                                    rupdate[0].SetField<int>(r.Field<string>("column_name"), r.Field<int>("int_value"));

                                                    if (r.Field<string>("column_name").Equals("geography_id")) //GroupColumnMan.ContainsKey(table_name + column_name)
                                                    {
                                                        DataGridViewRow row = r.Field<DataGridViewCell>("cell").OwningRow;
                                                        foreach (var column_name in GroupColumnMan[PassportCode.ACCESSION_SOURCE_GEOGRAPHY].RequiredColumns)
                                                            row.Cells[column_name].Style.BackColor = Color.Orange;
                                                    }
                                                    else
                                                        r.Field<DataGridViewCell>("cell").Style.BackColor = Color.Orange;
                                                }
                                                break;
                                            case "string":
                                                if (string.IsNullOrEmpty(rupdate[0].Field<string>(r.Field<string>("column_name")))
                                                    || !rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                                {
                                                    rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                                break;
                                            case "decimal":
                                                if (rupdate[0].ItemArray[dtAccessionPedigree.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                                    || rupdate[0].Field<decimal>(r.Field<string>("column_name")) != r.Field<decimal>("decimal_value"))
                                                {
                                                    rupdate[0].SetField<decimal>(r.Field<string>("column_name"), r.Field<decimal>("decimal_value"));
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                                break;
                                            case "datetime":
                                                if (rupdate[0].ItemArray[dtAccessionPedigree.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                                    || rupdate[0].Field<DateTime>(r.Field<string>("column_name")) != r.Field<DateTime>("datetime_value"))
                                                {
                                                    rupdate[0].SetField<DateTime>(r.Field<string>("column_name"), r.Field<DateTime>("datetime_value"));
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                                break;
                                        }
                                    }
                                }
                            }

                            DataSet saveErrors = new DataSet();
                            saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionPedigree);
                            if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                            {
                                foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();
                                    if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName))
                                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                    }
                                    else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor == Color.Orange)
                                            {
                                                //row.Cells[columnName].Style.BackColor = Color.Green;
                                                row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                                row.Cells[columnName].ErrorText = string.Empty;
                                            }
                                    }
                                    if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName))
                                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                    }
                                    else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor == Color.Orange)
                                            {
                                                row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                                row.Cells[columnName].ErrorText = string.Empty;
                                            }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    

                }//end_if(CIPNumbers.Count > 0)

                watch.Stop();

                _sharedUtils.LookupTablesUpdateTable("accession_lookup", false);
                _sharedUtils.LookupTablesUpdateTable("inventory_lookup", false);

                this.Invoke((MethodInvoker)delegate
                {
                    ux_progressBarWorking.Visible = false;
                    ux_buttonSaveData.Enabled = true;
                    ux_labelMessage.Text = "Elapsed time : " + watch.ElapsedMilliseconds + " ms";
                });
            }
            catch (Exception e)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    ux_progressBarWorking.Visible = false;
                    ux_buttonSaveData.Enabled = true;
                    ux_labelMessage.Text = string.Empty;
                });
                MessageBox.Show(e.Message);
            }


        }

        private void showHiddenColumnsMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (_main2 != null)
                {
                    if (ux_datagridviewSheet.Columns.Contains("accession_id")) ux_datagridviewSheet.Columns["accession_id"].Visible = true;
                    if (ux_datagridviewSheet.Columns.Contains("inventory_id")) ux_datagridviewSheet.Columns["inventory_id"].Visible = true;
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message, this.Text);
            }
        }

        private void showDBValuetoolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (ux_datagridviewSheet.SelectedCells.Count > 0)
                {
                    var cell = ux_datagridviewSheet.SelectedCells[0];
                    var row = cell.OwningRow;
                    var col = cell.OwningColumn;

                    if(row.Cells["inventory_id"].Value != null)
                    {
                        int rowInventoryId = int.Parse(row.Cells["inventory_id"].Value.ToString());
                        int rowAccessionId = int.Parse(row.Cells["accession_id"].Value.ToString());
                        if (col.Tag == null)
                            return;
                        ColumnTag colTag = (ColumnTag)col.Tag;

                        if (colTag.Category.Equals("CROP_TRAIT"))
                        {
                            #region CropTrait
                            int columnCropTraitId = int.Parse(col.Name);
                            DataTable dtObservationUpdate = null;
                            if (_dsObservationUpdate != null && _dsObservationUpdate.Tables.Contains("accession_batch_wizard_get_crop_trait_observation"))
                            {
                                dtObservationUpdate = _dsObservationUpdate.Tables["accession_batch_wizard_get_crop_trait_observation"];

                                var dbValue = dtObservationUpdate.AsEnumerable().FirstOrDefault(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<int>("crop_trait_id") == columnCropTraitId);
                                if (dbValue != null)
                                {
                                    int modifiedById = dbValue.IsNull("modified_by") ? dbValue.Field<int>("created_by") : dbValue.Field<int>("modified_by");
                                    DateTime modifiedDate = dbValue.IsNull("modified_date") ? dbValue.Field<DateTime>("created_date") : dbValue.Field<DateTime>("modified_date");

                                    DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                    string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                    var dtCropTrait = _sharedUtils.GetLocalData("select value_member, type from acc_batch_wizard_crop_trait_lookup where value_member = " + columnCropTraitId, "");
                                    switch (dtCropTrait.Rows[0].Field<string>("type"))
                                    {
                                        case "CODED":
                                            ux_labelCellInfo.Text = string.Format("Current \"{1}\" value for accession \"{2}\" \n\ncode = {0}"
                                            + "\n\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy
                                            , dbValue["code"], col.HeaderText, row.Cells[0].Value);
                                            break;
                                        case "NUMERIC":
                                            ux_labelCellInfo.Text = string.Format("Current \"{1}\" value for accession \"{2}\" \n\nnumeric_value = {0}"
                                            + "\n\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy
                                            , dbValue["numeric_value"], col.HeaderText, row.Cells[0].Value);
                                            break;
                                        default: //"CHAR","UPPER","LOWER"
                                            ux_labelCellInfo.Text = string.Format("Current \"{1}\" value for accession \"{2}\" \n\nstring_value = {0}"
                                            + "\n\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy
                                            , dbValue["string_value"], col.HeaderText, row.Cells[0].Value);
                                            break;
                                    }
                                }
                                else
                                {
                                    ux_labelCellInfo.Text = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                }
                                GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(ux_labelCellInfo.Text, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                mb.ShowDialog();
                            }
                            #endregion
                        }
                        else if (colTag.Category.Equals("PASSPORT"))
                        {
                            switch (col.Name)
                            {
                                #region accession_inv_name
                                case PassportCode.PUID: //0. Persistent unique identifier
                                    if(!_isCustomModificationNeeded)
                                    {
                                        if (_dtAcccession != null)
                                        {
                                            string messsage = string.Empty;
                                            var drValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                            if (!drValue.IsNull("doi"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                messsage += "\n\nvalue : " + drValue.Field<string>("doi");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                            GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                            mb.ShowDialog();
                                        }
                                    }
                                    else
                                    {
                                        ShowValueAccessionNameCell(rowInventoryId, "DOI", col, row);
                                    }
                                    break;
                                case PassportCode.COLLNUMB: //3. Collecting number
                                    ShowValueAccessionNameCell(rowInventoryId, "COLLECTOR", col, row);
                                    break;
                                case PassportCode.COLLMISSID: //4.2 Collecting mission identifier 
                                    ShowValueAccessionNameCell(rowInventoryId, "EXPLOREID", col, row);
                                    break;
                                case PassportCode.ACCENAME: //11. Accession name
                                    ShowValueAccessionNameCell(rowInventoryId, "CULTIVAR", col, row);
                                    break;
                                case PassportCode.DONORNUMB: //23. Donor accession number 
                                    ShowValueAccessionNameCell(rowInventoryId, "DONOR", col, row);
                                    break;
                                case PassportCode.OTHERNUMB: //24. Other identifiers associated with the accession
                                    ShowValueAccessionNameCell(rowInventoryId, "OTHER", col, row);
                                    break;
                                case PassportCode.BREEDER_CODE: //03.1.2 Breeder code
                                    ShowValueAccessionNameCell(rowInventoryId, "BREEDER", col, row);
                                    break;
                                case PassportCode.BIOSAFETY_CODE: //03.1.3 Biosafety code
                                    ShowValueAccessionNameCell(rowInventoryId, "BIOSAFETY", col, row);
                                    break;
                                #endregion

                                #region accession_source_map
                                case PassportCode.COLLCODE:
                                    ShowValueInsitutionCodeCell(rowAccessionId, "COLLECTED", col, row);
                                    break;
                                case PassportCode.BREDCODE:
                                    ShowValueInsitutionCodeCell(rowAccessionId, "DEVELOPED", col, row);
                                    break;
                                case PassportCode.DONORCODE:
                                    ShowValueInsitutionCodeCell(rowAccessionId, "DONATED", col, row);
                                    break;
                                #endregion

                                #region accession
                                case PassportCode.ACQDATE:
                                    if (_dtAcccession != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (!drValue.IsNull("initial_received_date"))
                                        {
                                            messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);
                                            
                                            int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                            DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                            DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                            string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                            string strDateTime = drValue.Field<DateTime>("initial_received_date").ToString("yyyyMMdd");
                                            if (!drValue.IsNull("initial_received_date_code"))
                                            {
                                                switch (drValue.Field<string>("initial_received_date_code"))
                                                {
                                                    case "yyyy":
                                                        strDateTime = strDateTime.Substring(0, 4) + "0000";
                                                        break;
                                                    case "MM/yyyy":
                                                        strDateTime = strDateTime.Substring(0, 6) + "00";
                                                        break;
                                                    case "MM/dd/yyyy":
                                                        break;
                                                }
                                            }
                                            
                                            messsage += "\n\nvalue : " + strDateTime;
                                            messsage += "\ndate format : " + (!drValue.IsNull("initial_received_date_code") ? drValue.Field<string>("initial_received_date_code") : "empty");
                                            messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }
                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    break;
                                case PassportCode.SAMPSTAT:
                                    if (_dtAcccession != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (!drValue.IsNull("improvement_status_code"))
                                        {
                                            messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                            int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                            DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                            DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                            string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                            messsage += "\n\nvalue : " + drValue.Field<string>("improvement_status_code");
                                            messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }
                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    break;
                                case PassportCode.REMARKS:
                                    if (_dtAcccession != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (!drValue.IsNull("note"))
                                        {
                                            messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                            int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                            DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                            DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                            string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                            messsage += "\n\nvalue : " + drValue.Field<string>("note");
                                            messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }
                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    break;
                                case PassportCode.ACCESSION_TAXON:
                                    if (_dtAcccession != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        
                                        messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                        int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                        DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                        DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                        string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                        DataTable dtTaxonomySpecies = _sharedUtils.GetLocalData("select display_member from taxonomy_species_lookup where value_member = " + drValue.Field<int>("taxonomy_species_id"), "");
                                        string TaxonName = dtTaxonomySpecies.Rows[0].Field<string>("display_member");

                                        messsage += "\n\nvalue : " + TaxonName;
                                        messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                        
                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    break;
                                case PassportCode.ACCESSION_STATUS:
                                    if (_dtAcccession != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);

                                        messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                        int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                        DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                        DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                        string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");
                                        
                                        messsage += "\n\nvalue : " + drValue.Field<string>("status_code");
                                        messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    break;
                                #endregion

                                #region accession_ipr
                                case PassportCode.MLSSTAT: //27. MLS status of the accession
                                    if (_dtAccessionIpr != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionIpr.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if(drValue != null)
                                        {
                                            if (!drValue.IsNull("ipr_number"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                messsage += "\n\nvalue : " + drValue.Field<string>("ipr_number");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }
                                        
                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    break;
                                #endregion

                                #region accession_pedigree
                                case PassportCode.ANCEST:
                                    break;
                                case PassportCode.PARENT_FEMALE: //20.1 Parent Female
                                    #region ParentFemale
                                    if (_dtAccessionPedigree != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionPedigree.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("female_external_accession"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                messsage += "\n\nvalue : " + drValue.Field<string>("female_external_accession");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                case PassportCode.PARENT_MALE: //20.2 Parent Male
                                    #region ParentMale
                                    if (_dtAccessionPedigree != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionPedigree.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("male_external_accession"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                messsage += "\n\nvalue : " + drValue.Field<string>("male_external_accession");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                case PassportCode.PARENT_FEMALE_ACCENUMB: //20.3 Parent Female (Accenumb)
                                    #region ParentFemaleAccenumb
                                    if (_dtAccessionPedigree != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionPedigree.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("female_accession_id"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                DataTable dtAccession = _sharedUtils.GetLocalData("select display_member from accession_lookup where value_member = " + drValue.Field<string>("female_accession_id"), "");
                                                
                                                messsage += "\n\nvalue : " + dtAccession.Rows[0].Field<string>("display_member"); ;
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                case PassportCode.PARENT_MALE_ACCENUM: //20.4 Parent Male (Accenumb)
                                    #region ParentMaleAccenumb
                                    if (_dtAccessionPedigree != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionPedigree.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("male_accession_id"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                DataTable dtAccession = _sharedUtils.GetLocalData("select display_member from accession_lookup where value_member = " + drValue.Field<string>("male_accession_id"), "");

                                                messsage += "\n\nvalue : " + dtAccession.Rows[0].Field<string>("display_member"); ;
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                #endregion

                                #region accession_source
                                case PassportCode.DECLATITUDE: //15.1 Latitude of collecting site
                                    #region DECLATITUDE
                                    if (_dtAccessionSource != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("latitude"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                messsage += "\n\nvalue : " + drValue.Field<decimal>("latitude");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                case PassportCode.DECLONGITUDE: //15.3 Longitude of collecting site
                                    #region DECLONGITUDE
                                    if (_dtAccessionSource != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("longitude"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                messsage += "\n\nvalue : " + drValue.Field<decimal>("longitude");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                case PassportCode.ELEVATION: //16. Elevation of collecting site
                                    #region ELEVATION
                                    if (_dtAccessionSource != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("elevation_meters"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                messsage += "\n\nvalue : " + drValue.Field<int>("elevation_meters");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                case PassportCode.COLLDATE: //17. Collecting date of sample
                                    #region COLLDATE
                                    if (_dtAccessionSource != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("source_date"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                string strDateTime = drValue.Field<DateTime>("source_date").ToString("yyyyMMdd");
                                                if (!drValue.IsNull("source_date_code"))
                                                {
                                                    switch (drValue.Field<string>("source_date_code")) {
                                                        case "yyyy":
                                                            strDateTime = strDateTime.Substring(0, 4) + "0000";
                                                            break;
                                                        case "MM/yyyy":
                                                            strDateTime = strDateTime.Substring(0, 6) + "00";
                                                            break;
                                                        case "MM/dd/yyyy":
                                                            break;
                                                    }
                                                }

                                                messsage += "\n\nvalue : " + strDateTime;
                                                messsage += "\ndate format : " + (!drValue.IsNull("source_date_code") ? drValue.Field<string>("source_date_code") : "empty");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                case PassportCode.ACCESSION_SOURCE_FORMATTED_LOCALITY: //14. Location of collecting site
                                    #region COLLSITE
                                    if (_dtAccessionSource != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("formatted_locality"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                messsage += "\n\nvalue : " + drValue.Field<string>("formatted_locality");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                case PassportCode.ACCESSION_SOURCE_VERBATIM_LOCALITY: //collector_verbatim_locality
                                    #region collector_verbatim_locality
                                    if (_dtAccessionSource != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("collector_verbatim_locality"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                messsage += "\n\nvalue : " + drValue.Field<string>("collector_verbatim_locality");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                case PassportCode.COORDUNCERT: //
                                    #region COORDUNCERT
                                    if (_dtAccessionSource != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null && !drValue.IsNull("uncertainty"))
                                        {
                                            messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                            int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                            DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                            DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                            string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                            messsage += "\n\nvalue : " + drValue.Field<int>("uncertainty");
                                            messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                case PassportCode.COORDDATUM: //
                                    #region COORDDATUM
                                    if (_dtAccessionSource != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("georeference_datum"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                messsage += "\n\nvalue : " + drValue.Field<string>("georeference_datum");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                case PassportCode.GEOREFMETH: //
                                    #region GEOREFMETH
                                    if (_dtAccessionSource != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("georeference_protocol_code"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                messsage += "\n\nvalue : " + drValue.Field<string>("georeference_protocol_code");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                case PassportCode.ACCESSION_SOURCE_NOTE: // Accession source note
                                    #region ACCESSION_SOURCE_NOTE
                                    if (_dtAccessionSource != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("note"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                messsage += "\n\nvalue : " + drValue.Field<string>("note");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                                #endregion

                                default:
                                    //Check if custom descriptor is accession_inv_name
                                    var drCustomDescriptor = dtPassportCriteria.AsEnumerable().FirstOrDefault(x => x.Field<string>("value_member").Equals(col.Name) && x.Field<string>("table_name").Equals("accession_inv_name"));
                                    if (drCustomDescriptor != null)
                                    {
                                        ShowValueAccessionNameCell(rowInventoryId, drCustomDescriptor.Field<string>("category_code"), col, row);
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            switch (col.Name)
                            {
                                case "13. Country of origin":
                                    #region ORIGCTY
                                    if (_dtAccessionSource != null)
                                    {
                                        string messsage = string.Empty;
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("geography_id"))
                                            {
                                                messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);

                                                int modifiedById = drValue.IsNull("modified_by") ? drValue.Field<int>("created_by") : drValue.Field<int>("modified_by");
                                                DateTime modifiedDate = drValue.IsNull("modified_date") ? drValue.Field<DateTime>("created_date") : drValue.Field<DateTime>("modified_date");

                                                DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                                                string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                                                DataTable dtGeography = _sharedUtils.GetLocalData("select display_member from geography_lookup where value_member = " + drValue.Field<int>("geography_id"), "");

                                                messsage += "\n\nvalue : " + dtGeography.Rows[0].Field<string>("display_member");
                                                messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                                            }
                                            else
                                            {
                                                messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                            }
                                        }
                                        else
                                        {
                                            messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                                        }

                                        GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                        mb.ShowDialog();
                                    }
                                    #endregion
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                GGMessageBox mb = new GGMessageBox(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_datagridviewSheet_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                //ux_datagridviewSheet.Sort(new RowComparer(SortOrder.Ascending));

                /*DataGridViewColumn newColumn = ux_datagridviewSheet.Columns[e.ColumnIndex];
                DataGridViewColumn oldColumn = ux_datagridviewSheet.SortedColumn;
                ListSortDirection direction;

                // If oldColumn is null, then the DataGridView is not sorted.
                if (oldColumn != null)
                {
                    // Sort the same column again, reversing the SortOrder.
                    if (oldColumn == newColumn &&
                        ux_datagridviewSheet.SortOrder == SortOrder.Ascending)
                    {
                        direction = ListSortDirection.Descending;
                    }
                    else
                    {
                        // Sort a new column and remove the old SortGlyph.
                        direction = ListSortDirection.Ascending;
                        oldColumn.HeaderCell.SortGlyphDirection = SortOrder.None;
                    }
                }
                else
                {
                    direction = ListSortDirection.Ascending;
                }

                // Sort the selected column.
                ux_datagridviewSheet.Sort(newColumn, direction);
                newColumn.HeaderCell.SortGlyphDirection =
                    direction == ListSortDirection.Ascending ?
                    SortOrder.Ascending : SortOrder.Descending;*/
            }
            catch (Exception ex)
            {
                GGMessageBox mb = new GGMessageBox(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }
        
        private void ux_buttonExportExcel_Click(object sender, EventArgs e)
        {
            Excel.Application oXL;
            Excel._Workbook oWB;
            Excel._Worksheet oSheet;
            Excel.Range oRng;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                //Copy template to temp folder

                var fileName = Path.GetTempPath() + Guid.NewGuid().ToString() + ".xlsx";

                //Start Excel and get Application object.
                oXL = new Excel.Application();
                oXL.Visible = true;

                //Get a new workbook.
                oWB = (Excel._Workbook)(oXL.Workbooks.Add(Missing.Value));
                oSheet = (Excel._Worksheet)oWB.ActiveSheet;

                /*//Add table headers going cell by cell.
                oSheet.Cells[1, 1] = "First Name";
                oSheet.Cells[1, 2] = "Last Name";
                oSheet.Cells[1, 3] = "Full Name";
                oSheet.Cells[1, 4] = "Salary";

                //Format A1:D1 as bold, vertical alignment = center.
                oSheet.get_Range("A1", "D1").Font.Bold = true;
                oSheet.get_Range("A1", "D1").VerticalAlignment =
                Excel.XlVAlign.xlVAlignCenter;

                //AutoFit columns A:D.
                oRng = oSheet.get_Range("A1", "D1");
                oRng.EntireColumn.AutoFit();*/

                
                Excel.Range header = oSheet.Rows[1];
                header.RowHeight = 30;

                StringBuilder builder = new StringBuilder();

                for (int c = 0; c < ux_datagridviewSheet.Columns.Count; c++)
                {
                    builder.Append(ux_datagridviewSheet.Columns[c].HeaderText).Append("\t");
                }
                builder.Append(Environment.NewLine);

                for (int r = 0; r < ux_datagridviewSheet.RowCount; r++)
                {
                    for (int c = 0; c < ux_datagridviewSheet.Columns.Count; c++)
                    {
                        string cellValue = ux_datagridviewSheet[c, r].Value == null ? string.Empty : ux_datagridviewSheet[c, r].FormattedValue.ToString();
                        builder.Append("\"=\"\"").Append(cellValue).Append("\"\"\"\t");
                    }
                    builder.Append(Environment.NewLine);
                }
                Clipboard.SetText(builder.ToString());
                //MessageBox.Show(builder.ToString());

                Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[1, 1];
                CR.Select();
                oSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
                
                //AutoFit columns
                oRng = oSheet.get_Range(oSheet.Cells[1, 1], oSheet.Cells[1, ux_datagridviewSheet.Columns.Count]);
                oRng.EntireColumn.AutoFit();
                oRng.WrapText = true;

                oSheet.get_Range(oSheet.Cells[1, 1], oSheet.Cells[1, ux_datagridviewSheet.Columns.Count]).Font.Bold = true;

                //Make sure Excel is visible and give the user control
                //of Microsoft Excel's lifetime.
                oXL.Visible = true;
                oXL.UserControl = true;

                //Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excelApp.Workbooks.Open(workbookPath, 0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);

                /*
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Open(fileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                xlApp.Visible = true;

                Excel.Range title = xlWorkSheet.Evaluate("Title");
                title.Value = "Evaluacion de 3 días";
                (xlWorkSheet.Evaluate("Supervisor") as Excel.Range).Value = "Brenda Zea";
                (xlWorkSheet.Evaluate("RegisteredBy") as Excel.Range).Value = "---";
                (xlWorkSheet.Evaluate("Date") as Excel.Range).Value = DateTime.Now;
                (xlWorkSheet.Evaluate("Crop") as Excel.Range).Value = "Camote";

                string query = "";
                query = string.Format(query, "'" + string.Join("','", _params.LabcodeList) + "'");

                DataTable dtItems = _serviceRequestManager.GetDataTable(query);
                (xlWorkSheet.Evaluate("Count") as Excel.Range).Value = dtItems.Rows.Count;

                for (int i = 0; i < dtItems.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < dtItems.Columns.Count - 1; j++)
                    {
                        xlWorkSheet.Cells[14 + i, j + 1] = dtItems.Rows[i].ItemArray[j];
                    }
                }*/

                //Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
                //CR.Select();
                //xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);

                //xlWorkBook.SaveAs(fileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                /*xlWorkBook.Save();*/

                //xlWorkBook.Close(true, misValue, misValue);
                //xlexcel.Quit();

                /*releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);*/

                //System.Diagnostics.Process.Start(fileName);

                this.Cursor = Cursors.Arrow;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Arrow;

                String errorMessage;
                errorMessage = "Error: " + Environment.NewLine;
                errorMessage = String.Concat(errorMessage, ex.Message);
                errorMessage = String.Concat(errorMessage, " Line: ");
                errorMessage = String.Concat(errorMessage, ex.Source);

                GGMessageBox mb = new GGMessageBox(ex.Message, errorMessage, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_datagridviewSheet_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (ux_checkBoxShowOnlyRowsWithError.Checked)
            {
                int visibleRowCount = ux_datagridviewSheet.DisplayedRowCount(true);
                ux_labelRows.Text = string.Format(ux_labelRows.Tag.ToString(), visibleRowCount, ux_datagridviewSheet.Rows.Count);
            }
            else
                ux_labelRows.Text = string.Format(ux_labelRows.Tag.ToString(), ux_datagridviewSheet.Rows.Count, ux_datagridviewSheet.Rows.Count);
        }

        private void ux_datagridviewSheet_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (ux_checkBoxShowOnlyRowsWithError.Checked)
            {
                int visibleRowCount = ux_datagridviewSheet.DisplayedRowCount(true);
                ux_labelRows.Text = string.Format(ux_labelRows.Tag.ToString(), visibleRowCount, ux_datagridviewSheet.Rows.Count);
            }
            else
                ux_labelRows.Text = string.Format(ux_labelRows.Tag.ToString(), ux_datagridviewSheet.Rows.Count, ux_datagridviewSheet.Rows.Count);
        }

        private void ClearCounters()
        {
            validationErrorCount = 0;
            validationOKWithChangesCount = 0;
            validationOKWithoutChangesCount = 0;

            ux_labelValidationErrorCount.Text = "--";
            ux_labelValidationOkWithoutChangesCount.Text = "--";
            ux_labelValidationOkWithChangesCount.Text = "--";

            savingErrorCount = 0;
            savingOKCount = 0;
            savingUnmodifiedCount = 0;

            ux_labelSavingErrorCount.Text = "--";
            ux_labelSavingOkCount.Text = "--";
            ux_labelSavingUnmodifiedCount.Text = "--";
        }

        #region ValidationMethods
        private void ValidateInstitutionCodeCell(DataGridViewCell cell, int rowAccessionId, string sourceTypeCode)
        {
            string cellValue = cell.Value.ToString();
            DataRow drValue;

            List<int> cellCooperatorIds = new List<int>();
            var faoNumbers = cellValue.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            string errorText = string.Empty;
            foreach (var faoNumber in faoNumbers)
            {
                drValue = _dtCooperator.AsEnumerable().FirstOrDefault(x => x.Field<string>("secondary_organization_abbrev").Equals(faoNumber));
                if (drValue == null)
                {
                    errorText += faoNumber + " is not found" + "\n";
                }
                else
                    cellCooperatorIds.Add(drValue.Field<int>("cooperator_id"));
            }
            if (!string.IsNullOrEmpty(errorText))
            {
                cell.ErrorText = errorText + "\nIf organization is registered, update FAO WIEWS code in column \"secondary_organization_abbrev\" in \"get_cooperator\" dataview.\n"
                    + "If organization is not registered, use Cooperator Wizard to register it first";
                cell.Style.BackColor = StyleColors.ValidationError;
                validationErrorCount++;
                return;
            }
            
            //Check modification
            var accSourceMapRows = _dtAccessionSourceMapExt.AsEnumerable().Where(x => x.Field<int>("accession_id") == rowAccessionId && x.Field<string>("source_type_code").Equals(sourceTypeCode));
            if (accSourceMapRows != null && accSourceMapRows.Count() > 0)
            {
                bool allFound = true;
                if (cellCooperatorIds.Count != accSourceMapRows.Count())
                {
                    allFound = false;
                }
                else
                {
                    foreach (var item in cellCooperatorIds)
                    {
                        if (!accSourceMapRows.Any(x => x.Field<int>("cooperator_id") == item))
                        {
                            allFound = false;
                            break;
                        }
                    }
                }
                if (allFound) // unmodified
                {
                    cell.ErrorText = string.Empty;
                    cell.Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                    validationOKWithoutChangesCount++;
                }
                else //modified
                {
                    cell.ErrorText = string.Empty;
                    cell.Style.BackColor = StyleColors.ValidationOKWithChanges;
                    validationOKWithChangesCount++;
                }
            }
            else //dbValue is empty
            {
                if (string.IsNullOrEmpty(cellValue))
                {
                    cell.ErrorText = string.Empty;
                    cell.Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                    validationOKWithoutChangesCount++;
                }
                else
                {
                    cell.ErrorText = string.Empty;
                    cell.Style.BackColor = StyleColors.ValidationOKWithChanges;
                    validationOKWithChangesCount++;
                }
            }
        }

        private void loadCurrentValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                foreach (DataGridViewCell cell in ux_datagridviewSheet.SelectedCells)
                {
                    var row = cell.OwningRow;
                    var col = cell.OwningColumn;

                    if (row.Cells["accession_id"].Value != null)
                    {
                        int rowInventoryId = int.Parse(row.Cells["inventory_id"].Value.ToString());
                        int rowAccessionId = int.Parse(row.Cells["accession_id"].Value.ToString());
                        if (col.Tag == null)
                            return;
                        ColumnTag colTag = (ColumnTag)col.Tag;

                        if (colTag.Category.Equals("CROP_TRAIT"))
                        {
                            #region CropTrait
                            int columnCropTraitId = int.Parse(col.Name);
                            DataTable dtObservationUpdate = null;
                            if (_dsObservationUpdate != null && _dsObservationUpdate.Tables.Contains("accession_batch_wizard_get_crop_trait_observation"))
                            {
                                dtObservationUpdate = _dsObservationUpdate.Tables["accession_batch_wizard_get_crop_trait_observation"];

                                var dbValue = dtObservationUpdate.AsEnumerable().FirstOrDefault(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<int>("crop_trait_id") == columnCropTraitId);
                                if (dbValue != null)
                                {
                                    var dtCropTrait = _sharedUtils.GetLocalData("select value_member, type, numeric_format from acc_batch_wizard_crop_trait_lookup where value_member = " + columnCropTraitId, "");
                                    switch (dtCropTrait.Rows[0].Field<string>("type"))
                                    {
                                        case "CODED":
                                            cell.Value = dbValue["code"];
                                            break;
                                        case "NUMERIC":
                                            if (!dbValue.IsNull("numeric_value"))
                                            {
                                                if (!dtCropTrait.Rows[0].IsNull("numeric_format")) {
                                                    cell.Value = dbValue.Field<decimal>("numeric_value").ToString(dtCropTrait.Rows[0].Field<string>("numeric_format"));
                                                }
                                                else
                                                    cell.Value = dbValue["numeric_value"].ToString();
                                            }
                                            else
                                                cell.Value = string.Empty;
                                            break;
                                        default: //"CHAR","UPPER","LOWER"
                                            cell.Value = dbValue["string_value"];
                                            break;
                                    }
                                }
                                else
                                {
                                    cell.Value = string.Empty;
                                }
                            }
                            #endregion
                        }
                        else if (colTag.Category.Equals("PASSPORT"))
                        {
                            switch (col.Name)
                            {
                                #region accession_inv_name
                                case PassportCode.PUID: //0. Persistent unique identifier
                                    if (!_isCustomModificationNeeded)
                                    {
                                        if (_dtAcccession != null)
                                        {
                                            var drValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                            if (!drValue.IsNull("doi"))
                                            {
                                                cell.Value = drValue.Field<string>("doi");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (_dtAccessionInvName != null)
                                        {
                                            var drValues = _dtAccessionInvName.AsEnumerable().Where(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<string>("category_code").Equals("DOI"));
                                            if (drValues.Count() > 0)
                                            {
                                                var names = drValues.AsEnumerable().Select(x => x.Field<string>("plant_name")).ToArray();
                                                cell.Value = string.Join(";", names);
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                    }
                                    break;
                                case PassportCode.COLLNUMB: //3. Collecting number
                                    if (_dtAccessionInvName != null)
                                    {
                                        var drValues = _dtAccessionInvName.AsEnumerable().Where(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<string>("category_code").Equals("COLLECTOR"));
                                        if (drValues.Count() > 0)
                                        {
                                            var names = drValues.AsEnumerable().Select(x => x.Field<string>("plant_name")).ToArray();
                                            cell.Value = string.Join(";", names);
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                case PassportCode.COLLMISSID: //4.2 Collecting mission identifier 
                                    if (_dtAccessionInvName != null)
                                    {
                                        var drValues = _dtAccessionInvName.AsEnumerable().Where(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<string>("category_code").Equals("EXPLOREID"));
                                        if (drValues.Count() > 0)
                                        {
                                            var names = drValues.AsEnumerable().Select(x => x.Field<string>("plant_name")).ToArray();
                                            cell.Value = string.Join(";", names);
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                case PassportCode.ACCENAME: //11. Accession name
                                    if (_dtAccessionInvName != null)
                                    {
                                        var drValues = _dtAccessionInvName.AsEnumerable().Where(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<string>("category_code").Equals("CULTIVAR"));
                                        if (drValues.Count() > 0)
                                        {
                                            var names = drValues.AsEnumerable().Select(x => x.Field<string>("plant_name")).ToArray();
                                            cell.Value = string.Join(";", names);
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                case PassportCode.DONORNUMB: //23. Donor accession number 
                                    if (_dtAccessionInvName != null)
                                    {
                                        var drValues = _dtAccessionInvName.AsEnumerable().Where(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<string>("category_code").Equals("DONOR"));
                                        if (drValues.Count() > 0)
                                        {
                                            var names = drValues.AsEnumerable().Select(x => x.Field<string>("plant_name")).ToArray();
                                            cell.Value = string.Join(";", names);
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                case PassportCode.OTHERNUMB: //24. Other identifiers associated with the accession
                                    if (_dtAccessionInvName != null)
                                    {
                                        var drValues = _dtAccessionInvName.AsEnumerable().Where(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<string>("category_code").Equals("OTHER"));
                                        if (drValues.Count() > 0)
                                        {
                                            var names = drValues.AsEnumerable().Select(x => x.Field<string>("plant_name")).ToArray();
                                            cell.Value = string.Join(";", names);
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                case PassportCode.BREEDER_CODE: //03.1.2 Breeder code
                                    if (_dtAccessionInvName != null)
                                    {
                                        var drValues = _dtAccessionInvName.AsEnumerable().Where(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<string>("category_code").Equals("BREEDER"));
                                        if (drValues.Count() > 0)
                                        {
                                            var names = drValues.AsEnumerable().Select(x => x.Field<string>("plant_name")).ToArray();
                                            cell.Value = string.Join(";", names);
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                case PassportCode.BIOSAFETY_CODE: //03.1.3 Biosafety code
                                    if (_dtAccessionInvName != null)
                                    {
                                        var drValues = _dtAccessionInvName.AsEnumerable().Where(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<string>("category_code").Equals("BIOSAFETY"));
                                        if (drValues.Count() > 0)
                                        {
                                            var names = drValues.AsEnumerable().Select(x => x.Field<string>("plant_name")).ToArray();
                                            cell.Value = string.Join(";", names);
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                #endregion

                                #region accession_source_map
                                case PassportCode.COLLCODE:
                                    if (_dtAccessionSourceMapExt != null)
                                    {
                                        var dbValues = _dtAccessionSourceMapExt.AsEnumerable().Where(x => x.RowState != DataRowState.Deleted && x.Field<int>("accession_id") == rowAccessionId
                                                                                                        && x.Field<string>("source_type_code").Equals("COLLECTED"));
                                        if (dbValues.Count() > 0)
                                        {
                                            List<string> faoNumbers = new List<string>();
                                            foreach (var dbValue in dbValues)
                                            {
                                                DataSet dsCooperator = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_organization", ":cooperatorid=" + dbValue.Field<int>("cooperator_id"), 0, 0);
                                                if (dsCooperator != null && dsCooperator.Tables.Contains("acc_batch_wizard_get_organization"))
                                                {
                                                    if (dsCooperator.Tables["acc_batch_wizard_get_organization"].Rows.Count > 0)
                                                    {
                                                        string faoNumber = dsCooperator.Tables["acc_batch_wizard_get_organization"].Rows[0].Field<string>("secondary_organization_abbrev");
                                                        faoNumbers.Add(faoNumber);
                                                    }
                                                    else
                                                    {
                                                        throw new WizardException("Error getting data from \"acc_batch_wizard_get_organization\" dataview.", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_organization");
                                                    }
                                                }
                                                else
                                                {
                                                    throw new WizardException("Error getting data from \"acc_batch_wizard_get_organization\" dataview.", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_organization");
                                                }
                                            }
                                            cell.Value = string.Join(";", faoNumbers);
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                case PassportCode.BREDCODE:
                                    if (_dtAccessionSourceMapExt != null)
                                    {
                                        var dbValues = _dtAccessionSourceMapExt.AsEnumerable().Where(x => x.RowState != DataRowState.Deleted && x.Field<int>("accession_id") == rowAccessionId
                                                                                                        && x.Field<string>("source_type_code").Equals("DEVELOPED"));
                                        if (dbValues.Count() > 0)
                                        {
                                            List<string> faoNumbers = new List<string>();
                                            foreach (var dbValue in dbValues)
                                            {
                                                DataSet dsCooperator = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_organization", ":cooperatorid=" + dbValue.Field<int>("cooperator_id"), 0, 0);
                                                if (dsCooperator != null && dsCooperator.Tables.Contains("acc_batch_wizard_get_organization"))
                                                {
                                                    if (dsCooperator.Tables["acc_batch_wizard_get_organization"].Rows.Count > 0)
                                                    {
                                                        string faoNumber = dsCooperator.Tables["acc_batch_wizard_get_organization"].Rows[0].Field<string>("secondary_organization_abbrev");
                                                        faoNumbers.Add(faoNumber);
                                                    }
                                                    else
                                                    {
                                                        throw new WizardException("Error getting data from \"acc_batch_wizard_get_organization\" dataview.", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_organization");
                                                    }
                                                }
                                                else
                                                {
                                                    throw new WizardException("Error getting data from \"acc_batch_wizard_get_organization\" dataview.", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_organization");
                                                }
                                            }
                                            cell.Value = string.Join(";", faoNumbers);
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                case PassportCode.DONORCODE:
                                    if (_dtAccessionSourceMapExt != null)
                                    {
                                        var dbValues = _dtAccessionSourceMapExt.AsEnumerable().Where(x => x.RowState != DataRowState.Deleted && x.Field<int>("accession_id") == rowAccessionId
                                                                                                        && x.Field<string>("source_type_code").Equals("DONATED"));
                                        if (dbValues.Count() > 0)
                                        {
                                            List<string> faoNumbers = new List<string>();
                                            foreach (var dbValue in dbValues)
                                            {
                                                DataSet dsCooperator = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_organization", ":cooperatorid="+  dbValue.Field<int>("cooperator_id"), 0, 0);
                                                if(dsCooperator != null && dsCooperator.Tables.Contains("acc_batch_wizard_get_organization"))
                                                {
                                                    if(dsCooperator.Tables["acc_batch_wizard_get_organization"].Rows.Count > 0)
                                                    {
                                                        string faoNumber = dsCooperator.Tables["acc_batch_wizard_get_organization"].Rows[0].Field<string>("secondary_organization_abbrev");
                                                        faoNumbers.Add(faoNumber);
                                                    }
                                                    else
                                                    {
                                                        throw new WizardException("Error getting data from \"acc_batch_wizard_get_organization\" dataview.", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_organization");
                                                    }
                                                }
                                                else
                                                {
                                                    throw new WizardException("Error getting data from \"acc_batch_wizard_get_organization\" dataview.", "ux_textboxErrorGettingDataview", "acc_batch_wizard_get_organization");
                                                }
                                            }
                                            cell.Value = string.Join(";", faoNumbers);
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                #endregion

                                #region accession
                                case PassportCode.ACQDATE:
                                    if (_dtAcccession != null)
                                    {
                                        var drValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (!drValue.IsNull("initial_received_date"))
                                        {
                                            string strDateTime = drValue.Field<DateTime>("initial_received_date").ToString("yyyyMMdd");
                                            if (!drValue.IsNull("initial_received_date_code"))
                                            {
                                                switch (drValue.Field<string>("initial_received_date_code"))
                                                {
                                                    case "yyyy":
                                                        strDateTime = strDateTime.Substring(0, 4) + "0000";
                                                        break;
                                                    case "MM/yyyy":
                                                        strDateTime = strDateTime.Substring(0, 6) + "00";
                                                        break;
                                                    case "MM/dd/yyyy":
                                                        break;
                                                }
                                            }
                                            cell.Value = strDateTime;
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }

                                    }
                                    break;
                                case PassportCode.SAMPSTAT:
                                    if (_dtAcccession != null)
                                    {
                                        var drValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (!drValue.IsNull("improvement_status_code"))
                                        {
                                            cell.Value = drValue.Field<string>("improvement_status_code");
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                case PassportCode.REMARKS:
                                    if (_dtAcccession != null)
                                    {
                                        var drValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (!drValue.IsNull("note"))
                                        {
                                            cell.Value = drValue.Field<string>("note");
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                case PassportCode.ACCESSION_TAXON:
                                    if (_dtAcccession != null)
                                    {
                                        var drValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        DataTable dtTaxonomySpecies = _sharedUtils.GetLocalData("select display_member from taxonomy_species_lookup where value_member = " + drValue.Field<int>("taxonomy_species_id"), "");
                                        cell.Value = dtTaxonomySpecies.Rows[0].Field<string>("display_member");
                                    }
                                    break;
                                case PassportCode.ACCESSION_STATUS:
                                    if (_dtAcccession != null)
                                    {
                                        var drValue = _dtAcccession.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        cell.Value = drValue.Field<string>("status_code");
                                    }
                                    break;
                                #endregion

                                #region accession_ipr
                                case PassportCode.MLSSTAT: //27. MLS status of the accession
                                    if (_dtAccessionIpr != null)
                                    {
                                        var drValue = _dtAccessionIpr.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("ipr_number"))
                                            {
                                                cell.Value = drValue.Field<string>("ipr_number");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    break;
                                #endregion

                                #region accession_pedigree
                                case PassportCode.ANCEST:
                                    break;
                                case PassportCode.PARENT_FEMALE: //20.1 Parent Female
                                    #region ParentFemale
                                    if (_dtAccessionPedigree != null)
                                    {
                                        var drValue = _dtAccessionPedigree.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("female_external_accession"))
                                            {
                                                cell.Value = drValue.Field<string>("female_external_accession");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                case PassportCode.PARENT_MALE: //20.2 Parent Male
                                    #region ParentMale
                                    if (_dtAccessionPedigree != null)
                                    {
                                        var drValue = _dtAccessionPedigree.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("male_external_accession"))
                                            {
                                                cell.Value = drValue.Field<string>("male_external_accession");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                case PassportCode.PARENT_FEMALE_ACCENUMB: //20.3 Parent Female (Accenumb)
                                    #region ParentFemaleAccenumb
                                    if (_dtAccessionPedigree != null)
                                    {
                                        var drValue = _dtAccessionPedigree.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("female_accession_id"))
                                            {
                                                DataTable dtAccession = _sharedUtils.GetLocalData("select display_member from accession_lookup where value_member = " + drValue.Field<string>("female_accession_id"), "");
                                                cell.Value = dtAccession.Rows[0].Field<string>("display_member");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                case PassportCode.PARENT_MALE_ACCENUM: //20.4 Parent Male (Accenumb)
                                    #region ParentMaleAccenumb
                                    if (_dtAccessionPedigree != null)
                                    {
                                        var drValue = _dtAccessionPedigree.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("male_accession_id"))
                                            {
                                                DataTable dtAccession = _sharedUtils.GetLocalData("select display_member from accession_lookup where value_member = " + drValue.Field<string>("male_accession_id"), "");
                                                cell.Value = dtAccession.Rows[0].Field<string>("display_member");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                #endregion

                                #region accession_source
                                case PassportCode.DECLATITUDE: //15.1 Latitude of collecting site
                                    #region DECLATITUDE
                                    if (_dtAccessionSource != null)
                                    {
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("latitude"))
                                            {
                                                cell.Value = drValue.Field<decimal>("latitude");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                case PassportCode.DECLONGITUDE: //15.3 Longitude of collecting site
                                    #region DECLONGITUDE
                                    if (_dtAccessionSource != null)
                                    {
                                        
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("longitude"))
                                            {
                                                cell.Value = drValue.Field<decimal>("longitude");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                case PassportCode.ELEVATION: //16. Elevation of collecting site
                                    #region ELEVATION
                                    if (_dtAccessionSource != null)
                                    {
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("elevation_meters"))
                                            {
                                                cell.Value = drValue.Field<int>("elevation_meters");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                case PassportCode.COLLDATE: //17. Collecting date of sample
                                    #region COLLDATE
                                    if (_dtAccessionSource != null)
                                    {
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("source_date"))
                                            {
                                                string strDateTime = drValue.Field<DateTime>("source_date").ToString("yyyyMMdd");
                                                if (!drValue.IsNull("source_date_code"))
                                                {
                                                    switch (drValue.Field<string>("source_date_code"))
                                                    {
                                                        case "yyyy":
                                                            strDateTime = strDateTime.Substring(0, 4) + "0000";
                                                            break;
                                                        case "MM/yyyy":
                                                            strDateTime = strDateTime.Substring(0, 6) + "00";
                                                            break;
                                                        case "MM/dd/yyyy":
                                                            break;
                                                    }
                                                }
                                                cell.Value = strDateTime;
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                case PassportCode.ACCESSION_SOURCE_FORMATTED_LOCALITY: //14. Location of collecting site
                                    #region COLLSITE
                                    if (_dtAccessionSource != null)
                                    {
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("formatted_locality"))
                                            {
                                                cell.Value = drValue.Field<string>("formatted_locality");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                case PassportCode.ACCESSION_SOURCE_VERBATIM_LOCALITY: //collector_verbatim_locality
                                    #region collector_verbatim_locality
                                    if (_dtAccessionSource != null)
                                    {
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("collector_verbatim_locality"))
                                            {
                                                cell.Value = drValue.Field<string>("collector_verbatim_locality");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                case PassportCode.COORDUNCERT: //
                                    #region COORDUNCERT
                                    if (_dtAccessionSource != null)
                                    {
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("uncertainty"))
                                            {
                                                cell.Value = drValue.Field<int>("uncertainty");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                case PassportCode.COORDDATUM: //
                                    #region COORDDATUM
                                    if (_dtAccessionSource != null)
                                    {
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("georeference_datum"))
                                            {
                                                cell.Value = drValue.Field<string>("georeference_datum");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                case PassportCode.GEOREFMETH: //
                                    #region GEOREFMETH
                                    if (_dtAccessionSource != null)
                                    {
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("georeference_protocol_code"))
                                            {
                                                cell.Value = drValue.Field<string>("georeference_protocol_code");
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                case PassportCode.ACCESSION_SOURCE_NOTE: //
                                    #region ACCESSION_SOURCE_NOTE
                                    if (_dtAccessionSource != null)
                                    {
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null && !drValue.IsNull("note"))
                                        {
                                            cell.Value = drValue.Field<string>("note");
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                    }
                                    #endregion
                                    break;
                                #endregion

                                default:
                                    //Check if custom descriptor is accession_inv_name
                                    var drCustomDescriptor = dtPassportCriteria.AsEnumerable().FirstOrDefault(x => x.Field<string>("value_member").Equals(col.Name) && x.Field<string>("table_name").Equals("accession_inv_name"));
                                    if (drCustomDescriptor != null)
                                    {
                                        if (_dtAccessionInvName != null)
                                        {
                                            var drValues = _dtAccessionInvName.AsEnumerable().Where(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<string>("category_code").Equals(drCustomDescriptor.Field<string>("category_code")));
                                            if (drValues.Count() > 0)
                                            {
                                                var names = drValues.AsEnumerable().Select(x => x.Field<string>("plant_name")).ToArray();
                                                cell.Value = string.Join(";", names);
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        break;
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            switch (col.Name)
                            {
                                case "13. Country of origin":
                                    #region ORIGCTY
                                    if (_dtAccessionSource != null)
                                    {
                                        var drValue = _dtAccessionSource.AsEnumerable().FirstOrDefault(x => x.Field<int>("accession_id") == rowAccessionId);
                                        if (drValue != null)
                                        {
                                            if (!drValue.IsNull("geography_id"))
                                            {
                                                DataTable dtGeography = _sharedUtils.GetLocalData("select display_member from geography_lookup where value_member = " + drValue.Field<int>("geography_id"), "");
                                                string[] geographyParts = dtGeography.Rows[0].Field<string>("display_member").Split(new char[] { ','});
                                                if(geographyParts.Length >= 1) cell.Value = geographyParts[0].Trim();
                                                if (geographyParts.Length >= 2) row.Cells["Adm1"].Value = geographyParts[1].Trim();
                                                if (geographyParts.Length >= 3) row.Cells["Adm2"].Value = geographyParts[2].Trim();
                                                if (geographyParts.Length >= 4) row.Cells["Adm3"].Value = geographyParts[3].Trim();
                                            }
                                            else
                                            {
                                                cell.Value = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = string.Empty;
                                        }
                                        
                                    }
                                    #endregion
                                    break;
                            }
                        }
                    }
                }
                Cursor.Current = Cursors.Arrow;
            }
            catch (WizardException wex)
            {
                Cursor.Current = Cursors.Arrow;
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "BatchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Arrow;
                GGMessageBox mb = new GGMessageBox(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void SelectAlltoolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow item in ux_datagridviewSheet.Rows)
                {
                    item.Selected = item.Visible;
                }
            }
            catch (Exception ex)
            {
                GGMessageBox mb = new GGMessageBox("\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_datagridviewSheet_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataObject doDragDropData;

                if ((e.RowIndex > -1) && (e.ColumnIndex > -1) && (ux_datagridviewSheet.Rows[e.RowIndex].Selected) && (e.Button == MouseButtons.Left))
                {
                    // If there were selected rows lets start the drag-drop operation...
                    if (ux_datagridviewSheet.SelectedRows.Count > 0)
                    {
                        //Check if accession_id is not empty
                        foreach (DataGridViewRow dgvRow in ux_datagridviewSheet.SelectedRows)
                        {
                            if (dgvRow.Cells["accession_id"].Value == null || string.IsNullOrEmpty(dgvRow.Cells["accession_id"].Value.ToString()))
                            {
                                throw new WizardException("Only validated accessions can be selected", "ux_textboxErrorOnlyValidatedAccessionsCanBeSelected");
                            }
                        }

                        // Get the string of selected rows extracted from the GridView...
                        doDragDropData = BuildDGVDragAndDropData(ux_datagridviewSheet);
                        ux_datagridviewSheet.DoDragDrop(doDragDropData, DragDropEffects.Copy);
                    }
                }
                /*if ((e.RowIndex > -1) && (e.ColumnIndex > -1) && (e.Button == MouseButtons.Right))
                {
                    // Change the color of the cell background so that the user
                    // knows what cell the context menu applies to...
                    ux_datagridviewSheet.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Red;
                    ux_datagridviewSheet.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionBackColor = Color.Red;
                }*/
            }
            catch (WizardException wex)
            {
                Cursor.Current = Cursors.Arrow;
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "BatchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                GGMessageBox mb = new GGMessageBox("\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private DataObject BuildDGVDragAndDropData(DataGridView dgv)
        {
            // Depending on how many rows are selected, this operation
            // could take a while - so change the cursor...
            Cursor.Current = Cursors.WaitCursor;

            DataObject dndData = new DataObject();

            // Write the rows out in text format first...
            StringBuilder sb = new StringBuilder("");

            string columnHeader = "";
            string columnNames = "";
            // First, gather the column headers...
            foreach (DataGridViewColumn dgvCol in dgv.Columns)
            {
                if (dgvCol.Visible)
                {
                    columnHeader += dgvCol.HeaderText + "\t";
                    columnNames += dgvCol.Name + "\t";
                }
            }
            // And write it to the string builder...
            sb.Append(columnHeader);
            // Build the array of visible column names...
            string[] columnList = columnNames.Trim().Split('\t');
            // And use that list for writing the values to the string builder...
            foreach (DataGridViewRow dgvr in dgv.SelectedRows)
            {
                sb.Append("\n");
                foreach (string columnName in columnList)
                {
                    sb.Append(dgvr.Cells[columnName].FormattedValue.ToString());
                    sb.Append("\t");
                }
            }

            DataTable dtQuickSearch = ((DataTable)dgv.DataSource).Clone();
            dtQuickSearch.PrimaryKey = new DataColumn[] { dtQuickSearch.Columns["accession_id"] };
            dtQuickSearch.PrimaryKey[0].ExtendedProperties["table_field_name"] = "accession_id";
            dtQuickSearch.PrimaryKey[0].ExtendedProperties["table_name"] = "accession";
            // Finally write it out as a collection of data table rows (for tree view drag and drop).
            DataSet dsData = new DataSet();
            dsData.Tables.Add(dtQuickSearch);
            foreach (DataGridViewRow dgvr in dgv.SelectedRows)
            {
                dsData.Tables[0].Rows.Add(((DataRowView)dgvr.DataBoundItem).Row.ItemArray);
            }

            // Set the data types into the data object and return...
            dndData.SetData(typeof(string), sb.ToString());
            dndData.SetData(typeof(DataSet), dsData);

            // We are all done so change the cursor back and return the data...
            //Cursor.Current = Cursors.Default;
            Cursor.Current = Cursors.Default;

            return dndData;
        }

        private void ux_textboxFindDescriptor_TextChanged(object sender, EventArgs e)
        {
            textChangeDelayTimer.Stop();
            textChangeDelayTimer.Interval = 1000;
            textChangeDelayTimer.Start();
        }

        private void ux_textboxFindDescriptor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                ux_listBoxTrait.Focus();
            }
        }
        void timerDelay_Tick(object sender, EventArgs e)
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            textChangeDelayTimer.Stop();
            RefreshDescriptorList();

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }
        private void RefreshDescriptorList()
        {
            if (ux_listBoxTrait.DataSource == null) return;

            string rowFilter = ux_listBoxTrait.DisplayMember + " like '%" + ux_textboxFindDescriptor.Text + "%'";

            (ux_listBoxTrait.DataSource as DataView).RowFilter = rowFilter;

            ux_textboxFindDescriptor.Focus();
            ux_textboxFindDescriptor.SelectionStart = ux_textboxFindDescriptor.Text.Length;
        }

        private void copyAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // could take a while - so change the cursor...
                Cursor.Current = Cursors.WaitCursor;

                DataObject dndData = new DataObject();

                // Write the rows out in text format first...
                StringBuilder sb = new StringBuilder("");

                string columnHeader = "";
                string columnNames = "";
                // First, gather the column headers...
                foreach (DataGridViewColumn dgvCol in ux_datagridviewSheet.Columns)
                {
                    if (dgvCol.Visible)
                    {
                        columnHeader += dgvCol.HeaderText + "\t";
                        columnNames += dgvCol.Name + "\t";
                    }
                }
                // And write it to the string builder...
                sb.Append(columnHeader);
                // Build the array of visible column names...
                string[] columnList = columnNames.Trim().Split('\t');
                // And use that list for writing the values to the string builder...
                foreach (DataGridViewRow dgvr in ux_datagridviewSheet.Rows)
                {
                    sb.Append("\n");
                    foreach (string columnName in columnList)
                    {
                        sb.Append(dgvr.Cells[columnName].FormattedValue.ToString());
                        sb.Append("\t");
                    }
                }

                // Set the data types into the data object and return...
                dndData.SetData(typeof(string), sb.ToString());

                Clipboard.SetDataObject(dndData);

                // We are all done so change the cursor back...
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void removeAllColumnsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                _descriptors.Clear();
                ux_listBoxDescriptors.DataSource = null;
                for (int iCol = _main2.Columns.Count - 1; iCol > -1; iCol--)
                {
                    if (_main2.Columns[iCol].ColumnName.Equals("ACCENUMB") || _main2.Columns[iCol].ColumnName.Equals("accession_id")
                        || _main2.Columns[iCol].ColumnName.Equals("inventory_id") || _main2.Columns[iCol].ColumnName.Equals("source_type_code")) continue;
                    _main2.Columns.RemoveAt(iCol);
                }
                if (ux_datagridviewSheet.Columns.Contains("source_type_code"))
                {
                    ux_datagridviewSheet.Columns["source_type_code"].Visible = false;
                }
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ValidateAccessionInvNameCell(DataGridViewCell cell, int rowInventoryId, string categoryCode)
        {
            string strVal = cell.Value.ToString();
            var dbValue = _dtAccessionInvName.AsEnumerable().FirstOrDefault(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<string>("category_code").Equals(categoryCode));
            if (dbValue != null)
            {
                strVal = !dbValue.IsNull("plant_name") ? dbValue.Field<string>("plant_name") : string.Empty;
                if (string.Equals(cell.Value.ToString(), strVal, ux_checkBoxMatchCase.Checked? StringComparison.InvariantCulture : StringComparison.OrdinalIgnoreCase))
                {
                    cell.ErrorText = string.Empty;
                    cell.Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                    validationOKWithoutChangesCount++;
                }
                else
                {
                    cell.ErrorText = string.Empty;
                    cell.Style.BackColor = StyleColors.ValidationOKWithChanges;
                    validationOKWithChangesCount++;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    cell.ErrorText = string.Empty;
                    cell.Style.BackColor = StyleColors.ValidationOKWithoutChanges;
                    validationOKWithoutChangesCount++;
                }
                else
                {
                    cell.ErrorText = string.Empty;
                    cell.Style.BackColor = StyleColors.ValidationOKWithChanges;
                    validationOKWithChangesCount++;
                }
            }
        }
        #endregion

        #region SavingMethods
        private void SaveInstitutionCodeCell(DataGridViewCell cell, int rowAccessionId, string sourceTypeCode)
        {
            string cellValue = cell.Value.ToString();

            List<int> cellCooperatorIds = new List<int>();
            var faoNumbers = cellValue.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            string errorText = string.Empty;
            foreach (var faoNumber in faoNumbers)
            {
                DataRow drValue = _dtCooperator.AsEnumerable().FirstOrDefault(x => x.Field<string>("secondary_organization_abbrev").Equals(faoNumber));
                if (drValue == null)
                {
                    errorText += faoNumber + " is not found" + "\n";
                }
                else
                    cellCooperatorIds.Add(drValue.Field<int>("cooperator_id"));
            }
            if (!string.IsNullOrEmpty(errorText))
            {
                cell.ErrorText = errorText + "\nIf organization is registered, update FAO WIEWS code in column \"secondary_organization_abbrev\" in table\"cooperator\".\n"
                    + "If organization is not registered, use Cooperator Wizard to register it first";
                cell.Style.BackColor = StyleColors.ValidationError;
                validationErrorCount++;
                return;
            }

            //Update dataviews
            var accSourceMapRows = _dtAccessionSourceMapExt.AsEnumerable().Where(x => x.RowState != DataRowState.Deleted && x.Field<int>("accession_id") == rowAccessionId && x.Field<string>("source_type_code").Equals(sourceTypeCode));
            if (accSourceMapRows == null || accSourceMapRows.Count() == 0) //dbValue is empty
            {
                //register all coopetators
                foreach (var cooperatorId in cellCooperatorIds)
                {
                    int minId;
                    if (_dtAccessionSourceMapExt.Rows.Count == 0)
                    {
                        minId = -1;
                    }
                    else
                    {
                        minId = _dtAccessionSourceMapExt.AsEnumerable().Min(x => x.Field<int>("accession_source_map_id"));
                        if (minId > 0)
                        {
                            minId = -1;
                        }
                        else
                        {
                            minId--;
                        }
                    }

                    DataRow drNew = _dtAccessionSourceMapExt.NewRow();
                    drNew["accession_source_map_id"] = minId;
                    drNew["accession_source_id"] = -1; //Update later
                    drNew["cooperator_id"] = cooperatorId;
                    drNew["created_date"] = DateTime.Now;
                    drNew["created_by"] = _sharedUtils.UserCooperatorID;
                    drNew["accession_id"] = rowAccessionId;
                    drNew["source_type_code"] = sourceTypeCode;
                    _dtAccessionSourceMapExt.Rows.Add(drNew);
                }
            }
            else //dbValue is not empty
            {
                int accessionSourceId = accSourceMapRows.First().Field<int>("accession_source_id");
                //register new ones
                foreach (var cellCooperatorId in cellCooperatorIds)
                {
                    if (!accSourceMapRows.Any(x => x.Field<int>("cooperator_id") == cellCooperatorId))
                    {
                        int minId;
                        if (_dtAccessionSourceMapExt.Rows.Count == 0)
                        {
                            minId = -1;
                        }
                        else
                        {
                            minId = _dtAccessionSourceMapExt.AsEnumerable().Min(x => x.Field<int>("accession_source_map_id"));
                            if (minId > 0)
                            {
                                minId = -1;
                            }
                            else
                            {
                                minId--;
                            }
                        }

                        DataRow drNew = _dtAccessionSourceMapExt.NewRow();
                        drNew["accession_source_map_id"] = minId;
                        drNew["accession_source_id"] = accessionSourceId;
                        drNew["cooperator_id"] = cellCooperatorId;
                        drNew["created_date"] = DateTime.Now;
                        drNew["created_by"] = _sharedUtils.UserCooperatorID;
                        drNew["accession_id"] = rowAccessionId;
                        drNew["source_type_code"] = sourceTypeCode;
                        _dtAccessionSourceMapExt.Rows.Add(drNew);
                    }
                }
                //deleted not found
                var deleteRows = accSourceMapRows.Where(x => !cellCooperatorIds.Any(y => y == x.Field<int>("cooperator_id")));
                foreach (var dr in deleteRows)
                {
                    dr.Delete();
                }
            }
            cell.Style.BackColor = Color.Orange;
        }

        private void ShowValueInsitutionCodeCell(int rowAccessionId, string sourceTypeCode, DataGridViewColumn col, DataGridViewRow row)
        {
            if (_dtAccessionSourceMapExt != null)
            {
                string messsage = string.Empty;
                var dbValues = _dtAccessionSourceMapExt.AsEnumerable().Where(x => x.RowState != DataRowState.Deleted && x.Field<int>("accession_id") == rowAccessionId 
                                                                                && x.Field<string>("source_type_code").Equals(sourceTypeCode));
                if (dbValues.Count() > 0)
                {
                    messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);
                    foreach (var dbValue in dbValues)
                    {
                        int modifiedById = dbValue.IsNull("modified_by") ? dbValue.Field<int>("created_by") : dbValue.Field<int>("modified_by");
                        DateTime modifiedDate = dbValue.IsNull("modified_date") ? dbValue.Field<DateTime>("created_date") : dbValue.Field<DateTime>("modified_date");

                        DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                        string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                        var drCooperator = _dtCooperator.AsEnumerable().FirstOrDefault(x => x.Field<int>("cooperator_id") == dbValue.Field<int>("cooperator_id"));
                        if (drCooperator != null)
                        {
                            messsage += "\n\nvalue : " + drCooperator.Field<string>("secondary_organization_abbrev");
                        }
                        else
                        {
                            messsage += "\n\nvalue : cooperator_id = " + dbValue.Field<int>("cooperator_id");
                        }
                        messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                    }
                }
                else
                {
                    messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                }
                GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ShowValueAccessionNameCell(int rowInventoryId, string categoryCode, DataGridViewColumn col, DataGridViewRow row)
        {
            if (_dtAccessionInvName != null)
            {
                string messsage = string.Empty;
                var drValues = _dtAccessionInvName.AsEnumerable().Where(x => x.Field<int>("inventory_id") == rowInventoryId && x.Field<string>("category_code").Equals(categoryCode));
                if (drValues.Count() > 0)
                {
                    messsage = string.Format("Current \"{0}\" value \nfor accession \"{1}\"", col.HeaderText, row.Cells[0].Value);
                    foreach (var dbValue in drValues)
                    {
                        int modifiedById = dbValue.IsNull("modified_by") ? dbValue.Field<int>("created_by") : dbValue.Field<int>("modified_by");
                        DateTime modifiedDate = dbValue.IsNull("modified_date") ? dbValue.Field<DateTime>("created_date") : dbValue.Field<DateTime>("modified_date");

                        DataTable dtModifiedBy = _sharedUtils.GetLocalData("select display_member from cooperator_lookup where value_member = " + modifiedById, "");
                        string modifiedBy = dtModifiedBy.Rows[0].Field<string>("display_member");

                        messsage += "\n\nvalue : " + dbValue.Field<string>("plant_name");
                        messsage += "\nLast modification date : " + modifiedDate + "\nModified by : " + modifiedBy;
                    }
                }
                else
                {
                    messsage = string.Format("Current \"{0}\" value for accession \"{1}\" is empty", col.HeaderText, row.Cells[0].Value);
                }
                GRINGlobal.Client.Common.GGMessageBox mb = new GGMessageBox(messsage, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }
        #endregion
    }

    class ColumnTag {
        public string Category {set; get;}
        public string TableName { set; get; }
        public ColumnTag(string category){ Category = category;}
    }

    static class StyleColors
    {
        public static Color ValidationError { get; set; }
        public static Color ValidationOKWithChanges { get; set; }
        public static Color ValidationOKWithoutChanges { get; set; }

        public static Color SavingError { get; set; }
        public static Color SavingUnmodified { get; set; }
        public static Color SavingOk{ get; set; }
    }
    static class PassportCode
    {
        public const string PUID = "PUID";
        public const string ACCENUMB = "ACCENUMB";
        
        public const string COLLNUMB = "COLLNUMB";
        
        public const string COLLCODE = "COLLCODE";
        public const string COLLNAME = "COLLNAME";
        public const string COLLINSTADDRESS = "COLLINSTADDRESS";
        public const string COLLMISSID = "COLLMISSID";

        public const string GENUS = "GENUS";
        public const string SPECIES = "SPECIES";
        public const string SPAUTHOR = "SPAUTHOR";
        public const string SUBTAXA = "SUBTAXA";
        public const string SUBTAUTHOR = "SUBTAUTHOR";
        public const string CROPNAME = "CROPNAME";

        public const string ACCENAME = "ACCENAME";
        public const string ACQDATE = "ACQDATE";
        public const string ORIGCTY = "ORIGCTY";

        public const string COLLSITE = "COLLSITE";
        public const string DECLATITUDE = "DECLATITUDE";
        public const string DECLONGITUDE = "DECLONGITUDE";
        public const string COORDUNCERT = "COORDUNCERT";
        public const string COORDDATUM = "COORDDATUM";
        public const string GEOREFMETH = "GEOREFMETH";
        public const string ELEVATION = "ELEVATION";
        public const string COLLDATE = "COLLDATE";
        
        public const string BREDCODE = "BREDCODE";
        public const string BREDNAME = "BREDNAME";

        public const string SAMPSTAT = "SAMPSTAT";
        public const string ANCEST = "ANCEST";
        public const string COLLSRC = "COLLSRC";
        
        public const string DONORCODE = "DONORCODE";
        public const string DONORNAME = "DONORNAME";

        public const string DONORNUMB = "DONORNUMB";
        public const string OTHERNUMB = "OTHERNUMB";
        public const string DUPLSITE = "DUPLSITE";
        public const string DUPLINSTNAME = "DUPLINSTNAME";

        public const string STORAGE = "STORAGE";
        public const string MLSSTAT = "MLSSTAT";
        public const string REMARKS = "REMARKS";

        /*Custom descriptors*/

        public const string ACCESSION_STATUS = "ACCESSION_STATUS";
        public const string BREEDER_CODE = "BREEDER_CODE";
        public const string BIOSAFETY_CODE = "BIOSAFETY_CODE";
        public const string PARENT_FEMALE = "PARENT_FEMALE";
        public const string PARENT_MALE = "PARENT_MALE";
        public const string PARENT_FEMALE_ACCENUMB = "PARENT_FEMALE_ACCENUMB";
        public const string PARENT_MALE_ACCENUM = "PARENT_MALE_ACCENUM";

        public const string ACCESSION_TAXON = "taxonomy_species_name";
        public const string ACCESSION_SOURCE_GEOGRAPHY = "accession_source_geography_id";
        public const string ACCESSION_SOURCE_VERBATIM_LOCALITY = "collector_verbatim_locality";
        public const string ACCESSION_SOURCE_FORMATTED_LOCALITY = "formatted_locality";
        public const string ACCESSION_SOURCE_NOTE = "accession_source_note";
    }

    class RowComparer : System.Collections.IComparer
    {
        private static int sortOrderModifier = 1;

        public RowComparer(SortOrder sortOrder)
        {
            if (sortOrder == SortOrder.Descending)
            {
                sortOrderModifier = -1;
            }
            else if (sortOrder == SortOrder.Ascending)
            {
                sortOrderModifier = 1;
            }
        }

        public int Compare(object x, object y)
        {
            DataGridViewRow DataGridViewRow1 = (DataGridViewRow)x;
            DataGridViewRow DataGridViewRow2 = (DataGridViewRow)y;

            // Try to sort based on the Last Name column.
            int CompareResult = System.String.Compare(
                DataGridViewRow1.Cells[1].Value.ToString(),
                DataGridViewRow2.Cells[1].Value.ToString());

            // If the Last Names are equal, sort based on the First Name.
            if (CompareResult == 0)
            {
                CompareResult = System.String.Compare(
                    DataGridViewRow1.Cells[0].Value.ToString(),
                    DataGridViewRow2.Cells[0].Value.ToString());
            }
            return CompareResult * sortOrderModifier;
        }
    }
}
