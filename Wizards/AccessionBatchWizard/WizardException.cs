﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessionBatchWizard
{
    public class WizardException : Exception
    {
        public string AppResourceName { get; set; }
        public string[] MessageParams { get; set; }

        public WizardException()
        {
            AppResourceName = string.Empty;
            MessageParams = new string[0];
        }

        public WizardException(string message, string appResourceName)
        : base(message)
        {
            AppResourceName = appResourceName;
            MessageParams = new string[0];
        }

        /*public WizardException(string message, string appResourceName, string[] messageParams)
        : base(message)
        {
            AppResourceName = appResourceName;
            MessageParams = messageParams;
        }*/

        public WizardException(string message, string appResourceName, params string[] messageParams)
        : base(message)
        {
            AppResourceName = appResourceName;
            MessageParams = messageParams;
        }

        public WizardException(string message)
        : base(message)
        {
            AppResourceName = string.Empty;
            MessageParams = new string[0];
        }

        public WizardException(string message, Exception inner)
        : base(message, inner)
        {
        }
    }
}
