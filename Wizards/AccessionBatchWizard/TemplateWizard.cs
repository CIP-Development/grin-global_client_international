﻿using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AccessionBatchWizard
{
    public partial class TemplateWizard : Form
    {
        SharedUtils _sharedUtils;
        string serverPath = string.Empty;

        public TemplateWizard()
        {
            InitializeComponent();
        }
        
        public TemplateWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();

            _sharedUtils = sharedUtils;
        }

        private void TemplateWizard_Load(object sender, EventArgs e)
        {
            serverPath = _sharedUtils.GetAppSettingValue("TemplateFormServerPath");
            //_sharedUtils.GetUserSetting
            //_sharedUtils.SaveUserSetting

            ux_datagridviewTemplate.Columns.Add("filename", "filename");
            ux_datagridviewTemplate.Columns["filename"].Visible = false;
            ux_datagridviewTemplate.Columns.Add("name", "name");
            if(!string.IsNullOrEmpty(serverPath))
                listTemplates();
        }

        private void listTemplates() {
            try
            {
                string[] filenames = Directory.GetFiles(serverPath);
                
                ux_datagridviewTemplate.Rows.Clear();
                string[] excelExtension = { ".xls",".xlsx"};

                foreach (var filename in filenames)
                {
                    if(excelExtension.Contains(Path.GetExtension(filename)))
                        ux_datagridviewTemplate.Rows.Add(filename, Path.GetFileName(filename));
                }
            }
            catch (Exception e) {
                MessageBox.Show(e.Message);
            }
        }

        private void ux_buttonLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdNewTemplate = new OpenFileDialog();
            if (ofdNewTemplate.ShowDialog() == DialogResult.OK) {
                string sourcefullpath = ofdNewTemplate.FileName;
                string destinationFullpath = serverPath + Path.GetFileName(sourcefullpath);
                try
                {
                    File.Copy(@sourcefullpath, @destinationFullpath, true);
                    listTemplates();
                }
                catch (Exception eAttach)
                {
                    MessageBox.Show(eAttach.Message);
                }
            }
        }

        private void ux_buttonDownload_Click(object sender, EventArgs e)
        {
            if (ux_datagridviewTemplate.SelectedRows.Count == 0) return;

            string sourcefullpath = ux_datagridviewTemplate.SelectedRows[0].Cells["filename"].Value.ToString();

            SaveFileDialog sfddownload = new SaveFileDialog();
            sfddownload.FileName = Path.GetFileName(sourcefullpath);
            if (sfddownload.ShowDialog() == DialogResult.OK)
            {
                string destinationFullpath = sfddownload.FileName;
                try
                {
                    File.Copy(@sourcefullpath, @destinationFullpath, true);
                }
                catch (Exception eAttach)
                {
                    MessageBox.Show(eAttach.Message);
                }
            }
        }
        
        private void ux_datagridviewTemplate_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (ux_datagridviewTemplate.SelectedRows.Count == 0) return;

            string sourcefullpath = ux_datagridviewTemplate.SelectedRows[0].Cells["filename"].Value.ToString();
            string TempPath = Path.GetTempPath();
            string fileName = Path.GetFileName(sourcefullpath);

            string fullpath = TempPath + fileName;

            Cursor current = Cursor.Current;
            ux_datagridviewTemplate.Enabled = false;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                File.Copy(@sourcefullpath, @fullpath, true);
                
                Process.Start(fullpath);
            }
            catch (Exception eAttach)
            {
                MessageBox.Show(eAttach.Message);
            }
            finally {
                ux_datagridviewTemplate.Enabled = true;
                Cursor.Current = current;
            }
        }
        
    }
}
