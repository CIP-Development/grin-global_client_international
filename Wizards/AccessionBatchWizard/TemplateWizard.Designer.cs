﻿namespace AccessionBatchWizard
{
    partial class TemplateWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_datagridviewTemplate = new System.Windows.Forms.DataGridView();
            this.ux_buttonLoad = new System.Windows.Forms.Button();
            this.ux_buttonDownload = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_datagridviewTemplate
            // 
            this.ux_datagridviewTemplate.AllowUserToAddRows = false;
            this.ux_datagridviewTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_datagridviewTemplate.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.ux_datagridviewTemplate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewTemplate.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ux_datagridviewTemplate.Location = new System.Drawing.Point(12, 12);
            this.ux_datagridviewTemplate.Name = "ux_datagridviewTemplate";
            this.ux_datagridviewTemplate.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ux_datagridviewTemplate.Size = new System.Drawing.Size(607, 314);
            this.ux_datagridviewTemplate.TabIndex = 0;
            this.ux_datagridviewTemplate.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ux_datagridviewTemplate_CellDoubleClick);
            // 
            // ux_buttonLoad
            // 
            this.ux_buttonLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_buttonLoad.Location = new System.Drawing.Point(12, 332);
            this.ux_buttonLoad.Name = "ux_buttonLoad";
            this.ux_buttonLoad.Size = new System.Drawing.Size(106, 23);
            this.ux_buttonLoad.TabIndex = 1;
            this.ux_buttonLoad.Text = "Upload File";
            this.ux_buttonLoad.UseVisualStyleBackColor = true;
            this.ux_buttonLoad.Click += new System.EventHandler(this.ux_buttonLoad_Click);
            // 
            // ux_buttonDownload
            // 
            this.ux_buttonDownload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonDownload.Location = new System.Drawing.Point(508, 332);
            this.ux_buttonDownload.Name = "ux_buttonDownload";
            this.ux_buttonDownload.Size = new System.Drawing.Size(111, 23);
            this.ux_buttonDownload.TabIndex = 2;
            this.ux_buttonDownload.Text = "Download File";
            this.ux_buttonDownload.UseVisualStyleBackColor = true;
            this.ux_buttonDownload.Click += new System.EventHandler(this.ux_buttonDownload_Click);
            // 
            // TemplateWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 362);
            this.Controls.Add(this.ux_buttonDownload);
            this.Controls.Add(this.ux_buttonLoad);
            this.Controls.Add(this.ux_datagridviewTemplate);
            this.Name = "TemplateWizard";
            this.Text = "Template Wizard";
            this.Load += new System.EventHandler(this.TemplateWizard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewTemplate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView ux_datagridviewTemplate;
        private System.Windows.Forms.Button ux_buttonLoad;
        private System.Windows.Forms.Button ux_buttonDownload;
    }
}