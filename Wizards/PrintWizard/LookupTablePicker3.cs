﻿using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GGWizard
{
    public partial class LookupTablePicker3 : Form
    {
        SharedUtils _sharedUtils;
        private DataTable _boundTable;
        
        private string _currentKey = "";
        private string _newKey = "";
        private string _newValue = "";
        private Timer textChangeDelayTimer = new Timer();
        private string _displayMember = "";
        private string _valueMember = "";
        private object _selectedItem = null;
        public bool IsListBoxStyled { get; set; }

        public LookupTablePicker3(SharedUtils sharedUtils, DataTable dtSource , string currentValue, string displayMember, string valueMember)
        {
            InitializeComponent();
            
            _sharedUtils = sharedUtils;
            _newKey = _currentKey;
            
            _boundTable = dtSource;
            _displayMember = displayMember;
            _valueMember = valueMember;

            textChangeDelayTimer.Tick += new EventHandler(timerDelay_Tick);
            ux_textboxFind.Text = currentValue;
            ux_textboxFind.Focus();
            ux_textboxFind.SelectionStart = ux_textboxFind.Text.Length;
            //sharedUtils.UpdateControls(this.Controls, this.Name);

            IsListBoxStyled = false;
        }

        public string NewKey
        {
            get
            {
                return _newKey;
            }
        }

        public Object SelectedItem
        {
            get
            {
                return _selectedItem;
            }
        }

        public string NewValue
        {
            get
            {
                return _newValue;
            }
        }

        private void LookupPicker_Load(object sender, EventArgs e)
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            _boundTable.DefaultView.Sort = _displayMember + " ASC";
                        
            ux_dataGridViewDisplayData.DataSource = _boundTable;
            
            ux_textboxFind.Focus();
            ux_textboxFind.SelectionStart = ux_textboxFind.Text.Length;

            if (IsListBoxStyled)
                SetListBoxStyle();

            if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void ux_textboxFind_TextChanged(object sender, EventArgs e)
        {
            textChangeDelayTimer.Stop();
            textChangeDelayTimer.Interval = 1000;
            textChangeDelayTimer.Start();
        }

        void timerDelay_Tick(object sender, EventArgs e)
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            textChangeDelayTimer.Stop();
            RefreshList();

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void RefreshList()
        {
            string rowFilter = "";
            /*
            if (_boundTable != null) rowFilter = _boundTable.DefaultView.RowFilter;
            
            // Break the row filter up into tokens...
            string[] tokens = rowFilter.Split(new string[1] { " AND " }, StringSplitOptions.RemoveEmptyEntries);

            // Start rebuilding the row filter for all criteria except display_member...
            rowFilter = "";
            foreach (string token in tokens)
            {
                if (!token.Contains(_displayMember)) rowFilter += token + " AND ";
            }
            */
            // Now add back in the display_member row filter...
            rowFilter += _displayMember + " like '%' + @displaymember + '%'";

            // Apply the row filter to the returned data...
            _boundTable.DefaultView.RowFilter = rowFilter.Replace("@displaymember", "'" + ux_textboxFind.Text.Replace("'", "''") + "'");

            ux_textboxFind.Focus();
            ux_textboxFind.SelectionStart = ux_textboxFind.Text.Length;
        }
        
        private void ux_buttonOK_Click(object sender, EventArgs e)
        {
            if (ux_dataGridViewDisplayData.SelectedRows.Count > 0)
            {
                _newKey = ux_dataGridViewDisplayData.SelectedRows[0].Cells[_valueMember].Value.ToString();
                _newValue = ux_dataGridViewDisplayData.SelectedRows[0].Cells[_displayMember].Value.ToString();
                _selectedItem = ux_dataGridViewDisplayData.SelectedRows[0].DataBoundItem;
            }
            else
            {
                _newKey = null;
                _newValue = "";
                _selectedItem = null;
            }
            ux_dataGridViewDisplayData.DataSource = null;
            _boundTable.DefaultView.RowFilter = "";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void ux_buttonCancel_Click(object sender, EventArgs e)
        {
            ux_dataGridViewDisplayData.DataSource = null;
            _boundTable.DefaultView.RowFilter = "";
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void ux_buttonRefresh_Click(object sender, EventArgs e)
        {
            if (_sharedUtils.LocalDatabaseTableExists(_boundTable.TableName))
            {
                _sharedUtils.LookupTablesUpdateTable(_boundTable.TableName, false);
                RefreshList();
            }
        }
        
        private void ux_textboxFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                ux_dataGridViewDisplayData.Focus();
                if (ux_dataGridViewDisplayData.SelectedRows.Count > 0 && ux_dataGridViewDisplayData.CurrentRow.Index < ux_dataGridViewDisplayData.Rows.Count - 1)
                {
                    ux_dataGridViewDisplayData.CurrentCell = ux_dataGridViewDisplayData.Rows[ux_dataGridViewDisplayData.CurrentRow.Index + 1].Cells[ux_dataGridViewDisplayData.FirstDisplayedScrollingColumnIndex];
                }
            }
        }

        private void SetListBoxStyle()
        {
            //Hide all columnas except displayMember
            foreach (DataGridViewColumn col in ux_dataGridViewDisplayData.Columns)
            {
                if (!col.Name.Equals(_displayMember))
                    col.Visible = false;
            }
            //Expand col size
            if (ux_dataGridViewDisplayData.Columns.Contains(_displayMember))
                ux_dataGridViewDisplayData.Columns[_displayMember].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            ux_dataGridViewDisplayData.ColumnHeadersVisible = false;
        }

        private void ux_dataGridViewDisplayData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ux_buttonOK.PerformClick();
        }

        private void ux_dataGridViewDisplayData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ux_buttonOK.PerformClick();
            }
        }
    }
}
