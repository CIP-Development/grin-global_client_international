﻿namespace PrintWizard
{
    partial class PrintWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintWizard));
            this.ux_bindingnavigatorForm = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPrintAllButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPrintButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPreviewButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAboutButton = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorEntityId = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.ux_tabcontrolMain = new System.Windows.Forms.TabControl();
            this.PrintingPage = new System.Windows.Forms.TabPage();
            this.ux_groupBoxLabel = new System.Windows.Forms.GroupBox();
            this.ux_buttonSaveAsUserSettings = new System.Windows.Forms.Button();
            this.ux_numericUpDownLabelsPerRecord = new System.Windows.Forms.NumericUpDown();
            this.ux_labelLabelsPerRecord = new System.Windows.Forms.Label();
            this.ux_labelLabelTemplate = new System.Windows.Forms.Label();
            this.ux_textboxDataview = new System.Windows.Forms.TextBox();
            this.ux_labelDataview = new System.Windows.Forms.Label();
            this.ux_textboxLabelTemplate = new System.Windows.Forms.TextBox();
            this.ux_groupBoxPrinter = new System.Windows.Forms.GroupBox();
            this.ux_numericUpDownPaperHeight = new System.Windows.Forms.NumericUpDown();
            this.ux_numericUpDownPaperWidth = new System.Windows.Forms.NumericUpDown();
            this.ux_numericUpDownHorizontalCount = new System.Windows.Forms.NumericUpDown();
            this.ux_numericUpDownVerticalGap = new System.Windows.Forms.NumericUpDown();
            this.ux_numericUpDownHorizontalGap = new System.Windows.Forms.NumericUpDown();
            this.ux_numericUpDownMarginTop = new System.Windows.Forms.NumericUpDown();
            this.ux_numericUpDownMarginLeft = new System.Windows.Forms.NumericUpDown();
            this.ux_comboBoxUnit = new System.Windows.Forms.ComboBox();
            this.ux_labelPageUnit = new System.Windows.Forms.Label();
            this.ux_labelVerticalGap = new System.Windows.Forms.Label();
            this.ux_labelMarginTop = new System.Windows.Forms.Label();
            this.ux_labelMarginLeft = new System.Windows.Forms.Label();
            this.ux_labelCustomPaperSize = new System.Windows.Forms.Label();
            this.ux_comboBoxPaperSize = new System.Windows.Forms.ComboBox();
            this.ux_labelHorizontalGap = new System.Windows.Forms.Label();
            this.ux_comboBoxPrintResolution = new System.Windows.Forms.ComboBox();
            this.ux_labelPrintResolution = new System.Windows.Forms.Label();
            this.ux_labelHorizontalCount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ux_textboxPrinter = new System.Windows.Forms.TextBox();
            this.ux_labelPrinter = new System.Windows.Forms.Label();
            this.ux_groupboxPreview = new System.Windows.Forms.GroupBox();
            this.ux_numericUpDownLabelHeight = new System.Windows.Forms.NumericUpDown();
            this.ux_numericUpDownLabelWidth = new System.Windows.Forms.NumericUpDown();
            this.ux_comboBoxSizeUnit = new System.Windows.Forms.ComboBox();
            this.ux_buttonRedraw = new System.Windows.Forms.Button();
            this.ux_comboBoxTemplatePrintDensity = new System.Windows.Forms.ComboBox();
            this.ux_pictureBoxPreview = new System.Windows.Forms.PictureBox();
            this.ux_labelSize = new System.Windows.Forms.Label();
            this.ux_labelTemplatePrintDensity = new System.Windows.Forms.Label();
            this.LabelSettingsPage = new System.Windows.Forms.TabPage();
            this.ux_textBoxTemplateExtension = new System.Windows.Forms.TextBox();
            this.ux_labelTemplateExtension = new System.Windows.Forms.Label();
            this.ux_groupBoxTemplateMap = new System.Windows.Forms.GroupBox();
            this.ux_dataGridViewLabelSettings = new System.Windows.Forms.DataGridView();
            this.ux_buttonNewTemplateMap = new System.Windows.Forms.Button();
            this.ux_buttonRefresh = new System.Windows.Forms.Button();
            this.ux_buttonSaveLabelSettings = new System.Windows.Forms.Button();
            this.ux_textBoxTemplateFolderPath = new System.Windows.Forms.TextBox();
            this.ux_labelTemplateFolderPath = new System.Windows.Forms.Label();
            this.ux_textBoxVariableEndsWith = new System.Windows.Forms.TextBox();
            this.ux_labelVariableEndsWith = new System.Windows.Forms.Label();
            this.ux_textBoxVariableStartsWith = new System.Windows.Forms.TextBox();
            this.ux_labelVariableStartsWith = new System.Windows.Forms.Label();
            this.ux_textBoxAppSettingsName = new System.Windows.Forms.TextBox();
            this.Ux_labelAppSettings = new System.Windows.Forms.Label();
            this.DataviewPage = new System.Windows.Forms.TabPage();
            this.ux_dataGridViewDataview = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.ux_bindingnavigatorForm)).BeginInit();
            this.ux_bindingnavigatorForm.SuspendLayout();
            this.ux_tabcontrolMain.SuspendLayout();
            this.PrintingPage.SuspendLayout();
            this.ux_groupBoxLabel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownLabelsPerRecord)).BeginInit();
            this.ux_groupBoxPrinter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownPaperHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownPaperWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownHorizontalCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownVerticalGap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownHorizontalGap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownMarginTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownMarginLeft)).BeginInit();
            this.ux_groupboxPreview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownLabelHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownLabelWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxPreview)).BeginInit();
            this.LabelSettingsPage.SuspendLayout();
            this.ux_groupBoxTemplateMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_dataGridViewLabelSettings)).BeginInit();
            this.DataviewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_dataGridViewDataview)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_bindingnavigatorForm
            // 
            this.ux_bindingnavigatorForm.AddNewItem = null;
            this.ux_bindingnavigatorForm.CountItem = this.bindingNavigatorCountItem;
            this.ux_bindingnavigatorForm.DeleteItem = this.bindingNavigatorDeleteItem;
            this.ux_bindingnavigatorForm.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ux_bindingnavigatorForm.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ux_bindingnavigatorForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorDeleteItem,
            this.bindingNavigatorPrintAllButton,
            this.toolStripSeparator7,
            this.bindingNavigatorPrintButton,
            this.toolStripSeparator1,
            this.bindingNavigatorPreviewButton,
            this.toolStripSeparator9,
            this.bindingNavigatorAboutButton,
            this.bindingNavigatorProgressBar,
            this.toolStripSeparator3,
            this.toolStripLabel1,
            this.toolStripSeparator5,
            this.bindingNavigatorEntityId,
            this.toolStripSeparator4});
            this.ux_bindingnavigatorForm.Location = new System.Drawing.Point(0, 0);
            this.ux_bindingnavigatorForm.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.ux_bindingnavigatorForm.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.ux_bindingnavigatorForm.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.ux_bindingnavigatorForm.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.ux_bindingnavigatorForm.Name = "ux_bindingnavigatorForm";
            this.ux_bindingnavigatorForm.PositionItem = this.bindingNavigatorPositionItem;
            this.ux_bindingnavigatorForm.Size = new System.Drawing.Size(667, 27);
            this.ux_bindingnavigatorForm.TabIndex = 25;
            this.ux_bindingnavigatorForm.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 24);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPrintAllButton
            // 
            this.bindingNavigatorPrintAllButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.bindingNavigatorPrintAllButton.Image = global::PrintWizard.Properties.Resources.i8_print;
            this.bindingNavigatorPrintAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorPrintAllButton.Name = "bindingNavigatorPrintAllButton";
            this.bindingNavigatorPrintAllButton.Size = new System.Drawing.Size(73, 24);
            this.bindingNavigatorPrintAllButton.Text = "Print All";
            this.bindingNavigatorPrintAllButton.Click += new System.EventHandler(this.bindingNavigatorPrintAllButton_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPrintButton
            // 
            this.bindingNavigatorPrintButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.bindingNavigatorPrintButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorPrintButton.Name = "bindingNavigatorPrintButton";
            this.bindingNavigatorPrintButton.Size = new System.Drawing.Size(79, 24);
            this.bindingNavigatorPrintButton.Text = "Print Current";
            this.bindingNavigatorPrintButton.Click += new System.EventHandler(this.bindingNavigatorPrintButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPreviewButton
            // 
            this.bindingNavigatorPreviewButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.bindingNavigatorPreviewButton.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorPreviewButton.Image")));
            this.bindingNavigatorPreviewButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorPreviewButton.Name = "bindingNavigatorPreviewButton";
            this.bindingNavigatorPreviewButton.Size = new System.Drawing.Size(89, 24);
            this.bindingNavigatorPreviewButton.Text = "Preview All";
            this.bindingNavigatorPreviewButton.Click += new System.EventHandler(this.bindingNavigatorPreviewButton_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorAboutButton
            // 
            this.bindingNavigatorAboutButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.bindingNavigatorAboutButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorAboutButton.Name = "bindingNavigatorAboutButton";
            this.bindingNavigatorAboutButton.Size = new System.Drawing.Size(44, 24);
            this.bindingNavigatorAboutButton.Text = "About";
            this.bindingNavigatorAboutButton.Click += new System.EventHandler(this.bindingNavigatorAboutButton_Click);
            // 
            // bindingNavigatorProgressBar
            // 
            this.bindingNavigatorProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.bindingNavigatorProgressBar.AutoSize = false;
            this.bindingNavigatorProgressBar.Name = "bindingNavigatorProgressBar";
            this.bindingNavigatorProgressBar.Size = new System.Drawing.Size(100, 22);
            this.bindingNavigatorProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.bindingNavigatorProgressBar.Visible = false;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(37, 24);
            this.toolStripLabel1.Text = "          ";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorEntityId
            // 
            this.bindingNavigatorEntityId.Name = "bindingNavigatorEntityId";
            this.bindingNavigatorEntityId.Size = new System.Drawing.Size(0, 24);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 27);
            // 
            // ux_tabcontrolMain
            // 
            this.ux_tabcontrolMain.Controls.Add(this.PrintingPage);
            this.ux_tabcontrolMain.Controls.Add(this.LabelSettingsPage);
            this.ux_tabcontrolMain.Controls.Add(this.DataviewPage);
            this.ux_tabcontrolMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tabcontrolMain.Location = new System.Drawing.Point(0, 27);
            this.ux_tabcontrolMain.Name = "ux_tabcontrolMain";
            this.ux_tabcontrolMain.SelectedIndex = 0;
            this.ux_tabcontrolMain.Size = new System.Drawing.Size(667, 567);
            this.ux_tabcontrolMain.TabIndex = 26;
            // 
            // PrintingPage
            // 
            this.PrintingPage.Controls.Add(this.ux_groupBoxLabel);
            this.PrintingPage.Controls.Add(this.ux_groupBoxPrinter);
            this.PrintingPage.Controls.Add(this.ux_groupboxPreview);
            this.PrintingPage.Location = new System.Drawing.Point(4, 22);
            this.PrintingPage.Name = "PrintingPage";
            this.PrintingPage.Padding = new System.Windows.Forms.Padding(3);
            this.PrintingPage.Size = new System.Drawing.Size(659, 541);
            this.PrintingPage.TabIndex = 0;
            this.PrintingPage.Text = "Print";
            this.PrintingPage.UseVisualStyleBackColor = true;
            // 
            // ux_groupBoxLabel
            // 
            this.ux_groupBoxLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupBoxLabel.Controls.Add(this.ux_buttonSaveAsUserSettings);
            this.ux_groupBoxLabel.Controls.Add(this.ux_numericUpDownLabelsPerRecord);
            this.ux_groupBoxLabel.Controls.Add(this.ux_labelLabelsPerRecord);
            this.ux_groupBoxLabel.Controls.Add(this.ux_labelLabelTemplate);
            this.ux_groupBoxLabel.Controls.Add(this.ux_textboxDataview);
            this.ux_groupBoxLabel.Controls.Add(this.ux_labelDataview);
            this.ux_groupBoxLabel.Controls.Add(this.ux_textboxLabelTemplate);
            this.ux_groupBoxLabel.Location = new System.Drawing.Point(8, 189);
            this.ux_groupBoxLabel.Name = "ux_groupBoxLabel";
            this.ux_groupBoxLabel.Size = new System.Drawing.Size(642, 113);
            this.ux_groupBoxLabel.TabIndex = 208;
            this.ux_groupBoxLabel.TabStop = false;
            this.ux_groupBoxLabel.Text = "Label template";
            // 
            // ux_buttonSaveAsUserSettings
            // 
            this.ux_buttonSaveAsUserSettings.Location = new System.Drawing.Point(484, 78);
            this.ux_buttonSaveAsUserSettings.Name = "ux_buttonSaveAsUserSettings";
            this.ux_buttonSaveAsUserSettings.Size = new System.Drawing.Size(141, 23);
            this.ux_buttonSaveAsUserSettings.TabIndex = 218;
            this.ux_buttonSaveAsUserSettings.Text = "Save for Mobile Printing";
            this.ux_buttonSaveAsUserSettings.UseVisualStyleBackColor = true;
            this.ux_buttonSaveAsUserSettings.Click += new System.EventHandler(this.ux_buttonSaveAsUserSettings_Click);
            // 
            // ux_numericUpDownLabelsPerRecord
            // 
            this.ux_numericUpDownLabelsPerRecord.Location = new System.Drawing.Point(208, 79);
            this.ux_numericUpDownLabelsPerRecord.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ux_numericUpDownLabelsPerRecord.Name = "ux_numericUpDownLabelsPerRecord";
            this.ux_numericUpDownLabelsPerRecord.Size = new System.Drawing.Size(58, 22);
            this.ux_numericUpDownLabelsPerRecord.TabIndex = 11;
            this.ux_numericUpDownLabelsPerRecord.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ux_labelLabelsPerRecord
            // 
            this.ux_labelLabelsPerRecord.AutoSize = true;
            this.ux_labelLabelsPerRecord.Location = new System.Drawing.Point(12, 81);
            this.ux_labelLabelsPerRecord.Name = "ux_labelLabelsPerRecord";
            this.ux_labelLabelsPerRecord.Size = new System.Drawing.Size(95, 13);
            this.ux_labelLabelsPerRecord.TabIndex = 201;
            this.ux_labelLabelsPerRecord.Tag = "";
            this.ux_labelLabelsPerRecord.Text = "Labels per record";
            // 
            // ux_labelLabelTemplate
            // 
            this.ux_labelLabelTemplate.AutoSize = true;
            this.ux_labelLabelTemplate.Location = new System.Drawing.Point(13, 26);
            this.ux_labelLabelTemplate.Name = "ux_labelLabelTemplate";
            this.ux_labelLabelTemplate.Size = new System.Drawing.Size(53, 13);
            this.ux_labelLabelTemplate.TabIndex = 113;
            this.ux_labelLabelTemplate.Tag = "";
            this.ux_labelLabelTemplate.Text = "Template";
            // 
            // ux_textboxDataview
            // 
            this.ux_textboxDataview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxDataview.Location = new System.Drawing.Point(208, 51);
            this.ux_textboxDataview.Name = "ux_textboxDataview";
            this.ux_textboxDataview.ReadOnly = true;
            this.ux_textboxDataview.Size = new System.Drawing.Size(417, 22);
            this.ux_textboxDataview.TabIndex = 10;
            this.ux_textboxDataview.Tag = "";
            this.ux_textboxDataview.Click += new System.EventHandler(this.ux_textboxDataview_Click);
            // 
            // ux_labelDataview
            // 
            this.ux_labelDataview.AutoSize = true;
            this.ux_labelDataview.Location = new System.Drawing.Point(13, 54);
            this.ux_labelDataview.Name = "ux_labelDataview";
            this.ux_labelDataview.Size = new System.Drawing.Size(54, 13);
            this.ux_labelDataview.TabIndex = 205;
            this.ux_labelDataview.Tag = "title";
            this.ux_labelDataview.Text = "Dataview";
            // 
            // ux_textboxLabelTemplate
            // 
            this.ux_textboxLabelTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxLabelTemplate.Location = new System.Drawing.Point(208, 23);
            this.ux_textboxLabelTemplate.Name = "ux_textboxLabelTemplate";
            this.ux_textboxLabelTemplate.Size = new System.Drawing.Size(417, 22);
            this.ux_textboxLabelTemplate.TabIndex = 9;
            this.ux_textboxLabelTemplate.Tag = "description";
            this.ux_textboxLabelTemplate.Click += new System.EventHandler(this.ux_textboxLabelTemplate_Click);
            // 
            // ux_groupBoxPrinter
            // 
            this.ux_groupBoxPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupBoxPrinter.Controls.Add(this.ux_numericUpDownPaperHeight);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_numericUpDownPaperWidth);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_numericUpDownHorizontalCount);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_numericUpDownVerticalGap);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_numericUpDownHorizontalGap);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_numericUpDownMarginTop);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_numericUpDownMarginLeft);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_comboBoxUnit);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_labelPageUnit);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_labelVerticalGap);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_labelMarginTop);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_labelMarginLeft);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_labelCustomPaperSize);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_comboBoxPaperSize);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_labelHorizontalGap);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_comboBoxPrintResolution);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_labelPrintResolution);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_labelHorizontalCount);
            this.ux_groupBoxPrinter.Controls.Add(this.label1);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_textboxPrinter);
            this.ux_groupBoxPrinter.Controls.Add(this.ux_labelPrinter);
            this.ux_groupBoxPrinter.Location = new System.Drawing.Point(8, 6);
            this.ux_groupBoxPrinter.Name = "ux_groupBoxPrinter";
            this.ux_groupBoxPrinter.Size = new System.Drawing.Size(643, 177);
            this.ux_groupBoxPrinter.TabIndex = 207;
            this.ux_groupBoxPrinter.TabStop = false;
            this.ux_groupBoxPrinter.Text = "Printer";
            // 
            // ux_numericUpDownPaperHeight
            // 
            this.ux_numericUpDownPaperHeight.DecimalPlaces = 3;
            this.ux_numericUpDownPaperHeight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.ux_numericUpDownPaperHeight.Location = new System.Drawing.Point(548, 81);
            this.ux_numericUpDownPaperHeight.Name = "ux_numericUpDownPaperHeight";
            this.ux_numericUpDownPaperHeight.Size = new System.Drawing.Size(58, 22);
            this.ux_numericUpDownPaperHeight.TabIndex = 241;
            this.ux_numericUpDownPaperHeight.Visible = false;
            // 
            // ux_numericUpDownPaperWidth
            // 
            this.ux_numericUpDownPaperWidth.DecimalPlaces = 3;
            this.ux_numericUpDownPaperWidth.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.ux_numericUpDownPaperWidth.Location = new System.Drawing.Point(471, 81);
            this.ux_numericUpDownPaperWidth.Name = "ux_numericUpDownPaperWidth";
            this.ux_numericUpDownPaperWidth.Size = new System.Drawing.Size(58, 22);
            this.ux_numericUpDownPaperWidth.TabIndex = 240;
            this.ux_numericUpDownPaperWidth.Visible = false;
            // 
            // ux_numericUpDownHorizontalCount
            // 
            this.ux_numericUpDownHorizontalCount.Location = new System.Drawing.Point(126, 111);
            this.ux_numericUpDownHorizontalCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ux_numericUpDownHorizontalCount.Name = "ux_numericUpDownHorizontalCount";
            this.ux_numericUpDownHorizontalCount.Size = new System.Drawing.Size(58, 22);
            this.ux_numericUpDownHorizontalCount.TabIndex = 4;
            this.ux_numericUpDownHorizontalCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ux_numericUpDownVerticalGap
            // 
            this.ux_numericUpDownVerticalGap.DecimalPlaces = 3;
            this.ux_numericUpDownVerticalGap.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.ux_numericUpDownVerticalGap.Location = new System.Drawing.Point(305, 141);
            this.ux_numericUpDownVerticalGap.Name = "ux_numericUpDownVerticalGap";
            this.ux_numericUpDownVerticalGap.Size = new System.Drawing.Size(58, 22);
            this.ux_numericUpDownVerticalGap.TabIndex = 6;
            // 
            // ux_numericUpDownHorizontalGap
            // 
            this.ux_numericUpDownHorizontalGap.DecimalPlaces = 3;
            this.ux_numericUpDownHorizontalGap.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.ux_numericUpDownHorizontalGap.Location = new System.Drawing.Point(305, 111);
            this.ux_numericUpDownHorizontalGap.Name = "ux_numericUpDownHorizontalGap";
            this.ux_numericUpDownHorizontalGap.Size = new System.Drawing.Size(58, 22);
            this.ux_numericUpDownHorizontalGap.TabIndex = 5;
            // 
            // ux_numericUpDownMarginTop
            // 
            this.ux_numericUpDownMarginTop.DecimalPlaces = 3;
            this.ux_numericUpDownMarginTop.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.ux_numericUpDownMarginTop.Location = new System.Drawing.Point(471, 141);
            this.ux_numericUpDownMarginTop.Name = "ux_numericUpDownMarginTop";
            this.ux_numericUpDownMarginTop.Size = new System.Drawing.Size(58, 22);
            this.ux_numericUpDownMarginTop.TabIndex = 8;
            // 
            // ux_numericUpDownMarginLeft
            // 
            this.ux_numericUpDownMarginLeft.DecimalPlaces = 3;
            this.ux_numericUpDownMarginLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.ux_numericUpDownMarginLeft.Location = new System.Drawing.Point(471, 111);
            this.ux_numericUpDownMarginLeft.Name = "ux_numericUpDownMarginLeft";
            this.ux_numericUpDownMarginLeft.Size = new System.Drawing.Size(58, 22);
            this.ux_numericUpDownMarginLeft.TabIndex = 7;
            // 
            // ux_comboBoxUnit
            // 
            this.ux_comboBoxUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboBoxUnit.FormattingEnabled = true;
            this.ux_comboBoxUnit.Items.AddRange(new object[] {
            "inches",
            "mm"});
            this.ux_comboBoxUnit.Location = new System.Drawing.Point(471, 52);
            this.ux_comboBoxUnit.Name = "ux_comboBoxUnit";
            this.ux_comboBoxUnit.Size = new System.Drawing.Size(58, 21);
            this.ux_comboBoxUnit.TabIndex = 234;
            this.ux_comboBoxUnit.SelectedIndexChanged += new System.EventHandler(this.ux_comboBoxUnit_SelectedIndexChanged);
            // 
            // ux_labelPageUnit
            // 
            this.ux_labelPageUnit.AutoSize = true;
            this.ux_labelPageUnit.Location = new System.Drawing.Point(392, 57);
            this.ux_labelPageUnit.Name = "ux_labelPageUnit";
            this.ux_labelPageUnit.Size = new System.Drawing.Size(29, 13);
            this.ux_labelPageUnit.TabIndex = 233;
            this.ux_labelPageUnit.Tag = "";
            this.ux_labelPageUnit.Text = "Unit";
            // 
            // ux_labelVerticalGap
            // 
            this.ux_labelVerticalGap.AutoSize = true;
            this.ux_labelVerticalGap.Location = new System.Drawing.Point(205, 143);
            this.ux_labelVerticalGap.Name = "ux_labelVerticalGap";
            this.ux_labelVerticalGap.Size = new System.Drawing.Size(67, 13);
            this.ux_labelVerticalGap.TabIndex = 230;
            this.ux_labelVerticalGap.Tag = "";
            this.ux_labelVerticalGap.Text = "Vertical gap";
            // 
            // ux_labelMarginTop
            // 
            this.ux_labelMarginTop.AutoSize = true;
            this.ux_labelMarginTop.Location = new System.Drawing.Point(392, 144);
            this.ux_labelMarginTop.Name = "ux_labelMarginTop";
            this.ux_labelMarginTop.Size = new System.Drawing.Size(65, 13);
            this.ux_labelMarginTop.TabIndex = 228;
            this.ux_labelMarginTop.Tag = "";
            this.ux_labelMarginTop.Text = "Margin top";
            // 
            // ux_labelMarginLeft
            // 
            this.ux_labelMarginLeft.AutoSize = true;
            this.ux_labelMarginLeft.Location = new System.Drawing.Point(392, 113);
            this.ux_labelMarginLeft.Name = "ux_labelMarginLeft";
            this.ux_labelMarginLeft.Size = new System.Drawing.Size(64, 13);
            this.ux_labelMarginLeft.TabIndex = 226;
            this.ux_labelMarginLeft.Tag = "";
            this.ux_labelMarginLeft.Text = "Margin left";
            // 
            // ux_labelCustomPaperSize
            // 
            this.ux_labelCustomPaperSize.AutoSize = true;
            this.ux_labelCustomPaperSize.Location = new System.Drawing.Point(392, 85);
            this.ux_labelCustomPaperSize.Name = "ux_labelCustomPaperSize";
            this.ux_labelCustomPaperSize.Size = new System.Drawing.Size(46, 13);
            this.ux_labelCustomPaperSize.TabIndex = 225;
            this.ux_labelCustomPaperSize.Tag = "";
            this.ux_labelCustomPaperSize.Text = "Custom";
            this.ux_labelCustomPaperSize.Visible = false;
            // 
            // ux_comboBoxPaperSize
            // 
            this.ux_comboBoxPaperSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboBoxPaperSize.FormattingEnabled = true;
            this.ux_comboBoxPaperSize.Location = new System.Drawing.Point(126, 52);
            this.ux_comboBoxPaperSize.Name = "ux_comboBoxPaperSize";
            this.ux_comboBoxPaperSize.Size = new System.Drawing.Size(237, 21);
            this.ux_comboBoxPaperSize.TabIndex = 2;
            // 
            // ux_labelHorizontalGap
            // 
            this.ux_labelHorizontalGap.AutoSize = true;
            this.ux_labelHorizontalGap.Location = new System.Drawing.Point(205, 113);
            this.ux_labelHorizontalGap.Name = "ux_labelHorizontalGap";
            this.ux_labelHorizontalGap.Size = new System.Drawing.Size(84, 13);
            this.ux_labelHorizontalGap.TabIndex = 222;
            this.ux_labelHorizontalGap.Tag = "";
            this.ux_labelHorizontalGap.Text = "Horizontal gap";
            // 
            // ux_comboBoxPrintResolution
            // 
            this.ux_comboBoxPrintResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboBoxPrintResolution.FormattingEnabled = true;
            this.ux_comboBoxPrintResolution.Location = new System.Drawing.Point(126, 81);
            this.ux_comboBoxPrintResolution.Name = "ux_comboBoxPrintResolution";
            this.ux_comboBoxPrintResolution.Size = new System.Drawing.Size(237, 21);
            this.ux_comboBoxPrintResolution.TabIndex = 3;
            // 
            // ux_labelPrintResolution
            // 
            this.ux_labelPrintResolution.AutoSize = true;
            this.ux_labelPrintResolution.Location = new System.Drawing.Point(12, 85);
            this.ux_labelPrintResolution.Name = "ux_labelPrintResolution";
            this.ux_labelPrintResolution.Size = new System.Drawing.Size(87, 13);
            this.ux_labelPrintResolution.TabIndex = 220;
            this.ux_labelPrintResolution.Tag = "";
            this.ux_labelPrintResolution.Text = "Print resolution";
            // 
            // ux_labelHorizontalCount
            // 
            this.ux_labelHorizontalCount.AutoSize = true;
            this.ux_labelHorizontalCount.Location = new System.Drawing.Point(13, 113);
            this.ux_labelHorizontalCount.Name = "ux_labelHorizontalCount";
            this.ux_labelHorizontalCount.Size = new System.Drawing.Size(94, 13);
            this.ux_labelHorizontalCount.TabIndex = 218;
            this.ux_labelHorizontalCount.Tag = "";
            this.ux_labelHorizontalCount.Text = "Horizontal count";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 214;
            this.label1.Tag = "";
            this.label1.Text = "Paper size";
            // 
            // ux_textboxPrinter
            // 
            this.ux_textboxPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxPrinter.Location = new System.Drawing.Point(208, 21);
            this.ux_textboxPrinter.Name = "ux_textboxPrinter";
            this.ux_textboxPrinter.Size = new System.Drawing.Size(417, 22);
            this.ux_textboxPrinter.TabIndex = 1;
            this.ux_textboxPrinter.Tag = "";
            this.ux_textboxPrinter.Click += new System.EventHandler(this.ux_textboxPrinter_Click);
            // 
            // ux_labelPrinter
            // 
            this.ux_labelPrinter.AutoSize = true;
            this.ux_labelPrinter.Location = new System.Drawing.Point(13, 24);
            this.ux_labelPrinter.Name = "ux_labelPrinter";
            this.ux_labelPrinter.Size = new System.Drawing.Size(72, 13);
            this.ux_labelPrinter.TabIndex = 111;
            this.ux_labelPrinter.Tag = "title";
            this.ux_labelPrinter.Text = "Printer name";
            // 
            // ux_groupboxPreview
            // 
            this.ux_groupboxPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxPreview.Controls.Add(this.ux_numericUpDownLabelHeight);
            this.ux_groupboxPreview.Controls.Add(this.ux_numericUpDownLabelWidth);
            this.ux_groupboxPreview.Controls.Add(this.ux_comboBoxSizeUnit);
            this.ux_groupboxPreview.Controls.Add(this.ux_buttonRedraw);
            this.ux_groupboxPreview.Controls.Add(this.ux_comboBoxTemplatePrintDensity);
            this.ux_groupboxPreview.Controls.Add(this.ux_pictureBoxPreview);
            this.ux_groupboxPreview.Controls.Add(this.ux_labelSize);
            this.ux_groupboxPreview.Controls.Add(this.ux_labelTemplatePrintDensity);
            this.ux_groupboxPreview.Location = new System.Drawing.Point(8, 308);
            this.ux_groupboxPreview.Name = "ux_groupboxPreview";
            this.ux_groupboxPreview.Size = new System.Drawing.Size(643, 225);
            this.ux_groupboxPreview.TabIndex = 104;
            this.ux_groupboxPreview.TabStop = false;
            this.ux_groupboxPreview.Text = "Template preview";
            // 
            // ux_numericUpDownLabelHeight
            // 
            this.ux_numericUpDownLabelHeight.DecimalPlaces = 3;
            this.ux_numericUpDownLabelHeight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.ux_numericUpDownLabelHeight.Location = new System.Drawing.Point(80, 84);
            this.ux_numericUpDownLabelHeight.Name = "ux_numericUpDownLabelHeight";
            this.ux_numericUpDownLabelHeight.Size = new System.Drawing.Size(58, 22);
            this.ux_numericUpDownLabelHeight.TabIndex = 14;
            this.ux_numericUpDownLabelHeight.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ux_numericUpDownLabelWidth
            // 
            this.ux_numericUpDownLabelWidth.DecimalPlaces = 3;
            this.ux_numericUpDownLabelWidth.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.ux_numericUpDownLabelWidth.Location = new System.Drawing.Point(16, 84);
            this.ux_numericUpDownLabelWidth.Name = "ux_numericUpDownLabelWidth";
            this.ux_numericUpDownLabelWidth.Size = new System.Drawing.Size(58, 22);
            this.ux_numericUpDownLabelWidth.TabIndex = 13;
            this.ux_numericUpDownLabelWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ux_comboBoxSizeUnit
            // 
            this.ux_comboBoxSizeUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboBoxSizeUnit.FormattingEnabled = true;
            this.ux_comboBoxSizeUnit.Items.AddRange(new object[] {
            "inches",
            "mm"});
            this.ux_comboBoxSizeUnit.Location = new System.Drawing.Point(144, 84);
            this.ux_comboBoxSizeUnit.Name = "ux_comboBoxSizeUnit";
            this.ux_comboBoxSizeUnit.Size = new System.Drawing.Size(58, 21);
            this.ux_comboBoxSizeUnit.TabIndex = 213;
            // 
            // ux_buttonRedraw
            // 
            this.ux_buttonRedraw.Location = new System.Drawing.Point(126, 127);
            this.ux_buttonRedraw.Name = "ux_buttonRedraw";
            this.ux_buttonRedraw.Size = new System.Drawing.Size(76, 23);
            this.ux_buttonRedraw.TabIndex = 15;
            this.ux_buttonRedraw.Text = "Redraw";
            this.ux_buttonRedraw.UseVisualStyleBackColor = true;
            this.ux_buttonRedraw.Click += new System.EventHandler(this.ux_buttonRedraw_Click);
            // 
            // ux_comboBoxTemplatePrintDensity
            // 
            this.ux_comboBoxTemplatePrintDensity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboBoxTemplatePrintDensity.FormattingEnabled = true;
            this.ux_comboBoxTemplatePrintDensity.Items.AddRange(new object[] {
            "6dpmm",
            "8dpmm",
            "12dpmm",
            "24dpmm"});
            this.ux_comboBoxTemplatePrintDensity.Location = new System.Drawing.Point(126, 31);
            this.ux_comboBoxTemplatePrintDensity.Name = "ux_comboBoxTemplatePrintDensity";
            this.ux_comboBoxTemplatePrintDensity.Size = new System.Drawing.Size(76, 21);
            this.ux_comboBoxTemplatePrintDensity.TabIndex = 12;
            // 
            // ux_pictureBoxPreview
            // 
            this.ux_pictureBoxPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_pictureBoxPreview.BackColor = System.Drawing.Color.LightGray;
            this.ux_pictureBoxPreview.Location = new System.Drawing.Point(208, 28);
            this.ux_pictureBoxPreview.Name = "ux_pictureBoxPreview";
            this.ux_pictureBoxPreview.Size = new System.Drawing.Size(417, 182);
            this.ux_pictureBoxPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ux_pictureBoxPreview.TabIndex = 204;
            this.ux_pictureBoxPreview.TabStop = false;
            // 
            // ux_labelSize
            // 
            this.ux_labelSize.AutoSize = true;
            this.ux_labelSize.Location = new System.Drawing.Point(13, 63);
            this.ux_labelSize.Name = "ux_labelSize";
            this.ux_labelSize.Size = new System.Drawing.Size(75, 13);
            this.ux_labelSize.TabIndex = 203;
            this.ux_labelSize.Tag = "";
            this.ux_labelSize.Text = "Template size";
            // 
            // ux_labelTemplatePrintDensity
            // 
            this.ux_labelTemplatePrintDensity.AutoSize = true;
            this.ux_labelTemplatePrintDensity.Location = new System.Drawing.Point(13, 31);
            this.ux_labelTemplatePrintDensity.Name = "ux_labelTemplatePrintDensity";
            this.ux_labelTemplatePrintDensity.Size = new System.Drawing.Size(71, 13);
            this.ux_labelTemplatePrintDensity.TabIndex = 203;
            this.ux_labelTemplatePrintDensity.Tag = "";
            this.ux_labelTemplatePrintDensity.Text = "Print density";
            // 
            // LabelSettingsPage
            // 
            this.LabelSettingsPage.Controls.Add(this.ux_textBoxTemplateExtension);
            this.LabelSettingsPage.Controls.Add(this.ux_labelTemplateExtension);
            this.LabelSettingsPage.Controls.Add(this.ux_groupBoxTemplateMap);
            this.LabelSettingsPage.Controls.Add(this.ux_buttonSaveLabelSettings);
            this.LabelSettingsPage.Controls.Add(this.ux_textBoxTemplateFolderPath);
            this.LabelSettingsPage.Controls.Add(this.ux_labelTemplateFolderPath);
            this.LabelSettingsPage.Controls.Add(this.ux_textBoxVariableEndsWith);
            this.LabelSettingsPage.Controls.Add(this.ux_labelVariableEndsWith);
            this.LabelSettingsPage.Controls.Add(this.ux_textBoxVariableStartsWith);
            this.LabelSettingsPage.Controls.Add(this.ux_labelVariableStartsWith);
            this.LabelSettingsPage.Controls.Add(this.ux_textBoxAppSettingsName);
            this.LabelSettingsPage.Controls.Add(this.Ux_labelAppSettings);
            this.LabelSettingsPage.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.LabelSettingsPage.Location = new System.Drawing.Point(4, 22);
            this.LabelSettingsPage.Name = "LabelSettingsPage";
            this.LabelSettingsPage.Padding = new System.Windows.Forms.Padding(3);
            this.LabelSettingsPage.Size = new System.Drawing.Size(659, 541);
            this.LabelSettingsPage.TabIndex = 2;
            this.LabelSettingsPage.Text = "Settings";
            this.LabelSettingsPage.UseVisualStyleBackColor = true;
            // 
            // ux_textBoxTemplateExtension
            // 
            this.ux_textBoxTemplateExtension.Enabled = false;
            this.ux_textBoxTemplateExtension.Location = new System.Drawing.Point(151, 117);
            this.ux_textBoxTemplateExtension.Name = "ux_textBoxTemplateExtension";
            this.ux_textBoxTemplateExtension.Size = new System.Drawing.Size(50, 22);
            this.ux_textBoxTemplateExtension.TabIndex = 217;
            this.ux_textBoxTemplateExtension.Tag = "description";
            // 
            // ux_labelTemplateExtension
            // 
            this.ux_labelTemplateExtension.AutoSize = true;
            this.ux_labelTemplateExtension.Location = new System.Drawing.Point(12, 120);
            this.ux_labelTemplateExtension.Name = "ux_labelTemplateExtension";
            this.ux_labelTemplateExtension.Size = new System.Drawing.Size(106, 13);
            this.ux_labelTemplateExtension.TabIndex = 216;
            this.ux_labelTemplateExtension.Text = "Template extension";
            // 
            // ux_groupBoxTemplateMap
            // 
            this.ux_groupBoxTemplateMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupBoxTemplateMap.Controls.Add(this.ux_dataGridViewLabelSettings);
            this.ux_groupBoxTemplateMap.Controls.Add(this.ux_buttonNewTemplateMap);
            this.ux_groupBoxTemplateMap.Controls.Add(this.ux_buttonRefresh);
            this.ux_groupBoxTemplateMap.Location = new System.Drawing.Point(15, 153);
            this.ux_groupBoxTemplateMap.Name = "ux_groupBoxTemplateMap";
            this.ux_groupBoxTemplateMap.Size = new System.Drawing.Size(624, 341);
            this.ux_groupBoxTemplateMap.TabIndex = 215;
            this.ux_groupBoxTemplateMap.TabStop = false;
            this.ux_groupBoxTemplateMap.Text = "Template mapping";
            // 
            // ux_dataGridViewLabelSettings
            // 
            this.ux_dataGridViewLabelSettings.AllowUserToAddRows = false;
            this.ux_dataGridViewLabelSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_dataGridViewLabelSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_dataGridViewLabelSettings.Location = new System.Drawing.Point(16, 51);
            this.ux_dataGridViewLabelSettings.Name = "ux_dataGridViewLabelSettings";
            this.ux_dataGridViewLabelSettings.Size = new System.Drawing.Size(591, 271);
            this.ux_dataGridViewLabelSettings.TabIndex = 214;
            this.ux_dataGridViewLabelSettings.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ux_dataGridViewLabelSettings_EditingControlShowing);
            this.ux_dataGridViewLabelSettings.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ux_dataGridViewLabelSettings_KeyDown);
            // 
            // ux_buttonNewTemplateMap
            // 
            this.ux_buttonNewTemplateMap.Location = new System.Drawing.Point(16, 20);
            this.ux_buttonNewTemplateMap.Name = "ux_buttonNewTemplateMap";
            this.ux_buttonNewTemplateMap.Size = new System.Drawing.Size(121, 23);
            this.ux_buttonNewTemplateMap.TabIndex = 216;
            this.ux_buttonNewTemplateMap.Text = "New template map";
            this.ux_buttonNewTemplateMap.UseVisualStyleBackColor = true;
            this.ux_buttonNewTemplateMap.Click += new System.EventHandler(this.ux_buttonNewTemplateMap_Click);
            // 
            // ux_buttonRefresh
            // 
            this.ux_buttonRefresh.Location = new System.Drawing.Point(143, 20);
            this.ux_buttonRefresh.Name = "ux_buttonRefresh";
            this.ux_buttonRefresh.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonRefresh.TabIndex = 215;
            this.ux_buttonRefresh.Text = "Refresh";
            this.ux_buttonRefresh.UseVisualStyleBackColor = true;
            this.ux_buttonRefresh.Click += new System.EventHandler(this.ux_buttonRefresh_Click);
            // 
            // ux_buttonSaveLabelSettings
            // 
            this.ux_buttonSaveLabelSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSaveLabelSettings.Location = new System.Drawing.Point(564, 500);
            this.ux_buttonSaveLabelSettings.Name = "ux_buttonSaveLabelSettings";
            this.ux_buttonSaveLabelSettings.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonSaveLabelSettings.TabIndex = 211;
            this.ux_buttonSaveLabelSettings.Text = "Save";
            this.ux_buttonSaveLabelSettings.UseVisualStyleBackColor = true;
            this.ux_buttonSaveLabelSettings.Click += new System.EventHandler(this.ux_buttonSaveLabelSettings_Click);
            // 
            // ux_textBoxTemplateFolderPath
            // 
            this.ux_textBoxTemplateFolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textBoxTemplateFolderPath.Location = new System.Drawing.Point(151, 82);
            this.ux_textBoxTemplateFolderPath.Name = "ux_textBoxTemplateFolderPath";
            this.ux_textBoxTemplateFolderPath.Size = new System.Drawing.Size(488, 22);
            this.ux_textBoxTemplateFolderPath.TabIndex = 209;
            this.ux_textBoxTemplateFolderPath.Tag = "description";
            this.ux_textBoxTemplateFolderPath.Click += new System.EventHandler(this.ux_textBoxTemplateFolderPath_Click);
            // 
            // ux_labelTemplateFolderPath
            // 
            this.ux_labelTemplateFolderPath.AutoSize = true;
            this.ux_labelTemplateFolderPath.Location = new System.Drawing.Point(12, 85);
            this.ux_labelTemplateFolderPath.Name = "ux_labelTemplateFolderPath";
            this.ux_labelTemplateFolderPath.Size = new System.Drawing.Size(114, 13);
            this.ux_labelTemplateFolderPath.TabIndex = 208;
            this.ux_labelTemplateFolderPath.Text = "Template folder path";
            // 
            // ux_textBoxVariableEndsWith
            // 
            this.ux_textBoxVariableEndsWith.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock;
            this.ux_textBoxVariableEndsWith.Enabled = false;
            this.ux_textBoxVariableEndsWith.Location = new System.Drawing.Point(403, 47);
            this.ux_textBoxVariableEndsWith.MaxLength = 3;
            this.ux_textBoxVariableEndsWith.Name = "ux_textBoxVariableEndsWith";
            this.ux_textBoxVariableEndsWith.Size = new System.Drawing.Size(50, 22);
            this.ux_textBoxVariableEndsWith.TabIndex = 207;
            this.ux_textBoxVariableEndsWith.Tag = "description";
            this.ux_textBoxVariableEndsWith.Visible = false;
            // 
            // ux_labelVariableEndsWith
            // 
            this.ux_labelVariableEndsWith.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock;
            this.ux_labelVariableEndsWith.AutoSize = true;
            this.ux_labelVariableEndsWith.Location = new System.Drawing.Point(264, 50);
            this.ux_labelVariableEndsWith.Name = "ux_labelVariableEndsWith";
            this.ux_labelVariableEndsWith.Size = new System.Drawing.Size(102, 13);
            this.ux_labelVariableEndsWith.TabIndex = 206;
            this.ux_labelVariableEndsWith.Text = "Variable ends with";
            this.ux_labelVariableEndsWith.Visible = false;
            // 
            // ux_textBoxVariableStartsWith
            // 
            this.ux_textBoxVariableStartsWith.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock;
            this.ux_textBoxVariableStartsWith.Enabled = false;
            this.ux_textBoxVariableStartsWith.Location = new System.Drawing.Point(151, 47);
            this.ux_textBoxVariableStartsWith.MaxLength = 3;
            this.ux_textBoxVariableStartsWith.Name = "ux_textBoxVariableStartsWith";
            this.ux_textBoxVariableStartsWith.Size = new System.Drawing.Size(50, 22);
            this.ux_textBoxVariableStartsWith.TabIndex = 205;
            this.ux_textBoxVariableStartsWith.Tag = "description";
            // 
            // ux_labelVariableStartsWith
            // 
            this.ux_labelVariableStartsWith.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock;
            this.ux_labelVariableStartsWith.AutoSize = true;
            this.ux_labelVariableStartsWith.Location = new System.Drawing.Point(12, 50);
            this.ux_labelVariableStartsWith.Name = "ux_labelVariableStartsWith";
            this.ux_labelVariableStartsWith.Size = new System.Drawing.Size(97, 13);
            this.ux_labelVariableStartsWith.TabIndex = 204;
            this.ux_labelVariableStartsWith.Text = "Variable indicator";
            // 
            // ux_textBoxAppSettingsName
            // 
            this.ux_textBoxAppSettingsName.Location = new System.Drawing.Point(151, 15);
            this.ux_textBoxAppSettingsName.Name = "ux_textBoxAppSettingsName";
            this.ux_textBoxAppSettingsName.ReadOnly = true;
            this.ux_textBoxAppSettingsName.Size = new System.Drawing.Size(259, 22);
            this.ux_textBoxAppSettingsName.TabIndex = 203;
            this.ux_textBoxAppSettingsName.Tag = "description";
            // 
            // Ux_labelAppSettings
            // 
            this.Ux_labelAppSettings.AutoSize = true;
            this.Ux_labelAppSettings.Location = new System.Drawing.Point(12, 18);
            this.Ux_labelAppSettings.Name = "Ux_labelAppSettings";
            this.Ux_labelAppSettings.Size = new System.Drawing.Size(103, 13);
            this.Ux_labelAppSettings.TabIndex = 0;
            this.Ux_labelAppSettings.Text = "App settings name";
            // 
            // DataviewPage
            // 
            this.DataviewPage.Controls.Add(this.ux_dataGridViewDataview);
            this.DataviewPage.Location = new System.Drawing.Point(4, 22);
            this.DataviewPage.Name = "DataviewPage";
            this.DataviewPage.Padding = new System.Windows.Forms.Padding(3);
            this.DataviewPage.Size = new System.Drawing.Size(659, 541);
            this.DataviewPage.TabIndex = 3;
            this.DataviewPage.Text = "Dataview";
            this.DataviewPage.UseVisualStyleBackColor = true;
            // 
            // ux_dataGridViewDataview
            // 
            this.ux_dataGridViewDataview.AllowUserToAddRows = false;
            this.ux_dataGridViewDataview.AllowUserToDeleteRows = false;
            this.ux_dataGridViewDataview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_dataGridViewDataview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_dataGridViewDataview.Location = new System.Drawing.Point(8, 6);
            this.ux_dataGridViewDataview.Name = "ux_dataGridViewDataview";
            this.ux_dataGridViewDataview.ReadOnly = true;
            this.ux_dataGridViewDataview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ux_dataGridViewDataview.Size = new System.Drawing.Size(667, 522);
            this.ux_dataGridViewDataview.TabIndex = 0;
            this.ux_dataGridViewDataview.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ux_datagridview_CellFormatting);
            this.ux_dataGridViewDataview.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.ux_datagridview_CellParsing);
            this.ux_dataGridViewDataview.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ux_datagridview_DataError);
            // 
            // PrintWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 594);
            this.Controls.Add(this.ux_tabcontrolMain);
            this.Controls.Add(this.ux_bindingnavigatorForm);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(640, 39);
            this.Name = "PrintWizard";
            this.Text = "Print Wizard";
            this.Load += new System.EventHandler(this.PrintWizard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ux_bindingnavigatorForm)).EndInit();
            this.ux_bindingnavigatorForm.ResumeLayout(false);
            this.ux_bindingnavigatorForm.PerformLayout();
            this.ux_tabcontrolMain.ResumeLayout(false);
            this.PrintingPage.ResumeLayout(false);
            this.ux_groupBoxLabel.ResumeLayout(false);
            this.ux_groupBoxLabel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownLabelsPerRecord)).EndInit();
            this.ux_groupBoxPrinter.ResumeLayout(false);
            this.ux_groupBoxPrinter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownPaperHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownPaperWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownHorizontalCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownVerticalGap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownHorizontalGap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownMarginTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownMarginLeft)).EndInit();
            this.ux_groupboxPreview.ResumeLayout(false);
            this.ux_groupboxPreview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownLabelHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericUpDownLabelWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pictureBoxPreview)).EndInit();
            this.LabelSettingsPage.ResumeLayout(false);
            this.LabelSettingsPage.PerformLayout();
            this.ux_groupBoxTemplateMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_dataGridViewLabelSettings)).EndInit();
            this.DataviewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_dataGridViewDataview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator ux_bindingnavigatorForm;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorPrintAllButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton bindingNavigatorPrintButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorPreviewButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripProgressBar bindingNavigatorProgressBar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorEntityId;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.TabControl ux_tabcontrolMain;
        private System.Windows.Forms.TabPage PrintingPage;
        private System.Windows.Forms.TabPage LabelSettingsPage;
        private System.Windows.Forms.GroupBox ux_groupboxPreview;
        private System.Windows.Forms.Label ux_labelLabelTemplate;
        private System.Windows.Forms.TextBox ux_textboxPrinter;
        private System.Windows.Forms.TextBox ux_textboxLabelTemplate;
        private System.Windows.Forms.Label ux_labelPrinter;
        private System.Windows.Forms.Label ux_labelLabelsPerRecord;
        private System.Windows.Forms.PictureBox ux_pictureBoxPreview;
        private System.Windows.Forms.TextBox ux_textboxDataview;
        private System.Windows.Forms.Label ux_labelDataview;
        private System.Windows.Forms.Label ux_labelTemplatePrintDensity;
        private System.Windows.Forms.ComboBox ux_comboBoxTemplatePrintDensity;
        private System.Windows.Forms.Label ux_labelSize;
        private System.Windows.Forms.Button ux_buttonRedraw;
        private System.Windows.Forms.ComboBox ux_comboBoxSizeUnit;
        private System.Windows.Forms.GroupBox ux_groupBoxPrinter;
        private System.Windows.Forms.Label ux_labelHorizontalCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox ux_groupBoxLabel;
        private System.Windows.Forms.ComboBox ux_comboBoxPrintResolution;
        private System.Windows.Forms.Label ux_labelPrintResolution;
        private System.Windows.Forms.Label ux_labelHorizontalGap;
        private System.Windows.Forms.ComboBox ux_comboBoxPaperSize;
        private System.Windows.Forms.Label ux_labelCustomPaperSize;
        private System.Windows.Forms.Label ux_labelMarginTop;
        private System.Windows.Forms.Label ux_labelMarginLeft;
        private System.Windows.Forms.Label ux_labelVerticalGap;
        private System.Windows.Forms.ComboBox ux_comboBoxUnit;
        private System.Windows.Forms.Label ux_labelPageUnit;
        private System.Windows.Forms.Label Ux_labelAppSettings;
        private System.Windows.Forms.TextBox ux_textBoxAppSettingsName;
        private System.Windows.Forms.TextBox ux_textBoxVariableStartsWith;
        private System.Windows.Forms.Label ux_labelVariableStartsWith;
        private System.Windows.Forms.TextBox ux_textBoxVariableEndsWith;
        private System.Windows.Forms.Label ux_labelVariableEndsWith;
        private System.Windows.Forms.TextBox ux_textBoxTemplateFolderPath;
        private System.Windows.Forms.Label ux_labelTemplateFolderPath;
        private System.Windows.Forms.Button ux_buttonSaveLabelSettings;
        private System.Windows.Forms.GroupBox ux_groupBoxTemplateMap;
        private System.Windows.Forms.DataGridView ux_dataGridViewLabelSettings;
        private System.Windows.Forms.Button ux_buttonNewTemplateMap;
        private System.Windows.Forms.Button ux_buttonRefresh;
        private System.Windows.Forms.TabPage DataviewPage;
        private System.Windows.Forms.DataGridView ux_dataGridViewDataview;
        private System.Windows.Forms.TextBox ux_textBoxTemplateExtension;
        private System.Windows.Forms.Label ux_labelTemplateExtension;
        private System.Windows.Forms.NumericUpDown ux_numericUpDownLabelHeight;
        private System.Windows.Forms.NumericUpDown ux_numericUpDownLabelWidth;
        private System.Windows.Forms.NumericUpDown ux_numericUpDownMarginTop;
        private System.Windows.Forms.NumericUpDown ux_numericUpDownMarginLeft;
        private System.Windows.Forms.NumericUpDown ux_numericUpDownVerticalGap;
        private System.Windows.Forms.NumericUpDown ux_numericUpDownHorizontalGap;
        private System.Windows.Forms.NumericUpDown ux_numericUpDownHorizontalCount;
        private System.Windows.Forms.NumericUpDown ux_numericUpDownLabelsPerRecord;
        private System.Windows.Forms.NumericUpDown ux_numericUpDownPaperHeight;
        private System.Windows.Forms.NumericUpDown ux_numericUpDownPaperWidth;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAboutButton;
        private System.Windows.Forms.Button ux_buttonSaveAsUserSettings;
    }
}