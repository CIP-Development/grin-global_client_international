﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace PrintWizard
{
    static class WizardAppSettings
    {
        public static string GetAppSettingValue(string name)
        {
            string value = string.Empty;

            string usersAppSettingsFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\AppSettings.txt";
            string allUsersAppSettingsFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GRIN-Global\Curator Tool\AppSettings.txt";
            string appSettingsFilePath = "";

            // Look for (and process) a AppSettings.txt file to allow custom application settings entries...
            // But first make sure the roaming profile directory exists...
            if (!System.IO.Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool")) System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool");

            // Now check the user's personal roaming profile for a copy of the AppSetting.txt file...
            if (System.IO.File.Exists(usersAppSettingsFilePath))
            {
                // Found a personal user's copy of the AppSettings.txt file - so use it...
                appSettingsFilePath = usersAppSettingsFilePath;
            }
            else if (System.IO.File.Exists(allUsersAppSettingsFilePath))
            {
                // Couldn't find a copy of the AppSettings.txt file so copy the master into the user's personal roaming profile AppData directory...
                System.IO.File.Copy(allUsersAppSettingsFilePath, usersAppSettingsFilePath);
                appSettingsFilePath = usersAppSettingsFilePath;
            }

            // Process the AppSettings.txt file...
            if (!string.IsNullOrEmpty(appSettingsFilePath))
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(appSettingsFilePath);
                while (!sr.EndOfStream)
                {
                    string localAppSetting = sr.ReadLine();
                    if (!string.IsNullOrEmpty(localAppSetting.Trim()) && !localAppSetting.Trim().StartsWith("#"))
                    {
                        var EqualsIndex = localAppSetting.IndexOf("=");
                        if (EqualsIndex > -1 && localAppSetting.Substring(0, EqualsIndex).Trim().Equals(name))
                        {
                            value = localAppSetting.Substring(EqualsIndex + 1, localAppSetting.Length - EqualsIndex - 1);
                            break;
                        }
                    }
                }
                sr.Close();
                sr.Dispose();
            }

            return value;
        }

        public static void SetAppSettingValue(string name, string value)
        {
            string usersAppSettingsFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\AppSettings.txt";
            string allUsersAppSettingsFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GRIN-Global\Curator Tool\AppSettings.txt";
            string appSettingsFilePath = "";

            // Look for (and process) a AppSettings.txt file to allow custom application settings entries...
            // But first make sure the roaming profile directory exists...
            if (!System.IO.Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool")) System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool");

            // Now check the user's personal roaming profile for a copy of the AppSetting.txt file...
            if (System.IO.File.Exists(usersAppSettingsFilePath))
            {
                // Found a personal user's copy of the AppSettings.txt file - so use it...
                appSettingsFilePath = usersAppSettingsFilePath;
            }
            else if (System.IO.File.Exists(allUsersAppSettingsFilePath))
            {
                // Couldn't find a copy of the AppSettings.txt file so copy the master into the user's personal roaming profile AppData directory...
                System.IO.File.Copy(allUsersAppSettingsFilePath, usersAppSettingsFilePath);
                appSettingsFilePath = usersAppSettingsFilePath;
            }

            if (!string.IsNullOrEmpty(appSettingsFilePath))
            {
                // Read the old file.
                string[] lines = File.ReadAllLines(appSettingsFilePath);
                try
                {
                    // Write the new file over the old file.
                    using (StreamWriter sw = new StreamWriter(appSettingsFilePath))
                    {
                        bool found = false;
                        foreach (var line in lines)
                        {
                            if (!string.IsNullOrEmpty(line.Trim()) && !line.Trim().StartsWith("#")
                                && line.IndexOf("=") > -1 && line.Substring(0, line.IndexOf("=")).Trim().Equals(name))
                            {
                                sw.WriteLine(name + " = " + value);
                                found = true;
                            }
                            else
                            {
                                sw.WriteLine(line);
                            }
                        }
                        if (!found)
                        {
                            sw.WriteLine(name + " = " + value);
                        }
                    }
                }
                catch (Exception)
                {
                    //restore original file
                    File.WriteAllLines(appSettingsFilePath, lines);
                    throw;
                }
            }
        }

        public static void GetReportMapping(DataTable dtTemplates, string extension)
        {
            dtTemplates.Clear();

            string usersReportsMappingFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\ReportsMapping.txt";
            string allUsersReportsMappingFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GRIN-Global\Curator Tool\ReportsMapping.txt";
            string reportsMappingFilePath = "";

            Dictionary<string, string> reportsMapping = new Dictionary<string, string>();

            // Look for (and process) a ReportsMapping.txt file to allow custom application settings entries...
            // But first make sure the roaming profile directory exists...
            if (!System.IO.Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool")) System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool");

            // Now check the user's personal roaming profile for a copy of the ReportsMapping.txt file...
            if (System.IO.File.Exists(usersReportsMappingFilePath))
            {
                // Found a personal user's copy of the ReportsMapping.txt file - so use it...
                reportsMappingFilePath = usersReportsMappingFilePath;
            }
            else if (System.IO.File.Exists(allUsersReportsMappingFilePath))
            {
                // Couldn't find a copy of the ReportsMapping.txt file so copy the master into the user's personal roaming profile AppData directory...
                System.IO.File.Copy(allUsersReportsMappingFilePath, usersReportsMappingFilePath);
                reportsMappingFilePath = usersReportsMappingFilePath;
            }

            // Process the ReportsMapping.txt file...
            if (!string.IsNullOrEmpty(reportsMappingFilePath))
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(reportsMappingFilePath);
                while (!sr.EndOfStream)
                {
                    string reportMap = sr.ReadLine().Trim()/*.ToUpper()*/;
                    if (!string.IsNullOrEmpty(reportMap.Trim()) &&
                        !reportMap.Trim().StartsWith("#"))
                    {
                        string[] keyValuePair = reportMap.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                        if (keyValuePair != null)
                        {
                            // Force the key to trim...
                            keyValuePair[0] = keyValuePair[0].Trim();
                            if (keyValuePair.Length >= 2)
                            {
                                // Working off the assumption that the first '=' sign found is the delimiter for the key/value pair so concat all fields after the first '=' sign...
                                for (int i = 2; i < keyValuePair.Length; i++)
                                {
                                    keyValuePair[1] += "=" + keyValuePair[i];
                                }

                                // Only load reports/templates with template extension 
                                if (Path.GetExtension(keyValuePair[0]).Equals(extension))
                                {
                                    var drTemplate = dtTemplates.NewRow();
                                    drTemplate["template_name"] = keyValuePair[0];
                                    drTemplate["dataviews"] = keyValuePair[1].Trim();
                                    dtTemplates.Rows.Add(drTemplate);
                                }
                            }
                            else if (keyValuePair.Length < 2)
                            {
                                continue;
                            }
                        }
                    }
                }
                sr.Close();
                sr.Dispose();
            }
        }

        public static void SaveReportMapping(DataTable dtTemplatesChanges)
        {
            string usersReportsMappingFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\ReportsMapping.txt";
            string allUsersReportsMappingFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GRIN-Global\Curator Tool\ReportsMapping.txt";
            string reportsMappingFilePath = "";

            Dictionary<string, string> reportsMapping = new Dictionary<string, string>();

            // Look for (and process) a ReportsMapping.txt file to allow custom application settings entries...
            // But first make sure the roaming profile directory exists...
            if (!System.IO.Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool")) System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool");

            // Now check the user's personal roaming profile for a copy of the ReportsMapping.txt file...
            if (System.IO.File.Exists(usersReportsMappingFilePath))
            {
                // Found a personal user's copy of the ReportsMapping.txt file - so use it...
                reportsMappingFilePath = usersReportsMappingFilePath;
            }
            else if (System.IO.File.Exists(allUsersReportsMappingFilePath))
            {
                // Couldn't find a copy of the ReportsMapping.txt file so copy the master into the user's personal roaming profile AppData directory...
                System.IO.File.Copy(allUsersReportsMappingFilePath, usersReportsMappingFilePath);
                reportsMappingFilePath = usersReportsMappingFilePath;
            }

            // Process the ReportsMapping.txt file...
            if (!string.IsNullOrEmpty(reportsMappingFilePath))
            {
                // Read the old file
                string[] lines = File.ReadAllLines(reportsMappingFilePath);
                try
                {
                    // Write updates over the old file.
                    using (StreamWriter sw = new StreamWriter(reportsMappingFilePath))
                    {
                        foreach (var line in lines)
                        {
                            DataRowState drState = DataRowState.Unchanged;
                            if (!string.IsNullOrEmpty(line.Trim()) && !line.Trim().StartsWith("#")
                                && line.IndexOf("=") > -1)
                            {
                                var templateName = line.Substring(0, line.IndexOf("=")).Trim();
                                var dr = dtTemplatesChanges.AsEnumerable().FirstOrDefault(x => (x.RowState != DataRowState.Deleted && x.RowState != DataRowState.Detached)
                                                                            && x.Field<string>("template_name").ToUpperInvariant().Equals(templateName.ToUpperInvariant()));
                                if (dr != null) //Added or Modified
                                {
                                    drState = dr.RowState;
                                    sw.WriteLine(templateName + " = " + (dr.IsNull("dataviews") ? string.Empty : dr.Field<string>("dataviews").Trim()));
                                    dr.AcceptChanges();
                                }
                                else
                                {
                                    dr = dtTemplatesChanges.AsEnumerable().FirstOrDefault(x => (x.RowState == DataRowState.Deleted)
                                                                        && x.Field<string>("template_name", DataRowVersion.Original).ToUpperInvariant().Equals(templateName.ToUpperInvariant()));
                                    if (dr != null) //Deleted
                                    {
                                        drState = dr.RowState;
                                        dr.AcceptChanges();
                                    }
                                }
                            }
                            if (drState == DataRowState.Unchanged)
                                sw.WriteLine(line);
                        }

                        foreach (DataRow drAdded in dtTemplatesChanges.Rows)
                        {
                            if ((drAdded.RowState == DataRowState.Added || drAdded.RowState == DataRowState.Modified)
                                && !string.IsNullOrEmpty(drAdded.Field<string>("template_name").Trim()))
                                sw.WriteLine(drAdded.Field<string>("template_name").Trim() + " = " + (drAdded.IsNull("dataviews") ? string.Empty : drAdded.Field<string>("dataviews").Trim()));
                        }
                    }
                }
                catch (Exception)
                {
                    //restore original file
                    File.WriteAllLines(reportsMappingFilePath, lines);
                    throw;
                }
            }
        }
    }
}
