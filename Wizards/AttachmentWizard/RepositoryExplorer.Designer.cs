﻿namespace AttachmentWizard
{
    partial class RepositoryExplorer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_datagridviewExplorer = new System.Windows.Forms.DataGridView();
            this.ux_buttonCancel = new System.Windows.Forms.Button();
            this.ux_buttonChoose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewExplorer)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_datagridviewExplorer
            // 
            this.ux_datagridviewExplorer.AllowUserToAddRows = false;
            this.ux_datagridviewExplorer.AllowUserToDeleteRows = false;
            this.ux_datagridviewExplorer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_datagridviewExplorer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewExplorer.Location = new System.Drawing.Point(12, 12);
            this.ux_datagridviewExplorer.Name = "ux_datagridviewExplorer";
            this.ux_datagridviewExplorer.ReadOnly = true;
            this.ux_datagridviewExplorer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ux_datagridviewExplorer.Size = new System.Drawing.Size(456, 325);
            this.ux_datagridviewExplorer.TabIndex = 0;
            // 
            // ux_buttonCancel
            // 
            this.ux_buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCancel.Location = new System.Drawing.Point(393, 347);
            this.ux_buttonCancel.Name = "ux_buttonCancel";
            this.ux_buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonCancel.TabIndex = 1;
            this.ux_buttonCancel.Text = "Cancel";
            this.ux_buttonCancel.UseVisualStyleBackColor = true;
            this.ux_buttonCancel.Click += new System.EventHandler(this.ux_buttonCancel_Click);
            // 
            // ux_buttonChoose
            // 
            this.ux_buttonChoose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonChoose.Location = new System.Drawing.Point(295, 347);
            this.ux_buttonChoose.Name = "ux_buttonChoose";
            this.ux_buttonChoose.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonChoose.TabIndex = 2;
            this.ux_buttonChoose.Text = "Choose";
            this.ux_buttonChoose.UseVisualStyleBackColor = true;
            this.ux_buttonChoose.Click += new System.EventHandler(this.ux_buttonChoose_Click);
            // 
            // RepositoryExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 380);
            this.Controls.Add(this.ux_buttonChoose);
            this.Controls.Add(this.ux_buttonCancel);
            this.Controls.Add(this.ux_datagridviewExplorer);
            this.Name = "RepositoryExplorer";
            this.Text = "Repository Explorer";
            this.Load += new System.EventHandler(this.RepositoryExplorer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewExplorer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView ux_datagridviewExplorer;
        private System.Windows.Forms.Button ux_buttonCancel;
        private System.Windows.Forms.Button ux_buttonChoose;
    }
}