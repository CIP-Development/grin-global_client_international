﻿using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace AttachmentWizard
{
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    public partial class AttachmentWizard : Form, IGRINGlobalDataWizard
    {
        SharedUtils _sharedUtils;
        string _originalPKeys = string.Empty;
        string _accessionPKeys = string.Empty;
        string _inventoryPKeys = string.Empty;
        List<int> _inventoryList = new List<int>();

        DataSet dsAttachmentList = null;

        public AttachmentWizard()
        {
            InitializeComponent();
        }
        public string FormName
        {
            get
            {
                return "Attachment Wizard";
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }

        public string PKeyName
        {
            get
            {
                return "accession_id";
            }
        }

        public AttachmentWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();
            
            _sharedUtils = sharedUtils;
            _originalPKeys = pKeys;
            
            foreach (string pkeyToken in pKeys.Split(';'))
            {
                switch (pkeyToken.Split('=')[0].Trim().ToUpper()) {
                    case ":ACCESSIONID":
                        _accessionPKeys = pkeyToken;
                        break;
                    case ":INVENTORYID":
                        _inventoryPKeys = pkeyToken;
                        break;
                }
            }
        }

        private void AttachmentWizard_Load(object sender, EventArgs e)
        {
            // Get language translations
            if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);

            if ((_inventoryPKeys == " :inventoryid=") || (_inventoryPKeys == " :accessionid=")) {
                MessageBox.Show(this, "To view attachments please select a dataview base on accessions or inventories", "Attachment Wizard", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        
        
            DataSet dsInventoryList = _sharedUtils.GetWebServiceData("attachment_wizard_get_inventory_id", _accessionPKeys, 0, 0);
            if (dsInventoryList != null && dsInventoryList.Tables.Contains("attachment_wizard_get_inventory_id"))
            {
                foreach (DataRow r in dsInventoryList.Tables["attachment_wizard_get_inventory_id"].Rows)
                {
                    _inventoryList.Add(r.Field<int>("inventory_id"));
                }
            }
            if (!string.IsNullOrEmpty(_inventoryPKeys)) {
                string ids = _inventoryPKeys.Split('=')[1];
                if (!string.IsNullOrEmpty(ids))
                {
                    foreach (var inventory_id in _inventoryPKeys.Split('=')[1].Split(','))
                    {
                        _inventoryList.Add(int.Parse(inventory_id));
                    }
                }
            }


            string[] hide = new string[]{"accession_inv_attach_id",
                "sort_order","content_type","attach_date_code","is_web_visible","attach_date","attach_cooperator_id",
                "created_by","modified_by","owned_by","owned_date"};
            dsAttachmentList = _sharedUtils.GetWebServiceData("attachment_wizard_get_accession_inv_attach", "" + _inventoryPKeys + ";" + _accessionPKeys + ";", 0, 0);
            if (dsAttachmentList != null && dsAttachmentList.Tables.Contains("attachment_wizard_get_accession_inv_attach"))
            {
                ux_datagridviewAttachmentsByInventory.DataSource = dsAttachmentList.Tables["attachment_wizard_get_accession_inv_attach"];
                foreach (var colname in hide)
                {
                    if (ux_datagridviewAttachmentsByInventory.Columns.Contains(colname))
                        ux_datagridviewAttachmentsByInventory.Columns[colname].Visible = false;
                }
                ux_datagridviewAttachmentsByInventory.AutoResizeColumns();
            }
        }

        private void ux_buttonOpen_Click(object sender, EventArgs e)
        {
            if (ux_datagridviewAttachmentsByInventory.SelectedRows.Count == 0) return;

            if (!ux_datagridviewAttachmentsByInventory.SelectedRows[0].Cells["category_code"].Value.ToString().Equals("LINK"))
            {
                string virtual_path = ux_datagridviewAttachmentsByInventory.SelectedRows[0].Cells["virtual_path"].Value.ToString().Replace("~", "");
                string TempPath = Path.GetTempPath();
                string fileName = Path.GetFileName(@"C:\" + virtual_path);

                string fullpath = TempPath + fileName;
                string serverPath = _sharedUtils.Url.Replace("GUI.asmx", "");
                string urlpath = serverPath + virtual_path;

                try
                {
                    using (var client = new WebClient())
                    {
                        client.DownloadFile(urlpath, fullpath);
                    }
                    Process cmd = new Process();
                    cmd.StartInfo.FileName = "cmd.exe";
                    cmd.StartInfo.RedirectStandardInput = true;
                    cmd.StartInfo.RedirectStandardOutput = true;
                    cmd.StartInfo.CreateNoWindow = true;
                    cmd.StartInfo.UseShellExecute = false;
                    cmd.Start();

                    cmd.StandardInput.WriteLine(fullpath);
                    cmd.StandardInput.Flush();
                    cmd.StandardInput.Close();
                    cmd.WaitForExit();
                    //MessageBox.Show(cmd.StandardOutput.ReadToEnd());
                }
                catch (Exception eAttach)
                {
                    MessageBox.Show(eAttach.Message);
                }
            }
            else {
                string virtual_path = ux_datagridviewAttachmentsByInventory.SelectedRows[0].Cells["virtual_path"].Value.ToString();
                try
                {
                    Process cmd = new Process();
                    cmd.StartInfo.FileName = "cmd.exe";
                    cmd.StartInfo.RedirectStandardInput = true;
                    cmd.StartInfo.RedirectStandardOutput = true;
                    cmd.StartInfo.CreateNoWindow = true;
                    cmd.StartInfo.UseShellExecute = false;
                    cmd.Start();

                    cmd.StandardInput.WriteLine("explorer " + virtual_path);
                    cmd.StandardInput.Flush();
                    cmd.StandardInput.Close();
                    cmd.WaitForExit();
                    //MessageBox.Show(cmd.StandardOutput.ReadToEnd());
                }
                catch (Exception eAttach)
                {
                    MessageBox.Show(eAttach.Message);
                }
            }
        }

        private void ux_buttonSelectFromPC_Click(object sender, EventArgs e)
        {
            DataSet dsExtensionWhitelist = _sharedUtils.GetWebServiceData("attachment_wizard_get_whitelist", "", 0, 0);
            DataTable dtExtensionWhitelist = null;
            if (dsExtensionWhitelist != null && dsExtensionWhitelist.Tables.Contains("attachment_wizard_get_whitelist"))
            {
                dtExtensionWhitelist = dsExtensionWhitelist.Tables["attachment_wizard_get_whitelist"];
            }

            string filter = string.Empty;

            if (dtExtensionWhitelist != null)
            {
                var result = dtExtensionWhitelist.AsEnumerable()
                .GroupBy(x => new
                {
                    category_code = x.Field<string>("category_code")
                })
                .Select(x => new
                {
                    category_code = x.Key.category_code,
                    extensions = String.Join(";", x.Select(z => "*." + z.Field<string>("extension")))
                });

                foreach (var row in result)
                {
                    filter += "|" + row.category_code + "|" + row.extensions;
                }
                if (filter.StartsWith("|")) filter = filter.Remove(0, 1);
            }
            if (string.IsNullOrEmpty(filter)) filter = "All|*.*";

            OpenFileDialog openFileDialogFromPC = new OpenFileDialog();
            openFileDialogFromPC.Filter = filter;
            openFileDialogFromPC.Title = "Select a file";
            openFileDialogFromPC.Multiselect = true;
            if (openFileDialogFromPC.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                foreach (var filename in openFileDialogFromPC.FileNames)
                {
                    bool exist = false;
                    foreach (DataGridViewRow row in ux_datagridviewNewAttachment.Rows)
                    {
                        if (row.Cells["file_physical_path"].Value.ToString().Equals(filename)) {
                            exist = true;
                            break;
                        }   
                    }
                    if (!exist)
                    {
                        FileInfo fi = new FileInfo(filename);
                        string ext = Path.GetExtension(filename).Replace(".","");
                        var category_code = dtExtensionWhitelist.Select("extension = '" + ext + "'");
                        /*
                        string hash = string.Empty;
                        
                        if (fi.Length <= 5242880)
                        {
                            using (var md5 = MD5.Create())
                            {
                                using (var stream = File.OpenRead(filename))
                                {
                                    hash = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "");
                                }
                            }
                        }
                        */
                        ux_datagridviewNewAttachment.Rows.Add("PC", (!category_code.Any() ? "FILE" : category_code.First().Field<string>("category_code"))
                                                                , Path.GetFileNameWithoutExtension(filename), ""
                                                                , filename, "", Math.Round((fi.Length / 1024.0), 2) + " KB", "", "Y","");
                        if (fi.Length > 5242880) {
                            ux_datagridviewNewAttachment.Rows[ux_datagridviewNewAttachment.Rows.Count - 1].ErrorText = "Maximum file size permitted is 5MB";
                        }
                    }
                }

                ux_progressbarLoading.Visible = false;
                ux_datagridviewNewAttachment.AutoResizeColumns();
                
                for (int i = ux_datagridviewNewAttachment.Rows.Count - 1; i >= 0; i--)
                {
                    //FileInfo fi = new FileInfo(ux_datagridviewNewAttachment.Rows[i].Cells["file_physical_path"].Value.ToString());
                    if (!string.IsNullOrEmpty(ux_datagridviewNewAttachment.Rows[i].ErrorText))
                    {
                        MessageBox.Show(this, "Error '" + ux_datagridviewNewAttachment.Rows[i].Cells["file_physical_path"].Value + "' \nFile size must be less than 5MB");
                        ux_datagridviewNewAttachment.Rows.RemoveAt(i);
                    }
                }
            }
        }

        private void ux_buttonAttachAll_Click(object sender, EventArgs e)
        {
            //ux_datagridviewNewAttachment.ReadOnly = true;
            if (ux_datagridviewNewAttachment.Rows.Count == 0) return;
            
            DataSet dsAccessionInvAttach = _sharedUtils.GetWebServiceData("attachment_wizard_get_accession_inv_attach", ":accessionid; :inventoryid;", 0, 0);
            DataTable dtAccessionInvAttach = null;
            if (dsAccessionInvAttach != null && dsAccessionInvAttach.Tables.Contains("attachment_wizard_get_accession_inv_attach"))
            {
                dtAccessionInvAttach = dsAccessionInvAttach.Tables["attachment_wizard_get_accession_inv_attach"];
                
            }else
            {
                MessageBox.Show("GetWebServiceData error");
                return;
            }

            ux_progressbarLoading.Minimum = 0;
            ux_progressbarLoading.Maximum = ux_datagridviewNewAttachment.Rows.Count;
            ux_progressbarLoading.Step = 1;
            ux_progressbarLoading.Show();

            foreach (DataGridViewRow row in ux_datagridviewNewAttachment.Rows)
            {
                //Clean Row error
                row.ErrorText = string.Empty;

                //Check if file has to be uploaded
                if (row.Cells["file_source"].Value.ToString().Equals("PC") && string.IsNullOrEmpty(row.Cells["virtual_path"].Value.ToString()))
                {
                    string remotePath = string.Empty;
                    string file_physical_path = row.Cells["file_physical_path"].Value.ToString();
                    byte[] imageBytes = File.ReadAllBytes(file_physical_path);
                    
                    if (imageBytes != null)
                    {
                        string error = string.Empty;
                        try
                        {
                            remotePath = _sharedUtils.SaveAttachment("/uploads/images/" + Guid.NewGuid() + Path.GetExtension(file_physical_path), imageBytes, false, false);
                            // If the upload was successful the remotePath will contain the destination path (so now we can insert the record)...
                            if (!string.IsNullOrEmpty(remotePath))
                            {
                                row.Cells["virtual_path"].Value = remotePath;

                                foreach (var inventory_id in _inventoryList)
                                {
                                    DataRow n = dtAccessionInvAttach.NewRow();
                                    n["inventory_id"] = inventory_id;
                                    n["virtual_path"] = row.Cells["virtual_path"].Value;
                                    n["title"] = row.Cells["title"].Value;
                                    n["description"] = row.Cells["description"].Value;
                                    n["category_code"] = row.Cells["category_code"].Value;
                                    n["attach_cooperator_id"] = _sharedUtils.UserCooperatorID;
                                    n["is_web_visible"] = row.Cells["is_visible"].Value;
                                    n["attach_date_code"] = "MM/dd/yyyy";
                                    n["attach_date"] = DateTime.Now;
                                    n["note"] = row.Cells["note"].Value;
                                    n["owned_by"] = _sharedUtils.UserCooperatorID;
                                    dtAccessionInvAttach.Rows.Add(n);
                                }
                            }
                            else
                                error = "Error on SaveImage WebService";
                        }
                        catch (Exception eSaveImage) {
                            error = eSaveImage.Message;
                        }
                        if(!string.IsNullOrEmpty(error))
                            row.ErrorText = error;
                    }
                }
                else //--REPOSITORY OR EXTERNAL LINK
                {
                    DataTable dtAttachmentList = dsAttachmentList.Tables["attachment_wizard_get_accession_inv_attach"];
                    foreach (var inventory_id in _inventoryList)
                    {
                        DataRow[] drs = dtAttachmentList.Select("inventory_id = " + inventory_id + " and virtual_path = '" + row.Cells["virtual_path"].Value + "'");
                        if (drs != null && drs.Length == 0) {
                            DataRow n = dtAccessionInvAttach.NewRow();
                            n["inventory_id"] = inventory_id;
                            n["virtual_path"] = row.Cells["virtual_path"].Value;
                            n["title"] = row.Cells["title"].Value;
                            n["description"] = row.Cells["description"].Value;
                            n["category_code"] = row.Cells["category_code"].Value;
                            n["attach_cooperator_id"] = _sharedUtils.UserCooperatorID;
                            n["is_web_visible"] = "Y";
                            n["attach_date_code"] = "MM/dd/yyyy";
                            n["attach_date"] = DateTime.Now;
                            n["note"] = row.Cells["note"].Value;
                            dtAccessionInvAttach.Rows.Add(n);
                        }
                    }
                }
                ux_progressbarLoading.PerformStep();
            }
            ux_datagridviewNewAttachment.AutoResizeColumns();
            DataSet dsError = _sharedUtils.SaveWebServiceData(dsAccessionInvAttach);

            dsAttachmentList = _sharedUtils.GetWebServiceData("attachment_wizard_get_accession_inv_attach", "" + _inventoryPKeys + ";" + _accessionPKeys + ";", 0, 0);
            if (dsAttachmentList != null && dsAttachmentList.Tables.Contains("attachment_wizard_get_accession_inv_attach"))
            {
                ux_datagridviewAttachmentsByInventory.DataSource = dsAttachmentList.Tables["attachment_wizard_get_accession_inv_attach"];
                ux_datagridviewAttachmentsByInventory.AutoResizeColumns();
            }
        }
        
        private void uz_buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ux_buttonSelectFromRepository_Click(object sender, EventArgs e)
        {
            using (RepositoryExplorer re = new RepositoryExplorer(_sharedUtils)) {
                if (re.ShowDialog() == DialogResult.OK) {
                    foreach (DataGridViewRow filename in re.Filenames())
                    {
                        bool exist = false;
                        foreach (DataGridViewRow row in ux_datagridviewNewAttachment.Rows)
                        {
                            if (row.Cells["virtual_path"].Value.ToString().Equals(filename.Cells["virtual_path"].Value))
                            {
                                exist = true;
                                break;
                            }
                        }
                        if (!exist)
                        {
                            ux_datagridviewNewAttachment.Rows.Add("REPOSITORY", filename.Cells["category_code"].Value
                                , filename.Cells["title"].Value, filename.Cells["description"].Value
                                , "", filename.Cells["virtual_path"].Value, "", filename.Cells["content_type"].Value, "Y","");
                        }
                    }

                    ux_progressbarLoading.Visible = false;
                    ux_datagridviewNewAttachment.AutoResizeColumns();
                }
            }
        }

        private void ux_buttonDelete_Click(object sender, EventArgs e)
        {
            if (ux_datagridviewAttachmentsByInventory.SelectedRows.Count == 0) return;
            
            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("WARNING!!!  You are about to permanently delete {0} records from the central database!\n\nAre you sure you want to do this?", "Record Delete Confirmation", MessageBoxButtons.OKCancel, MessageBoxDefaultButton.Button2);
            ggMessageBox.Name = "UserInterfaceUtils_ProcessDGVEditShortcutKeysMessage1";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = ux_datagridviewAttachmentsByInventory.SelectedRows.Count.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (DialogResult.OK == ggMessageBox.ShowDialog())
            {
                foreach (DataGridViewRow dgvr in ux_datagridviewAttachmentsByInventory.SelectedRows)
                {
                    ux_datagridviewAttachmentsByInventory.Rows.Remove(dgvr);
                }
            }


            if (ux_datagridviewAttachmentsByInventory.DataSource != null)
            {
                int errorCount = 0;
                
                DataTable dtAttachment = (DataTable)ux_datagridviewAttachmentsByInventory.DataSource;
                DataSet accessionActionChanges = new DataSet();
                DataSet accessionActionSaveResults = new DataSet();
                //dtAttachment.AcceptChanges();
                if (dtAttachment.GetChanges() != null)
                {
                    // Before saving the results to the remote server check to see if any new rows in accession_action have
                    // a FK related to a new row in the accessions table (pkey < 0).  If so get the new pkey returned from
                    // the get_accession save and update the records in accession_action...
                   /* DataRow[] accessionActionRowsWithNewParent = _accessionAction.Select("accession_id<0");
                    foreach (DataRow dr in accessionActionRowsWithNewParent)
                    {
                        DataRow[] newParent = accessionSaveResults.Tables["get_accession"].Select("OriginalPrimaryKeyID=" + dr["accession_id"].ToString());
                        if (newParent != null && newParent.Length > 0)
                        {
                            dr["accession_id"] = newParent[0]["NewPrimaryKeyID"];
                        }
                    }*/
                    accessionActionChanges.Tables.Add(dtAttachment.GetChanges());
                    //ScrubData(accessionActionChanges);
                    // Save the changes to the remote server...
                    accessionActionSaveResults = _sharedUtils.SaveWebServiceData(accessionActionChanges);
                    // Sync the saved results with the original table (to give user feedback about results of save)...
                    if (accessionActionSaveResults.Tables.Contains(dtAttachment.TableName))
                    {
                        errorCount += SyncSavedResults(dtAttachment, accessionActionSaveResults.Tables[dtAttachment.TableName]);
                        if (errorCount == 0)
                        {
                            MessageBox.Show(this, "All data was saved successfully", "Attachment Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show(this, "The data being saved has errors that should be reviewed.\n\n  Error Count: " + errorCount, "Attachment Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        //RefreshAccessionData();
                        //// Update the row error message for this accession row...
                        //ux_labelAccessionRowError.Text = ((DataRowView)ux_datagridviewAttachmentsByInventory.Current).Row.RowError;
                        // Update the Accession Form (and child DataGridViews) data...
                        //_mainBindingSource_CurrentChanged(sender, e);
                    }
                }

            }
        }

        private void ux_buttonDownloadAll_Click(object sender, EventArgs e)
        {
            if (ux_datagridviewAttachmentsByInventory.SelectedRows.Count == 0) return;

            FolderBrowserDialog openFileDialogFromPC = new FolderBrowserDialog();
            if (openFileDialogFromPC.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                /*
                DataView view = new DataView((DataTable)ux_datagridviewAttachmentsByInventory.DataSource);
                DataTable distinctFiles = view.ToTable(true, "title", "virtual_path");
                */
                
                ux_progressbarAttachmentsByInventory.Minimum = 0;
                ux_progressbarAttachmentsByInventory.Maximum = ux_datagridviewAttachmentsByInventory.SelectedRows.Count;
                ux_progressbarAttachmentsByInventory.Step = 1;
                ux_progressbarAttachmentsByInventory.Show();

                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;

                foreach (DataGridViewRow dgvr in ux_datagridviewAttachmentsByInventory.SelectedRows)
                {

                    string selectedPath = openFileDialogFromPC.SelectedPath;
                    string fullpath = "";
                    string urlpath = "";
                    
                    string accession_inv_attach_id = dgvr.Cells["accession_inv_attach_id"].Value.ToString();
                    try
                    {
                        string filename = string.Empty;
                        string extension = string.Empty;
                        if (!dgvr.Cells["category_code"].Value.ToString().Equals("LINK"))
                        {
                            string virtual_path = dgvr.Cells["virtual_path"].Value.ToString().Replace("~", "");
                        
                            filename = dgvr.Cells["title"].Value.ToString();
                            extension = Path.GetExtension(@"C:\" + virtual_path);
                        
                            string serverPath = _sharedUtils.Url.Replace("GUI.asmx", "");
                            urlpath = serverPath + virtual_path;
                        }
                        else
                        {
                            urlpath = dgvr.Cells["virtual_path"].Value.ToString();
                            
                            Uri uri = new Uri(urlpath);
                            if (Path.HasExtension(uri.AbsoluteUri))
                                filename = Path.GetFileNameWithoutExtension(uri.LocalPath);
                            else 
                                throw new Exception("Url is not a file");
                            extension = Path.GetExtension(uri.LocalPath);
                            
                        }
                        fullpath = selectedPath + "\\" + filename + "-" + accession_inv_attach_id + extension;
                        int cont = 1;
                        while (File.Exists(fullpath))
                        {
                            fullpath = selectedPath + "\\" + filename + "-" + accession_inv_attach_id + "(" + cont++ + ")" + extension;
                        }

                        //Download File
                        using (var client = new WebClient())
                        {
                            client.DownloadFile(urlpath, fullpath);
                        }
                    }
                    catch (Exception eAttach)
                    {
                        MessageBox.Show(eAttach.Message);
                    }
                    
                    ux_progressbarAttachmentsByInventory.PerformStep();    
                }

                Cursor.Current = current;

                Process cmd = new Process();
                cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();

                cmd.StandardInput.WriteLine("explorer  \"" + openFileDialogFromPC.SelectedPath + "\"");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                //MessageBox.Show(cmd.StandardOutput.ReadToEnd());
            }
        }

        private int SyncSavedResults(DataTable originalTable, DataTable savedResults)
        {
            int errorCount = 0;

            if (savedResults != null && savedResults.PrimaryKey.Length == 1)
            {
                string pKeyCol = savedResults.PrimaryKey[0].ColumnName.Trim().ToUpper();
                savedResults.Columns[pKeyCol].ReadOnly = false;
                foreach (DataRow dr in savedResults.Rows)
                {
                    DataRow originalRow = originalTable.Rows.Find(dr["OriginalPrimaryKeyID"]);

                    switch (dr["SavedAction"].ToString())
                    {
                        case "Insert":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the originalTable row's status for this new row to committed (and update the pkey with the int returned from the server DB)...
                                if (originalRow != null)
                                {
                                    bool origColumnReadOnlyValue = originalRow.Table.Columns[pKeyCol].ReadOnly;
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = false;
                                    originalRow[pKeyCol] = dr["NewPrimaryKeyID"];
                                    originalRow.AcceptChanges();
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = origColumnReadOnlyValue;
                                    originalRow.ClearErrors();
                                }
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Update":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                originalRow.AcceptChanges();
                                originalRow.ClearErrors();
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Delete":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the row's status for this deleted row to committed...
                                // To do this you must first find the deleted row (NOTE: datatable.rows.find() method does not work on deleted rows)...
                                foreach (DataRow deletedRow in originalTable.Rows)
                                {
                                    if (deletedRow.RowState == DataRowState.Deleted &&
                                        deletedRow[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                    {
                                        originalRow = deletedRow;
                                    }
                                }
                                if (originalRow != null)
                                {
                                    originalRow.AcceptChanges();
                                    originalRow.ClearErrors();
                                }
                            }
                            else
                            {
                                errorCount++;
                                // Find the deleted row (NOTE: datatable.rows.find() method does not work on deleted rows)...
                                foreach (DataRow deletedRow in originalTable.Rows)
                                {
                                    if (deletedRow[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                    {
                                        deletedRow.RejectChanges();
                                        deletedRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            return errorCount;
        }

        private void ux_buttonAddLink_Click(object sender, EventArgs e)
        {
            string link = Prompt.ShowDialog("Link to external resource", "Attach link");
            if (!link.Equals("")) {
                ux_datagridviewNewAttachment.Rows.Add("EXTERNAL", "LINK", link, "", "", link, "", "", "Y", "");
                ux_datagridviewNewAttachment.AutoResizeColumns();
            }
        }
    }

    public static class Prompt
    {
        public static string ShowDialog(string text, string caption)
        {
            Form prompt = new Form()
            {
                Width = 460,
                Height = 140,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = caption,
                StartPosition = FormStartPosition.CenterScreen
            };
            Label textLabel = new Label() { Left = 20, Top = 20, Text = text };
            TextBox textBox = new TextBox() { Left = 20, Top = 40, Width = 400 };
            Button confirmation = new Button() { Text = "OK", Left = 320, Width = 100, Top = 65, DialogResult = DialogResult.OK };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(textBox);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.AcceptButton = confirmation;

            return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";
        }
    }
}
