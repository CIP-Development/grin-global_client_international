﻿using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttachmentWizard
{
    public partial class RepositoryExplorer : Form
    {
        SharedUtils _sharedUtils;
        List<DataGridViewRow> filenames = new List<DataGridViewRow>();


        public RepositoryExplorer(SharedUtils sharedUtils)
        {
            InitializeComponent();
            _sharedUtils = sharedUtils;
        }

        public List<DataGridViewRow> Filenames() { return filenames; }

        private void ux_buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void ux_buttonChoose_Click(object sender, EventArgs e)
        {
            if (ux_datagridviewExplorer.SelectedRows.Count > 0) {

                foreach (DataGridViewRow row in ux_datagridviewExplorer.SelectedRows)
                {
                    filenames.Add(row);
                }
                
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void RepositoryExplorer_Load(object sender, EventArgs e)
        {
            DataSet dsRepositoryExplorer = _sharedUtils.GetWebServiceData("attachment_wizard_files", "", 0, 0);
            if (dsRepositoryExplorer != null && dsRepositoryExplorer.Tables.Contains("attachment_wizard_files"))
            {
                ux_datagridviewExplorer.DataSource = dsRepositoryExplorer.Tables["attachment_wizard_files"];
                ux_datagridviewExplorer.AutoResizeColumns();
            }
        }
    }
}
