﻿namespace SearchWizard
{
    partial class DescriptorDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DescriptorDetailForm));
            this.ux_tableLayoutPanelDetails = new System.Windows.Forms.TableLayoutPanel();
            this.ux_labelMaximumLength = new System.Windows.Forms.Label();
            this.ux_labelDataType = new System.Windows.Forms.Label();
            this.ux_labelCategory = new System.Windows.Forms.Label();
            this.ux_labelCrop = new System.Windows.Forms.Label();
            this.ux_textBoxMaximumLength = new System.Windows.Forms.TextBox();
            this.ux_textBoxDataType = new System.Windows.Forms.TextBox();
            this.ux_textBoxCategory = new System.Windows.Forms.TextBox();
            this.ux_labelDefinition = new System.Windows.Forms.Label();
            this.ux_textBoxDefinition = new System.Windows.Forms.TextBox();
            this.ux_textBoxTraitName = new System.Windows.Forms.TextBox();
            this.ux_labelName = new System.Windows.Forms.Label();
            this.ux_labelTraitCodes = new System.Windows.Forms.Label();
            this.ux_textBoxTraitCodes = new System.Windows.Forms.TextBox();
            this.ux_labelOntologyUrl = new System.Windows.Forms.Label();
            this.ux_textBoxOntologyURL = new System.Windows.Forms.TextBox();
            this.ux_labelNumericMinimum = new System.Windows.Forms.Label();
            this.ux_labelNumericMaximum = new System.Windows.Forms.Label();
            this.ux_textBoxNumericMinimum = new System.Windows.Forms.TextBox();
            this.ux_textBoxNumericMaximum = new System.Windows.Forms.TextBox();
            this.ux_labelIsCoded = new System.Windows.Forms.Label();
            this.ux_textBoxIsCoded = new System.Windows.Forms.TextBox();
            this.ux_buttonOK = new System.Windows.Forms.Button();
            this.ux_textBoxCrop = new System.Windows.Forms.TextBox();
            this.ux_labelNumericFormat = new System.Windows.Forms.Label();
            this.ux_textBoxNumericFormat = new System.Windows.Forms.TextBox();
            this.ux_tableLayoutPanelDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_tableLayoutPanelDetails
            // 
            this.ux_tableLayoutPanelDetails.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ux_tableLayoutPanelDetails.ColumnCount = 2;
            this.ux_tableLayoutPanelDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.ux_tableLayoutPanelDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_labelMaximumLength, 0, 6);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_labelDataType, 0, 5);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_labelCategory, 0, 3);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_labelCrop, 0, 2);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_textBoxMaximumLength, 1, 6);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_textBoxDataType, 1, 5);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_textBoxCategory, 1, 3);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_labelDefinition, 0, 1);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_textBoxDefinition, 1, 1);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_textBoxTraitName, 1, 0);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_labelName, 0, 0);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_labelTraitCodes, 0, 11);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_textBoxTraitCodes, 1, 11);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_labelOntologyUrl, 0, 10);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_textBoxOntologyURL, 1, 10);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_labelNumericMinimum, 0, 7);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_labelNumericMaximum, 0, 8);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_textBoxNumericMinimum, 1, 7);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_textBoxNumericMaximum, 1, 8);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_labelIsCoded, 0, 4);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_textBoxIsCoded, 1, 4);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_buttonOK, 1, 13);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_textBoxCrop, 1, 2);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_labelNumericFormat, 0, 9);
            this.ux_tableLayoutPanelDetails.Controls.Add(this.ux_textBoxNumericFormat, 1, 9);
            this.ux_tableLayoutPanelDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tableLayoutPanelDetails.Location = new System.Drawing.Point(0, 0);
            this.ux_tableLayoutPanelDetails.Name = "ux_tableLayoutPanelDetails";
            this.ux_tableLayoutPanelDetails.RowCount = 14;
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.ux_tableLayoutPanelDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_tableLayoutPanelDetails.Size = new System.Drawing.Size(602, 461);
            this.ux_tableLayoutPanelDetails.TabIndex = 0;
            // 
            // ux_labelMaximumLength
            // 
            this.ux_labelMaximumLength.AutoSize = true;
            this.ux_labelMaximumLength.BackColor = System.Drawing.SystemColors.Control;
            this.ux_labelMaximumLength.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelMaximumLength.Location = new System.Drawing.Point(3, 182);
            this.ux_labelMaximumLength.Margin = new System.Windows.Forms.Padding(3);
            this.ux_labelMaximumLength.Name = "ux_labelMaximumLength";
            this.ux_labelMaximumLength.Size = new System.Drawing.Size(154, 20);
            this.ux_labelMaximumLength.TabIndex = 5;
            this.ux_labelMaximumLength.Text = "Maximum Length:";
            // 
            // ux_labelDataType
            // 
            this.ux_labelDataType.AutoSize = true;
            this.ux_labelDataType.BackColor = System.Drawing.SystemColors.Control;
            this.ux_labelDataType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelDataType.Location = new System.Drawing.Point(3, 156);
            this.ux_labelDataType.Margin = new System.Windows.Forms.Padding(3);
            this.ux_labelDataType.Name = "ux_labelDataType";
            this.ux_labelDataType.Size = new System.Drawing.Size(154, 20);
            this.ux_labelDataType.TabIndex = 4;
            this.ux_labelDataType.Text = "Data Type:";
            // 
            // ux_labelCategory
            // 
            this.ux_labelCategory.AutoSize = true;
            this.ux_labelCategory.BackColor = System.Drawing.SystemColors.Control;
            this.ux_labelCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelCategory.Location = new System.Drawing.Point(3, 105);
            this.ux_labelCategory.Margin = new System.Windows.Forms.Padding(3);
            this.ux_labelCategory.Name = "ux_labelCategory";
            this.ux_labelCategory.Size = new System.Drawing.Size(154, 19);
            this.ux_labelCategory.TabIndex = 3;
            this.ux_labelCategory.Text = "Category:";
            // 
            // ux_labelCrop
            // 
            this.ux_labelCrop.AutoSize = true;
            this.ux_labelCrop.BackColor = System.Drawing.SystemColors.Control;
            this.ux_labelCrop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelCrop.Location = new System.Drawing.Point(3, 80);
            this.ux_labelCrop.Margin = new System.Windows.Forms.Padding(3);
            this.ux_labelCrop.Name = "ux_labelCrop";
            this.ux_labelCrop.Size = new System.Drawing.Size(154, 19);
            this.ux_labelCrop.TabIndex = 2;
            this.ux_labelCrop.Text = "Crop:";
            // 
            // ux_textBoxMaximumLength
            // 
            this.ux_textBoxMaximumLength.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ux_textBoxMaximumLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_textBoxMaximumLength.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxMaximumLength.Location = new System.Drawing.Point(163, 182);
            this.ux_textBoxMaximumLength.Name = "ux_textBoxMaximumLength";
            this.ux_textBoxMaximumLength.ReadOnly = true;
            this.ux_textBoxMaximumLength.Size = new System.Drawing.Size(436, 20);
            this.ux_textBoxMaximumLength.TabIndex = 12;
            // 
            // ux_textBoxDataType
            // 
            this.ux_textBoxDataType.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ux_textBoxDataType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_textBoxDataType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxDataType.Location = new System.Drawing.Point(163, 156);
            this.ux_textBoxDataType.Name = "ux_textBoxDataType";
            this.ux_textBoxDataType.ReadOnly = true;
            this.ux_textBoxDataType.Size = new System.Drawing.Size(436, 20);
            this.ux_textBoxDataType.TabIndex = 11;
            // 
            // ux_textBoxCategory
            // 
            this.ux_textBoxCategory.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ux_textBoxCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_textBoxCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxCategory.Location = new System.Drawing.Point(163, 105);
            this.ux_textBoxCategory.Name = "ux_textBoxCategory";
            this.ux_textBoxCategory.ReadOnly = true;
            this.ux_textBoxCategory.Size = new System.Drawing.Size(436, 20);
            this.ux_textBoxCategory.TabIndex = 10;
            // 
            // ux_labelDefinition
            // 
            this.ux_labelDefinition.AutoSize = true;
            this.ux_labelDefinition.BackColor = System.Drawing.SystemColors.Control;
            this.ux_labelDefinition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelDefinition.Location = new System.Drawing.Point(3, 28);
            this.ux_labelDefinition.Margin = new System.Windows.Forms.Padding(3);
            this.ux_labelDefinition.Name = "ux_labelDefinition";
            this.ux_labelDefinition.Size = new System.Drawing.Size(154, 46);
            this.ux_labelDefinition.TabIndex = 0;
            this.ux_labelDefinition.Text = "Definition:";
            // 
            // ux_textBoxDefinition
            // 
            this.ux_textBoxDefinition.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ux_textBoxDefinition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_textBoxDefinition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxDefinition.Location = new System.Drawing.Point(163, 28);
            this.ux_textBoxDefinition.Multiline = true;
            this.ux_textBoxDefinition.Name = "ux_textBoxDefinition";
            this.ux_textBoxDefinition.ReadOnly = true;
            this.ux_textBoxDefinition.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textBoxDefinition.Size = new System.Drawing.Size(436, 46);
            this.ux_textBoxDefinition.TabIndex = 7;
            // 
            // ux_textBoxTraitName
            // 
            this.ux_textBoxTraitName.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ux_textBoxTraitName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxTraitName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_textBoxTraitName.Location = new System.Drawing.Point(163, 3);
            this.ux_textBoxTraitName.MinimumSize = new System.Drawing.Size(4, 20);
            this.ux_textBoxTraitName.Name = "ux_textBoxTraitName";
            this.ux_textBoxTraitName.ReadOnly = true;
            this.ux_textBoxTraitName.Size = new System.Drawing.Size(436, 20);
            this.ux_textBoxTraitName.TabIndex = 17;
            // 
            // ux_labelName
            // 
            this.ux_labelName.AutoSize = true;
            this.ux_labelName.BackColor = System.Drawing.Color.Transparent;
            this.ux_labelName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelName.Location = new System.Drawing.Point(3, 3);
            this.ux_labelName.Margin = new System.Windows.Forms.Padding(3);
            this.ux_labelName.Name = "ux_labelName";
            this.ux_labelName.Size = new System.Drawing.Size(154, 19);
            this.ux_labelName.TabIndex = 18;
            this.ux_labelName.Text = "Name:";
            // 
            // ux_labelTraitCodes
            // 
            this.ux_labelTraitCodes.AutoSize = true;
            this.ux_labelTraitCodes.BackColor = System.Drawing.SystemColors.Control;
            this.ux_labelTraitCodes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelTraitCodes.Location = new System.Drawing.Point(3, 312);
            this.ux_labelTraitCodes.Margin = new System.Windows.Forms.Padding(3);
            this.ux_labelTraitCodes.Name = "ux_labelTraitCodes";
            this.ux_labelTraitCodes.Size = new System.Drawing.Size(154, 80);
            this.ux_labelTraitCodes.TabIndex = 14;
            this.ux_labelTraitCodes.Text = "Trait codes";
            // 
            // ux_textBoxTraitCodes
            // 
            this.ux_textBoxTraitCodes.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ux_textBoxTraitCodes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_textBoxTraitCodes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxTraitCodes.Location = new System.Drawing.Point(163, 312);
            this.ux_textBoxTraitCodes.Multiline = true;
            this.ux_textBoxTraitCodes.Name = "ux_textBoxTraitCodes";
            this.ux_textBoxTraitCodes.ReadOnly = true;
            this.ux_textBoxTraitCodes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textBoxTraitCodes.Size = new System.Drawing.Size(436, 80);
            this.ux_textBoxTraitCodes.TabIndex = 16;
            // 
            // ux_labelOntologyUrl
            // 
            this.ux_labelOntologyUrl.AutoSize = true;
            this.ux_labelOntologyUrl.BackColor = System.Drawing.SystemColors.Control;
            this.ux_labelOntologyUrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelOntologyUrl.Location = new System.Drawing.Point(3, 286);
            this.ux_labelOntologyUrl.Margin = new System.Windows.Forms.Padding(3);
            this.ux_labelOntologyUrl.Name = "ux_labelOntologyUrl";
            this.ux_labelOntologyUrl.Size = new System.Drawing.Size(154, 20);
            this.ux_labelOntologyUrl.TabIndex = 13;
            this.ux_labelOntologyUrl.Text = "Ontology URL:";
            // 
            // ux_textBoxOntologyURL
            // 
            this.ux_textBoxOntologyURL.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ux_textBoxOntologyURL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_textBoxOntologyURL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxOntologyURL.Location = new System.Drawing.Point(163, 286);
            this.ux_textBoxOntologyURL.Name = "ux_textBoxOntologyURL";
            this.ux_textBoxOntologyURL.ReadOnly = true;
            this.ux_textBoxOntologyURL.Size = new System.Drawing.Size(436, 20);
            this.ux_textBoxOntologyURL.TabIndex = 15;
            // 
            // ux_labelNumericMinimum
            // 
            this.ux_labelNumericMinimum.AutoSize = true;
            this.ux_labelNumericMinimum.BackColor = System.Drawing.SystemColors.Control;
            this.ux_labelNumericMinimum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelNumericMinimum.Location = new System.Drawing.Point(3, 208);
            this.ux_labelNumericMinimum.Margin = new System.Windows.Forms.Padding(3);
            this.ux_labelNumericMinimum.Name = "ux_labelNumericMinimum";
            this.ux_labelNumericMinimum.Size = new System.Drawing.Size(154, 20);
            this.ux_labelNumericMinimum.TabIndex = 5;
            this.ux_labelNumericMinimum.Text = "Numeric Minimun:";
            // 
            // ux_labelNumericMaximum
            // 
            this.ux_labelNumericMaximum.AutoSize = true;
            this.ux_labelNumericMaximum.BackColor = System.Drawing.SystemColors.Control;
            this.ux_labelNumericMaximum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelNumericMaximum.Location = new System.Drawing.Point(3, 234);
            this.ux_labelNumericMaximum.Margin = new System.Windows.Forms.Padding(3);
            this.ux_labelNumericMaximum.Name = "ux_labelNumericMaximum";
            this.ux_labelNumericMaximum.Size = new System.Drawing.Size(154, 20);
            this.ux_labelNumericMaximum.TabIndex = 5;
            this.ux_labelNumericMaximum.Text = "Numeric Maximum:";
            // 
            // ux_textBoxNumericMinimum
            // 
            this.ux_textBoxNumericMinimum.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ux_textBoxNumericMinimum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_textBoxNumericMinimum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxNumericMinimum.Location = new System.Drawing.Point(163, 208);
            this.ux_textBoxNumericMinimum.Name = "ux_textBoxNumericMinimum";
            this.ux_textBoxNumericMinimum.ReadOnly = true;
            this.ux_textBoxNumericMinimum.Size = new System.Drawing.Size(436, 20);
            this.ux_textBoxNumericMinimum.TabIndex = 12;
            // 
            // ux_textBoxNumericMaximum
            // 
            this.ux_textBoxNumericMaximum.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ux_textBoxNumericMaximum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_textBoxNumericMaximum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxNumericMaximum.Location = new System.Drawing.Point(163, 234);
            this.ux_textBoxNumericMaximum.Name = "ux_textBoxNumericMaximum";
            this.ux_textBoxNumericMaximum.ReadOnly = true;
            this.ux_textBoxNumericMaximum.Size = new System.Drawing.Size(436, 20);
            this.ux_textBoxNumericMaximum.TabIndex = 12;
            // 
            // ux_labelIsCoded
            // 
            this.ux_labelIsCoded.AutoSize = true;
            this.ux_labelIsCoded.BackColor = System.Drawing.SystemColors.Control;
            this.ux_labelIsCoded.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelIsCoded.Location = new System.Drawing.Point(3, 130);
            this.ux_labelIsCoded.Margin = new System.Windows.Forms.Padding(3);
            this.ux_labelIsCoded.Name = "ux_labelIsCoded";
            this.ux_labelIsCoded.Size = new System.Drawing.Size(154, 20);
            this.ux_labelIsCoded.TabIndex = 3;
            this.ux_labelIsCoded.Text = "Is coded:";
            // 
            // ux_textBoxIsCoded
            // 
            this.ux_textBoxIsCoded.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ux_textBoxIsCoded.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_textBoxIsCoded.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxIsCoded.Location = new System.Drawing.Point(163, 130);
            this.ux_textBoxIsCoded.Name = "ux_textBoxIsCoded";
            this.ux_textBoxIsCoded.ReadOnly = true;
            this.ux_textBoxIsCoded.Size = new System.Drawing.Size(436, 20);
            this.ux_textBoxIsCoded.TabIndex = 10;
            // 
            // ux_buttonOK
            // 
            this.ux_buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonOK.Location = new System.Drawing.Point(519, 428);
            this.ux_buttonOK.Margin = new System.Windows.Forms.Padding(8);
            this.ux_buttonOK.Name = "ux_buttonOK";
            this.ux_buttonOK.Size = new System.Drawing.Size(75, 25);
            this.ux_buttonOK.TabIndex = 8;
            this.ux_buttonOK.Text = "OK";
            this.ux_buttonOK.UseVisualStyleBackColor = true;
            this.ux_buttonOK.Click += new System.EventHandler(this.ux_buttonOK_Click);
            // 
            // ux_textBoxCrop
            // 
            this.ux_textBoxCrop.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ux_textBoxCrop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_textBoxCrop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxCrop.Location = new System.Drawing.Point(163, 80);
            this.ux_textBoxCrop.Name = "ux_textBoxCrop";
            this.ux_textBoxCrop.ReadOnly = true;
            this.ux_textBoxCrop.Size = new System.Drawing.Size(436, 20);
            this.ux_textBoxCrop.TabIndex = 9;
            // 
            // ux_labelNumericFormat
            // 
            this.ux_labelNumericFormat.AutoSize = true;
            this.ux_labelNumericFormat.BackColor = System.Drawing.SystemColors.Control;
            this.ux_labelNumericFormat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_labelNumericFormat.Location = new System.Drawing.Point(3, 260);
            this.ux_labelNumericFormat.Margin = new System.Windows.Forms.Padding(3);
            this.ux_labelNumericFormat.Name = "ux_labelNumericFormat";
            this.ux_labelNumericFormat.Size = new System.Drawing.Size(154, 20);
            this.ux_labelNumericFormat.TabIndex = 5;
            this.ux_labelNumericFormat.Text = "Numeric Format:";
            // 
            // ux_textBoxNumericFormat
            // 
            this.ux_textBoxNumericFormat.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ux_textBoxNumericFormat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_textBoxNumericFormat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxNumericFormat.Location = new System.Drawing.Point(163, 260);
            this.ux_textBoxNumericFormat.Name = "ux_textBoxNumericFormat";
            this.ux_textBoxNumericFormat.ReadOnly = true;
            this.ux_textBoxNumericFormat.Size = new System.Drawing.Size(436, 20);
            this.ux_textBoxNumericFormat.TabIndex = 12;
            // 
            // DescriptorDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(602, 461);
            this.Controls.Add(this.ux_tableLayoutPanelDetails);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(600, 500);
            this.Name = "DescriptorDetailForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DescriptorDetail";
            this.ux_tableLayoutPanelDetails.ResumeLayout(false);
            this.ux_tableLayoutPanelDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel ux_tableLayoutPanelDetails;
        private System.Windows.Forms.Label ux_labelDefinition;
        private System.Windows.Forms.Label ux_labelCrop;
        private System.Windows.Forms.Label ux_labelCategory;
        private System.Windows.Forms.Label ux_labelDataType;
        private System.Windows.Forms.Label ux_labelMaximumLength;
        private System.Windows.Forms.TextBox ux_textBoxDefinition;
        private System.Windows.Forms.Button ux_buttonOK;
        private System.Windows.Forms.TextBox ux_textBoxCrop;
        private System.Windows.Forms.TextBox ux_textBoxCategory;
        private System.Windows.Forms.TextBox ux_textBoxDataType;
        private System.Windows.Forms.TextBox ux_textBoxMaximumLength;
        private System.Windows.Forms.Label ux_labelOntologyUrl;
        private System.Windows.Forms.Label ux_labelTraitCodes;
        private System.Windows.Forms.TextBox ux_textBoxOntologyURL;
        private System.Windows.Forms.TextBox ux_textBoxTraitCodes;
        private System.Windows.Forms.TextBox ux_textBoxTraitName;
        private System.Windows.Forms.Label ux_labelName;
        private System.Windows.Forms.Label ux_labelNumericMinimum;
        private System.Windows.Forms.Label ux_labelNumericMaximum;
        private System.Windows.Forms.TextBox ux_textBoxNumericMinimum;
        private System.Windows.Forms.TextBox ux_textBoxNumericMaximum;
        private System.Windows.Forms.Label ux_labelIsCoded;
        private System.Windows.Forms.TextBox ux_textBoxIsCoded;
        private System.Windows.Forms.Label ux_labelNumericFormat;
        private System.Windows.Forms.TextBox ux_textBoxNumericFormat;
    }
}