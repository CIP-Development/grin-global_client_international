﻿namespace SearchWizard
{
    partial class SearchWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchWizard));
            this.ux_tabcontrolMain = new System.Windows.Forms.TabControl();
            this.ux_tabPageSearchByDescriptor = new System.Windows.Forms.TabPage();
            this.ux_groupboxSearchCriteria = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ux_dataGridViewCriteria = new System.Windows.Forms.DataGridView();
            this.category = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.search_operator = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.values = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.trait_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fieldname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ux_textBoxOutput = new System.Windows.Forms.TextBox();
            this.ux_buttonRefreshSearchCriteria = new System.Windows.Forms.Button();
            this.ux_buttonRemoveCriteria = new System.Windows.Forms.Button();
            this.ux_buttonDescriptorSearch = new System.Windows.Forms.Button();
            this.ux_groupboxSearchResults = new System.Windows.Forms.GroupBox();
            this.ux_numericupdownMaxRecords = new System.Windows.Forms.NumericUpDown();
            this.ux_labelMaxRecords = new System.Windows.Forms.Label();
            this.ux_datagridviewQueryResults = new System.Windows.Forms.DataGridView();
            this.ux_labelTimeElapsed = new System.Windows.Forms.Label();
            this.ux_labelResultsRows = new System.Windows.Forms.Label();
            this.ux_panelDescriptorSearch = new System.Windows.Forms.Panel();
            this.ux_groupboxMoreOptions = new System.Windows.Forms.GroupBox();
            this.ux_checkboxIncludeMethod = new System.Windows.Forms.CheckBox();
            this.ux_checkboxIsArchived = new System.Windows.Forms.CheckBox();
            this.ux_comboboxMethod = new System.Windows.Forms.ComboBox();
            this.ux_groupboxOtherDescriptors = new System.Windows.Forms.GroupBox();
            this.ux_listboxTrait = new System.Windows.Forms.ListBox();
            this.ux_textboxFindDescriptor = new System.Windows.Forms.TextBox();
            this.ux_panelFrequency = new System.Windows.Forms.Panel();
            this.ux_datagridviewFrecuency = new System.Windows.Forms.DataGridView();
            this.ux_buttonFrequency = new System.Windows.Forms.Button();
            this.ux_buttonShowDescription = new System.Windows.Forms.Button();
            this.ux_listboxCategory = new System.Windows.Forms.ListBox();
            this.ux_textBoxPassportValue = new System.Windows.Forms.TextBox();
            this.ux_buttonAddDescriptorToResults = new System.Windows.Forms.Button();
            this.ux_buttonAddCriteria = new System.Windows.Forms.Button();
            this.ux_listBoxPassportValue = new System.Windows.Forms.ListBox();
            this.ux_labelValue = new System.Windows.Forms.Label();
            this.ux_listBoxValue = new System.Windows.Forms.ListBox();
            this.ux_labelOperator = new System.Windows.Forms.Label();
            this.ux_listboxOperator = new System.Windows.Forms.ListBox();
            this.ux_labelTrait = new System.Windows.Forms.Label();
            this.ux_labelCategory = new System.Windows.Forms.Label();
            this.ux_groupboxCrop = new System.Windows.Forms.GroupBox();
            this.ux_comboBoxCrop = new System.Windows.Forms.ComboBox();
            this.ux_labelCropName = new System.Windows.Forms.Label();
            this.ux_tabPageQuickSearch = new System.Windows.Forms.TabPage();
            this.ux_labelQSResultsInfo = new System.Windows.Forms.Label();
            this.ux_checkboxQSOnlyFound = new System.Windows.Forms.CheckBox();
            this.ux_buttonQSSearch = new System.Windows.Forms.Button();
            this.ux_labelQSResultsRows = new System.Windows.Forms.Label();
            this.ux_dataGridViewQSResults = new System.Windows.Forms.DataGridView();
            this.ux_groupboxQSSearchOperator = new System.Windows.Forms.GroupBox();
            this.ux_radiobuttonQSContains = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonQSEquals = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonQSStartsWith = new System.Windows.Forms.RadioButton();
            this.ux_groupboxQSWhereToSearch = new System.Windows.Forms.GroupBox();
            this.ux_listBoxQSFilter = new System.Windows.Forms.ListBox();
            this.ux_groupboxQSEntryList = new System.Windows.Forms.GroupBox();
            this.ux_textboxQSEntryList = new System.Windows.Forms.TextBox();
            this.ux_labelQSEntryListInfo = new System.Windows.Forms.Label();
            this.ux_groupboxQSCrop = new System.Windows.Forms.GroupBox();
            this.ux_listBoxQSCrop = new System.Windows.Forms.ListBox();
            this.ux_tabpageBasicQuery = new System.Windows.Forms.TabPage();
            this.ux_contextmenustripDGVResultsColumnHeader = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.sortAscendingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sortDescendingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noSortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.resetAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.removeColumnMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeAllColumnsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.columnFrequencyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_contextmenustripDGVResultsCell = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_tabcontrolMain.SuspendLayout();
            this.ux_tabPageSearchByDescriptor.SuspendLayout();
            this.ux_groupboxSearchCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_dataGridViewCriteria)).BeginInit();
            this.ux_groupboxSearchResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericupdownMaxRecords)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewQueryResults)).BeginInit();
            this.ux_panelDescriptorSearch.SuspendLayout();
            this.ux_groupboxMoreOptions.SuspendLayout();
            this.ux_groupboxOtherDescriptors.SuspendLayout();
            this.ux_panelFrequency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewFrecuency)).BeginInit();
            this.ux_groupboxCrop.SuspendLayout();
            this.ux_tabPageQuickSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_dataGridViewQSResults)).BeginInit();
            this.ux_groupboxQSSearchOperator.SuspendLayout();
            this.ux_groupboxQSWhereToSearch.SuspendLayout();
            this.ux_groupboxQSEntryList.SuspendLayout();
            this.ux_groupboxQSCrop.SuspendLayout();
            this.ux_contextmenustripDGVResultsColumnHeader.SuspendLayout();
            this.ux_contextmenustripDGVResultsCell.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_tabcontrolMain
            // 
            this.ux_tabcontrolMain.Controls.Add(this.ux_tabPageSearchByDescriptor);
            this.ux_tabcontrolMain.Controls.Add(this.ux_tabPageQuickSearch);
            this.ux_tabcontrolMain.Controls.Add(this.ux_tabpageBasicQuery);
            this.ux_tabcontrolMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tabcontrolMain.Location = new System.Drawing.Point(0, 0);
            this.ux_tabcontrolMain.Name = "ux_tabcontrolMain";
            this.ux_tabcontrolMain.SelectedIndex = 0;
            this.ux_tabcontrolMain.Size = new System.Drawing.Size(884, 684);
            this.ux_tabcontrolMain.TabIndex = 6;
            this.ux_tabcontrolMain.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.ux_tabcontrolMain_Selecting);
            // 
            // ux_tabPageSearchByDescriptor
            // 
            this.ux_tabPageSearchByDescriptor.Controls.Add(this.ux_groupboxSearchCriteria);
            this.ux_tabPageSearchByDescriptor.Controls.Add(this.ux_groupboxSearchResults);
            this.ux_tabPageSearchByDescriptor.Controls.Add(this.ux_panelDescriptorSearch);
            this.ux_tabPageSearchByDescriptor.Location = new System.Drawing.Point(4, 22);
            this.ux_tabPageSearchByDescriptor.Name = "ux_tabPageSearchByDescriptor";
            this.ux_tabPageSearchByDescriptor.Padding = new System.Windows.Forms.Padding(3);
            this.ux_tabPageSearchByDescriptor.Size = new System.Drawing.Size(876, 658);
            this.ux_tabPageSearchByDescriptor.TabIndex = 2;
            this.ux_tabPageSearchByDescriptor.Text = "Search by filters";
            this.ux_tabPageSearchByDescriptor.UseVisualStyleBackColor = true;
            // 
            // ux_groupboxSearchCriteria
            // 
            this.ux_groupboxSearchCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxSearchCriteria.BackColor = System.Drawing.Color.Transparent;
            this.ux_groupboxSearchCriteria.Controls.Add(this.splitContainer1);
            this.ux_groupboxSearchCriteria.Controls.Add(this.ux_buttonRefreshSearchCriteria);
            this.ux_groupboxSearchCriteria.Controls.Add(this.ux_buttonRemoveCriteria);
            this.ux_groupboxSearchCriteria.Controls.Add(this.ux_buttonDescriptorSearch);
            this.ux_groupboxSearchCriteria.Location = new System.Drawing.Point(3, 261);
            this.ux_groupboxSearchCriteria.Name = "ux_groupboxSearchCriteria";
            this.ux_groupboxSearchCriteria.Size = new System.Drawing.Size(867, 164);
            this.ux_groupboxSearchCriteria.TabIndex = 9;
            this.ux_groupboxSearchCriteria.TabStop = false;
            this.ux_groupboxSearchCriteria.Text = "Search Criteria";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainer1.Location = new System.Drawing.Point(3, 16);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ux_dataGridViewCriteria);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ux_textBoxOutput);
            this.splitContainer1.Size = new System.Drawing.Size(861, 111);
            this.splitContainer1.SplitterDistance = 472;
            this.splitContainer1.TabIndex = 8;
            // 
            // ux_dataGridViewCriteria
            // 
            this.ux_dataGridViewCriteria.AllowUserToAddRows = false;
            this.ux_dataGridViewCriteria.AllowUserToResizeRows = false;
            this.ux_dataGridViewCriteria.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.ux_dataGridViewCriteria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_dataGridViewCriteria.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.category,
            this.descriptor,
            this.search_operator,
            this.values,
            this.trait_id,
            this.fieldname,
            this.value});
            this.ux_dataGridViewCriteria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_dataGridViewCriteria.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ux_dataGridViewCriteria.Location = new System.Drawing.Point(0, 0);
            this.ux_dataGridViewCriteria.Name = "ux_dataGridViewCriteria";
            this.ux_dataGridViewCriteria.Size = new System.Drawing.Size(472, 111);
            this.ux_dataGridViewCriteria.TabIndex = 7;
            // 
            // category
            // 
            this.category.HeaderText = "Category";
            this.category.Name = "category";
            // 
            // descriptor
            // 
            this.descriptor.HeaderText = "Descriptor";
            this.descriptor.Name = "descriptor";
            // 
            // search_operator
            // 
            this.search_operator.HeaderText = "Operator";
            this.search_operator.Name = "search_operator";
            // 
            // values
            // 
            this.values.HeaderText = "Values";
            this.values.Name = "values";
            // 
            // trait_id
            // 
            this.trait_id.HeaderText = "trait_id";
            this.trait_id.Name = "trait_id";
            // 
            // fieldname
            // 
            this.fieldname.HeaderText = "fieldname";
            this.fieldname.Name = "fieldname";
            // 
            // value
            // 
            this.value.HeaderText = "value";
            this.value.Name = "value";
            // 
            // ux_textBoxOutput
            // 
            this.ux_textBoxOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_textBoxOutput.Location = new System.Drawing.Point(0, 0);
            this.ux_textBoxOutput.Multiline = true;
            this.ux_textBoxOutput.Name = "ux_textBoxOutput";
            this.ux_textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textBoxOutput.Size = new System.Drawing.Size(385, 111);
            this.ux_textBoxOutput.TabIndex = 6;
            this.ux_textBoxOutput.Enter += new System.EventHandler(this.ux_textBoxOutput_Enter);
            this.ux_textBoxOutput.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ux_textBoxOutput_MouseDoubleClick);
            this.ux_textBoxOutput.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ux_textBoxOutput_MouseDown);
            this.ux_textBoxOutput.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ux_textBoxOutput_MouseMove);
            this.ux_textBoxOutput.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ux_textBoxOutput_MouseUp);
            // 
            // ux_buttonRefreshSearchCriteria
            // 
            this.ux_buttonRefreshSearchCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_buttonRefreshSearchCriteria.Location = new System.Drawing.Point(132, 132);
            this.ux_buttonRefreshSearchCriteria.Name = "ux_buttonRefreshSearchCriteria";
            this.ux_buttonRefreshSearchCriteria.Size = new System.Drawing.Size(120, 23);
            this.ux_buttonRefreshSearchCriteria.TabIndex = 4;
            this.ux_buttonRefreshSearchCriteria.Text = "Refresh Criteria";
            this.ux_buttonRefreshSearchCriteria.UseVisualStyleBackColor = true;
            this.ux_buttonRefreshSearchCriteria.Click += new System.EventHandler(this.ux_buttonRefreshSearchCriteria_Click);
            // 
            // ux_buttonRemoveCriteria
            // 
            this.ux_buttonRemoveCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_buttonRemoveCriteria.Location = new System.Drawing.Point(6, 132);
            this.ux_buttonRemoveCriteria.Name = "ux_buttonRemoveCriteria";
            this.ux_buttonRemoveCriteria.Size = new System.Drawing.Size(120, 23);
            this.ux_buttonRemoveCriteria.TabIndex = 4;
            this.ux_buttonRemoveCriteria.Text = "Remove Criteria";
            this.ux_buttonRemoveCriteria.UseVisualStyleBackColor = true;
            this.ux_buttonRemoveCriteria.Click += new System.EventHandler(this.ux_buttonRemoveCriteria_Click);
            // 
            // ux_buttonDescriptorSearch
            // 
            this.ux_buttonDescriptorSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonDescriptorSearch.Location = new System.Drawing.Point(741, 133);
            this.ux_buttonDescriptorSearch.Name = "ux_buttonDescriptorSearch";
            this.ux_buttonDescriptorSearch.Size = new System.Drawing.Size(120, 23);
            this.ux_buttonDescriptorSearch.TabIndex = 2;
            this.ux_buttonDescriptorSearch.Text = "Search Now!";
            this.ux_buttonDescriptorSearch.UseVisualStyleBackColor = true;
            this.ux_buttonDescriptorSearch.Click += new System.EventHandler(this.ux_buttonDescriptorSearch_Click);
            // 
            // ux_groupboxSearchResults
            // 
            this.ux_groupboxSearchResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxSearchResults.Controls.Add(this.ux_numericupdownMaxRecords);
            this.ux_groupboxSearchResults.Controls.Add(this.ux_labelMaxRecords);
            this.ux_groupboxSearchResults.Controls.Add(this.ux_datagridviewQueryResults);
            this.ux_groupboxSearchResults.Controls.Add(this.ux_labelTimeElapsed);
            this.ux_groupboxSearchResults.Controls.Add(this.ux_labelResultsRows);
            this.ux_groupboxSearchResults.Location = new System.Drawing.Point(3, 431);
            this.ux_groupboxSearchResults.Name = "ux_groupboxSearchResults";
            this.ux_groupboxSearchResults.Size = new System.Drawing.Size(867, 221);
            this.ux_groupboxSearchResults.TabIndex = 24;
            this.ux_groupboxSearchResults.TabStop = false;
            this.ux_groupboxSearchResults.Text = "Search Results";
            // 
            // ux_numericupdownMaxRecords
            // 
            this.ux_numericupdownMaxRecords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_numericupdownMaxRecords.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.ux_numericupdownMaxRecords.Location = new System.Drawing.Point(800, 12);
            this.ux_numericupdownMaxRecords.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ux_numericupdownMaxRecords.Name = "ux_numericupdownMaxRecords";
            this.ux_numericupdownMaxRecords.Size = new System.Drawing.Size(64, 20);
            this.ux_numericupdownMaxRecords.TabIndex = 15;
            this.ux_numericupdownMaxRecords.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // ux_labelMaxRecords
            // 
            this.ux_labelMaxRecords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelMaxRecords.Location = new System.Drawing.Point(715, 12);
            this.ux_labelMaxRecords.Name = "ux_labelMaxRecords";
            this.ux_labelMaxRecords.Size = new System.Drawing.Size(79, 20);
            this.ux_labelMaxRecords.TabIndex = 16;
            this.ux_labelMaxRecords.Text = "Max Rows:";
            this.ux_labelMaxRecords.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_datagridviewQueryResults
            // 
            this.ux_datagridviewQueryResults.AllowUserToAddRows = false;
            this.ux_datagridviewQueryResults.AllowUserToDeleteRows = false;
            this.ux_datagridviewQueryResults.AllowUserToOrderColumns = true;
            this.ux_datagridviewQueryResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewQueryResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ux_datagridviewQueryResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ux_datagridviewQueryResults.DefaultCellStyle = dataGridViewCellStyle2;
            this.ux_datagridviewQueryResults.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ux_datagridviewQueryResults.Location = new System.Drawing.Point(3, 35);
            this.ux_datagridviewQueryResults.Name = "ux_datagridviewQueryResults";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_datagridviewQueryResults.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.ux_datagridviewQueryResults.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ux_datagridviewQueryResults.Size = new System.Drawing.Size(861, 167);
            this.ux_datagridviewQueryResults.TabIndex = 1;
            this.ux_datagridviewQueryResults.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.ux_datagridviewQueryResults_CellContextMenuStripNeeded);
            this.ux_datagridviewQueryResults.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ux_datagridviewQueryResults_CellMouseClick);
            this.ux_datagridviewQueryResults.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ux_datagridviewQueryResults_CellMouseDown);
            this.ux_datagridviewQueryResults.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ux_datagridviewQueryResults_ColumnHeaderMouseClick);
            // 
            // ux_labelTimeElapsed
            // 
            this.ux_labelTimeElapsed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_labelTimeElapsed.AutoSize = true;
            this.ux_labelTimeElapsed.Location = new System.Drawing.Point(184, 205);
            this.ux_labelTimeElapsed.Name = "ux_labelTimeElapsed";
            this.ux_labelTimeElapsed.Size = new System.Drawing.Size(110, 13);
            this.ux_labelTimeElapsed.TabIndex = 5;
            this.ux_labelTimeElapsed.Tag = "Time elapsed: {0} seg";
            this.ux_labelTimeElapsed.Text = "Time elapsed: {0} seg";
            // 
            // ux_labelResultsRows
            // 
            this.ux_labelResultsRows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_labelResultsRows.AutoSize = true;
            this.ux_labelResultsRows.Location = new System.Drawing.Point(7, 205);
            this.ux_labelResultsRows.Name = "ux_labelResultsRows";
            this.ux_labelResultsRows.Size = new System.Drawing.Size(125, 13);
            this.ux_labelResultsRows.TabIndex = 5;
            this.ux_labelResultsRows.Tag = "Showing rows: {0} de {1}";
            this.ux_labelResultsRows.Text = "Showing rows: {0} de {1}";
            // 
            // ux_panelDescriptorSearch
            // 
            this.ux_panelDescriptorSearch.Controls.Add(this.ux_groupboxMoreOptions);
            this.ux_panelDescriptorSearch.Controls.Add(this.ux_groupboxOtherDescriptors);
            this.ux_panelDescriptorSearch.Controls.Add(this.ux_groupboxCrop);
            this.ux_panelDescriptorSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.ux_panelDescriptorSearch.Location = new System.Drawing.Point(3, 3);
            this.ux_panelDescriptorSearch.Name = "ux_panelDescriptorSearch";
            this.ux_panelDescriptorSearch.Size = new System.Drawing.Size(870, 254);
            this.ux_panelDescriptorSearch.TabIndex = 0;
            // 
            // ux_groupboxMoreOptions
            // 
            this.ux_groupboxMoreOptions.Controls.Add(this.ux_checkboxIncludeMethod);
            this.ux_groupboxMoreOptions.Controls.Add(this.ux_checkboxIsArchived);
            this.ux_groupboxMoreOptions.Controls.Add(this.ux_comboboxMethod);
            this.ux_groupboxMoreOptions.Location = new System.Drawing.Point(261, 3);
            this.ux_groupboxMoreOptions.Name = "ux_groupboxMoreOptions";
            this.ux_groupboxMoreOptions.Size = new System.Drawing.Size(552, 51);
            this.ux_groupboxMoreOptions.TabIndex = 18;
            this.ux_groupboxMoreOptions.TabStop = false;
            // 
            // ux_checkboxIncludeMethod
            // 
            this.ux_checkboxIncludeMethod.AutoSize = true;
            this.ux_checkboxIncludeMethod.Checked = true;
            this.ux_checkboxIncludeMethod.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ux_checkboxIncludeMethod.Location = new System.Drawing.Point(114, 21);
            this.ux_checkboxIncludeMethod.Name = "ux_checkboxIncludeMethod";
            this.ux_checkboxIncludeMethod.Size = new System.Drawing.Size(112, 17);
            this.ux_checkboxIncludeMethod.TabIndex = 9;
            this.ux_checkboxIncludeMethod.Text = "Consider Method?";
            this.ux_checkboxIncludeMethod.UseVisualStyleBackColor = true;
            this.ux_checkboxIncludeMethod.CheckedChanged += new System.EventHandler(this.ux_checkBoxIncludeMethod_CheckedChanged);
            // 
            // ux_checkboxIsArchived
            // 
            this.ux_checkboxIsArchived.AutoSize = true;
            this.ux_checkboxIsArchived.Location = new System.Drawing.Point(15, 21);
            this.ux_checkboxIsArchived.Name = "ux_checkboxIsArchived";
            this.ux_checkboxIsArchived.Size = new System.Drawing.Size(84, 17);
            this.ux_checkboxIsArchived.TabIndex = 5;
            this.ux_checkboxIsArchived.Text = "Is archived?";
            this.ux_checkboxIsArchived.UseVisualStyleBackColor = true;
            this.ux_checkboxIsArchived.CheckedChanged += new System.EventHandler(this.ux_checkboxIsArchived_CheckedChanged);
            // 
            // ux_comboboxMethod
            // 
            this.ux_comboboxMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboboxMethod.FormattingEnabled = true;
            this.ux_comboboxMethod.Location = new System.Drawing.Point(238, 19);
            this.ux_comboboxMethod.Name = "ux_comboboxMethod";
            this.ux_comboboxMethod.Size = new System.Drawing.Size(303, 21);
            this.ux_comboboxMethod.TabIndex = 7;
            this.ux_comboboxMethod.SelectedIndexChanged += new System.EventHandler(this.ux_comboboxMethod_SelectedIndexChanged);
            // 
            // ux_groupboxOtherDescriptors
            // 
            this.ux_groupboxOtherDescriptors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_listboxTrait);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_textboxFindDescriptor);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_panelFrequency);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_buttonFrequency);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_buttonShowDescription);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_listboxCategory);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_textBoxPassportValue);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_buttonAddDescriptorToResults);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_buttonAddCriteria);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_listBoxPassportValue);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_labelValue);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_listBoxValue);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_labelOperator);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_listboxOperator);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_labelTrait);
            this.ux_groupboxOtherDescriptors.Controls.Add(this.ux_labelCategory);
            this.ux_groupboxOtherDescriptors.Location = new System.Drawing.Point(0, 60);
            this.ux_groupboxOtherDescriptors.Name = "ux_groupboxOtherDescriptors";
            this.ux_groupboxOtherDescriptors.Size = new System.Drawing.Size(867, 191);
            this.ux_groupboxOtherDescriptors.TabIndex = 16;
            this.ux_groupboxOtherDescriptors.TabStop = false;
            this.ux_groupboxOtherDescriptors.Text = "Select Trait / Descriptors";
            // 
            // ux_listboxTrait
            // 
            this.ux_listboxTrait.FormattingEnabled = true;
            this.ux_listboxTrait.Location = new System.Drawing.Point(165, 37);
            this.ux_listboxTrait.Name = "ux_listboxTrait";
            this.ux_listboxTrait.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.ux_listboxTrait.Size = new System.Drawing.Size(283, 121);
            this.ux_listboxTrait.TabIndex = 8;
            this.ux_listboxTrait.SelectedValueChanged += new System.EventHandler(this.ux_listBoxTrait_SelectedValueChanged);
            // 
            // ux_textboxFindDescriptor
            // 
            this.ux_textboxFindDescriptor.Location = new System.Drawing.Point(279, 16);
            this.ux_textboxFindDescriptor.Name = "ux_textboxFindDescriptor";
            this.ux_textboxFindDescriptor.Size = new System.Drawing.Size(169, 20);
            this.ux_textboxFindDescriptor.TabIndex = 19;
            this.ux_textboxFindDescriptor.TextChanged += new System.EventHandler(this.ux_textboxFindDescriptor_TextChanged);
            this.ux_textboxFindDescriptor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ux_textboxFindDescriptor_KeyDown);
            // 
            // ux_panelFrequency
            // 
            this.ux_panelFrequency.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_panelFrequency.Controls.Add(this.ux_datagridviewFrecuency);
            this.ux_panelFrequency.Location = new System.Drawing.Point(580, 61);
            this.ux_panelFrequency.Name = "ux_panelFrequency";
            this.ux_panelFrequency.Size = new System.Drawing.Size(281, 97);
            this.ux_panelFrequency.TabIndex = 18;
            this.ux_panelFrequency.Visible = false;
            // 
            // ux_datagridviewFrecuency
            // 
            this.ux_datagridviewFrecuency.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_datagridviewFrecuency.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewFrecuency.Location = new System.Drawing.Point(3, 11);
            this.ux_datagridviewFrecuency.Name = "ux_datagridviewFrecuency";
            this.ux_datagridviewFrecuency.Size = new System.Drawing.Size(275, 83);
            this.ux_datagridviewFrecuency.TabIndex = 0;
            // 
            // ux_buttonFrequency
            // 
            this.ux_buttonFrequency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_buttonFrequency.Location = new System.Drawing.Point(377, 161);
            this.ux_buttonFrequency.Name = "ux_buttonFrequency";
            this.ux_buttonFrequency.Size = new System.Drawing.Size(71, 23);
            this.ux_buttonFrequency.TabIndex = 17;
            this.ux_buttonFrequency.Text = "Frequency";
            this.ux_buttonFrequency.UseVisualStyleBackColor = true;
            this.ux_buttonFrequency.Click += new System.EventHandler(this.ux_buttonFrequency_Click);
            // 
            // ux_buttonShowDescription
            // 
            this.ux_buttonShowDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_buttonShowDescription.Location = new System.Drawing.Point(282, 161);
            this.ux_buttonShowDescription.Name = "ux_buttonShowDescription";
            this.ux_buttonShowDescription.Size = new System.Drawing.Size(94, 23);
            this.ux_buttonShowDescription.TabIndex = 15;
            this.ux_buttonShowDescription.Text = "Show Definition";
            this.ux_buttonShowDescription.UseVisualStyleBackColor = true;
            this.ux_buttonShowDescription.Click += new System.EventHandler(this.ux_buttonShowDescription_Click);
            // 
            // ux_listboxCategory
            // 
            this.ux_listboxCategory.FormattingEnabled = true;
            this.ux_listboxCategory.Location = new System.Drawing.Point(15, 37);
            this.ux_listboxCategory.Name = "ux_listboxCategory";
            this.ux_listboxCategory.Size = new System.Drawing.Size(144, 121);
            this.ux_listboxCategory.TabIndex = 7;
            this.ux_listboxCategory.SelectedValueChanged += new System.EventHandler(this.ux_listBoxCategory_SelectedValueChanged);
            // 
            // ux_textBoxPassportValue
            // 
            this.ux_textBoxPassportValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textBoxPassportValue.Location = new System.Drawing.Point(580, 37);
            this.ux_textBoxPassportValue.Name = "ux_textBoxPassportValue";
            this.ux_textBoxPassportValue.Size = new System.Drawing.Size(278, 20);
            this.ux_textBoxPassportValue.TabIndex = 11;
            this.ux_textBoxPassportValue.Visible = false;
            // 
            // ux_buttonAddDescriptorToResults
            // 
            this.ux_buttonAddDescriptorToResults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_buttonAddDescriptorToResults.Location = new System.Drawing.Point(165, 161);
            this.ux_buttonAddDescriptorToResults.Name = "ux_buttonAddDescriptorToResults";
            this.ux_buttonAddDescriptorToResults.Size = new System.Drawing.Size(116, 23);
            this.ux_buttonAddDescriptorToResults.TabIndex = 3;
            this.ux_buttonAddDescriptorToResults.Text = "Add to Results";
            this.ux_buttonAddDescriptorToResults.UseVisualStyleBackColor = true;
            this.ux_buttonAddDescriptorToResults.Click += new System.EventHandler(this.ux_buttonAddDescriptorToResults_Click);
            // 
            // ux_buttonAddCriteria
            // 
            this.ux_buttonAddCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonAddCriteria.Location = new System.Drawing.Point(741, 161);
            this.ux_buttonAddCriteria.Name = "ux_buttonAddCriteria";
            this.ux_buttonAddCriteria.Size = new System.Drawing.Size(120, 23);
            this.ux_buttonAddCriteria.TabIndex = 3;
            this.ux_buttonAddCriteria.Text = "Filter by Criteria";
            this.ux_buttonAddCriteria.UseVisualStyleBackColor = true;
            this.ux_buttonAddCriteria.Click += new System.EventHandler(this.ux_buttonAddCriteria_Click);
            // 
            // ux_listBoxPassportValue
            // 
            this.ux_listBoxPassportValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_listBoxPassportValue.FormattingEnabled = true;
            this.ux_listBoxPassportValue.Location = new System.Drawing.Point(580, 37);
            this.ux_listBoxPassportValue.Name = "ux_listBoxPassportValue";
            this.ux_listBoxPassportValue.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.ux_listBoxPassportValue.Size = new System.Drawing.Size(281, 121);
            this.ux_listBoxPassportValue.TabIndex = 10;
            this.ux_listBoxPassportValue.Visible = false;
            // 
            // ux_labelValue
            // 
            this.ux_labelValue.AutoSize = true;
            this.ux_labelValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_labelValue.Location = new System.Drawing.Point(580, 21);
            this.ux_labelValue.Name = "ux_labelValue";
            this.ux_labelValue.Size = new System.Drawing.Size(34, 13);
            this.ux_labelValue.TabIndex = 14;
            this.ux_labelValue.Text = "Value";
            // 
            // ux_listBoxValue
            // 
            this.ux_listBoxValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_listBoxValue.FormattingEnabled = true;
            this.ux_listBoxValue.Location = new System.Drawing.Point(580, 37);
            this.ux_listBoxValue.Name = "ux_listBoxValue";
            this.ux_listBoxValue.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.ux_listBoxValue.Size = new System.Drawing.Size(281, 121);
            this.ux_listBoxValue.TabIndex = 9;
            // 
            // ux_labelOperator
            // 
            this.ux_labelOperator.AutoSize = true;
            this.ux_labelOperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_labelOperator.Location = new System.Drawing.Point(454, 21);
            this.ux_labelOperator.Name = "ux_labelOperator";
            this.ux_labelOperator.Size = new System.Drawing.Size(48, 13);
            this.ux_labelOperator.TabIndex = 13;
            this.ux_labelOperator.Text = "Operator";
            // 
            // ux_listboxOperator
            // 
            this.ux_listboxOperator.FormattingEnabled = true;
            this.ux_listboxOperator.Location = new System.Drawing.Point(454, 37);
            this.ux_listboxOperator.Name = "ux_listboxOperator";
            this.ux_listboxOperator.Size = new System.Drawing.Size(119, 121);
            this.ux_listboxOperator.TabIndex = 10;
            // 
            // ux_labelTrait
            // 
            this.ux_labelTrait.AutoSize = true;
            this.ux_labelTrait.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_labelTrait.Location = new System.Drawing.Point(168, 21);
            this.ux_labelTrait.Name = "ux_labelTrait";
            this.ux_labelTrait.Size = new System.Drawing.Size(87, 13);
            this.ux_labelTrait.TabIndex = 12;
            this.ux_labelTrait.Text = "Trait / Descriptor";
            // 
            // ux_labelCategory
            // 
            this.ux_labelCategory.AutoSize = true;
            this.ux_labelCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_labelCategory.Location = new System.Drawing.Point(18, 21);
            this.ux_labelCategory.Name = "ux_labelCategory";
            this.ux_labelCategory.Size = new System.Drawing.Size(49, 13);
            this.ux_labelCategory.TabIndex = 11;
            this.ux_labelCategory.Text = "Category";
            // 
            // ux_groupboxCrop
            // 
            this.ux_groupboxCrop.Controls.Add(this.ux_comboBoxCrop);
            this.ux_groupboxCrop.Controls.Add(this.ux_labelCropName);
            this.ux_groupboxCrop.Location = new System.Drawing.Point(3, 3);
            this.ux_groupboxCrop.Name = "ux_groupboxCrop";
            this.ux_groupboxCrop.Size = new System.Drawing.Size(252, 52);
            this.ux_groupboxCrop.TabIndex = 15;
            this.ux_groupboxCrop.TabStop = false;
            this.ux_groupboxCrop.Text = "Select Crop";
            // 
            // ux_comboBoxCrop
            // 
            this.ux_comboBoxCrop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboBoxCrop.FormattingEnabled = true;
            this.ux_comboBoxCrop.Location = new System.Drawing.Point(66, 17);
            this.ux_comboBoxCrop.Name = "ux_comboBoxCrop";
            this.ux_comboBoxCrop.Size = new System.Drawing.Size(180, 21);
            this.ux_comboBoxCrop.TabIndex = 1;
            this.ux_comboBoxCrop.SelectedValueChanged += new System.EventHandler(this.ux_comboBoxCrop_SelectedValueChanged);
            // 
            // ux_labelCropName
            // 
            this.ux_labelCropName.AutoSize = true;
            this.ux_labelCropName.Location = new System.Drawing.Point(9, 22);
            this.ux_labelCropName.Name = "ux_labelCropName";
            this.ux_labelCropName.Size = new System.Drawing.Size(29, 13);
            this.ux_labelCropName.TabIndex = 5;
            this.ux_labelCropName.Text = "Crop";
            // 
            // ux_tabPageQuickSearch
            // 
            this.ux_tabPageQuickSearch.Controls.Add(this.ux_labelQSResultsInfo);
            this.ux_tabPageQuickSearch.Controls.Add(this.ux_checkboxQSOnlyFound);
            this.ux_tabPageQuickSearch.Controls.Add(this.ux_buttonQSSearch);
            this.ux_tabPageQuickSearch.Controls.Add(this.ux_labelQSResultsRows);
            this.ux_tabPageQuickSearch.Controls.Add(this.ux_dataGridViewQSResults);
            this.ux_tabPageQuickSearch.Controls.Add(this.ux_groupboxQSSearchOperator);
            this.ux_tabPageQuickSearch.Controls.Add(this.ux_groupboxQSWhereToSearch);
            this.ux_tabPageQuickSearch.Controls.Add(this.ux_groupboxQSEntryList);
            this.ux_tabPageQuickSearch.Controls.Add(this.ux_groupboxQSCrop);
            this.ux_tabPageQuickSearch.Location = new System.Drawing.Point(4, 22);
            this.ux_tabPageQuickSearch.Name = "ux_tabPageQuickSearch";
            this.ux_tabPageQuickSearch.Padding = new System.Windows.Forms.Padding(3);
            this.ux_tabPageQuickSearch.Size = new System.Drawing.Size(876, 658);
            this.ux_tabPageQuickSearch.TabIndex = 3;
            this.ux_tabPageQuickSearch.Text = "Search by list";
            this.ux_tabPageQuickSearch.UseVisualStyleBackColor = true;
            // 
            // ux_labelQSResultsInfo
            // 
            this.ux_labelQSResultsInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_labelQSResultsInfo.AutoSize = true;
            this.ux_labelQSResultsInfo.Location = new System.Drawing.Point(153, 640);
            this.ux_labelQSResultsInfo.Name = "ux_labelQSResultsInfo";
            this.ux_labelQSResultsInfo.Size = new System.Drawing.Size(303, 13);
            this.ux_labelQSResultsInfo.TabIndex = 7;
            this.ux_labelQSResultsInfo.Tag = "Total entries: {0} ; Entries not found: {1}; Time elapsed: {2} seg";
            this.ux_labelQSResultsInfo.Text = "Total entries: {0} ; Entries not found: {1}; Time elapsed: {2} seg";
            this.ux_labelQSResultsInfo.Visible = false;
            // 
            // ux_checkboxQSOnlyFound
            // 
            this.ux_checkboxQSOnlyFound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_checkboxQSOnlyFound.AutoSize = true;
            this.ux_checkboxQSOnlyFound.Checked = true;
            this.ux_checkboxQSOnlyFound.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ux_checkboxQSOnlyFound.Location = new System.Drawing.Point(725, 639);
            this.ux_checkboxQSOnlyFound.Name = "ux_checkboxQSOnlyFound";
            this.ux_checkboxQSOnlyFound.Size = new System.Drawing.Size(139, 17);
            this.ux_checkboxQSOnlyFound.TabIndex = 22;
            this.ux_checkboxQSOnlyFound.Text = "Show only found entries";
            this.ux_checkboxQSOnlyFound.UseVisualStyleBackColor = true;
            this.ux_checkboxQSOnlyFound.CheckedChanged += new System.EventHandler(this.ux_checkBoxOnlyFound_CheckedChanged);
            // 
            // ux_buttonQSSearch
            // 
            this.ux_buttonQSSearch.Location = new System.Drawing.Point(8, 189);
            this.ux_buttonQSSearch.Name = "ux_buttonQSSearch";
            this.ux_buttonQSSearch.Size = new System.Drawing.Size(200, 23);
            this.ux_buttonQSSearch.TabIndex = 20;
            this.ux_buttonQSSearch.Text = "Search Now!";
            this.ux_buttonQSSearch.UseVisualStyleBackColor = true;
            this.ux_buttonQSSearch.Click += new System.EventHandler(this.ux_buttonQSSearch_Click);
            // 
            // ux_labelQSResultsRows
            // 
            this.ux_labelQSResultsRows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_labelQSResultsRows.AutoSize = true;
            this.ux_labelQSResultsRows.Location = new System.Drawing.Point(8, 640);
            this.ux_labelQSResultsRows.Name = "ux_labelQSResultsRows";
            this.ux_labelQSResultsRows.Size = new System.Drawing.Size(122, 13);
            this.ux_labelQSResultsRows.TabIndex = 7;
            this.ux_labelQSResultsRows.Tag = "Showing rows: {0} of {1}";
            this.ux_labelQSResultsRows.Text = "Showing rows: {0} of {1}";
            // 
            // ux_dataGridViewQSResults
            // 
            this.ux_dataGridViewQSResults.AllowUserToAddRows = false;
            this.ux_dataGridViewQSResults.AllowUserToDeleteRows = false;
            this.ux_dataGridViewQSResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_dataGridViewQSResults.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.ux_dataGridViewQSResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_dataGridViewQSResults.Location = new System.Drawing.Point(8, 225);
            this.ux_dataGridViewQSResults.Name = "ux_dataGridViewQSResults";
            this.ux_dataGridViewQSResults.Size = new System.Drawing.Size(859, 412);
            this.ux_dataGridViewQSResults.TabIndex = 21;
            this.ux_dataGridViewQSResults.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ux_dataGridViewQSResults_CellMouseDown);
            // 
            // ux_groupboxQSSearchOperator
            // 
            this.ux_groupboxQSSearchOperator.Controls.Add(this.ux_radiobuttonQSContains);
            this.ux_groupboxQSSearchOperator.Controls.Add(this.ux_radiobuttonQSEquals);
            this.ux_groupboxQSSearchOperator.Controls.Add(this.ux_radiobuttonQSStartsWith);
            this.ux_groupboxQSSearchOperator.Location = new System.Drawing.Point(232, 171);
            this.ux_groupboxQSSearchOperator.Name = "ux_groupboxQSSearchOperator";
            this.ux_groupboxQSSearchOperator.Size = new System.Drawing.Size(252, 48);
            this.ux_groupboxQSSearchOperator.TabIndex = 18;
            this.ux_groupboxQSSearchOperator.TabStop = false;
            this.ux_groupboxQSSearchOperator.Text = "Search Operator";
            // 
            // ux_radiobuttonQSContains
            // 
            this.ux_radiobuttonQSContains.AutoSize = true;
            this.ux_radiobuttonQSContains.Location = new System.Drawing.Point(180, 21);
            this.ux_radiobuttonQSContains.Name = "ux_radiobuttonQSContains";
            this.ux_radiobuttonQSContains.Size = new System.Drawing.Size(66, 17);
            this.ux_radiobuttonQSContains.TabIndex = 21;
            this.ux_radiobuttonQSContains.Text = "Contains";
            this.ux_radiobuttonQSContains.UseVisualStyleBackColor = true;
            this.ux_radiobuttonQSContains.CheckedChanged += new System.EventHandler(this.ux_radiobutton_CheckedChanged);
            // 
            // ux_radiobuttonQSEquals
            // 
            this.ux_radiobuttonQSEquals.AutoSize = true;
            this.ux_radiobuttonQSEquals.Location = new System.Drawing.Point(98, 21);
            this.ux_radiobuttonQSEquals.Name = "ux_radiobuttonQSEquals";
            this.ux_radiobuttonQSEquals.Size = new System.Drawing.Size(57, 17);
            this.ux_radiobuttonQSEquals.TabIndex = 20;
            this.ux_radiobuttonQSEquals.Text = "Equals";
            this.ux_radiobuttonQSEquals.UseVisualStyleBackColor = true;
            this.ux_radiobuttonQSEquals.CheckedChanged += new System.EventHandler(this.ux_radiobutton_CheckedChanged);
            // 
            // ux_radiobuttonQSStartsWith
            // 
            this.ux_radiobuttonQSStartsWith.AutoSize = true;
            this.ux_radiobuttonQSStartsWith.Location = new System.Drawing.Point(3, 21);
            this.ux_radiobuttonQSStartsWith.Name = "ux_radiobuttonQSStartsWith";
            this.ux_radiobuttonQSStartsWith.Size = new System.Drawing.Size(77, 17);
            this.ux_radiobuttonQSStartsWith.TabIndex = 19;
            this.ux_radiobuttonQSStartsWith.Text = "Starts With";
            this.ux_radiobuttonQSStartsWith.UseVisualStyleBackColor = true;
            this.ux_radiobuttonQSStartsWith.CheckedChanged += new System.EventHandler(this.ux_radiobutton_CheckedChanged);
            // 
            // ux_groupboxQSWhereToSearch
            // 
            this.ux_groupboxQSWhereToSearch.Controls.Add(this.ux_listBoxQSFilter);
            this.ux_groupboxQSWhereToSearch.Location = new System.Drawing.Point(232, 6);
            this.ux_groupboxQSWhereToSearch.Name = "ux_groupboxQSWhereToSearch";
            this.ux_groupboxQSWhereToSearch.Size = new System.Drawing.Size(252, 162);
            this.ux_groupboxQSWhereToSearch.TabIndex = 17;
            this.ux_groupboxQSWhereToSearch.TabStop = false;
            this.ux_groupboxQSWhereToSearch.Text = "Where To Search";
            // 
            // ux_listBoxQSFilter
            // 
            this.ux_listBoxQSFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_listBoxQSFilter.FormattingEnabled = true;
            this.ux_listBoxQSFilter.Location = new System.Drawing.Point(3, 16);
            this.ux_listBoxQSFilter.Name = "ux_listBoxQSFilter";
            this.ux_listBoxQSFilter.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.ux_listBoxQSFilter.Size = new System.Drawing.Size(246, 143);
            this.ux_listBoxQSFilter.TabIndex = 22;
            // 
            // ux_groupboxQSEntryList
            // 
            this.ux_groupboxQSEntryList.Controls.Add(this.ux_textboxQSEntryList);
            this.ux_groupboxQSEntryList.Controls.Add(this.ux_labelQSEntryListInfo);
            this.ux_groupboxQSEntryList.Location = new System.Drawing.Point(507, 6);
            this.ux_groupboxQSEntryList.Name = "ux_groupboxQSEntryList";
            this.ux_groupboxQSEntryList.Size = new System.Drawing.Size(289, 213);
            this.ux_groupboxQSEntryList.TabIndex = 16;
            this.ux_groupboxQSEntryList.TabStop = false;
            this.ux_groupboxQSEntryList.Text = "Entry List";
            // 
            // ux_textboxQSEntryList
            // 
            this.ux_textboxQSEntryList.AllowDrop = true;
            this.ux_textboxQSEntryList.Dock = System.Windows.Forms.DockStyle.Top;
            this.ux_textboxQSEntryList.Location = new System.Drawing.Point(3, 16);
            this.ux_textboxQSEntryList.MaxLength = 0;
            this.ux_textboxQSEntryList.Multiline = true;
            this.ux_textboxQSEntryList.Name = "ux_textboxQSEntryList";
            this.ux_textboxQSEntryList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxQSEntryList.Size = new System.Drawing.Size(283, 178);
            this.ux_textboxQSEntryList.TabIndex = 19;
            this.ux_textboxQSEntryList.TextChanged += new System.EventHandler(this.ux_textboxQSEntryList_TextChanged);
            // 
            // ux_labelQSEntryListInfo
            // 
            this.ux_labelQSEntryListInfo.AutoSize = true;
            this.ux_labelQSEntryListInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ux_labelQSEntryListInfo.Location = new System.Drawing.Point(3, 197);
            this.ux_labelQSEntryListInfo.Name = "ux_labelQSEntryListInfo";
            this.ux_labelQSEntryListInfo.Size = new System.Drawing.Size(91, 13);
            this.ux_labelQSEntryListInfo.TabIndex = 7;
            this.ux_labelQSEntryListInfo.Tag = "Entry list rows: {0}";
            this.ux_labelQSEntryListInfo.Text = "Entry list rows: {0}";
            // 
            // ux_groupboxQSCrop
            // 
            this.ux_groupboxQSCrop.Controls.Add(this.ux_listBoxQSCrop);
            this.ux_groupboxQSCrop.Location = new System.Drawing.Point(8, 6);
            this.ux_groupboxQSCrop.Name = "ux_groupboxQSCrop";
            this.ux_groupboxQSCrop.Size = new System.Drawing.Size(200, 162);
            this.ux_groupboxQSCrop.TabIndex = 16;
            this.ux_groupboxQSCrop.TabStop = false;
            this.ux_groupboxQSCrop.Text = "Crop";
            // 
            // ux_listBoxQSCrop
            // 
            this.ux_listBoxQSCrop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_listBoxQSCrop.FormattingEnabled = true;
            this.ux_listBoxQSCrop.Location = new System.Drawing.Point(3, 16);
            this.ux_listBoxQSCrop.Name = "ux_listBoxQSCrop";
            this.ux_listBoxQSCrop.Size = new System.Drawing.Size(194, 143);
            this.ux_listBoxQSCrop.TabIndex = 22;
            // 
            // ux_tabpageBasicQuery
            // 
            this.ux_tabpageBasicQuery.Location = new System.Drawing.Point(4, 22);
            this.ux_tabpageBasicQuery.Name = "ux_tabpageBasicQuery";
            this.ux_tabpageBasicQuery.Padding = new System.Windows.Forms.Padding(3);
            this.ux_tabpageBasicQuery.Size = new System.Drawing.Size(876, 658);
            this.ux_tabpageBasicQuery.TabIndex = 1;
            this.ux_tabpageBasicQuery.Text = "Temp";
            this.ux_tabpageBasicQuery.UseVisualStyleBackColor = true;
            // 
            // ux_contextmenustripDGVResultsColumnHeader
            // 
            this.ux_contextmenustripDGVResultsColumnHeader.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sortAscendingToolStripMenuItem,
            this.sortDescendingToolStripMenuItem,
            this.noSortToolStripMenuItem,
            this.toolStripSeparator1,
            this.resetAllToolStripMenuItem,
            this.toolStripSeparator2,
            this.removeColumnMenuItem,
            this.removeAllColumnsMenuItem,
            this.toolStripSeparator3,
            this.columnFrequencyMenuItem});
            this.ux_contextmenustripDGVResultsColumnHeader.Name = "uxcmsDGVColumnHeader";
            this.ux_contextmenustripDGVResultsColumnHeader.Size = new System.Drawing.Size(232, 176);
            // 
            // sortAscendingToolStripMenuItem
            // 
            this.sortAscendingToolStripMenuItem.Name = "sortAscendingToolStripMenuItem";
            this.sortAscendingToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.sortAscendingToolStripMenuItem.Text = "Sort Ascending";
            this.sortAscendingToolStripMenuItem.Visible = false;
            // 
            // sortDescendingToolStripMenuItem
            // 
            this.sortDescendingToolStripMenuItem.Name = "sortDescendingToolStripMenuItem";
            this.sortDescendingToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.sortDescendingToolStripMenuItem.Text = "Sort Descending";
            this.sortDescendingToolStripMenuItem.Visible = false;
            // 
            // noSortToolStripMenuItem
            // 
            this.noSortToolStripMenuItem.Name = "noSortToolStripMenuItem";
            this.noSortToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.noSortToolStripMenuItem.Text = "No Sort On This Column";
            this.noSortToolStripMenuItem.Visible = false;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(228, 6);
            this.toolStripSeparator1.Visible = false;
            // 
            // resetAllToolStripMenuItem
            // 
            this.resetAllToolStripMenuItem.Name = "resetAllToolStripMenuItem";
            this.resetAllToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.resetAllToolStripMenuItem.Text = "Reset Sorting For All Columns";
            this.resetAllToolStripMenuItem.Visible = false;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(228, 6);
            this.toolStripSeparator2.Visible = false;
            // 
            // removeColumnMenuItem
            // 
            this.removeColumnMenuItem.Name = "removeColumnMenuItem";
            this.removeColumnMenuItem.Size = new System.Drawing.Size(231, 22);
            this.removeColumnMenuItem.Text = "Remove Column";
            this.removeColumnMenuItem.Click += new System.EventHandler(this.removeColumnMenuItem_Click);
            // 
            // removeAllColumnsMenuItem
            // 
            this.removeAllColumnsMenuItem.Name = "removeAllColumnsMenuItem";
            this.removeAllColumnsMenuItem.Size = new System.Drawing.Size(231, 22);
            this.removeAllColumnsMenuItem.Text = "Remove All Columns";
            this.removeAllColumnsMenuItem.Click += new System.EventHandler(this.removeAllColumnsMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(228, 6);
            // 
            // columnFrequencyMenuItem
            // 
            this.columnFrequencyMenuItem.Name = "columnFrequencyMenuItem";
            this.columnFrequencyMenuItem.Size = new System.Drawing.Size(231, 22);
            this.columnFrequencyMenuItem.Text = "Frequency";
            this.columnFrequencyMenuItem.Click += new System.EventHandler(this.columnFrequencyMenuItem_Click);
            // 
            // ux_contextmenustripDGVResultsCell
            // 
            this.ux_contextmenustripDGVResultsCell.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyAllToolStripMenuItem});
            this.ux_contextmenustripDGVResultsCell.Name = "uxcmsDGVCell";
            this.ux_contextmenustripDGVResultsCell.Size = new System.Drawing.Size(118, 26);
            // 
            // copyAllToolStripMenuItem
            // 
            this.copyAllToolStripMenuItem.Name = "copyAllToolStripMenuItem";
            this.copyAllToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.copyAllToolStripMenuItem.Text = "Copy all";
            this.copyAllToolStripMenuItem.Click += new System.EventHandler(this.copyAllToolStripMenuItem_Click);
            // 
            // SearchWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 684);
            this.Controls.Add(this.ux_tabcontrolMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "SearchWizard";
            this.Text = "Search Wizard";
            this.Load += new System.EventHandler(this.SearchWizard_Load);
            this.ux_tabcontrolMain.ResumeLayout(false);
            this.ux_tabPageSearchByDescriptor.ResumeLayout(false);
            this.ux_groupboxSearchCriteria.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_dataGridViewCriteria)).EndInit();
            this.ux_groupboxSearchResults.ResumeLayout(false);
            this.ux_groupboxSearchResults.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_numericupdownMaxRecords)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewQueryResults)).EndInit();
            this.ux_panelDescriptorSearch.ResumeLayout(false);
            this.ux_groupboxMoreOptions.ResumeLayout(false);
            this.ux_groupboxMoreOptions.PerformLayout();
            this.ux_groupboxOtherDescriptors.ResumeLayout(false);
            this.ux_groupboxOtherDescriptors.PerformLayout();
            this.ux_panelFrequency.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewFrecuency)).EndInit();
            this.ux_groupboxCrop.ResumeLayout(false);
            this.ux_groupboxCrop.PerformLayout();
            this.ux_tabPageQuickSearch.ResumeLayout(false);
            this.ux_tabPageQuickSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_dataGridViewQSResults)).EndInit();
            this.ux_groupboxQSSearchOperator.ResumeLayout(false);
            this.ux_groupboxQSSearchOperator.PerformLayout();
            this.ux_groupboxQSWhereToSearch.ResumeLayout(false);
            this.ux_groupboxQSEntryList.ResumeLayout(false);
            this.ux_groupboxQSEntryList.PerformLayout();
            this.ux_groupboxQSCrop.ResumeLayout(false);
            this.ux_contextmenustripDGVResultsColumnHeader.ResumeLayout(false);
            this.ux_contextmenustripDGVResultsCell.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl ux_tabcontrolMain;
        private System.Windows.Forms.TabPage ux_tabpageBasicQuery;
        private System.Windows.Forms.TabPage ux_tabPageSearchByDescriptor;
        private System.Windows.Forms.GroupBox ux_groupboxSearchCriteria;
        private System.Windows.Forms.DataGridView ux_dataGridViewCriteria;
        private System.Windows.Forms.TextBox ux_textBoxOutput;
        private System.Windows.Forms.Label ux_labelResultsRows;
        private System.Windows.Forms.Button ux_buttonRemoveCriteria;
        private System.Windows.Forms.Button ux_buttonDescriptorSearch;
        private System.Windows.Forms.GroupBox ux_groupboxCrop;
        private System.Windows.Forms.ComboBox ux_comboBoxCrop;
        private System.Windows.Forms.Label ux_labelCropName;
        private System.Windows.Forms.Panel ux_panelDescriptorSearch;
        private System.Windows.Forms.GroupBox ux_groupboxMoreOptions;
        private System.Windows.Forms.ComboBox ux_comboboxMethod;
        private System.Windows.Forms.CheckBox ux_checkboxIsArchived;
        private System.Windows.Forms.GroupBox ux_groupboxOtherDescriptors;
        private System.Windows.Forms.Panel ux_panelFrequency;
        private System.Windows.Forms.DataGridView ux_datagridviewFrecuency;
        private System.Windows.Forms.Button ux_buttonFrequency;
        private System.Windows.Forms.Button ux_buttonShowDescription;
        private System.Windows.Forms.ListBox ux_listboxCategory;
        private System.Windows.Forms.ListBox ux_listboxTrait;
        private System.Windows.Forms.TextBox ux_textBoxPassportValue;
        private System.Windows.Forms.Button ux_buttonAddCriteria;
        private System.Windows.Forms.ListBox ux_listBoxPassportValue;
        private System.Windows.Forms.Label ux_labelValue;
        private System.Windows.Forms.ListBox ux_listBoxValue;
        private System.Windows.Forms.Label ux_labelOperator;
        private System.Windows.Forms.ListBox ux_listboxOperator;
        private System.Windows.Forms.Label ux_labelTrait;
        private System.Windows.Forms.Label ux_labelCategory;
        private System.Windows.Forms.TabPage ux_tabPageQuickSearch;
        private System.Windows.Forms.DataGridView ux_dataGridViewQSResults;
        private System.Windows.Forms.Button ux_buttonQSSearch;
        private System.Windows.Forms.TextBox ux_textboxQSEntryList;
        private System.Windows.Forms.GroupBox ux_groupboxQSSearchOperator;
        private System.Windows.Forms.RadioButton ux_radiobuttonQSContains;
        private System.Windows.Forms.RadioButton ux_radiobuttonQSEquals;
        private System.Windows.Forms.RadioButton ux_radiobuttonQSStartsWith;
        private System.Windows.Forms.GroupBox ux_groupboxQSWhereToSearch;
        private System.Windows.Forms.GroupBox ux_groupboxQSCrop;
        private System.Windows.Forms.Label ux_labelQSResultsRows;
        private System.Windows.Forms.Label ux_labelQSEntryListInfo;
        private System.Windows.Forms.ListBox ux_listBoxQSCrop;
        private System.Windows.Forms.GroupBox ux_groupboxQSEntryList;
        private System.Windows.Forms.ListBox ux_listBoxQSFilter;
        private System.Windows.Forms.Button ux_buttonAddDescriptorToResults;
        private System.Windows.Forms.GroupBox ux_groupboxSearchResults;
        private System.Windows.Forms.DataGridView ux_datagridviewQueryResults;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ContextMenuStrip ux_contextmenustripDGVResultsColumnHeader;
        private System.Windows.Forms.ToolStripMenuItem sortAscendingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sortDescendingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noSortToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem resetAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem removeColumnMenuItem;
        private System.Windows.Forms.CheckBox ux_checkboxQSOnlyFound;
        private System.Windows.Forms.CheckBox ux_checkboxIncludeMethod;
        private System.Windows.Forms.Button ux_buttonRefreshSearchCriteria;
        private System.Windows.Forms.NumericUpDown ux_numericupdownMaxRecords;
        private System.Windows.Forms.Label ux_labelMaxRecords;
        private System.Windows.Forms.TextBox ux_textboxFindDescriptor;
        private System.Windows.Forms.Label ux_labelQSResultsInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn category;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptor;
        private System.Windows.Forms.DataGridViewTextBoxColumn search_operator;
        private System.Windows.Forms.DataGridViewTextBoxColumn values;
        private System.Windows.Forms.DataGridViewTextBoxColumn trait_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fieldname;
        private System.Windows.Forms.DataGridViewTextBoxColumn value;
        private System.Windows.Forms.ToolStripMenuItem removeAllColumnsMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem columnFrequencyMenuItem;
        private System.Windows.Forms.ContextMenuStrip ux_contextmenustripDGVResultsCell;
        private System.Windows.Forms.ToolStripMenuItem copyAllToolStripMenuItem;
        private System.Windows.Forms.Label ux_labelTimeElapsed;
    }
}