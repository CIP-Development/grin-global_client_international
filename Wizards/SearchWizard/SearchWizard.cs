﻿using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SearchWizard
{
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    public partial class SearchWizard : Form, IGRINGlobalDataWizard
    {
        bool escapeKeyPressed = false;
        DataSet GUIData;
        DataSet queryResults = new DataSet();
        TreeView uxtvTableFields;
        string DataGridViewSortOrder = "";
        string DataGridViewFilter = "";
        int MouseClickRowIndex = -1;
        int MouseClickColumnIndex = -1;
        int txtBoxSelectionStart = 0;
        int txtBoxSelectionLength = 0;
        string searchResultType = "accession";
        // Attach to the GRIN-Global web service...
        //GRINGlobalGUIWebServices.GUI GUIWebServices = new GRINGlobalGUIWebServices.GUI();
        DataRow[] basicQuerySearchResults;
        GRINGlobal.Client.Common.SharedUtils _sharedUtils;
        Cursor _cursorGG;

        DataTable _dtSettings;
        DataTable dtPassportCriteria;
        DataTable dtSearchQuery;
        List<KeyValuePair<string, string>> operatorSourceCHAR;
        List<KeyValuePair<string, string>> operatorSourceNUMERIC;
        List<KeyValuePair<string, string>> operatorSourceBASIC;

        DataTable _dtResults = new DataTable();

        private Timer textChangeDelayTimer = new Timer();
        DataTable _dtDescriptor = new DataTable();
        Dictionary<string, string> dicQSResultColumnName = new Dictionary<string, string>();

        private bool _isCustomModificationNeeded = false;

        string PassportCategory = string.Empty;
        public SearchWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();

            _sharedUtils = sharedUtils;
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }

        public string FormName
        {
            get
            {
                return "Search Wizard";
            }
        }

        public string PKeyName
        {
            get
            {
                return "";
            }
        }

        private void SearchWizard_Load(object sender, EventArgs e)
        {
            try
            {
                _isCustomModificationNeeded = _sharedUtils.GetAppSettingValue("INSTITUTE").Equals("CIP") ? true : false;

                _cursorGG = Cursors.Default;
                ux_tabcontrolMain.TabPages.RemoveAt(2);
                ux_dataGridViewCriteria.Columns["trait_id"].Visible = false;
                ux_dataGridViewCriteria.Columns["fieldname"].Visible = false;
                ux_dataGridViewCriteria.Columns["value"].Visible = false;

                // Get language translations
                if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
                if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);
                // Save messages translations 
                if (!string.IsNullOrWhiteSpace(ux_labelQSEntryListInfo.Text)) ux_labelQSEntryListInfo.Tag = ux_labelQSEntryListInfo.Text;
                if (!string.IsNullOrWhiteSpace(ux_labelQSResultsRows.Text)) ux_labelQSResultsRows.Tag = ux_labelQSResultsRows.Text;
                if (!string.IsNullOrWhiteSpace(ux_labelQSResultsInfo.Text)) ux_labelQSResultsInfo.Tag = ux_labelQSResultsInfo.Text;
                if (!string.IsNullOrWhiteSpace(ux_labelResultsRows.Text)) ux_labelResultsRows.Tag = ux_labelResultsRows.Text;
                if (!string.IsNullOrWhiteSpace(ux_labelTimeElapsed.Text)) ux_labelTimeElapsed.Tag = ux_labelTimeElapsed.Text;

                ux_labelQSResultsRows.Text = string.Format(ux_labelQSResultsRows.Tag.ToString(), 0, 0);
                ux_labelQSEntryListInfo.Text = string.Format(ux_labelQSEntryListInfo.Tag.ToString(), 0);
                ux_labelResultsRows.Text = string.Format(ux_labelResultsRows.Tag.ToString(), 0, 0);
                ux_labelTimeElapsed.Visible = false;

                #region LoadInfo
                PassportCategory = _sharedUtils.GetLookupDisplayMember("code_value_lookup", "PassportDescriptors", "SEARCH_WIZARD_STRINGS", "Passport Descriptors");

                // Get Passport Descriptors 
                DataSet passportCriteria = _sharedUtils.GetWebServiceData("search_wizard_get_passport_descriptors", "", 0, 0);
                if (passportCriteria != null && passportCriteria.Tables.Contains("search_wizard_get_passport_descriptors"))
                {
                    dtPassportCriteria = passportCriteria.Tables["search_wizard_get_passport_descriptors"];
                    //Make sure display_member column is editable
                    dtPassportCriteria.Columns["title"].ReadOnly = false;
                    //Update display_member based on value_member
                    foreach (DataRow drDescriptor in dtPassportCriteria.Rows)
                    {
                        drDescriptor["title"] = _sharedUtils.GetLookupDisplayMember("code_value_lookup", drDescriptor.Field<string>("code"),
                                                                                                "MULTI_CROP_PASSPORT_DESCRIPTORS",
                                                                                                drDescriptor["title"].ToString());
                    }
                    dtPassportCriteria.DefaultView.Sort = "title ASC";
                }

                // Populate 'Crop' dropdownlists
                DataTable crop = _sharedUtils.GetLocalData(@"select value_member, display_member from crop_lookup", "");
                if (crop.Rows.Count > 0)
                {
                    crop.DefaultView.Sort = "value_member ASC";
                    ux_comboBoxCrop.ValueMember = "value_member";
                    ux_comboBoxCrop.DisplayMember = "display_member";
                    ux_comboBoxCrop.DataSource = crop;

                    ux_listBoxQSCrop.ValueMember = "value_member";
                    ux_listBoxQSCrop.DisplayMember = "display_member";
                    ux_listBoxQSCrop.DataSource = crop.Copy();
                }

                DataTable dtAccessionName = _sharedUtils.GetLocalData("select value_member, display_member, group_name from code_value_lookup where group_name = 'ACCESSION_NAME_TYPE'", "");
                if(dtAccessionName.Columns.Count == 0)
                {
                    dtAccessionName.Columns.Add("value_member", typeof(string));
                    dtAccessionName.Columns.Add("display_member", typeof(string));
                    dtAccessionName.Columns.Add("group_name", typeof(string));
                }
                DataRow drAccessionNameNew = dtAccessionName.NewRow();
                drAccessionNameNew.SetField<string>("value_member", "ACCESSIONNUMBER");
                drAccessionNameNew.SetField<string>("display_member", _sharedUtils.GetLookupDisplayMember("code_value_lookup", "AccessionNumber", "SEARCH_WIZARD_STRINGS", "Accession Number"));
                drAccessionNameNew.SetField<string>("group_name", "CUSTOM");
                dtAccessionName.Rows.InsertAt(drAccessionNameNew, 0);
                if (dtAccessionName.Rows.Count > 0)
                {
                    ux_listBoxQSFilter.ValueMember = "value_member";
                    ux_listBoxQSFilter.DisplayMember = "display_member";
                    ux_listBoxQSFilter.DataSource = dtAccessionName;
                }

                /*DataTable cropRTA = _sharedUtils.GetLocalData(@"select value_member, display_member from crop_lookup where GroupCrop = 'ARTC'", "");
                DataRow n = cropRTA.NewRow();
                n["value_member"] = "0";
                n["display_member"] = "[All]";
                cropRTA.Rows.Add(n);
                if (cropRTA.Rows.Count > 0)
                {
                    cropRTA.DefaultView.Sort = "display_member ASC";
                    ux_comboBoxCropRTA.ValueMember = "value_member";
                    ux_comboBoxCropRTA.DisplayMember = "display_member";
                    ux_comboBoxCropRTA.DataSource = cropRTA;
                }
                */

                operatorSourceNUMERIC = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("In", _sharedUtils.GetLookupDisplayMember("code_value_lookup", "EqualTo", "SEARCH_WIZARD_STRINGS", "Equal To")),
                    new KeyValuePair<string, string>(">=", _sharedUtils.GetLookupDisplayMember("code_value_lookup", "GreaterThanOrEqualTo", "SEARCH_WIZARD_STRINGS", "Greater Than or Equal To")),
                    new KeyValuePair<string, string>("<=", _sharedUtils.GetLookupDisplayMember("code_value_lookup", "LessThanOrEqualTo", "SEARCH_WIZARD_STRINGS", "Less Than or Equal To")),
                    new KeyValuePair<string, string>("Not in", _sharedUtils.GetLookupDisplayMember("code_value_lookup", "NotEqualTo", "SEARCH_WIZARD_STRINGS", "Not Equal To")),
                };
                operatorSourceCHAR = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("In", _sharedUtils.GetLookupDisplayMember("code_value_lookup", "EqualTo", "SEARCH_WIZARD_STRINGS", "Equal to")),
                    new KeyValuePair<string, string>("Contains", _sharedUtils.GetLookupDisplayMember("code_value_lookup", "Contains", "SEARCH_WIZARD_STRINGS", "Contains")),
                    new KeyValuePair<string, string>("Starts with", _sharedUtils.GetLookupDisplayMember("code_value_lookup", "StartsWith", "SEARCH_WIZARD_STRINGS", "Starts with")),
                    new KeyValuePair<string, string>("<>", _sharedUtils.GetLookupDisplayMember("code_value_lookup", "NotEqualTo", "SEARCH_WIZARD_STRINGS", "Not equal to")),
                };
                operatorSourceBASIC = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("In", _sharedUtils.GetLookupDisplayMember("code_value_lookup", "EqualTo", "SEARCH_WIZARD_STRINGS", "Equal to")),
                    new KeyValuePair<string, string>("Not in", _sharedUtils.GetLookupDisplayMember("code_value_lookup", "NotEqualTo", "SEARCH_WIZARD_STRINGS", "Not equal to")),
                };
                ux_listboxOperator.DataSource = operatorSourceBASIC;
                ux_listboxOperator.ValueMember = "Key";
                ux_listboxOperator.DisplayMember = "Value";

                DataSet dsCriteriaTemplate = _sharedUtils.GetWebServiceData("search_wizard_get_template_criteria", "", 0, 0);
                if (dsCriteriaTemplate != null && dsCriteriaTemplate.Tables.Contains("search_wizard_get_template_criteria"))
                {
                    DataTable dt = dsCriteriaTemplate.Tables["search_wizard_get_template_criteria"];
                    ux_dataGridViewCriteria.Columns["category"].HeaderText = GetFriendlyFieldNameFromDataTable(dt, "category", "Category");
                    ux_dataGridViewCriteria.Columns["descriptor"].HeaderText = GetFriendlyFieldNameFromDataTable(dt, "descriptor", "Descriptor");
                    ux_dataGridViewCriteria.Columns["search_operator"].HeaderText = GetFriendlyFieldNameFromDataTable(dt, "search_operator", "Operator");
                    ux_dataGridViewCriteria.Columns["values"].HeaderText = GetFriendlyFieldNameFromDataTable(dt, "values", "Values");
                }
                /*else
                    throw new WizardException("Error getting \"search_wizard_get_template_criteria\" dataview", "ux_textboxErrorGettingDataview", "search_wizard_get_template_criteria");
                */

                DataSet dsQSResultsTemplate = _sharedUtils.GetWebServiceData("search_wizard_get_template_quicksearch_results", "", 0, 0);
                if (dsQSResultsTemplate != null && dsQSResultsTemplate.Tables.Contains("search_wizard_get_template_quicksearch_results"))
                {
                    DataTable dt = dsQSResultsTemplate.Tables["search_wizard_get_template_quicksearch_results"];
                    dicQSResultColumnName.Add("entry", GetFriendlyFieldNameFromDataTable(dt, "entry", "Entry"));
                    dicQSResultColumnName.Add("is_found", GetFriendlyFieldNameFromDataTable(dt, "is_found", "Is found"));
                    dicQSResultColumnName.Add("filter", GetFriendlyFieldNameFromDataTable(dt, "filter", "Filter"));
                }
                else
                {
                    dicQSResultColumnName.Add("entry", "Entry");
                    dicQSResultColumnName.Add("is_found", "Is found");
                    dicQSResultColumnName.Add("filter", "Filter");
                }
                
                #endregion

                ux_radiobuttonQSEquals.Checked = true;

                DataSet dsSettings = _sharedUtils.GetWebServiceData("search_wizard_settings", "", 0, 0);
                if (dsSettings != null && dsSettings.Tables.Contains("search_wizard_settings"))
                {
                    _dtSettings = dsSettings.Tables["search_wizard_settings"];
                }
                else
                    throw new WizardException("Error getting \"search_wizard_settings\" dataview", "ux_textboxErrorGettingDataview", "search_wizard_settings");

                _dtResults.Columns.Add("accession_id", typeof(int));
                _dtResults.Columns.Add("accession_number", typeof(string));
                ux_datagridviewQueryResults.DataSource = _dtResults;
                ux_datagridviewQueryResults.Columns["accession_id"].HeaderText = _sharedUtils.GetLookupDisplayMember("code_value_lookup", "AccessionId", "SEARCH_WIZARD_STRINGS", "Accession Id");
                ux_datagridviewQueryResults.Columns["accession_id"].ContextMenuStrip = ux_contextmenustripDGVResultsCell;
                ux_datagridviewQueryResults.Columns["accession_number"].HeaderText = _sharedUtils.GetLookupDisplayMember("code_value_lookup", "AccessionNumber", "SEARCH_WIZARD_STRINGS", "Accession Number");
                ux_datagridviewQueryResults.Columns["accession_number"].ContextMenuStrip = ux_contextmenustripDGVResultsCell;

                textChangeDelayTimer.Tick += new EventHandler(timerDelay_Tick);
            }
            catch (WizardException wex)
            {
                Cursor.Current = Cursors.Arrow;
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox("\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }
        private string GetFriendlyFieldNameFromDataTable(DataTable dt, string fieldName,  string defaultFriendlyName)
        {
            string friendlyFieldName = defaultFriendlyName;
            if (dt.Columns.Contains(fieldName))
            {
                DataColumn dc = dt.Columns[fieldName];
                // Try to find the friendly_field_name first...
                if (dc.ExtendedProperties.Contains("friendly_field_name") &&
                    dc.ExtendedProperties["friendly_field_name"].ToString().Length > 0)
                {
                    friendlyFieldName = dc.ExtendedProperties["friendly_field_name"].ToString();
                }
                // Then try to find the title next...
                if (dc.ExtendedProperties.Contains("title") &&
                    dc.ExtendedProperties["title"].ToString().Length > 0)
                {
                    friendlyFieldName = dc.ExtendedProperties["title"].ToString();
                }
                // Otherwise the caption property should have the friendly name
                else if (dc.Caption.Length > 0)
                {
                    friendlyFieldName = dc.Caption;
                }
                // Fallback to the ColumnName if all else fails...
                else
                {
                    friendlyFieldName = dc.ColumnName;
                }
                return friendlyFieldName;
            }
            return defaultFriendlyName;
        }

        private void setListByValue(ref System.Windows.Forms.ComboBox objList, string strValue)
        {
            //int i =0;

            for (int i = 0; i < objList.Items.Count; i++)
            {
                string value = objList.GetItemText(objList.Items[i]);
                if (value.ToUpper() == strValue.ToUpper())
                {
                    objList.SelectedIndex = i;
                    break;
                }
            }


        }

        private void ux_buttonQSSearch_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            ux_dataGridViewQSResults.DataSource = null;

            try
            {
                if (string.IsNullOrWhiteSpace(ux_textboxQSEntryList.Text))
                    throw new WizardException("Entry list is empty", "ux_textboxErrorEmptyList", new string[] { "dataviewname", 1.ToString()});
                if(ux_listBoxQSFilter.SelectedItems.Count == 0)
                {
                    throw new WizardException("Select at least one filter to search by", "ux_textboxErrorEmptyFilter");
                }

                //Format entries
                var lines = ux_textboxQSEntryList.Lines;
                for (int i = 0; i < lines.Length; i++)
                {
                    lines[i] = System.Text.RegularExpressions.Regex.Replace(lines[i].Trim(), @"\p{C}+", string.Empty);
                }
                ux_textboxQSEntryList.Lines = lines;

                //Clear
                ux_dataGridViewQSResults.DataSource = null;
                ux_labelQSResultsRows.Text = string.Format(ux_labelQSResultsRows.Tag.ToString(), 0, 0);
                ux_labelQSResultsInfo.Text = string.Empty;
                ux_labelQSResultsInfo.Visible = false;

                var watch = System.Diagnostics.Stopwatch.StartNew();

                DataTable dtResults = new DataTable();
                dtResults.Columns.Add("entry", typeof(string));
                dtResults.Columns.Add("is_found", typeof(string));
                dtResults.Columns.Add("filter", typeof(string));
                dtResults.Columns.Add("accession_id", typeof(int));

                bool IsCropEmpty = ux_listBoxQSCrop.SelectedValue == null ? false : true;
                bool IsEqualsOperator = (SearchOperator)ux_groupboxQSSearchOperator.Tag == SearchOperator.Equals ? true : false;

                DataTable result = new DataTable();
                foreach (DataRowView item in ux_listBoxQSFilter.SelectedItems)
                {
                    if (item.Row.Field<string>("group_name").Equals("CUSTOM") && item.Row.Field<string>("value_member").Equals("ACCESSIONNUMBER"))
                    {
                        DataTable all = new DataTable();
                        all.Columns.Add("ID", typeof(Int32));

                        //Check if operator is Equals
                        if (IsEqualsOperator)
                        {
                            if (_isCustomModificationNeeded)
                            {
                                List<KeyValuePair<string, string>> entryKeyValueList = new List<KeyValuePair<string, string>>();
                                foreach (string entry in ux_textboxQSEntryList.Lines)
                                {
                                    if (string.IsNullOrEmpty(entry)) continue;
                                    entryKeyValueList.Add(new KeyValuePair<string, string>(entry, "CIP " + entry.Replace("CIP", string.Empty).Trim() ));
                                }
                                
                                string searchParameters = ":cropid=" + (ux_listBoxQSCrop.SelectedValue == null ? string.Empty : ux_listBoxQSCrop.SelectedValue) + ";";
                                searchParameters += ":accessionnumber=" + string.Join("','", entryKeyValueList.AsEnumerable().Select(x => x.Value));

                                DataSet dsPartialResult = _sharedUtils.GetWebServiceData("search_wizard_get_accession_by_accession_number", searchParameters, 0, 0);
                                if (dsPartialResult != null && dsPartialResult.Tables.Contains("search_wizard_get_accession_by_accession_number"))
                                {
                                    DataTable dtPartialResult = dsPartialResult.Tables["search_wizard_get_accession_by_accession_number"];

                                    foreach (var entryKeyValue in entryKeyValueList)
                                    {
                                        var drResult = dtPartialResult.AsEnumerable().FirstOrDefault(x => x.Field<string>("accession_number").Equals(entryKeyValue.Value));
                                        if (drResult != null)
                                        {
                                            dtResults.Rows.Add(entryKeyValue.Key, "Y", item.Row.Field<string>("display_member"), drResult.Field<int>("accession_id"));
                                        }
                                        else
                                            dtResults.Rows.Add(entryKeyValue.Key, "N", item.Row.Field<string>("display_member"), DBNull.Value);
                                    }
                                    dtPartialResult.Columns.Remove("accession_number");
                                    dtPartialResult.Columns["accession_id"].ColumnName = "ID";
                                    all.Merge(dtPartialResult);
                                }
                            }
                            else {
                                var entryList = ux_textboxQSEntryList.Lines.AsEnumerable().Where(x => !string.IsNullOrEmpty(x)).ToList();
                                string searchParameters = ":cropid=" + (ux_listBoxQSCrop.SelectedValue == null ? string.Empty : ux_listBoxQSCrop.SelectedValue) + ";";
                                searchParameters += ":accessionnumber=" + string.Join("','", entryList);

                                DataSet dsPartialResult = _sharedUtils.GetWebServiceData("search_wizard_get_accession_by_accession_number", searchParameters, 0, 0);
                                if (dsPartialResult != null && dsPartialResult.Tables.Contains("search_wizard_get_accession_by_accession_number"))
                                {
                                    DataTable dtPartialResult = dsPartialResult.Tables["search_wizard_get_accession_by_accession_number"];

                                    foreach (var entry in entryList)
                                    {
                                        var drResult = dtPartialResult.AsEnumerable().FirstOrDefault(x => x.Field<string>("accession_number").Equals(entry));
                                        if (drResult != null)
                                        {
                                            dtResults.Rows.Add(entry, "Y", item.Row.Field<string>("display_member"), drResult.Field<int>("accession_id"));
                                        }
                                        else
                                            dtResults.Rows.Add(entry, "N", item.Row.Field<string>("display_member"), DBNull.Value);
                                    }
                                    dtPartialResult.Columns.Remove("accession_number");
                                    dtPartialResult.Columns["accession_id"].ColumnName = "ID";
                                    all.Merge(dtPartialResult);
                                }
                            }
                        }
                        else
                        {
                            foreach (string line in ux_textboxQSEntryList.Lines)
                            {
                                if (string.IsNullOrEmpty(line)) continue;

                                string searchParameters = ":cropid=" + (ux_listBoxQSCrop.SelectedValue == null ? string.Empty : ux_listBoxQSCrop.SelectedValue) + ";";
                                switch ((SearchOperator)ux_groupboxQSSearchOperator.Tag)
                                {
                                    case SearchOperator.Contains:
                                        searchParameters += ":likeword=%" + line + "%;";
                                        break;
                                    case SearchOperator.StartsWith:
                                        searchParameters += ":likeword=" + line + "%;";
                                        break;
                                    case SearchOperator.Equals:
                                        searchParameters += ":likeword=" + line + ";";
                                        break;
                                }

                                DataSet dsResults = _sharedUtils.GetWebServiceData("search_wizard_get_accession_by_word", searchParameters, 0, 0);
                                if (dsResults != null && dsResults.Tables.Contains("search_wizard_get_accession_by_word"))
                                {
                                    if (dsResults.Tables["search_wizard_get_accession_by_word"].Rows.Count > 0)
                                    {
                                        foreach (DataRow drResult in dsResults.Tables["search_wizard_get_accession_by_word"].Rows)
                                        {
                                            dtResults.Rows.Add(line, "Y", item.Row.Field<string>("display_member"), drResult.Field<int>("ID"));
                                        }
                                        all.Merge(dsResults.Tables["search_wizard_get_accession_by_word"]);
                                    }
                                    else
                                    {
                                        dtResults.Rows.Add(line, "N", item.Row.Field<string>("display_member"), DBNull.Value);
                                    }
                                }
                            }
                        }
                        result.Merge(all);
                    }
                    else if (item.Row.Field<string>("group_name").Equals("ACCESSION_NAME_TYPE"))
                    {
                        result.Merge(quickSearch(dtResults, item.Row.Field<string>("display_member"), "@accession_inv_name.category_code='" + item.Row.Field<string>("value_member") + "' AND @accession_inv_name.plant_name"));
                    }
                    else
                    {
                        throw new WizardException("Filter category is not implemented", "ux_textboxErrorFilterCategoryNotImplemented");
                    }
                }
                
                var accessionids = from row in result.AsEnumerable() select row.Field<int>("ID");
                
                DataRow drQuickSearchDataview = _dtSettings.AsEnumerable().FirstOrDefault(x => x.Field<string>("setting_name").Equals("quick_search_dataview_name"));
                if (drQuickSearchDataview != null)
                {
                    string dataviewName = drQuickSearchDataview.Field<string>("setting_value");
                    DataSet dsQuickSearch = _sharedUtils.GetWebServiceData(dataviewName, ":accessionid=" + String.Join(",", accessionids), 0, 0);
                    if (dsQuickSearch != null && dsQuickSearch.Tables.Contains(dataviewName))
                    {
                        DataTable dtQuickSearch = dsQuickSearch.Tables[dataviewName];

                        DataTable dtJoined = DataTableHelper.JoinTwoDataTablesOnOneColumn(dtResults, dtQuickSearch, "accession_id", DataTableHelper.JoinType.Left);
                        _sharedUtils.BuildReadOnlyDataGridView(ux_dataGridViewQSResults, dtQuickSearch);

                        ux_dataGridViewQSResults.DataSource = dtJoined;
                        ux_dataGridViewQSResults.Columns["entry"].DisplayIndex = 0;
                        ux_dataGridViewQSResults.Columns["is_found"].DisplayIndex = 1;
                        ux_dataGridViewQSResults.Columns["filter"].DisplayIndex = 2;
                        ux_dataGridViewQSResults.Columns["entry"].HeaderText = dicQSResultColumnName["entry"];
                        ux_dataGridViewQSResults.Columns["is_found"].HeaderText = dicQSResultColumnName["is_found"];
                        ux_dataGridViewQSResults.Columns["filter"].HeaderText = dicQSResultColumnName["filter"];
                        ux_dataGridViewQSResults.Columns["entry"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        ux_dataGridViewQSResults.Columns["is_found"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        ux_dataGridViewQSResults.Columns["filter"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        //ux_dataGridViewQSResults.AutoResizeColumns();

                        int TotalEntries = ux_textboxQSEntryList.Lines.AsEnumerable().Count(x => !string.IsNullOrEmpty(x));
                        var entryList = new List<string>(); 
                        foreach (string line in ux_textboxQSEntryList.Lines)
                        {
                            if (string.IsNullOrEmpty(line)) continue;
                            entryList.Add(line);
                        }
                        int EntriesNotFound = entryList.AsEnumerable().Count(x => !dtResults.AsEnumerable().Any(y=> y.Field<string>("entry").Equals(x) && y.Field<string>("is_found").Equals("Y")));

                        int totalRows = dtJoined.Rows.Count;
                        if (ux_checkboxQSOnlyFound.Checked)
                        {
                            dtJoined.DefaultView.RowFilter = "is_found = 'Y'";
                        }
                        else
                        {
                            dtJoined.DefaultView.RowFilter = string.Empty;
                        }

                        watch.Stop();
                        ux_labelQSResultsRows.Text = string.Format(ux_labelQSResultsRows.Tag.ToString(), ux_dataGridViewQSResults.Rows.Count, totalRows);
                        ux_labelQSResultsInfo.Visible = true;
                        ux_labelQSResultsInfo.Text = string.Format(ux_labelQSResultsInfo.Tag.ToString(), TotalEntries, EntriesNotFound, watch.ElapsedMilliseconds / 1000);
                    }
                    else
                    {
                        ux_dataGridViewQSResults.DataSource = dtResults;
                        throw new WizardException("Error getting data from \""+ dataviewName +"\" dataview", "ux_textboxErrorGettingDataview", new string[] { dataviewName});
                    }
                }
                else
                    throw new WizardException("quick_search_dataview_name is not defined in \"search_wizard_settings\" dataview", "ux_textboxErrorQuickSearchDataviewNotDefined");

            }
            catch (WizardException wex)
            {
                Cursor.Current = Cursors.Arrow;
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                /*if (!string.IsNullOrEmpty(wex.AppResourceName)) mb.MessageControlName = wex.AppResourceName;
                _sharedUtils.UpdateControls(mb.Controls, "SearchWizardErrorMessageBox");
                if (mb.MessageText.Contains("{0}") && wex.MessageParams.Length > 0) mb.MessageText = string.Format(mb.MessageText, wex.MessageParams);*/
                mb.ShowDialog();
            }
            catch (Exception ex) {
                Cursor.Current = Cursors.Arrow;
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                _sharedUtils.UpdateControls(mb.Controls, "SearchWizardErrorMessageBox");
                mb.ShowDialog();
            }
            finally
            {
                Cursor.Current = Cursors.Arrow;
            }
        }

        private DataTable quickSearch(DataTable dtResults, string filter, string columnQuery)
        {
            DataTable all = new DataTable();
            foreach (string line in ux_textboxQSEntryList.Lines)
            {
                if (string.IsNullOrEmpty(line)) continue;

                string searchString = (ux_listBoxQSCrop.SelectedValue == null ? string.Empty : "@taxonomy_crop_map.crop_id = " + ux_listBoxQSCrop.SelectedValue + " and ") + columnQuery + " ";
                //string searchString = columnQuery + " ";
                switch ((SearchOperator)ux_groupboxQSSearchOperator.Tag)
                {
                    case SearchOperator.Contains:
                        searchString += "like '%" + line + "%'";
                        break;
                    case SearchOperator.StartsWith:
                        searchString += "like '" + line + "%'";
                        break;
                    case SearchOperator.Equals:
                        searchString += "= '" + line + "'";
                        break;
                }

                DataSet dsResults = _sharedUtils.SearchWebService(searchString, true, false, "", "accession", 0, 0, "");
                if (dsResults != null && dsResults.Tables.Contains("SearchResult"))
                {
                    if (dsResults.Tables["SearchResult"].Rows.Count > 0)
                    {
                        foreach (DataRow drResult in dsResults.Tables["SearchResult"].Rows)
                        {
                            dtResults.Rows.Add(line, "Y", filter, drResult.Field<int>("ID"));
                        }
                        all.Merge(dsResults.Tables["SearchResult"]);
                    }
                    else
                    {
                        dtResults.Rows.Add(line, "N", filter, DBNull.Value);
                    }
                }
            }

            return all;
        }

        private void ux_radiobutton_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                if (ux_radiobuttonQSStartsWith.Checked == true) ux_groupboxQSSearchOperator.Tag = SearchOperator.StartsWith;

                if (ux_radiobuttonQSContains.Checked == true) ux_groupboxQSSearchOperator.Tag = SearchOperator.Contains;

                if (ux_radiobuttonQSEquals.Checked == true) ux_groupboxQSSearchOperator.Tag = SearchOperator.Equals;
            }
        }

        private void ux_textboxQSEntryList_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (ux_labelQSEntryListInfo.Tag.ToString().Contains("{0}"))
                    ux_labelQSEntryListInfo.Text = string.Format(ux_labelQSEntryListInfo.Tag.ToString(), ux_textboxQSEntryList.Lines.Length);
                else
                    ux_labelQSEntryListInfo.Text = ux_labelQSEntryListInfo.Tag.ToString().Trim() + " " + ux_textboxQSEntryList.Lines.Length.ToString();
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_dataGridViewQSResults_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {

        }

        private void ux_dataGridViewQSResults_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {

        }

        private void ux_tabcontrolMain_Selecting(object sender, TabControlCancelEventArgs e)
        {
            /*if (e.TabPage == ux_tabPageSearchByDescriptor)
                e.Cancel = true;
                */
        }

        private void ux_dataGridViewQSResults_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataObject doDragDropData;

                if ((e.RowIndex > -1) && (e.ColumnIndex > -1) && (ux_dataGridViewQSResults.Rows[e.RowIndex].Selected) && (e.Button == MouseButtons.Left))
                {
                    // If there were selected rows lets start the drag-drop operation...
                    if (ux_dataGridViewQSResults.SelectedRows.Count > 0)
                    {
                        //Check if accession_id is not empty
                        foreach (DataGridViewRow dgvRow in ux_dataGridViewQSResults.SelectedRows)
                        {
                            if (dgvRow.Cells["accession_id"].Value == null || string.IsNullOrEmpty(dgvRow.Cells["accession_id"].Value.ToString()))
                            {
                                throw new WizardException("Only found entries can be selected", "ux_textboxErrorOnlyFoundEntriesCanBeSelected");
                            }
                        }
                        
                        // Get the string of selected rows extracted from the GridView...
                        doDragDropData = BuildDGVDragAndDropData(ux_dataGridViewQSResults);
                        ux_dataGridViewQSResults.DoDragDrop(doDragDropData, DragDropEffects.Copy);
                    }
                }
                /*if ((e.RowIndex > -1) && (e.ColumnIndex > -1) && (e.Button == MouseButtons.Right))
                {
                    // Change the color of the cell background so that the user
                    // knows what cell the context menu applies to...
                    ux_dataGridViewQSResults.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Red;
                    ux_dataGridViewQSResults.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionBackColor = Color.Red;
                }*/
            }
            catch (WizardException wex)
            {
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        public DataObject BuildDGVDragAndDropData(DataGridView dgv)
        {
            // Depending on how many rows are selected, this operation
            // could take a while - so change the cursor...
            Cursor.Current = Cursors.WaitCursor;

            DataObject dndData = new DataObject();

            // Write the rows out in text format first...
            StringBuilder sb = new StringBuilder("");

            string columnHeader = "";
            string columnNames = "";
            // First, gather the column headers...
            foreach (DataGridViewColumn dgvCol in dgv.Columns)
            {
                if (dgvCol.Visible)
                {
                    columnHeader += dgvCol.HeaderText + "\t";
                    columnNames += dgvCol.Name + "\t";
                }
            }
            // And write it to the string builder...
            sb.Append(columnHeader);
            // Build the array of visible column names...
            string[] columnList = columnNames.Trim().Split('\t');
            // And use that list for writing the values to the string builder...
            foreach (DataGridViewRow dgvr in dgv.SelectedRows)
            {
                sb.Append("\n");
                foreach (string columnName in columnList)
                {
                    sb.Append(dgvr.Cells[columnName].FormattedValue.ToString());
                    sb.Append("\t");
                }
            }

            DataTable dtQuickSearch = ((DataTable)dgv.DataSource).Clone();
            dtQuickSearch.PrimaryKey = new DataColumn[] { dtQuickSearch.Columns["accession_id"]};
            dtQuickSearch.PrimaryKey[0].ExtendedProperties["table_field_name"] = "accession_id";
            dtQuickSearch.PrimaryKey[0].ExtendedProperties["table_name"] = "accession";
            // Finally write it out as a collection of data table rows (for tree view drag and drop).
            DataSet dsData = new DataSet();
            dsData.Tables.Add(dtQuickSearch);
            foreach (DataGridViewRow dgvr in dgv.SelectedRows)
            {
                dsData.Tables[0].Rows.Add(((DataRowView)dgvr.DataBoundItem).Row.ItemArray);
            }

            // Set the data types into the data object and return...
            dndData.SetData(typeof(string), sb.ToString());
            dndData.SetData(typeof(DataSet), dsData);

            // We are all done so change the cursor back and return the data...
            //Cursor.Current = Cursors.Default;
            Cursor.Current = _cursorGG;

            return dndData;
        }

        private void ux_comboBoxCrop_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (ux_comboBoxCrop.DataSource == null) return;

                if (ux_comboBoxCrop.SelectedItem != null)
                {
                    ux_textBoxOutput.Clear();
                    ux_dataGridViewCriteria.Rows.Clear();
                    ux_listboxCategory.DataSource = null;
                    for (int i = _dtResults.Columns.Count - 1; i >= 0; i--)
                    {
                        if (!_dtResults.Columns[i].ColumnName.Equals("accession_id") && !_dtResults.Columns[i].ColumnName.Equals("accession_number"))
                            _dtResults.Columns.RemoveAt(i);
                    }

                    string query = @"select distinct category, b.display_member from search_wizard_crop_trait_lookup a 
join code_value_lookup b on a.category = b.value_member and b.group_name = 'DESCRIPTOR_CATEGORY' where crop_id = ";
                    DataTable localDBCodeCategoryTable = _sharedUtils.GetLocalData(query + ux_comboBoxCrop.SelectedValue.ToString(), "");
                    if(localDBCodeCategoryTable.Columns.Count == 0)
                    {
                        localDBCodeCategoryTable.Columns.Add("category", typeof(string));
                        localDBCodeCategoryTable.Columns.Add("display_member", typeof(string));
                    }

                    DataRow n = localDBCodeCategoryTable.NewRow();
                    n["category"] = "PASSPORT";
                    n["display_member"] = PassportCategory;
                    localDBCodeCategoryTable.Rows.Add(n);

                    if (localDBCodeCategoryTable.Rows.Count > 0)
                    {
                        ux_listboxCategory.ValueMember = "category";
                        ux_listboxCategory.DisplayMember = "display_member";
                        ux_listboxCategory.DataSource = localDBCodeCategoryTable.DefaultView;
                    }

                    //Populate method combobox
                    DataSet dsMethod = _sharedUtils.GetWebServiceData("search_wizard_get_method", ":cropid=" + ux_comboBoxCrop.SelectedValue, 0, 0);
                    if (dsMethod != null && dsMethod.Tables.Contains("search_wizard_get_method"))
                    {
                        ux_comboboxMethod.DataSource = dsMethod.Tables["search_wizard_get_method"];
                        ux_comboboxMethod.ValueMember = "method_id";
                        ux_comboboxMethod.DisplayMember = "name";
                        //ux_comboboxMethod.SelectedItem = null;
                    }
                }
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_listBoxCategory_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (ux_listboxCategory.DataSource == null) return;
                //if (ux_listboxCategory.SelectedItem == null) return;

                ux_listboxTrait.DataSource = null;
                ux_listboxTrait.DataBindings.Clear();

                if (ux_listboxCategory.SelectedItem != null)
                {
                    //Clear
                    ux_listboxTrait.DataSource = null;

                    if (ux_listboxCategory.SelectedValue.ToString().Equals("PASSPORT"))
                    {
                        if (dtPassportCriteria != null)
                        {
                            _dtDescriptor = dtPassportCriteria;

                            //filter list
                            if (string.IsNullOrEmpty(ux_textboxFindDescriptor.Text))
                                _dtDescriptor.DefaultView.RowFilter = string.Empty;
                            else
                                _dtDescriptor.DefaultView.RowFilter = "title like '%" + ux_textboxFindDescriptor.Text + "%'";

                            ux_listboxTrait.ValueMember = "code";
                            ux_listboxTrait.DisplayMember = "title";
                            ux_listboxTrait.DataSource = _dtDescriptor.DefaultView;

                            ux_listboxTrait.SelectionMode = SelectionMode.One;
                        }
                    }
                    else
                    {
                        /*string query;
                        query = "select * from search_wizard_crop_trait_lookup where crop_id = " + ux_comboBoxCrop.SelectedValue.ToString() + " and category = '" + ux_listBoxCategory.SelectedValue.ToString() + "'";

                        _dtDescriptor = _sharedUtils.GetLocalData(query, "");*/
                        if (!ux_checkboxIncludeMethod.Checked || (ux_checkboxIncludeMethod.Checked && ux_comboboxMethod.SelectedValue != null))
                        {
                            string parameters = string.Format(":crop_id={0};:methodid={1};:categorycode={2};",
                                                                ux_comboBoxCrop.SelectedValue,
                                                                ux_checkboxIncludeMethod.Checked ? ux_comboboxMethod.SelectedValue.ToString() : string.Empty,
                                                                ux_listboxCategory.SelectedValue);

                            DataSet dsDescriptorList = _sharedUtils.GetWebServiceData("search_wizard_get_descriptor_list", parameters, 0, 0);
                            if (dsDescriptorList != null && dsDescriptorList.Tables.Contains("search_wizard_get_descriptor_list"))
                            {
                                _dtDescriptor = dsDescriptorList.Tables["search_wizard_get_descriptor_list"];
                            }
                            else
                            {
                                throw new WizardException("Error getting data from \"search_wizard_get_descriptor_list\" dataview", "ux_textboxErrorGettingDataview", "search_wizard_get_descriptor_list");
                            }

                            //filter list
                            if (string.IsNullOrEmpty(ux_textboxFindDescriptor.Text))
                                _dtDescriptor.DefaultView.RowFilter = string.Empty;
                            else
                                _dtDescriptor.DefaultView.RowFilter = "display_member like '%" + ux_textboxFindDescriptor.Text + "%'";

                            _dtDescriptor.DefaultView.Sort = "display_member ASC";

                            ux_listboxTrait.ValueMember = "value_member";
                            ux_listboxTrait.DisplayMember = "display_member";
                            ux_listboxTrait.DataSource = _dtDescriptor.DefaultView;

                            ux_listboxTrait.SelectionMode = SelectionMode.MultiExtended;
                        }
                        else
                        {
                            ux_listboxTrait.DataSource = null;
                        }
                    }
                }
                Cursor.Current = Cursors.Default;
            }
            catch (WizardException wex)
            {
                if(Cursor.Current != Cursors.Default) Cursor.Current = Cursors.Default;
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                if (Cursor.Current != Cursors.Default) Cursor.Current = Cursors.Default;
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_buttonAddCriteria_Click(object sender, EventArgs e)
        {
            try
            {
                var values = "";
                var displayValues = "";
                var fieldname = "";

                if (ux_listboxCategory.SelectedValue.ToString().Equals("PASSPORT"))
                {
                    if (ux_listBoxPassportValue.Visible == true)
                    {
                        for (int i = 0; i < ux_listBoxPassportValue.SelectedIndices.Count; i++)
                        {
                            var item = ux_listBoxPassportValue.Items[ux_listBoxPassportValue.SelectedIndices[i]];
                            var value = "";

                            value = ((DataRowView)item)[ux_listBoxPassportValue.ValueMember].ToString();
                            /*if (ux_listBoxPassportValue.SelectedIndex != 0)
                                values += "'" + value + "'";
                            else
                                values += "null";*/

                            values += "'" + value + "'";
                            displayValues += ((DataRowView)item)[ux_listBoxPassportValue.DisplayMember].ToString();

                            if (i < ux_listBoxPassportValue.SelectedIndices.Count - 1)
                            {
                                values += ",";
                                displayValues += ";";
                            }
                        }
                    }
                    else
                    {
                        values = "'" + ux_textBoxPassportValue.Text + "'";
                        displayValues = values;
                    }
                }
                else //TRAITS
                {
                    if (ux_listBoxValue.Visible == true)
                    {
                        for (int i = 0; i < ux_listBoxValue.SelectedIndices.Count; i++)
                        {
                            var item = ux_listBoxValue.Items[ux_listBoxValue.SelectedIndices[i]];
                            DataRowView drv = (DataRowView)item;
                            var split = drv[ux_listBoxValue.ValueMember].ToString().Split('|');

                            if (fieldname == "")
                                fieldname = split[0];
                            var value = split[1];

                            if (fieldname.Equals("string_value"))
                                values += "'" + value + "'";
                            else
                                values += value;

                            displayValues += ((DataRowView)item)[ux_listBoxValue.DisplayMember].ToString();
                            if (i < ux_listBoxValue.SelectedIndices.Count - 1)
                            {
                                values += ",";
                                displayValues += ";";
                            }
                        }
                    }
                    else
                    {
                        DataRow drDescriptor =  ((DataRowView)ux_listboxTrait.SelectedItem).Row;
                        switch (drDescriptor.Field<string>("type"))
                        {
                            case "NUMERIC":
                                    fieldname = "numeric_value";
                                    break;
                            default:
                                fieldname = "string_value";
                                break;
                        }
                        
                        values = ux_textBoxPassportValue.Text;
                        displayValues = values;
                    }
                }

                //if criteria exists
                int index = -1;
                foreach (DataGridViewRow row in ux_dataGridViewCriteria.Rows)
                {
                    if (row.Cells["trait_id"].Value.ToString() == ux_listboxTrait.SelectedValue.ToString())
                    {
                        index = row.Index;
                    }
                }
                if (index > -1)
                    ux_dataGridViewCriteria.Rows.RemoveAt(index);

                ux_dataGridViewCriteria.Rows.Add(ux_listboxCategory.Text,
                    ux_listboxTrait.Text, ux_listboxOperator.SelectedValue.ToString(), displayValues,
                    ux_listboxTrait.SelectedValue.ToString(), fieldname.Equals("") ? (ux_listboxTrait.SelectedItem as DataRowView).Row["name"].ToString() : fieldname, values);

                ux_dataGridViewCriteria.AutoResizeColumns();

                // Refresh query
                BuildQuery();
            }
            catch (WizardException wex)
            {
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
                ux_textBoxOutput.Clear();
            }
        }
        private void BuildQuery()
        {
            /*Create query*/
            ux_textBoxOutput.Clear();

            List<string> listTraitIDs = new List<string>();
            List<string> listOperators = new List<string>();
            List<string> listFieldnames = new List<string>();
            List<string> listValues = new List<string>();

            string parameters = "";

            foreach (DataGridViewRow row in ux_dataGridViewCriteria.Rows)
            {
                if (!row.Cells["category"].Value.ToString().Equals(PassportCategory))
                {//Traits
                    listTraitIDs.Add(row.Cells["trait_id"].Value.ToString());
                    listOperators.Add(row.Cells["search_operator"].Value.ToString());
                    listFieldnames.Add(row.Cells["fieldname"].Value.ToString());
                    listValues.Add(row.Cells["value"].Value.ToString());
                }
                else
                {
                    switch (row.Cells["search_operator"].Value.ToString())
                    {
                        case "In":
                        case "Not in":
                            parameters += " and " + row.Cells["fieldname"].Value.ToString() + " " + row.Cells["search_operator"].Value.ToString() + " (" + row.Cells["value"].Value.ToString() + ")";
                            break;
                        case ">=":
                        case "<=":
                            parameters += " and " + row.Cells["fieldname"].Value.ToString() + " " + row.Cells["search_operator"].Value.ToString() + " " + row.Cells["value"].Value.ToString();
                            break;
                        case "Contains":
                            parameters += " and " + row.Cells["fieldname"].Value.ToString() + " LIKE '%" + row.Cells["value"].Value.ToString().Replace("'", "") + "%'";
                            break;
                        case "Starts with":
                            parameters += " and " + row.Cells["fieldname"].Value.ToString() + " LIKE '" + row.Cells["value"].Value.ToString().Replace("'", "") + "%'";
                            break;
                        case "<>":
                            parameters += " and " + row.Cells["fieldname"].Value.ToString() + " <> '" + row.Cells["value"].Value.ToString().Replace("'", "") + "'";
                            break;
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(parameters))
            {
                ux_textBoxOutput.Text = "(@taxonomy_crop_map.crop_id in (" + ux_comboBoxCrop.SelectedValue + ")" + parameters + ")";
            }

            //List<string> accessionIDs = new List<string>();


            if (listTraitIDs.Count > 0)
            {
                List<string> queryCropTraitList = new List<string>();

                for (int i = 0; i < listTraitIDs.Count; i++)
                {
                    string criteria = string.Empty;
                    switch (listOperators[i])
                    {
                        case "In":
                        case "Not in":
                            criteria = string.Format("({0} @crop_trait_observation.crop_trait_id = {1} AND @crop_trait_observation.{2} {3} ({4}) and @crop_trait_observation.is_archived = '{5}' )",
                                                    (ux_checkboxIncludeMethod.Checked? " @crop_trait_observation.method_id = " + ux_comboboxMethod.SelectedValue + " AND" : string.Empty), 
                                                    listTraitIDs[i], listFieldnames[i], listOperators[i], listValues[i], 
                                                    (ux_checkboxIsArchived.Checked? "Y" : "N"));
                            break;
                        case ">=":
                        case "<=":
                            criteria = string.Format("({0} @crop_trait_observation.crop_trait_id = {1} AND @crop_trait_observation.{2} {3} {4} and @crop_trait_observation.is_archived = '{5}' )",
                                                    (ux_checkboxIncludeMethod.Checked ? " @crop_trait_observation.method_id = " + ux_comboboxMethod.SelectedValue + " AND" : string.Empty), 
                                                    listTraitIDs[i], listFieldnames[i], listOperators[i], listValues[i],
                                                    (ux_checkboxIsArchived.Checked ? "Y" : "N"));
                            break;
                        case "Contains":
                            criteria = string.Format("({0} @crop_trait_observation.crop_trait_id = {1} AND @crop_trait_observation.{2} {3} '%{4}%' and @crop_trait_observation.is_archived = '{5}' )",
                                                    (ux_checkboxIncludeMethod.Checked ? " @crop_trait_observation.method_id = " + ux_comboboxMethod.SelectedValue + " AND" : string.Empty), 
                                                    listTraitIDs[i], listFieldnames[i], "LIKE", listValues[i],
                                                    (ux_checkboxIsArchived.Checked ? "Y" : "N"));
                            break;
                        case "Starts with":
                            criteria = string.Format("({0} @crop_trait_observation.crop_trait_id = {1} AND @crop_trait_observation.{2} {3} '{4}%' and @crop_trait_observation.is_archived = '{5}' )",
                                                    (ux_checkboxIncludeMethod.Checked ? " @crop_trait_observation.method_id = " + ux_comboboxMethod.SelectedValue + " AND" : string.Empty), 
                                                    listTraitIDs[i], listFieldnames[i], "LIKE", listValues[i],
                                                    (ux_checkboxIsArchived.Checked ? "Y" : "N"));
                            break;
                        case "<>":
                            criteria = string.Format("({0} @crop_trait_observation.crop_trait_id = {1} AND @crop_trait_observation.{2} {3} '{4}' and @crop_trait_observation.is_archived = '{5}' )",
                                                    (ux_checkboxIncludeMethod.Checked ? " @crop_trait_observation.method_id = " + ux_comboboxMethod.SelectedValue + " AND" : string.Empty), 
                                                    listTraitIDs[i], listFieldnames[i], listOperators[i], listValues[i],
                                                    (ux_checkboxIsArchived.Checked ? "Y" : "N"));
                            break;
                        default:
                            throw new WizardException("Search operator not supported", "ux_textboxErrorSearchOperatorNotSupported");
                    }
                    queryCropTraitList.Add(criteria);
                }

                ux_textBoxOutput.Text += (string.IsNullOrEmpty(ux_textBoxOutput.Text) ? string.Empty : " INTERSECT " + Environment.NewLine) + string.Join(" INTERSECT" + Environment.NewLine, queryCropTraitList);
            }
            else if(string.IsNullOrWhiteSpace(parameters))
                ux_textBoxOutput.Text = "@taxonomy_crop_map.crop_id in (" + ux_comboBoxCrop.SelectedValue + ")" + parameters;

            // + inventory.form_type_code = '**'
            //ux_textBoxOutput.Text += " and @inventory.form_type_code = '**'";
            //ux_textBoxOutput.Text = "@inventory.form_type_code = '**' " + parameters;
            //ux_textboxBasicQueryText.Text = ux_textBoxOutput.Text;
        }

        private void ux_listBoxTrait_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                ux_listBoxValue.DataSource = null;
                ux_textBoxPassportValue.Text = "";

                if (ux_listboxTrait.SelectedValue == null) return;

                //ux_panelFrequency.Visible = false;

                Cursor.Current = Cursors.WaitCursor;

                /*Update Search Operators*/
                DataRowView dtv = (DataRowView)ux_listboxTrait.SelectedItem;
                if (ux_listboxCategory.SelectedValue.ToString().Equals("PASSPORT"))
                {
                    if (dtv["is_coded"].ToString().Equals("N"))
                    {
                        switch (dtv["data_type_code"].ToString())
                        {
                            case "CHAR":
                                ux_listboxOperator.DataSource = operatorSourceCHAR;
                                break;
                            case "NUMERIC":
                                ux_listboxOperator.DataSource = operatorSourceNUMERIC;
                                break;
                            default:
                                ux_listboxOperator.DataSource = operatorSourceBASIC;
                                break;
                        }
                    }
                    else
                    {
                        ux_listboxOperator.DataSource = operatorSourceBASIC;
                    }

                    ux_listBoxValue.Visible = false;

                    if (!string.IsNullOrEmpty(dtv["group_name"].ToString()))
                    {
                        string group_name = dtv["group_name"].ToString();
                        DataSet passportValues = _sharedUtils.GetWebServiceData("web_searchcriteria_item_value", ":groupname=" + group_name + ";:langid=1", 0, 0);
                        if (passportValues != null && passportValues.Tables.Contains("web_searchcriteria_item_value"))
                        {
                            if (passportValues.Tables["web_searchcriteria_item_value"].Rows.Count > 1)
                            {
                                ux_textBoxPassportValue.Visible = false;

                                passportValues.Tables["web_searchcriteria_item_value"].DefaultView.Sort = "title ASC";

                                ux_listBoxPassportValue.ValueMember = "value";
                                ux_listBoxPassportValue.DisplayMember = "title";
                                ux_listBoxPassportValue.DataSource = passportValues.Tables["web_searchcriteria_item_value"].DefaultView;
                                ux_listBoxPassportValue.Visible = true;
                            }
                            else
                            {
                                ux_listBoxPassportValue.Visible = false;
                                ux_listBoxPassportValue.Text = "";
                                ux_textBoxPassportValue.Visible = true;
                            }
                        }
                    }
                    else if (!string.IsNullOrEmpty(dtv["source_dataview"].ToString()))
                    {
                        string dataview = dtv["source_dataview"].ToString();
                        DataSet dsValues = _sharedUtils.GetWebServiceData(dataview, ":cropid=" + ux_comboBoxCrop.SelectedValue, 0, 0);
                        if (dsValues != null && dsValues.Tables.Contains(dataview))
                        {
                            if (dsValues.Tables[dataview].Rows.Count > 0)
                            {
                                ux_textBoxPassportValue.Visible = false;

                                dsValues.Tables[dataview].DefaultView.Sort = "title ASC";

                                ux_listBoxPassportValue.ValueMember = "value";
                                ux_listBoxPassportValue.DisplayMember = "title";
                                ux_listBoxPassportValue.DataSource = dsValues.Tables[dataview].DefaultView;
                                ux_listBoxPassportValue.Visible = true;
                            }
                            else
                            {
                                ux_listBoxPassportValue.Visible = false;
                                ux_listBoxPassportValue.Text = "";
                                ux_textBoxPassportValue.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        ux_listBoxPassportValue.Visible = false;
                        ux_listBoxPassportValue.Text = "";
                        ux_textBoxPassportValue.Visible = true;
                    }
                }
                else
                {
                    switch (dtv["data_type_code"].ToString())
                    {
                        case "NUMERIC":
                            ux_listboxOperator.DataSource = operatorSourceNUMERIC;
                            break;
                        case "CHAR":
                            ux_listboxOperator.DataSource = operatorSourceCHAR;
                            break;
                        default:
                            ux_listboxOperator.DataSource = operatorSourceBASIC;
                            break;
                    }

                    ux_listBoxPassportValue.Visible = false;
                    ux_textBoxPassportValue.Visible = false;
                    ux_listBoxValue.Visible = false;

                    string type = dtv["type"].ToString();
                    if (type.Equals("NUMERIC"))
                    {
                        ux_textBoxPassportValue.Visible = true;
                    }
                    else if (type.Equals("CHAR"))
                    {
                        ux_textBoxPassportValue.Visible = true;
                    }
                    else // CODED
                    {
                        ux_listBoxValue.Visible = true;
                        /*
                        //get_search_crop_trait_values_rta
                        if (ux_comboBoxCrop.Text.Equals("ARTC") && ux_comboBoxCropRTA.Text.Equals("[All]"))
                        {
                            DataSet listValues = _sharedUtils.GetWebServiceData("get_search_crop_trait_values_rta", ":langid1=1;:traitid=" + ux_listBoxTrait.SelectedValue.ToString(), 0, 0);
                            if (listValues != null && listValues.Tables.Contains("get_search_crop_trait_values_rta"))
                            {
                                ux_listBoxValue.ValueMember = "field_name_value";
                                ux_listBoxValue.DisplayMember = "crop_trait_code_text";
                                ux_listBoxValue.DataSource = listValues.Tables["get_search_crop_trait_values_rta"].DefaultView;
                            }
                        }*/
                        if (!ux_checkboxIncludeMethod.Checked || (ux_checkboxIncludeMethod.Checked && ux_comboboxMethod.SelectedValue != null))
                        {
                            DataSet listValues = _sharedUtils.GetWebServiceData("search_wizard_get_descriptor_detail_values", ":langid1=1;:langid2=1;:traitid=" + ux_listboxTrait.SelectedValue.ToString() +
                                ";:methodid=" + (ux_checkboxIncludeMethod.Checked ? ux_comboboxMethod.SelectedValue.ToString() : string.Empty), 0, 0);
                            if (listValues != null && listValues.Tables.Contains("search_wizard_get_descriptor_detail_values"))
                            {
                                ux_listBoxValue.ValueMember = "field_name_value";
                                ux_listBoxValue.DisplayMember = "crop_trait_code_text";
                                ux_listBoxValue.DataSource = listValues.Tables["search_wizard_get_descriptor_detail_values"].DefaultView;
                            }
                        }
                    }

                }
                Cursor.Current = _cursorGG;
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_buttonDescriptorSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ux_labelResultsRows.Text = string.Empty;
                ux_labelTimeElapsed.Text = string.Empty;

                ux_buttonDescriptorSearch.Enabled = false;
                _dtResults.Rows.Clear();
                _dtResults.DefaultView.Sort = string.Empty;
                
                string tableToSearch = "accession accession_ipr accession_pedigree accession_source accession_source_map cooperator crop crop_trait crop_trait_code crop_trait_code_lang crop_trait_observation geography inventory method site taxonomy_family taxonomy_genus accession_inv_name ";

                DataSet ds2 = _sharedUtils.SearchWebService(ux_textBoxOutput.Text, true, true, tableToSearch, "accession", 0, 0);

                if (ds2 != null && ds2.Tables.Contains("SearchResult"))
                {
                      //ds2.Tables["SearchResult"].Rows.Count + " accession records found";

                    basicQuerySearchResults = ds2.Tables["SearchResult"].Select("ID IS NOT NULL");

                    DialogResult userResponse = DialogResult.Cancel;
                    if (basicQuerySearchResults.Length == 0)
                    {
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("No matches in the database were found.", "Query Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                        ggMessageBox.Name = "GRINGlobalClientSearchTool_ux_buttonDoBasicQueryMessage5";
                        _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                        userResponse = ggMessageBox.ShowDialog();
                    }
                    else
                    {
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("Found {0} accessions in the database. \nWould you like to retrieve data?", "Query Results", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                        ggMessageBox.Name = "SearchWizard_ux_buttonDescriptorSearchMessage1";
                        _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);

                        ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, basicQuerySearchResults.Length.ToString());
                        userResponse = ggMessageBox.ShowDialog();
                    }
                    if (userResponse == DialogResult.No)
                    {
                        // User is bailing out on the query so clear the results with an empty array...
                        basicQuerySearchResults = new DataRow[0];
                        ux_labelResultsRows.Text = string.Format(ux_labelResultsRows.Tag.ToString(), 0, 0);
                        //ux_textboxBasicQueryText.Clear();
                    }
                    else if (userResponse == DialogResult.Yes)
                    {
                        ux_tabcontrolMain.SelectTab(0);
                        if(basicQuerySearchResults.Length > ux_numericupdownMaxRecords.Value)
                        {
                            Array.Resize(ref basicQuerySearchResults, (int)ux_numericupdownMaxRecords.Value);
                        }
                        ux_labelResultsRows.Text = string.Format(ux_labelResultsRows.Tag.ToString(), basicQuerySearchResults.Length, ds2.Tables["SearchResult"].Rows.Count);
                    }
                }
                else
                {
                    if (ds2 != null && ds2.Tables.Contains("ExceptionTable"))
                    {
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There was an unexpected error searching for data.\n\nFull error message:\n{0}", "Search Engine Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                        ggMessageBox.Name = "GRINGlobalClientSearchTool_ux_buttonDoBasicQueryMessage2";
                        _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                        ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, ds2.Tables["ExceptionTable"].Rows[0]["Message"].ToString());
                        ggMessageBox.ShowDialog();
                    }
                    else
                    {
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There was an unexpected error searching for data.\n\nWeb Service at '{0}' did not respond to your search request.", "Search Engine Response Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                        ggMessageBox.Name = "GRINGlobalClientSearchTool_ux_buttonDoBasicQueryMessage3";
                        _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                        ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, _sharedUtils.Url);
                        ggMessageBox.ShowDialog();
                    }
                    // Clear the results with an empty array...
                    basicQuerySearchResults = new DataRow[0];
                }

                // Refresh the DGV data and then the formatting...
                //RefreshDGVData();
                //RefreshDGVFormatting();

                if(basicQuerySearchResults.Length > 0)
                {
                    var watch = System.Diagnostics.Stopwatch.StartNew();

                    foreach (DataRow drId in basicQuerySearchResults)
                    {
                       DataRow drNew = _dtResults.NewRow();
                        drNew.SetField<int>("accession_id", drId.Field<int>("ID"));

                        DataTable dtAccessionNumber = _sharedUtils.GetLocalData("select display_member from accession_lookup where value_member = " + drId.Field<int>("ID"), "");
                        if(dtAccessionNumber.Rows.Count == 1)
                        {
                            drNew.SetField<string>("accession_number", dtAccessionNumber.Rows[0].Field<string>("display_member"));
                        }
                        else
                        {
                            throw new WizardException("Accession id not found", "ux_textboxErrorAccessionIdNotFound");
                        }
                        _dtResults.Rows.Add(drNew);
                    }

                    var accessionIdList = basicQuerySearchResults.AsEnumerable().Select(x => x.Field<int>("ID")).ToArray();

                    foreach (DataGridViewColumn dgvCol in ux_datagridviewQueryResults.Columns)
                    {
                        if (dgvCol.Tag == null) continue;

                        DescriptorTag tag = (DescriptorTag)dgvCol.Tag;

                        if(!string.IsNullOrEmpty(tag.dataviewName))
                        {
                            string params1 = string.Empty;
                            if (tag.DescriptorType == DescriptorType.Passport)
                                params1 = string.Format(tag.dataviewParameters, string.Join(",", accessionIdList));
                            else
                                params1 = string.Format(tag.dataviewParameters, string.Join(",", accessionIdList), (ux_checkboxIncludeMethod.Checked? ux_comboboxMethod.SelectedValue: string.Empty), dgvCol.Name);

                            DataSet dsDescriptorValues = _sharedUtils.GetWebServiceData(tag.dataviewName, params1, 0, 0);
                            if(dsDescriptorValues != null && dsDescriptorValues.Tables.Contains(tag.dataviewName))
                            {
                                foreach (DataRow drResult in _dtResults.Rows)
                                {
                                    var drDescriptorValues = dsDescriptorValues.Tables[tag.dataviewName].AsEnumerable().Where(x => x.Field<int>("accession_id") == drResult.Field<int>("accession_id"));
                                    if (drDescriptorValues != null && drDescriptorValues.Count() > 0)
                                    {
                                        List<string> values = new List<string>();
                                        foreach (var drDescriptorValue in drDescriptorValues)
                                        {
                                            if (dsDescriptorValues.Tables[tag.dataviewName].Columns[tag.dataviewColumnName].DataType == typeof(DateTime) && !drDescriptorValue.IsNull(tag.dataviewColumnName))
                                            {
                                                string dateFormat = drDescriptorValue[tag.dataviewColumnName + "_code"].ToString();
                                                string formmattedValue = drDescriptorValue.Field<DateTime>(tag.dataviewColumnName).ToString(dateFormat);
                                                values.Add(formmattedValue);
                                            }
                                            else
                                                values.Add(drDescriptorValue[tag.dataviewColumnName].ToString());
                                        }
                                        drResult[dgvCol.Name] = string.Join("; ", values);
                                    }
                                }
                            }
                        }
                    }

                    watch.Stop();
                    ux_labelTimeElapsed.Text = string.Format(ux_labelTimeElapsed.Tag.ToString(), watch.ElapsedMilliseconds / 1000);
                    ux_labelTimeElapsed.Visible = true;
                }

                Cursor.Current = _cursorGG;
                ux_buttonDescriptorSearch.Enabled = true;
            }
            catch (WizardException wex)
            {
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                Cursor.Current = _cursorGG;
                ux_buttonDescriptorSearch.Enabled = true;

                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void RefreshDGVData()
        {
            int pageSize = 100;

            // Get the name of the dataview to use...
            string dataGridViewTableName = "get_accession";
            
            /*if (ux_tabcontrolSTDataviews.SelectedTab.Tag.GetType() == typeof(GRINGlobal.Client.Common.DataviewProperties))
            {
                dataGridViewTableName = ((GRINGlobal.Client.Common.DataviewProperties)ux_tabcontrolSTDataviews.SelectedTab.Tag).DataviewName;
            }*/
          

            // Get the data for the selected dataview tab based on the search results...
            DataSet ds = GetDGVData(dataGridViewTableName, basicQuerySearchResults, pageSize);
            if (queryResults.Tables.Contains("BasicSearchResults")) queryResults.Tables.Remove("BasicSearchResults");
            if (ds.Tables.Contains(dataGridViewTableName))
            {
                queryResults.Tables.Add(ds.Tables[dataGridViewTableName].Copy());
                queryResults.Tables[dataGridViewTableName].TableName = "BasicSearchResults";

                // Reset the rowfilter and the sortorder...
                DataGridViewSortOrder = "";
                DataGridViewFilter = "";
                queryResults.Tables["BasicSearchResults"].DefaultView.Sort = DataGridViewSortOrder;
                queryResults.Tables["BasicSearchResults"].DefaultView.RowFilter = DataGridViewFilter;

                // Now build the DGV and bind it to the datasource...
                _sharedUtils.BuildReadOnlyDataGridView(ux_datagridviewQueryResults, queryResults.Tables["BasicSearchResults"]);

                // Update the statusbar...
                /*if (ux_statusLeftMessage.Tag != null &&
                    ux_statusLeftMessage.Tag.ToString().Contains("{0}") &&
                    ux_statusLeftMessage.Tag.ToString().Contains("{1}"))
                {
                    ux_statusLeftMessage.Text = string.Format(ux_statusLeftMessage.Tag.ToString(), ux_datagridviewQueryResults.Rows.Count.ToString(), ((DataTable)ux_datagridviewQueryResults.DataSource).Rows.Count.ToString());
                }
                else if (ux_statusLeftMessage.Tag != null &&
                    ux_statusLeftMessage.Tag.ToString().Contains("{0}"))
                {
                    ux_statusLeftMessage.Text = string.Format(ux_statusLeftMessage.Tag.ToString(), ((DataTable)ux_datagridviewQueryResults.DataSource).Rows.Count.ToString());
                }*/
                //ux_statusLeftMessage.Text = "Showing rows: " + ux_datagridviewQueryResults.Rows.Count.ToString() + " of " + ((DataTable)ux_datagridviewQueryResults.DataSource).Rows.Count.ToString() + " retrieved";

                foreach (DataGridViewColumn dgvc in ux_datagridviewQueryResults.Columns)
                {
                    dgvc.SortMode = DataGridViewColumnSortMode.Programmatic;
                    /*dgvc.ContextMenuStrip = ux_contextmenustripDGVResultsCell;*/
                    DataColumn dc = ((DataTable)dgvc.DataGridView.DataSource).Columns[dgvc.DataPropertyName];
                    dgvc.HeaderText = _sharedUtils.GetFriendlyFieldName(dc, dc.ColumnName);
                }
                //RefreshDGVFormatting();
            }
        }

        public DataSet GetDGVData(string dataviewName, DataRow[] groupList, int pageSize)
        {
            DataSet results = new DataSet();

            // Reset the escapeKeyPressed flag...
            escapeKeyPressed = false;

            if (groupList != null &&
                groupList.Length > 0)
            {
                //string acids = "";
                //string ivids = "";
                //string ornos = "";
                //string coops = "";
                /*string centerMessage = ux_statusCenterMessage.Text;
                string rightMessage = ux_statusRightMessage.Text;
                ContentAlignment centerMessageAlignment = ux_statusCenterMessage.TextAlign;
                int nextPage = pageSize;

                // Set the statusbar up to show the query progress...
                ux_statusCenterMessage.TextAlign = ContentAlignment.MiddleRight;
                //ux_statusCenterMessage.Text = "Retrieving Data:";
                ux_statusCenterMessage.Text = "";
                // Update the statusbar...
                ux_statusLeftMessage.Text = ux_statusLeftMessage.Tag.ToString();
                //ux_statusRightMessage.Text = "   Press the escape key (esc) to cancel query   ";
                ux_statusProgressBar.Visible = true;
                ux_statusProgressBar.Minimum = 0;
                ux_statusProgressBar.Maximum = groupList.Length;
                ux_statusProgressBar.Step = pageSize;
                ux_statusProgressBar.Value = 0;
                ux_statusstripMain.Refresh();*/

                // Begin iterating through the collection of ACIDS, IVIDS, and ORNOS...
                int pageStart = 0;
                int pageStop = Math.Min(pageSize, groupList.Length);
                while (pageStart < groupList.Length && !escapeKeyPressed)
                {
                    //acids = "";
                    //ivids = "";
                    //ornos = "";
                    //coops = "";
                    string pkeyCollection = ":" + searchResultType.Replace("_", "") + "id=";
                    for (int i = pageStart; i < pageStop; i++)
                    {
                        //switch (searchResultType)
                        //{
                        //    case "accession":
                        //        acids += groupList[i]["ID"].ToString() + ",";
                        //        break;
                        //    case "inventory":
                        //        ivids += groupList[i]["ID"].ToString() + ",";
                        //        break;
                        //    case "order_request":
                        //        ornos += groupList[i]["ID"].ToString() + ",";
                        //        break;
                        //    case "cooperator":
                        //        coops += groupList[i]["ID"].ToString() + ",";
                        //        break;
                        //    default:
                        //        acids += groupList[i]["ID"].ToString() + ",";
                        //        break;
                        //}
                        pkeyCollection += groupList[i]["ID"].ToString() + ",";
                    }
                    // Update the paging indexes...
                    pageStart = pageStop;
                    pageStop = Math.Min((pageStart + pageSize), groupList.Length);
                    // Build the param string and get the data...
                    //string pkeyCollection = ":accessionid=" + acids.TrimEnd(',') + "; :inventoryid=" + ivids.TrimEnd(',') + "; :orderrequestid=" + ornos.TrimEnd(',') + "; :cooperatorid=" + coops.TrimEnd(',');
                    pkeyCollection = pkeyCollection.TrimEnd(',');
                    //DataSet pagedDataSet = GUIWebServices.GetData(false, _user, _password, resultsetName, pkeyCollection, 0, Int32.Parse(ux_numericupdownBasicMaxRecords.Value.ToString()));
                    DataSet pagedDataSet = _sharedUtils.GetWebServiceData(dataviewName, pkeyCollection, 0, Int32.Parse(ux_numericupdownMaxRecords.Value.ToString()));
                    // Add the results to the dataset that will be returned...
                    if (results.Tables.Contains(dataviewName))
                    {
                        results.Tables[dataviewName].Merge(pagedDataSet.Tables[dataviewName].Copy());
                    }
                    else
                    {
                        results.Tables.Add(pagedDataSet.Tables[dataviewName].Copy());
                    }

                    // Update the progress bar on the statusbar...
                    /*ux_statusProgressBar.Value = pageStop;*/

                    // Check to see if we should bail out (at user request)...
                    Cursor currentCursor = Cursor.Current;
                    Application.DoEvents();
                    Cursor.Current = currentCursor;
                }

                // Now we need to get only distinct rows in the dataset to return...
                // First build a string array of the columns to return...
                /*ux_statusCenterMessage.Text = "Removing duplicate rows...  Please wait.";*/
                string[] columnNames = new string[results.Tables[dataviewName].Columns.Count];
                foreach (DataColumn dc in results.Tables[dataviewName].Columns)
                {
                    columnNames[dc.Ordinal] = dc.ColumnName;
                }
                DataTable dt = results.Tables[dataviewName].DefaultView.ToTable("distinct" + dataviewName, true, columnNames);
                results.Tables.Add(dt);
                results.Tables.Remove(dataviewName);
                results.Tables["distinct" + dataviewName].TableName = dataviewName;
                // Restore the statusbar back to it's original state...
                /*ux_statusCenterMessage.TextAlign = centerMessageAlignment;
                ux_statusCenterMessage.Text = centerMessage;
                ux_statusRightMessage.Text = rightMessage;
                ux_statusProgressBar.Visible = false;
                ux_statusstripMain.Refresh();*/
            }
            else
            {
                // If no primary keys were found return an empty dataset...
                results = _sharedUtils.GetWebServiceData(dataviewName, ":accessionid=; :inventoryid=; :orderrequestid=", 0, Int32.Parse(ux_numericupdownMaxRecords.Value.ToString()));
            }

            if (results.Tables.Contains("ExceptionTable") &&
                results.Tables["ExceptionTable"].Rows.Count > 0)
            {
                //MessageBox.Show("There was an unexpected error retrieving data for " + dataviewName + ".\n\nFull error message:\n" + results.Tables["ExceptionTable"].Rows[0]["Message"].ToString());
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There was an unexpected error retrieving data for {0}.\n\nFull error message:\n{1}", "Search Engine Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "GRINGlobalClientSearchTool_GetDGVDataMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                //if (ggMessageBox.MessageText.Contains("{0}") &&
                //    ggMessageBox.MessageText.Contains("{1}"))
                //{
                //    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, dataviewName, results.Tables["ExceptionTable"].Rows[0]["Message"].ToString());
                //}
                //else if (ggMessageBox.MessageText.Contains("{0}"))
                //{
                //    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, dataviewName);
                //}
                //else if (ggMessageBox.MessageText.Contains("{1}"))
                //{
                //    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, results.Tables["ExceptionTable"].Rows[0]["Message"].ToString());
                //}
                string[] argsArray = new string[100];
                argsArray[0] = dataviewName;
                argsArray[1] = results.Tables["ExceptionTable"].Rows[0]["Message"].ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }

            return results;
        }

        private void ux_buttonRemoveCriteria_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = ux_dataGridViewCriteria.SelectedRows.Count - 1; i >= 0; i--)
                {
                    ux_dataGridViewCriteria.Rows.RemoveAt(ux_dataGridViewCriteria.SelectedRows[i].Index);
                }

                // Refresh query
                BuildQuery();
            }
            catch (WizardException wex)
            {
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_buttonAddDescriptorToResults_Click(object sender, EventArgs e)
        {
            try
            {
                if (ux_listboxTrait.SelectedItem == null) return;

                DataRow drDescriptor = ((DataRowView)ux_listboxTrait.SelectedItem).Row;

                if (drDescriptor.Table.Columns.Contains("title")) //Passport
                {
                    if (_dtResults.Columns.Contains(drDescriptor.Field<string>("code")))
                        throw new WizardException("\"" + drDescriptor.Field<string>("title") + "\" column is already in results grid", "ux_textboxErrorColumnAlreadyExists", drDescriptor.Field<string>("title"));

                    _dtResults.Columns.Add(drDescriptor.Field<string>("code"), typeof(string));
                    ux_datagridviewQueryResults.Columns[drDescriptor.Field<string>("code")].HeaderText = drDescriptor.Field<string>("title");
                    ux_datagridviewQueryResults.Columns[drDescriptor.Field<string>("code")].Tag = new DescriptorTag
                    {
                        DescriptorType = DescriptorType.Passport,
                        dataviewName = drDescriptor.Field<string>("dataview_name"),
                        dataviewColumnName = drDescriptor.Field<string>("dataview_column_name"),
                        dataviewParameters = drDescriptor.Field<string>("dataview_params")
                    };
                    ux_datagridviewQueryResults.Columns[drDescriptor.Field<string>("code")].ContextMenuStrip = ux_contextmenustripDGVResultsCell;
                }
                else //Crop Trait
                {
                    if (_dtResults.Columns.Contains(drDescriptor.Field<int>("value_member").ToString()))
                        throw new WizardException("\"" + drDescriptor.Field<string>("display_member") + "\" column is already in results grid", "ux_textboxErrorColumnAlreadyExists", drDescriptor.Field<string>("display_member"));

                    _dtResults.Columns.Add(drDescriptor.Field<int>("value_member").ToString(), typeof(string));
                    ux_datagridviewQueryResults.Columns[drDescriptor.Field<int>("value_member").ToString()].HeaderText = drDescriptor.Field<string>("display_member");

                    ux_datagridviewQueryResults.Columns[drDescriptor.Field<int>("value_member").ToString()].Tag = new DescriptorTag
                    {
                        DescriptorType = DescriptorType.CropTrait,
                        dataviewName = "search_wizard_get_descriptor_crop_trait",
                        dataviewColumnName = "value",
                        dataviewParameters = ":accessionid={0};:methodid={1};:croptraitid={2}"
                    };
                    ux_datagridviewQueryResults.Columns[drDescriptor.Field<int>("value_member").ToString()].ContextMenuStrip = ux_contextmenustripDGVResultsCell;
                }
            }
            catch (WizardException wex)
            {
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_datagridviewQueryResults_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataObject doDragDropData;

                if ((e.RowIndex > -1) && (e.ColumnIndex > -1) && (ux_datagridviewQueryResults.Rows[e.RowIndex].Selected) && (e.Button == MouseButtons.Left))
                {
                    // If there were selected rows lets start the drag-drop operation...
                    if (ux_datagridviewQueryResults.SelectedRows.Count > 0)
                    {
                        //Check if accession_id is not empty
                        foreach (DataGridViewRow dgvRow in ux_datagridviewQueryResults.SelectedRows)
                        {
                            if (dgvRow.Cells["accession_id"].Value == null || string.IsNullOrEmpty(dgvRow.Cells["accession_id"].Value.ToString()))
                            {
                                throw new WizardException("Only found entries can be selected", "ux_textboxErrorOnlyFoundEntriesCanBeSelected");
                            }
                        }

                        // Get the string of selected rows extracted from the GridView...
                        doDragDropData = BuildDGVDragAndDropData(ux_datagridviewQueryResults);
                        ux_datagridviewQueryResults.DoDragDrop(doDragDropData, DragDropEffects.Copy);
                    }
                }
                /*if ((e.RowIndex > -1) && (e.ColumnIndex > -1) && (e.Button == MouseButtons.Right))
                {
                    // Change the color of the cell background so that the user
                    // knows what cell the context menu applies to...
                    ux_datagridviewQueryResults.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Red;
                    ux_datagridviewQueryResults.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionBackColor = Color.Red;
                }*/
            }
            catch (WizardException wex)
            {
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_buttonFrequency_Click(object sender, EventArgs e)
        {
            try
            {
                if (ux_listboxTrait.SelectedItem == null) return;

                DataRowView dtv = (DataRowView)ux_listboxTrait.SelectedItem;

                if (ux_listboxCategory.SelectedValue.ToString().Equals("PASSPORT"))
                {
                    if (!string.IsNullOrEmpty(dtv["frequency_dataview_name"].ToString()))
                    {
                        string frequencyParams = dtv["frequency_params"].ToString();
                        FrequencyForm freq = new FrequencyForm(_sharedUtils, dtv.Row.Field<string>("frequency_dataview_name"), string.Format(frequencyParams, ux_comboBoxCrop.SelectedValue));
                        freq.Show();
                    }
                }
                else
                {
                    List<string> selectedIndicesList = new List<string>();
                    foreach (int index in ux_listboxTrait.SelectedIndices)
                    {
                        DataRowView row = (DataRowView)ux_listboxTrait.Items[index];
                        selectedIndicesList.Add(row["value_member"].ToString());
                    }
                    string selectedIndices = string.Join(",", selectedIndicesList);

                    FrequencyForm freq = new FrequencyForm(_sharedUtils, "search_wizard_get_frequency_trait", string.Format(":croptraitid={0};:methodid={1};:isarchived={2}"
                        , selectedIndices, (!ux_checkboxIncludeMethod.Checked? string.Empty : ux_comboboxMethod.SelectedValue.ToString()), ux_checkboxIsArchived.Checked ? "Y" : "N"));
                    freq.Show();
                }
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_textBoxOutput_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                TextBox txtBox = (TextBox)sender;
                if (e.Button == MouseButtons.Left)
                {
                    int pointerCharIndex = txtBox.GetCharIndexFromPosition(new Point(e.X, e.Y));
                    // Check to see if the left mouse click contains the currently selected text
                    // and if so start a Drag and Drop operation...
                    if (pointerCharIndex > txtBoxSelectionStart + 1 &&
                        pointerCharIndex < (txtBoxSelectionStart + txtBoxSelectionLength - 2))
                    {
                        string searchCriteria = "Search Tool Query :: ";
                        /*if (ux_radiobuttonDefaultFind.Checked)
                        {
                            searchCriteria += "ResolveTo=default :: ";
                        }
                        else
                        {
                            searchCriteria += "ResolveTo=" + searchResultType + " :: ";
                        }*/

                        searchCriteria += "ResolveTo=" + "ACCESSION" + " :: ";

                        searchCriteria += "SearchCriteria=" + txtBox.Text.Substring(System.Math.Max(txtBoxSelectionStart, 0), System.Math.Min(txtBoxSelectionLength, txtBox.Text.Length - System.Math.Max(txtBoxSelectionStart, 0)));
                        txtBox.DoDragDrop(searchCriteria, DragDropEffects.Copy);
                        // Reset the global selection indexes...
                        txtBoxSelectionStart = 0;
                        txtBoxSelectionLength = 0;
                        // Reselect the text (since the control cleared the select)...
                        txtBox.SelectionStart = txtBoxSelectionStart;
                        txtBox.SelectionLength = txtBoxSelectionLength;
                        txtBox.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_textBoxOutput_MouseMove(object sender, MouseEventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            txtBox.Cursor = Cursors.IBeam;
            if (e.Button == MouseButtons.None)
            {
                int pointerCharIndex = txtBox.GetCharIndexFromPosition(new Point(e.X, e.Y));
                if (pointerCharIndex > txtBoxSelectionStart + 1 &&
                    pointerCharIndex < (txtBoxSelectionStart + txtBoxSelectionLength - 2))
                {
                    txtBox.Cursor = Cursors.Arrow;
                }
            }
        }

        private void ux_textBoxOutput_MouseUp(object sender, MouseEventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            txtBoxSelectionStart = txtBox.SelectionStart;
            txtBoxSelectionLength = txtBox.SelectionLength;
        }

        private void ux_buttonShowDescription_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (ux_listboxTrait.SelectedItem == null) return;

                if (ux_listboxCategory.SelectedValue.ToString().Equals("PASSPORT"))
                {
                    DataSet descriptorDetail = _sharedUtils.GetWebServiceData("search_wizard_get_mcpd_detail", ":mcpd=" + ux_listboxTrait.SelectedValue.ToString(), 0, 0);
                    if (descriptorDetail != null && descriptorDetail.Tables.Contains("search_wizard_get_mcpd_detail"))
                    {
                        if (descriptorDetail.Tables["search_wizard_get_mcpd_detail"].Rows.Count > 0)
                        {
                            descriptorDetail.Tables["search_wizard_get_mcpd_detail"].Columns.Add("crop_name");
                            descriptorDetail.Tables["search_wizard_get_mcpd_detail"].Rows[0].SetField<string>("crop_name", ux_comboBoxCrop.Text);


                            DescriptorDetailForm dd = new DescriptorDetailForm(_sharedUtils, descriptorDetail.Tables["search_wizard_get_mcpd_detail"]);
                            dd.Show();
                        }
                    }
                }
                else
                {
                    DataSet descriptorDetail = _sharedUtils.GetWebServiceData("search_wizard_get_descriptor_detail", ":descriptorid=" + ux_listboxTrait.SelectedValue.ToString(), 0, 0);
                    if (descriptorDetail != null && descriptorDetail.Tables.Contains("search_wizard_get_descriptor_detail"))
                    {
                        if (descriptorDetail.Tables["search_wizard_get_descriptor_detail"].Rows.Count > 0)
                        {
                            DescriptorDetailForm dd = new DescriptorDetailForm(_sharedUtils, descriptorDetail.Tables["search_wizard_get_descriptor_detail"]);
                            dd.Show();
                        }
                    }
                }
                Cursor.Current = Cursors.Arrow;
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Arrow;
                WizardMessageBox mb = new WizardMessageBox("Error :\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_textBoxOutput_Enter(object sender, EventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            ToolTip toolTip = new ToolTip();
            toolTip.Show("Select all text and drag it over a folder in Curator Tool to create a dynamic folder", textBox, 0, -20, 2000);
        }

        private void ux_datagridviewQueryResults_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                string columnName = ux_datagridviewQueryResults.Columns[MouseClickColumnIndex].Name;
                if (columnName.Equals("accession_id") || columnName.Equals("accession_number")) return;

                ux_contextmenustripDGVResultsColumnHeader.Show(ux_datagridviewQueryResults.Columns[e.ColumnIndex].HeaderCell.AccessibilityObject.Bounds.Location);
            }
        }

        private void removeColumnMenuItem_Click(object sender, EventArgs e)
        {
            if (MouseClickColumnIndex >= 0)
            {
                string columnName = ux_datagridviewQueryResults.Columns[MouseClickColumnIndex].Name;
                if (columnName.Equals("accession_id") || columnName.Equals("accession_number")) return;

                WizardMessageBox mb = new WizardMessageBox("Are you sure to remove \"" + ux_datagridviewQueryResults.Columns[MouseClickColumnIndex].HeaderText + "\" column?", this.Text, MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                if( mb.ShowDialog() == DialogResult.Yes)
                {
                    _dtResults.Columns.RemoveAt(MouseClickColumnIndex);
                }
            }
        }

        private void ux_datagridviewQueryResults_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            MouseClickColumnIndex = e.ColumnIndex;
            MouseClickRowIndex = e.RowIndex;
        }

        private void ux_checkBoxOnlyFound_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if(ux_dataGridViewQSResults.DataSource != null)
                {
                    DataTable dtSource = (DataTable)ux_dataGridViewQSResults.DataSource;
                    if (ux_checkboxQSOnlyFound.Checked)
                        dtSource.DefaultView.RowFilter = "is_found = 'Y'";
                    else
                        dtSource.DefaultView.RowFilter = string.Empty;

                    ux_labelQSResultsRows.Text = string.Format(ux_labelQSResultsRows.Tag.ToString(), ux_dataGridViewQSResults.Rows.Count, dtSource.Rows.Count);
                }
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_checkBoxIncludeMethod_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ux_comboboxMethod.Visible = ux_checkboxIncludeMethod.Checked;

                ux_listBoxCategory_SelectedValueChanged(null, null);

                if (!ux_checkboxIncludeMethod.Checked || (ux_checkboxIncludeMethod.Checked && ux_comboboxMethod.SelectedValue != null))
                    BuildQuery();
                else
                    ux_textBoxOutput.Clear();
            }
            catch (WizardException wex)
            {
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_buttonRefreshSearchCriteria_Click(object sender, EventArgs e)
        {
            try
            {
                BuildQuery();
            }
            catch (WizardException wex)
            {
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_checkboxIsArchived_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BuildQuery();
            }
            catch (WizardException wex)
            {
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_comboboxMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ux_comboboxMethod.SelectedValue != null)
                {
                    ux_listBoxCategory_SelectedValueChanged(null, null);

                    BuildQuery();
                }
                else
                    ux_textBoxOutput.Clear();
            }
            catch (WizardException wex)
            {
                WizardMessageBox mb = new WizardMessageBox("\n\n" + wex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.UpdateControls(_sharedUtils, "SearchWizardErrorMessageBox", wex.AppResourceName, wex.MessageParams);
                mb.ShowDialog();
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_textBoxOutput_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            splitContainer1.Panel1Collapsed = !splitContainer1.Panel1Collapsed;
            if (splitContainer1.Panel1Collapsed)
            {
                ux_textBoxOutput.Font = new Font("Microsoft Sans Serif", 13f);
            }
            else
                ux_textBoxOutput.Font = new Font("Microsoft Sans Serif", 8.25f);
        }

        private void ux_textboxFindDescriptor_TextChanged(object sender, EventArgs e)
        {
            textChangeDelayTimer.Stop();
            textChangeDelayTimer.Interval = 1000;
            textChangeDelayTimer.Start();
        }

        private void ux_textboxFindDescriptor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                ux_listboxTrait.Focus();
            }
        }

        void timerDelay_Tick(object sender, EventArgs e)
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            textChangeDelayTimer.Stop();
            RefreshDescriptorList();

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void RefreshDescriptorList()
        {
            if (_dtDescriptor == null) return;
            
            string rowFilter = ux_listboxTrait.DisplayMember + " like '%" + ux_textboxFindDescriptor.Text + "%'";
            _dtDescriptor.DefaultView.RowFilter = rowFilter;
            
            ux_textboxFindDescriptor.Focus();
            ux_textboxFindDescriptor.SelectionStart = ux_textboxFindDescriptor.Text.Length;
        }

        private void removeAllColumnsMenuItem_Click(object sender, EventArgs e)
        {
            if (MouseClickColumnIndex >= 0)
            {  
                WizardMessageBox mb = new WizardMessageBox("Are you sure to remove all columns added?", this.Text, MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                if (mb.ShowDialog() == DialogResult.Yes)
                {
                    for (int iCol = _dtResults.Columns.Count - 1; iCol > 0; iCol--)
                    {
                        if (_dtResults.Columns[iCol].ColumnName.Equals("accession_id") || _dtResults.Columns[iCol].ColumnName.Equals("accession_number")) continue;
                        _dtResults.Columns.RemoveAt(iCol);
                    }
                }
            }
        }

        private void columnFrequencyMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MouseClickColumnIndex >= 0)
                {
                    string columnName = ux_datagridviewQueryResults.Columns[MouseClickColumnIndex].Name;
                    if (columnName.Equals("accession_id") || columnName.Equals("accession_number")) return;

                    var accountGroups = _dtResults.AsEnumerable()
                                            .GroupBy(row => row.Field<String>(columnName))
                                            .Select(grp => new { Value = grp.Key, Count = grp.Count() });

                    DataTable dtFrequency = new DataTable();
                    dtFrequency.Columns.Add("value");
                    dtFrequency.Columns.Add("count", typeof(int));

                    foreach (var grp in accountGroups)
                        dtFrequency.Rows.Add(grp.Value, grp.Count);

                    FrequencyForm frm = new FrequencyForm(dtFrequency);
                    frm.Text = ux_datagridviewQueryResults.Columns[MouseClickColumnIndex].HeaderText;
                    frm.MaximizeBox = false;
                    frm.MinimizeBox = false;
                    frm.Show(this);
                    //frm.Location = ux_datagridviewQueryResults.Columns[MouseClickColumnIndex].HeaderCell.AccessibilityObject.Bounds.Location;
                    frm.Left = this.Left + (this.Width - frm.Width) / 2;
                    frm.Top = this.Top + (this.Height - frm.Height) / 2;
                }
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }

        private void ux_datagridviewQueryResults_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                //(ux_datagridviewQueryResults[e.ColumnIndex, e.RowIndex]).
                //ux_contextmenustripDGVResultsCell.Show( (sender as Control), 10,10 );
            }
        }

        private void copyAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // could take a while - so change the cursor...
                Cursor.Current = Cursors.WaitCursor;

                DataObject dndData = new DataObject();

                // Write the rows out in text format first...
                StringBuilder sb = new StringBuilder("");

                string columnHeader = "";
                string columnNames = "";
                // First, gather the column headers...
                foreach (DataGridViewColumn dgvCol in ux_datagridviewQueryResults.Columns)
                {
                    if (dgvCol.Visible)
                    {
                        columnHeader += dgvCol.HeaderText + "\t";
                        columnNames += dgvCol.Name + "\t";
                    }
                }
                // And write it to the string builder...
                sb.Append(columnHeader);
                // Build the array of visible column names...
                string[] columnList = columnNames.Trim().Split('\t');
                // And use that list for writing the values to the string builder...
                foreach (DataGridViewRow dgvr in ux_datagridviewQueryResults.Rows)
                {
                    sb.Append("\n");
                    foreach (string columnName in columnList)
                    {
                        sb.Append(dgvr.Cells[columnName].FormattedValue.ToString());
                        sb.Append("\t");
                    }
                }

                // Set the data types into the data object and return...
                dndData.SetData(typeof(string), sb.ToString());

                Clipboard.SetDataObject(dndData);

                // We are all done so change the cursor back...
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                WizardMessageBox mb = new WizardMessageBox(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                mb.ShowDialog();
            }
        }
    }

    enum SearchOperator { In, NotIn, GreaterThanOrEqualTo, LessThanOrEqualTo, LessThan, GreaterThan, Contains, StartsWith, EndsWith, Equals, NotEquals }

    static class DataTableHelper
    {
        public enum JoinType
        {
            /// <summary>
            /// Same as regular join. Inner join produces only the set of records that match in both Table A and Table B.
            /// </summary>
            Inner = 0,
            /// <summary>
            /// Same as Left Outer join. Left outer join produces a complete set of records from Table A, with the matching records (where available) in Table B. If there is no match, the right side will contain null.
            /// </summary>
            Left = 1
        }

        /// <summary>
        /// Joins the passed in DataTables on the colToJoinOn.
        /// <para>Returns an appropriate DataTable with zero rows if the colToJoinOn does not exist in both tables.</para>
        /// </summary>
        /// <param name="dtblLeft"></param>
        /// <param name="dtblRight"></param>
        /// <param name="colToJoinOn"></param>
        /// <param name="joinType"></param>
        /// <returns></returns>
        /// <remarks>
        /// <para>http://stackoverflow.com/questions/2379747/create-combined-datatable-from-two-datatables-joined-with-linq-c-sharp?rq=1</para>
        /// <para>http://msdn.microsoft.com/en-us/library/vstudio/bb397895.aspx</para>
        /// <para>http://www.codinghorror.com/blog/2007/10/a-visual-explanation-of-sql-joins.html</para>
        /// <para>http://stackoverflow.com/questions/406294/left-join-and-left-outer-join-in-sql-server</para>
        /// </remarks>
        public static DataTable JoinTwoDataTablesOnOneColumn(DataTable dtblLeft, DataTable dtblRight, string colToJoinOn, JoinType joinType)
        {
            //Change column name to a temp name so the LINQ for getting row data will work properly.
            string strTempColName = colToJoinOn + "_2";
            if (dtblRight.Columns.Contains(colToJoinOn))
                dtblRight.Columns[colToJoinOn].ColumnName = strTempColName;

            //Get columns from dtblLeft
            DataTable dtblResult = dtblLeft.Clone();

            //Get columns from dtblRight
            var dt2Columns = dtblRight.Columns.OfType<DataColumn>().Select(dc => new DataColumn(dc.ColumnName, dc.DataType, dc.Expression, dc.ColumnMapping));

            //Get columns from dtblRight that are not in dtblLeft
            var dt2FinalColumns = from dc in dt2Columns.AsEnumerable()
                                  where !dtblResult.Columns.Contains(dc.ColumnName)
                                  select dc;

            //Add the rest of the columns to dtblResult
            dtblResult.Columns.AddRange(dt2FinalColumns.ToArray());

            //No reason to continue if the colToJoinOn does not exist in both DataTables.
            if (!dtblLeft.Columns.Contains(colToJoinOn) || (!dtblRight.Columns.Contains(colToJoinOn) && !dtblRight.Columns.Contains(strTempColName)))
            {
                if (!dtblResult.Columns.Contains(colToJoinOn))
                    dtblResult.Columns.Add(colToJoinOn);
                return dtblResult;
            }

            switch (joinType)
            {

                default:
                case JoinType.Inner:
                    #region Inner
                    //get row data
                    //To use the DataTable.AsEnumerable() extension method you need to add a reference to the System.Data.DataSetExtension assembly in your project. 
                    var rowDataLeftInner = from rowLeft in dtblLeft.AsEnumerable()
                                           join rowRight in dtblRight.AsEnumerable() on rowLeft[colToJoinOn] equals rowRight[strTempColName]
                                           select rowLeft.ItemArray.Concat(rowRight.ItemArray).ToArray();


                    //Add row data to dtblResult
                    foreach (object[] values in rowDataLeftInner)
                        dtblResult.Rows.Add(values);

                    #endregion
                    break;
                case JoinType.Left:
                    #region Left
                    var rowDataLeftOuter = from rowLeft in dtblLeft.AsEnumerable()
                                           join rowRight in dtblRight.AsEnumerable() on rowLeft[colToJoinOn] equals rowRight[strTempColName] into gj
                                           from subRight in gj.DefaultIfEmpty()
                                           select rowLeft.ItemArray.Concat((subRight == null) ? (dtblRight.NewRow().ItemArray) : subRight.ItemArray).ToArray();


                    //Add row data to dtblResult
                    foreach (object[] values in rowDataLeftOuter)
                        dtblResult.Rows.Add(values);

                    #endregion
                    break;
            }

            //Change column name back to original
            dtblRight.Columns[strTempColName].ColumnName = colToJoinOn;

            //Remove extra column from result
            dtblResult.Columns.Remove(strTempColName);

            return dtblResult;
        }
    }

    enum DescriptorType { CropTrait, Passport}

    class DescriptorTag
    {
        public DescriptorType DescriptorType { get; set; }
        public string dataviewName { get; set; }
        public string dataviewColumnName { get; set; }
        public string dataviewParameters{ get; set; }
    }
}
