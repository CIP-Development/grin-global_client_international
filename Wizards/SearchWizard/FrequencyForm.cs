﻿using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SearchWizard
{
    public partial class FrequencyForm : Form
    {
        string _values_dataview = string.Empty;
        string _parameters = string.Empty;
        SharedUtils _sharedUtils;
        DataTable _dtSource = null;

        public FrequencyForm(SharedUtils sharedUtils, string values_dataview, string parameters)
        {
            InitializeComponent();
            _values_dataview = values_dataview;
            _parameters = parameters;
            _sharedUtils = sharedUtils;
        }

        public FrequencyForm(DataTable dtSource)
        {
            InitializeComponent();
            _dtSource = dtSource;
        }

        private void ux_datagridviewFrecuency_CurrentCellChanged(object sender, EventArgs e)
        {
            Int32 selectedCellCount;
            Int32 intCol;
            Int32 intRow;
            String strValue;
            Int32 intSum;

            selectedCellCount = ux_datagridviewFrecuency.GetCellCount(DataGridViewElementStates.Selected);
            if (selectedCellCount > 1)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                intSum = 0;
                for (int i = 0; i < selectedCellCount; i++)
                {

                    intRow = ux_datagridviewFrecuency.SelectedCells[i].RowIndex;
                    intCol = ux_datagridviewFrecuency.SelectedCells[i].ColumnIndex;
                    strValue = ux_datagridviewFrecuency.Rows[intRow].Cells[intCol].Value.ToString();

                    int test;
                    if (int.TryParse(strValue, out test))
                    {
                        intSum = intSum + Int32.Parse(strValue);
                    }
                    else
                    {
                    }
                }
                if (intSum > 0)
                {
                    this.Text = "Frequency - Total Count: " + intSum.ToString();
                }
                else
                {
                    this.Text = "Frequency";
                }

            }
            else
            {
                this.Text = "Frequency";
            }
        }

        private void FrequencyForm_Load(object sender, EventArgs e)
        {
            if (_sharedUtils != null)
            {
                DataSet dsValues = _sharedUtils.GetWebServiceData(_values_dataview, _parameters, 0, 0);
                if (dsValues != null && dsValues.Tables.Contains(_values_dataview))
                {
                    _sharedUtils.BuildReadOnlyDataGridView(ux_datagridviewFrecuency, dsValues.Tables[_values_dataview]);
                }
                ux_datagridviewFrecuency.AutoResizeColumns();

                int width = 0;
                foreach (DataGridViewColumn column in ux_datagridviewFrecuency.Columns)
                    if (column.Visible == true)
                        width += column.Width;
                width += ux_datagridviewFrecuency.RowHeadersWidth;
                width += 20;

                ClientSize = new Size(width + 40, ClientSize.Height);

                // Get language translations
                if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
                if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);
            }
            else
            {
                ux_datagridviewFrecuency.DataSource = _dtSource;
                ux_datagridviewFrecuency.RowHeadersVisible = false;

                ux_datagridviewFrecuency.AutoResizeColumns();

                int width = 0;
                foreach (DataGridViewColumn column in ux_datagridviewFrecuency.Columns)
                    if (column.Visible == true)
                        width += column.Width;

                int height = ClientSize.Height;
                if (ux_datagridviewFrecuency.Rows.Count > 0)
                {
                    int allRowsHeight = ux_datagridviewFrecuency.Rows[0].Height * ux_datagridviewFrecuency.Rows.Count;
                    if(allRowsHeight > height)
                        width += 20;
                    else
                    {
                        height = Math.Max(ux_datagridviewFrecuency.Rows[0].Height * 5, allRowsHeight);
                    }
                }
                ClientSize = new Size(width, height);
            }
        }
    }
}
