﻿namespace SearchWizard
{
    partial class FrequencyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_datagridviewFrecuency = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewFrecuency)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_datagridviewFrecuency
            // 
            this.ux_datagridviewFrecuency.AllowUserToAddRows = false;
            this.ux_datagridviewFrecuency.AllowUserToDeleteRows = false;
            this.ux_datagridviewFrecuency.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewFrecuency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_datagridviewFrecuency.Location = new System.Drawing.Point(0, 0);
            this.ux_datagridviewFrecuency.Name = "ux_datagridviewFrecuency";
            this.ux_datagridviewFrecuency.Size = new System.Drawing.Size(284, 261);
            this.ux_datagridviewFrecuency.TabIndex = 2;
            // 
            // FrequencyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.ux_datagridviewFrecuency);
            this.Name = "FrequencyForm";
            this.Text = "FrequencyForm";
            this.Load += new System.EventHandler(this.FrequencyForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewFrecuency)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView ux_datagridviewFrecuency;
    }
}